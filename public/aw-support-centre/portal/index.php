<?php

define('SFNAMESPACE_REST', ''); //apexrest namespace for packaged sf
define('SFNAMESPACE_SOQL', ''); //soql namespace for packaged sf

// error reporting
error_reporting(0); // for live deployment
//error_reporting(E_ALL); // for debugging

// for problem alert email
$error_data = array();

// connote was 'production_ConsignmentNote__c' and is now 'Consignment_Note__c'

// reqs
require_once('include/php_sforce_nusoap/salesforce.php');
require_once('include/lib/auth_form.php');
require_once('include/lib/display_output.php');
require_once('include/lib/track_consignment.php');
require_once('include/lib/portal_questions.php');
require_once('include/lib/example_output_data.php');
require_once('include/lib/report_portal_login.php');

// disable caching
header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
header('Expires: Sat, 01 Jan 2000 01:00:00 GMT'); // date in the past
header('Pragma: no-cache');

##########################################################################################################################
if(isset($_GET['vendor']) && (
($_GET['vendor'] == '1031') || 
($_GET['vendor'] == '3183') ||
($_GET['vendor'] == '4084') ||  
($_GET['vendor'] == '3245') || 
($_GET['vendor'] == '3255') )) {

	if (isset($_GET['track_consignment']) && ($_GET['track_consignment'] != '')) {
		track_consignment($_GET['track_consignment'],$_GET['vendor']);
	} else {
	
		// example order process
		if(isset($_GET['albumid']) && isset($_GET['email']) && ($_GET['albumid'] == 'example') && ($_GET['email'] == 'example')) {
			// report login to db
			report_portal_login(TRUE,$_GET['vendor']);
			portal_display_output(example_portal_data($_GET['vendor']));
			die;
		}
		if($_GET['vendor']=='4084'){
			$_POST['albumid'] = '4084'.$_POST['albumid'];
		}
        //if(isset($_POST['albumid']) && is_numeric(preg_replace('/\s+/','',$_POST['albumid'])) && (strlen(preg_replace('/\s+/','',$_POST['albumid'])) > 8)) {
		if(isset($_POST['albumid'])) {
			if(isset($_POST['email']) && (preg_replace('/\s+/','',$_POST['email']) != '')) {
				portal_process($_GET['vendor'],preg_replace('/\s+/','',$_POST['albumid']),preg_replace('/\s+/','',$_POST['email']));
			} else {
				portal_login($_GET['vendor'],'invalid_email');
			}
		} elseif (isset($_POST['albumid'])) {            
			portal_login($_GET['vendor'],'invalid_albumid');
		} else {
			portal_login($_GET['vendor'],'standard');
		}
		
	}

} else {
	// if no vendor is provided, display blank page - 403
	header('HTTP/1.1 403 Forbidden');
}
##########################################################################################################################

##########################################################################################################################
function portal_process($portal_vendor,$portal_albumid,$portal_email) {

	$sfcom_result = portal_sfcom($portal_albumid);
    //echo '<pre>'; print_r($sfcom_result); echo '</pre>';
	// test to ensure a proper response was received from salesforce (i.e. system is not down)
	if (($sfcom_result == 'system_down') || ($sfcom_result == NULL) || ($sfcom_result == '') || ($sfcom_result == 'system_failed')) {
		portal_login($portal_vendor,'system_down');
		portal_down_alert('Bad SF response');
	// test to see if albumid is valid, if the vendor matches, and make sure its not cancelled.
	} elseif ($sfcom_result == 'invalid_albumid') {
		// even though invalid_albumid returned, test to see if in backoffice (i.e. BESI not updated yet)
			$nlbe_result = check_nlbe($portal_albumid);
			if ($nlbe_result == 'no_nlbe_result') {
				portal_login($portal_vendor,'invalid_albumid');
			} elseif (is_array($nlbe_result)) {
				if (strtolower($nlbe_result[SFNAMESPACE_SOQL.'Customer_Email__c']) == strtolower($portal_email)) {
					portal_output_preflight($nlbe_result,$portal_vendor);
				} else {                    
					portal_login($portal_vendor,'auth_mismatch');
					// report login to db
					report_portal_login(FALSE,$portal_vendor);
				}
			} else {
				portal_login($portal_vendor,'system_down');
				portal_down_alert('Bad NLBE response');
			}
	} elseif (is_array($sfcom_result)) {
		if (($sfcom_result[SFNAMESPACE_SOQL.'Cancelled__c'] == 'true')) {
		portal_login($portal_vendor,'invalid_albumid');
		} else {
			// test if name matches albumid
			if(	strtolower(($sfcom_result[SFNAMESPACE_SOQL.'Customer_Email__c']) != strtolower($portal_email)) &&
				strtolower(($sfcom_result[SFNAMESPACE_SOQL.'Customer_Email_text__c']) != strtolower($portal_email))) {
				portal_login($portal_vendor,'auth_mismatch');
				// report login to db
				report_portal_login(FALSE,$portal_vendor);
			} else {
				// all good. now pre-process $sfcom_result array
				$sfcom_result[SFNAMESPACE_SOQL.'Order_Quantity__c'] = (int)$sfcom_result[SFNAMESPACE_SOQL.'Order_Quantity__c'];
				$sfcom_result[SFNAMESPACE_SOQL.'Amount_incGST__c'] = number_format($sfcom_result[SFNAMESPACE_SOQL.'Amount_incGST__c'],2);
				$sfcom_result[SFNAMESPACE_SOQL.'Upload_time__c'] = portal_date_transform($sfcom_result[SFNAMESPACE_SOQL.'Upload_time__c']);
				$sfcom_result[SFNAMESPACE_SOQL.'Pay_Date__c'] = portal_date_transform($sfcom_result[SFNAMESPACE_SOQL.'Pay_Date__c']);
				$sfcom_result[SFNAMESPACE_SOQL.'Date_Dispatched__c'] = portal_date_transform_short($sfcom_result[SFNAMESPACE_SOQL.'Date_Dispatched__c']);

				portal_output_preflight($sfcom_result,$portal_vendor);
			}
		}
	} else {
		portal_login($portal_vendor,'system_down');
		portal_down_alert('sfcom_result array malformed');
	}
}
##########################################################################################################################

function query_2023($query){
	try{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://pwg:MCgC_riGqERn2oERhEK@api.photo-products.com.au/zenpanel/api/sf/query');
		curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$query, true]));
		$response = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		if(!in_array($status, [0, 200]))
			throw new Exception($response);

		return json_decode($response);
	}
	catch(Exception $e){
		return 'ERROR: '.$e->getMessage();
	}
}

##########################################################################################################################
function portal_sfcom($portal_albumid) {

	global $error_data;

	// auth details
	$username = getenv('PWWSF_USERNAME');
	$error_data['salesforce_user'] = $username;
	$error_data['salesforce_query'] = '';
	$error_data['time'] = date("r");

		$portal_query = "Select ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."VendorID__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Portal_Stage__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Cover_Summary__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Can_Have_Cover__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Cover_Title_Text__c, ".SFNAMESPACE_SOQL."Order__r.Name, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Customer_Email__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Article_Description__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Order_Quantity__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Amount_incGST__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Delivery_Type__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Dispatch_Method__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Cancelled__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Order_Type__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Upload_time__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Pay_Status__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Pay_Date__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Portal_Comments__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Portal_Comments_icon__c, ".SFNAMESPACE_SOQL."production_File_Ripped__c, ".SFNAMESPACE_SOQL."production_PrintQueued__c, ".SFNAMESPACE_SOQL."production_File_Printed__c, ".SFNAMESPACE_SOQL."production_Printed__c, ".SFNAMESPACE_SOQL."production_Bound__c, ".SFNAMESPACE_SOQL."production2_Download_Time_Cover__c, ".SFNAMESPACE_SOQL."production2_PrintedCoverDate__c, ".SFNAMESPACE_SOQL."production2_Bound_Cover__c, ".SFNAMESPACE_SOQL."Consignment_Note__c, ".SFNAMESPACE_SOQL."Delivery_Company_Name__c, ".SFNAMESPACE_SOQL."Delivery_Name__c, ".SFNAMESPACE_SOQL."Delivery_Address_1__c, ".SFNAMESPACE_SOQL."Delivery_Address_2__c, ".SFNAMESPACE_SOQL."Delivery_Suburb_Town__c, ".SFNAMESPACE_SOQL."Delivery_Post_Code__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Processed__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Process_Status__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Date_Dispatched__c, ".SFNAMESPACE_SOQL."Order__r.".SFNAMESPACE_SOQL."Customer_Email_text__c FROM ".SFNAMESPACE_SOQL."LineID__c WHERE ".SFNAMESPACE_SOQL."Order__r.Name = '$portal_albumid'";

	    $portal_query_result = query_2023($portal_query);
	    if(is_string($portal_query_result))
	    	die($portal_query_result);

		$portal_query_result_status = $portal_query_result['done'];
		$portal_query_result_count = $portal_query_result['size'];
        
		// single line
		if (($portal_query_result_status == 'true') && ($portal_query_result_count == 1)) {
			
			// sort out line data
			foreach ($portal_query_result['records']->values as $name => $field) {
				if(is_string($portal_query_result['records']->values[$name]) || is_null($portal_query_result['records']->values[$name])) {
					$portal_query_result_line['delivery_data_array'][0]['this_line_count'] = '1';
					if (strstr($name,'production') == FALSE) {
						$portal_query_result_line['delivery_data_array'][0][$name] = $field;
					} elseif ($name == SFNAMESPACE_SOQL.'Consignment_Note__c') {
						if ($field != '') {
							$portal_query_result_line['delivery_data_array'][0][$name][0] = $field;
						}
					} else {
						$portal_query_result_line[$name] = $field;
					}
				}
			}
			
			// sort out order data
			foreach ($portal_query_result['records']->values[SFNAMESPACE_SOQL.'Order__r']->values as $name => $field) {
				if(is_string($portal_query_result['records']->values[SFNAMESPACE_SOQL.'Order__r']->values[$name]) || is_null($portal_query_result['records']->values[SFNAMESPACE_SOQL.'Order__r']->values[$name])) {
					$portal_query_result_order[$name] = $field;
				}
			}
			
			$portal_query_result = array_merge($portal_query_result_order,$portal_query_result_line);
			
			$portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c'] = portal_date_transform($portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c']);
			$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'] = portal_date_time_transform($portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c']);
			$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] = portal_date_transform($portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c']);
			
			if($portal_query_result[SFNAMESPACE_SOQL.'production2_Download_Time_Cover__c'] != '') {
				if($portal_query_result[SFNAMESPACE_SOQL.'production2_PrintedCoverDate__c'] == '') {
					$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'] = '';
				}
                /*
				if($portal_query_result['production2_Bound_Cover__c'] == '') {
					$portal_query_result['production_Bound__c'] = '';
				}
                */
			}
            
            if($portal_query_result[SFNAMESPACE_SOQL.'VendorID__c'] == '1337') $portal_query_result[SFNAMESPACE_SOQL.'VendorID__c'] = '1031';
			
			return $portal_query_result;
			
		// multiple lines
		} elseif (($portal_query_result_status == 'true') && ($portal_query_result_count > 1)) {
            
			foreach ($portal_query_result['records'] as $key => $line) {
				
				// sort out line data
				foreach ($portal_query_result['records'][$key]->values as $name => $field) {
					if(is_string($portal_query_result['records'][$key]->values[$name]) || is_null($portal_query_result['records'][$key]->values[$name])) {
						$portal_query_result_line['line_data'][$key][$name] = $field;
					}
				}
				
				// sort out order data
				if ($key == 0) { // we only need the first line, as for order data all the lines are identical
					foreach ($portal_query_result['records'][$key]->values[SFNAMESPACE_SOQL.'Order__r']->values as $name => $field) {
						if(is_string($portal_query_result['records'][$key]->values[SFNAMESPACE_SOQL.'Order__r']->values[$name]) || is_null($portal_query_result['records'][$key]->values[SFNAMESPACE_SOQL.'Order__r']->values[$name])) {
							$portal_query_result_order[$name] = $field;
						}
					}
				}
				
			}

			$portal_query_result = array_merge($portal_query_result_order,$portal_query_result_line);

			$portal_query_result['delivery_data_array'] = array();
			
			foreach ($portal_query_result['line_data'] as $key => $value) {
				$address_combo_key = $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Address_1__c'].'_'.$portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Suburb_Town__c'].'_'.$portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Post_Code__c'];
				if (isset($portal_query_result['delivery_data_array'][$address_combo_key])) {
					$portal_query_result['delivery_data_array'][$address_combo_key]['this_line_count']++;
					
					if ($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'] != '') {
						$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Consignment_Note__c'][$key + 1] = $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'];
					}
					$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Consignment_Note__c'] = array_unique($portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Consignment_Note__c']);
				} else {
					$portal_query_result['delivery_data_array'][$address_combo_key]['this_line_count'] 				= 1;
                    $portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Delivery_Company_Name__c']     = $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Company_Name__c'];
					$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Delivery_Name__c'] 			= $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Name__c'];
					$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Delivery_Address_1__c'] 		= $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Address_1__c'];
					$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Delivery_Address_2__c'] 		= $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Address_2__c'];
					$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Delivery_Suburb_Town__c'] 		= $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Suburb_Town__c'];
					$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Delivery_Post_Code__c'] 		= $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Delivery_Post_Code__c'];
					
					if ($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'] != '') {
						$portal_query_result['delivery_data_array'][$address_combo_key][SFNAMESPACE_SOQL.'Consignment_Note__c'][0] = $portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'];
					}
				}
			}
			
			$new_key = 0;
			foreach ($portal_query_result['delivery_data_array'] as $key => $value) {
				$portal_query_result['delivery_data_array'][$new_key] = $value;
				unset($portal_query_result['delivery_data_array'][$key]);
				$new_key++;
			}
	
			$portal_query_result[SFNAMESPACE_SOQL.'production_File_Ripped__c'] = 'true';
			$portal_query_result[SFNAMESPACE_SOQL.'production_File_Printed__c'] = 'true';
			$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] = 'true';
			foreach ($portal_query_result['line_data'] as $key => $value) {
				$portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_PrintQueued__c'] = portal_date_transform($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_PrintQueued__c']);
				
				if (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_File_Ripped__c'] == 'false') || ($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_File_Ripped__c'] == '')) {
					$portal_query_result[SFNAMESPACE_SOQL.'production_File_Ripped__c'] = 'false';
				}
				if (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_File_Printed__c'] == 'false') || ($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_File_Printed__c'] == '') || (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production2_Download_Time_Cover__c'] != '') && ($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production2_PrintedCoverDate__c'] == ''))) {
					$portal_query_result[SFNAMESPACE_SOQL.'production_File_Printed__c'] = 'false';
				}
				if (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_Bound__c'] == '') || (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production2_Download_Time_Cover__c'] != '') && ($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production2_Bound_Cover__c'] == ''))) {
					$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] = 0;
				}
			}
			
			$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'] = 0;
			if ($portal_query_result[SFNAMESPACE_SOQL.'production_File_Printed__c'] == 'true') {
				foreach ($portal_query_result['line_data'] as $key => $value) {
					if (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_Printed__c'] != '') && ((int)strtotime(str_replace(array('T','.000Z'),array(' ','+000'),$portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_Printed__c'])) > (int)$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'])) {
						$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'] = strtotime(str_replace(array('T','.000Z'),array(' ','+000'),$portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_Printed__c']));
					}
				}
			}
			
			if ($portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] === 'true') {
				$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] = 0;
				foreach ($portal_query_result['line_data'] as $key => $value) {
					if (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_Bound__c'] != '') && ((int)strtotime($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_Bound__c']) > (int)$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'])) {
						$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] = strtotime($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_Bound__c']);
					}
				}
			}
			
			$portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c'] = 0;
			if ($portal_query_result[SFNAMESPACE_SOQL.'production_File_Ripped__c'] == 'true') {
				foreach ($portal_query_result['line_data'] as $key => $value) {
					if (($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_PrintQueued__c'] != '') && ((int)strtotime($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_PrintQueued__c']) > (int)$portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c'])) {
						$portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c'] = strtotime($portal_query_result['line_data'][$key][SFNAMESPACE_SOQL.'production_PrintQueued__c']);
					}
				}
			}
			
			if ($portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'] != 0) {
				$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'] = date("D j M h:iA",$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c']);
			} else {
				$portal_query_result[SFNAMESPACE_SOQL.'production_Printed__c'] = '';
			}

			if ($portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c'] != 0) {
				$portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c'] = date("D j M",$portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c']);
			} else {
				$portal_query_result[SFNAMESPACE_SOQL.'production_PrintQueued__c'] = '';
			}
			
			if ($portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] != 0) {
				$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] = date("D j M",$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c']);
			} else {
				$portal_query_result[SFNAMESPACE_SOQL.'production_Bound__c'] = '';
			}
			
			unset($portal_query_result['line_data']);
			
			return $portal_query_result;
			
		} elseif (($portal_query_result_status == 'true') && ($portal_query_result_count == 0)) {
		                      
		// no matching albumid
		return 'invalid_albumid';
		
		} else {
		
		// request failed - malformed query?
		return 'system_failed';
		
		}

}
##########################################################################################################################

##########################################################################################################################
function portal_date_transform($input_date) {
	if ($input_date != '') {
		preg_match('/([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2}).000Z/', $input_date, $output_date);
		return date("D j M",mktime($output_date[4]+10+date("I"),$output_date[5],$output_date[6],$output_date[2],$output_date[3],$output_date[1]));
	} else {
		return '';
	}
}
##########################################################################################################################

##########################################################################################################################
function portal_date_time_transform($input_date) {
	if ($input_date != '') {
		preg_match('/([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2}).000Z/', $input_date, $output_date);
		return date("D j M h:iA",mktime($output_date[4]+10+date("I"),$output_date[5],$output_date[6],$output_date[2],$output_date[3],$output_date[1]));
	} else {
		return '';
	}
}
##########################################################################################################################

##########################################################################################################################
function portal_date_transform_short($input_date) {
	if ($input_date != '') {
		preg_match('/([0-9]{4})-([0-9]{2})-([0-9]{2})/', $input_date, $output_date);
		return date("D j M",mktime(1,1,1,$output_date[2],$output_date[3],$output_date[1]));
	} else {
		return '';
	}
}
##########################################################################################################################

##########################################################################################################################
function portal_date_transform_nlbe($input_date) {
	if ($input_date != '') {
		list($nl_date,$nl_time) = explode(' ',$input_date);
		$nl_date = explode('.',$nl_date);
		$nl_time = explode(':',$nl_time);
		return date("D j M",mktime($nl_time[0]+9,$nl_time[0],0,$nl_date[0],$nl_date[1],$nl_date[2]));
	} else {
		return '';
	}
}
##########################################################################################################################

##########################################################################################################################
function check_nlbe($albumid) {

	$url = 'https://backoffice.albumprinter.com/findorders.asp';
	$params = "account=APUA&password=ifudysdfuy3&login=Login&languageid=EN&show=Search&albumid=$albumid";
	$user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";

	$curl_handle = curl_init();
	curl_setopt($curl_handle, CURLOPT_POST,1);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$params);
	curl_setopt($curl_handle, CURLOPT_URL,$url);
	curl_setopt($curl_handle, CURLOPT_COOKIEJAR, 'backend_proxy_cookie.txt');
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST,  2);
	curl_setopt($curl_handle, CURLOPT_USERAGENT, $user_agent);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);

	$curl_result = curl_exec ($curl_handle);
	curl_close ($curl_handle);
	
	if ((preg_match('/<td nowrap><a title="Show this order \(OrderID: \d+\)" href="\?show=true&albumid=(\d+)">(\d+)<\/a>/is',$curl_result,$curl_result_out) == TRUE) && ($curl_result_out[1] == $curl_result_out[2])) {
		$return_results = array('Name' => $curl_result_out[1]);
		if (preg_match_all('/<td nowrap>(.*)<\/td>/',$curl_result,$curl_result_out) == TRUE) {
			$return_results[SFNAMESPACE_SOQL.'Upload_time__c'] = portal_date_transform_nlbe($curl_result_out[1][0]); // order/upload date from backend
			$return_results[SFNAMESPACE_SOQL.'Pay_Date__c'] = portal_date_transform_nlbe($curl_result_out[1][2]); // pay date from backend
			$return_results[SFNAMESPACE_SOQL.'Pay_Status__c'] = strtoupper($curl_result_out[1][3]); // pay status from backend - eg Ordered, CAPTURED, empty etc
			if ((preg_match('/<a title="Send message to customer" href="mailto:(.+)">(.+)<\/a>/is',$curl_result_out[1][6],$curl_result_out_2) == TRUE) && ($curl_result_out_2[1] == $curl_result_out_2[2])) {
				$return_results[SFNAMESPACE_SOQL.'Customer_Email__c'] = $curl_result_out_2[1];
				if ((preg_match('/<div style="text-decoration:underline;cursor:hand" title="Show Invoice" onClick="javascript:window.open\(\'http:\/\/api.albumprinter.com\/client\/invoiceView.ashx\?orderid=\d+&vendorid=\d+\', \'window\', \'height=500, width=460, toolbar=no, scrollbars=1\'\)" target="_blank">(\d+\.\d+)<\/div>/is',$curl_result_out[1][4],$curl_result_out) == TRUE)) {
					$return_results[SFNAMESPACE_SOQL.'Amount_incGST__c'] = $curl_result_out[1];
					if (preg_match('#<td colspan="6">(.+)<\/td>#',$curl_result,$curl_result_out)) {
						$return_results[SFNAMESPACE_SOQL.'Article_Description__c'] = preg_replace('#\(.+\)#','',preg_replace('#\[.+\]#','',$curl_result_out[1]));
						if (preg_match_all('/<a style="text-decoration: none" title="Reset" href="reset.asp\?orderid=\d+&albumid=\d+&lineid=\d+" target="_blank"><font color="#FF0000">\*<\/font><\/a>/is',$curl_result,$curl_result_out) != FALSE) {
							$return_results[SFNAMESPACE_SOQL.'Order_Quantity__c'] = count($curl_result_out[0]);
							return $return_results;
						} else {
							echo '<!-- nlbe fail check 6 -->';
							return 'no_nlbe_result';
						}
					} else {
						echo '<!-- nlbe fail check 5 -->';
						return 'no_nlbe_result';
					}
				} else {
					echo '<!-- nlbe fail check 4 -->';
					return 'no_nlbe_result';
				}
			} else {
				echo '<!-- nlbe fail check 3 -->';
				return 'no_nlbe_result';
			}
		} else {
			echo '<!-- nlbe fail check 2 -->';
			return 'no_nlbe_result';
		}		
	} else {
		echo '<!-- nlbe fail check 1 -->';
		return 'no_nlbe_result';
	}
	
}
##########################################################################################################################

##########################################################################################################################
function portal_output_preflight($pre_out_data,$portal_vendor) {
	// report login to db
	report_portal_login(TRUE,$portal_vendor);
	
	$output_fields = array(SFNAMESPACE_SOQL.'Cover_Summary__c',SFNAMESPACE_SOQL.'Can_Have_Cover__c',SFNAMESPACE_SOQL.'Cover_Title_Text__c',SFNAMESPACE_SOQL.'Portal_Stage__c',SFNAMESPACE_SOQL.'VendorID__c','Name',SFNAMESPACE_SOQL.'Customer_Email__c',SFNAMESPACE_SOQL.'Article_Description__c',SFNAMESPACE_SOQL.'Order_Quantity__c',SFNAMESPACE_SOQL.'Amount_incGST__c',SFNAMESPACE_SOQL.'Delivery_Type__c',SFNAMESPACE_SOQL.'Dispatch_Method__c',SFNAMESPACE_SOQL.'Cancelled__c',SFNAMESPACE_SOQL.'Order_Type__c',SFNAMESPACE_SOQL.'Upload_time__c',SFNAMESPACE_SOQL.'Pay_Status__c',SFNAMESPACE_SOQL.'Pay_Date__c',SFNAMESPACE_SOQL.'Portal_Comments__c',SFNAMESPACE_SOQL.'Portal_Comments_icon__c',SFNAMESPACE_SOQL.'Processed__c',SFNAMESPACE_SOQL.'Process_Status__c',SFNAMESPACE_SOQL.'Date_Dispatched__c',SFNAMESPACE_SOQL.'production_File_Ripped__c',SFNAMESPACE_SOQL.'production_PrintQueued__c',SFNAMESPACE_SOQL.'production_Bound__c',SFNAMESPACE_SOQL.'production_File_Printed__c',SFNAMESPACE_SOQL.'production_Printed__c',SFNAMESPACE_SOQL.'production2_Download_Time_Cover__c',SFNAMESPACE_SOQL.'production2_PrintedCoverDate__c',SFNAMESPACE_SOQL.'production2_Bound_Cover__c');
	foreach ($output_fields as $key => $value) {
		if ((!isset($pre_out_data[$value])) || ($pre_out_data[$value] == '')) {
			if ($value == SFNAMESPACE_SOQL.'VendorID__c' && ((!isset($pre_out_data[$value])) || ($pre_out_data[$value] == ''))) {
				$pre_out_data[$value] = $portal_vendor;
			} elseif ($value == SFNAMESPACE_SOQL.'Pay_Status__c') {
				// if pay status/date is empty in SF, get from backend just in case
				$sf_data = check_nlbe($pre_out_data['Name']);
				if(is_array($sf_data)) {
					$pre_out_data[SFNAMESPACE_SOQL.'Pay_Status__c'] = $sf_data[SFNAMESPACE_SOQL.'Pay_Status__c'];
					$pre_out_data[SFNAMESPACE_SOQL.'Pay_Date__c'] = $sf_data[SFNAMESPACE_SOQL.'Pay_Date__c'];
				} else {
					$pre_out_data[SFNAMESPACE_SOQL.'Pay_Status__c'] = '';
					$pre_out_data[SFNAMESPACE_SOQL.'Pay_Date__c'] = '';
				}
			} else {
				$pre_out_data[$value] = '';
			}
		}
	}
	portal_display_output($pre_out_data);
}
##########################################################################################################################

##########################################################################################################################
function portal_down_alert($reason = 'General') {
	global $error_data;
	mail('system.admin@pictureworks.com.au','AUTO ALERT: Portal Down! By the Beard of Zeus!',"This is an automatic email from the Portal to let you know that I appear to be not working very well :-( so you better fix me or customers will miss out on glorious information.\n\n
	==Available Details==\n
	AlbumID: {$_POST['albumid']}\n
	Vendor: {$_GET['vendor']}\n
	Salesforce User: {$error_data['salesforce_user']}\n
	Salesforce Query: {$error_data['salesforce_query']}\n
	Time: {$error_data['time']}\n
	Reason: $reason");
}
##########################################################################################################################

?>
