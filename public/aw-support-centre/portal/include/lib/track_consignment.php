<?php

function track_consignment($consignment,$vendor) {

	echo <<<ENDECHO
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

	<head>
		<link rel="stylesheet" type="text/css" media="screen" title="Default Style" href="include/css/index_1031.css" />
		<title>Track Consignment</title>
	</head>

	<body>
	
		<div id="track_consignment">
			<span id="consignment_title">Show Tracking Information for Consignment $consignment</span>
			<div id="consignment_desc">
				<p>We use Toll Priority as our courier service provider, and all tracking information for your consignment is provided by them.</p>
				<p>Clicking the button below will take you to an Toll Priority page, with the latest details about your consignment. Please note that there may be a delay of a couple of hours in this information.</p>
				<p>If you would like more information on tracking the status of the consignment or have any issue with the delivery of your order, please contact Toll Priority for further assistance on their <a href="https://online.toll.com.au/trackandtrace/">website</a>.</p>
				<p>If clicking the button below does not take you to the tracking page, please visit <a href="https://online.toll.com.au/trackandtrace/">online.toll.com.au/trackandtrace</a>, enter your consignment number ($consignment) and click [Submit].</p>
			</div>
            <form action="https://online.toll.com.au/trackandtrace/">
                <input id="consignment_submit" name="searchbutton" type="submit" value="I agree, show tracking info" onclick="" />
            </form>
            <!--
			<form method="post" action="http://203.43.1.230/scripts/cgiip.exe/WService=wtsaae/inquiry.w">
				<fieldset id="consignment_fieldset">
					<input type="hidden" name="inquirynumber" value="$consignment" />
					<input id="consignment_submit" name="searchbutton" type="submit" value="I agree, show tracking info" onclick="" />
				</fieldset>
			</form>
            -->
		</div>
		
		<script src="https://www.google-analytics.com/urchin.js" type="text/javascript"></script>
		<script type="text/javascript">_uacct = "UA-1338422-1"; urchinTracker();</script>
		
	</body>
	
</html>
ENDECHO;

}

?>