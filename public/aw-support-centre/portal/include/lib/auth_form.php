<?php
function portal_login($portal_vendor,$portal_login_mode) {

// echo <<<ENDECHO
// <div>======================================================</div>
// <div><strong>QC info:</strong></div>
// <div>vendor: $portal_vendor</div>
// <div>login mode/response: $portal_login_mode</div>
// <div>======================================================</div>
	switch($portal_vendor){
		case '1031':
			$vendorName = 'albumworks';
			$vendorShort = 'AP';
			break;
		case '3183':
			$vendorName = 'Officeworks Photobooks';
			$vendorShort = 'OW';
			break;
		case '3255':
			$vendorName = 'Target Photobooks';
			$vendorShort = 'TG';
			break;
	}
    
    $hijack = isset($_REQUEST['hijack']) ? $_REQUEST['hijack'] : false;    
    
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" title="Default Style" href="include/css/login_tbm.css" />
<script src="/aw-support-centre/portal/include/js/portal_login.js" type="text/javascript"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.smartedit').each(function(){
            if(jQuery(this).attr('alt') == '')
                jQuery(this).attr('alt', jQuery(this).val());
        }).focus(function(){
            if(jQuery(this).val() == jQuery(this).attr('alt'))
                jQuery(this).val('').addClass('activesmart');
        }).blur(function(){
            if(jQuery(this).val() == '')
                jQuery(this).val(jQuery(this).attr('alt')).removeClass('activesmart');
        });
    });
</script>
<title>Track My Order - <?php echo $vendorName; ?> login page</title>
</head>

<body>

<form id="portal_auth_form" method="post" action="index.php?vendor=1031">
<input type="hidden" name="vendor" id="portal_auth_form_vendor" value="<?php echo $portal_vendor; ?>" />
  
  <table class="formtable" border="0" cellspacing="5" style="position: relative; top: -5px; width:100%; " >
  <?php if ($portal_vendor == "1031" and !$hijack) {  //Albumprinter ?>
    <!--<tr style="float: left;">
      <td>
          <img alt="" src="http://probuntu/albumworks/img/pages/track/02.png"></img>
      </td>
    </tr>-->
  <tr>
    <td class="headings" colspan="4">
        <h2>Where is my order?</h2>
        <p>Track the progress of any current order</p>
    </td>
  </tr>
  <?php } ?>
<?php if ($portal_vendor == "3183") {  //Officeworks ?>
<!--<tr>
    <td colspan="4" cellspacing="0" style="padding: 0; margin: 0; background: #FFFFFF url(include/img/ow_subtitle_roundcorners.gif) 16px 0 repeat-x;"><h1 style="position: relative; left: -5px; display: block; padding: 0; margin: 0; padding-left: 10px; margin-left: 5px; margin-right: 0px; font-weight: bold; font-size: 12pt; color: #FFFFFF; background: #005AAB url(include/img/ow_subtitle_roundcorners.gif) -18px 0 no-repeat; height: 32px; line-height: 32px; ">Track My Order</h1></td>
  </tr> --><?php } ?>

<?php if ($portal_vendor == "3255") { //Target - dont show heading in iframe ?><?php } ?> 

  <?php if($hijack){ ?>
    
    <tr>
        <td colspan="2">
            <div id="hijack" class="<?php echo $hijack; ?>">
                <input type="text" name="albumid" id="hijack_albumid" class="smartedit inputbox" value="Order ID" size="19" maxlength="20" />
                <input type="text" name="email" id="hijack_email" class="smartedit inputbox" value="Email Address"  size="40" maxlength="250" />
                <button type="submit" name="portal_auth" id="hijack_button" class="button" value="submit">TRACK ORDER</button>
                <input type="hidden" name="hijack" value="<?php echo $hijack; ?>" />
            </div>
        </td>
    </tr>

    
  <?php } else{ ?>
  
    
    <tr>
        <td colspan="4" class="for able" >
            <p>
                <label for="portal_auth_form_albumid">Order ID:</label>
                <input type="text" name="albumid" id="portal_auth_form_albumid" class="smartedit inputbox" value="Order ID" size="19" maxlength="20" />
                <span class="note">Tip: Your AlbumID was emailed to you when you uploaded your order.</span>
            </p>
            <p>
                <label for="portal_auth_form_albumid">Email:</label>
                <input type="text" name="email" id="portal_auth_form_email" class="smartedit inputbox" value="Email Address"  size="40" maxlength="250" />
            </p>
            <p class="loginbuttontd" style="clear:both">
                <button type="submit" name="portal_auth" id="portal_auth_form_submit" class="cta inline" value="submit">TRACK ORDER</button>
            </p>
        </td>
    </tr>

<?php
  } // end not-hijack
?> 
	<tr>
		<td colspan="2"><?php if (($portal_login_mode == "invalid_email") || ($portal_login_mode == "invalid_albumid") || ($portal_login_mode == "auth_mismatch")) { ?> 
		<span class="error_message"><strong>Sorry, the details you have entered aren't appearing in our database. <!-- <?=$portal_login_mode?> --></strong>
		<ul style="list-style: none">
			<li>Can you please try again by retyping your AlbumID and Email address.</li>
		</ul>
    </span><?php } elseif($portal_login_mode == "system_down")
		{ ?> <span class="error_message"><strong>Our database appears to be down at the moment.  We're currently working on fixing this and hope to have it back up again soon.<br />
			We apologise for the inconvenience. Please try again later.</strong></span> <?php } ?> 
</td>
    </tr>
  <?php if(!$hijack){ ?>
  <tr>
    <td colspan="3">
        
    </td>
  </tr>
  <?php } ?>
		
</table>

</form>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try{
var pageTracker = _gat._getTracker("UA-1338422-1");
pageTracker._trackPageview();
} catch(err) {}
</script>

</body>
</html>

<?php
}
?>