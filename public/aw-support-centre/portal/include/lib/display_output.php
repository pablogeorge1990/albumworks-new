<?php

function portal_display_output($output_data) {
    //if($output_data['Name'] == '10400119463') { print_r($output_data); die; }
    
	$generated_data = array();
	if(isset($_GET['albumid']) && isset($_GET['email']) && ($_GET['albumid'] == 'example') && ($_GET['email'] == 'example')) {
		$generated_data['invoice_link'] = '<a class="invoice_link_dud" title="Invoice not availble for this example order">[ <del>INVOICE</del> ]</a>';
	} elseif(strtoupper($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']) == 'STORE') {
		$generated_data['invoice_link'] = '<a class="invoice_link_dud" title="Invoice not availble for store delivered orders">[ <del>INVOICE</del> ]</a>';
	} else {
		$generated_data['invoice_link'] = '<a class="invoice_link" onclick="window.open(\'https://www.albumworks.com.au/aw-support-centre/portal/invoice.php?albumid='.$output_data['Name'].'&amp;email='.$output_data[SFNAMESPACE_SOQL.'Customer_Email__c'].'\',\'Order_Invoice\',\'toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes,width=750,height=600,top=100,left=100\'); return false;" href="/aw-support-centre/portal/invoice.php?albumid='.$output_data['Name'].'&amp;email='.$output_data[SFNAMESPACE_SOQL.'Customer_Email__c'].'" title="Click here for invoice">[INVOICE]</a>';
	}

	switch ($output_data[SFNAMESPACE_SOQL.'Can_Have_Cover__c']) {
		case '1':
			$generated_data['covertype_title'] = 'Cover type:';
			$generated_data['covertype_value'] = $output_data[SFNAMESPACE_SOQL.'Cover_Summary__c'];
			break;
		default:
			$generated_data['covertype_title'] = '';
			$generated_data['covertype_value'] = '';
			break;
	}

	$generated_data['covertitle_title'] = '';
	$generated_data['covertitle_value'] = '';

	switch ($output_data[SFNAMESPACE_SOQL.'Portal_Comments_icon__c']) {
		case 'Informative':
			$generated_data['portal_icon_id'] = 'comments_talk';
			break;
		case 'Warning':
			$generated_data['portal_icon_id'] = 'comments_excl';
			break;
		case 'Question':
			$generated_data['portal_icon_id'] = 'comments_ques';
			break;
		default:
			$generated_data['portal_icon_id'] = 'comments_none';
			break;
	}
	
	switch ($output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c']) {
		case 'Courier':
			$generated_data['deliver_method_note'] = 'Delivery via DHL';
			break;
		case 'Aust Post':
			$generated_data['deliver_method_note'] = 'Delivery via DHL';
			break;
		case 'Other':
			$generated_data['deliver_method_note'] = '';
			break;
		default:
			$generated_data['deliver_method_note'] = 'Delivery details are yet to be updated. Please check back shortly.';
			break;
	}
	
	switch (strtoupper($output_data[SFNAMESPACE_SOQL.'Pay_Status__c'])) {
		case 'CAPTURED':
		case 'AUTHORISED':
		case 'VIA_PREVIOUS_ORDER':
		case 'MANUAL':
			$generated_data['process_payment_id'] = 'processflow_payment_tick';
			break;
		case 'ORDERED':
		case 'INVOICE':
			$generated_data['process_payment_id'] = 'processflow_payment_inst';
			break;
		case 'REFUSED':
			$generated_data['process_payment_id'] = 'processflow_payment_cros';
			break;
		case (preg_match("/REFUND|CHARGE/i", strtoupper($output_data[SFNAMESPACE_SOQL.'Pay_Status__c']))?strtoupper($output_data[SFNAMESPACE_SOQL.'Pay_Status__c']):!strtoupper($output_data[SFNAMESPACE_SOQL.'Pay_Status__c'])):
			$generated_data['process_payment_id'] = 'processflow_payment_excl';
			$generated_data['process_payment_text'] = $output_data[SFNAMESPACE_SOQL.'Pay_Date__c'].'<br />'.ucwords(str_replace('_',' ',$output_data[SFNAMESPACE_SOQL.'Pay_Status__c']));
			break;
		default:
			$generated_data['process_payment_id'] = 'processflow_payment_grey';
			break;
	}
	
	if (strtoupper($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']) == "STORE") {
		$generated_data['process_payment_text'] = 'Payment on Collection';
	} elseif (strtoupper($output_data[SFNAMESPACE_SOQL.'Pay_Status__c']) == 'VIA_PREVIOUS_ORDER') {
		$generated_data['process_payment_text'] = 'Via Previous Order';
	} elseif (strtoupper($output_data[SFNAMESPACE_SOQL.'Pay_Status__c']) == 'INVOICE') {
		$generated_data['process_payment_text'] = 'Invoice Payment';
	} else {
		if (!isset($generated_data['process_payment_text'])) {
			$generated_data['process_payment_text'] = $output_data[SFNAMESPACE_SOQL.'Pay_Date__c'];
		}
	}

	switch (strtoupper($output_data[SFNAMESPACE_SOQL.'Process_Status__c'])) {
		case 'OK':
			$generated_data['process_process_id'] = 'processflow_process_tick';
			break;
		case '':
		case 'UNPROCESSED':
			$generated_data['process_process_id'] = 'processflow_process_grey';
			break;
		default:
			$generated_data['process_process_id'] = 'processflow_process_excl';
			break;
	}
	
	switch (strtoupper($output_data[SFNAMESPACE_SOQL.'production_File_Printed__c'])) {
		case 'TRUE':
			$generated_data['process_print_id'] = 'processflow_print_tick';
			$generated_data['process_print_date'] = $output_data[SFNAMESPACE_SOQL.'production_Printed__c'];
			break;
		default: 
			$generated_data['process_print_id'] = 'processflow_print_grey';
			$generated_data['process_print_date'] = '';
			break;
	}
	
	switch ($output_data[SFNAMESPACE_SOQL.'production_Bound__c']) {
		case '':
			$generated_data['process_bind_id'] = 'processflow_bind_grey';
			break;
		default:
			$generated_data['process_bind_id'] = 'processflow_bind_tick';
			break;
	}

	switch (strtoupper($output_data[SFNAMESPACE_SOQL.'Processed__c'])) {
		case 'TRUE':
			$generated_data['process_print_id'] = 'processflow_print_tick';
			$generated_data['process_bind_id'] = 'processflow_bind_tick';
			$generated_data['process_dispatch_id'] = 'processflow_dispatch_tick';
			break;
		default:
			$generated_data['process_dispatch_id'] = 'processflow_dispatch_grey';
			break;
	}

	if ($output_data[SFNAMESPACE_SOQL.'VendorID__c'] != '1031') {
		$poweredby_html = <<<END
			<div id="track_poweredby">
				<span>Powered by Pictureworks</span>
			</div>

END;
		$height_adjust_js = '';
	} else {
		$poweredby_html = '';
		$height_adjust_js = <<<ENDECHO
				tallest = 0;
				tallestid = 0;

				$( ".helpguide_question" ).each(
					function( intIndex ){
						if ($( this ).height() > tallest) {
							tallest = $( this ).height();
							tallestid = intIndex;
						}
					}
				);
				
				tallestid++;
				
				$('div.helpguide_question .question_content').hide();
				$("div.helpguide_question:nth-child("+tallestid+") .question_content").show();
			
				var parent_iframe = $('#portal_iframe', parent.document.body);
					if(jQuery.browser.msie == true && jQuery.browser.version.substr(0,1)<"7") {
						parent_iframe.height($(document.body).height() + 200);
					} else {
						parent_iframe.height($(document.body).height() + 20);
					}



ENDECHO;
	}
	
	$generated_data[SFNAMESPACE_SOQL.'Portal_Comments__c'] = str_replace("\n",'<br />',$output_data[SFNAMESPACE_SOQL.'Portal_Comments__c']);
	
	switch($output_data[SFNAMESPACE_SOQL.'VendorID__c']){
		case '1031':
			$vendorName = 'albumworks';
			break;
		case '3183':
			$vendorName = 'Officeworks Photobooks';
			break;
		case '3255':
			$vendorName = 'Target Photobooks';
			break;
		case '4084':
			$vendorName = 'My Reflections';
			break;
	}
			
    $rando = time();
	echo <<<ENDECHO
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

	<head>
		<link rel="stylesheet" type="text/css" media="screen" title="Default Style" href="include/css/index_1031.css?$rando" />
		<title>Track My Order - $vendorName</title>
		<script type="text/javascript" src="include/js/jquery-1.2.6.min.js"></script>
		<script type="text/javascript">

			$(document).ready(function(){
				$("a[rel='external']").attr('target','_blank');
			});
			
			togslide_faq = {
				init : function() {
					$('div.helpguide_question .question_content').hide();
					$('div.helpguide_question .question_title').click(function() {
						togslide_faq.toggle(this)
					});
					prev_thisitem = 0;
				},

				toggle : function(thisitem) {
					$('div.helpguide_question .question_content').hide('fast');
					$('div.helpguide_question .question_title').removeClass('question_title_active');
					
					if (thisitem != prev_thisitem) {
						$(thisitem).toggleClass('question_title_active');
						$(thisitem).siblings('.question_content').toggle('fast');
						prev_thisitem = thisitem;
					} else {
						prev_thisitem = 0;
					}
				}
			}
			
			$(function() {
$height_adjust_js
				togslide_faq.init();
			});

		</script>
	</head>

	<body>
	    <div id="trackpart">
		<div id="track_container">
			<div id="track_details">
				<div id="track_albumid">
					<span id="albumid_title">Order ID:</span>
					<span id="albumid_value">{$output_data['Name']} {$generated_data['invoice_link']}</span>
				</div>
				<div id="track_price">
					<span id="price_title">Price:</span>
					<span id="price_value">\${$output_data[SFNAMESPACE_SOQL.'Amount_incGST__c']}</span>
				</div>
				<div id="track_productdesc">
					<span id="productdesc_title">Product:</span>
					<span id="productdesc_value">{$output_data[SFNAMESPACE_SOQL.'Article_Description__c']}</span>
				</div>
				<div id="track_quantity">
					<span id="quantity_title">Quantity:</span>
					<span id="quantity_value">{$output_data[SFNAMESPACE_SOQL.'Order_Quantity__c']}</span>
				</div>
				<div id="track_covertype">
					<span id="covertype_title">{$generated_data['covertype_title']}</span>
					<span id="covertype_value">{$generated_data['covertype_value']}</span>
				</div>
				<div id="track_covertitle">
					<span id="covertitle_title">{$generated_data['covertitle_title']}</span>
					<span id="covertitle_value">{$generated_data['covertitle_value']}</span>
				</div>
			</div>$poweredby_html
			<div id="track_processflow">
				<div class="processflow_item" id="processflow_order">
					<span class="processflow_title">Ordering:</span>
					<span class="processflow_date processflow_date_order" id="processflow_order_tick">{$output_data[SFNAMESPACE_SOQL.'Upload_time__c']}</span>
				</div>
				<div  class="processflow_item" id="processflow_payment">
					<span class="processflow_title">Payment:</span>
					<span class="processflow_date processflow_date_payment" id="{$generated_data['process_payment_id']}">{$generated_data['process_payment_text']}</span>
				</div>
				<div class="processflow_item" id="processflow_process">
					<span class="processflow_title">Press Preparation:</span>
					<span class="processflow_date processflow_date_process" id="{$generated_data['process_process_id']}">{$output_data[SFNAMESPACE_SOQL.'production_PrintQueued__c']}</span>
				</div>
				<div class="processflow_item" id="processflow_print">
					<span class="processflow_title">Printing:</span>
					<span class="processflow_date processflow_date_print" id="{$generated_data['process_print_id']}">{$generated_data['process_print_date']}</span>
				</div>
				<div class="processflow_item" id="processflow_bind">
					<span class="processflow_title">Assembly:</span>
					<span class="processflow_date processflow_date_bind" id="{$generated_data['process_bind_id']}">{$output_data[SFNAMESPACE_SOQL.'production_Bound__c']}</span>
				</div>
				<div class="processflow_item" id="processflow_dispatch">
					<span class="processflow_title">Dispatch:</span>
					<span class="processflow_date processflow_date_dispatch" id="{$generated_data['process_dispatch_id']}">{$output_data[SFNAMESPACE_SOQL.'Date_Dispatched__c']}</span>
				</div>
			</div>
			<div id="track_comments">
				<span id="comments_title">Comments:</span>
				<span class="comments_value" id="{$generated_data['portal_icon_id']}">{$generated_data[SFNAMESPACE_SOQL.'Portal_Comments__c']}</span>
			</div>
			<div id="track_delivery">
				<div id="delivery_header">
					<span id="delivery_title">Delivery Information:</span>
					<span id="delivery_method">{$generated_data['deliver_method_note']}</span>
				</div>
ENDECHO;
	if (isset($output_data['delivery_data_array'])) {
		if (strtoupper($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']) == 'STORE') {
			switch ($output_data[SFNAMESPACE_SOQL.'VendorID__c']) {
				case '3183':
					$delv_name_pre = 'Officeworks ';
					break;
				case '3255':
					$delv_name_pre = 'Target ';
					break;
				default:
					$delv_name_pre = '';
					break;
			}
		} else {
			$delv_name_pre = '';
		}
		
		foreach ($output_data['delivery_data_array'] as $key => $value) {
		($output_data['delivery_data_array'][$key]['this_line_count'] == 1 ? $delvery_count_string_ext = '' : $delvery_count_string_ext = 's');
		($output_data[SFNAMESPACE_SOQL.'Order_Quantity__c'] == 1 ? $delvery_count_string = '' : $delvery_count_string = $output_data['delivery_data_array'][$key]['this_line_count'].' item'.$delvery_count_string_ext.' to:');
			echo <<<ENDECHO

				<div class="delivery_item">
					<div class="delv_details">
						<span class="delv_item_count">{$delvery_count_string}</span>
ENDECHO;

if(isset($output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Company_Name__c']) and $output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Company_Name__c'])
    echo '<span class="delv_name">'.$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Company_Name__c'].'</span>';
    
            echo <<<ENDECHO
						<span class="delv_name">$delv_name_pre{$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Name__c']}</span>
						<span class="delv_addyline1">{$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Address_1__c']}</span>
						<span class="delv_addyline2">{$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Address_2__c']}</span>
						<span class="delv_suburb">{$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Suburb_Town__c']}</span>
						<span class="delv_postcode">{$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Delivery_Post_Code__c']}</span>
					</div>
ENDECHO;
            $output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c'] = trim($output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c']);
			if (isset($output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c']) && ($output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c'] == 'Courier')) {
				echo "\n",'					<div class="delv_consnotes">',"\n";
				echo '						<span class="consnotes_desc">Consignment note(s):</span>',"\n";
				if (isset($output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'])) {
                    if(is_array($output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'])){
					    foreach ($output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'] as $key2 => $value2) {
						    echo '						<a rel="external" class="consnotes_link" href="?track_consignment=',$value2,'">',$value2,'</a>',"\n";
					    }
                    }
                    else echo '<a rel="external" class="consnotes_link" href="?track_consignment=',$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'],'">',$output_data['delivery_data_array'][$key][SFNAMESPACE_SOQL.'Consignment_Note__c'],'</a>',"\n";
				} else {
					echo '						<span><em>(Not yet available)</em></span>',"\n";
				}
				echo <<<ENDECHO
					</div>
ENDECHO;
			}
			echo "\n",'				</div>';
		}
	}
	echo <<<ENDECHO

			</div>
ENDECHO;
	if (isset($output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c'])) {
		echo <<<ENDECHO

			<div id="track_helpguide">
				<div id="helpguide_header">
					<span id="helpguide_title">Common Questions:</span>
				</div>
				<div id="helpguide_content">

ENDECHO;

	if(strtolower($output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c']) == 'other') {
		$output_questions_dispatch_method = 'courier';
	} else {
		$output_questions_dispatch_method = strtolower($output_data[SFNAMESPACE_SOQL.'Dispatch_Method__c']);
	}
	switch ($output_data[SFNAMESPACE_SOQL.'Portal_Stage__c']) {
		case 'No Payment':
			output_questions(array(1,2),$output_data[SFNAMESPACE_SOQL.'VendorID__c'],strtolower($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']),$output_questions_dispatch_method, null);
			break;
		case 'Refused Payment':
			output_questions(array(4,5),$output_data[SFNAMESPACE_SOQL.'VendorID__c'],strtolower($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']),$output_questions_dispatch_method, null);
			break;
		case 'Corrupt File':
			output_questions(array(7,8,2),$output_data[SFNAMESPACE_SOQL.'VendorID__c'],strtolower($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']),$output_questions_dispatch_method, null);
			break;
		case 'In Production':
			output_questions(array(2,22,9),$output_data[SFNAMESPACE_SOQL.'VendorID__c'],strtolower($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']),$output_questions_dispatch_method, null);
			break;
		case 'Dispatched':
			output_questions(array(10,11,12),$output_data[SFNAMESPACE_SOQL.'VendorID__c'],strtolower($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']),$output_questions_dispatch_method,null);
			break;
		case 'example':
			output_questions(array(51,52,53,54),$output_data[SFNAMESPACE_SOQL.'VendorID__c'],strtolower($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']),$output_questions_dispatch_method,null);
			break;
		default:
			output_questions(array(2,9),$output_data[SFNAMESPACE_SOQL.'VendorID__c'],strtolower($output_data[SFNAMESPACE_SOQL.'Delivery_Type__c']),$output_questions_dispatch_method,null);
			break;
	}
	
	echo <<<ENDECHO
				</div>
			</div>
ENDECHO;
	}
	echo <<<ENDECHO

		  </div> <!-- end track_container -->
          <div style="clear:both"> </div>
        </div> <!-- end trackpart -->
ENDECHO;

    $status = 'ordering';
    if($generated_data['process_payment_id']  == 'processflow_payment_tick')  $status = 'payment';
    if($generated_data['process_process_id']  == 'processflow_process_tick')  $status = 'press_prep';
    if($generated_data['process_print_id']    == 'processflow_print_tick')    $status = 'printing';
    if($generated_data['process_bind_id']     == 'processflow_bind_tick')     $status = 'assembly';
    if($generated_data['process_dispatch_id'] == 'processflow_dispatch_tick') $status = 'dispatched';
    
    ?>
        <div id="stagebit" class="status_<?=$status?>">
            Your order status
        </div>
        <div style="clear:both"> </div>
        <a id="facebook_promotion" href="http://www.facebook.com/albumworks" target="_blank" rel="nofollow">Like us on facebook</a>
        <div style="clear:both"> </div>
    <?

    echo <<<ENDECHO
		<div id="empty_spacer"></div>
		
		<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try{
		var pageTracker = _gat._getTracker("UA-1338422-1");
		pageTracker._trackPageview();
		} catch(err) {}
		</script>
	
	</body>
	
</html>
ENDECHO;

}

?>