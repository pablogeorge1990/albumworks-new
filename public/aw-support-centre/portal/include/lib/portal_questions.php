<?php

function output_questions($question_num_array,$vendor,$storehome,$delvmethod,$order_category) {

$portal_questions = array();

$portal_questions[1] = array('title' => 'How do I pay for my order?', 'content' => array (
	array(	'home' => 'After you uploaded your order to our servers you would have received an automated upload confirmation email. At the bottom of this automated email is a link to a secure payments page that you can use to pay for your order.',
			'store' => 'Seeing as you selected an in-store pickup, no payment is required at this stage as payment is made upon collection from your nomincated store.')
	));
	
$portal_questions[2] = array('title' => 'How long will my order take?', 'content' => array (
	'There are a number of factors that influence the amount of time to produce an order, so we can never say an order will take a certain period of time.<br />
	In over 90% of cases we dispatch our products within 4 working days from the time of ',
	array(	'home' => 'payment. ',
			'store' => 'upload. '),
	'Please keep in mind that our service level is to dispatch in ',
	array(	'1031' => '7 working days.',
			'3183' => '10 working days.',
			'3255' => '10 working days.'), 
	'so we highly recommend allowing this time for the production of your order.'
	));
	
$portal_questions[22] = array('title' => 'My order has been printed. What happens next?', 'content' => array (
	'Once a product has been printed it needs to cure for a minimum of 24 hours to ensure the inks have hardened and settled into the paper correctly.<br>We then bind your album, using state of the art PUR glue binding methods. This requires a 48 hour curing time to ensure that the bind is as strong as possible. If you are ordering an A3 book, we hand-sew these.<br><br>In most cases your order should be dispatched within four working days of payment being received, however there are situations where it may take slightly longer. This may be caused by your order being printed in the evening or not passing our Quality Control checkpoint.',
	));
	
$portal_questions[3] = array('title' => 'Can I alter or cancel my order?', 'content' => array (
	'We can\'t actually alter an order as the files we received aren\'t editable.<br />&nbsp;<br />
	To make any alterations we\'d need to cancel your order and have you re-upload with your alterations made.<br />&nbsp;<br />
	Unfortunately we\'re unable to cancel your order at this stage as it has already been processed by our automated system and to cancel it would require cancelling other customers orders.'
	));

$portal_questions[13] = array('title' => 'Can I alter or cancel my order?', 'content' => array (
	'We can\'t actually alter an order as the files we received aren\'t editable.<br />&nbsp;<br />
	To make any alterations we\'d need to cancel your order and have you re-upload with your alterations made and seeing as no payment has been made for this order, it can effectively be cancelled by not submitting payment.<br />&nbsp;<br />
	Please keep in mind that once payment has been received, ',
	array(	'3183' => 'or if your order is an in-store pickup (in which case no payment is required until collection),',
			'3255' => 'or if your order is an in-store pickup (in which case no payment is required until collection),'),
	'we wouldn\'t be able to cancel it as the print file would already have been submitted to our automated production system.'
	));
	
$portal_questions[4] = array('title' => 'Why was my payment declined?', 'content' => array (
	'There are a number of reasons a payment can be declined. With the new Verified by Visa and MasterCard SecureCode security processes, there can often be issues with this aspect of your payment. We suggest contacting your financial institution as they will usually be able to tell you why your payment was declined.<br />&nbsp;<br />
	All payments are processed through our merchant provider, which means we never receive your credit card details and therefore are unable to determine the cause of a declined payment.'
	));
	
$portal_questions[5] = array('title' => 'How do I make payment for my order?', 'content' => array (
	'After you uploaded your order to our servers you would have received an automated upload confirmation email. At the bottom of this automated email is a link to a secure payments page that you can use to pay for your order. This link may not work if the number of attempts to make payment is too high. If this is the case, please step through the ordering process again and enter your credit card details when prompted.'
	));
	
$portal_questions[6] = array('title' => 'Can I make payment over the phone?', 'content' => array (
	'Unfortunately we don\'t currently have the facilities in place to take payment over the phone.'
	));

$portal_questions[7] = array('title' => 'Why is my order corrupt?', 'content' => array (
	'Usually an order is corrupt due to the connection between your computer and our servers being dropped. This is commonly caused by an unstable ADSL connection or a wireless connection. Because this is usually an intermittent communication error, a subsequent upload attempt will generally be successful.'
	));
	
$portal_questions[8] = array('title' => 'What do I need to do to have my order produced?', 'content' => array (
	'To have your order produced, please re-upload it to our servers. ',
	array(	'home' => 'We don\'t want to charge you twice, so please exit from the ordering process when prompted for payment and then email us and we\'ll manually forward it to production.',
			'store' => 'Seeing as you\'ve selected an in-store pickup, no payment has been made so please rest assured you\'ll only be charged once for your order.'),
			'<br />&nbsp;<br />A',
	array(	'1031' => 'n Albumprinter',
			'3183' => 'n Officeworks Photobooks',
			'3255' => ' Target Photobooks'),
			' staff member will also be in contact via email to help resolve this corrupt file issue with you.'
	));

$portal_questions[9] = array('title' => 'Can I request quicker production of my order?', 'content' => array (
	'We already produce our products as quickly as we can, so unfortunately there isn\'t anything extra we can do to produce your order any quicker. We do everything we can to ensure the swift production of all our products and in over 90% of cases we dispatch within 4 working days.'
	));
	
$portal_questions[10] = array('title' => array('store' => 'When will my order be ready for collection?', 'home' => 'When will my order be delivered?'), 'content' => array (
	'Your order is being delivered via ',
	array(	'courier' => 'DHL ',
			'aust post' => 'DHL ',
			'other' => 'DHL '),
	'and should arrive at your nominated',
	array(	'home' => ' address ',
			'store' => ' store '),
	array(	'courier' => 'the next working day* after it\'s dispatched.',
			'aust post' => 'within 2-5 working days.',
			'other' => 'within 1-3 working days'),
	array(	'store' => 
		array( 	'3183' => '<br />&nbsp;<br />An Officeworks ',
				'3255' => '<br />&nbsp;<br />A Target ')),
	array(	'store' => 	'staff member will contact you via your provided phone number once it\'s ready for collection.'),
	array(	'courier' =>	'<br />&nbsp;<br /><em>* Next working day deliveries are for locations based in the \'Next Business Day Express Post Networks\' zone as defined by Australia Post. For a listing of Postcodes in this zone, please use <a href="http://www.auspost.com.au/BCP/0,1467,CH2432%257EMO19,00.html" rel="external">this link</a> to the AusPost website.</em>')

	));
	
$portal_questions[11] = array('title' => array('store' => 'How do I know when my order arrives at store and can I track it?', 'home' => 'How do I track my order?'), 'content' => array (
	array(	'store' => 'When your order arrives at your nominated store, a staff member will contact you via phone to collect and pay for it.<br />&nbsp;<br />'),
	array(	'courier' => 'You can track the progress of your delivery by clicking on the Consignment Note in the left hand Delivery Information section.<br />&nbsp;<br /><em>NOTE: the consignment note won\'t appear until the day after your order is dispatched.</em>',
			'aust post' =>'Seeing as your order is being delivery by Austraila Post, unfortunately we don\'t have the ability to track your order as Austraila Post don\'t provide us with this information.',
			'other' => 'We have organised a custom delivery of your order. To get an update on the delivery of your order, please contact us.  We\'ll attempt to keep you as up to date as possible via the comments section of this page.') 
	));
	
$portal_questions[12] = array('title' => 'How do I order more copies of my order?', 'content' => array (
	'To order more copies of your order, simply open up the editor and step through the ordering process. Unfortunately we\'re unable to create a new order from an existing order.'
	));
	
$portal_questions[51] = array('title' => 'What are these \'Common Quesions\'?', 'content' => array (
	'These \'Common Questions\' are questions customer generally ask at a given point in the production process.<br />&nbsp;<br />These questions vary based on the stage of your order, only displaying the most relevant questions for your convenience.'
	));

$portal_questions[52] = array('title' => 'What information is displayed on this page?', 'content' => array (
	'As can be seen, we provide you with information on all the major stages of the production cycle of your album.<br />&nbsp;<br />We also use the comments section to keep you up-to-date with the progress of your order.'
	));

$portal_questions[53] = array('title' => array(	'1031' => 'How long does it take to produce and dispatch Albumprinter products?', '3183' => 'How long does it take to produce and dispatch Officeworks Photobooks products?', '3255' => 'How long does it take to produce and dispatch Target Photobooks products?'), 'content' => array (
	'In over 90% of cases we dispatch our products within 4 working days.<br />&nbsp;<br />Please keep in mind we state a ',
	array(	'1031' => '7',
			'3183' => '10',
			'3255' => '7',), ' working day dispatch period and while we do everything we can to create and dispatch as quickly as we can, we\'re unable to guarantee this as there are those rare times that variables out of our control may delay the production of your order.'
	));

$portal_questions[54] = array('title' => 'Can I Track &amp; Trace delivery of my order?', 'content' => array (
	'In the Delivery Information section of this \'Track My order\' page (to the left), a consignment note is provided after your order has been dispatched. You can use this to Track &amp; Trace your order with DHL.'
	));



	if (is_array($question_num_array)) {
			foreach ($question_num_array as $key => $value) {
				echo '					<div class="helpguide_question">',"\n";
				$question = $portal_questions[$value];
				echo '						<a href="javascript:void(0);" class="question_title">';
				if (is_string($question['title'])) {
					echo $question['title'];
				} elseif (is_array($question['title'])) {
					if (isset($question['title'][$vendor])) {
						echo $question['title'][$vendor];
					} elseif (isset($question['title'][$storehome])) {
						echo $question['title'][$storehome];
					}
				}
				echo '</a>',"\n";
				echo '						<span class="question_content">';
				foreach ($question['content'] as $key2 => $value2) {
					if (is_string($value2)) {
						echo $value2;
					} elseif (is_array($value2)) {
						if (isset($value2[$vendor])) {
							if (is_string($value2[$vendor])) {
								echo $value2[$vendor];
							} elseif (is_array($value2[$vendor])) {
								if (isset($value2[$vendor][$storehome])) {
									echo $value2[$vendor][$storehome];
								} elseif (isset($value2[$vendor][$delvmethod])) {
									echo $value2[$vendor][$delvmethod];
								}
							}
						} elseif (isset($value2[$storehome])) {
							if (is_string($value2[$storehome])) {
								echo $value2[$storehome];
							} elseif (is_array($value2[$storehome])) {
								if (isset($value2[$storehome][$vendor])) {
									echo $value2[$storehome][$vendor];
								} elseif (isset($value2[$storehome][$delvmethod])) {
									echo $value2[$storehome][$delvmethod];
								}
							}
						} elseif (isset($value2[$delvmethod])) {
							if (is_string($value2[$delvmethod])) {
								echo $value2[$delvmethod];
							} elseif (is_array($value2[$delvmethod])) {
								if (isset($value2[$delvmethod][$vendor])) {
									echo $value2[$delvmethod][$vendor];
								} elseif (isset($value2[$delvmethod][$storehome])) {
									echo $value2[$delvmethod][$storehome];
								}
							}
						}
					}
				}
				echo '</span>',"\n";
				echo '					</div>',"\n";
			}
	}
}

?>