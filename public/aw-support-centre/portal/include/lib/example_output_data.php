<?php

function example_portal_data($vendor) {

	if (date('w') == 6) { // sat
		$order_date 		= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
		$pay_date 			= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
		$processing_date 	= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
		$printing_date		= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
	} elseif (date('w') == 0) { //sun
		$order_date 		= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-3, date("Y")));
		$pay_date 			= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-3, date("Y")));
		$processing_date 	= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-3, date("Y")));
		$printing_date		= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-2, date("Y")));
	} else { // mon-fri
		$order_date 		= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
		$pay_date 			= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
		$processing_date 	= date("D j M",mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
		$printing_date		= date("D j M h:iA",mktime(date("H")-3, date("i")-37, 0, date("m")  , date("d"), date("Y")));
	}

	$example_portal_data = array(
	    SFNAMESPACE_SOQL.'VendorID__c'				=> $vendor,
		SFNAMESPACE_SOQL.'Portal_Stage__c'			=> 'example',
	    'Name'						=> '103100000',
		SFNAMESPACE_SOQL.'Customer_Email__c'			=> 'example@pictureworks.com.au',
		SFNAMESPACE_SOQL.'Article_Description__c'	=> 'A4 Landscape Hardcover',
		SFNAMESPACE_SOQL.'Order_Quantity__c'			=> '1',
		SFNAMESPACE_SOQL.'Amount_incGST__c'			=> '69.95',
		SFNAMESPACE_SOQL.'Cover_Title_Text__c'		=> 'Happy Birthday',
		SFNAMESPACE_SOQL.'Can_Have_Cover__c'			=> '1',
		SFNAMESPACE_SOQL.'Cover_Summary__c'			=> 'Faux leather: Black',
		SFNAMESPACE_SOQL.'Delivery_Type__c'			=> 'Home',
		SFNAMESPACE_SOQL.'Dispatch_Method__c'		=> 'Courier',
		SFNAMESPACE_SOQL.'Cancelled__c'				=> 'false',
		SFNAMESPACE_SOQL.'Order_Type__c'				=> 'INITIAL',
		SFNAMESPACE_SOQL.'Upload_time__c'			=> $order_date,
		SFNAMESPACE_SOQL.'Pay_Status__c'				=> 'CAPTURED',
		SFNAMESPACE_SOQL.'Pay_Date__c'				=> $pay_date,
		SFNAMESPACE_SOQL.'Portal_Comments__c'		=> 'We use this comments section to keep you up to date with your order.<br />In this instance, the album has been sucessfully uploaded, paid for and printed.  We\'d expect it to be assembled and dispatched on the next working day.',
		SFNAMESPACE_SOQL.'Portal_Comments_icon__c'	=> 'Informative',
		SFNAMESPACE_SOQL.'Processed__c'				=> 'false',
		SFNAMESPACE_SOQL.'Process_Status__c'			=> 'OK',
		SFNAMESPACE_SOQL.'Date_Dispatched__c'		=> '',
	    'delivery_data_array'		=> Array
	        (
	            '0' => Array
	                (
	                    'this_line_count' 			=> '1',
						SFNAMESPACE_SOQL.'Delivery_Name__c' 			=> 'John Citizen',
						SFNAMESPACE_SOQL.'Delivery_Address_1__c'		=> '1 Smith St',
						SFNAMESPACE_SOQL.'Delivery_Address_2__c'		=> '',
						SFNAMESPACE_SOQL.'Delivery_Suburb_Town__c'	=> 'Melbourne',
						SFNAMESPACE_SOQL.'Delivery_Post_Code__c'		=> '3000',
	                )

	        ),

		SFNAMESPACE_SOQL.'production_File_Ripped__c'		=> 'true',
		SFNAMESPACE_SOQL.'production_PrintQueued__c'		=> $processing_date,
		SFNAMESPACE_SOQL.'production_File_Printed__c'	=> 'true',
		SFNAMESPACE_SOQL.'production_Printed__c'			=> $printing_date
	);
	
	return $example_portal_data;
	
}

?>
