<?php

define('SFNAMESPACE_REST', ''); //apexrest namespace for packaged sf
define('SFNAMESPACE_SOQL', ''); //soql namespace for packaged sf

error_reporting(0);
if(isset($_GET['css'])) {
	css($_GET['css']);
} elseif(isset($_GET['aplogo']))  {
	aplogo();
} elseif(isset($_GET['albumid']) && isset($_GET['email'])) {
	$_GET['email'] = str_replace(' ', '+', $_GET['email']);
    if(strpos($_GET['albumid'], 'i') !== FALSE){
        $parts = explode('i', $_GET['albumid']);
        $_GET['albumid'] = $parts[0];
    }
    invoice(preg_replace('/[^\d\s]/','',$_GET['albumid']),addslashes($_GET['email']));
} elseif(isset($_GET['litid']) && isset($_GET['email'])) {
	$_GET['email'] = str_replace(' ', '+', $_GET['email']);
    if(strpos($_GET['litid'], 'i') !== FALSE){
        $parts = explode('i', $_GET['litid']);
        $_GET['litid'] = $parts[0];
    }
	$_GET['albumid'] = $_GET['litid'];
    invoice($_GET['albumid'],addslashes($_GET['email']));
} else {
	header("HTTP/1.0 403 Forbidden");
	invoice_error('ALBUM_ERROR3');
}


function invoice_error($error_type) {
	// $error_type is one of the following
	//		SYSTEM_ERROR 	- 	Communication with Salesforce API failed, or response was invalid
	//		ALBUM_ERROR 	- 	AlbumID and email combination was invalid, or no matching order was found with an Order Type of INITIAL, or no LineIDs found for aggregation
	//		SHOP_ORDER		-	AlbumID is an Officeworks or Target store-pickup order
	//		PAYSTATUS_ERROR	-	The Pay Status for AlbumID queried is such that an accurate invoice could not be generated - e.g. order was refunded etc

	echo <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

	<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		<link rel="stylesheet" type="text/css" media="screen,print" title="Default Style" href="?css=invoice" />
		<link rel="stylesheet" type="text/css" media="print" title="Print Style" href="?css=print" />
		<!--[if lte IE 6]>
		<link rel="stylesheet" type="text/css" media="screen,print" title="IE 6 Style" href="?css=lteie6" />
		<![endif]-->
		<title>Invoice Error</title>
	</head>

	<body>

		<!-- {$error_type} -->

		<div id="invoice">

			<span id="invoicer-address">
				<span class="invoicer-address name">Pictureworks Group Pty Ltd trading as <em>albumworks</em></span>
				<span class="invoicer-address street">Level 2, 696 Bourke St</span>
				<span class="invoicer-address suburb">Melbourne</span>
				<span class="invoicer-address state">VIC</span>
				<span class="invoicer-address postcode">3000</span> 
				<span class="invoicer-address abn">ABN 28 119 011 933</span>
			</span>

			<img src="?aplogo" width="180" height="53" id="logo" alt="albumworks" />

			<h2 id="invoice-title">INVOICE ERROR</h2>

			<div id="invoice-details">
				<span class="invoice-error row heading">
					<span class="invoice-error description">Unfortunately an invoice could not be generated. This may be for several reasons:</span>
				</span>
				<span class="invoice-error row">
					<span class="invoice-error description">&raquo; The AlbumID and/or email details provided do not match anything in our database</span>
				</span>
				<span class="invoice-error row">
					<span class="invoice-error description">&raquo; You have just placed your order, and our invoicing system has not yet been updated with these details</span>
				</span>
				<span class="invoice-error row">
					<span class="invoice-error description">&raquo; We're unfortunately experiencing an error with some of our infrrastructure - you might want to try again in a short time, or contact our customer service department for assistance</span>
				</span>
			</div>

			<span id="vendor-note"><strong>Please note</strong>: Pictureworks Group Pty Ltd supply production and payment infrastructure for the Photobooks range of products.</span>

		</div><!-- /invoice -->

	</body>

</html>
EOF;

	die;
}

function invoice($albumid,$email) {
	try{
		$query = "SELECT Name, ".SFNAMESPACE_SOQL."Vendor_Name_Full__c, ".SFNAMESPACE_SOQL."Upload_Time__c, ".SFNAMESPACE_SOQL."Payment_Code__c, ".SFNAMESPACE_SOQL."Customer_First_Name__c, ".SFNAMESPACE_SOQL."Customer_Last_Name__c, ".SFNAMESPACE_SOQL."Customer_Address__c, ".SFNAMESPACE_SOQL."Customer_Suburb_Town__c, ".SFNAMESPACE_SOQL."Customer_Post_Code__c, ".SFNAMESPACE_SOQL."Order_Quantity__c, ".SFNAMESPACE_SOQL."pap_code__c, ".SFNAMESPACE_SOQL."Article_Description__c, ".SFNAMESPACE_SOQL."Base_Price_incGST__c, ".SFNAMESPACE_SOQL."Extra_Pages_Qty__c, ".SFNAMESPACE_SOQL."Extra_Pages_Price_incGST__c, ".SFNAMESPACE_SOQL."Photo_Cover_Price_incGST__c, ".SFNAMESPACE_SOQL."Cover_Title_Price_incGST__c, ".SFNAMESPACE_SOQL."Full_Price_incGST__c, ".SFNAMESPACE_SOQL."Discount_incGST__c, ".SFNAMESPACE_SOQL."Amount_incGST__c, ".SFNAMESPACE_SOQL."Pay_Status__c, ".SFNAMESPACE_SOQL."Postage_Price_incGST__c, ".SFNAMESPACE_SOQL."Has_Slip_Case__c, ".SFNAMESPACE_SOQL."Has_Hot_Stamping__c, ".SFNAMESPACE_SOQL."Has_Presentation_Box__c, ".SFNAMESPACE_SOQL."Slip_Case_Price_incGST__c, ".SFNAMESPACE_SOQL."Hot_Stamping_Price_incGST__c, ".SFNAMESPACE_SOQL."Presentation_Box_Price_incGST__c, ".SFNAMESPACE_SOQL."Quality_Code__c, (SELECT ".SFNAMESPACE_SOQL."Quality_Price_incGST__c FROM ".SFNAMESPACE_SOQL."LineIds__r), ".SFNAMESPACE_SOQL."Cover_Laminate_Price_incGST__c, ".SFNAMESPACE_SOQL."Cover_Laminate_Code__c, ".SFNAMESPACE_SOQL."End_Paper_Colour_Price_incGST__c, ".SFNAMESPACE_SOQL."End_Paper_Colour_Code__c, ".SFNAMESPACE_SOQL."Tracing_Paper_Price_incGST__c, ".SFNAMESPACE_SOQL."Tracing_Paper_Code__c, ".SFNAMESPACE_SOQL."DustJacket_Price_incGST__c, ".SFNAMESPACE_SOQL."Has_Dust_Jacket__c FROM ".SFNAMESPACE_SOQL."Orderc__c WHERE  Name like '{$albumid}%' AND ".SFNAMESPACE_SOQL."Customer_Email__c = '{$email}' AND ".SFNAMESPACE_SOQL."Order_Type__c = 'INITIAL'";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://pwg:MCgC_riGqERn2oERhEK@api.photo-products.com.au/zenpanel/api/sf/query');
		curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$query, true]));
		$response = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		if(!in_array($status, [0, 200]))
			throw new Exception($response);

		$response = json_decode($response, true);

		if($response['size'] == 0) {
			invoice_error('ALBUM_ERROR1');
		} elseif($response['size'] == 1) {
			invoice_display(invoice_process($response['records'][0]));
		} elseif($response['size'] > 1) {
			foreach($response['records'] as $record){
				invoice_display(invoice_process($record));
				echo '<div style="clear:both; margin-top:50px;"> </div>';
			}
		} else {
			invoice_error('SYSTEM_ERROR');
		}
	}
	catch(Exception $e){
		invoice_error('SYSTEM_ERROR');
	}
}

/**
 * Sums a field over a lineId and returns a single number.
 * Parameters:	invoice_data_raw (same parameter that gets passed to invoice_process
 * 		field (the SalesForce API field name of the field in question)
 * Returns:	The sum of that field, if present, otherwise returns 0.
 * This function is necessary because some fields (e.g. Quality_Price_IncGST__c) are only available at Line level and not at Order level and SalesForce SOQL doesn't support aggregating in parent-child subqueries, and we can't easily add those fields to Order Level because we've hit the limit for rollup fields. 
 */
function lineid_rollup_aggregate($invoice_data_raw, $field){
	$price = 0;
	if($invoice_data_raw[SFNAMESPACE_SOQL.'LineIDs__r']['totalSize'] > 1){
		foreach($invoice_data_raw[SFNAMESPACE_SOQL.'LineIDs__r']['records'] as $line){
			$price += $line->values[$field];
		}
	}elseif($invoice_data_raw[SFNAMESPACE_SOQL.'LineIDs__r']['totalSize'] == 1){
		$price = $invoice_data_raw[SFNAMESPACE_SOQL.'LineIDs__r']['records']->values[$field];
	}elseif($invoice_data_raw[SFNAMESPACE_SOQL.'LineIDs__r']['totalSize'] == 0){
		invoice_error('ALBUM_ERROR2');
	}else{
		invoice_error('SYSTEM_ERROR');
	}
	return $price;
}

function invoice_process($invoice_data_raw) {
	$qualityPrice = lineid_rollup_aggregate($invoice_data_raw, SFNAMESPACE_SOQL.'Quality_Price_incGST__c');

	$invoice_data = array();

	$invoice_data['vendor'] = $invoice_data_raw[SFNAMESPACE_SOQL.'Vendor_Name_Full__c'];
	
	$invoice_data['albumid'] = $invoice_data_raw['Name'];
	$invoice_data['invoiceid'] = strtoupper($invoice_data_raw[SFNAMESPACE_SOQL.'Payment_Code__c']);

	list($date,$time) = explode('T',str_replace('.000Z','',$invoice_data_raw[SFNAMESPACE_SOQL.'Upload_Time__c']));
	list($year,$month,$day) = explode('-',$date);
	list($hour,$minute,$second) = explode(':',$time);
	$invoice_data['date'] = date("d M Y",mktime($hour,$minute,$second,$month,$day,$year));

	$invoice_data['custname'] = ucwords($invoice_data_raw[SFNAMESPACE_SOQL.'Customer_First_Name__c'].' '.$invoice_data_raw[SFNAMESPACE_SOQL.'Customer_Last_Name__c']);
	$invoice_data['custaddress'] = $invoice_data_raw[SFNAMESPACE_SOQL.'Customer_Address__c'];
	$invoice_data['custsuburb'] = $invoice_data_raw[SFNAMESPACE_SOQL.'Customer_Suburb_Town__c'];
	$invoice_data['custpostcode'] = $invoice_data_raw[SFNAMESPACE_SOQL.'Customer_Post_Code__c'];

	$invoice_data['orderdetails']['product']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
	$invoice_data['orderdetails']['product']['code'] = strtoupper($invoice_data_raw[SFNAMESPACE_SOQL.'pap_code__c']);
	$invoice_data['orderdetails']['product']['desc'] = $invoice_data_raw[SFNAMESPACE_SOQL.'Article_Description__c'];
	$invoice_data['orderdetails']['product']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Base_Price_incGST__c'],2);
	
	if($invoice_data_raw[SFNAMESPACE_SOQL.'Quality_Code__c'] == 'HD' || (int)$qualityPrice > 0) {
		$invoice_data['orderdetails']['quality']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
		switch($invoice_data['vendor']) {
			case 'Canon':
				$invoice_data['orderdetails']['quality']['code'] = strtoupper($invoice_data_raw[SFNAMESPACE_SOQL.'Quality_Code__c']);
				$invoice_data['orderdetails']['quality']['desc'] = '&nbsp;';
				break;
			case 'kikki.K':
				$invoice_data['orderdetails']['quality']['code'] = '&nbsp;';
				$invoice_data['orderdetails']['quality']['desc'] = 'Premium Matte Deluxe paper finish';
				break;
			case 'Officeworks Photobooks':
			case 'Target Photobooks':
			case 'albumworks':
			case 'Albumworks':
			case 'albumprinter':
				$invoice_data['orderdetails']['quality']['code'] = '&nbsp;';
				$invoice_data['orderdetails']['quality']['desc'] = 'High Definition printing';
				break;
			default:
				$invoice_data['orderdetails']['quality']['code'] = '&nbsp;';
				$invoice_data['orderdetails']['quality']['desc'] = '&nbsp;';
				break;
		}

		$invoice_data['orderdetails']['quality']['price'] = $qualityPrice;
	}

	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Extra_Pages_Qty__c'] > 0 && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Extra_Pages_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['extrapages']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Extra_Pages_Qty__c'];
		$invoice_data['orderdetails']['extrapages']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['extrapages']['desc'] = 'Extra pages';
		$invoice_data['orderdetails']['extrapages']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Extra_Pages_Price_incGST__c'],2);
	}

	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Photo_Cover_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['photocover']['qty'] = 1;
		$invoice_data['orderdetails']['photocover']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['photocover']['desc'] = 'Cover';
		$invoice_data['orderdetails']['photocover']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Photo_Cover_Price_incGST__c'],2);
	}

	/*
	// "We simply overwrite the vars if these values exist, who needs logic?" - some guy
	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Photocover_Qty__c'] > 0 && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Photo_Cover_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['photocover']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Photocover_Qty__c'];
		$invoice_data['orderdetails']['photocover']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['photocover']['desc'] = 'Photocover';
		$invoice_data['orderdetails']['photocover']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Photo_Cover_Price_incGST__c'],2);
	}
	
	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Premium_Cover_Qty__c'] > 0 && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Photo_Cover_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['photocover']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Premium_Cover_Qty__c'];
		$invoice_data['orderdetails']['photocover']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['photocover']['desc'] = 'Premium Cover';
		$invoice_data['orderdetails']['photocover']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Photo_Cover_Price_incGST__c'],2);
	}

	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Cover_Title_Qty__c'] > 0 && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Cover_Title_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['covertitle']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Cover_Title_Qty__c'];
		$invoice_data['orderdetails']['covertitle']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['covertitle']['desc'] = 'Cover title';
		$invoice_data['orderdetails']['covertitle']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Cover_Title_Price_incGST__c'],2);
	}
	*/

	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Cover_Laminate_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['coverlaminate']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
		$invoice_data['orderdetails']['coverlaminate']['code'] = strtoupper($invoice_data_raw[SFNAMESPACE_SOQL.'Cover_Laminate_Code__c']);
		$invoice_data['orderdetails']['coverlaminate']['desc'] = 'Cover Laminate';
		$invoice_data['orderdetails']['coverlaminate']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Cover_Laminate_Price_incGST__c'],2);
	}

	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'DustJacket_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['dustjacket']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
		$invoice_data['orderdetails']['dustjacket']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['dustjacket']['desc'] = 'Dust Jacket';
		$invoice_data['orderdetails']['dustjacket']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'DustJacket_Price_incGST__c'],2);
	}
	
	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'End_Paper_Colour_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['endpapercolour']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
		$invoice_data['orderdetails']['endpapercolour']['code'] = strtoupper($invoice_data_raw[SFNAMESPACE_SOQL.'End_Paper_Colour_Code__c']);
		$invoice_data['orderdetails']['endpapercolour']['desc'] = 'End Paper Colour';
		$invoice_data['orderdetails']['endpapercolour']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'End_Paper_Colour_Price_incGST__c'],2);
	}
	
	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Tracing_Paper_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['tracingpaper']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
		if(strtoupper($invoice_data_raw[SFNAMESPACE_SOQL.'Tracing_Paper_Code__c']) == "VELLUM"){
			$invoice_data['orderdetails']['tracingpaper']['code'] = '&nbsp;';
			$invoice_data['orderdetails']['tracingpaper']['desc'] = 'Translucent Vellum Overlay';
		}else{
			$invoice_data['orderdetails']['tracingpaper']['code'] = strtoupper($invoice_data_raw[SFNAMESPACE_SOQL.'Tracing_Paper_Code__c']);
			$invoice_data['orderdetails']['tracingpaper']['desc'] = 'First Page Insert';
		}
		$invoice_data['orderdetails']['tracingpaper']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Tracing_Paper_Price_incGST__c'],2);
	}
	
    if($invoice_data_raw[SFNAMESPACE_SOQL.'Has_Presentation_Box__c'] && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Presentation_Box_Price_incGST__c'] > 0) {
        $invoice_data['orderdetails']['presentationbox']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
        $invoice_data['orderdetails']['presentationbox']['code'] = '&nbsp;';
        // $invoice_data['orderdetails']['presentationbox']['desc'] = 'Presentation Box';
        // $invoice_data['orderdetails']['presentationbox']['code'] = strtoupper($invoice_data_raw['Presentation_Box_Code__c']);
        $invoice_data['orderdetails']['presentationbox']['desc'] = 'Presentation Box';
        $invoice_data['orderdetails']['presentationbox']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Presentation_Box_Price_incGST__c'],2);
    }    

    if($invoice_data_raw[SFNAMESPACE_SOQL.'Has_Slip_Case__c'] && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Slip_Case_Price_incGST__c'] > 0) {
        $invoice_data['orderdetails']['slipcase']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
        $invoice_data['orderdetails']['slipcase']['code'] = '&nbsp;';
        $invoice_data['orderdetails']['slipcase']['desc'] = 'Slip Case';
        $invoice_data['orderdetails']['slipcase']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Slip_Case_Price_incGST__c'],2);
    }    

	if($invoice_data_raw[SFNAMESPACE_SOQL.'Has_Hot_Stamping__c'] && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Hot_Stamping_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['hotstamping']['qty'] = (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Order_Quantity__c'];
		$invoice_data['orderdetails']['hotstamping']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['hotstamping']['desc'] = 'Hot Stamping';
		$invoice_data['orderdetails']['hotstamping']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Hot_Stamping_Price_incGST__c'],2);
	}	

	if((int)$invoice_data_raw[SFNAMESPACE_SOQL.'Postage_Price_incGST__c'] > 0 && (int)$invoice_data_raw[SFNAMESPACE_SOQL.'Postage_Price_incGST__c'] > 0) {
		$invoice_data['orderdetails']['postage']['qty'] = '1';
		$invoice_data['orderdetails']['postage']['code'] = '&nbsp;';
		$invoice_data['orderdetails']['postage']['desc'] = 'Postage';
		$invoice_data['orderdetails']['postage']['price'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Postage_Price_incGST__c'],2);
	}	

	$invoice_data['subtotal'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Full_Price_incGST__c'],2);
	$invoice_data['discount'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Discount_incGST__c'],2);
	$invoice_data['total'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Amount_incGST__c'],2);
	$invoice_data['incgst'] = number_format($invoice_data_raw[SFNAMESPACE_SOQL.'Amount_incGST__c']/11,2);
	
	switch($invoice_data_raw[SFNAMESPACE_SOQL.'Pay_Status__c']) {
		case 'ORDERED':
			invoice_error('SHOP_ORDER');
			break;
		case 'MANUAL':
		case 'CAPTURED':
		case 'AUTHORISED':
		case 'VIA_PREVIOUS_ORDER':
			$invoice_data['applied'] = $invoice_data['total'];
			$invoice_data['due'] = number_format(0.00,2);
			break;
		case '':
		case 'REFUSED':
		case 'INVOICE':
			$invoice_data['applied'] = number_format(0.00,2);
			$invoice_data['due'] = $invoice_data['total'];
			break;
		default:
			invoice_error('PAYSTATUS_ERROR');
			break;
	}
	
	switch($invoice_data['vendor']) {
		case 'Officeworks Photobooks':
		case 'Target Photobooks':
			$invoice_data['bootnote'] = 'Pictureworks Group Pty Ltd supply production and payment infrastructure for the '.$invoice_data['vendor'].' range of products. All prices are in AUD.';
			break;
		default:
			$invoice_data['bootnote'] = 'All prices are in AUD';
			break;
	}

	return $invoice_data;
	
}

function invoice_display($invoice_data) {
$img = '<img src="?aplogo" width="180" height="53" id="logo" alt="albumworks" />';
//If vendor is not albumworks then dont display logo
$vend = strtolower($invoice_data['vendor']);
if(!($vend == 'albumworks' || $vend == 'Albumworks' || $vend == 'albumprinter')) $img ='';
	$css = '<link rel="stylesheet" type="text/css" media="screen,print" title="Default Style" href="?css=invoice" />
		<link rel="stylesheet" type="text/css" media="print" title="Print Style" href="?css=print" />';
	$connote = "";
	if(isset($_REQUEST['toll'])){
		$css = '<link rel="stylesheet" type="text/css" media="screen,print" title="Default Style" href="?css=toll" />';
		$img = '';
		$connote = '<span class="invoice-info connote">Connote:'.$_REQUEST['connote'].'</span>';
		}
	echo <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

	<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		{$css}
		<!--[if lte IE 6]>
		<link rel="stylesheet" type="text/css" media="screen,print" title="IE 6 Style" href="?css=lteie6" />
		<![endif]-->
		<title>{$invoice_data['vendor']} - Tax Invoice {$invoice_data['albumid']}/{$invoice_data['invoiceid']}</title>
	</head>

	<body>
		
		<div id="invoice">

			<span id="invoicer-address">
				<span class="invoicer-address name">Pictureworks Group Pty Ltd trading as <em>albumworks</em></span>
                <span class="invoicer-address street">Level 2, 696 Bourke St</span>                
				<span class="invoicer-address suburb">Melbourne</span>
				<span class="invoicer-address state">VIC</span>
				<span class="invoicer-address postcode">3000</span> 
				<br><span class="invoicer-address country">Australia</span> 
				<span class="invoicer-address abn">ABN 28 119 011 933</span>
				
			</span>

			{$img}

			<span id="invoicee-address">
				<span class="invoicee-address name">{$invoice_data['custname']}</span>
				<span class="invoicee-address street">{$invoice_data['custaddress']}</span>
				<span class="invoicee-address suburb">{$invoice_data['custsuburb']}</span>
				<span class="invoicee-address postcode">{$invoice_data['custpostcode']}</span>
			</span>

			<span id="invoice-info">
				<span class="invoice-info date">
					<span class="invoice-info item">Date:</span>
					<span class="invoice-info value">{$invoice_data['date']}</span>
				</span>
				<span class="invoice-info invoiceid">
					<span class="invoice-info item">Invoice #:</span>
					<span class="invoice-info value">{$invoice_data['invoiceid']}</span>
					{$connote}
				</span>
			</span>

			<h2 id="invoice-title">TAX INVOICE - ALBUMID {$invoice_data['albumid']} <a href="javascript:window.print();" id="print-link">[PRINT]</a></h2>

			<div id="invoice-details">
				<span class="invoice-details row heading">
					<span class="invoice-details quantity">QTY</span>
					<span class="invoice-details papcode">ITEM CODE</span>
					<span class="invoice-details description">DESCRIPTION</span>
					<span class="invoice-details linetotal">AMOUNT</span>
				</span>
EOF;
	foreach($invoice_data['orderdetails'] as $invoiceline) {
		echo <<<EOF
				<span class="invoice-details row">
					<span class="invoice-details quantity">{$invoiceline['qty']}</span>
					<span class="invoice-details papcode">{$invoiceline['code']}</span>
					<span class="invoice-details description">{$invoiceline['desc']}</span>
					<span class="invoice-details linetotal">{$invoiceline['price']}</span>
				</span>

EOF;
	}
echo <<<EOF
			</div>

			<div id="invoice-summary">
				<span class="invoice-summary row subtotal">
					<span class="invoice-summary item">SUBTOTAL</span>
					<span class="invoice-summary value">{$invoice_data['subtotal']}</span>
				</span>
				<span class="invoice-summary row discount">
					<span class="invoice-summary item">DISCOUNT</span>
					<span class="invoice-summary value">{$invoice_data['discount']}</span>
				</span>
				<span class="invoice-summary row total">
					<span class="invoice-summary item">TOTAL</span>
					<span class="invoice-summary value">{$invoice_data['total']}</span>
				</span>
				<span class="invoice-summary row applied">
					<span class="invoice-summary item">AMOUNT APPLIED</span>
					<span class="invoice-summary value">{$invoice_data['applied']}</span>
				</span>
				<span class="invoice-summary row due">
					<span class="invoice-summary item">AMOUNT DUE</span>
					<span class="invoice-summary value">{$invoice_data['due'] }</span>
				</span>
				<span class="invoice-summary row gst">
					<span class="invoice-summary item">(GST INCLUDED</span>
					<span class="invoice-summary value">{$invoice_data['incgst']})</span>
				</span>
			</div>

			<span id="vendor-note"><strong>Please note</strong>: {$invoice_data['bootnote']}</span>

		</div><!-- /invoice -->

	</body>

</html>
EOF;

}


function css($css) {

	switch($css) {

		case 'invoice':
			header('Content-type: text/css');
			echo <<<EOF

* {
	font-family: Calibri, "Trebuchet MS", "Gill Sans", Frutiger, Univers, "Helvetica Neue", Arial, Helvetica;
}
div#invoice {
	width: 70%;
	min-width: 700px;
	
}
	span#invoicer-address {
		width: 50%;
		display: block;
		float: left;
	}
	img#logo {
		display: block;
		float: right;
		clear: right;
	}
	span#invoicee-address, span#invoice-info {
		padding-top: 20px;
		width: 40%;
		display: block;

	}
	span#invoicee-address {
		float: left;
		clear: left;
	}
	span#invoice-info {
		float: right;
	}
		span.invoicer-address, span.invoicee-address {
			display: block;
		}
		span.invoicer-address.name, span.invoicee-address.name {
			font-weight: bold;
		}
		span.invoicer-address.state {
			float: left;
			margin-right: 20px;
		}
		span.invoicer-address.postcode {
			float: left;
		}
		span.invoicer-address.abn {
			clear: both;
		}
		span.invoice-info.date, span.invoice-info.invoiceid {
			display: block;
			float: right;
			clear: both;
		}
		span.invoice-info.item {
			margin-right: 20px;
			display: block;
			float: left;
			clear: left;
		}
		span.invoice-info.value {
			display: block;
			float: left;
			width: 100px;
			text-align: right;
			clear: right;
		}
	h2#invoice-title {
		display: block;
		clear: both;
		width: 100%;
		text-align: center;
		float: left;
		margin-top: 10px;
		margin-bottom: 20px;
	}
	a#print-link {
		font-size: 10pt;
		color: #EF7D00;
		outline: none;
	}
	div#invoice-details {
		display: block;
		width: 80%;
		margin: 0 auto;
		clear: both;
	}
		span.invoice-details.row {
			width: 100%;
			display: block;
			clear: both;
			float: left;
			border-bottom: 1px solid #999999;
		}
		span.invoice-error.row {
			width: 100%;
			display: block;
			clear: both;
			float: left;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		span.invoice-details.row.heading {
			font-weight: bold;
			border-bottom: 1px solid #000000;
			border-top: 1px solid #000000;
		}
		span.invoice-error.row.heading {
			font-weight: bold;
			border-bottom: 1px solid #000000;
			border-top: 1px solid #000000;
		}
			span.invoice-details {
				display: block;
				float: left;
			}
			span.invoice-details.quantity {
				width: 15%;
			}
			span.invoice-details.papcode {
				width: 15%;
			}
			span.invoice-details.description {
				width: 50%;
			}
			span.invoice-error.description {
				width: 80%;
			}
			span.invoice-details.linetotal {
				width: 15%;
				text-align: right;
				float: right;
			}
	div#invoice-summary {
		width: 80%;
		display: block;
		margin: 0 auto;
		padding-top: 20px;
		clear: both;
		
	}
		span.invoice-summary.row {
			display: block;
			float: right;
			clear: both;
			border-bottom: 1px solid #999999;
		}
			span.invoice-summary.item {
				display: block;
				float: left;
				margin-right: 20px;
				text-align: left;
				width: 200px;
			}
			span.invoice-summary.value {
				display: block;
				float: left;
				width: 60px;
				text-align: right;
			}
			span.invoice-summary.total, span.invoice-summary.due {
				font-weight: bold;
				border-bottom: 1px solid #000000;
			}
			span.invoice-summary.gst {
				padding-top: 20px;
			}
	span#vendor-note {
		display: block;
		width: 50%;
		float: left;
		padding-top: 20px;
		clear: both;
	}
EOF;
			break;

		case 'lteie6':
			header('Content-type: text/css');
			echo <<<EOF
div#invoice {
	width:expression(document.body.clientWidth * .7 < 700 ? "700px" : "70%");
}
div#invoice-summary {
	width: 285px;
	position: relative;
	left: 80%;
	margin-left: -280px;
}
span.invoice-info {
	width: 100px;
}
span.invoice-error.description {
	width: 50%;
}
span.invoice-error.row {
	width: 100%;
	display: block;
	clear: both;
	float: left;
	padding: 0;
}
EOF;
			break;

		case 'print':
			header('Content-type: text/css');
			echo <<<EOF
a#print-link {
	display: none;
}
EOF;
			break;
			
		case 'toll':
		header('Content-type: text/css');
			echo <<<EOF
* {
	font-family: Calibri, "Trebuchet MS", "Gill Sans", Frutiger, Univers, "Helvetica Neue", Arial, Helvetica;
}
div#invoice {
	width: 600px;
	page-break-after:always;
}
	span#invoicer-address {
		width: 50%;
		display: block;
		float: left;
	}
	img#logo {
		display: block;
		float: right;
		clear: right;
	}
	span#invoicee-address, span#invoice-info {
		padding-top: 20px;
		width: 40%;
		display: block;

	}
	span#invoicee-address {
		float: left;
		clear: left;
	}
	span#invoice-info {
		float: left;
	}
		span.invoicer-address, span.invoicee-address {
			display: block;
		}
		span.invoicer-address.name, span.invoicee-address.name {
			font-weight: bold;
		}
		span.invoicer-address.state {
			float: left;
			margin-right: 20px;
		}
		span.invoicer-address.postcode {
			float: left;
		}
		span.invoicer-address.abn {
			clear: both;
		}
		span.invoice-info.date, span.invoice-info.invoiceid {
			display: block;
			float: right;
			clear: both;

		}
		span.invoice-info.item {
			margin-right: 20px;
			display: block;
			float: left;
			clear: left;
		}
		span.invoice-info.value {
			display: block;
			float: left;
			width: 100px;
			text-align: right;
			clear: right;
		}
		span.invoice-info.connote {
			display: block;
			float: left;
			width: 100px;
			text-align: right;
			clear: right;
		}
	h2#invoice-title {
		display: block;
		clear: both;
		width: 100%;
		text-align: center;
		float: left;
		margin-top: 10px;
		margin-bottom: 20px;
	}
	
	div#invoice-details {
		display: block;
		width: 80%;
		margin: 0 auto;
		clear: both;
	}
		span.invoice-details.row {
			width: 100%;
			display: block;
			clear: both;
			float: left;
			border-bottom: 1px solid #999999;
		}
		span.invoice-error.row {
			width: 100%;
			display: block;
			clear: both;
			float: left;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		span.invoice-details.row.heading {
			font-weight: bold;
			border-bottom: 1px solid #000000;
			border-top: 1px solid #000000;
		}
		span.invoice-error.row.heading {
			font-weight: bold;
			border-bottom: 1px solid #000000;
			border-top: 1px solid #000000;
		}
			span.invoice-details {
				display: block;
				float: left;
			}
			span.invoice-details.quantity {
				width: 15%;
			}
			span.invoice-details.papcode {
				width: 0%;
				display:none;
			}
			span.invoice-details.description {
				width: 50%;
			}
			span.invoice-error.description {
				width: 80%;
			}
			span.invoice-details.linetotal {
				width: 15%;
				text-align: right;
				float: right;
			}
	div#invoice-summary {
		width: 80%;
		display: block;
		margin: 0 auto;
		padding-top: 20px;
		clear: both;
		
	}
		span.invoice-summary.row {
			display: block;
			float: right;
			clear: both;
			border-bottom: 1px solid #999999;
		}
			span.invoice-summary.item {
				display: block;
				float: left;
				margin-right: 20px;
				text-align: left;
				width: 200px;
			}
			span.invoice-summary.value {
				display: block;
				float: left;
				width: 60px;
				text-align: right;
			}
			span.invoice-summary.total, span.invoice-summary.due {
				font-weight: bold;
				border-bottom: 1px solid #000000;
			}
			span.invoice-summary.gst {
				padding-top: 20px;
			}
	span#vendor-note {
		display: block;
		width: 50%;
		float: left;
		padding-top: 20px;
		clear: both;
	}
	a#print-link {
	display: none;
	}
EOF;
break;

		default:
			header("Status: 404 Not Found");
			break;

	}

}


function aplogo() {
    /*
	$logo_data = 'iVBORw0KGgoAAAANSUhEUgAAALQAAAA1CAYAAADoIcFPAAAAAXNSR0IArs4c6QAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9kKFRcVAhRAaoYAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAgAElEQVR42u2dd5QlR33vP1XdfdPEnU2zQbvSKqx2tRKKK1YJrZCsCAeEJZB5z2CMAw6Y4GP7YZ7DebaPAxY+hCcewkGSRbAARWyEEkIZC+XVslFI2hwn3dDdVfV7f1T3vXdm7oxWsIJzYH7nSDNzb3dVdfe3fvX9fX+/6lUiIhxmm6pJpdSkY9o/m7EZ+0lNH+4G34D5MWMzdsh22AGtlGr+1/53kiTcfvtdXHzxZfzVX/3NjGeesTfEwsPeogBqvKd++umn+cd//DRDB4YJCNm0YTPVao2ursrP+vpn7OfMfmJAT6IYChDvmXfs2MmNN9zEvffcjyAsXryYfXv3MjQ8zLZt21i+/Lif9fXP2M+Z/cSAVkohIq2fKATLDTfczDe/cRtxEqOV8Acf+wgXXXQRl196BSjF6Oho87wZm7HDZYfVQyuleOKJJ7j+i//K1i1biQoRbznnHH7r936b2QOzAChXKiRxzNDQ0M/62mfs59B+IkC3e9ihoRH+4i/+gk0bthDXG8xbMI9PfervWbRgAUorRBxKaXp7e9izu87Y2NjP+tpn7OfQpgV0C7ACokBlPwHBfzc6Osqtt97G12/5JqPDo8ydO48P/ub7eec73gFKccfOBnfvTvn8yT0A9PX3sWf3nhlA/7ybCCjVFAmaYkH2pz8m/8xT1cNhkwA9jg9n3ldEgXIop0FnCRFRPPWDp/nLv/wrbJJSbdS56OKL+MSf/hEiiqozvOOxUe7dUeek2cVm+7Nn9bFJFCMj1WZ/wKQ+35h7PD6ZM/4aZ/j84TRR+aqsmthGyGIs8j9ABKUOn3o8CdA5sGCyhxblB7Rhw0ZuvulmHnv8+8T1mDPOPI0PfODXOGHVSlKBT74wzBc2x4ykgIZ91bTZ/sCceaBhbHRk/A14AwGVtz0D2J+i5c9TWq5ZIQgCSvvPPaBa3vww2LQeuv1vgEajxle//HX+/aYvUypElAsRf/iHH+PiSy7COsuju2Muu38Pw4mCCHACVnOwkZKvL7MHBlBKMTY2NglobxSo26+nfSVo72sG7IfXFApxFrRCif9EMkgrEUB7MDsQfbgIxxQeutPfd9x2J9+87Q5effkVlFJc+e6rufLKt9Hf388LB2I+/L19PLAjprmUWJMBWqjHCak5giiEefNmo5SiWquj9WFPVE6yqbxzp8kzQzsOoynvwFQWcznlUGh0G3QznOeHHhZrAbqNvItqSXGvvrqNT1/7GZ57/jmKOuSYY47ik3/+SY5YvJhqbPjT+1/lbx7cC4GAtWAdpAZcimoYSGOkajg4cjTlCOLYALBz+w7+6Z8+w/59+9mxcweXXHIJV131rtcVIHTm+zLJI+/Zs497772PMIg48qglrF59OgDr16/nuWdfBIRTTj2J4447bso+Xqv/vK+pxvXjXs/kg9qfUyvegjwHINlnCiWSxfJq3LPNj8sZrYjquOJ7b6r8OZko4NttaQTNzzrcK6UUTsApCNGYjXdQu+1DqEIZteQcut75JZBgfAFG+xjbnysTgD/F76FkHEdlo8yHV6vV+MxnP8vjjz7BgQNDLFgwyEc+9mHWrFkNaK5/aBuf/Pom9gw3QBICW6OY1im5Kt12lF5XpWIPUlRVStbwWx/8Cmm9QWIFpwts3zfG1267h1JBUwkcr2zb5m/CIc7WqR5+p8/SNGXf3gNEUcicObObn9frMfv370dEaDSSKUE8EbhT9dWJ1kz3c+Kx07U3aRzN55V/nwVh6OzhS2spVwIiiMrAiD8O8L8p54GEb8eflIEqmxCiHIj3r5lGADo/ymXj0K17lD1Hje8PAlxSx+3fAQUI+5eCaN+GTOgv4yKt9rIpk09K/ASUnJerlgsMM34ABKDAGsNDDz3E5z77BepjDaJCxFXvvpL3v/99JHHCxk0/4o+/9CAPP7WZJZUxlptRuhgjcjFaWbQDEYtRIVYHnoFohTaO2V2OgVLC/O4qR3TVWdyTsm0o4t9+uIB1z6xjZGSE3t7eaUE8bvY7h9Z6SqC1g0Fr3YF6qCYYJs6iacE0DdA7xR/TTZCpJuRUf2cfeoiIQnQGGNHQBHvm5ZzCqRRF5K/POdAamh5a/ASQFpAFh3JZ4JaxXqMUoeg8pAPJKITT2Tl+ckhGKxwOnX0uWLTLgIvz3Siy9vNnYPyxgp8copsrgzfft/feCiUOJ/k6ozPdROWAlmbj69at47rPf5HNWzYRBgWCELp7Kzz19LM8eN8HqKcNbCo4qzg5UJixEKsgxRIWoa8iDPYYFnYbFvUOM687ZW7J0l9O6FKGQFtCAawCZ3DO4tIBRBRpGjNWG5sW0BNBMRHMU3m2KdtrehXXWr0mgS9zRRMm1FQTaALuOgbXnazzKqCa/eenirimExUl/uFnHrV5UeIBI1pQNgSdeW6lfBCmBP+rQosFpb1Hz2S1HMySuePAOdBBNp48vNO5+wWcfx4qAAStdDZdQBPkqQtQhY7PQCTwN0tB7lj9F65NXQOUNMencmaBy0BNBujsBqz/4Xo++uGPEha7ULpE6hxaBew/WCNSowSB0FsI6O61LOyNWdCTckRvwsK+lMHuOv0lRzkyfmDO4VKFSTVGBGsFsYJzmthalHOIUzgX0aVSjAipTUiq8bQAzB+6MZZqdYyhoVEU0D+rj56ebpxz1Ov1piculUrTgwjBOdtCH1Cv1xkaGiFJUrq6SvT09FIqFTNlqdWWMZY4iUGEKIooFArjQB/HMcZYlIJisUgYhtnnCdb6PguFAlEUUqvVOHjwIGmaMjAwQE9PT7Mv66A6OsKBg8MEgWJgYBZdlS7I+K1SBucUMrYTcCgB1bMwm6h+dXSNYVw6htIhdA+iBZwIgdK46gFwMU5Al2chUTmDhKDRuGQYGd2F7NuA2Bg9dzn0L0MVujMaI2jRoAx2ZDuy+xmcgXDBCtzIdhqPfJbotF8lOu4KlDNTPgv38kMkm75FHkhGaz6C7poD2sPVJQ3kwCbsga3onnnouatQpR7P/9vaCfMpXenqplzpQolw5pIqg70pi3obDHan9FdSukuOghICbdFKo0VwiHf9DsQKVRuhrUWcn6lOnL+pVsA5f5xxOBTKGZQLKSgLKNLUMFqvTwtopRQHDw5zz3fuYXh4BGMcIIRRyOD8+Zx2+incded/EUURc+YOcNlllxAEwTTttaKRQlTkySef4vnn1hHHcXMFKJfLnHHGaaxYuXyc59++fQf33fsAzjmWH38s55579jgv+90Hv8fO7bswxrL2grdwzDHLAHjggQfYuWM3IrDmrNXs3r2Hl7a+TJLEOCcUCgWOWLKIiy++iFqtzrfu+jZDQ0MYY1BKEUYhK1Yu5+yz1mRLcgDxMLV/uQBX24OkNbrefy/R0nMzfgmNO36bdNN3UGFI75/s8fc/g8Ho51aCGJSN6fr1/yZYsDJb2jXJ49cRP/zXuLG9OJv4+CaM0H2LKF/0d0Srrm4uDKBJX7qf+JZfRRyEx63FvPIYkjTQx11MBLRc9TgvBUpR/cb/RA68jDNQOPWXKZXn+IXGaczwVho3XY47sAXSFAkUzDqC7qu+iV586rjmQpUtabP6ewnCCGcS/vitr/rVxAXEFkQ0zvpFxLkAl3migABxDgc+lHWGVPnoWon3yloEZyxIgBXnBXcD4jSOhAoBKIczitHRzunwHCj79h7gy1/5CoWo1FyCnRPSxLBt23Z27tyNc0IcJ6SJbdtk4P3xVO0qpXjooYeJkzpaBeO4bb1e59vf/g6pSTnppFXjvHCSJDgndGIizghxnGKMGf8gRdNoJGiteOzR7xPH+aqk0DrAGMvGDZspl8q89KOXqFUb4yaSSQ3PPv0C/f19nLBihT+12Ecw9xjs+o0oAfPczURLz83KeCHZ+h1UchA3CuZH3yU48ny/yrz8CDK0Dx0p9Kwj0XOWek+tFPFdv0/j0c+hcn/gMtdnU+TgK4zd+G4qV2yhcN6fNClOoDROe8ZiNj/gp4ylqWK4Tr5FKeJbfwO7/2W0hmjwaCpX3+K5vGhEQ+36s7Fju2hSahGC/a9QveGtdP/JXlQQNZvTudjd09NDpVKh2hA27ymRpAG1xCtxTlwmPziUWM9fHIh1iBMwzlMKKyjjwDgwAtbh8u+MgdRBohBrEWuQVChgCZTCOEe9Xp3WQ99zz/0UoiJKgbWWU049icsuv5g1Z60mDEOsNZ6KqZzBtZzA9DvDFNZalNKcsGoFl156MeevPY9ypYxzQqVS4bFHn2Dfvn1NYLXrZ/nkaqckrtmhZFF+3pVrBqJJkhJFEeevfQsXXriWUqkEeAqzbt166rWY/v4+LvqlCzknWwEAtFZs2rDFB4FZv+GZvw/Wx4bulUcQU0e0wux4FjV8EOeFBpIn/7m5RJtn/hUJwYqgj3oLKqr4SfzQ3xI/9jlUqMBCMHs5pcv+gcIln0L1LwDnCAqa+r2fwOx8sqklCwotOd8FPX8FXR+8l+KKd3vvKZma4sgCV0he/CaNp77kJ44OKP/KbZnI4hu1z9yE7PNgVt3zqVxxLXr+SqyADA/TuPvj455kmM8SheLoZUfx6o4DbNxf4Zg5BzA2QkhREuAEH1g4QTlpBhF+gH6UKvtdOe+dcYI4wc8HjRjrgxpxOOsQ6yi4lFB5L14d6wxopRQvv/wye/bsoVQqorXml696J7NnD2RHLOX444/nlv/4BtVqtSNv7kSlW0Gkp0YXXriW4447tvn98uXHcuutt7F3zwG01jz//IusXXseh2JTMfcc50opisUC7/0f76FQ8B6mq7uLW795O2EYopSir7+Ha665unluuVzi7m/fQxRFjI6OtrUpFI65hHr/fGR0N+mBLUh9BN1TJnn6ekRlcq+CdNNdOJuixJK+/AhagyRQOONDXrYb3k78nf+FChTOCcVzP0L50k83r6d07sdpfPVqGi/eQiDQ+Mp7iD62JVuE8qARKHTR9YEHUV1zPdcGrHiGqzRoCcElxHd/3H+WQvHyvyGYtwov2fnQMx16FQl928XTPkC05qOEK6/CbLjDS8wD43MHuv0un3jiKqw1rN9VQXuG7AV1mwUbNgOrx2QTlGIdknvmRHCp99rOOMQAKYhJcdbgTIpNLTZViIXAOkItGCfUxqb20OvXbyCKIpwT3nTyiRmYpem1yuUiq888rRlwHRLEMmohIqxYeXwTzHmbYRhy6aWX+Khd4Ecvvdz6XtqbeR0bg1XrnGXLjsrA7D3DokULxnH+s886u/l4RCxHH30UYeS/T9O0OTFU5qlLa//M147FdcwLX/HHrbsFpaEweBoqCJB4DNn1HNIYQUZewQmES04mXLwaALPtMbTSOBEKc473YM6u12WXWXzHFwh6Bj1I920l2f54dlmuWVEX9AyiuuY2S5G8Umebsh9hgepX3oXb+yMQKJzxPgrn/BGCwbpWbBMuOweM19Xjx/6J+O6PYZ2lsPp3KJzxuxSOeeu41Xdc7nnVqpUEWNbtKpEaP0NxIE55gFqHM4KzFrEWrHgaYQQMuNQfg7E4A1iFGIsz1lMOq3CpwqWAsdhUo0xCSRkQGKuOeXrTwYaHRtHa37YFCwczILZkOhFh8eJFOOeml+va6WwGCASWLl3SOqRNPiuXy3R1d6G1plarY2zWfsf5MVk/zoPLid2LOPr6+7JjWm10d3U3z+/r72kmFkCjtaZQKDa15vZrRxzRcZchpS5EQfz4dcjIdqS6H2eh9K4boHshyhrs0Gbsxrux9RpaoHDeJ5p5Q3Ngs9eRDQSrf6tJliRTlRGHKg0Qzj+FZknG7g3ZmNtuRlTJFb1W2ahogizLmLx0H+7F/0RCBTqgcOFfo0UQCQhUluQRIVpyHtFpV3onmtRpPPhpqv9wJGP/ejFm03+26dAZoFVbmnvJsiMpRIrt1QJjtdBTCwPWJbhMekMEZR069XzZWYsY8d7agLKedogRJHW41IG12ERjE++1nbGYVHCJITAJpdCf00jqnr50MOdcKzgiD/ZawVIraaKm9Jh+i1gbtpvklwx0QicN2+vJDrDjxtdSNSb30x5wTlRacgkw7KDAKD1+t/zEsbRKMtuuPRuk6lmM7luIVgq7eyON5/4dxBH0zyWcdwLBgpNxCsymB4ifud5fV2UW4dJzW/zcJlk2DlS513u8VpDQ1Ed0ocuvKwLikmxs+SzLp6Dn9Hl5j8PgAn9OUJqFBF7TRhz2xW/4J9uezsYiDirv/jqVK671k0QDIZit36F6w+WYJ74wbibp9qRBpVRm6ZFLMKmweyzApQ5rHdpqcIK2DlJPL2z2n0s9cEm915bU4hLBpRabek9tU0GMwaQGlwg2cUhqSOMU2xC6iTEK6vWGXxU6WLu32r17TzP12W47duxE66nLRCeCL0/7g+foeeawfULEcczYWBVQlCsVosirIFFUaI6hWp2szniZTXDOc99WpxqlpJW+bgJ2fFIof6gTQd2scWhbXPN0sQpCojN+B0kFFUHywP9BixAsO98rA8vfRuAgee5G0lceBQXh3JWo7rl5715j1qACSJ/5chageRonzucKxSaku55t1jjrWcsm3+tmnke1AU7749GohadTuepmMH5W1L71B6Qb7mirMxGE0I8FReGsj9L/Z1W6f+0eomMv93gPoP7I347rV0/0AMuWLaMQwI6hCO18CaCzFpVaXOo8YFNBjEMZQVkgVztSPIhTD2gxObAFkypcorCJw8SOpO6QxOESQ1EZlEC91sDazuL78uXH4axD64Dnnn0ea9v4owhpavj+9/+bKIo4VPNZKofWimefeZ69e1sqRg6s++57oPn74sWLm+cWiwWUUmitePWV7VkSpUUzhoeHyWtk+voOLfv541pe+yAilNZ8BCn44EuSKk5pwmMvBSVEx1yEs4CtEzoLKURv/jBKBVm5pyNacDKO0CtJL91HuvWBZo2H0l4JStZ9Ddm/2XvhKEAfdX7zfk51n3Nr1m2kYxROvIbolPcCECio/ce7cfs3t1ZcVyO+/UPUvno11a9eiRt+lXDZWsrvuN57fwGli+P6mlQ+umjRQrSGbcMRa+ZZUudB68R5tcLhowMBcQ5sRgec+OxflkRx1iscOPG823klwzYDScGmgLP0SB1RXTTqCc515tBHH72M2XMGGB4eodFIuOnGm1m16gRmz5nF2NgoL67byPDQGEHwOkpS22hKoVDktlvv5MSTVjFv3hwajQbrX9zArl17CMMQ5yynnPKmJrh7eroJQ02aOpxz3HH7XaxYuRznHD9cv5GxsRpaKwYH51MqlVr0pDWVfmIgt8Ek8/y+1eJpv078/f/nYw7jiJZdAE6h+49Ez1+G27cVqzW6fxbFE69u0SNRMH8VpdPeR/zYPyMR1G68hPCM36C49DysjTEb7yZ55mZ04Oll8T03o3XwWrpo0wK8TOhpiaV0xWdIN/8ntnYQlTSo3nQZPb/3PBIWQJdJX/4eZteLKAXVkd2Uzv9T0g134hTj6VAO6ImccdHiRWhleXWkSEBKGmssgnZZdZd4wErGlXHOp15tDmrxXiBLb5tM7bBNEIvn0Q5sorAOukwMgWZsbGwalUJYe8H53H7bnYBPnjz55JOtCsGMq+Yc85CBkN2TXNt+9pnnxnHjMAxI04RTTz+ZuXPn+LPEZ/TOfPOZfO+7DxOEmj179mZUqFUMpTWce965NCMjWqvCa9V1vD48q6wwSFAEFN/0K6RP/TPKGJi3HN23tNlmcfXv0vjWxxHnKJ354dZtEPG6ngjlt38Jt3MdybbHUTYhffzzpI9/HqV8lXAe5EWr3k5xxS+Pq1PpRAXbzZFtWFEAGlUeoPdDTzL8j0cjgcLu28TIv6yl5zceBqUpX/y3jPzb21Eh2G2PUr3x8uY4nIXSpZ8a176eWPCzcMFCcML20RI6tYjzgZBY8RzZGB/oGYcYi81piHWIEWwiuMRhGo6kbjGxxcYO2xBMI6MbDUVcFRqxo1YXiqkBpanX61jrxo0n/ymiGBycx5Xvejt9fX0kSYq1DuscaZoya9Yszl97HtVqlSRJmrJWfn6SJCRJimmjNNZYkiShVqtz2mmnsvKE40mSBGssxlhMmqKVZu0F53HWmjeTB405tVi1aiXnvuUsRHx9iXMOay1paihEEeevPZfBwbltXB3S1JCmJssyukngjWM/ziRJM5yN/z5J/bUlSdq6RyrT+TOOqua/CaUL2BpUzvy9caXD0YnvwVm/0gYr3ukLP7VPkefiiYhQ+bX7KK/+ENbiZTPrAWRSUFFE+eJrqVxzuyfbeSxoG7gGkIBtDGfja0dzCg2QGEiqmQ4oSP8ySmv/DFcTJAG36TEad/6OB+jyt9H1tmtRKV50EHAGnNKU33kd0XFXjFeXXBaF5Q9p3759vP99H6QcCf9+3lPEib9JYv0y6ZwvL7RWmh5ZnOBSsgIkTy2M9V7cJR44xoIzCmsgtQ7rxMt2zvFksJg7CytZNGc2137675g9u1WzPFXp5bZt29m2bTtKKRYtWsDixYsxxjA2NuZncBDS1dWVFTMZxsZGUUoRRQUqFf8KskajQSNugPjArVgsMTQ0zNatW6nXG8yZM5tjjjmmSWOmqm1O05SXXnqJffsOoJRi7ty5LFt2ZEeVolqtkqYpSkGpVG4WNeXHjY6OZgVTiu7u7qZCkvflubn3hL29fW2gVuAsSvnqNju2C10/iB5Yhg0KBE4h2oFTuJGXQBy6/6hW/fS4QvrMkSiF1PaRPHsjdudzPuBeej7FVVehwko2CQxaQowSVGMYdXATlpAgKqHmZKl55du0yRgc3EwApMXZRP1HNFdXAezudWhxOBVCMkqw6BTQIaCR4VeJf3A9bmgzwfxTKZ3xm1DoHVfbjR97C94igrWWq971HuqJ5UtrnqM3MN5rOpvp0sprgjmgrcXajCfn8p0FYy029nwpMYDRWCMk1oFRxKKx1oNiUziP20snMtDbzbWf/nsWLFgwCUDtD/31bJ96PZ//ONuyDuX7duvU/qF8P1Vddeu7rNgir5jP0+JZaaxkNZhK5VXOzQ38MKFIPjcnWWFmS41rHq9yaS9TqL06oJu7V5pvvMjUHBHtNxLkqlq2tzDv10/KifFPvlnAfzc5uIR8I0BuOo/K27NjixcvpmZg01CZwCZgLBgFRvlqucTg0hSbJKRxiokNpmH8z9hg6pZazREnENfB1BX1hqHRMCRpSN0o4lSTuJDEKQriFYI0SWg0Gh2B0a5o5A+zU63xxO87AWDiz07Aaj9/Ok47sd+J7U+3Abhdq24/TkTG6+7TjKG1g8UT01YBj0OLRSmXAS5P0LjswdsmZDqBmSZYW0FcU2DG16IpHE5pz19ckPXvgerIchbZOZ5bZ/u+s2IOyUAqWXETeZ1QXpstCuVUE8zOB244cSglHcccdvIuJ528imde3MIz+/tY073Xy3EOnLU461PeWB/8OSPYzDvbLOgzRsAKsTGkLkTXa4hVbFy4htFiL04pFuxeR9/wLnRg6DIB1qYkCeOCwk5L9msBrB0o7aCf2M7Etqba6d7pvIl9TNw+dSj7CTtvCOi8I/21Jsa4sTffd6GywMuX8irJdpdIrvFmn+fKSzvRzqGeASkQ8r0qvj/PUdpWgKyvtr2AWmW5RckKuLIp0obuvKFMe86qpzKE64xMKJ0VNCmFyjYL+B01qnWt7YCeeIOVUqxadSKBfJ1N1W7SxGBS7dPeNpPprK/ncM0ESw5mz5PrVnBGkzhNYgPWL7mIVEfsmLWcOCwQEjJUnkNX7QBaFAmKxcX5aOe4//4HWb9+g+eO0jYurZk7dw6nn37qOJBN94CnS7BMBZRD3eDa6djpNsx2ok7TTZJDAfrkbOZ4FSvf7wd+90q7upKRjlbAOmk4fqtTqx0/BpdvuBJpArCZns9UMP/ajfZ0f4cXerYlk8h3wCgZV4mnxfoC/kyPzGuLRKuml1fNbTNqPKDbb9CKFcdSjEJ2NgJqDQGbZtGuRZzGGtvkzNaBzTy4MWCMw1hFwyjCeJQtR6zlR0vX0FMs0iOO3qwfNWsFifLFs0rB8mwW79u715dpOkFr1dyvhhJ2bt/FEUcsYXBwbkcQtl/Da0l3021WfT3nTzVZpgLoa03Eqfp+XQkYaVZfIsoDRUnm4dp3hbcQ19lDt937/EeQnSDZB6q1v6qt7Fs1S0o7XIgH4rj+cm8/vkshaI0lr+ZDNSdjPsp2whS2+mnd6P7+AebPm82uoWFGGyEV6ojBp7qt15St9Sltaz2QrQVjAxKriJ1CjGbLwjN5ZeBojl2yhLPPerNPYRYLFAsFomJIFEboQBMGITrQBEFIdayKNYZZA/0Ui+P3oH3iE/+b++67l/e+95rXBMBr2VR05vWeP915hw2ghzailndWzeffWuqbZSutxX/cODp46InntLt/NbGj/P+q/fdxo5vQVoeu1PhJ1qG3DmMbb1O+rHFwwQJ2Hayypx5xhKr6ZIgVnIHUOKzVGCOIDUidIrFgXEDiFAaFE8tLg6dQj7oJsmXIzzTBikUZ5WuntSZRKeKEG2+4iQMHfO2xE8cv/dJbOeecs0mSlDCMUGh27dpFHMcUi8XXpAUz9otnk/LEOUAGFwwS4djVKEGStmowYiGJFUmsSVJNzeJVC6OJnSIBDAFWOYJspQjCgFKpRLFYpFAoEIZh9lqBwO/bK5b4+n98g927d7N3z242bdxIbbTGt+76Ntu27cRa19zn56zK9irOgHnGJtuUhQ8LFw4iYnjF9KKMxSR4GS5RxElAw2jqiaaehsTW105rZ4kcBCIkFLGZRKQKAeVSiSiKCIKAIPBA1oEijAJGRkd4ddur7N27l2veew0nnHgCL7zwAiZN+a9vfRuAWr2WBRR+m/yhqB0z9otnU1AOYcnSIxHn2JF2ERmHSg3aaIyFutUMqzKjKqQalBiWMgeCLoZ0hWoQMarKVAk5TncRChRUSBhFzSKUDCoAAALXSURBVIyU986eO5eLRfbs2oM1ltGREfbu3cvoyBgg1Gp1hoaGGKuOETfiTEnqXLM8YzMGHd8+6nn9woULUE6xy3bzQ9XP8wyyK+hiZzjAQVWhho9CNc6XYDoviNtUUAFEgfemSnnJLQgDjDForZte2he4C4sWLaJYKjB79my++IXrCcKAQqFIX38fCxbNp15vkCRx8+UlMzZjU1mHt4/6mpF5g/Nw2rLXFfhzeStgCUQIlBBqoRgERGFAqdxFsVSiXKnQVSnT09fLwKw+entm8cP1P6Raq3mvrAKCICQMNGEYEugAHXi9qFgscPoZp/O9736P45evIE5i+vr6KJVKnHbq6YyOVQmDkDzlmmufMx56xibaZMqReehKqcySI5YyfHCUuZWArkoPXd3ddPdU6Oruplyp0N/bw8DAbAZmzWLO7AH6+mfR3dNDsRghInzus9cxsmVr5pU1YRgQZQGhf42XRRDSpMGFF11Ib08PDz30CEmSMG/ePM46+ywkKy7KBZ18g+oMmGesk02mHFlllABf/NJ1bNm6hd079lDsLjPQ10dXpYtKpUyxVCSKCh2alOaeud//8O/yta/dQhzHxPUGqUlJdZ7Z8ZV5Xsf2ZZ1HHnUkg4MLqDfqaB0QxwkjIyO+9jbwL7VBT52YmLEZm0w5RHx2yb/Vj2XLjmbZUcv8Pris1DDf/jKpCo4sM59RAq0VJ606kf973Rd44vHHO2fmUP79a3lql8nZNMlqCaIwJIqi5ptEZ2zGJtq48lFoy4AK2Yuu8YUgQrbLWk3Okrad10lK+8EPnubhhx8Z/xKY1xvdKf/GoNWrV3PBBefPeOgZ62iTAH247Y0A3qEWEM3YL54d/n+8PrNO9cXt9uMCcQbMMzadvSH/as/EUsfXKps8lPam+uceZmzG2u0NAfRUZZHwY+xonmCvp8Rzxn7x7A3n0DBDE2bsp2c/FUDP2Iz9tOz/A1/xZFImUGEHAAAAAElFTkSuQmCC';
	header("Content-type: image/png");
	echo base64_decode($logo_data);
    */
    
    $logo_data = '/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MSA2NC4xNDA5NDksIDIwMTAvMTIvMDctMTA6NTc6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjk1Qjk5NzVFODc1ODExRTBBNzEyRjI5MkY5NkEwNTc0IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjk1Qjk5NzVEODc1ODExRTBBNzEyRjI5MkY5NkEwNTc0IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUuMSBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5kaWQ6Nzk4QkJEODU1ODg3RTAxMTg1NUY5NjRFN0Q0MTNCRjAiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Nzk4QkJEODU1ODg3RTAxMTg1NUY5NjRFN0Q0MTNCRjAiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAACAgICAgICAgICAwICAgMEAwICAwQFBAQEBAQFBgUFBQUFBQYGBwcIBwcGCQkKCgkJDAwMDAwMDAwMDAwMDAwMAQMDAwUEBQkGBgkNCwkLDQ8ODg4ODw8MDAwMDA8PDAwMDAwMDwwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAA1ALQDAREAAhEBAxEB/8QAngABAAEEAwEAAAAAAAAAAAAAAAcBBQYIAgMECQEBAAIDAQEBAAAAAAAAAAAAAAEEAgUGAwcIEAABAwQBAwIGAQIEBwAAAAABAgMEABEFBgchMRITCEFRYSIUFYEyQnEjFgnwkaFSkkOTEQABBAECAwYEBAYDAQAAAAABAAIDBBEhBTFBElFhcTITBpGhIlKBFBUHsdFCYnIjwaIzFv/aAAwDAQACEQMRAD8A+/lESiJRFwW4htClrIQlIuonoAKE4RdceSzKQXWFhxsKKfIdrpNjWDJGvGWlSRhd9ZqEoiURKIlESiJREoiURKIlESiJREoiURKIlESiLj5J736fOiLxzMhEhIK5L6WUpF1KUQkD/EnoKrWbUVdvVIQAO1ZxsLzhoyVGWb5ZwGP9VuAtzLykC/oQ0Ket9SpNkj/nXC7r+5O21MiM+o4cm6rdVvb9ibVwDB2uUA7RzJsGV9RiGtGKZUCEkHyet8x2SLfQV8u3f9y9xvZbXb6bfmuz272jAzBlPUpp443zGu6lGyGQlAeQ8XXLixdbuly5JAuSL/zX132xvkb9tZLO7XGpOmq4nfKX5W09nAZ08FKmFzcbOR3JUQEx0qCUOeQUFAjuCkkV1dO4y0zrZq3kVpiMK+VcUJREoiURKIlESiJREoiURKIlESiJREoiURKIoP5i5Vj8aRsQHZDEF3LiSpufJYdlJSIyAsNoZaKCpbilBKbqAHc1s9r2t95xDQTjHArUbturKLcuPEaLTZz3CcwciYnJxIWqYaXACkRHZDbLqVrfe+5qOwkvKLjqwLhKATa5Nh1rY+4fYu2Wqzq1mVzesHhrhaHavd+4F4lgiaccjorpioPuo1rHycvj+LMdJRJbQt2Gt5lUuybnxSwJIV8eqa+a0/2h9r1yWtsSfiF1lr3VvlkAuhaMdhXz623dd0Gx5iTmcnPxmUE11eQw7rTbJjOKUVFn01o8kgdbC9fYaPsfZY6zYo4I3N6cZwMnvXLf/Vbn6pJleDnhnRbU8fbFL2Xi3aocl4Pzdbdxu24p0Dx8ozn+TJR4gdCmxuK/Pnv32jFtMEteEkg5e3tHaNPwX0G1vEm9UhZkbhzPpdjh3H5L6KcEPpf0iMpBujyPpj5J8lW/6Vd/bx5dtTAeWnzK17PKFNXmPlXdZTKt+Vy8DCYzIZnJviLjMVGdlz5RBIbYYQVuLISCbJSCegrONhkcGt1JOAsXyNY0udoBxWAM8z8bP4/j7KtbK0qBym+IuhSPTdtkHVJKglA8LpuB/darZ26wHSNLdY/N3KoNxgIYerR/l71J/mOo+XeqOVdyqlXb61KlU8uvaihV8rfCoypVPMfK1MqE8x/x9KlSq+X0/miJ5fS9EVPMXI+VRlFUqABPyqUWObRt+A0zF/utlnpxmM9dqL+UtKlD1X1eLabJBP3Hp2r2r13zu6Yxk4yq9i1HXb1POBnCyFLgUlKk9QpIUk9rg144K9wc6hAu5ta1MoDlc70UrQL3yrSxA4+V/wB0icCb/Dxa6da7X2XpLLryXEe9G5ji01yr37K9XjOaPkNxlkSZMnLSouGSoApitoShLym7dlOqA8ld7JAvaq3vCx1WegaDpH4qx7Sq4r+o7Uk/BSRF9y2pSuW4nFETHPznpD8mE/nIziXEQpbDhaDcxvxBa9VQIRZRPYkAEVqH7PK2v65OgW8bubHTekAtVP8AcY4+xEXXdW5Sgxm4maTkU4POvoABlMPNrcYLnbyU2psgE/A/Stz7UuPDnQknpxkKhvlVp6ZANc6rXDhvfBreizoUbWY+Wy2yYdGPVl5T6m0x4iw8lTaW0gqWSbK726V8z/dvdG07EbOnqc5pHhwWw2id7KrogMtccr6R+1/InD8OibmZSlNYVqW9NkrB8gxGW4oqsevRCa0/sRpdU6QMZdp8Vfc4Rsy7kCVGGlZv3Ne4rHv8manyJjOGtCnyZCeP9e/VN5OVNjMOFtMme66R4hwpv4p/jp1V9ivQ7btbxXljdLIBl5BIA7h2rl6s24bg0zseI2E/SCMk+KtOn8p8xbZJ90PH/LJx0aZx9pK2kQsYz6cdchyI+FzGlm6ymQkJWEk/bewq7NtlOH8nPWyQ+UaniMEfTju+aqRbhbm/NQWcZYzlw171D+bm7DjeFPYVO1HGs5faI+TA17GyFFDLstbJS0HFJ6hAJuq3wFbCs2J17cBK4hpackaqhafI2lRMYy7qGPFbFbYz7rOH8DL5TyXJuH5Px+voM/dePf1LcFoQUkF8459B8wppNyPPuB/FaGr+mXpPy7Iiwu0Y7qzryyO/gt3Z/UaTDO6UPDdS3GNO7w4rOFcz5rOc4cE4TW8g2OPuStMyGwy4bjKFOuLQhK45DvVSfEGxA6GqP6YIqs7n/wDpG8M/jnRWxubpLUDW+R7C7+HNZLyfyBtWs8z8C6fh5zbGA3qXlGdlirZS4t5EaP6jQQ4RdFlfKvPb6UctSxK4fVGG9OvadfFeu4XZIbUEbT9LycqGtc2D3D8ubrzBreuckYzR9c4+2l/HQMmMW3KmuC12Y1lEJDaEi6lEeRJraW623Ua8D3sc98jOo6kYWrq2L92edjHhrI39I0yV59G2T3Lcsztt1H/XmF0KVxTPVhNi2OFjUynsxkApRQtLTpKWm/TAKgn4mvS/W2yi1kvQ54lb1NBOA0ePNYUbO5XXPj62t9I9JOPMf+FdIfLXMc/iXltAkQmuVOEswYuWnx4yVRsrEjWecUllVwguM+Xb5X+NeUm11G2ocZ9KZumurXeKlm6XHVJjp6sLtdPM3uV25N9ykzCI4UyuqFD+I2tqPsO7WbDoYwbimo67q/8AXZ50jy+abVht/t8Smw2U4czLWd7lnuHuF0XoGIZD8Od3NKz6dydm5HN2b1+BkG43HnG+qnMbw6hpDinZclJcjtpcPVPi2PKw71SbtzRSa86yyP6WeA0Onirr9zebrmA/6o2dTz3nULDdZme4DmLFjfsLu0DjDWMopx3T9fGOROefipUUtvTHHLEepa9k9h2qzZZt9B/ovYZHjzHONe4c8KrWk3DcG+s14jYfKMZJC9COZ92RxzzDAz7MTD8qcURwJcqKgORJIcAUxMZbX8FDuk9Kk7TA6zAWEmGX4juyoG8TirOHgCaL4Hsco/39/krLcDp2fkfLwZK9ozOtzMDh4UcMJgMKeCiHF/3KdulR+Aq7QFZm4uZXBw1rwc8ytffdak20SWXAlzmEY5BTtrm87PyLyPJY0+Y3B4v0omJnMx6SHVZjJWF2Iy1A+LbV/uWnv/IrS2aUdSAGX/1fqB9re0rdVL0tyx0wnELMAn7jjgOxROOS+VNo3fbI2ubtgsBltWzysdieKsq22wvJQ21BK3lSXPu8lglSfG9bN23VYII3PY5zXtz1tJ0PZjgtaNzuT2JGxva0tdgMcOI7c+C3P9aX+N6noJ/K9Ly/F8h4+p438fO3by6XtXJdQz/bnjz+C7DLsd+OHLPivn/7+luJxfHHooU4pUqfZKASeiGr2AvXY+z3BskmftC5X3XH1RxgfcsQ9nXMDOk47I6PviHMDgszkTL1jYZ3+VGRJdSlL0d5SrekF+IUhSvtJuL9q9vdNMzPE8eumCPBR7dtGFpik0GdFvwnSuPIece31ONx0bJuEyns36wSz5lPiZBBWGvMp7rtf61yTrMxj9LqOOxdM2GIO6wBlaFf7hz+S3PT+KsJpuYxuWx2Xy0mbJixpCHFupbj2ZlhaVFBZb8lA/NRTat/7a6Y5JHPB8vzVHcx6gaMrLPZPpmEc13ccfLYRN/Su4uIzIUlKifGMsqIKkkjyJJ71rPdFSK1K17264/krdD6GkLel/UsQ7iMzhEsFqHnIb0Kb4nr6Tzam1ePwHRRrSVIGViDGMYOVZmb6rS08wQtGON9g5w9tuBHEGX4SzvKmC1x95nj/ctXcZLUmE64pxpuYh0gsrR5WJPb+AT224Q0d0kFps4ic7ztdk4P3NwuSpzW9tZ6DoTIGn6HDn4rx8c6DzfK2r3QbbyNphxWY5M0xtGCiwnEPx/VVHfQzjmnAr73GElKVki3ke9qs271FkVOKB+RHJlxI14jLvA8l4VaVx0lmSZmDIwYAOmezxVjz3FfNjXEHtAgadqLqd840m/m5aJMUhDMB1plRSmYoL6IcI8CU371lW3Ol+buGZ56JWkAjmsZ9uuCrVETB1xnJB5KQd25D595f1bI8V69wHmuP81tbBxe07fnpDH6vHRXh4SnIzjarvkpuE2F+vzqhTpUKM35h87ZGs1a1oPU48s/jqrlu7cvReg2BzC7RxJGGjn/ACXu5B4n3Li3OcDchcZ649yBF4ewT2rZzVGXENTpMB5pKDJjlfRSwq5Kf8O/Wsal+G6yxFO70zK7rB5A66HCzuUZqjoJYW9fpjpIHEjtVocf5d5Z534U3uXxDmdH0PS5U9pa8stn8wOSIyg5IfaSq7bdwlCO9zc9q9WflKW3zxCYPkfjQZxp+CrvNu7filMXTGzPHipf9v8Ap+z6ztvuDnbDhZGKh7TvLuS16Q/4+MuIWvEPN+Kj9t/natfvdqKeKsGOyWRgO7j2LZ7LVkglsF4wHSEjvHavXwXqeya5uvuDyGdw0jGQtn3VWQ16S94+MuKWvH1W7KPS/TrY1hvVmOevXaw5LY8EdhzwUbNVkhmsOe3Ac/I7x2rq4f0vO4rd/cPJ2TBuwsLuW0JkYd2R4lubDVG9Na0eKibG9jexqd0txyQVgx2SxmD3HKbZTkZPY9RuGvOneMKKOKvbhloEbmnA7g045i5sKRqHHbz6krCcOpbkttxruUp9VxPfr5JNbLc9+ZIYHQ6EfW//AD4H/qPmtXtuwvj9dsnld9LP8eRUie33ibOYnjDZ4vI7Dydu5Edko2gvFKnxGS0YUdBUkkdGwVDr/dVLfd0ZLcY+v5I8FvjxPzVzY9pfHTdHY88merw4D5Kw6bsXLfC+Cj8b5rinLcgxNfC4upbTgFtFqTE8iWUSUuG7S0g2JNeluvT3F/rtlaxx8wcOB7vHivOnYubez0HxOeG+VzSNRyXnd4u5Ancdc5bbsmKQvkblSEERNVgrS7+JGjpCI0UOEgLXb+o3t9a9WblXZZrxRuPpRf1HmeZXidssOrWJZGj1ZeDRyGmB2LLuUtO2nN8C6jrOJwj8/Pwla8ZmKR4+ohMUt+v5BSgk+ASbi9VtuuRRbhJK52GnrwfHgre5UpZtvjia3Lh05HhxXfidS2jiXkQL0/CyczxZvbxdzmFilKlYLIqA8pLSFEf5S/7gnt8ugrCa1Ffrf7j0zRjQ/e3sU16k1Cz/AKm9UL+IH9Du1RLyBrvIm4t5rUdk4m/db6MkRp/LGOQ1EisQw6lTLzrySF3bQCCk9z8K2NCxXrFsjJsR4+qM51PBavcK1qz1RyQ5lz9MgwAB2LdX9dlP9Mfqf2B/cfrPxP2tuv5Poen63/l91cn1N9brx9PVnH9ueC7D03+h0Z+rpxnvwrVu/HeschQI+P2WI483Dd9eFJjuKYfYcI8SW3EG48k9CPjXpWtSV3FzDjKynrRzjDxlRI97VOLX0qS8vOupWLOJXknFBQ+Ruk3H0q6N6sjmFX/TYOxWhXs34YW2GFM578Ydfxf20j0f/l/R/FrVH6xPnP0/ALIUIwMa/FcWvZrwvHUtUePnGFOf1lvJuoJF72+0Dp9Kj9Ys/cvQVGAYU58f8capxlhV4LUoCocR54yJj7zi35Eh4gJLjzqyVKNgAPgB2qlNO+Z3U8r2ZGGcFnVeKzXX6fS16gBFUIt8enyp0onifnUqECbG96xDR2KVUpvb6VJCKnh3696YRPCpwFGqr49e9FKePfra/wAaKMJ49rm9qhMKoFvqalSuJQbkg2vWPSM5wi5EXHe1ZIuPgb3JvfvRRqgSR8qKUCLfGowEXK1SirREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlEX/9k%3D';
    header("Content-type: image/jpeg");
    echo base64_decode($logo_data);
}

?>
