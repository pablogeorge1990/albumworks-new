<?php

$sol_title = "How do I download and install the editor?";
$sol_keywords = "download, install, editor, start, dl, down, software";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>To start creating your own <i>albumworks</i> product, you'll first need to download the editor. This can be done from the <a href="http://www.albumworks.com.au/free-software-download.html" rel="external">download page</a>.<br>
Simply enter your details, select your operating system and click 'Download Now'. <br><br>
If you have selected <i>'Windows'</i>, your browser should provide you with the option to either 'Run' the install file or 'Save to Disk'.  Click 'Run' and the installer file will start downloading.<br>
Once the download is complete, you will be presented with the installation wizard that will step you through the installation of the %%vendor%% editor.<br>
After the installation is complete, an icon should appear on your desktop and the <i>albumworks</i> editor should also appear in the Start Menu.<br>
To then run the ediotr, just double click the icon on your desktop or select the <i>albumworks</i> editor from your Start Menu.<br><br>
If you have selected <i>'Mac'</i>, depending on the browser you use, the download will either start automatically, or you will be prompted to click 'Save File'. <br>
Once the download is complete, simply double click on the installation file and follow the installation prompts (drag the editor folder into the Applications folder), then you should be able to open the editor directly from your Applications.  You can also drag the icon down to the dock for easier access.
</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>