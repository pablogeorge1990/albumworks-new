<?php

$sol_title = "%%vendor_reshape_photo%%";
$sol_keywords = "%%vendor_reshape_keywords%%";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>%%vendor_reshape_content%%</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>