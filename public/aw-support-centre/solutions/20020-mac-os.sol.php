<?php

$sol_title = "I have an Apple Mac. Can I use your services?";
$sol_keywords = "MAC, Mac, Macs, Mac OSX, Tiger, Leopard, Macbook, Apple, compatible, operating system, specifications, specs, run, work, Windows, PC";
$sol_categories = "[cat=system requirements]";

$sol_content = <<<ENDCONTENT
<div class="faqsol_sol_body">
<p>Yes, our editor is 100% compatible with Mac operating systems. Our editor has been tested and works on Mac OS 10.4.x and Mac OS 10.5.x. Intel and PowerMac.</p>
</div>
ENDCONTENT;

// variable tags should only be in the global variable file

?>