<?php

$sol_title = "Can I scan developed photos to use in my project?";
$sol_keywords = "scan, scanned, scanning, scanner, developed, prints, photos, scans";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>Scanned photos turn out well in the final product if they're scanned at a high enough resolution.</p><p>We have many customers who have successfully created superb albums using scanned photos.</p><p>When
scanning your images, make sure they're free of dust or dirt otherwise
these imperfections will turn up in the final product. You may also
need to crop your photos after scanning to ensure there are no white
edges.</p><p>If you have the computer resources it would be
beneficial to scan all your photos at a high dpi value (600). This will
allow you greater freedom when deciding where and how to display your
photos in your album.</p>
<p>We highly recommend against scanning at higher than 600 dpi as this won't introduce any greater quality but it will slow down your computer as the files will become very large. This may also introduce file processing issues if the files are too large.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>