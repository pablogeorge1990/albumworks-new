<?php

$sol_title = "%%vendor_apc_exe_error_title%%";
$sol_keywords = "%%vendor_apc_exe_error_keywords%%";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
%%vendor_apc_exe_error_content%%
ENDCONTENT;

// variable tags should only be in the global variable file

?>