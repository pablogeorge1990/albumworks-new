<?php

$sol_title = "Can I see some print samples of your products?";
$sol_keywords = "samples, sample, print, a5, example, proof, example, examples";
$sol_categories = "[cat=overview]";

$sol_content = <<<ENDCONTENT
<p>Sure, we'd be delighted to post some A5 print samples to your nominated address to demonstrate the print quality of our products. %%vendor_samples%%</p>
<p>If you'd like us to post some A5 print samples to you, please send us an email with your name and mailing address to 'service@albumworks.com.au' and we'll arrange it.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>