<?php

$sol_title = "What happens if I'm not happy with my product?";
$sol_keywords = "fault, faulty, damage, delivery, broken, torn, damaged, order, refund, reprint, return, policy, unhappy, issue, production, quality, complain, complaint";
$sol_categories = "[cat=ordering / payment][cat=overview]";

$sol_content = <<<ENDCONTENT
<p>While we do everything we can to ensure your product is of the very high standard we expect, there are rare circumstances where this doesn't occur.</p>
<p>If you're not happy with your product, please contact us via email and we'll do everything we can to ensure you receive a product you're happy with.</p>
<p>If it's the quality of the product you're not happy with, please return it to us and we'll happily provide you with a full refund.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>