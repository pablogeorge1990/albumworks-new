<?php

$sol_title = "How do I add more pages to my photobook?";
$sol_keywords = "add, pages, page, insert, new, more, extra, additional, create";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
##version_add_pages##
<p>For clarity, a page is considered one side of a sheet of paper and are always added in pairs and cannot be added individually.</p>
ENDCONTENT;

?>