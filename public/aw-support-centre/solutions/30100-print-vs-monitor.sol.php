<?php

$sol_title = "Will my project look identical to how it appears on my computer monitor?";
$sol_keywords = "screen, monitor, appearance, appears, look, looks, appear, printing, process, colour, difference, differences, print, calibrate, RGB, CMYK";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>When using a printing process, the photos are never going to be printed 100% exactly the same as the version you see on your computer monitor. This is independent of the printer that is used and is an artifact of printing (not just our own production process).</p>
<p>Because of the difference between the way colours are created on a monitor and when printed, there will always be a slight difference, both in colour representation and brightness levels.</p>
<p>A monitor uses an additive RGB (Red, Green, Blue) process to display images on your screen and a printer uses a subtractive CMYK (Cyan, Magenta, Yellow, Key otherwise known as black) process to print images on paper. Images need to be converted from RGB to CMYK by the printer to be printed and this will always introduce some variations.</p>
<p>It also depends on the settings of your monitor. Many new LCD monitors are shipped with the default settings of the brightness control at a very high level. This may result in a print appearing to be darker than what is on on your monitor.</p>
<p>Although there are some differences, we're confident we can provide high quality Photobooks that customers will be happy with. We calibrate our printers daily before we print to ensure consistent print quality across all our products over time. We have selected a calibration level that best represents skin tones as well as natural colours for landscape type photos.</p>
<p>Customers should be aware that this is a printing process instead of a traditional photography technique that involves a chemical silver-halide process. As such the quality of any printer will never be as high a quality as a chemical silver-halide process.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>