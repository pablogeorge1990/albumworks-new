<?php

$sol_title = "How do I rotate a text box?";
$sol_keywords = "rotate, degrees, 45, 90, free, turn, rotation, sideways, flip, clockwise, text, writing, box, boxes, box";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT

##version_rotate_text##<p>You may then need to re-shape the text box to create a better fit.</p><p> Please note, text boxes cannot be 'freely' rotated in the same way a photo can.
ENDCONTENT;

// variable tags should only be in the global variable file

?>