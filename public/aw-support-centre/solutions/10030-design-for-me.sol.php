<?php

$sol_title = "Can you design my album for me?";
$sol_keywords = "photo, book, photobook, design, create, make";
$sol_categories = "[cat=overview]";

$sol_content = <<<ENDCONTENT
<p>To have your Photobook created, you'll need to design it using our free and easy to use editor.</p>
<p>We provide an end to end production service with our editor giving you all the tools you require to create your own personalised album.</p>
<p>Unfortunately we're unable to provide a design service as well. There are design companies out there that may offer this service, however you'd generally be looking at a couple of hundred dollars to design an album.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>