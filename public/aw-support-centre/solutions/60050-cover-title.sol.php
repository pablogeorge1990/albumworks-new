<?php

$sol_title = "What is a cover title and how do I add it?";
$sol_keywords = "cover title, cover text, cover, title, text, etching, etch, engrave, add, front cover, personalise, personalize";
$sol_categories = "[cat=using the software][cat=products]";

$sol_content = <<<ENDCONTENT
<p>The cover title involves a laser
etching process that places a nice bronze coloured text on the bottom
right corner of the front cover of a hard cover album with a material cover type.</p><p>This
position is determined to be optimal because it is very prominent while
still remaining subtle. The font used for the cover title is Arial.</p><p>The cover title costs an extra $6.95 and allows you to further personalise your album.</p><p>Please
keep in mind that there is a 20 character limit on the cover text of
our albums (NB: a character refers to any text including letters,
numbers, symbols and spaces).</p><p>The option to add a cover title can be found during the ordering process.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>