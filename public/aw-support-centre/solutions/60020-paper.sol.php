<?php

$sol_title = "What type of paper do you use? What is the life span of the paper?";
$sol_keywords = "paper, gsm, quality, gloss, semi-glass, matte, quality, fade, sheet, thick, thickness, page, pages, archival, lifespan, life span, time, sheets, acidfree, acid free, acid";
$sol_categories = "[cat=products]";

$sol_content = <<<ENDCONTENT
<p>Our SD albums are printed on 170 GSM semi-gloss acid-free paper, while our HD albums are printed on a 230 GSM lustre paper stock.</p>
<p>This paper is considered optimal for albums due to their increased thickness and quality while still remaining pliable enough to fulfil the requirements of our albums.</p>
<p>Our Calendars, posters and the covers on our softcover album are all printed on a heavier 350 GSM card and have the same semi-gloss and acid-free characteristics.</p>
<p>Here are some definitions of the terms used to describe the paper:</p>
<ul>
	<li><em>Semi-gloss</em> - defines how 'shiny' the paper is.  A semi-gloss paper gives it that slightly wet look that traditional photos have.</li>
	<li><em>Acid-free</em> - means that the paper has been treated to neutralise it's natural acidity.  Without this the paper would slowly turn yellow over time, similar to an old newspaper.</li>
	<li><em>Lustre</em> - Lustre finish is not as shiny as a semi-gloss, but not as flat (dull) as matte finish paper. Lustre paper has a slightly textured finish.</li>
	
</ul>
<br>
<p>As long your photobook is cared for and it's stored in a cool dry location out of direct sunlight, we'd expect it to last for generations.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>