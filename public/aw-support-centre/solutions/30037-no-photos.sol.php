<?php

$sol_title = "How many photos should I have in my project?";
$sol_keywords = "how many, photos, best, number, optimal, per, page, amount, photo, photographs, photograph";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>The optimal number of photos per page depends on the kind of album you're creating.</p><p>The
more pictures on a page, the 'busier' the page is and someone browsing
your album is less likely to be drawn to any particular photo.
Conversely, the less photos on a page, the bigger you can make them and
the more attractive these photos become.</p><p>The power of our
albums is the capability to spread photos over a large area. This draws
the eye to these photos and provides a significant amount of detail.
You can also supplement these large photos with smaller, less crucial
photos on the opposite page.</p><p>In general, our artistic
customers tend to have less photos per page, more full page photo
spreads, but obviously they don't fit as many photos in their album. On
the flip side, you may not want to be creating an artistic
representation and merely want to include all relevant photos.</p><p>We
suggest customers should experiment with different styles using full
page spreads, photos as backgrounds, and smaller photos on one page.
See what looks good for the type of album you're creating.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>