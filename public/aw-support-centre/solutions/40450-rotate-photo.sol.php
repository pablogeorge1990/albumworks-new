<?php

$sol_title = "How do I rotate a photo?";
$sol_keywords = "rotate, degrees, 45, 90, free, turn, rotation, sideways, flip, clockwise, image, photo, photos";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
##version_rotate_photo##
ENDCONTENT;

// variable tags should only be in the global variable file

?>