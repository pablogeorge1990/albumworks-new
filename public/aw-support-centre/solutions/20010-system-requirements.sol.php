<?php

$sol_title = "What are the System Requirements to use your editor?";
$sol_keywords = "editor, system, requirements, ,requirement, require, windows, version, internet, XP, vista";
$sol_categories = "[cat=system requirements]";

$sol_content = <<<ENDCONTENT
<p>The minimum system requirements to use our editor are:</p>
<ul>
	<li>Mac OSX 10.4 or higher; Windows XP, Windows Vista or Windows 7</li>
	<li>at or above a 2.66 GHz processor</li>
	<li>2 GB RAM</li>
	<li>2 GB free hard disk space</li>
	<li>An active internet connection</li>
	</ul>
We recommend a fast broadband connection, and when uploading your project to our servers we recommend against a wireless, satellite or Dial-up connection as these connection types can cause uploading issues.

ENDCONTENT;

// variable tags should only be in the global variable file

?>