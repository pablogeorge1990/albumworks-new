<?php

$sol_title = "What is the print area of a page?";
$sol_keywords = "print area, page area, area, page, border, spine, trim, trimming edge, edge, binding edge, binding, bind, trim edge";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>You can place photos anywhere on the page you like, and it is a great effect to stretch an image across a whole page, or if it's big enough, across a double page. However there are a few things that you should note.</p>
<p>Due to the nature of printing and trimming, the trim area around a page cannot be defined to within millimetre accuracy. On a page, ##version_print_area## is the trim area and the trimming can occur anywhere within this area. To ensure your photos are spread to page edge correctly, please make sure your photos are dragged slightly beyond the outside edge of the ##version_print_area1##. This will ensure there are no white edges when the pages of your album are trimmed.</p>
<p>The best way to make sure this is done correctly is to use the full page template which can be found within the ##version_print_area2##.</p>
<p>We also recommend making sure there is nothing <em>important</em> within 1cm of the edge of a page. If text or an important part of a photo (such as a person's face) is placed at the page edge, there is a real possibility that trimming inaccuracies may result in some portion of the text or photo being trimmed.</p>
<p>This is the same with the binding edge or spine. Nothing <em>important</em> should be placed on or within 2cm of the binding edge between two pages. As mentioned above, a full page image should be spread right into the spine, but nothing important, such as a person or face, should be in the spine.</p>
<p>This is the same with a two-page spread - an image can be spread across one page, through the spine, but care should be taken that the part of the photo <em>in</em> or near the spine should not be of significance. If these guidelines are not followed, the significant piece of the photo across/in the spine may be lost into the spine/binding edge during the binding stage of the production process.</p>
<p><strong>Photocovers</strong></p>
<p>A similar principal applies for designing your photocover. Thes spine of the photocover will be on the outside, so you will be able to see it, but it still cannot be applied with millimeter accuracy.</p>
<p>##version_print_area3## around the photocover is actually folded over the cover and sits on the inside of the cover (similar to how the covers of some children's books are). If you place a person, face, text or even any other significant part of a photo on or within 1 cm of this line, it may be folded over on the inside of the cover.</p>
<p>##version_print_area4##</p>
<p>It is also generally considered good design to keep a 'fair' distance between your content and the page edge.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>