<?php

$sol_title = "Why am I getting a 'Missing Photos' warning?";
$sol_keywords = "missing photos, missing, exclamation, blurry, add photos, where are my photos, what does missing photos mean";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
<p>If you're receiving a warning advising of missing photos, or photos that you have added before now have a red exclamation mark in the middle of them, then this means that the editor is unable to locate the photos.  </p>
<p>You will still be able to see a thumbnail version of the photos though, to assist you in re-adding the correct photos to the correct spot.</p>
<p>The most common cause of this is if your photos are saved on USB/CD or to an external hard drive.</p>
<p>As these are removable sources of media, the editor is unable to successfully locate photos stored on them.</p>
<p>To avoid this happening, we would recommend to save all the photos you wish to use directly to the computer.</p>
<p>Another cause of this is if you have moved or renamed the photos from the original location where you took them from - as they are no longer there, the file path that the editor has remembered is no longer valid.</p>
<p>To add your photos back into your project, when you open it a window will pop-up saying "The following pictures are missing...". Click on the 'Find Pictures' button.</p>
<p>The editor will then continue with a "Find missing pictures.. " window, which has the pictures listed there. Click on one of the pictures and click the 'Update' button. <br>
<p>A window should come up to allow you to choose the photo folder (you will not be able to copy them straight off the CD, so please make sure you copy the photo folder to an easy to find location, such as 'My Documents' or the Desktop). </p>
<p>Once you've located the photo that you were intending to update, select the photo and click 'Select'. <br>
<p>After a moment, the editor should recognise that the other 'missing' pictures are located in the picture folder (if they are all located in the same folder) and will ask you if you would like to update the other missing photos as well. Click 'Yes' and every photo should now be updated and you should be able to click on the 'Continue' button to proceed to editing your project.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>