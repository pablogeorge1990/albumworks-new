<?php

$sol_title = "%%vendor_freezes_startup_title%%";
$sol_keywords = "%%vendor_freezes_startup_keywords%%";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
%%vendor_freezes_startup_content%%
ENDCONTENT;

// variable tags should only be in the global variable file

?>