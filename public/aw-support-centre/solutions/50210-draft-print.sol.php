<?php

$sol_title = "Can I request a draft print of my project before ordering?";
$sol_keywords = "draft print, draft, test print, test, sample print, preview print, own printer, pdf, PDF, check, proof, compare, confirmation, approval, review, approve";
$sol_categories = "[cat=using the software][cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
<p>We understand that you'd like to see a draft of your product before you upload to ensure all your visual effects are acceptable.</p><p>We're deeply sorry but the software is currently unable to provide this service.</p><p>As
always, we recommend customers should thoroughly review their project
via the 'Preview' function to ensure correct and accurate production of
their project.</p><p>We stand by our product and if the final
product isn't up to your desired standard, you can contact us and send
your product back for a full refund.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>