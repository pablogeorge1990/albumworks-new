<?php

$sol_title = "Can I request quicker production of my order?";
$sol_keywords = "delivery, how long, time, frame, deliver, dispatch, print, produce, when, deadline, receive, guarantee, guaranteed, arrive, ready, faster, quicker, rush, priority, pay, express, service, sooner, soon, urgent";
$sol_categories = "[cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
<p>We already produce and deliver
our products as quickly as we're able to and due to the automated
system in place, there is no process we can throw extra resources at to
speed this up.</p><p>Over %%vendor_percentage_within_delivery_timeframe%%% of orders go through our system as
per a best case scenario, however there are a number of very real
variables that are out of control that may affect this.</p><p>As
such, we're unable to place any guarantees on our delivery period,
however we will do everything reasonably within our power to ensure
swift production of all our products.</p><p>We'd normally expect to dispatch an item within %%vendor_number_days_timeframe%% working days.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>