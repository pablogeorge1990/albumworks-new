<?php

$sol_title = "Can I change the type or size of my album once I have started?";
$sol_keywords = "change size, increase, decrease, softcover, hardcover, change, bigger, smaller, size, alter";
$sol_categories = "[cat=using the software][cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
<p>Unfortunately the editor does not support changing album types or sizes after the creation has begun.</p>
<p>We would suggest that if you wish to change album types, to create proofs of each page of your album so that you can refer to the original design as you create the new album type.</p>

<p>To create proofs:</p>
<ol>
	<li>Click on the 'File' menu then select 'Create proofs' from the drop down list.</li>
	<li>On the 'Flight check' screen click 'OK'</li>
	<li>Select the format you want to save the Proofs in; if you would like all pages saved, or just some selected pages; and the destination on your computer for them to be saved to </li>
	<li>Click 'OK'</li>
</ol>
<p>The proofs will then be created in the selected location so that you can open and refer to them while working on your new project at the same time.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>