<?php

$sol_title = "My upload did not complete. How do I start it again without re-ordering?";
$sol_keywords = "save, upload, not complete, upload didn't work, trouble uploading, restart upload, re-order";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
<p>If your upload has not completed, you can start it again by opening the project on your computer and click the 'Order' button again.</p>
<p>The editor will recognise that the order details have been completed, so you will not be prompted to enter these details again. The editor will instead proceed to upload the project to our servers.</p>
<p>When you're in the uploading process, please also ensure that you choose the option to 'Upload finished pages' instead of 'Save finished pages'. Please also allow for the upload process to finish before you close down the editor.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>