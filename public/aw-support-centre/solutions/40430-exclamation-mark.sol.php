<?php

$sol_title = "%%vendor_exclamation_mark_title%%";
$sol_keywords = "exclamation mark, exclamation, mark, low, resolution, red, yellow, marks, image size, image, size, quality";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>The editor will provide a warning when you spread any photo over too large an area, or if the photo you are trying to use is of a low resolution.</p>
##version_low_resolution##
<p>If you have a photo with this exclamation mark, it will turn out grainy and pixelated if printed.</p>
<p>We highly recommend either reducing the photo to a smaller size or replacing the photo with a higher quality photo.</p>
<p>If printed, the exclamation marks won't appear in your album.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>