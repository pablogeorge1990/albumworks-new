<?php

$sol_title = "How are your products delivered?";
$sol_keywords = "deliver, method, delivery, delivered, packaged, sent, post, posted, mail, mailed, dispatch, dispatched, despatched, despatch";
$sol_categories = "[cat=delivery]";

$sol_content = <<<ENDCONTENT
<p>We deliver all our hard cover albums via Toll Priority's overnight delivery.<br />This means that if you live within an Australia Post next day delivery zone, you should expect to receive your album the day after it's dispatched.</p>
<p>We deliver our smaller products via Australia Post which may take anywhere from 1-5 working days for your item to arrive, depending on location and the performance of Australia Post.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>