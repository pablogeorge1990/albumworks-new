<?php

$sol_title = "Do I have to complete my project in one sitting? Can I save my project and come back to it later?";
$sol_keywords = "one, sitting, save, return, saving, come back, go, complete, submit, change, modify, saved";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>The %%vendor_editor%% editor allows you to save your project and return to edit it as many times as you like, in the same way you would with a document or spreadsheet.</p><p>It's not necessary to complete your project in one sitting.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>