<?php

$sol_title = "I have a Dial-Up or Satellite internet connection. Can I still use your services?";
$sol_keywords = "internet, dial, up, dial-up, dialup, broadband, web, satellite, wireless, unwired";
$sol_categories = "[cat=system requirements]";

$sol_content = <<<ENDCONTENT
<p>In theory, any internet connection should be sufficient to upload your project to our servers. Unfortunately in practice this isn't always the case.</p>
<p>Dial-Up, Satellite and Wireless based internet connections are notoriously unreliable, and while you may not notice this in general web browsing, there can sometimes be issues sustaining a large file upload.</p>
<p>We'd highly recommend using an ADSL, ADSL2+ or Cable broadband internet connection when uploading to our servers.</p>
<p>It may take a 'fair' while to upload an album on a Dial-up or sattelite connection, but it should work.<br />If you do experience difficulties when uploading via a Dial-Up or Satellite connection, please contact us and we'll help resolve this with you.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>