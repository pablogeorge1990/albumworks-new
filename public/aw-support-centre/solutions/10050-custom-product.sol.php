<?php

$sol_title = "Can you produce a product not on your product list?";
$sol_keywords = "custom, product, different, products";
$sol_categories = "[cat=overview]";

$sol_content = <<<ENDCONTENT
<p>Unfortunately we don't have the ability to produce custom products.</p>
<p>All of the products we produce <em>must</em> be created and uploaded from within our editor, meaning that you can only use products within our product range and as specified by our product range.</p>
<p>For a full listing of all our products, please visit the products page.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>