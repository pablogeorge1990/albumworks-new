<?php

$sol_title = "Can I create a dust cover/jacket for my album?";
$sol_keywords = "dust, jacket, cover, custom, made, create, service, sleeve";
$sol_categories = "[cat=products]";

$sol_content = <<<ENDCONTENT
We don't support dust covers, however we have a similar product called a Photocover which you can insert into your project.</p><p>The Photocover is a a printed page which is bonded to the front and back cover of the album and replaces the regular cover options. The Photocover allows you to insert your own photos and text and design your cover (front and back) in the same way as any other page in your album. It is similar to a dust cover but is physically bonded to the cover rather than loose.</p><p>Within our editor, the left hand side is the back cover and the right hand side is the front cover.</p><p>
##version_dustcover##
ENDCONTENT;

// variable tags should only be in the global variable file

?>