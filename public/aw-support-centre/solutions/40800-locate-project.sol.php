<?php

$sol_title = "%%vendor_locate_project_title%%";
$sol_keywords = "locate, find, lost, where, open, save, saved, return to, old, search, load, existing, stored";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>The editor allows you to save a project during creation and come back at a later date to complete or edit it again, in the same way you would with any other document.</p>
<p>All projects are stored in a %%vendor_folder%% within your Documents folder (Vista/7) or My Documents folder (XP). </p>
<p>The  editor does not delete your project, so if you're unable to find it here we suggest looking in your Recycle Bin.
ENDCONTENT;

// variable tags should only be in the global variable file

?>