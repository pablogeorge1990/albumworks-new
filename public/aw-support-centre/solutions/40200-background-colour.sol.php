<?php

$sol_title = "How do I set a background colour?";
$sol_keywords = "background colour, background color, background, colour, color, set, change, choose, apply, contrast, plain, backgrounds, back, ground";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
##version_background_colour##
When choosing a background colour, we recommend going for a pastel or 'dirty' colour.<br/>
When printing, the background colours generally appear a lot more vibrant than it looks on a computer monitor. This can take the focus off the photos, which is obviously an undesirable effect. Choosing pastel or dirty colours will provide a more subtle background for your photos.
</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>