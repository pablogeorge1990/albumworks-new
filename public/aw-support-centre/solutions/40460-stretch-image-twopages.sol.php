<?php

$sol_title = "Can I stretch a photo across two pages in an album?";
$sol_keywords = "stretch, spread, two, pages, 2, across, over, both, photo";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
##version_image_twopages##
ENDCONTENT;

// variable tags should only be in the global variable file

?>