<?php

$sol_title = "What binding method do you use to create your albums?";
$sol_keywords = "binding, created, create, process, creation, bound, method, stitch, stitching, sewing";
$sol_categories = "[cat=products]";

$sol_content = <<<ENDCONTENT
<p>Our albums use a binding method called perfect binding.</p><p>The pages of an album are glued together with the latest technology using a glue called PUR glue.  This form of glue is exceptionally strong and ensures your album will last for generations.</p>
<p>By gluing your album instead of sewing it together, we ensure it lays as flat as possible and none of your beautiful photos are lost in the binding.</p>
<p>We're currently the only Photobook producer in Australia to use the superior PUR glue to bind our albums.</p>
##version_binding##

ENDCONTENT;

// variable tags should only be in the global variable file

?>