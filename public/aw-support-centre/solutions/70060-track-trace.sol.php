<?php

$sol_title = "Can I track and trace the delivery of my order?";
$sol_keywords = "deliver, delivery, delivered, track, trace, consignment, note, con, arrive, arrived, arrived, collect, locate, where, album, package, parcel";
$sol_categories = "[cat=delivery][cat=overview]";

$sol_content = <<<ENDCONTENT
<p>We deliver our hard cover albums via Toll Priority's Overnight courier service which enables us to track the delivery progress of your order.</p>
<p>You can locate the consignment note for the delivery of your order in the 'Track My Order' page. It takes a while for the consignment note to be populated in our database, so please check the 'Track My Order' page on the day following the dispatch of your album.</p>
<p>Our smaller products are sent via Australia Post which unfortunately means we're unable to trace the delivery of these products.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>