<?php

$sol_title = "What products are available and how much do they cost?";
$sol_keywords = "product, products, price, calendar, album, hardcover, softcover, cost, range, dimensions, size, choice, choose, available, offer, diary, diaries, poster, photoflip, calendar";
$sol_categories = "[cat=product][cat=overview]";

$sol_content = <<<ENDCONTENT
<p>%%vendor_productinstructions%%</p><p>The prices of all our products include GST.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>