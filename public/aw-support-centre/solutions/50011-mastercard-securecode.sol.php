<?php

$sol_title = "%%vendor_mastercard_securecode_title%%";
$sol_keywords = "%%vendor_mastercard_securecode_keywords%%";
$sol_categories = "[cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
%%vendor_mastercard_securecode_content%%
ENDCONTENT;

// variable tags should only be in the global variable file

?>