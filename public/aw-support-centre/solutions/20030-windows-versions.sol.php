<?php

$sol_title = "What versions of Windows does the editor work on?";
$sol_keywords = "version, windows, XP, Vista, ME, 98, 95, 2000, NT, 2003, OS, old, older, win, 7, Windows 7, Windows7";
$sol_categories = "[cat=system requirements]";

$sol_content = <<<ENDCONTENT
<p>Our editor has been tested and works on the Windows XP, Windows Vista and Windows 7 Operating Systems.<br />There's evidence that the editor works on older versions of Windows (such as 98, 2000, ME, NT etc..), however it has only been tested on Windows XP, Windows Vista and Windows 7 and we only offer support for these three versions of Windows.</p>
 
ENDCONTENT;

// variable tags should only be in the global variable file

?>