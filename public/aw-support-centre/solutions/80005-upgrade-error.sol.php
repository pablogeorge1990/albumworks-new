<?php

$sol_title = "%%vendor_upgrade_failed_title%%";
$sol_keywords = "%%vendor_upgrade_failed_keywords%%";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
%%vendor_upgrade_failed_content%%
ENDCONTENT;

// variable tags should only be in the global variable file

?>