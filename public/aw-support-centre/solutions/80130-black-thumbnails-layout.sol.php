<?php

$sol_title = "%%vendor_black_thumbnails_title%%";
$sol_keywords = "%%vendor_black_thumbnails_keywords%%";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
%%vendor_black_thumbnails_content%%
ENDCONTENT;

// variable tags should only be in the global variable file

?>