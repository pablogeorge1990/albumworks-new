<?php

$sol_title = "What font size is best for my project?";
$sol_keywords = "size, font, text, big, contrast, writing, small, fonts";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>The optimal text size varies depending on what background is being used.
</p><p>We suggest you use stronger fonts such as Arial, Verdana or Tahoma
as opposed to serifed fonts (fonts with small tails contained on the
end of characters) so they don't fade into coloured or transparent
backgrounds. You should only use serifed fonts at larger sizes (14 point
or above, preferably with bold) when dealing with coloured or
transparent backgrounds.
</p><p>10 point is fine on a white or very light coloured background, but
if the background varies from this then we suggest as a minimum going
to a 10 point bold or if space permits, then preferably 12 point.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>