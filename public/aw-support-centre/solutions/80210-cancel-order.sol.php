<?php

$sol_title = "Can I alter or cancel my order?";
$sol_keywords = "cancel, stop, delete, mistake, error, fix, alter, make changes";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
<p>Due to the automated system in place and our batch printing production method, we're not able to alter or cancel an order once it has been placed.</p>
<p>We do not have edit access to our customers projects to be able to make any changes, which is why you are asked with the Terms and Conditions if you have checked your order and wish to continue.</p>
<p>With regards to cancellation, when an order is placed it is immediately and automatically submitted to production and combined with other orders to create a batch. In cancelling any given order we'd need to cancel the whole batch which we obviously cannot do.</p>
<p>It is this level of automation and integration that provides us with an efficient process allowing us to create quality products at the current prices. Without this automation, our costs and therefore product prices would be significantly higher.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>