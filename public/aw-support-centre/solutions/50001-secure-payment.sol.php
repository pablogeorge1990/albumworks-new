<?php

$sol_title = "What measures do you use to keep my credit card details secure?";
$sol_keywords = "credit, card, payment, security, secure, details, privacy, encryption, SSL, bank, internet, steal, banking, pay, payments, cc, credit card";
$sol_categories = "[cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
<p>We have two levels of security in place:</p><p>Firstly, we employ 128bit SSL encryption between the %%vendor_editor%% editor and our back end systems. The %%vendor_editor%% editor is used to design
and order your product, a separate process is then invoked for the payments
process. The information passed during the ordering process is your contact
details, the address you would like products delivered to and which product you
are ordering. All this information is encrypted using SSL.</p>
##version_secure_payment##
ENDCONTENT;

// variable tags should only be in the global variable file

?>