<?php

$sol_title = "Can I order multiple copies in a single order?";
$sol_keywords = "multiple, copies, payment, together, single, save, bulk, more, copy, print";
$sol_categories = "[cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
<p>You can order multiple copies of the same product on a single payment.</p> 
##version_order_multiple_copies##
<p>However, we're unable to merge separate products onto a single payment - because they are different products, they must be ordered and paid for separately.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>