<?php

$sol_title = "How do I move pages within my project?";
$sol_keywords = "page, pages, middle, move, shift, swap, location";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<div class="faqsol_sol_body">
##version_move_pages##
<p>Please note: Pages must be moved in pairs and cannot be moved individually.</p>
</div>
ENDCONTENT;

// variable tags should only be in the global variable file

?>