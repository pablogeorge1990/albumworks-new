<?php

$sol_title = "How do I spread a photo over an entire page?";
$sol_keywords = "full, cover, spread, drag, edge, entire, covered, bleed, page, photo";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>To ensure a full page spread is placed correctly on the page, we recommend using the full page template on the page as this will ensure your photo is spread correctly to page edge. If a photo isn't spread to page edge correctly, there is the potential for your printed page having white edges.</p>
##version_full_page##
<p>Once you've applied this template to your page, simply drag and drop a photo onto the template and it will size it correctly.</p>
##version_full_page1##
ENDCONTENT;

// variable tags should only be in the global variable file

?>