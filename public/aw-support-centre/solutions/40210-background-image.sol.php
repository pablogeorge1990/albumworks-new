<?php

$sol_title = "How do I insert my own image as a background?";
$sol_keywords = "background, back, ground, photo, image, own, custom, use, set, apply, insert, transparent, transparency, backgrounds";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
##version_use_image_background##
ENDCONTENT;

// variable tags should only be in the global variable file

?>