<?php

$sol_title = "%%vendor_vbv_title%%";
$sol_keywords = "%%vendor_vbv_keywords%%";
$sol_categories = "[cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
%%vendor_vbv_content%%
ENDCONTENT;

// variable tags should only be in the global variable file

?>