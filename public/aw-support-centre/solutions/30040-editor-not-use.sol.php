<?php

$sol_title = "Do I have to use your editor? Can I use another program instead?";
$sol_keywords = "editor, require, have to, other program, iphoto, indesign, design, pdf, publisher, word, photoshop, fireworks, dreamweaver";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>To create a Photobook product through us, the %%vendor_editor%% editor must be used.</p>
<p>The production process we use is completely automated from start to finish. To get a file into the correct format that our system can understand, it must be uploaded to our servers via our editor as there are various formatting steps required.</p>
<p>Unfortunately we're unable to receive a file any other way.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>