<?php

$sol_title = "Why am I receiving an 'empty page' warning?";
$sol_keywords = "empty, blank, empty page, blank page, empty pages, blank pages, page, warning, error, not, message, spread, background, warnings, backgrounds, errors, messages";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
##version_empty_page##
<p>Please note, due to our highly automated system and variable page allowance it is impossible for our production system to recognise blank pages that are unwanted.  Therefore if you have any blank pages in your project at the time of upload, please be aware that they will be bound into your album.</p>
<p>You can delete unwanted pages by selecting them, then either 'Page' from the top menu then 'Delete', or 'Ctrl-D' on your keyboard. Pages are only able to be deleted in pairs also.</p>
<p>If you are unable to delete pages, then it is likely you have reached our binding minimum for the album you have created.  The number of pages we allow in our albums is based on the binding process we use. Any more less pages will result in a less than satisfactory binding of your album that we are unwilling to provide.</p>  
ENDCONTENT;

// variable tags should only be in the global variable file

?>