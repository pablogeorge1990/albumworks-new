<?php

$sol_title = "Do I need an internet connection?";
$sol_keywords = "internet, need, require, have, access, connected, connection, connect, online";
$sol_categories = "[cat=system requirements]";

$sol_content = <<<ENDCONTENT
<p>Yes, in order to use our editor you will need an active internet connection.</p>
<p>We recommend a fast broadband connection, and when uploading your project to our servers we recommend against a wireless, satellite or Dial-up connection as these connection types can cause uploading issues.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>