<?php

$sol_title = "Can I increase the quality of my photos?";
$sol_keywords = "increase, quality, photo, resolution, megapixel, too, low, bigger, larger, improve, modify";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>It's not possible to increase the quality of your photos as the resolution of each photo is determined by the camera used to take the photo. As such, there's no process that you can apply to photos you've already taken to improve their quality.</p>
<p>If you received the photos in an email from another person, it's possible they may have sent you a lower resolution version. In this case it might be possible to obtain the original photos (which are greater in size and quality). Similarly, if you've pre-processed your photos to a smaller size, it would be worth sourcing the original photos directly from the camera.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>