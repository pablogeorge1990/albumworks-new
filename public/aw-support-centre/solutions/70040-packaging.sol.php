<?php

$sol_title = "How are your products packaged?";
$sol_keywords = "deliver, method, package, packaging, packaged, safe, protection, protected";
$sol_categories = "[cat=delivery]";

$sol_content = <<<ENDCONTENT
<p>We package our hardcover albums in secure cardboard packaging that contain significant buffers to protect your album from damage during transit.</p>
<p>We've found this packaging method to be extremely resilient with next to no complaints surrounding a damage in delivery item.</p>
<p>Our other products are packaged using the appropriate packaging, such as poster tubes and stiff board packaging, which again we've found to provide excellent protection for our products.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>