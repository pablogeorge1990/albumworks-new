<?php

$sol_title = "Can I supply you with a PDF, iPhoto or any other file to make into an album?";
$sol_keywords = "pdf, iphoto, supply, doc, word, publisher, document, print ready, iPhoto";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>Unfortunately we can only accept orders that are uploaded via the %%vendor_editor%% editor.</p>
<p>The system we use is a highly automated system from start to finish. When you upload your order all its associated details are submitted into the system via a specialised file that integrates with our back-end database. It is the format of this file that allows a smooth and efficient process giving us the opportunity to provide such quality products at the current price.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>