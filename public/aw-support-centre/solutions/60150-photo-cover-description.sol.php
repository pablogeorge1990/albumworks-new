<?php

$sol_title = "What is a Photocover? How do I add/remove it to/from my album?";
$sol_keywords = "photo cover, photocover, what is, print, cover, design, customise, customize, dust";
$sol_categories = "[cat=using the software][cat=products]";

$sol_content = <<<ENDCONTENT
<p>The Photocover is a printed page which is bonded to the front and back cover of the album and replaces the regular cover options. The Photocover allows you to insert your own photos and text and design your cover (front and back) in the same way as any other page in your album. It is similar to a dust cover but is physically bonded to the cover rather than loose.</p>
<p>Within our editor, the left hand side is the back cover and the right hand side is the front cover.</p>
##version_photocover_description##
ENDCONTENT;

// variable tags should only be in the global variable file

?>