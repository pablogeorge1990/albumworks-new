<?php

$sol_title = "What are photobooks?  How do I create my own?";
$sol_keywords = "photo, book, photobook, start, create, photobooks";
$sol_categories = "[cat=overview]";

$sol_content = <<<ENDCONTENT
<p>Photobooks are a relatively new product category that provide customers with the ability to create their own coffee table style photo albums.<br />Photobooks are different to traditional photo albums in that each photo is printed directly onto the page instead of numerous individual photos being stuck to a page, making for a much slimmer album that is a lot easier to share.&nbsp; No more thick heavy photo albums with individual photos of a set size being added 3 to a page.<br />Now you can design each page exactly as you'd like it,  adding text, background colours and photos, which can be easily resized resulting in an album that lets you tell your own story in your own unique way.</p>
<p>To create your own %%vendor%% Photobook, simply download and install the %%vendor_editor%% editor from our website and start creating your own Photobook.<br />The editor is FREE and easy to use and our Support Centre has nearly every question imagineable.<br />If you're unable to find answers to your questions, our fast and friendly staff are just an email away.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>