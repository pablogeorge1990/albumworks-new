<?php

$sol_title = "How many pages can I have?";
$sol_keywords = "add, pages, page, insert, new, more, extra, remove, delete, maximum, minimum, less, more, additional, create, empty, blank";
$sol_categories = "[cat=using the software][cat=products]";

$sol_content = <<<ENDCONTENT
<p>Our A3, A4 and 30x30 sized albums have a default of 40 pages, but you can remove 10 pages to the binding minimum of 30 for these sizes.</p>
<p>The 21x21, 15x15 and A5 sized albums have a default and minimum of 20 pages.</p>
<p>To delete any unwanted pages to the minimum, you need to make sure you are on a double page spread, then select 'Page' then 'Delete'.<br>Please note that pages need to be deleted in pairs and cannot be deleted individually.</p>
<p>For the maximum number of pages, you can have up to 200 pages in all albums, except for the A3 sizes which have a maximum of 90 pages.</p>
<p>The number of pages we allow in our albums is based on the binding process we use. Any more or less pages will result in a less than satisfactory binding of your album that we are unwilling to provide.</p>
<p>For clarity, a page is considered one side of a sheet of paper and are always added/deleted in pairs and cannot be added/deleted individually.</p>
ENDCONTENT;

?>