<?php

$sol_title = "How long will it take to produce and deliver my order?";
$sol_keywords = "delivery, how long, time, frame, deliver, dispatch, print, produce, when, deadline, receive, guarantee, guaranteed, arrive, ready, turn around, turn around";
$sol_categories = "[cat=ordering / payment][cat=delivery][cat=overview]";

$sol_content = <<<ENDCONTENT
<p>Our service level is to dispatch within 7 business days from upload to completion, however there is a chance that it will be completed and dispatched a bit quicker than this.</p>
<p>We always recommend customers allow the full service level to ensure they receive their product in time though, as quicker production cannot be guaranteed.<br />
We produce all our products as quickly as we can, so unfortunately there isn't anything extra we can do to speed this up (even for a fee).</p>
<p>To provide some additional information, there are many factors that contribute to the amount of time it takes to produce our products, so unfortunately we're unable to be more definitive about exactly how long it will take and can only provide estimates.<br />
There can be up to a dozen different machines used for a single item, and while Quality Assurance issues are rare, they do occur (just like in any production system) so we always recommend allowing the full %%vendor_dispatch%% business days for the production of your order.</p>
<p>In regards to delivery of your order, our larger albums (A4 and larger) are delivered via Toll Priority's Overnight service, while we offer delivery via either Australia Post or Toll Priority for our smaller items.</p>
<p>If your item is being sent via Toll Priority, and if you're within an Australia Post next day delivery zone you should expect to receive your album the next working day or two after it's dispatched (Australian Air Express don't provide a guarantee on this though).</p>
<p>To see a list of postcodes that are within the next day delivery zone, please see the <a href="http://auspost.com.au/personal/next-business-day-delivery-networks.html" rel="external">Australia Post</a> website.</p>
<p>If your item is being sent via regular Australia Post mail, it may take 1-5 working days to arrive.</p>

<p>You can also track your order online via our <a href="http://www.albumworks.com.au/track" rel="external">Track My Order</a> page, by entering your email address and AlbumID.<br>



ENDCONTENT;

// variable tags should only be in the global variable file

?>