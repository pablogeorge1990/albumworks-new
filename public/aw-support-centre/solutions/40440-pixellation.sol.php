<?php

$sol_title = "Why do my photos look pixellated in the work area of the editor?";
$sol_keywords = "pixellated, pixels, pixellation, pixelated, pixelate, pixelation, pixellate, pixel, grainy, blurry, resolution, low, poor, quality, image, images";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>Due to the large amount of data involved in creating your product, our editor needs to streamline some of its processes to ensure the software runs smoothly on your computer.</p>
<p>To achieve this, the editor creates a thumbnail (a smaller version of your photo) for each photo that it uses on the main editing page of your product. This reduces the amount of data required for your computer to process when editing your product.</p>
<p>Please rest assured that the editor will use the full resolution version of your photos in the final product.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>