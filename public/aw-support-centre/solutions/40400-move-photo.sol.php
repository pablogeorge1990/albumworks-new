<?php

$sol_title = "How do I move a photo between pages?";
$sol_keywords = "move, cut, copy, paste, photo, between, pages, page, photos";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>To move a photo between adjacent pages, you should be able to just drag it across.  If you'd like to move a photo between non-adjacent pages, you'll need to cut-and-paste the photo from one page to another.</p>
<p>To do this:</p>
	<ol>
		<li>Right-click on the photo you wish to move.</li>
		<li>Select 'Cut'.</li>
		<li>Find the page you want the photo moved to and right-click on this page.</li>
		<li>Select 'Paste'.</li>
	</ol>
<p>This will move the photo to the desired page and it will retain its size and shape. You can easily reposition the photo by simply clicking on it and dragging to the desired location.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>