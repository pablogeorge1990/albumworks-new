<?php

$sol_title = "Can I use unusual fonts in my project?";
$sol_keywords = "fifont, fonts, international, symbol, support, unusual, strange, characters, character, text, writing, style, styles, foreign, font, text, chars, char";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>Our editor can handle many different font types, from strange fonts to international characters.</p><p>Our
editor uses the fonts installed on your computer, so to ensure your
order is produced correctly with the unusual fonts, make sure you
upload your album from the same computer you used to edit it.</p><p>While these fonts work within the pages of your album, unfortunately they don't work for the cover title.</p><p>The
cover title is an optional extra ($6.95) and results in a nice bronze
text of up to 20 characters (including spaces) being laser etched in
the bottom right corner on the front cover of a hard cover album with either a faux leather or linen cover type.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>