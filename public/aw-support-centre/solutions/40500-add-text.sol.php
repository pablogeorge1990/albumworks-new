<?php

$sol_title = "How do I add text to a page?";
$sol_keywords = "add, insert, new, more, extra, additional, create, text, boxes, box, title, titles, paragraph, caption, writing, comment, block, boxs";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>To add text to a page, create a text box by ##version_add_text##.</p>
<p>Once you have inserted a text box onto your page, you can resize it by clicking and dragging the little ##version_add_text2## boxes on the edges of the text box and move it as need be.</p>
<p>To edit the text in the text box, simply double click on the text box to go to the edit screen. In this edit screen you can change the font style, font size, font colour and allignment of the text.</p>
<p>TIP: Make sure the text is a different colour to the page it's being placed on.  To change the text colour, highlight the text, then select the 'Text Colour' icon ##version_add_text3## to select your colour, then click 'OK'.</p>
ENDCONTENT;

?>