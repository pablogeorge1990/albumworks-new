<?php

$sol_title = "%%vendor_giftvouchers_title%%";
$sol_keywords = "%%vendor_giftvouchers_keywords%%";
$sol_categories = "[cat=overview]";

$sol_content = <<<ENDCONTENT
<p>%%vendor_giftvouchers_content%%</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>