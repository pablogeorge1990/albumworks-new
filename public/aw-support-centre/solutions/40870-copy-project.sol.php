<?php

$sol_title = "How do I copy my project to a new location?";
$sol_keywords = "copy project, move project, copy, move, moving, another, backup, transfer, CD, USB, send, between, computers, computer, laptop";
$sol_categories = "[cat=using the software]";

$sol_content = <<<ENDCONTENT
<p>The process of copying your project to a new location depends on which operating system you are using (Windows/Mac).</p>
<p>To copy your project to a new location:</p>
<p><b>1.</b> Go to the following folder on your hard drive:</p>
<p><i>If you're working on a PC:</i>
My Documents\albumworks Projects\</li></p>
<p><i>If you're working on a Mac:</i>
username\albumworks Projects (the user name is the folder with the 'home' icon next to it)</p>
<p>In this folder you will find another folder which contains your project.</p>
<p><b>2.</b> Unfortunately the pictures will not automatically be copied with the project file.</p>
<p>To ensure that the pictures are copied as well, find the pictures that you have used (you can do this easily by right-clicking the picture within the editor and clicking 'Show in Windows Explorer' on a PC and 'Show in Finder' on a Mac) and copy them to another folder.</p>
<p><b>3.</b> Copy the folder containing your project and photos to your desired destination (network location, USB stick, CD). Once you have finished copying your project, check the destination location to make sure you've copied it correctly.</p>
<p><b>4.</b> Please ensure the editor has been installed on the destination computer prior to copying the project to the computer.</p>
<p><i>If you're working on a PC:</i>
Copy the project and photo folders to the albumworks Projects in the 'My Documents' folder of the destination computer.</p>
<p><i>If you're working on a Mac:</i>
Copy the project and photo folders to the albumworks Projects in the 'username' folder of the destination computer.</p>
<p><b>5.</b> To open the project, simply open the editor and click on 'Open an Existing Project'. The project should be listed as one of the existing projects under the same name.</p>
<p>As the pictures are not imported automatically, the editor will ask you to update the new picture location.
A window will pop-up saying "The following pictures are missing...". Click on the 'Find Pictures' button.</p>
<p>The editor will then continue with a "Find missing pictures.. " window, which has the pictures listed there. Click on one of the pictures and click the 'Update' button.
A window should come up to allow you to choose the photo folder (you will not be able to copy them straight off the CD, so please make sure you copy the photo folder to an easy to find location, such as 'My Documents' or the Desktop).</p>
<p>Once you've located the photo that you were intending to update, select the photo and click 'Select'.
After a moment, the editor should recognise that the other 'missing' pictures are located in the picture folder and will ask you if you would like to update the other missing photos as well. Click 'Yes' and every photo should now be updated and you should be able to click on the 'Continue' button to proceed to editing your project.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>