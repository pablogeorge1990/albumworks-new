<?php

$sol_title = "Why am I having trouble uploading? What does 'Cannot Connect to Server' mean?";
$sol_keywords = "firewall, fire wall, exception, upload, security, allow, program, application, cannot connect, server, cannot connect to server, trouble uploading";
$sol_categories = "[cat=using the software][cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
<p>If you are receiving the error message 'Cannot connect to server', or your upload does not commence (or keeps restarting), then this means that you most likely have a firewall restriction that is preventing the editor from uploading your file.</p>
<p>If you're using a work network, most work places have network firewalls in place so we'd recommend contacting your network administrator to see if they can add an exception in the network firewall, otherwise you may need to do this from home.</p>
<p>To resolve this issue, you'll need to add an exception to your firewall to allow our editor to upload your project. You can do this by following the below steps:</p>
<ol>
	<li>Go to START > Control Panel</li>
	<li>Click on "Security Center"</li>
	<li>Click on "Windows Firewall" or "Allow a Program through Windows Firewall"</li>
	<li>Click the "Exceptions" tab, or click the "Change Settings" option</li>
	<li>Highlight "%%vendor_editor%%" and click "OK"</li>
</ol>
<p>This will allow the %%vendor_editor%% editor access to the internet.</p>
<p>If our editor is not showing in the drop down list, you may need to click on 'Add Program' or 'Allow Another Program' and select it from the second drop down list that comes up.</p>
<p>If you use a 3rd party firewall (ie. McAffe, Norton, Avast etc..), please consult their website for instructions on how to create an exception. If you're still unable to get around your firewall, please let us know and we'll try to help as best we can.</p>
<p>If your upload stalls part way through, or keeps stopping and starting, then it may be your firewall kicking in and blocking the upload partway through. Following the above steps should resolve this issue.</p>
<p>If you're uploading wirelessly, we highly recommend directly connecting your computer to your modem and trying again. A wireless internet connection works well for bursty traffic such as web browsing, however for sustained uploads such as uploading your project to our servers, wireless connections can be quite unreliable.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>