<?php

$sol_title = "Why am I receiving an 'empty photo frame' or 'empty text box' warning?";
$sol_keywords = "empty, photo frame, text box, text, frame, page, warning, error, not, message, warnings, messages, errors";
$sol_categories = "[cat=troubleshooting]";

$sol_content = <<<ENDCONTENT
<p>The empty photo frame or empty text box warning is
used by the editor to draw your attention to the fact that there is a
frame or text box in your project that doesn't contain a photo/text. If you can't see
this frame, it's possible you've covered it with other photos or text.</p><p>So
long as you're happy with the affected page as it appears in the work
area and 'Preview' function of the editor, you can safely ignore this
warning and continue with the ordering process. Please rest assured
that empty photo frames or text boxes won't be printed in the final product.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>