<?php

$sol_title = "What cover options do you have available?";
$sol_keywords = "cover, covers, colour, title, linen, faux leather, faux, leather, photo cover, photocover, front cover, cover page";
$sol_categories = "[cat=products]";

$sol_content = <<<ENDCONTENT
<p>For our hard cover albums, the options for cover type and colour are as follows:
				%%vendor_cover_standard_black%%
				##version_covers##
				<br>Faux Leather: Red, Blue, Black
				<br>Linen: Red, Blue, Black, Brown
				</p><p>%%vendor_cover_option_price%%</p>
<p>We also provide a laser etching service that places a nice bronze coloured text on the bottom right corner of the front cover of a hard cover album. This costs an extra $6.95 and is only available on linen and faux leather covers.</p>
##version_covers2##


ENDCONTENT;

// variable tags should only be in the global variable file

?>