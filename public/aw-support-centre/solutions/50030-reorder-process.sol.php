<?php

$sol_title = "I've received my album and would like to order more copies, what should I do?";
$sol_keywords = "copy, copies, order, another, more, get, multiple, additional, again";
$sol_categories = "[cat=ordering / payment][cat=delivery]";

$sol_content = <<<ENDCONTENT
<p>To re-order subsequent copies of an album, simply re-open the same project on your computer and go through the normal ordering and payment procedure again.</p>
<p>Unfortunately it is not currently possible to place a new order from a previous upload due to privacy and technical issues.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>
