<?php

$sol_title = "How do I know if the quality of my photos is high enough to use in my project?";
$sol_keywords = "quality, high, megapixel, MP, good, big, look, minimum, min, resolution, res, pixels, megapixels, camera, settings";
$sol_categories = "[cat=getting started]";

$sol_content = <<<ENDCONTENT
<p>There are many variables which can affect the quality (real or perceived) of the photos that you use in your project, including the camera used to take the photos, camera settings, focus and light factors, compression and so on. As a general guide, the resolution of the photos used is the most common way to judge the quality of photo.</p>
<p>If you have a high megapixel camera (6 megapixel or above), you don't need to worry about the quality of your photos for the purposes of our editor.</p>
##version_photo_quality## of your photo if the resolution of the photo is too low for the area it's covering. If you're receiving this warning, your photos will appear grainy and pixellated if printed and we'd recommend using either a higher quality photo or reducing the size of the photo on the page.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>