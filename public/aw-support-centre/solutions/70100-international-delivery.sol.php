<?php

$sol_title = "Do you deliver outside of Austraila?";
$sol_keywords = "international, overseas, over, seas, country, new zealand, NZ, ship, shipping, deliver, post, send, usa, states, united states, canada, UK";
$sol_categories = "[cat=delivery]";

$sol_content = <<<ENDCONTENT
<div class="faqsol_sol_body"><p>We're deeply sorry, but unfortunately we're unable to organise
international delivery of our products as we're only able to deliver within Australia.</p>
</div>
ENDCONTENT;

// variable tags should only be in the global variable file

?>