<?php

$sol_title = "How are your products printed?";
$sol_keywords = "printing, printed, print, process, ink, colour, liquid, quality, process, type, printer, production, produced, toner, laser, indigo, hp";
$sol_categories = "[cat=products]";

$sol_content = <<<ENDCONTENT
<p>Our products are printed on the latest in digital ink based printing
presses (HP Indigo) to carefully defined production standards using 4
colour electrostatic inks. The presses have a colour gamut which
matches and in some cases exceeds traditional offset printing. </p><p>We believe that only liquid inks place the image in the page and
provide the beautiful tones that you are looking for and that other
digital presses which use toner rather than liquid inks don't produce
products of the quality that we (and you) require.</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>