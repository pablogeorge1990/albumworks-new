<?php

$sol_title = "How do I use my promotion code?";
$sol_keywords = "ordering, promo, code, promotion, 34%, 34, discount, disc, order, ordered, field, use, enter, submit, campaign";
$sol_categories = "[cat=ordering / payment]";

$sol_content = <<<ENDCONTENT
<p>If you've been provided a promotion or voucher code to receive a discount, it is important to ensure you do use it during the ordering process as we're unable to apply a discount once an order has been processed.</p>
##version_promo_code##
ENDCONTENT;

// variable tags should only be in the global variable file

?>