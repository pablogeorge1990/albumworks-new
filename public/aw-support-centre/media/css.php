<?php

if (strstr($_SERVER['REQUEST_URI'],'.php') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

// media/css.php?vendor=(1031|3183|3255)&css_req=(search|solution|browse|contact)_css

switch($_GET['vendor']) {

	case 1031:	############################################
		header('Content-type:text/css');
		ap_default_css();
		switch($_GET['css_req']) {
			case 'search':
				ap_search_css();
                ap_browse_css();
				break;
			case 'solution':
                ap_search_css();
				ap_solution_css();
				break;
			case 'browse':
				ap_browse_css();
				break;
			case 'contact':
				ap_contact_css();
				break;
		}
		break;

	case 3183:	############################################
		header('Content-type:text/css');
		ow_default_css();
		switch($_GET['css_req']) {
			case 'search':
				ow_search_css();
				break;
			case 'solution':
				ow_solution_css();
				break;
			case 'browse':
				ow_browse_css();
				break;
			case 'contact':
				ow_contact_css();
				break;
		}
		break;

	case 3255:	############################################
		header('Content-type:text/css');
		tg_default_css();
		switch($_GET['css_req']) {
			case 'search':
				tg_search_css();
				break;
			case 'solution':
				tg_solution_css();
				break;
			case 'browse':
				tg_browse_css();
				break;
			case 'contact':
				tg_contact_css();
				break;
		}
		break;
	
	default:	############################################
		// bad request
		header('HTTP/1.1 404 Not Found');
		break;

}

################################################

function ap_default_css() {
	echo <<<ENDECHO
h2.bufferbot { margin-bottom:20px }
.smartedit { color:gray; }
.activesmart { color:black; }
input.rem, textarea.rem { border:1px solid gray; padding:2px; font-family: Arial, Helvetica; font-size:12px; }

#content .faqsol_container 			    	{ margin: 45px 0 0 85px; padding: 0; width: 615px; min-height: 540px; height: auto !important; height: 540px; }
body > .faqsol_container 					{ margin: 0; padding: 0; width: 600px; min-height: 540px; background-image: url(/images/support_help.jpg); background-position: 20px 0; background-repeat: no-repeat; background-color: #FFFFFF; }
div > .faqsol_container 					{ margin: 0; padding: 0; width: 600px; min-height: 540px; background-image: url(/images/support_help.jpg); background-position: 20px 0; background-repeat: no-repeat; background-color: #FFFFFF; }
	.faqsol_header							{ display: none; }
		.faqsol_head_title					{ display: none; }
		.faqsol_head_subtitle				{ display: none; font-weight: bold;  color: #666666; }
		.faqsol_head_tabs					{ display: block; float: left; width: 200px;  font-weight: bold; clear: left; }
			.faqsol_head_tabs_divider		{ display: block; float: left; height: 20px; line-height: 20px; color: #6d6d6d; padding-left: 3px; padding-right: 3px; }
			.faqsol_head_tabs_aa			{ }
			.faqsol_head_tabs_an			{ }
			.faqsol_head_tabs_aa:hover		{ }
			.faqsol_head_tabs_an:hover		{ }
			.faqsol_head_tabs_search		{ display: block; float: left; height: 20px; line-height: 20px; color: #F66B06; }
			.faqsol_head_tabs_browse		{ display: block; float: left; height: 20px; line-height: 20px; color: #F66B06; }
			.faqsol_head_tabs_search:hover	{ color: #6d6d6d; font-weight: bold}
			.faqsol_head_tabs_browse:hover	{ color: #6d6d6d; font-weight: bold}
		.faqsol_head_otherlinks							{ display: block; float: right; width: 170px;  font-weight: bold; clear: right; padding-left:20px; background:white; }
			.faqsol_head_otherlinks_divider				{ display: none; }
            .faqsol_head_otherlinks_manual                { display: block; float: right; line-height: 20px; text-indent:-999em; overflow:hidden; width: 170px; height: 301px; background:url(../images/awmanual.jpg) left top no-repeat; }
            .faqsol_head_otherlinks_manual:hover        { background-position: 0 100%; }
			.faqsol_head_otherlinks_blog				{ display: block; float: right; line-height: 20px; margin-top:50px; text-indent:-999em; overflow:hidden; width: 170px; height: 301px; background:url(../images/awblog.jpg) left top no-repeat; }
			.faqsol_head_otherlinks_blog:hover		    { background-position: 0 100%; }
			.faqsol_head_otherlinks_trackorder			{ display: block; float: right; margin-right: 20px; height: 20px; padding-left: 25px; background-image: url(../images/ap_track_mag.gif); background-repeat: no-repeat; line-height: 20px; }
			.faqsol_head_otherlinks_trackorder:hover	{ background-position: 0 100%; }
			
	.clear_both_empty						{ clear: both; }
	
	.form_title_hint						{ color: #999999; }

	.contact_link							{ margin-left: 15px; margin-right: 15px; padding-left: 25px; margin-top: 20px; padding-top: 5px; border-top: 1px solid #EEEEEE; }
			.contact_link_pre				{ color: #666666; display: block; height: 21px; line-height: 21px; float: left; }
			.contact_link_a					{ outline: none; margin-left: 20px; display: block; width: 82px; height: 21px; background-image: url(../images/contactus.png); background-repeat: no-repeat; text-indent: -9999px; float: left; }
			.contact_link_a:hover			{ background-position: 0 100%; }
			.back_link_a					{ outline: none; margin-left: 20px; display: block; width: 62px; height: 21px; background-image: url(../images/back-button.png); background-repeat: no-repeat; text-indent: -9999px; float: left; }
			.back_link_a:hover				{ background-position: 0 100%; }
			.poweredby_image				{ display: none; }

ENDECHO;
}

function ap_search_css() {
	echo <<<ENDECHO
	.faqsol_prioritysol						{ width: 526px; }
		.faqsol_prioritysol_li				{ color: #F66B06; list-style: none; display: inline; margin: 0; padding: 0; text-indent: 0; }
		.faqsol_prioritysol_a				{ color: #F66B06; background-image: url(../images/question-arrow.png); background-repeat: no-repeat; background-position: 2px 3px; display: block; text-decoration: none; margin-left: 20px; padding-left: 20px; margin-bottom: 5px; }
		.faqsol_prioritysol_a:hover			{ color: #6d6d6d; background-position: 2px -37px;}
			.faqsol_prioritysol_title		{ font-weight: bold;  color: #666666; margin-bottom: 15px; }
			.faqsol_topten 					{ width: 500px; float: left;  }
			.faqsol_timely					{ color: #666666; margin-bottom: 15px; }

	.faqsol_searchbar						{ display: block; width: 546px; padding: 20px; padding-left: 40px; padding-right: 40px; margin-bottom:50px; }
	.faqsol_container > .faqsol_searchbar	{ display: block; width: 466px; padding: 0; }
		#faqsol_search_label				{ color: #666666; }
		#faqsol_search_input				{ margin: 10px; margin-left: 0; margin-top: 20px; float: left; }
		#faqsol_search_submit				{ margin: 10px; margin-left: 0; margin-top: 18px; float: left; }
		
	.sol_result_head						{ float:left; }
		.result_head_uppersearchbar			{ display: none; clear: both; width: 526px; text-align: center; }
			#result_head_search_form		{ display: block; width: 300px; margin: 0 auto; }
				#result_head_search_fieldset		{ }
					#result_head_searchupper_input	{ }
					#result_head_searchupper_submit { }
		.sol_result_head_title				{ display: none; color: #F66B06; font-weight: bold;  margin-bottom: 5px; }
		.sol_result_head_desc				{ display: block;  color: #333333; }
	
	.sol_result_body						{ padding-top:15px; clear:left; }
		.faqsol_result_container			{ padding-left: 25px; padding-bottom: 20px;  }
			.faqsol_result_link				{ color: #F66B06; font-weight: bold}
			.faqsol_result_link:hover		{ color: #6d6d6d; }
			.faqsol_result_desc				{ display: block; color: #333333; }
				.faqsol_highlight			{ font-weight: bold; }
			.faqsol_result_cat				{ display: none; }
		.sol_result_nomatch					{ display: block; padding-left: 25px; padding-right: 25px; padding-bottom: 20px;  }

ENDECHO;
}

function ap_solution_css() {
	echo <<<ENDECHO
    .faqsol_searchbar                       { margin-bottom:0px; }
	.solution_uppersearchbar				{ display: block; clear: both; width: 526px; padding-top: 35px; text-align: center; }
		#solution_search_form				{ display: block; width: 300px; margin: 0 auto; }
			#solution_search_fieldset		{ }
				#solution_searchupper_input	{ }
				#solution_searchupper_submit{ }
				
	.faqsol_sol_container					{ margin-top: 15px; padding-bottom: 0; }
		.faqsol_sol_title					{ display: block; color: #F66B06; margin-bottom: 5px;  font-weight: bold }
        .faqsol_sol_content                    { color: #6d6d6d;  }
		.faqsol_sol_content p					{ margin-bottom:1em;  }
ENDECHO;
}

function ap_browse_css() {
	echo <<<ENDECHO
	.sol_browse_head							{ }
		.sol_browse_head_title					{ display: none; }
		.sol_browse_head_desc					{ display: block; width: 466px; clear: both; color: #666666; }
	.sol_browse_body							{ display: block; padding-bottom: 10px; }
		.sol_cat_head							{ margin-top: 5px; }
			.sol_cat_title						{ padding-left: 0px; clear: both; }
#content		.sol_cat_title_link				{ text-decoration: none; font-weight: bold;  color: #F66B06; outline: none; background-image: url(../images/question-arrow.png); background-repeat: no-repeat; padding-left: 15px; background-position: 2px 3px; display: block;}
#content        .sol_cat_title_link:hover		{ text-decoration: none; font-weight: bold;  color: #6d6d6d; outline: none; background-position: 2px -37px;}
#content        .sol_cat_title_link_active		{ text-decoration: none; font-weight: bold;  color: #F66B06; outline: none; background-image: url(../images/question-arrow.png); background-repeat: no-repeat; padding-left: 15px; background-position: 2px -17px; display: block;}
#content        .sol_cat_title_link_active:hover{ text-decoration: none; font-weight: bold;  color: #6d6d6d; outline: none; background-position: 2px -57px;}
			.sol_cat_desc						{ padding-left: 20px;  color: #666666; display: block; padding-top:5px; }
		.sol_cat_body							{ margin: 5px 0 0 0; display:none; }
			.sol_link_li						{ display: inline; list-style: none; margin: 0; padding: 0; text-indent: 0; }
#content		.sol_link						{ font-size:12px; color: #6d6d6d; display: block; text-decoration: none; font-weight: bold; background-image: url(../images/solution-arrow.png); background-repeat: no-repeat; background-position: 20px 2px; padding-left: 35px; padding-bottom: 5px; }
				.sol_link:hover					{ color: #F66B06; background-position: 20px -38px;}
ENDECHO;
}

function ap_contact_css() {
	echo <<<ENDECHO
	.contact_form_container							{ padding: 0; }
		.contact_form_head							{ margin-top: 15px; }
			.contact_form_title						{ display: none; }
			.contact_form_desc						{ margin-top: 20px; margin-bottom: 20px; padding:0;  line-height: 15px; display: block; color: #666666; }
			.contact_form_err						{ background-color: #CCCCCC; margin-top: 15px; margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px;  display: block; color: #F66B06; font-weight: bold; }
		.contact_form_body							{ margin-left: 0; margin-right: 0; padding:0; }
			label									{ float: left; margin-top: 5px; }
			label.wrong								{ border-left: 3px solid #F66B06; }
			.contact_form_label_star				{ color: #F66B06; }
				#container #main .contact_input_divider				{ width: 400px; display: block; clear: both; margin: 0; padding: 0; color: #EEEEEE; border: 0; height: 0; visibility: hidden; }
				#contact_form_name					{ width: 200px; display: block; float: left; margin-right: 5px; }
				#contact_form_lname					{ width: 200px; display: block; float: left; }
				#contact_form_email					{ width: 200px; display: block; float: left; margin-right: 5px; }
				#contact_form_subject				{ width: 411px; display: block; float: left; }
                #contact_form_albumid                { width: 200px; display: block; float: left; }
				.query_wrapper						{ float: left }
				#faqsol_search_input_contact		{ clear: both; float: left; width:411px; margin-top:5px; }
				#contact_form_submit				{ display: block; float: left; clear: both; margin-top: 20px; }
                #ajax_response                       { display: none; float: left; padding-left: 10px; }
				#ajax_response a					{  }
				span.ajax_response					{  }
				ul.ajax_response					{ list-style-type: none; }
				li.ajax_response					{ padding-top: 5px; margin-left: 0; display: block; clear: both; }
				html>body li.ajax_response			{ padding-top: 10px; margin-left: 0; display: block; clear: both; }
				a.ajax_response						{ display: block; padding-left: 20px ; background: transparent url(../images/ajax-solution-arrow.png) no-repeat scroll 0 -68px; *background-position: 0 -63px; }
				a.ajax_response:hover				{ background-position: 0 2px; *background-position: 0 7px; }
				span.get-down-girl-go-head-get-down { display: none; }
		.contact_result_body						{ padding-left: 25px; padding-right: 25px; margin-top: 15px; }
			.faqsol_contact_result_container		{ margin-left: 25px;  }
				.faqsol_contact_result_link			{ color: #333333; }
				.faqsol_contact_result_link:hover	{ color: #cc2229; }
				.faqsol_contact_result_nomatch		{ padding-top: 15px; padding-left: 25px; padding-right: 25px;  display: block; }
	form											{ }
		.contact_form_checked_submit				{ display: block; margin-left: 40px; margin-top: 20px; }

	label.special									{ line-height: 25px; display: block; width: 150px; float: left; margin-top: 5px; color: #666666; clear: left; }
	.special_contact_form_container					{ display: block; margin: 15px; }
		.special_contact_form_head					{ display: block; padding-top: 15px; }
			.special_contact_form_title				{ display: block; margin-left: 20px; margin-bottom: 5px; margin-top: 25px; font-weight: bold;  color: #666666; }
			.special_contact_form_desc				{ margin-top: 5px; padding-left: 20px; padding-right: 20px;  line-height: 15px; display: block; color: #666666; }
			.special_contact_form_err				{ background-color: #CCCCCC; margin-top: 15px; margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px;  display: block; color: #F66B06; font-weight: bold; }
		.special_contact_form_body					{ margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; background-color: #EEEEEE; }
			.special_contact_form_label_star		{ color: #F66B06; }
				#special_contact_form_name			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_email			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_email_conf	{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_address1		{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_address2		{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_city			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_state			{ width: 100px; display: block; float: left; margin-top: 5px; clear: right; }
				#special_contact_form_postcode		{ width: 95px; display: block; float: left; margin-top: 5px; clear: right; }
		#special_contact_form_submit				{ display: block; float: left; margin-top: 5px; }
				
ENDECHO;
}

################################################

function tg_default_css() {
	echo <<<ENDECHO
body, div, span, h1, h2, h3, ul,
li, ol, a, form, fieldset					{ margin: 0; padding: 0; border: 0; font-family: Arial, Helvetica, sans-serif; }
li											{ margin-left: 40px; }
a											{ color: #E40A09; text-decoration: none; outline: none; font-weight: bold; }
a:hover										{ color: #E40A09; text-decoration: underline; outline: none; font-weight: bold; }
ol,ul										{ padding-bottom: 10px; }
html,body									{ _height: 100%; _overflow-y: auto; overflow-x: hidden; background-color: transparent; }

.faqsol_container 							{ margin: 0; padding: 0; margin-bottom: 0px; width: 546px; background-color: transparent; }
body > .faqsol_container 					{ margin: 0; padding: 0; margin-bottom: 0px; width: 546px; background-color: transparent; }
	.faqsol_header							{ }
		.faqsol_head_title					{ display: none; }
		.faqsol_head_subtitle				{ display: none; }
		.faqsol_head_tabs					{ display: block; float: left; width: 200px; font-size: 8pt; font-weight: bold; clear: left; padding-top: 5px; padding-left: 20px; }
			.faqsol_head_tabs_divider		{ display: block; float: left; height: 20px; line-height: 20px; color: #6d6d6d; padding-left: 3px; padding-right: 3px; }
			.faqsol_head_tabs_aa			{ }
			.faqsol_head_tabs_an			{ }
			.faqsol_head_tabs_aa:hover		{ }
			.faqsol_head_tabs_an:hover		{ }
			.faqsol_head_tabs_search		{ display: block; float: left; height: 20px; line-height: 20px; color: #E40A09; margin-left: 15px; }
			.faqsol_head_tabs_browse		{ display: block; float: left; height: 20px; line-height: 20px; color: #E40A09; }
			.faqsol_head_tabs_search:hover	{ color: #E40A09; font-weight: bold; text-decoration: underline; }
			.faqsol_head_tabs_browse:hover	{ color: #E40A09; font-weight: bold; text-decoration: underline; }
		.faqsol_head_otherlinks							{ display: block; float: right; width: 326px; font-size: 8pt; font-weight: bold; clear: right; padding-top: 5px; }
			.faqsol_head_otherlinks_divider				{ display: none; }
			.faqsol_head_otherlinks_manual				{ display: block; float: right; height: 20px; padding-left: 25px; background-image: url(../images/tg_download_arrow.png); background-repeat: no-repeat; line-height: 20px; }
			.faqsol_head_otherlinks_manual:hover		{ background-position: 0 100%; }
			.faqsol_head_otherlinks_trackorder			{ display: block; float: right; margin-right: 20px; height: 20px; padding-left: 25px; background-image: url(../images/tg_track_mag.gif); background-repeat: no-repeat; line-height: 20px; }
			.faqsol_head_otherlinks_trackorder:hover	{ background-position: 0 100%; }
			
	.clear_both_empty						{ clear: both; }

	.form_title_hint						{ color: #999999; }

	.contact_link							{ margin-left: 15px; margin-right: 15px; padding-left: 25px; margin-top: 15px; padding-top: 5px; border-top: 1px solid #EEEEEE; }
			.contact_link_pre				{ font-size: 8pt; color: #666666; display: block; height: 21px; line-height: 21px; float: left; }
			.contact_link_a					{ outline: none; margin-left: 15px; display: block; width: 127px; height: 21px; background-image: url(../images/tg_contactus.png); background-repeat: no-repeat; text-indent: -9999px; float: left; }
			.contact_link_a:hover			{ background-position: 0 100%; }
			.back_link_a					{ outline: none; margin-left: 20px; display: block; width: 62px; height: 21px; background-image: url(../images/tg_back-button.png); background-repeat: no-repeat; text-indent: -9999px; float: left; }
			.back_link_a:hover				{ background-position: 0 100%; }
			.poweredby_image				{ margin-left: 20px; display: block; width: 82px; height: 21px; background-image: url(../images/poweredby_small.png); background-repeat: no-repeat; text-indent: -9999px; float: right; }

ENDECHO;




}
function tg_search_css() {
	echo <<<ENDECHO
	.faqsol_prioritysol						{ width: 526px; padding: 10px; padding-top: 0; }
		.faqsol_prioritysol_li				{ list-style: none; display: inline; margin: 0; padding: 0; text-indent: 0; }
		.faqsol_prioritysol_a				{ color: #E40A09; background-image: url(../images/tg_right_arrow.jpg); background-repeat: no-repeat; background-position: 0px 0px; display: block; text-decoration: none; margin-left: 20px; padding-left: 20px; margin-bottom: 5px;  }
		.faqsol_prioritysol_a:hover			{ color: #E40A09; background-position: 0px -35px; text-decoration: underline; }
			.faqsol_prioritysol_title		{ font-weight: bold; font-size: 10pt; color: #E40A09; margin-bottom: 5px; }
			.faqsol_topten 					{ width: 500px; margin: 5px; float: left; font-size: 8pt; margin-bottom: 0; }
			.faqsol_timely					{ font-size: 10pt; color: #333333; margin-bottom: 5px; }

	.faqsol_searchbar						{ display: block; width: 546px; clear: both; padding-top: 10px; padding-bottom: 2px; padding-left: 40px; padding-right: 40px; }
	.faqsol_container > .faqsol_searchbar	{ display: block; width: 466px; clear: both; padding: 10px; padding-left: 40px; padding-right: 40px; }
		#faqsol_search_label				{ font-size: 10pt; }
		#faqsol_search_input				{ margin: 10px; margin-left: 0; margin-top: 10px; }
		#faqsol_search_submit				{ margin: 10px; margin-left: 0; margin-top: 10px; }
		
	.sol_result_head						{ padding: 15px; margin-top: 15px; }
		.result_head_uppersearchbar			{ display: block; clear: both; width: 526px; padding-top: 35px; padding-bottom: 20px; text-align: center; }
			#result_head_search_form		{ display: block; width: 300px; margin: 0 auto; }
		.sol_result_head_title				{ display: block; font-weight: bold; font-size: 10pt; color: #E40A09; margin-bottom: 5px; }
		.sol_result_head_desc				{ display: block; font-size: 10pt; padding-left: 25px; padding-right: 25px; color: #333333; }
	
	.sol_result_body						{ padding: 15px; padding-bottom: 0; }
		.faqsol_result_container			{ padding-left: 25px; padding-right: 25px; padding-bottom: 20px; font-size: 8pt; }
			.faqsol_result_link				{ color: #E40A09; }
			.faqsol_result_link:hover		{ color: #E40A09; text-decoration: underline; }
			.faqsol_result_desc				{ color: #333333; }
				.faqsol_highlight			{ font-weight: bold; }
			.faqsol_result_cat				{ display: none; color: #819dcf; }
		.sol_result_nomatch					{ display: block; padding-left: 25px; padding-right: 25px; padding-bottom: 20px; font-size: 8pt; }

ENDECHO;
}
function tg_solution_css() {
echo <<<ENDECHO
	.solution_uppersearchbar				{ display: block; clear: both; width: 526px; padding-top: 35px; text-align: center; }
		#solution_search_form				{ display: block; width: 300px; margin: 0 auto; }
			#solution_search_fieldset		{ }
				#solution_searchupper_input	{ }
				#solution_searchupper_submit{ }
				
	.faqsol_sol_container					{ padding: 15px; margin-top: 15px; padding-bottom: 0; }
		.faqsol_sol_title					{ display: block; color: #E40A09; margin-bottom: 5px; font-size: 10pt; font-weight: bold }
		.faqsol_sol_content					{ padding-left: 25px; color: #666666; padding-right: 25px; padding-bottom: 10px; font-size: 8pt; }
ENDECHO;
}
function tg_browse_css() {
	echo <<<ENDECHO
	.sol_browse_head						{ }
		.sol_browse_head_title				{ display: none; }
		.sol_browse_head_desc				{ display: block; width: 466px; clear: both; padding: 20px; padding-left: 40px; padding-right: 40px; padding-bottom: 0; font-size: 10pt; }
	.sol_browse_body						{ display: block; padding-bottom: 10px; margin-left: 40px; }
		.sol_cat_head						{ margin-left: 15px; margin-right: 15px; margin-top: 5px; }
			.sol_cat_title					{ display: inline; clear: both; }
				.sol_cat_title_link			{ float: left; text-decoration: none; font-weight: bold; font-size: 10pt; color: #E40A09; background-image: url(../images/tg_right_arrow.jpg); background-repeat: no-repeat; background-position: 0px 0px; display: block; padding-left: 15px; }
				.sol_cat_title_link:hover	{ float: left; text-decoration: underline; font-weight: bold; font-size: 10pt; color: #E40A09; background-position: 0px -35px;}
				.sol_cat_desc				{ padding-left: 20px; font-size: 8pt; color: #333333; }
		.sol_cat_body						{ margin: 5px; font-size: 8pt; }
			.sol_link_li					{ display: inline; list-style: none; margin: 0; padding: 0; text-indent: 0; }
				.sol_link					{ color: #666666; text-decoration: none; padding-left: 45px; padding-bottom: 0px; display: block; }
				.sol_link:hover				{ color: #666666; text-decoration: underline; }
ENDECHO;
}
function tg_contact_css() {
echo <<<ENDECHO
	.contact_form_container							{ padding: 15px; }
		.contact_form_head							{ margin-top: 15px; padding-bottom: 10px; }
			.contact_form_title						{ display: none; }
			.contact_form_desc						{ margin-top: 20px; padding-left: 25px; padding-right: 25px; font-size: 8pt; line-height: 15px; display: block; color: #666666; }
			.contact_form_err						{ background-color: #CCCCCC; margin-top: 15px; margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; font-size: 10pt; display: block; color: #E40A09; font-weight: bold; }
		.contact_form_body							{ margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; padding-top: 10px; padding-bottom: 10px; background-color: #EEEEEE; }
			label									{ font-size: 8pt; float: left; margin-top: 5px; border-left: 3px solid #F2F2F2; }
			label.wrong								{ border-left: 3px solid #E40A09; }
			.contact_form_label_star				{ font-size: 10pt; color: #E40A09; }
				#container #main .contact_input_divider				{ width: 400px; display: block; clear: both; margin: 0; padding: 0; color: #F2F2F2; border: 0; height: 0; }
				#contact_form_name					{ width: 165px; display: block; float: left; margin-right: 25px; }
				#contact_form_lname					{ width: 165px; display: block; float: left; }
				#contact_form_email					{ width: 165px; display: block; float: left; margin-right: 25px; }
				#contact_form_subject				{ width: 360px; display: block; float: left; }
				#contact_form_albumid				{ width: 165px; display: block; float: left; }
				.query_wrapper						{ float: left }
				#faqsol_search_input_contact		{ clear: both; float: left; }
				#contact_form_submit				{ display: block; float: left; clear: both; margin-top: 20px; }
				#ajax_response						{ display: block; width: 200px; float: left; padding-left: 10px; }
				span.ajax_response					{ font-size: 8pt; }
				ul.ajax_response					{ list-style-type: none; font-size: 9pt; }
				li.ajax_response					{ padding-top: 10px; margin-left: 0; display: block; clear: both; }
				a.ajax_response						{ display: block; padding-left: 20px ; background: transparent url(../images/ajax-solution-arrow-tg.png) no-repeat scroll 0 2px; }
				a.ajax_response:hover				{ background-position: 0 -58px; }
				span.get-down-girl-go-head-get-down { display: none; }
		.contact_result_body						{ padding-left: 25px; padding-right: 25px; margin-top: 15px; }
			.faqsol_contact_result_container		{ margin-left: 25px; font-size: 8pt; }
				.faqsol_contact_result_link			{ color: #333333; }
				.faqsol_contact_result_link:hover	{ color: #E40A09; }
				.faqsol_contact_result_nomatch		{ padding-top: 15px; padding-left: 25px; padding-right: 25px; font-size: 10pt; display: block; }
	form											{ }
		.contact_form_checked_submit				{ display: block; margin-left: 40px; margin-top: 20px; }

	label.special									{ font-size: 8pt; line-height: 25px; display: block; width: 150px; float: left; margin-top: 5px; color: #666666; clear: left; }
	.special_contact_form_container					{ display: block; margin: 15px; }
		.special_contact_form_head					{ display: block; padding-top: 15px; }
			.special_contact_form_title				{ display: block; margin-left: 20px; margin-bottom: 5px; margin-top: 25px; font-weight: bold; font-size: 10pt; color: #666666; }
			.special_contact_form_desc				{ margin-top: 5px; padding-left: 20px; padding-right: 20px; font-size: 8pt; line-height: 15px; display: block; color: #666666; }
			.special_contact_form_err				{ background-color: #CCCCCC; margin-top: 15px; margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; font-size: 10pt; display: block; color: #F66B06; font-weight: bold; }
		.special_contact_form_body					{ margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; background-color: #EEEEEE; }
			.special_contact_form_label_star		{ font-size: 10pt; color: #F66B06; }
				#special_contact_form_name			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_email			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_email_conf	{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_address1		{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_address2		{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_city			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_state			{ width: 100px; display: block; float: left; margin-top: 5px; clear: right; }
				#special_contact_form_postcode		{ width: 95px; display: block; float: left; margin-top: 5px; clear: right; }
		#special_contact_form_submit				{ display: block; float: left; margin-top: 5px; }
				
ENDECHO;
}

################################################

function ow_default_css() {
	echo <<<ENDECHO
body, div, span, h1, h2, h3, ul,
li, ol, a, form, fieldset					{ margin: 0; padding: 0; border: 0; font-family: Arial, Helvetica, sans-serif; }
body										{ color: #333333; }
li											{ margin-left: 40px; }
a											{ color: #1E58A7; text-decoration: none; outline: none; font-weight: bold; }
a:hover										{ color: #ED1B2E; text-decoration: none; outline: none; font-weight: bold; }
ol,ul										{ padding-bottom: 10px; }

.faqsol_container 							{ margin: 0; padding: 0; margin-bottom: 40px; width: 546px; background-color: #FFFFFF; }
body > .faqsol_container 					{ margin: 0; padding: 0; margin-bottom: 40px; width: 546px; background-color: #FFFFFF; }
	.faqsol_header							{ background: #FFFFFF url(../images/ow_subtitle_roundcorners.gif) 0 0 repeat-x; }
		.faqsol_head_title					{ display: none; }
		.faqsol_head_subtitle				{ display: none; padding-left: 10px; margin-left: 5px; margin-right: 6px; font-weight: bold; font-size: 12pt; color: #FFFFFF; background: #005AAB url(../images/ow_subtitle_roundcorners.gif) -18px 0 no-repeat; height: 32px; line-height: 32px; }

		.faqsol_head_tabs					{ display: block; float: left; width: 200px; font-size: 8pt; font-weight: bold; clear: left; padding-top: 20px; padding-left: 20px; }
			.faqsol_head_tabs_divider		{ display: block; float: left; height: 20px; line-height: 20px; color: #304788; padding-left: 3px; padding-right: 3px; }
			.faqsol_head_tabs_aa			{ }
			.faqsol_head_tabs_an			{ }
			.faqsol_head_tabs_aa:hover		{ }
			.faqsol_head_tabs_an:hover		{ }
			.faqsol_head_tabs_search		{ display: block; float: left; height: 20px; line-height: 20px; color: #1E58A7; margin-left: 15px; }
			.faqsol_head_tabs_browse		{ display: block; float: left; height: 20px; line-height: 20px; color: #1E58A7; }
			.faqsol_head_tabs_search:hover	{ color: #ED1B2E; font-weight: bold; text-decoration: underline; }
			.faqsol_head_tabs_browse:hover	{ color: #ED1B2E; font-weight: bold; text-decoration: underline; }
		.faqsol_head_otherlinks							{ display: block; float: right; width: 326px; font-size: 8pt; font-weight: bold; clear: right; padding-top: 20px; }
			.faqsol_head_otherlinks_divider				{ display: none; }
			.faqsol_head_otherlinks_manual				{ display: block; float: right; height: 20px; padding-left: 25px; background-image: url(../images/ow_download_arrow.png); background-repeat: no-repeat; line-height: 20px; }
			.faqsol_head_otherlinks_manual:hover		{ background-position: 0 100%; }
			.faqsol_head_otherlinks_trackorder			{ display: block; float: right; margin-right: 20px; height: 20px; padding-left: 25px; background-image: url(../images/ow_track_mag.gif); background-repeat: no-repeat; line-height: 20px; }
			.faqsol_head_otherlinks_trackorder:hover	{ background-position: 0 100%; }
			
	.clear_both_empty						{ clear: both; }

	.form_title_hint						{ color: #999999; }

	.contact_link							{ margin-left: 15px; margin-right: 15px; padding-left: 25px; margin-top: 20px; padding-top: 5px; border-top: 1px solid #EEEEEE; }
			.contact_link_pre				{ font-size: 8pt; color: #666666; display: block; height: 21px; line-height: 21px; float: left; }
			.contact_link_a					{ outline: none; margin-left: 15px; display: block; width: 127px; height: 21px; background-image: url(../images/ow_contactus.png); background-repeat: no-repeat; text-indent: -9999px; float: left; }
			.contact_link_a:hover			{ background-position: 0 100%; }
			.back_link_a					{ outline: none; margin-left: 20px; display: block; width: 62px; height: 21px; background-image: url(../images/ow_back-button.png); background-repeat: no-repeat; text-indent: -9999px; float: left; }
			.back_link_a:hover				{ background-position: 0 100%; }
			.poweredby_image				{ margin-left: 20px; display: block; width: 82px; height: 21px; background-image: url(../images/poweredby_small.png); background-repeat: no-repeat; text-indent: -9999px; float: right; }

ENDECHO;
}

function ow_search_css() {
	if($_SERVER["REMOTE_ADDR"] == '203.6.240.254'){
	echo <<<ENDECHO
	.faqsol_prioritysol						{ width: 526px; padding: 10px; }
		.faqsol_prioritysol_li				{ margin-left: 25px; list-style: none; display: inline; margin: 0; padding: 0; text-indent: 0; }
		.faqsol_prioritysol_a				{ font-weight: normal; background-image: url(../images/ow_red_arrow.jpg); background-repeat: no-repeat; background-position: 5px 27px; display: block; text-decoration: none; margin-left: 20px; padding-left: 20px; margin-bottom: 5px;  }
		.faqsol_prioritysol_a:hover			{ font-weight: normal; background-position: 5px -4px; }
			.faqsol_prioritysol_title		{ color: #1E58A7; font-weight: bold; font-size: 10pt; margin-bottom: 5px; }
			.faqsol_topten 					{ width: 500px; margin: 5px; float: left; font-size: 8pt; }
			.faqsol_timely					{ color: #333333; font-size: 10pt; margin-bottom: 5px; }
	
	#result_head_search_fieldset				{ text-align: left; }
	.faqsol_searchbar						{ display: block; width: 546px; clear: both; padding: 20px; padding-left: 40px; padding-right: 40px; }
	.faqsol_container > .faqsol_searchbar	{ display: block; width: 466px; clear: both; padding: 20px; padding-left: 40px; padding-right: 40px; }
		#faqsol_search_label				{ font-size: 12px; font-weight: bold; color: #000000; display: block }
		#faqsol_search_sub					{ font-size: 12px; }
		#faqsol_search_input				{ margin: 3px 10px 10px 0 }
		#faqsol_search_submit				{ margin: 3px 10px 10px 0 }
		#faqsol_search_meta					{ margin: 0 0 10px 0; font-size: 12px; }
		
	.sol_result_head						{ padding: 15px; margin-top: 15px; }
		.result_head_uppersearchbar			{ display: block; clear: both; width: 526px; padding-top: 35px; padding-bottom: 20px; text-align: center; }
			#result_head_search_form		{ display: block; width: 300px; margin: 0 auto; }
		.sol_result_head_title				{ display: block; font-weight: bold; font-size: 10pt; color: #1E58A7; margin-bottom: 5px; }
		.sol_result_head_desc				{ display: block; font-size: 10pt; padding-left: 25px; padding-right: 25px; color: #333333; }
	
	.sol_result_body						{ padding: 15px; padding-bottom: 0; }
		.faqsol_result_container			{ padding-left: 25px; padding-right: 25px; padding-bottom: 20px; font-size: 8pt; }
			.faqsol_result_link				{ }
			.faqsol_result_link:hover		{ }
			.faqsol_result_desc				{ color: #333333; }
				.faqsol_highlight			{ font-weight: bold; }
			.faqsol_result_cat				{ color: #819dcf; display: none; }
		.sol_result_nomatch					{ display: block; padding-left: 25px; padding-right: 25px; padding-bottom: 20px; font-size: 8pt; }

ENDECHO;
	} else {
	echo <<<ENDECHO
	.faqsol_prioritysol						{ width: 526px; padding: 10px; }
		.faqsol_prioritysol_li				{ margin-left: 25px; list-style: none; display: inline; margin: 0; padding: 0; text-indent: 0; }
		.faqsol_prioritysol_a				{ font-weight: normal; background-image: url(../images/ow_red_arrow.jpg); background-repeat: no-repeat; background-position: 5px 27px; display: block; text-decoration: none; margin-left: 20px; padding-left: 20px; margin-bottom: 5px;  }
		.faqsol_prioritysol_a:hover			{ font-weight: normal; background-position: 5px -4px; }
			.faqsol_prioritysol_title		{ color: #1E58A7; font-weight: bold; font-size: 10pt; margin-bottom: 5px; }
			.faqsol_topten 					{ width: 500px; margin: 5px; float: left; font-size: 8pt; }
			.faqsol_timely					{ color: #333333; font-size: 10pt; margin-bottom: 5px; }

	.faqsol_searchbar						{ display: block; width: 546px; clear: both; padding: 20px; padding-left: 40px; padding-right: 40px; }
	.faqsol_container > .faqsol_searchbar	{ display: block; width: 466px; clear: both; padding: 20px; padding-left: 40px; padding-right: 40px; }
		#faqsol_search_label				{ font-size: 10pt; }
		#faqsol_search_input				{ margin: 10px; margin-left: 0; margin-top: 30px; }
		#faqsol_search_submit				{ margin: 10px; margin-left: 0; margin-top: 30px; }
		
	.sol_result_head						{ padding: 15px; margin-top: 15px; }
		.result_head_uppersearchbar			{ display: block; clear: both; width: 526px; padding-top: 35px; padding-bottom: 20px; text-align: center; }
			#result_head_search_form		{ display: block; width: 300px; margin: 0 auto; }
		.sol_result_head_title				{ display: block; font-weight: bold; font-size: 10pt; color: #1E58A7; margin-bottom: 5px; }
		.sol_result_head_desc				{ display: block; font-size: 10pt; padding-left: 25px; padding-right: 25px; color: #333333; }
	
	.sol_result_body						{ padding: 15px; padding-bottom: 0; }
		.faqsol_result_container			{ padding-left: 25px; padding-right: 25px; padding-bottom: 20px; font-size: 8pt; }
			.faqsol_result_link				{ }
			.faqsol_result_link:hover		{ }
			.faqsol_result_desc				{ color: #333333; }
				.faqsol_highlight			{ font-weight: bold; }
			.faqsol_result_cat				{ color: #819dcf; display: none; }
		.sol_result_nomatch					{ display: block; padding-left: 25px; padding-right: 25px; padding-bottom: 20px; font-size: 8pt; }

ENDECHO;
	}
}

function ow_solution_css() {
	echo <<<ENDECHO
	.solution_uppersearchbar				{ display: block; clear: both; width: 526px; padding-top: 35px; text-align: center; }
		#solution_search_form				{ display: block; width: 300px; margin: 0 auto; }
			#solution_search_fieldset		{ }
				#solution_searchupper_input	{ }
				#solution_searchupper_submit{ }

	.faqsol_sol_container					{ padding: 15px; margin-top: 15px; padding-bottom: 0; }
		.faqsol_sol_title					{ display: block; font-weight: bold; font-size: 10pt; color: #1E58A7; margin-bottom: 5px; }
		.faqsol_sol_content					{ padding-left: 25px; padding-right: 25px; padding-bottom: 10px; font-size: 8pt; }
ENDECHO;
}

function ow_browse_css() {
	echo <<<ENDECHO
	.sol_browse_head						{ }
		.sol_browse_head_title				{ display: none; }
		.sol_browse_head_desc				{ display: block; width: 466px; clear: both; padding: 20px; padding-left: 40px; padding-right: 40px; padding-bottom: 0; font-size: 10pt; }
	.sol_browse_body						{ display: block; padding-bottom: 10px; margin-left: 40px; }
		.sol_cat_head						{ margin-left: 15px; margin-right: 15px; margin-top: 5px; }
			.sol_cat_title					{ float: left; clear: both; }
				.sol_cat_title_link			{ background-image: url(../images/ow_red_arrow.jpg); background-repeat: no-repeat; background-position: 0px -2px; padding-left: 10px; text-decoration: none; font-weight: bold; font-size: 10pt; outline: none; }
				.sol_cat_title_link:hover	{ }
			.sol_cat_desc					{ padding-left: 20px; font-size: 8pt; color: #333333; }
		.sol_cat_body						{ margin: 5px; font-size: 8pt; }
			.sol_link_li					{ display: inline; list-style: none; margin: 0; padding: 0; text-indent: 0; }
				.sol_link					{ font-weight: normal; display: block; background-image: url(../images/ow_red_arrow.jpg); background-repeat: no-repeat; background-position: 30px 27px; text-decoration: none; padding-left: 45px; padding-bottom: 5px; }
				.sol_link:hover				{ font-weight: normal; background-position: 30px -4px; }
ENDECHO;
}

function ow_contact_css() {
	if($_SERVER["REMOTE_ADDR"] == '203.6.240.254'){
		
	echo <<<ENDECHO
	.faqsol_container		{ width: 560px !important; }
	.form-heading			{ background: url(../images/ow_box01-2.gif) top no-repeat #00AEEF; width: 560px; height: 35px; clear: both; color: #FFFFFF; margin: 15px 0 0 0; }
		.form-heading h2 	{ float: left; font-size: 16px; margin: 8px 0 0 11px; }
		.form-heading div	{ float: right; text-align: right; font-size: 11px; margin: 12px 12px 0 0; }
	.asterix				{ color: #E2001A; }
	.form-body				{ background: url(../images/ow_box02-2.gif) top no-repeat; width: 558px; border-left: #00aeef 1px solid; border-right: #00aeef 1px solid;}
	.contact-form			{ padding: 25px 10px 0 15px; }
	.input-field			{ display: inline; float: left; margin: 0 45px 10px 0; }
		.input-field input	{ width: 170px; }
		.input-field label	{ display: block; font-size: 12px; font-weight: bold; margin-bottom: 3px; color: #000 }
	.form-bottom			{ background: url(../images/ow_box03-2.gif) top no-repeat; width: 560px; height: 10px}
	.message-box			{ clear: both; margin-right: 0px; }
	#ajax_response			{ float: right; width: 300px; font-size: 12px; }
		#ajax_response label{ display: block; font-size: 12px; font-weight: bold; margin-bottom: 3px; color: #000 }
	.ajax_response 			{ background: none !important; }
	#contact_form_submit	{ clear: both; display: block; }

ENDECHO;
	} else {
		
	echo <<<ENDECHO
	.contact_form_container							{ padding: 15px; }
		.contact_form_head							{ margin-top: 15px; }
			.contact_form_title						{ display: none; }
			.contact_form_desc						{ margin-top: 50px; padding-left: 25px; padding-right: 25px; font-size: 8pt; line-height: 15px; display: block; color: #666666; }
			.contact_form_err						{ background-color: #CCCCCC; margin-top: 15px; margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; font-size: 10pt; display: block; color: #1E58A7; font-weight: bold; }
		.contact_form_body							{ margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; padding-top: 10px; padding-bottom: 20px; background-color: #EEEEEE; }
			label									{ font-size: 8pt; float: left; margin-top: 5px; border-left: 3px solid #EEEEEE; }
			label.wrong								{ border-left: 3px solid #1E58A7; }
			.contact_form_label_star				{ font-size: 10pt; color: #FF0000; }
				#container #main .contact_input_divider				{ width: 400px; display: block; clear: both; margin: 0; padding: 0; color: #EEEEEE; border: 0; height: 0; }
				#contact_form_name					{ width: 165px; display: block; float: left; margin-right: 22px; }
				#contact_form_lname					{ width: 165px; display: block; float: left; }
				#contact_form_email					{ width: 165px; display: block; float: left; margin-right: 22px; }
				#contact_form_subject				{ width: 360px; display: block; float: left; }
				#contact_form_albumid				{ width: 165px; display: block; float: left; }
				.query_wrapper						{ float: left }
				#faqsol_search_input_contact		{ clear: both; float: left; }
				#contact_form_submit				{ display: block; float: left; clear: both; margin-top: 20px; }
				#ajax_response						{ display: block; width: 200px; float: left; padding-left: 10px; }
				span.ajax_response					{ font-size: 8pt; }
				ul.ajax_response					{ font-size: 9pt; list-style-type: none; }
				li.ajax_response					{ padding-top: 10px; margin-left: 0; display: block; clear: both; }
				a.ajax_response						{ display: block; padding-left: 20px ; background: transparent url(../images/ajax-solution-arrow-ow.png) no-repeat scroll 0 -3px; }
				a.ajax_response:hover				{ background-position: 5px -3px; }
				span.get-down-girl-go-head-get-down { display: none; }
		.contact_result_body						{ padding-left: 25px; padding-right: 25px; margin-top: 15px; }
			.faqsol_contact_result_container		{ margin-left: 25px; font-size: 8pt; }
				.faqsol_contact_result_link			{ color: #333333; }
				.faqsol_contact_result_link:hover	{ color: #cc2229; }
				.faqsol_contact_result_nomatch		{ padding-top: 15px; padding-left: 25px; padding-right: 25px; font-size: 10pt; display: block; }
	form											{ }
		.contact_form_checked_submit				{ display: block; margin-left: 40px; margin-top: 20px; }

	label.special									{ font-size: 8pt; line-height: 25px; display: block; width: 150px; float: left; margin-top: 5px; color: #666666; clear: left; }
	.special_contact_form_container					{ display: block; margin: 15px; }
		.special_contact_form_head					{ display: block; padding-top: 15px; }
			.special_contact_form_title				{ display: block; margin-left: 20px; margin-bottom: 5px; margin-top: 25px; font-weight: bold; font-size: 10pt; color: #1E58A7; }
			.special_contact_form_desc				{ margin-top: 5px; padding-left: 20px; padding-right: 20px; font-size: 8pt; line-height: 15px; display: block; color: #666666; }
			.special_contact_form_err				{ background-color: #CCCCCC; margin-top: 15px; margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; font-size: 10pt; display: block; color: #1E58A7; font-weight: bold; }
		.special_contact_form_body					{ margin-left: 20px; margin-right: 20px; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; background-color: #EEEEEE; }
			.special_contact_form_label_star		{ font-size: 10pt; color: #FF0000; }
				#special_contact_form_name			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_email			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_email_conf	{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_address1		{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_address2		{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_city			{ width: 265px; display: block; float: left; margin-top: 5px; }
				#special_contact_form_state			{ width: 100px; display: block; float: left; margin-top: 5px; clear: right; }
				#special_contact_form_postcode		{ width: 95px; display: block; float: left; margin-top: 5px; clear: right; }
		#special_contact_form_submit				{ display: block; float: left; margin-top: 5px; }
				
ENDECHO;
	}
}

################################################

?>