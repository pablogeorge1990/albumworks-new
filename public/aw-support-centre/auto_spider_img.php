<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

/* Set the following variable to True to debug the automatic spidering */
$isearch_verbose = True;

if (!$isearch_verbose)
{
    // Send a 1x1 pixel transparent GIF
    header("Content-type: image/gif");

    echo "\x47\x49\x46\x38\x39\x61\x04\x00\x04\x00\x80\x00\x00\xff\xff\xff\x00\x00\x00\x21\xf9\x04\x01\x00\x00\x00\x00\x2c\x00\x00\x00\x00\x04\x00\x04\x00\x00\x02\x04\x84\x8f\x09\x05\x00\x3b";
}

// Start output buffering
ob_start();

define('IN_ISEARCH', true);
$isearch_path = '.';
include "$isearch_path/inc/auto_spider.inc.php";

$contents = ob_get_contents();

if (strlen($contents) > 0)
{
    // Write output to a file
    if ($fh = fopen('auto_spider_img.log', 'w'))
    {
        fwrite($fh, $contents);
        fclose($fh);
    }
}

if ($isearch_verbose)
{
    // Flush the output
    ob_flush();
}

// Discard any output
ob_clean();

?>