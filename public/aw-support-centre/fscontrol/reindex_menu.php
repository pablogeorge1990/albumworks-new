<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

// PHPLOCKITOPT NOENCODE

$isearch_path = dirname(__FILE__).'/..';
define('IN_ISEARCH', true);


require_once "$isearch_path/inc/core.inc.php";
isearch_open();
require_once "$isearch_path/inc/admin_auth.inc.php";
require_once "$isearch_path/inc/spider.inc.php";

echo <<<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<HEAD>
<TITLE>iSearch Reindex Site</TITLE>
<META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">
<META HTTP-EQUIV="Content-Language" CONTENT="EN-GB">
<META HTTP-EQUIV="Pragma" content="no-cache">
<META HTTP-EQUIV="Expires" content="Fri, 01 Jan 1999 00:00:01 GMT">
<META NAME="robots" CONTENT="noindex,nofollow">
<LINK REL=StyleSheet HREF="admin.css" TYPE="text/css">
</HEAD>

<BODY BGCOLOR="#ffffc0" TEXT="#000000">

<H1>FAQsol Reindex Site</H1>

EOF;

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

if ($action == 'copy')
{
    if (isearch_getUrlCount(True, 'ok') > 0)
    {
        echo <<<EOF
<P>Copying tables - please wait...

</BODY>

EOF;
        echo str_pad(' ', 1024);

        @ini_set('max_execution_time', 0);
        isearch_copyUrlTables();
        $delay = 1;
    }
    else
    {
        echo "<P>No results available to copy.";
        $delay = 3;
    }

    echo <<<EOF

<HEAD>
    <META HTTP-EQUIV="REFRESH" CONTENT="$delay;URL=$PHP_SELF">
</HEAD>
</HTML>

EOF;

}
else
{
    echo <<<EOF
<script language="JavaScript">
<!--
function pauseClicked()
{
    if (isFinished())
    {
        alert("Spidering has already finished.");
    }
    else
    {
        document.form1.pause.value='true';
        document.form1.state.value='pausing - please wait';
    }
}

function isFinished()
{
    if (document.form1.finished.value=='true')
    {
        return true;
    }
    return false;
}

function copyClicked()
{
    if (isFinished())
    {
        alert("Spidering has already finished. The spidered pages have already been copied to the search tables.");
    }
    else
    {
        if (confirm("This will copy the pages that the spider has already spidered into the search tables so that you can search them immediately. If the spider has not yet spidered a page it will not be found when searching.\\n\\nAre you sure?"))
        {
            window.location='$PHP_SELF?action=copy';
        }
    }
}

function resetClicked()
{
    if (isFinished())
    {
        if (confirm("This will start the spider engine again.\\n\\nAre you sure?"))
        {
            document.form1.state.value='starting';
            parent.isearch_reindex.window.location='reindex.php?reset=true';
        }
    }
    else
    {
        if (confirm("This will reset the spider engine.\\n\\nAre you sure?"))
        {
            document.form1.state.value='starting';
            parent.isearch_reindex.window.location='reindex.php?reset=true';
        }
    }
}


// -->
</script>

<TABLE>
<TR>
<TD>
 <FORM name="form1">
 <INPUT NAME="pause" TYPE="hidden">
 <INPUT NAME="finished" TYPE="hidden">
 State: <INPUT NAME="state" SIZE="40" VALUE="starting">
 </FORM>
</TD>

<TD>&nbsp;&nbsp;<A HREF="$PHP_SELF?action=pause" onClick="pauseClicked(); return false;" onMouseOver="window.status='Pause the Spider Engine';return true;" onMouseOut="window.status=' ';return true;">Pause</A></TD>
<TD>&nbsp;&nbsp;<A HREF="reindex.php?reset=true" TARGET="isearch_reindex" onClick="resetClicked(); return false;" onMouseOver="window.status='Reset the Spider Engine';return true;" onMouseOut="window.status=' ';return true;">Reset</A></TD>
<TD>&nbsp;&nbsp;<A HREF="$PHP_SELF?action=copy" onClick="copyClicked(); return false;" onMouseOver="window.status='Copy the current Spidered Pages to the Search Index';return true;" onMouseOut="window.status=' ';return true;">Copy</A></TD>
<TD>&nbsp;&nbsp;<A HREF="log.php" TARGET="isearch_log" onMouseOver="window.status='View the Spider Log';return true;" onMouseOut="window.status=' ';return true;">View Log</A></TD>

<NOSCRIPT>
<P>Please enable JavaScript before using the &quot;Pause&quot; feature.
</NOSCRIPT>

</TR>
</TABLE>
</BODY>
</HTML>

EOF;
}

?>
