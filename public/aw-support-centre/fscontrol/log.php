<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

// PHPLOCKITOPT NOENCODE

$isearch_path = dirname(__FILE__).'/..';
define('IN_ISEARCH', true);

require_once "$isearch_path/inc/core.inc.php";
isearch_open();
require_once "$isearch_path/inc/admin_auth.inc.php";
require_once "$isearch_path/inc/spider.inc.php";

echo <<<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<HEAD>
<TITLE>FAQsol Spider Log</TITLE>
<META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">
<META HTTP-EQUIV="Content-Language" CONTENT="EN-GB">
<META HTTP-EQUIV="Pragma" content="no-cache">
<META HTTP-EQUIV="Expires" content="Fri, 01 Jan 1999 00:00:01 GMT">
<META NAME="robots" CONTENT="noindex,nofollow">
<LINK REL=StyleSheet HREF="admin.css" TYPE="text/css">
</HEAD>

<BODY BGCOLOR="#ffffc0" TEXT="#000000">

<H1>FAQsol Spider Log</H1>

<PRE>
EOF;

echo isearch_html_entities(isearch_getSpiderLog());

isearch_close();

echo '</PRE>
</BODY>
</HTML>
';

?>
