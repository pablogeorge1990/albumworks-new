<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

// PHPLOCKITOPT NOENCODE

$isearch_path = dirname(__FILE__).'/..';
define('IN_ISEARCH', true);


require_once "$isearch_path/inc/core.inc.php";
isearch_open();
require_once "$isearch_path/inc/admin_auth.inc.php";
require_once "$isearch_path/inc/spider.inc.php";

/* Display the frame set */

$reset = isset($_REQUEST['reset']) ? strip_tags($_REQUEST['reset']) : 'true';
$followmode = isset($_REQUEST['followmode']) ? intval($_REQUEST['followmode']) : 2;
$indexmode = isset($_REQUEST['indexmode']) ? intval($_REQUEST['indexmode']) : 2;

$reindexUrl = "reindex.php?reset=$reset&followmode=$followmode&indexmode=$indexmode";

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>FAQsol Spider</TITLE>
<META HTTP-EQUIV="Content-Type" content="text/html; charset=' . $isearch_config['char_set'] . '">
<META HTTP-EQUIV="Content-Language" CONTENT="' . $isearch_languageCode . '">
<META NAME="robots" CONTENT="noindex,nofollow">
<LINK REL=StyleSheet HREF="admin.css" TYPE="text/css">
</HEAD>
<FRAMESET cols="100%" rows="110,*" FRAMEBORDER="YES" FRAMESPACING="2" BORDER="2">
<FRAME frameborder="0" src="reindex_menu.php" name="isearch_reindex_menu">
<FRAME frameborder="0" src="'.$reindexUrl.'" name="isearch_reindex">
</FRAMESET>

<NOFRAMES>
<BODY>

<H3>Sorry - your browser does not support frames. Please <A HREF="'.$reindexUrl.'">click here</A></H3>

</BODY>
</NOFRAMES>
</HTML>

';

?>
