<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

// PHPLOCKITOPT NOENCODE

$isearch_path = dirname(__FILE__).'/..';
define('IN_ISEARCH', true);

require_once "$isearch_path/inc/core.inc.php";
isearch_open();
require_once "$isearch_path/inc/admin_auth.inc.php";

isearch_open(True);


/* Check the action and set the page title accordingly */
if (isset($isearch_lang['stats_title']))
{
    $isearch_pageTitle = $isearch_lang['stats_title'];
}
else
{
    $isearch_pageTitle = $isearch_lang['results_title'];
}

echo '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xml:lang="' . $isearch_languageCode . '">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset='.$isearch_config['char_set'].'" />
        <title>' . $isearch_pageTitle . '</title>
        <style type="text/css">
		<!--
		table.isearch-stats
		{
		}

		table.isearch-stats th, table.isearch-stats td
		{
		    padding: 3px;
		    padding-left: 10px;
		    padding-right: 10px;
		}

		table.isearch-stats th
		{
		    color: #ffffff;
		    background-color: #3366cc;
		}

		table.isearch-stats td
		{
		    background-color: #ffffcc;
		    color: #003399;
		}

		table.isearch-stats caption
		{
		    color: #003399;
		    font-weight: bold;
		    font-size: 17px;
		    font-style: italic;
		}
		-->
		</style>
    </head>

';


echo <<<EOF

        <h1 class="isearch">$isearch_pageTitle</h1>\n
		
		<div><a href="portal_login_report.php">Portal Login Report</a> | <a href="portal_downloads_report.php">Portal Downloads Report</a></div>

EOF;

if ($isearch_config['total_searches'])
{
    $result = mysql_query("SELECT count(search_term) AS num FROM $isearch_table_search_log", $isearch_ro_db);
    if (!$result)
    {
        echo '<p>ERROR: MySQL error : ' . mysql_error() . '</p>';
    }
    else if ($item = mysql_fetch_object($result))
    {
        echo '
        <table summary="' . $isearch_lang['sum_stats_total'] . '" width="100%" border=0 cellpadding=0 class="isearch-stats">
            <caption>Total Searches</caption>
            <thead>
                <tr>
                    <th scope="col">Total Searches</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align:center">' . $item->num . '</td>
                </tr>
            </tbody>
        </table>
        <br />
';
    }
}

if ($isearch_config['top_searches'] > 0)
{
    $result = mysql_query("SELECT DISTINCT search_term, matches, count(search_term) AS num FROM $isearch_table_search_log WHERE matches>0 GROUP BY search_term ORDER BY num desc LIMIT " . $isearch_config['top_searches'], $isearch_ro_db);
    if (!$result)
    {
        echo '<p>ERROR: MySQL error : ' . mysql_error() . '</p>';
    }
    else
    {
        echo '
        <table summary="' . $isearch_lang['sum_stats_top'] . '" width="100%" border="0" cellpadding="0" class="isearch-stats">
            <caption>' . str_replace("%d", $isearch_config['top_searches'], $isearch_lang['topsearches']) . '</caption>
            <col width="15%" />
            <col width="70%" />
            <col width="15%" />
            <thead>
                <tr>
                    <th scope="col">Searches</th>
                    <th scope="col">' . $isearch_lang['searchterm'] . '</th>
                    <th scope="col">' . $isearch_lang['matches'] . '</th>
                </tr>
            </thead>
            <tbody>
';
        while ($item = mysql_fetch_object($result))
        {
            echo '
                <tr>
                    <td>' . $item->num . '</td>
                    <td><a href="index.php?s=' . $item->search_term . '">' . $item->search_term . '</a></td>
                    <td>' . $item->matches . '</td>
                </tr>
';
        }
        echo '
            </tbody>
        </table>
        <br />
';
    }
}

if ($isearch_config['last_searches'] > 0)
{
    $result = mysql_query("SELECT DISTINCT search_term, matches, time FROM $isearch_table_search_log ORDER BY time desc LIMIT " . $isearch_config['last_searches'], $isearch_ro_db);
    if (!$result)
    {
        echo '
        <p>ERROR: MySQL error : ' . mysql_error() . '</p>
';
    }
    else
    {
        echo '
        <table summary="' . $isearch_lang['sum_stats_last'] . '" width="100%" border="0" cellpadding="0" class="isearch-stats">
            <caption>' . str_replace("%d", $isearch_config['last_searches'], $isearch_lang['lastsearches']) . '</caption>
            <col width="70%" />
            <col width="15%" />
			<col width="15%" />
            <thead>
                <tr>
                    <th scope="col">' . $isearch_lang['searchterm'] . '</th>
                    <th scope="col">' . $isearch_lang['matches'] . '</th>
                    <th scope="col">Date/Time</th>
                </tr>
            </thead>
            <tbody>
';
        while ($item = mysql_fetch_object($result))
        {
            echo '
                <tr>
                    <td><a href="index.php?s=' . $item->search_term . '">' . $item->search_term . '</a></td>
                    <td>' . $item->matches . '</td>
					<td>' . date("d/m/Y H:i",$item->time) . '</td>
                </tr>
';
        }
        echo '
            </tbody>
        </table>
        <br />
';
    }
}


echo <<<EOF

    </body>
</html>

EOF;

?>
