<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

// PHPLOCKITOPT NOENCODE

$isearch_path = dirname(__FILE__).'/..';
define('IN_ISEARCH', true);

function usage()
{
    echo '
Usage:

    php reindex.php -c [-r reset] [-c clear] [-d delay] [-p pages]
                    [-l loglevel] [-v] [-h]
';
}

function help()
{
    echo '
iSearch reindex.php help
';
    usage();
    echo '
Options:

    -c              Execute from command line (required for command line
                    execution)
    -r reset        Reset the spidered database (0 or 1, default 1).
    -d delay        Set delay in secs between spidering pages.
    -t time         Set maximum number of seconds to spider for.
    -p pages        Set maximum number of pages to spider.
    -l loglevel     Sets log level (0 to 10). 0=No logging, 10=very verbose.
    -f followMode   Set the mode for link following. 0=Do not follow,
                    1=always follow, 2=use robots meta tag. Default 2.
    -i indexMode    Set the mode for storing pages in the index. 0=Do not inedx,
                    1=always index, 2=use robots meta tag. Default 2.
    -v              Show version and exit.
    -h              Show help and exit.
';
}

$isearch_fromCommandLine = False;
$followmode = 2;
$indexmode = 2;
if (isset($_SERVER['argv']))
{
    $argv = $_SERVER['argv'];

    // Skip script name
    next($argv);

    while (($arg = current($argv)) !== False)
    {
        switch ($arg)
        {
            case 'cmd':
            case '-c':
                $isearch_fromCommandLine = True;
                break;
            case '-r':
                $reset = next($argv) ? 'true' : 'false';
                break;
            case '-d':
                $isearch_spiderDelay = intval(next($argv));
                break;
            case '-t':
                $isearch_maxSpiderTime = intval(next($argv));
                break;
            case '-p':
                $isearch_maxPages = intval(next($argv));
                break;
            case '-l':
                $isearch_logEchoLevel = intval(next($argv));
                break;
            case '-f':
                $followmode = intval(next($argv));
                break;
            case '-i':
                $indexmode = intval(next($argv));
                break;
            case '-v':
                echo "iSearch version ".$isearch_version;
                exit(0);
            case '-h':
                help();
                exit(0);
            default:
                echo "Unrecognised argument $arg\n";
                usage();
                echo "Use php reindex.php -h for detailed help\n";
                exit(1);
        }
        next($argv);
    }
}

if ($isearch_fromCommandLine)
{
    chdir(dirname(__FILE__));
}

require_once "$isearch_path/inc/core.inc.php";
isearch_open();
require_once "$isearch_path/inc/spider.inc.php";

if (!isset($isearch_maxPages))
{
    $isearch_maxPages = 9999999;
}
if (!isset($isearch_spiderDelay))
{
    $isearch_spiderDelay = $isearch_config['spider_delay'];
}
if (!isset($isearch_logEchoLevel))
{
    $isearch_logEchoLevel = $isearch_config['log_echo_level'];
}

@ini_set('max_execution_time', 0);
if ($isearch_fromCommandLine)
{
    if (!isset($isearch_maxSpiderTime))
    {
        $isearch_maxSpiderTime = 9999999;
    }
    if (!isset($reset))
    {
        $reset = 'true';
    }
    $pause = 'false';

    isearch_reset();
}
else
{
    require_once "$isearch_path/inc/admin_auth.inc.php";

    $reset = isset($_REQUEST['reset']) ? $_REQUEST['reset'] : 'true';
    $pause = isset($_REQUEST['pause']) ? $_REQUEST['pause'] : 'false';
    $followmode = isset($_REQUEST['followmode']) ? $_REQUEST['followmode'] : $followmode;
    $indexmode = isset($_REQUEST['indexmode']) ? $_REQUEST['indexmode'] : $indexmode;

    $isearch_maxSpiderTime = 10;
    $isearch_maxPages = 9999999;

    $reindexUrl = "$PHP_SELF?reset=false&followmode=$followmode&indexmode=$indexmode";

    echo <<<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<HEAD>
<TITLE>iSearch Reindex Site</TITLE>
<META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">
<META HTTP-EQUIV="Content-Language" CONTENT="EN-GB">
<META HTTP-EQUIV="Pragma" content="no-cache">
<META HTTP-EQUIV="Expires" content="Fri, 01 Jan 1999 00:00:01 GMT">
<META NAME="robots" CONTENT="noindex,nofollow">
<LINK REL=StyleSheet HREF="admin.css" TYPE="text/css">
</HEAD>

<BODY BGCOLOR="#ffffc0" TEXT="#000000">

<SCRIPT LANGUAGE="JavaScript">
<!--
menuFrame = parent.isearch_reindex_menu;
if (menuFrame != null)
{
    menuDoc = menuFrame.document;
    if (menuDoc.form1.finished != null)
    {
        menuDoc.form1.finished.value = 'false';
    }
}
// -->
</SCRIPT>

EOF;

    if ($pause == 'true')
    {
        $found = isearch_getUrlCount(True);
        $new = isearch_getUrlCount(True, 'new');
        $indexed = $found - $new;

        echo <<<EOF
<SCRIPT LANGUAGE="JavaScript">
<!--
menuFrame = parent.isearch_reindex_menu;
if (menuFrame != null)
{
    menuDoc = menuFrame.document;
    menuDoc.form1.state.value = 'paused [found: $found, indexed: $indexed]';
}
// -->
</SCRIPT>

<H3>Spidering Paused</H3>
<A HREF="$reindexUrl">Resume</A>
</BODY>
</HTML>

EOF;
        isearch_close();
        exit;
    }
    else if ($reset == 'false')
    {
        echo "<H3>Spidering Continuing...</H3>\n";
    }
    else
    {
        echo "<H3>Spidering Starting Now...</H3>\n";
        isearch_reset();
    }

    echo str_pad(' ', 256);
    flush();
}

$startTime = time();
$pageCount = 0;
do
{
    $moreToDo = isearch_indexAFile(True, $followmode, $indexmode);

    if (!$isearch_fromCommandLine)
    {
        echo str_pad(' ', 256);
        flush();
    }

    if ($isearch_spiderDelay > 0)
    {
        sleep($isearch_spiderDelay);
    }

    $currentTime = time();
    $pageCount ++;
} while (($moreToDo) &&
         (($currentTime - $startTime) < $isearch_maxSpiderTime) &&
         ($pageCount < $isearch_maxPages));

if ($isearch_fromCommandLine)
{
    if ($isearch_logEchoLevel > 0)
    {
        echo "\nTotal time " . ($currentTime - $startTime) . " secs for $pageCount pages\n";
        if (($pageCount > 0) && (($currentTime - $startTime) > 0))
        {
            echo ($pageCount / ($currentTime - $startTime)) . " pages per second\n";
        }
    }
}
else
{
    $found = isearch_getUrlCount($moreToDo);
    $new = isearch_getUrlCount($moreToDo, 'new');
    $indexed = $found - $new;

    if ($moreToDo)
    {
        echo <<<EOF
<SCRIPT LANGUAGE="JavaScript">
<!--
newLocation='$reindexUrl';
menuFrame = parent.isearch_reindex_menu;
if (menuFrame != null)
{
    menuDoc = menuFrame.document;
    if (menuDoc.form1.pause.value == 'true')
    {
        newLocation += '&pause=true';
        menuDoc.form1.pause.value = 'false';
    }
    else
    {
        menuDoc.form1.state.value = 'spidering [found: $found, indexed: $indexed]';
    }
}
setTimeout("window.location=newLocation",1000);
// -->
</SCRIPT>

</BODY>

<NOSCRIPT>
<HEAD>
    <META HTTP-EQUIV="REFRESH" CONTENT="1;URL=$reindexUrl">
</HEAD>
</NOSCRIPT>

</HTML>

EOF;
    }
    else
    {
        $count = isearch_getUrlCount();

        echo <<<EOF
<H3>...Spidering Completed.</H3>
<P>Found a total of $count pages.
<P><A HREF="log.php" TARGET="isearch_spider_log">View Spider Log</A>

<SCRIPT LANGUAGE="JavaScript">
<!--
menuFrame = parent.isearch_reindex_menu;
if (menuFrame != null)
{
    menuDoc = menuFrame.document;
    menuDoc.form1.state.value = 'completed [found: $found, indexed: $indexed]';
    menuDoc.form1.finished.value = 'true';
}
// -->
</SCRIPT>

</BODY>
</HTML>

EOF;
        if ($isearch_config['spider_log_email'])
        {
            isearch_emailSpiderLog();
        }
    }
}

isearch_close();

?>
