<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

// PHPLOCKITOPT NOENCODE

$isearch_path = dirname(__FILE__);
@define('IN_ISEARCH', true);

require_once "$isearch_path/inc/core.inc.php";
require_once "$isearch_path/inc/search.inc.php";

/* Open the search component (read only) */
isearch_open(True);

$partial = isset($_REQUEST['partial']) ? intval($_REQUEST['partial']) : ($isearch_config['search_partial'] == 1);
$advanced = isset($_REQUEST['advanced']) ? intval($_REQUEST['advanced']) : 0;

if (isset($_REQUEST['s_all']))
{
    /* Using advanced search form. Build up search string. */
    $advanced = 1;
    $s = '';
    $s_all = strip_tags(trim($_REQUEST['s_all']));
    $s_any = strip_tags(trim($_REQUEST['s_any']));
    if ($s_all != '')
    {
        $s .= '+' . ereg_replace(' +', '+', $s_all);
    }
    if ($s_any != '')
    {
        $s .= ' ' . $s_any;
    }

    $s_exact = strip_tags(trim($_REQUEST['s_exact']));
    if ($s_exact != '')
    {
        $s .= ' ' . '"'.$s_exact.'"';
    }

    if (!$isearch_config['allow_dashes'])
    {
        $s_without = strip_tags(trim($_REQUEST['s_without']));
        if ($s_without != '')
        {
            $s .= '-' . ereg_replace(' +', '-', $s_without);
        }
    }
}
else if (isset($_REQUEST['s']))
{
    $s = strip_tags($_REQUEST['s']);
}
else
{
    $s = '';
}
$s = trim($s);

if (isset($_REQUEST['internet']))
{
    @ob_end_clean();
    $url = 'http://www.iSearchTheNet.com/search.php?s='.urlencode(isearch_cleanSearchString($s)).'&amp;lang='.$isearch_config['lang_name'].'&amp;style='.$isearch_config['style_name'];
    if (headers_sent())
    {
        echo '
<body>
<script type="text/javascript">
<!--
window.location="'.$url.'"
-->
</script>
</body>
<noscript>
<head>
<meta http-equiv="REFRESH" content="1;URL='.$url.'">
</head>
</noscript>
';
    }
    else
    {
        @header( 'Location: '.$url );
    }

    /* Close the search component */
    isearch_close();

    exit;       /*** EXIT ***/
}

/* Check the action and set the page title accordingly */
if (isset($_REQUEST['action']))
{
    $isearch_pageTitle = $isearch_lang['results_title'];
}
else
{
    $isearch_pageTitle = $isearch_lang['search_title'];
}

if ($s == '')
{
    /* We do not have a search string to process */
}
else if ((preg_match('#[\\x0d\\x0a]#', $s)) || (preg_match('#content-type multipartmixed#', $s)))
{
    echo '<p>Probable SPAM hacking attempt detected.</p>
';
}
else
{
    /* Process the search form data */
    if ($isearch_config['char_set'] == 'utf-8')
    {
        $s = utf8_decode($s);
    }

    /* Clean the search strings */
    $s = isearch_cleanSearchString($s);

    if (isset($_REQUEST['groups']))
    {
        $group = '';
        foreach ($_REQUEST['groups'] as $g)
        {
            $g = strip_tags($g);
            if ($g == 'isearch_all')
            {
                $group = '';
                break;
            }

            if ($group != '')
            {
                $group .= ',';
            }
            $group .= $g;
        }
    }
    else if (isset($_REQUEST['group']))
    {
        $group = ($_REQUEST['group'] == 'isearch_all') ? '' : strip_tags($_REQUEST['group']);
    }
    else
    {
        /* Search all groups */
        $group = '';
    }

    if ($isearch_config['soundex'] == 1)
    {
        /* Always use soundex */
        $numResults = isearch_find($s, $group, $partial, True);

        if (($numResults == 0) && (!$partial) && ($isearch_config['search_partial'] == 2))
        {
            $numResults = isearch_find($s, $group, True, True);
        }
    }
    else
    {
        /* Try normal match */
        $numResults = isearch_find($s, $group, $partial, False);

        if (($numResults == 0) && (!$partial) && ($isearch_config['search_partial'] == 2))
        {
            /* Try partial match */
            $numResults = isearch_find($s, $group, True, False);
        }

        if (($numResults == 0) && ($isearch_config['soundex'] == 2))
        {
            /* Try soundex match */
            $numResults = isearch_find($s, $group, $partial, True);
        }
    }

    isearch_showResults(isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1);
}

?>
