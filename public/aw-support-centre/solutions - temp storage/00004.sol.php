<?php

$sol_title = "%%new_version_new_features_title%%";
$sol_keywords = "%%new_version_new_features_keywords%%";
$sol_categories = "[cat=]";

$sol_content = <<<ENDCONTENT
<p>%%new_version_new_features_content%%</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>