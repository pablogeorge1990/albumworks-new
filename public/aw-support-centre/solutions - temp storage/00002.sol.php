<?php

$sol_title = "%%new_version_how_update_title%%";
$sol_keywords = "%%new_version_how_update_keywords%%";
$sol_categories = "[cat=]";

$sol_content = <<<ENDCONTENT
<p>%%new_version_how_update_content%%</p>
ENDCONTENT;

// variable tags should only be in the global variable file

?>