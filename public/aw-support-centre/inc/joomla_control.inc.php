<?php

##############################################################################################################

if(isset($_GET['id']) && $_GET['id'] == '13' && isset($_GET['Itemid']) && $_GET['Itemid'] == '12') {

	$_GET['faqsol_vendor'] = 1031;
	ob_start();

	switch ($_GET['faqsol_page']) {
		case 'browse':
			require_once 'aw-support-centre/handler/browse.php';
			$link_support = 'browse/';
			break;

		case 'contact':
			$_GET['show_form'] = 'true';
			$link_support = 'contact/';
			if (isset($_GET['reason']) && ($_GET['reason'] == 'cd' || $_GET['reason'] == 'samples')) {
				require_once 'aw-support-centre/handler/contact_special.php';
			} else {
				require_once 'aw-support-centre/handler/contact_v3.php';
			}
			break;

		case 'solution':
			require_once 'aw-support-centre/handler/solution.php';
			$link_support = '';
			break;

		default:
			require_once 'aw-support-centre/handler/search.php';
			$link_support = '';
			break;
	}

	$faqsol_output = ob_get_contents();
	$absurl = JURI::base();
	ob_end_clean();
	preg_match("/<body>(.*)<\\/body>/smU",$faqsol_output,$faqsol_processed_output);                   
	$faqsol_processed_output = str_replace(array($absurl.'support/'.$link_support.'http://',$absurl.'support/'.$link_support.'https://',$absurl.'support/solution/foobar/javascript',$absurl.'support/browse/#'),array('http://','https://','javascript','#'),str_replace(array('href="','action="'),array('href="'.$absurl.'support/'.$link_support,'action="'.$absurl.'support/'.$link_support),$faqsol_processed_output[1]));
	echo $faqsol_processed_output;

}

##############################################################################################################

?>