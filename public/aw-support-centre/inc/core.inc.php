<?php

/******************************************************************************
 * iSearch2 - website search engine                                           *
 *                                                                            *
 * Visit the iSearch homepage at http://www.iSearchTheNet.com/isearch         *
 *                                                                            *
 * Copyright (C) 2002-2008 Z-Host. All rights reserved.                       *
 *                                                                            *
 ******************************************************************************/

if ( !defined('IN_ISEARCH') )
{
    die('Hacking attempt');
}

/* Disable notice/warning errors */
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

/* Include configuration options */
require_once "$isearch_path/inc/config.inc.php";

/* Import the list of supported languages */
require_once "$isearch_path/lang/config.inc.php";

$isearch_version = '2.20';

/* Die function - called on a fatal error */
function isearch_die( $error = 'unknown' )
{
    echo "<h1>iSearch Error: $error.</h1>";
    exit;
}


/* Open the iSearch component */
function isearch_open($readOnly = False)
{
    global $isearch_path;
    global $isearch_sql_server, $isearch_sql_username, $isearch_sql_password, $isearch_sql_database;
    global $isearch_sql_ro_username, $isearch_sql_ro_password;
    global $isearch_db, $isearch_ro_db;
    global $isearch_table_info, $isearch_table_urls;

    global $isearch_config;

    /* From language include file */
    global $isearch_lang;
    global $isearch_languageCode;
    global $isearch_lang_config;

	// for some reason the above global vars came through empty, so redeclaring mysql auth details here
	require_once ($_SERVER['DOCUMENT_ROOT'].'/configuration.php');
	$AP_global_db_config = new JConfig;
	$isearch_sql_server   = 'localhost';
	$isearch_sql_username = $AP_global_db_config->user;
	$isearch_sql_password = $AP_global_db_config->password;
	$isearch_sql_database = $AP_global_db_config->db;

    /* Connect to the MySQL database. */
    $isearch_db = mysql_connect($isearch_sql_server, $isearch_sql_username, $isearch_sql_password);
    if (!$isearch_db)
    {	echo mysql_error();
        isearch_die('Unable to connect to MySQL. Please check your iSearch MySQL server, username and password configuration options in <i>isearch2/inc/config.inc.php</i>.');
    }
    if (! mysql_select_db($isearch_sql_database, $isearch_db))
    {
        isearch_die('Unable to select the database. Please check your iSearch MySQL database configuration option in <i>isearch2/inc/config.inc.php</i>.');
    }

    /* If a readonly MySQL database is configured, then open it, else use the
     * default database instead.
     */
    if ($isearch_sql_ro_username != '')
    {
        $isearch_ro_db = mysql_connect($isearch_sql_server, $isearch_sql_ro_username, $isearch_sql_ro_password);

        if (!$isearch_ro_db)
        {
            isearch_die('Unable to connect to MySQL (readonly). Please check your iSearch MySQL server, username and password configuration options in <i>isearch2/inc/config.inc.php</i>.');
        }
        if (! mysql_select_db($isearch_sql_database, $isearch_ro_db))
        {
            isearch_die('Unable to select the database (readonly). Please check your iSearch MySQL database configuration option in <i>isearch2/inc/config.inc.php</i>.');
        }
    }
    else
    {
        $isearch_ro_db = $isearch_db;
    }


    $resultInfo = mysql_query("SELECT * FROM $isearch_table_info", $isearch_ro_db);
    if (($resultInfo) && ($isearch_config = mysql_fetch_array($resultInfo, MYSQL_ASSOC)))
    {
        $explodeVars = array(
            'allowed_ext',
            'allowed_urls',
            'allowed_urls_beginning',
            'exclude_urls',
            'exclude_urls_beginning',
            'groups',
            'remove_get_vars',
            'start_urls',
            'stop_words',
            'strip_defaults',

            'robots_domains',
            'robots_excludes',
            );

        foreach ($explodeVars as $varname)
        {
            if ($isearch_config[$varname] == '')
            {
                $isearch_config[$varname] = array();
            }
            else
            {
                $isearch_config[$varname] = explode(' ', $isearch_config[$varname]);
            }
        }

        /* Check that language is valid */
        if (!in_array($isearch_config['lang_name'], $isearch_lang_config))
        {
            $isearch_config['lang_name'] = 'english';
        }

        include $isearch_path . '/lang/' . $isearch_config['lang_name'] . '.inc.php';

        /* Set maximum execution time */
        @ini_set('max_execution_time', $isearch_config['max_execution_time']);
        switch ($isearch_config['error_reporting'])
        {
            case 2:
                // Disable notices and warnings
                error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                break;
            case 3:
                // Show all errors
                error_reporting(E_ALL);
                break;
            case 4:
                // Turn off all error reporting
                error_reporting(0);
                break;
            default:
                // Disable notices
                error_reporting(E_ALL ^ (E_NOTICE));
                break;
        }

        if ((time() - $isearch_config['update_last_checked']) > (7 * 86400))
        {
            isearch_check_for_update();
        }

        if ($isearch_config['char_set'] == 'utf-8')
        {
            if (! $readOnly)
            {
                mysql_query("SET CHARACTER SET 'utf8'", $isearch_db);
            }
            mysql_query("SET CHARACTER SET 'utf8'", $isearch_ro_db);
        }

        return True;
    }

    return False;
}


/* Close the iSearch component */
function isearch_close()
{
    global $isearch_db, $isearch_ro_db;

    mysql_close($isearch_ro_db);
    if (isset($isearch_db))
    {
        mysql_close($isearch_db);
    }
}


function isearch_getPostVar($var, $default='')
{
    if (isset($_REQUEST[$var]))
    {
        return $_REQUEST[$var];
    }

    return $default;
}


/* Check for an updated version of iSearch */
function isearch_check_for_update()
{
    global $isearch_table_info;
    global $isearch_db;
    global $isearch_config;
    global $isearch_version;

    if (isset($isearch_db))
    {
        $host='www.iSearchTheNet.com';
        $port=80;
        $path='/isearch/version.txt';

        $version = '';
        $msg = array();

        $sock = @fsockopen($host, $port, $errno, $errstr, 1.0);
        if ($sock)
        {
            $request  = "GET http://$host$path HTTP/1.0\r\n";

            $request .= "User-Agent: iSearchPro/$isearch_version\r\n";



            $request .= "Host: $host\r\n";
            $request .= "Referer: http://".$_SERVER['HTTP_HOST'].$isearch_config['base_url']."/\r\n";
            $request .= "Connection: Close\r\n";
            $request .= "\r\n";

            fputs($sock, $request);

            /* Read status line */
            $status = fgets($sock, 1024);

            while (!feof($sock))
            {
                $line = fgets($sock, 1024);
                if ($line == '\r\n')
                {
                    $version = fgets($sock, 1024);
                    $mailSubject = fgets($sock, 1024);
                    while (!feof($sock))
                    {
                        $msg[] = fgets($sock, 1024);
                    }
                    break;
                }
            }
            fclose($sock);
        }

        if ($version != '')
        {
            if ($version != $isearch_version)
            {
                $mailTo = $isearch_config['admin_email'];
                $mailHeaders = "From: " . $isearch_config['admin_email'] . "\n";
                $mailBody = explode("\n", $msg);
                $mailBody .= "\n";
                $mailBody .= "This email is automatically generated by iSearch. To prevent checking\n";
                $mailBody .= "for future updates, please untick the Update Notification setting in\n";
                $mailBody .= "your iSearch admin control panel.\n";
                $mailBody .= "\n";
                $mailBody .= "Visit the iSearch home page at http://www.iSearchTheNet.com/isearch\n";
                if (@mail($mailTo, $mailSubject, $mailBody, $mailHeaders))
                {
                    mysql_query("UPDATE $isearch_table_info SET search_log_last_emailed='$now'", $isearch_db);
                    $isearch_config['search_log_last_emailed'] = $now;
                    isearch_clearSearchLog();
                }
            }
        }

        mysql_query("UPDATE $isearch_table_info SET update_last_checked='" . time() . "' WHERE id='1'", $isearch_db);
    }
}

if (function_exists('mysql_real_escape_string'))
{
    function isearch_escape_string($str)
    {
        return mysql_real_escape_string($str);
    }
}
else
{
    function isearch_escape_string($str)
    {
        return mysql_escape_string($str);
    }
}


if (get_magic_quotes_gpc())
{
    if ((bool) @ini_get(' magic_quotes_sybase'))
    {
        function isearch_stripslashes($str)
        {
            return str_replace("''", "'", $str);
        }
    }
    else
    {
        function isearch_stripslashes($str)
        {
            return stripslashes($str);
        }
    }
}
else
{
    function isearch_stripslashes($str)
    {
        return $str;
    }
}

function isearch_utf8_chr($ch)
{
    if ($ch <= 0x7f)
    {
        return chr($ch);
    }

    if ($ch <= 0x7ff)
    {
        return chr(($ch >> 6) + 0xc0).chr(($ch & 0x3f) + 0x80);
    }

    if ($ch <= 0xffff)
    {
        return chr(($ch >> 12) + 0xe0).chr((($ch >> 6) & 0x3f) + 0x80).chr(($ch & 0x3f) + 0x80);
    }

    if ($ch <= 0x1fffff)
    {
        return chr(($ch >> 18) + 0xf0).chr((($ch >> 12) & 0x3f) + 0x80).chr((($ch >> 6) & 0x3f) + 0x80).chr(($num & 0x3f) + 0x80);
    }

    // Invalid UTF-8 code
    return '';
}

function isearch_html_entity_decode($string, $quote, $charset)
{
    $version = phpversion();
    if ($version{0} == '4')
    {
        if (strtolower($charset) == 'utf-8')
        {
            // PHP4 html_entity_decode does not support multi-byte charsets
            static $utf8_trans_tbl;

            $string = preg_replace('/&#x([0-9a-f]+);/ei', 'isearch_utf8_chr(hexdec("\\1"))', $string);
            $string = preg_replace('/&#([0-9]+);/e', 'isearch_utf8_chr(\\1)', $string);

            if (!isset($utf8_trans_tbl))
            {
                $utf8_trans_tbl = array();

                foreach (get_html_translation_table(HTML_ENTITIES) as $val=>$key)
                {
                    $utf8_trans_tbl[$key] = utf8_encode($val);
                }
            }

            return strtr($string, $utf8_trans_tbl);
        }

        $string = preg_replace('/&#x([0-9a-f]+);?/ei', 'chr(hexdec("\\1"))', $string);
        $string = preg_replace('/&#([0-9]+);?/e', 'chr("\\1")', $string);
    }

    if (!function_exists('html_entity_decode'))
    {
        // html_entity_decode was new in PHP 4.3.0

        global $isearch_htmlToAsciiTrans;
        if (!isset($isearch_htmlToAsciiTrans))
        {
            /* Translate from HTML to ASCII */
            $isearch_htmlToAsciiTrans = array_flip(get_html_translation_table(HTML_ENTITIES));
        }
        return strtr($string, $isearch_htmlToAsciiTrans);
    }

    return html_entity_decode($string, $quote, $charset);
}

function isearch_html_to_ascii($str)
{
    global $isearch_config;

    $str = isearch_html_entity_decode($str, ENT_QUOTES, $isearch_config['char_set']);

    return $str;
}

function isearch_html_entities($str)
{
    global $isearch_config;

    if ($isearch_config['char_set_8_bit'])
    {
        if (version_compare(PHP_VERSION, '4.1.0', '>='))
        {
            $str = @htmlentities($str, ENT_QUOTES, $isearch_config['char_set']);
        }
        else
        {
            $str = @htmlentities($str);
        }
    }
    else
    {
        if (version_compare(PHP_VERSION, '4.1.0', '>='))
        {
            $str = @htmlspecialchars($str, ENT_QUOTES, $isearch_config['char_set']);
        }
        else
        {
            $str = @htmlspecialchars($str);
        }
    }

    return $str;
}



?>
