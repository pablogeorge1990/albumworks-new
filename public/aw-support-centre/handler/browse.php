<?php

// do not edit this code directly, as it is generated by
// fscontrol/sol_list_regen.php and will be overwritten on
// next regeneration of sulutions
// edit source file (fscontrol/sol_list_regen.php) instead

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

require_once 'wrapper_out.php';
require_once 'tag_process.php';

$cat_desc_array = array(	"overview"				=> "General questions about the service we offer",
							"system requirements" 	=> "Questions about the requirements to use our services",
							"getting started" 		=> "Information required to start creating your product",
							"using the software" 	=> "Questions about using the software",
							"ordering / payment" 	=> "Questions about ordering and paying for your product",	
							"products" 				=> "Questions about our products and the quality",						
							"delivery"				=> "Questions about dispatch and delivery",
							"troubleshooting" 		=> "Common errors and troubleshooting steps",
							"generic"				=> "Questions that couldn't fit anywhere else");

$sol_list_array = array(
	0 => array(
		'filename' 	=> '60020-paper.sol.php', 
		'title' 	=> 'What type of paper do you use? What is the life span of the paper?', 
		'cat' 		=> array(
					'0' => 'products', ),), 
	1 => array(
		'filename' 	=> '10010-photobooks.sol.php', 
		'title' 	=> 'What are photobooks?  How do I create my own?', 
		'cat' 		=> array(
					'0' => 'overview', ),), 
	2 => array(
		'filename' 	=> '10030-design-for-me.sol.php', 
		'title' 	=> 'Can you design my album for me?', 
		'cat' 		=> array(
					'0' => 'overview', ),), 
	3 => array(
		'filename' 	=> '10050-custom-product.sol.php', 
		'title' 	=> 'Can you produce a product not on your product list?', 
		'cat' 		=> array(
					'0' => 'overview', ),), 
	4 => array(
		'filename' 	=> '10060-delivery-time.sol.php', 
		'title' 	=> 'How long will it take to produce and deliver my order?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', 
					'1' => 'delivery', 
					'2' => 'overview', ),), 
	5 => array(
		'filename' 	=> '10070-product-range.sol.php', 
		'title' 	=> 'What products are available and how much do they cost?', 
		'cat' 		=> array(
					'0' => 'product', 
					'1' => 'overview', ),), 
	6 => array(
		'filename' 	=> '10080-gift-vouchers.sol.php', 
		'title' 	=> '%%vendor_giftvouchers_title%%', 
		'cat' 		=> array(
					'0' => 'overview', ),), 
	7 => array(
		'filename' 	=> '10090-samples.sol.php', 
		'title' 	=> 'Can I see some print samples of your products?', 
		'cat' 		=> array(
					'0' => 'overview', ),), 
	8 => array(
		'filename' 	=> '20010-system-requirements.sol.php', 
		'title' 	=> 'What are the System Requirements to use your editor?', 
		'cat' 		=> array(
					'0' => 'system requirements', ),), 
	9 => array(
		'filename' 	=> '20020-mac-os.sol.php', 
		'title' 	=> 'I have an Apple Mac. Can I use your services?', 
		'cat' 		=> array(
					'0' => 'system requirements', ),), 
	10 => array(
		'filename' 	=> '20030-windows-versions.sol.php', 
		'title' 	=> 'What versions of Windows does the editor work on?', 
		'cat' 		=> array(
					'0' => 'system requirements', ),), 
	11 => array(
		'filename' 	=> '20040-need-internet.sol.php', 
		'title' 	=> 'Do I need an internet connection?', 
		'cat' 		=> array(
					'0' => 'system requirements', ),), 
	12 => array(
		'filename' 	=> '20050-dial-up.sol.php', 
		'title' 	=> 'I have a Dial-Up or Satellite internet connection. Can I still use your services?', 
		'cat' 		=> array(
					'0' => 'system requirements', ),), 
	13 => array(
		'filename' 	=> '30010-download-install.sol.php', 
		'title' 	=> 'How do I download and install the editor?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	14 => array(
		'filename' 	=> '30020-save-project.sol.php', 
		'title' 	=> 'Do I have to complete my project in one sitting? Can I save my project and come back to it later?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	15 => array(
		'filename' 	=> '30020-scan.sol.php', 
		'title' 	=> 'Can I scan developed photos to use in my project?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	16 => array(
		'filename' 	=> '30030-increase-quality.sol.php', 
		'title' 	=> 'Can I increase the quality of my photos?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	17 => array(
		'filename' 	=> '30035-photo-quality.sol.php', 
		'title' 	=> 'How do I know if the quality of my photos is high enough to use in my project?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	18 => array(
		'filename' 	=> '30037-no-photos.sol.php', 
		'title' 	=> 'How many photos should I have in my project?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	19 => array(
		'filename' 	=> '30040-editor-not-use.sol.php', 
		'title' 	=> 'Do I have to use your editor? Can I use another program instead?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	20 => array(
		'filename' 	=> '30080-supply-pdf.sol.php', 
		'title' 	=> 'Can I supply you with a PDF, iPhoto or any other file to make into an album?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	21 => array(
		'filename' 	=> '30100-print-vs-monitor.sol.php', 
		'title' 	=> 'Will my project look identical to how it appears on my computer monitor?', 
		'cat' 		=> array(
					'0' => 'getting started', ),), 
	22 => array(
		'filename' 	=> '40100-add-pages.sol.php', 
		'title' 	=> 'How do I add more pages to my photobook?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	23 => array(
		'filename' 	=> '40110-move-pages.sol.php', 
		'title' 	=> 'How do I move pages within my project?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	24 => array(
		'filename' 	=> '40200-background-colour.sol.php', 
		'title' 	=> 'How do I set a background colour?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	25 => array(
		'filename' 	=> '40210-background-image.sol.php', 
		'title' 	=> 'How do I insert my own image as a background?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	26 => array(
		'filename' 	=> '40220-background-more.sol.php', 
		'title' 	=> 'How do I access more background images?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	27 => array(
		'filename' 	=> '40300-border-apply.sol.php', 
		'title' 	=> 'How do I apply a ##version_instructions_fr1## to my photos?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	28 => array(
		'filename' 	=> '40400-move-photo.sol.php', 
		'title' 	=> 'How do I move a photo between pages?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	
	29 => array(
		'filename' 	=> '40420-aspect-ratio.sol.php', 
		'title' 	=> '%%vendor_reshape_photo%%', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	30 => array(
		'filename' 	=> '40430-exclamation-mark.sol.php', 
		'title' 	=> '%%vendor_exclamation_mark_title%%', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	31 => array(
		'filename' 	=> '40440-pixellation.sol.php', 
		'title' 	=> 'Why do my photos look pixellated in the work area of the editor?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	32 => array(
		'filename' 	=> '40450-rotate-photo.sol.php', 
		'title' 	=> 'How do I rotate a photo?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	33 => array(
		'filename' 	=> '40455-print-area.sol.php', 
		'title' 	=> 'What is the print area of a page?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	34 => array(
		'filename' 	=> '40460-stretch-image-twopages.sol.php', 
		'title' 	=> 'Can I stretch a photo across two pages in an album?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	35 => array(
		'filename' 	=> '40470-full-page-spread.sol.php', 
		'title' 	=> 'How do I spread a photo over an entire page?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	36 => array(
		'filename' 	=> '40500-add-text.sol.php', 
		'title' 	=> 'How do I add text to a page?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	37 => array(
		'filename' 	=> '40520-rotate-text.sol.php', 
		'title' 	=> 'How do I rotate a text box?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	38 => array(
		'filename' 	=> '40530-font-size.sol.php', 
		'title' 	=> 'What font size is best for my project?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
    39 => array(
        'filename'  => '40870-copy-project.sol.php', 
        'title'     => 'How do I copy my project to a new location?', 
        'cat'       => array(
                    '0' => 'using the software', ),),                     
	40 => array(
		'filename' 	=> '40540-font-support.sol.php', 
		'title' 	=> 'Can I use unusual fonts in my project?', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	41 => array(
		'filename' 	=> '40800-locate-project.sol.php', 
		'title' 	=> '%%vendor_locate_project_title%%', 
		'cat' 		=> array(
					'0' => 'using the software', ),), 
	42 => array(
		'filename' 	=> '50001-secure-payment.sol.php', 
		'title' 	=> 'What measures do you use to keep my credit card details secure?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	43 => array(
		'filename' 	=> '50010-verified-by-visa.sol.php', 
		'title' 	=> '%%vendor_vbv_title%%', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	44 => array(
		'filename' 	=> '50011-mastercard-securecode.sol.php', 
		'title' 	=> '%%vendor_mastercard_securecode_title%%', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	45 => array(
		'filename' 	=> '50020-promotion-code.sol.php', 
		'title' 	=> 'How do I use my promotion code?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	46 => array(
		'filename' 	=> '50030-reorder-process.sol.php', 
		'title' 	=> 'I\'ve received my album and would like to order more copies, what should I do?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', 
					'1' => 'delivery', ),), 
	47 => array(
		'filename' 	=> '50080-multiple-address.sol.php', 
		'title' 	=> 'Can I send copies of my album to different addresses?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	48 => array(
		'filename' 	=> '50090-multiple-copies-order.sol.php', 
		'title' 	=> 'Can I order multiple copies in a single order?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	49 => array(
		'filename' 	=> '50100-faulty-order.sol.php', 
		'title' 	=> 'What happens if I\'m not happy with my product?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', 
					'1' => 'overview', ),), 
	50 => array(
		'filename' 	=> '50210-draft-print.sol.php', 
		'title' 	=> 'Can I request a draft print of my project before ordering?', 
		'cat' 		=> array(
					'0' => 'using the software', 
					'1' => 'ordering / payment', ),), 
	51 => array(
		'filename' 	=> '50240-optimise-process.sol.php', 
		'title' 	=> 'What is the process when my project is uploaded?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	52 => array(
		'filename' 	=> '60005-quicker-delivery.sol.php', 
		'title' 	=> 'Can I request quicker production of my order?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', ),), 
	53 => array(
		'filename' 	=> '60030-binding.sol.php', 
		'title' 	=> 'What binding method do you use to create your albums?', 
		'cat' 		=> array(
					'0' => 'products', ),), 
	54 => array(
		'filename' 	=> '60040-covers.sol.php', 
		'title' 	=> 'What cover options do you have available?', 
		'cat' 		=> array(
					'0' => 'products', ),), 
	55 => array(
		'filename' 	=> '60050-cover-title.sol.php', 
		'title' 	=> 'What is a cover title and how do I add it?', 
		'cat' 		=> array(
					'0' => 'using the software', 
					'1' => 'products', ),), 
	56 => array(
		'filename' 	=> '60110-dust-cover.sol.php', 
		'title' 	=> 'Can I create a dust cover/jacket for my album?', 
		'cat' 		=> array(
					'0' => 'products', ),), 
	57 => array(
		'filename' 	=> '60150-photo-cover-description.sol.php', 
		'title' 	=> 'What is a Photocover? How do I add/remove it to/from my album?', 
		'cat' 		=> array(
					'0' => 'using the software', 
					'1' => 'products', ),), 
	58 => array(
		'filename' 	=> '60200-print-process.sol.php', 
		'title' 	=> 'How are your products printed?', 
		'cat' 		=> array(
					'0' => 'products', ),), 
	59 => array(
		'filename' 	=> '70020-delivery-method.sol.php', 
		'title' 	=> 'How are your products delivered?', 
		'cat' 		=> array(
					'0' => 'delivery', ),), 
	60 => array(
		'filename' 	=> '70040-packaging.sol.php', 
		'title' 	=> 'How are your products packaged?', 
		'cat' 		=> array(
					'0' => 'delivery', ),), 
	61 => array(
		'filename' 	=> '70060-track-trace.sol.php', 
		'title' 	=> 'Can I track and trace the delivery of my order?', 
		'cat' 		=> array(
					'0' => 'delivery', 
					'1' => 'overview', ),), 
	62 => array(
		'filename' 	=> '70100-international-delivery.sol.php', 
		'title' 	=> 'Do you deliver outside of Austraila?', 
		'cat' 		=> array(
					'0' => 'delivery', ),), 
	
	63 => array(
		'filename' 	=> '80030-freezes-startup.sol.php', 
		'title' 	=> '%%vendor_freezes_startup_title%%', 
		'cat' 		=> array(
					'0' => 'troubleshooting', ),), 
	64 => array(
		'filename' 	=> '80050-apc-error.sol.php', 
		'title' 	=> '%%vendor_apc_exe_error_title%%', 
		'cat' 		=> array(
					'0' => 'troubleshooting', ),), 
	65 => array(
		'filename' 	=> '80110-empty-page-warning.sol.php', 
		'title' 	=> 'Why am I receiving an \'empty page\' warning?', 
		'cat' 		=> array(
					'0' => 'troubleshooting', ),), 
	66 => array(
		'filename' 	=> '80120-empty-photo-frame-warning.sol.php', 
		'title' 	=> 'Why am I receiving an \'empty photo frame\' or \'empty text box\' warning?', 
		'cat' 		=> array(
					'0' => 'troubleshooting', ),), 
	67 => array(
		'filename' 	=> '80009-upload-error.sol.php', 
		'title' 	=> 'Why am I having trouble uploading? What does \'Cannot Connect to Server\' mean?', 
		'cat' 		=> array(
					'0' => 'ordering / payment', 
					'1' => 'troubleshooting', ),), 
	68 => array(
		'filename' 	=> '80005-upgrade-error.sol.php', 
		'title' 	=> '%%vendor_upgrade_failed_title%%', 
		'cat' 		=> array(
					'0' => 'troubleshooting', ),), 
	
	69 => array(
		'filename' 	=> '40820-change-size.sol.php', 
		'title' 	=> 'Can I change the type or size of my album once I have started?', 
		'cat' 		=> array(
					'0' => 'using the software', 
					'1' => 'products', ),), 
					
	70 => array(
		'filename' 	=> '40830-page-count.sol.php', 
		'title' 	=> 'How many pages can I have?', 
		'cat' 		=> array(
					'0' => 'using the software', 
					'1' => 'products', ),), 	
					
	71 => array(
        'filename'     => '80200-restart-upload.sol.php', 
        'title'     => 'My upload did not complete. How do I start it again without re-ordering?', 
        'cat'         => array(
                    '0' => 'troubleshooting', ),), 			
	72 => array(
        'filename'     => '80210-cancel-order.sol.php', 
        'title'     => 'Can I alter or cancel my order?', 
        'cat'         => array(
                    '0' => 'troubleshooting', ),), 
										
	73 => array(
        'filename'     => '89437-missing-photos-new-editor.sol.php', 
        'title'     => 'Why am I getting a \'Missing Photos\' warning?', 
        'cat'         => array(
                    '0' => 'troubleshooting', ),), 				
);

foreach ($sol_list_array as $key => $field) {
	if (count($sol_list_array[$key]['cat']) > 0) {;
		foreach ($sol_list_array[$key]['cat'] as $sub_key => $sub_field) {
			$sol_list_cats[$sub_field][$key]['filename'] = $sol_list_array[$key]['filename'];
			list($solurlpart,,) = explode('.',$sol_list_array[$key]['filename']);
			$sol_list_cats[$sub_field][$key]['urlpart'] = $solurlpart;
			unset($solurlpart);
			$sol_list_cats[$sub_field][$key]['title'] = $sol_list_array[$key]['title'];
		}
	} else {
		foreach ($sol_list_array[$key] as $sub_key => $sub_field) {
			$sol_list_cats['generic'][$key]['filename'] = $sol_list_array[$key]['filename'];
			list($solurlpart,,) = explode('.',$sol_list_array[$key]['filename']);
			$sol_list_cats['generic'][$key]['urlpart'] = $solurlpart;
			unset($solurlpart);
			$sol_list_cats['generic'][$key]['title'] = $sol_list_array[$key]['title'];
		}
	}
}

unset($sol_list_array);

$vendor = $_GET['faqsol_vendor'];

//wrapper_start_html($vendor,'Browse','browse');
echo '			<div class="sol_browse_head">
				<span class="sol_browse_head_title">Browse Solutions</span>
				<span class="sol_browse_head_desc">Click the categories below to display relevant questions.</span>				
			</div>
			<div class="sol_browse_body">
';

foreach ($cat_desc_array as $key => $value) {
	if (isset($sol_list_cats[$key])) {
		$sol_list_cats_new[$key] = $sol_list_cats[$key];
	}
}
$sol_list_cats = $sol_list_cats_new;
unset($sol_list_cats_new);

$sol_cat_iter = 0;
foreach ($sol_list_cats as $cat => $data) {
	$sol_cat_iter++;
	echo "\t\t\t\t".'<div class="sol_cat">'."\n";
	echo "\t\t\t\t\t".'<div class="sol_cat_head">'."\n";
	echo "\t\t\t\t\t\t".'<span class="sol_cat_title"><a href="#'.$sol_cat_iter.'" class="sol_cat_title_link">'.str_replace("The","the",ucwords($cat)).'</a></span>'."\n";
	echo "\t\t\t\t\t\t".'<span class="sol_cat_desc">';
	if (isset($cat_desc_array[strtolower($cat)])) {
		echo $cat_desc_array[strtolower($cat)];
	}
	echo '</span>'."\n";
	echo "\t\t\t\t\t".'</div>'."\n";
	echo "\t\t\t\t\t".'<div class="sol_cat_body">'."\n";
		echo "\t\t\t\t\t\t".'<ul>'."\n";
	foreach ($sol_list_cats[$cat] as $sub_key => $sol) {
		$sol_list_cats[$cat][$sol_list_cats[$cat][$sub_key]['urlpart']] = $sol;
		unset ($sol_list_cats[$cat][$sub_key]);
	}
	ksort($sol_list_cats[$cat]);
	foreach ($sol_list_cats[$cat] as $sub_key => $sol) {
		if ((tag_process($sol_list_cats[$cat][$sub_key]['title'],$vendor) != '') && (preg_match("/(%%|##)/is",tag_process($sol_list_cats[$cat][$sub_key]['title'],$vendor)) == 0)) {
		// check for solutions which don't belong to this particular vendor
			echo "\t\t\t\t\t\t\t".'<li class="sol_link_li"><a href="solution/'.$sol_list_cats[$cat][$sub_key]['urlpart'].'/" class="sol_link">'.tag_process($sol_list_cats[$cat][$sub_key]['title'],$vendor).'</a></li>'."\n";
		}
	}
	echo "\t\t\t\t\t\t".'</ul>'."\n";
	echo "\t\t\t\t\t".'</div>'."\n";
	echo "\t\t\t\t".'</div>'."\n";
}

echo "\t\t\t".'</div>'."\n";

		switch($vendor) {
			case '1031':
				$contact_link = '../contact/';
				break;
			case '3183':
				$contact_link = 'http://www.officeworks.com.au/owbd/b2c/init.do?landing=photobookcontact.shtml" target="_top';
				break;
			case '3255':
				$contact_link = 'http://www.target.com.au/photobooks/contact_photobooks.htm" target="_top';
				break;
		}
// edit 13/11/08 - don't show contact link in browse - only when solution is clicked
// echo "\t\t\t",'<div class="contact_link"><span class="contact_link_pre">Can\'t find what you\'re looking for?</span><a class="contact_link_a" href="'.$contact_link.'">Contact Us</a><span class="poweredby_image">Powered by Albumprinter</span></div>',"\n";
//wrapper_end_html();

?>