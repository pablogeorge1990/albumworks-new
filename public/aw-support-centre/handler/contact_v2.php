<?php
if(isset($_POST['query'])) {

	$isearch_path = dirname(__FILE__).'/..';
	define('IN_ISEARCH', TRUE);

	require_once "$isearch_path/inc/core.inc.php";
	require_once "$isearch_path/inc/search.inc.php";

	isearch_open(TRUE);
	$vendor = $_POST['vendor'];
	$search_query = isearch_cleanSearchString($_POST['query']);
	$numResults = isearch_find($search_query,$vendor);
	$results = isearch_getResultArray(1,20);
		
	// echo "\n",rand(100000,999999),"\n"; // qc number to show ajax responses
	foreach($results as $key => $result) {
		if($key < 5) {
			echo '<a href="'.preg_replace('/http:\\/\\/www.albumworks.com.au\\/aw-support-centre\\/(1031|3183|3255)\\//','../',$result['url']).'" onclick="ajax_sol(\''.preg_replace('/http:\\/\\/www.albumworks.com.au\\/aw-support-centre\\/(1031|3183|3255)\\//','../',$result['url']).'\'); return false;">'.preg_replace('#\[cat=[-0-9a-zA-Z/ ]+\]#','',$result['title']).'</a>'."\n";
		}
	}

} else if(isset($_GET['done']) && $_GET['done'] == 'true') {
	
	if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

	require_once 'wrapper_out.php';
	$vendor = $_GET['faqsol_vendor'];

		if ($vendor == '1031') {
			$hide_tabs = FALSE;
		} else {
			$hide_tabs = TRUE;
		}

		wrapper_start_html($vendor,'Contact Us','contact','',TRUE,NULL,'',FALSE,$hide_tabs);

		echo <<<ENDECHO
				<div class="contact_form_container">
					<div class="contact_form_head">
						<span class="contact_form_title">Contact Us</span>
						<span class="contact_form_desc">Thanks for taking the time to contact us.<br /><br />One of our friendly support team will get back to you shortly. In the mean time please check out the rest of our support centre to get an immediate answer to your query.</span>
					</div>

				</div>
				
				<pre id="response">
				</pre>
				


ENDECHO;

	wrapper_end_html();
	
} else {

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

require_once 'wrapper_out.php';
$vendor = $_GET['faqsol_vendor'];

$doneurl = $_SERVER["SERVER_NAME"].$_SERVER['REDIRECT_URL'];
$doneurl[(int)(strlen($doneurl)-1)] = '-';
$doneurl.='thanks/';

	if ($vendor == '1031') {
		$hide_tabs = FALSE;
	} else {
		$hide_tabs = TRUE;
	}

	wrapper_start_html($vendor,'Contact Us','contact','',TRUE,NULL,'',FALSE,$hide_tabs);

	echo <<<ENDECHO
			<div class="contact_form_container">
				<div class="contact_form_head">
					<span class="contact_form_title">Contact Us</span>
					<span class="contact_form_desc">If you would like to contact us, please fill in the form below and our friendly support staff will get back to you as soon as possible. <br />If your enquiry is regarding your order, please include your AlbumID.</span>
				</div>
				<div class="contact_form_body">
					<form action="https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8" method="post" onsubmit="return validateForm();">
						<fieldset>
							<input type="hidden" name="orgid" value="00D36000000oZE6" />
							<input type="hidden" name="retURL" value="http://{$doneurl}" />
							<input type="hidden" name="00N20000001Qe3d" value="$vendor" />
							<input type="hidden"  id="external" name="external" value="1" />
							<input type="hidden" name="status" value="New" />
						</fieldset>
						<fieldset>
							<label for="contact_form_name">First Name:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="50" name="00N20000001SbZB" class="rem" id="contact_form_name"  />
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_lname">Last Name:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="50" name="00N20000001SbZz" class="rem" id="contact_form_lname" />
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_email">Email:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="100" name="email" class="rem" id="contact_form_email" />
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_subject">Subject:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="200" name="subject" class="rem" id="contact_form_subject" />
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_albumid">AlbumID (optional):</label>
							<input type="text" maxlength="50" name="contact_form_albumid" class="rem" id="contact_form_albumid" title="9 digit number starting with 1031" />
							
							<hr class="contact_input_divider" />
							
							<label for="faqsol_search_input_contact">Query/Message:<span class="contact_form_label_star">*</span></label>
							<textarea name="description" id="faqsol_search_input_contact" class="maxlimit3k rem" rows="6" cols="50"></textarea>
							
							<input type="submit" id="contact_form_submit" name="contact_form_submit" value="Submit" />
						</fieldset>
					</form>
				</div>
			</div>
			
			<pre id="response">
			</pre>
			
			<script type="text/javascript">
				// document.onload = get_results_init(); // not at this stage

				function get_results_init() {
					var waiting;
					var desc_field = document.getElementById("faqsol_search_input_contact");

					desc_field.onkeyup = desc_field.onchange = function() {
						if(waiting != 1) {
							waiting = 1;
							var http = new XMLHttpRequest();
							var url = "{$_SERVER['REQUEST_URI']}";
							var query = document.getElementById("faqsol_search_input_contact").value;
							var params = "vendor={$vendor}&query="+query;
							http.open("POST", url, true);

							// send the proper header information along with the request
							http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							http.setRequestHeader("Content-length", params.length);
							http.setRequestHeader("Connection", "close");

							http.onreadystatechange = function() { // output the data when the state changes
								if(http.readyState == 4 && http.status == 200) {
									document.getElementById("response").innerHTML = (http.responseText);
									waiting = 0;
								}
							}
							http.send(params);
						}
					}
				}
				
				function isValidEmail(str) {
				  // These comments use the following terms from RFC2822:
				  // local-part, domain, domain-literal and dot-atom.
				  // Does the address contain a local-part followed an @ followed by a domain?
				  // Note the use of lastIndexOf to find the last @ in the address
				  // since a valid email address may have a quoted @ in the local-part.
				  // Does the domain name have at least two parts, i.e. at least one dot,
				  // after the @? If not, is it a domain-literal?
				  // This will accept some invalid email addresses
				  // BUT it doesn't reject valid ones. 
				  var atSym = str.lastIndexOf("@");
				  if (atSym < 1) { return false; } // no local-part
				  if (atSym == str.length - 1) { return false; } // no domain
				  if (atSym > 64) { return false; } // there may only be 64 octets in the local-part
				  if (str.length - atSym > 255) { return false; } // there may only be 255 octets in the domain

				  // Is the domain plausible?
				  var lastDot = str.lastIndexOf(".");
				  // Check if it is a dot-atom such as example.com
				  if (lastDot > atSym + 1 && lastDot < str.length - 1) { return true; }
				  //  Check if could be a domain-literal.
				  if (str.charAt(atSym + 1) == '[' &&  str.charAt(str.length - 1) == ']') { return true; }
				  return false;
				}


				function validateForm(){
					var valid = 1;
					var fields = ["contact_form_name","contact_form_lname","contact_form_subject","faqsol_search_input_contact"];
					for (var field in fields) {
						if(document.getElementById(fields[field])) {
							if(document.getElementById(fields[field]).value == "") {
								valid = 0;
								jQuery("label[for='"+fields[field]+"']").addClass("wrong");
							} else {
								jQuery("label[for='"+fields[field]+"']").removeClass("wrong");
							}
						}
					}

					var emailfields = ["contact_form_email"];
					for (var field in emailfields) {
						if(document.getElementById(emailfields[field])) {
							if(!isValidEmail(document.getElementById(emailfields[field]).value)) {
								valid = 0;
								jQuery("label[for='"+emailfields[field]+"']").addClass("wrong");
							} else {
								jQuery("label[for='"+emailfields[field]+"']").removeClass("wrong");
							}
						}
					}

					if(valid == 0) { 
						alert("Please ensure all the fields are filled correctly, and your email is valid");
						return false;
					}
					document.getElementById("faqsol_search_input_contact").value = "AlbumID: " + (document.getElementById("contact_form_albumid").value + "\\n\\n" + document.getElementById("faqsol_search_input_contact").value);
					return true;
				}
				
				function ajax_sol(url) {
					var waiting;
					if(waiting != 1) {
						waiting = 1;
						var http = new XMLHttpRequest();
						var params = "ajax=1";
						http.open("POST", url, true);

						// send the proper header information along with the request
						http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
						http.setRequestHeader("Content-length", params.length);
						http.setRequestHeader("Connection", "close");

						http.onreadystatechange = function() { // output the data when the state changes
							if(http.readyState == 4 && http.status == 200) {
								document.getElementById("response").innerHTML = (http.responseText);
								waiting = 0;
							}
						}
						http.send(params);
					}
				}

			</script>

ENDECHO;

	wrapper_end_html();

}

?>