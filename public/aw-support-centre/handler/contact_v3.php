<?php
if(isset($_POST['query'])) {
	
	define('ajax_search','1'); // stop logging of queries

	$isearch_path = dirname(__FILE__).'/..';
	define('IN_ISEARCH', TRUE);

	require_once "$isearch_path/inc/core.inc.php";
	require_once "$isearch_path/inc/search.inc.php";

	isearch_open(TRUE);
	$vendor = $_POST['vendor'];
	$search_query = isearch_cleanSearchString($_POST['query']);
	$numResults = isearch_find($search_query,$vendor);
	$results = isearch_getResultArray(1,20);
		
	//echo "\n",rand(100000,999999),"\n"; // qc number to show ajax responses
	echo '<ul class="ajax_response">',"\n",'<label>Suggested Solutions:<span class="contact_form_label_star">&nbsp;</span></label>';
	if(($vendor == '1031') && isset($_POST['apincluded'])) {
		$sol_url_pre = '/support/';
	} else {
		$sol_url_pre = '../';
	}
	foreach($results as $key => $result) {
		if($key < 5) {
			echo '<li class="ajax_response"><a class="ajax_response" href="'.preg_replace('/http:\\/\\/www.albumworks.com.au\\/aw-support-centre\\/(1031|3183|3255)\\//',$sol_url_pre,$result['url']).'">'.str_replace('[cat=]','',preg_replace('#\[cat=[-0-9a-zA-Z/ ]+\]#','',$result['title'])).'</a></li>'."\n";
		}
	}
	echo '</ul>';

} else if(isset($_GET['done']) && $_GET['done'] == 'true') {
	
	if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

	require_once 'wrapper_out.php';
	$vendor = $_GET['faqsol_vendor'];

		if ($vendor == '1031') {
			$hide_tabs = FALSE;
		} else {
			$hide_tabs = TRUE;
		}

		wrapper_start_html($vendor,'Contact Us','contact','',TRUE,NULL,'',FALSE,$hide_tabs);
		
		if(isset($_COOKIE['email']) && $_COOKIE['email'] != '') {
			$email_msg = 'We\'ll also send an automatic email to <strong>'.$_COOKIE['email'].'</strong> - please make sure this email is correct.';
		}
        else $email_msg = '';

		echo <<<ENDECHO
				<div class="contact_form_container">
					<div class="contact_form_head">
						<span id="remclear" class="description contact_form_albumid contact_form_subject"><!-- required to clear cookies -->
						<h2 class="sectionheader">Contact Us</h2>
						<span class="contact_form_desc">Thanks for taking the time to contact us.<br /><br />One of our friendly support team will get back to you shortly. {$email_msg} In the mean time please check out the rest of our support centre to get an immediate answer to your query.</span>
					</div>

				</div>


ENDECHO;

	wrapper_end_html();
	
} else {

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

require_once 'wrapper_out.php';
$vendor = $_GET['faqsol_vendor'];

$thisurl = $_SERVER["SERVER_NAME"].$_SERVER['REDIRECT_URL'];
$doneurl = $thisurl;
	if($vendor == '1031'){
	$doneurl[(int)(strlen($doneurl))] = '-';
	}
	else{
	$doneurl[(int)(strlen($doneurl)-1)] = '-';
	}
$doneurl.='thanks';

	if ($vendor == '1031') {
		$hide_tabs = FALSE;
	} else {
		$hide_tabs = TRUE;
	}

	wrapper_start_html($vendor,'Contact Us','contact','',TRUE,NULL,'',FALSE,$hide_tabs);

	if(strstr($_SERVER['REQUEST_URI'],'aw-support-centre') == FALSE) {
		$apincluded = 'apincluded=true&';
	} else {
		$apincluded = '';
	}
	if($vendor == 3183 && $_SERVER["REMOTE_ADDR"] == '203.6.240.254') {
	echo <<<ENDECHO
	
	<div style="height:50px;width:580px;"></div>
	<div class="form-heading">
		<h2>Contact us</h2>
		<div>
			Required information <span class="asterix">*</span>
		</div>
	</div>
	<div class="form-body">
		<form class="contact-form" action="https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8" method="post" onsubmit="return validateForm();">
			<fieldset>
				<input type="hidden" name="orgid" value="00D36000000oZE6" />
				<input type="hidden" name="retURL" value="http://{$doneurl}" />
				<input type="hidden" name="status" value="New" />
				<input type="hidden" name="00N20000001Qe3d" value="$vendor" />
				<input type="hidden"  id="external" name="external" value="1" />
			</fieldset>
			<fieldset>
				
				<div class="input-field"><label for="contact_form_name">First Name<span class="asterix">*</span></label><input type="text" value=" " maxlength="50" name="00N20000001SbZB" rel="First name" class="rem smartedit" id="contact_form_name" /></div>
				<div class="input-field"><label for="contact_form_lname">Last Name<span class="asterix">*</span></label><input type="text" value=" " maxlength="50" name="00N20000001SbZz" rel="Last name" class="rem smartedit" id="contact_form_lname" /></div>
								
				<div class="input-field"><label for="contact_form_email">Email<span class="asterix">*</span></label><input type="text" value=" " maxlength="100" name="email" rel="Email" class="rem smartedit" id="contact_form_email" /></div>
				<div class="input-field"><label for="contact_form_albumid">Order ID</label><input type="text" value=" " maxlength="50" name="contact_form_albumid" rel="AlbumID" class="rem smartedit" id="contact_form_albumid" title="9 digit number starting with 1031" /></div>

				<div class="input-field"><label for="contact_form_subject">Subject<span class="asterix">*</span></label><input type="text" value=" " maxlength="200" name="subject" rel="Subject" class="rem smartedit" id="contact_form_subject" /></div>
				
				<div class="input-field message-box">
					<label for="faqsol_search_input_contact">Message<span class="asterix">*</span></label>
					<textarea name="description" id="faqsol_search_input_contact" class="maxlimit3k rem" rows="7" cols="25"></textarea>
				</div>
				
				<div id="ajax_response">
				</div>
				
				<input type="submit" id="contact_form_submit" name="contact_form_submit" value="Submit" />
			</fieldset>
		</form>	
	</div>
	<div class="form-bottom">
	</div>

ENDECHO;
	} else {
	echo <<<ENDECHO
		<div class="contact_form_container">
				<div class="contact_form_head">
					<h2 class="sectionheader">Contact Us</h2>
					<span class="contact_form_desc">If you would like to contact us, please fill in the form below and our friendly support staff will get back to you as soon as possible. <br />If your enquiry is regarding your order, please include your AlbumID.</span>
				</div>
				<div class="contact_form_body">
					<form action="https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8" method="post" onsubmit="return validateForm();">
						<fieldset>
							<input type="hidden" name="orgid" value="00D36000000oZE6" />
							<input type="hidden" name="retURL" value="http://{$doneurl}" />
							<input type="hidden" name="status" value="New" />
							<input type="hidden" name="00N20000001Qe3d" value="$vendor" />
							<input type="hidden"  id="external" name="external" value="1" />
						</fieldset>
						<fieldset>
							
							<label for="contact_form_name"><input type="text" value="First name" maxlength="50" name="00N20000001SbZB" rel="First name" class="rem smartedit" id="contact_form_name" /></label>
							<label for="contact_form_lname"><input type="text" value="Last name" maxlength="50" name="00N20000001SbZz" rel="Last name" class="rem smartedit" id="contact_form_lname" /></label>
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_email"><input type="text" value="Email" maxlength="100" name="email" rel="Email" class="rem smartedit" id="contact_form_email" /></label>
							<label for="contact_form_albumid"><input type="text" value="AlbumID (optional)" maxlength="50" name="contact_form_albumid" rel="AlbumID" class="rem smartedit" id="contact_form_albumid" title="9 digit number starting with 1031" /></label>
							
							<hr class="contact_input_divider" />

							<label for="contact_form_subject"><input type="text" value="Subject" maxlength="200" name="subject" rel="Subject" class="rem smartedit" id="contact_form_subject" /></label>
							
							<hr class="contact_input_divider" />
							
							<div class="query_wrapper">
							<textarea name="description" id="faqsol_search_input_contact" class="maxlimit3k rem smartedit" rows="14" cols="25">Query / Message</textarea>
							</div>
							
							<div id="ajax_response">
							</div>
							
							<input type="submit" id="contact_form_submit" name="contact_form_submit" value="Submit" />
						</fieldset>
					</form>

				</div>
				
			</div>
			<span class="get-down-girl-go-head-get-down"><strong class="get-down-girl-go-head-get-down">&#8595;</strong> more</span>
ENDECHO;
	}
	
echo <<<ENDECHO
			<script type="text/javascript">
			//<![CDATA[
				document.onload = get_results_init();

				function get_results_init() {
					var waiting;
					var prev_response;
					var prev_bg = jQuery('div.contact_form_body').css("background-color");
					var desc_field = document.getElementById("faqsol_search_input_contact");

					desc_field.onkeyup = desc_field.onchange = function() {
						if(waiting != 1) {
							waiting = 1;
							if (window.XMLHttpRequest){
								var http = new XMLHttpRequest()
							} else {
								if (window.ActiveXObject) {
									var http = new ActiveXObject("Microsoft.XMLHTTP");
								} else {
									// alert('wtf');
								}
							}
							if(http) {
								try {
									var url = "/aw-support-centre/{$vendor}/contact/";
									var query = document.getElementById("faqsol_search_input_contact").value;
									var params = "{$apincluded}vendor={$vendor}&query="+query;
									http.open("POST", url, true);

									// send the proper header information along with the request
									http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
									http.setRequestHeader("Content-length", params.length);
									http.setRequestHeader("Connection", "close");

									http.onreadystatechange = function() { // output the data when the state changes
										if(http.readyState == 4 && http.status == 200) {
											document.getElementById("ajax_response").innerHTML = (http.responseText);
											if(prev_response != http.responseText) {
												jQuery('li.ajax_response').animate({ backgroundColor: "#F8F8F8" }, 1).animate({ backgroundColor: prev_bg }, 500).animate({ backgroundColor: "#F8F8F8" }, 1).animate({ backgroundColor: prev_bg }, 500);
											}
											prev_response = http.responseText;
											waiting = 0;
										}
									}
									http.send(params);
								} catch(err) {
								}
							}
						}
					}
					
					desc_field.onkeyup();
				
				}
				
				function isValidEmail(str) {
				  // These comments use the following terms from RFC2822:
				  // local-part, domain, domain-literal and dot-atom.
				  // Does the address contain a local-part followed an @ followed by a domain?
				  // Note the use of lastIndexOf to find the last @ in the address
				  // since a valid email address may have a quoted @ in the local-part.
				  // Does the domain name have at least two parts, i.e. at least one dot,
				  // after the @? If not, is it a domain-literal?
				  // This will accept some invalid email addresses
				  // BUT it doesn't reject valid ones. 
				  var atSym = str.lastIndexOf("@");
				  if (atSym < 1) { return false; } // no local-part
				  if (atSym == str.length - 1) { return false; } // no domain
				  if (atSym > 64) { return false; } // there may only be 64 octets in the local-part
				  if (str.length - atSym > 255) { return false; } // there may only be 255 octets in the domain

				  // Is the domain plausible?
				  var lastDot = str.lastIndexOf(".");
				  // Check if it is a dot-atom such as example.com
				  if (lastDot > atSym + 1 && lastDot < str.length - 1) { return true; }
				  //  Check if could be a domain-literal.
				  if (str.charAt(atSym + 1) == '[' &&  str.charAt(str.length - 1) == ']') { return true; }
				  return false;
				}


				function validateForm(){
					var valid = 1;
					var fields = ["contact_form_name","contact_form_lname","contact_form_subject","faqsol_search_input_contact"];
					for (var field in fields) {
						if(document.getElementById(fields[field])) {
							if(document.getElementById(fields[field]).value == "" || document.getElementById(fields[field]).value == jQuery('#'+fields[field]+'').attr('rel') ) {
								valid = 0;
								jQuery("label[for='"+fields[field]+"']").addClass("wrong");
							} else {
								jQuery("label[for='"+fields[field]+"']").removeClass("wrong");
							}
						}
					}

					var emailfields = ["contact_form_email"];
					for (var field in emailfields) {
						if(document.getElementById(emailfields[field])) {
							if(!isValidEmail(document.getElementById(emailfields[field]).value)) {
								valid = 0;
								jQuery("label[for='"+emailfields[field]+"']").addClass("wrong");
							} else {
								jQuery("label[for='"+emailfields[field]+"']").removeClass("wrong");
							}
						}
					}

					if(valid == 0) { 
						alert("Please ensure all the fields are filled correctly, and your email is valid");
						return false;
					}
					document.getElementById("faqsol_search_input_contact").value = "AlbumID: " + (document.getElementById("contact_form_albumid").value + "\\n\\n" + document.getElementById("faqsol_search_input_contact").value);
					return true;
				}
			//]]>
			</script>

ENDECHO;

	wrapper_end_html();

}

?>