<?php

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

	require_once 'wrapper_out.php';
                    
if (isset($_GET['faqsol_vendor']) 			&&
	(	$_GET['faqsol_vendor'] 	== '1031' 	||
		$_GET['faqsol_vendor'] 	== '3183' 	||
		$_GET['faqsol_vendor'] 	== '3255'))	{
		
	$isearch_path = dirname(__FILE__).'/..';
	define('IN_ISEARCH', TRUE);

	require_once "$isearch_path/inc/core.inc.php";
	require_once "$isearch_path/inc/search.inc.php";
		
	isearch_open(TRUE);
    
	$faqsol_search_query = (isset($_POST['faqsol_search_query'])) ? strip_tags(trim($_POST['faqsol_search_query'])) : '';
    
	$faqsol_vendor = $_GET['faqsol_vendor'];
	
	if(strstr($_SERVER['REQUEST_URI'],'aw-support-centre') == FALSE) {
		$search_action = '../support';
	} else {
		$search_action = '../'.$faqsol_vendor.'/';
	}

	wrapper_start_html($faqsol_vendor,'Search','search','');

	if 	((preg_match('#[\\x0d\\x0a]#', $faqsol_search_query)) ||
			(preg_match('#content-type multipartmixed#', $faqsol_search_query))) {
		// probably an attempt by a spammer to abuse a formmail script
	    header('HTTP/1.1 404 Not Found');
	} else if ($faqsol_search_query != '') {
		if($faqsol_vendor == 3183 && $_SERVER["REMOTE_ADDR"] == '203.6.240.254') {
			echo <<<ENDECHO
				<div class="sol_result_head">
					<div class="result_head_uppersearchbar">
                    <form class="search" action="{$search_action}" method="POST" id="bodysearch">
                        <input type="hidden" value="search" name="action">
                        
                        <label for="field_bodysearch">SEARCH:</label>
                        <input type="text" name="search" id="field_bodysearch" value="$faqsol_search_query" />
                        <button type="submit">GO</button>
                    </form>                     
					</div>
					<span class="sol_result_head_title">Search results</span>
					<span class="sol_result_head_desc">You searched on <span class="faqsol_highlight">$faqsol_search_query</span>.</span>
				</div>
				<div class="sol_result_body">
ENDECHO;
		} else {
			echo <<<ENDECHO
                <div class="faqsol_searchbar" style="margin-bottom:0;">
                        <form class="search" action="{$search_action}" method="POST" id="bodysearch">
                            <input type="hidden" value="search" name="action">
                            
                            <label for="field_bodysearch">SEARCH:</label>
                            <input type="text" name="search" id="field_bodysearch" value="$faqsol_search_query" />
                            <button type="submit">GO</button>
                        </form>                     

                    <span class="sol_result_head_title">Search results</span>
                    <span class="sol_result_head_desc">You searched on <span class="faqsol_highlight">$faqsol_search_query</span>.</span>
                </div>
				<div class="sol_result_body">
ENDECHO;
		}
	    $faqsol_search_query = isearch_cleanSearchString($faqsol_search_query);

	    $numResults = isearch_find($faqsol_search_query,$faqsol_vendor);

	    $results = isearch_getResultArray(1, 10);

	    if (count($results) == 0) {
			echo "\t\t\t\t",'<span class="sol_result_nomatch">No matches found. Try simplifying your search by only using keywords. If you still cannot find what you\'re looking for, you can contact us using the link below.</span>'."\n";
	    } else {
			include_once ($_SERVER['DOCUMENT_ROOT'].'/configuration.php');
			if(!isset($AP_global_db_config)) {
				$AP_global_db_config = new JConfig;
			}
			if ($dbcon 		= mysql_connect('localhost', $AP_global_db_config->user, $AP_global_db_config->password)) {
				$db_seldb 	= mysql_select_db($AP_global_db_config->db,$dbcon);
				$db_query	= mysql_query('SELECT stop_words FROM faqsol_info',$dbcon);
				$stop_words	= mysql_result($db_query,0);
			} else {
				$stop_words = '';
			}

			
			foreach (explode(' ',$faqsol_search_query) as $key => $value) {
			
				$stop_word_test = FALSE;
				foreach (explode(' ',$stop_words) as $subkey => $subvalue) {
					if ($subvalue == $value) {
						$stop_word_test = TRUE;
					}
				}
				
				if ((strlen($value) > 2 ) && ($stop_word_test == FALSE)) {
					$highligh_keywords_search[$key] = ' '.$value.' ';
					$highligh_keywords_replace[$key] = ' <span class="faqsol_highlight">'.$value.'</span> ';

					$highligh_keywords_search[$key+1000] = ' '.$value.',';
					$highligh_keywords_replace[$key+1000] = ' <span class="faqsol_highlight">'.$value.'</span>,';
					
					$highligh_keywords_search[$key+10000] = ' '.$value.'.';
					$highligh_keywords_replace[$key+10000] = ' <span class="faqsol_highlight">'.$value.'</span>.';
				}
			}
	        foreach ($results as $result) {
				$result['description'] = str_replace($highligh_keywords_search,$highligh_keywords_replace,$result['description']);
				$result_url = $result['url'];
				$result_title_compound = $result['title'];
				$result_title = str_replace('[cat=]','',preg_replace('#\[cat=[a-zA-Z \\/]+\]#','',$result_title_compound));
				$title_cat_preg = preg_match_all('#\[cat=([a-zA-Z \\/]+)\]#',$result_title_compound,$title_cat_preg_match);
				if ($title_cat_preg > 0) {
				$result_cat = 'Categories:';
					foreach ($title_cat_preg_match[1] as $key => $value) {
						if ($key == 0) {
							$result_cat .= ' '.ucwords($value);
						} else {
							$result_cat .= ', '.ucwords($value);
						}
					}
				$result_cat = str_replace('The','the',$result_cat);
				} else {
					$result_cat = '';
				}
				$result_desc = $result['description'];
				
				$result_url = preg_replace('/https:\\/\\/www.albumworks.com.au\\/support-centre\\/(1031|3183|3255)\\//','',$result_url);
				
				//if (($result_title != '') && (preg_match("/(%%|##)/is",$result_desc.$result_title) == 0)) {
				// check for solutions which don't belong to this particular vendor -- UPDATE: not necessary- these won't show as when attempted to index will 404 due to handling in solution.php
					echo <<<ENDECHO
				<div class="faqsol_result_container">
					<h6><a href="$result_url" class="faqsol_result_link">$result_title</a></h6>
					<p class="faqsol_result_desc">$result_desc</p>
                    <p><a href="$result_url">$result_url</a></p>
				</div>

ENDECHO;
				//}

				unset($result_url);
				unset($result_title);
				unset($result_desc);
				unset($result_cat);
	        }
	    }
		echo "\t\t\t</div>\n";

	} else {
	if($faqsol_vendor == 3183 && $_SERVER["REMOTE_ADDR"] == '203.6.240.254') {
		echo <<<ENDECHO
				<div class="faqsol_searchbar">
                        <form class="search" action="{$search_action}" method="POST" id="bodysearch">
                            <input type="hidden" value="search" name="action">
                            
                            <label for="field_bodysearch">SEARCH:</label>
                            <input type="text" name="search" id="field_bodysearch" value="" />
                            <button type="submit">GO</button>
                        </form>                     
					<span id="faqsol_search_sub"><strong>Note:</strong> An option to contact us by email appears after search<br /> results are displayed.</span>
				</div>  
ENDECHO;
	} else {
		echo <<<ENDECHO
				<div class="faqsol_searchbar">
                        <form class="search" action="{$search_action}" method="POST" id="bodysearch">
                            <input type="hidden" value="search" name="action">
                            
                            <label for="field_bodysearch">SEARCH:</label>
                            <input type="text" name="search" id="field_bodysearch" value="" />
                            <button type="submit">GO</button>
                        </form>                     

					<span id="faqsol_search_label"><strong>Note:</strong> An option to contact us by email appears after search<br /> results are displayed.</span>
					
				</div>
ENDECHO;
	}
		echo <<<ENDECHO
			<div class="faqsol_prioritysol">
ENDECHO;
/*		echo <<<ENDECHO
					<!--<h3 class="faqsol_prioritysol_title">Server Status</h3>
					<p>Update (3:20pm):  Earlier we experienced some issues with our servers that resulted in customers receiving an error message during the ordering process.<br />
					Our engineers have identified the cause of this and our servers are now back up and running.<br />
					Our servers were down for approximately 110 minutes.</p>
					<p>If you previously tried uploading your project to our servers and received an error message, please try again now as it should work.</p>
					<p>We apologise for any inconvenience this has caused and we thank you for your patience.</p>-->
ENDECHO;*/
/*		echo <<<ENDECHO
				<div class="faqsol_timely">
					<h3 class="faqsol_prioritysol_title">Mother's Day order deadline</h3>
					<p>Our deadline for Mother's Day orders is <strong>9pm (AEDT) on Wednesday 28th April</strong>, 2010.</p>
					<p>Our goal is to fulfill every order placed before the deadline, however some elements are beyond our control and this deadline is not a guarantee of delivery.</p>
					<p>We highly recommend uploading your order well before the deadline to ensure you receive it in time for Mother's Day as we cannot extend our deadline under any circumstances.</p>
				</div>
ENDECHO;*/
		echo <<<ENDECHO
				<div class="faqsol_topten">
ENDECHO;
					if($faqsol_vendor == '1031'){
					/*echo <<<ENDECHO
					<span id="faqsol_search_label"><p>We've changed our name! Albumprinter is now known as <em>albumworks</em>.  For more information please visit our news page:  <a href="http://www.albumworks.com.au/news.html">albumworks.com.au/news.html</a></p></span>
ENDECHO;*/
					/*echo <<<ENDECHO
					<!--<h3 class="faqsol_prioritysol_title">Questions about the new editor</h3>					
					<ul>
						<li class="faqsol_prioritysol_li"><a href="solution/00002/" class="faqsol_prioritysol_a">How do I update to the new version?</a></li>
						<li class="faqsol_prioritysol_li"><a href="solution/00003/" class="faqsol_prioritysol_a">Will I lose any of my projects when upgrading?</a></li>
						<li class="faqsol_prioritysol_li"><a href="solution/00004/" class="faqsol_prioritysol_a">What new features are available in the new version?</a></li>
						<li class="faqsol_prioritysol_li"><a href="solution/00001/" class="faqsol_prioritysol_a">Does the new version work on a Mac?</a></li>
					</ul>-->
ENDECHO;*/
					}
					/*if($faqsol_vendor == '3255'){
					echo <<<ENDECHO
					<h3 class="faqsol_prioritysol_title">Information about the <em>2nd Copy Free</em> promotion</h3>					
					<p>To ensure you receive this offer, please check the following:
					<ul>
						<li>This offer is for 2 copies of the same album uploaded within the same ordering process.</li>
						<li>This offer expires at midnight AET on Wednesday the 30th September 2009.</li>
						<li>Please ensure you enter the 2NDCOPYFREE9 code at stage 5/8 of the ordering process.  We cannot adjust or cancel your order once it has been placed.</li>
						<li>Please ensure you increase the quantity during the ordering process to '2' at stage 5/8 of the ordering process.</li>
					</ul></p>
ENDECHO;
					}*/   
					echo <<<ENDECHO
                    <h2 class="sectionheader bufferbot">Browse</h2>
ENDECHO;

                    require_once 'browse.php';

                    /*
                    echo <<<ENDECHO
					<h2 class="sectionheader bufferbot">Top 4 Questions</h2>
ENDECHO;
					if($faqsol_vendor == 'none'){
					echo <<<ENDECHO
						<ul>
							<li class="faqsol_prioritysol_li"><a href="solution/00100-buy1get1free-promotion/" class="faqsol_prioritysol_a">How do I use the Buy 1 Get 1 Free promotion?</a></li>
							<li class="faqsol_prioritysol_li"><a href="solution/10060-delivery-time/" class="faqsol_prioritysol_a">How long will it take to produce and deliver my order?</a></li>
							<li class="faqsol_prioritysol_li"><a href="solution/70060-track-trace/" class="faqsol_prioritysol_a">Can I track and trace the delivery of my order?</a></li>
							<li class="faqsol_prioritysol_li"><a href="solution/10090-samples/" class="faqsol_prioritysol_a">Can I see some print samples of your products?</a></li>
							<li class="faqsol_prioritysol_li"><a href="solution/20020-mac-os/" class="faqsol_prioritysol_a">I have an Apple Mac. Can I use your services?</a></li>
						</ul>
ENDECHO;
					} else {
					echo <<<ENDECHO
						<ul>
							<!--<li class="faqsol_prioritysol_li"><a href="solution/80005-upgrade-error/" class="faqsol_prioritysol_a">I'm having trouble upgrading my editor, what should I do?</a></li>-->
							<li class="faqsol_prioritysol_li"><a href="solution/10060-delivery-time/" class="faqsol_prioritysol_a">How long will it take to produce and deliver my order?</a></li>
							<li class="faqsol_prioritysol_li"><a href="solution/70060-track-trace/" class="faqsol_prioritysol_a">Can I track and trace the delivery of my order?</a></li>
							<li class="faqsol_prioritysol_li"><a href="solution/10090-samples/" class="faqsol_prioritysol_a">Can I see some print samples of your products?</a></li>
							<li class="faqsol_prioritysol_li"><a href="solution/20020-mac-os/" class="faqsol_prioritysol_a">I have an Apple Mac. Can I use your services?</a></li>
						</ul>
ENDECHO;
					}              
                   */        
				echo <<<ENDECHO
                <div class="clr"> </div>
				</div>
				
				<div class="faqsol_timely">
				
ENDECHO;
				/*echo <<<ENDECHO
					<h3 class="faqsol_prioritysol_title">Server Status</h3>
					<p>Earlier we experienced some difficulties with our servers that resulted in customers not being able to upload to our servers.</p>
					<p>Our engineers are currently looking into this and we hope to find a solution soon.</p>
					<p>As soon as we have more information we'll post an update here.</p>
					<p>We apologise for any inconvenience this has caused and we thank you for your patience.</p>
ENDECHO;*/
				/*echo <<<ENDECHO
					<h3 class="faqsol_prioritysol_title">Server Status</h3>
					<p>Earlier we experienced some difficulties with our servers that resulted in customers not being able to upload to our servers.</p>
					<p>Our engineers have located the source of the issue and the system is now fixed.</p>
					<p>Please feel free to upload your project to our servers again.</p>
					<p>We apologise for any inconvenience this has caused and we thank you for your patience.</p>
ENDECHO;*/
				/*echo <<<ENDECHO
					<h3 class="faqsol_prioritysol_title">Server Status</h3>
					<p>Update (7:50pm):  Our engineers have located the cause of this problem and have made the necessary changes.  Due to the nature of the issue (a DNS issue), these 						changes may take some time to take affect and this will vary depending on your Internet provider.<br />
					Initial testing shows some customers will be able to upload now, others may need to wait until the morning. Unfortunately we can't be more specific than this as DNS requires a range of updates to 						occur on servers around the world and it just depends on when the DNS table of your Internet provider is updated.<br /><br /></p>
					<p>Tuesday Feb 3 - We're currently experiencing some difficulties with our servers which is resulting in customers not being able to upload their orders to our servers.</p>
					<p>Customers are receiving an error message stating "Socket Error # 10054.  Connection reset by peer."</p>
					<p>Our engineers are currently investigating this issue and at this stage it's too early to provide an estimate on when this will be resolved.</p>
					<p>We'll keep this page updated as soon as we have more information.</p>
					<p>We apologise for any inconvenience this has caused and we thank you for your patience.</p>
					<!--<ul>
						<li class="faqsol_prioritysol_li">We're currently experience<a href="#" class="faqsol_prioritysol_a">Fathers Day deadline</a></li>
						<li class="faqsol_prioritysol_li"><a href="#" class="faqsol_prioritysol_a">Etc etc</a></li>
						<li class="faqsol_prioritysol_li"><a href="#" class="faqsol_prioritysol_a">Etc etc</a></li>
						<li class="faqsol_prioritysol_li"><a href="#" class="faqsol_prioritysol_a">Etc etc</a></li>
					</ul>-->
ENDECHO;*/
				echo <<<ENDECHO
				</div>

				<div class="clear_both_empty"></div>
			</div>


ENDECHO;
	}

	isearch_close();
	
	if (isset($_SERVER['PHP_AUTH_USER']) && ($_SERVER['PHP_AUTH_USER'] == 'faqsol_spider')) {
		// don't display contact link/text when indexing
	} else {
		switch($faqsol_vendor) {
			case '1031':
				if(strstr($_SERVER['REQUEST_URI'],'aw-support-centre') == FALSE) {
					$contact_link = './contact';
				} else {
					$contact_link = './contact/';
				}
				break;
			case '3183':
				$contact_link = 'http://www.officeworks.com.au/retail/b2c/landingWithMenuForward.do?page=photobookcontact&left=menu_photobooks" target="_top';
				break;
			case '3255':
				$contact_link = 'http://www.target.com.au/photobooks/contact_photobooks.htm" target="_top';
				break;
		}
		// edit 13/11/08 - remove contact link unless customer has done a search
		if ($faqsol_search_query != '') {
			echo "\t\t\t",'<div class="contact_link"><span class="contact_link_pre">Can\'t find what you\'re looking for?</span><a class="contact_link_a" href="'.$contact_link.'">Contact Us</a><span class="poweredby_image">Powered by Albumprinter</span></div>',"\n";
		}
	}

	wrapper_end_html();

} else {
	// file not accessed correctly
	header("HTTP/1.1 404 Not Found");
}

?>