<?php

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

require_once 'wrapper_out.php';

$vendor=$_GET['faqsol_vendor'];

if (isset($_GET['reason']) && ($_GET['reason'] == 'cd' || $_GET['reason'] == 'samples')) {
	$contact_reason = $_GET['reason'];
} else {
	header('HTTP/1.1 404 Not Found');
	die;
}

($contact_reason == 'cd' ? $reason_desc = 'Software on CD' : $reason_desc = 'Samples');

if($contact_reason == 'cd'){
	$special_contact_title = 'CD request form';
	$special_contact_desc = "Please enter your postal details below to receive a CD containing our editor in the post.";
	$special_contact_postsubmission = "Thank you for submitting your postal details. We'll dispatch a CD containing our editor to you as quickly as possible.";
	
} else {
	$special_contact_title = "A5 Sample request form";
	$special_contact_desc = "Please enter your postal details below to receive 2 A5 print samples in the post to demonstrate the print quality of our products.";
	$special_contact_postsubmission = "Thank you for submitting your postal details. We'll dispatch 2 A5 print samples to you as quickly as possible.";
}

if (isset($_POST['special_contact_form_submit'])) {
	submit_form($vendor, $reason_desc, $contact_reason, $special_contact_title, $special_contact_desc, $special_contact_postsubmission);
} else {
	contact_form(NULL,'','','','','','','','',$vendor, $reason_desc, $contact_reason, $special_contact_title, $special_contact_desc, $special_contact_postsubmission);
}

function contact_form($error_message=NULL,$def_name='',$def_email='',$def_email_conf='',$def_address1='',$def_address2='',$def_city='',$def_state='',$def_postcode='', $vendor, $reason_desc, $contact_reason, $special_contact_title, $special_contact_desc, $special_contact_postsubmission) {
	
	$state_selected = array('ACT' => '', 'NSW' => '', 'NT' => '', 'QLD' => '', 'SA' => '', 'TAS' => '', 'VIC' => '', 'WA' => '');
	$state_selected[$def_state] = ' selected="selected"';
	if (strstr($_SERVER['REQUEST_URI'],'aw-support-centre') != FALSE) {
		$form_slashes = '/';
	} else {
		$form_slashes = '';
	}
	
	wrapper_start_html($vendor,'Contact Us','contact','',TRUE);
	echo <<<ENDECHO
			<div class="special_contact_form_container">
				<div class="special_contact_form_head">
					<span class="special_contact_form_title">$special_contact_title</span>
					<span class="special_contact_form_desc">$special_contact_desc<br />The fields marked with a <span class="special_contact_form_label_star">*</span> are required.</span>
					<span class="special_contact_form_err">$error_message</span>
				</div>
				<div class="special_contact_form_body">
					<form action="../contact{$form_slashes}?reason=$contact_reason" method="post">
						<fieldset>
							<label class="special" for="special_contact_form_name">Name:<span class="special_contact_form_label_star">*</span></label>
							<input type="text" name="special_contact_form_name" id="special_contact_form_name" value="$def_name" />
							<input type="hidden" name="status" value="New" />							
							<hr class="contact_input_divider" />
							
							<label class="special" for="special_contact_form_email">Email:<span class="special_contact_form_label_star">*</span></label>
							<input type="text" name="special_contact_form_email" id="special_contact_form_email" value="$def_email" />
							
							<hr class="contact_input_divider" />
							
							<!-- <label class="special" for="special_contact_form_email_conf">Email:<span class="special_contact_form_label_star">*</span></label> -->
							<input type="hidden" name="special_contact_form_email_conf" id="special_contact_form_email_conf" value="$def_email_conf" />
							
							<hr class="contact_input_divider" />

							<label class="special" for="special_contact_form_address1">Address:<span class="special_contact_form_label_star">*</span></label>
							<input type="text" name="special_contact_form_address1" id="special_contact_form_address1" value="$def_address1" />
							
							<hr class="contact_input_divider" />
							
							<label class="special" for="special_contact_form_address2">Address 2:</label>
							<input type="text" name="special_contact_form_address2" id="special_contact_form_address2" value="$def_address2" />
							
							<hr class="contact_input_divider" />

							<label class="special" for="special_contact_form_city">Suburb/Town:<span class="special_contact_form_label_star">*</span></label>
							<input type="text" name="special_contact_form_city" id="special_contact_form_city" value="$def_city" />
							
							<hr class="contact_input_divider" />

							<label class="special" for="special_contact_form_state">State:<span class="special_contact_form_label_star">*</span></label>
							<select name="special_contact_form_state" id="special_contact_form_state">
								<option value="">--Pick One--</option>
								<option value="ACT"{$state_selected['ACT']}>ACT</option>
								<option value="NSW"{$state_selected['NSW']}>NSW</option>
								<option value="NT"{$state_selected['NT']}>NT</option>
								<option value="QLD"{$state_selected['QLD']}>QLD</option>
								<option value="SA"{$state_selected['SA']}>SA</option>
								<option value="TAS"{$state_selected['TAS']}>TAS</option>
								<option value="VIC"{$state_selected['VIC']}>VIC</option>
								<option value="WA"{$state_selected['WA']}>WA</option>
							</select>
							
							<hr class="contact_input_divider" />

							<label class="special" for="special_contact_form_postcode">Post Code:<span class="special_contact_form_label_star">*</span></label>
							<input type="text" name="special_contact_form_postcode" id="special_contact_form_postcode" value="$def_postcode" />
							
							<hr class="contact_input_divider" />

							<label class="special" for="special_contact_form_submit"></label>
							<input type="submit" id="special_contact_form_submit" name="special_contact_form_submit" value="Submit" />
						</fieldset>
					</form>
				</div>
			</div>

ENDECHO;
	wrapper_end_html();
}

function submit_form($vendor, $reason_desc, $contact_reason, $special_contact_title, $special_contact_desc, $special_contact_postsubmission) {
	
	$checked_special_contact_form_name 		= (isset($_POST['special_contact_form_name'])) 		? stripslashes(trim($_POST['special_contact_form_name'])) 	 	: '';
	$checked_special_contact_form_email 	= (isset($_POST['special_contact_form_email'])) 	? stripslashes(trim($_POST['special_contact_form_email'])) 	 	: '';
	$checked_special_contact_form_email_conf= (isset($_POST['special_contact_form_email_conf']))? stripslashes(trim($_POST['special_contact_form_email_conf']))	: '';
	$checked_special_contact_form_address1	= (isset($_POST['special_contact_form_address1'])) 	? stripslashes(trim($_POST['special_contact_form_address1']))		: '';
	$checked_special_contact_form_address2	= (isset($_POST['special_contact_form_address2'])) 	? stripslashes(trim($_POST['special_contact_form_address2']))		: '';
	$checked_special_contact_form_city 		= (isset($_POST['special_contact_form_city'])) 		? stripslashes(trim($_POST['special_contact_form_city'])) 		: '';
	$checked_special_contact_form_state 	= (isset($_POST['special_contact_form_state'])) 	? stripslashes(trim($_POST['special_contact_form_state'])) 		: '';
	$checked_special_contact_form_postcode	= (isset($_POST['special_contact_form_postcode'])) 	? stripslashes(trim($_POST['special_contact_form_postcode']))		: '';
		
		if ($checked_special_contact_form_name		!= ''		&&
			$checked_special_contact_form_email		!= ''		&&
			// $checked_special_contact_form_email		== $checked_special_contact_form_email_conf && // not required, nick doesn't want to confirm email, as we don't do it on the standard contact form
			preg_match("/[a-zA-Z0-9._%-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}/", $checked_special_contact_form_email) > 0 &&
			$checked_special_contact_form_address1	!= ''		&&
			$checked_special_contact_form_city		!= ''		&&
			$checked_special_contact_form_state		!= ''		&&
			$checked_special_contact_form_postcode	!= ''		&&
			is_numeric($checked_special_contact_form_postcode)	&&
			strlen($checked_special_contact_form_postcode) == 4) {
			
			if ($checked_special_contact_form_address2 != '') {
				$checked_special_contact_form_address = $checked_special_contact_form_address1."\n".$checked_special_contact_form_address2;
			} else {
				$checked_special_contact_form_address = $checked_special_contact_form_address1;
			}
				
			$url = 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';
			$params = "orgid=00D36000000oZE6&external=&submit=submit&email=".str_replace('&','%26',$checked_special_contact_form_email)."&subject=Site form request - {$reason_desc}&description=Please post {$reason_desc} (Vendor: {$vendor}) to:\n\n".str_replace('&','%26',$checked_special_contact_form_name)."\n".str_replace('&','%26',$checked_special_contact_form_address)."\n".str_replace('&','%26',$checked_special_contact_form_city)."\n".str_replace('&','%26',$checked_special_contact_form_state)."\t".str_replace('&','%26',$checked_special_contact_form_postcode)."&00N20000001Qe3d=$vendor";
			$user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
			curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			
			$result = curl_exec ($ch);
			curl_close ($ch);
			
			global $vendor;
			
			wrapper_start_html($vendor,'Contact Us','contact','');
			echo <<<ENDECHO
				<div class="special_contact_form_container">
					<div class="special_contact_form_head">
						<span class="special_contact_form_title">$special_contact_title</span>
						<span class="special_contact_form_desc">$special_contact_postsubmission</span>
						<span class="special_contact_form_err"></span>
					</div>
				</div>

ENDECHO;
			wrapper_end_html();

		} else {
			contact_form($error_message='Please ensure all fields are filled in correctly.',$checked_special_contact_form_name, $checked_special_contact_form_email, $checked_special_contact_form_email_conf, $checked_special_contact_form_address1, $checked_special_contact_form_address2, $checked_special_contact_form_city, $checked_special_contact_form_state, $checked_special_contact_form_postcode,$vendor, $reason_desc, $contact_reason, $special_contact_title, $special_contact_desc, $special_contact_postsubmission);
		}

}


	

?>