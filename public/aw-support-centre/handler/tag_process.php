<?php

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

function tag_process($input,$vendor) {

	require 'solution_global_vars.php';

	$version_match = array('1031' => '041', '3183' => '046', '3255'=> '041');
	$editor_version = $version_match[$vendor];

	$preg_vendor = preg_match_all('#<vendorvartag name="(.+)" 1031="(.*)" 3183="(.*)" 3255="(.*)" />#',$sol_tags,$vendorvartag_preg,PREG_SET_ORDER);
	if ($preg_vendor > 0) {
		foreach ($vendorvartag_preg as $key => $value) {
			$vendor_var[$vendorvartag_preg[$key][1]][1031] = $vendorvartag_preg[$key][2];
			$vendor_var[$vendorvartag_preg[$key][1]][3183] = $vendorvartag_preg[$key][3];
			$vendor_var[$vendorvartag_preg[$key][1]][3255] = $vendorvartag_preg[$key][4];
		}
		unset($vendorvartag_preg);

		foreach ($vendor_var as $key => $val) {
			$vendor_var_clean[$key] = $vendor_var[$key][$vendor];
		}

		$vendor_var = $vendor_var_clean;

		unset($vendor_var_clean);

		foreach ($vendor_var as $key => $value) {
		    $vendor_var_search[] = "%%$key%%";
		    $vendor_var_replace[] = $value;
		}

		unset($vendor_var);

		$input = str_replace($vendor_var_search, $vendor_var_replace, $input);

		unset($vendor_var_search);
		unset($vendor_var_replace);
	}

	$preg_version = preg_match_all('#<versionvartag name="(.+)" 041="(.*)" 046="(.*)" />#',$sol_tags,$versionvartag_preg,PREG_SET_ORDER);
	if ($preg_version > 0) {
		foreach ($versionvartag_preg as $key => $value) {
			$version_var[$versionvartag_preg[$key][1]]['041'] = $versionvartag_preg[$key][2];
			$version_var[$versionvartag_preg[$key][1]]['046'] = $versionvartag_preg[$key][3];
		}
		unset($versionvartag_preg);

		foreach ($version_var as $key => $val) {
			$version_var_clean[$key] = $version_var[$key][$editor_version];
		}

		$version_var = $version_var_clean;

		unset($version_var_clean);

		foreach ($version_var as $key => $value) {
		    $version_var_search[] = "##$key##";
		    $version_var_replace[] = $value;
		}

		unset($version_var);

		$input = str_replace($version_var_search, $version_var_replace, $input);

		unset($version_var_search);
		unset($version_var_replace);
	}
	
	return $input;
	
}
	
?>