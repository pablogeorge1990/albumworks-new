<?php
error_reporting(E_ALL);
if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

if(isset($_POST['ajax'])) { $class_prefix = 'ajax_'; } else { $class_prefix = 'faqsol_'; }

$faq = $_GET['faqsol_faq'];
$vendor = $_GET['faqsol_vendor'];

$pseudorandid = 'uniqid'.rand(10000,99999);
$pseudorandid_tag = "\n\t\t<meta name=\"pseudorandid\" content=\"$pseudorandid\" />";
                                            
if (strstr($_SERVER['SERVER_NAME'],'172') == TRUE || strstr($_SERVER['SERVER_NAME'],'127') == TRUE || strstr($_SERVER['SERVER_NAME'],'bobso') == TRUE) {
	$sol_include_path = '../';
    $sol_include_path = $_SERVER['DOCUMENT_ROOT'].'/aw-support-centre/';
} else {
	$sol_include_path = $_SERVER['DOCUMENT_ROOT'].'/aw-support-centre/';
}

if ((file_exists($sol_include_path.'solutions/'.$faq.'.sol.php'))== TRUE) {
	require_once $sol_include_path.'solutions/'.$faq.'.sol.php';
	require_once 'tag_process.php';
	require_once 'wrapper_out.php';
	
	$sol_title = tag_process($sol_title,$vendor);
	$sol_content = tag_process($sol_content,$vendor);
	$sol_categories = tag_process($sol_categories,$vendor);

    if(false){
	    if (($sol_title == '') || ($sol_content == '') || (preg_match("/(%%|##)/is",$sol_title.$sol_content) > 0)) {
	    // check for solutions which don't belong to this particular vendor
		    header('HTTP/1.1 404 Not Found');
		    die;
	    }
    }
	    
	if(!isset($_POST['ajax'])) {
	
		if (isset($_SERVER['PHP_AUTH_USER']) && ($_SERVER['PHP_AUTH_USER'] == 'faqsol_spider')) {
			wrapper_start_html($vendor,$sol_title.$sol_categories,'solution','',FALSE,$sol_keywords,$pseudorandid_tag,TRUE);
		} else {
			wrapper_start_html($vendor,$sol_title,'solution','',TRUE,$sol_keywords,'',FALSE);
		}

		if(strstr($_SERVER['REQUEST_URI'],'aw-support-centre') == FALSE) {
			$search_action = '../support';
		} else {
			$search_action = '';
		}

		echo <<<ENDECHO
                <div class="faqsol_searchbar">
                    <form method="post" action="{$search_action}" id="faqsol_search_form">
                        <fieldset id="faqsol_search_fieldset">
                            <h2 class="sectionheader">Search</h2>
                            <!-- <label for="faqsol_search_input" id="faqsol_search_label">Have a question or need help with our products or software?<br />Type it in below, and we'll do our best to find a solution for you.</label> -->
                            <input type="text" name="faqsol_search_query" id="faqsol_search_input" class="prefill_example smartedit" size="50" value="Enter your question here..." />
                            <input type="submit" id="faqsol_search_submit" value="Search" />
                        </fieldset>
                    </form>
                </div>
			<div class="faqsol_sol_container">
ENDECHO;

	}

	if (isset($_SERVER['PHP_AUTH_USER']) && ($_SERVER['PHP_AUTH_USER'] == 'faqsol_spider')) {
		// don't display title when indexing
	} else {
		echo <<<ENDECHO

				<span class="{$class_prefix}sol_title">$sol_title</span>

ENDECHO;
	}

	echo <<<ENDECHO
				<div class="{$class_prefix}sol_content">		
$sol_content
				</div>
			</div>

ENDECHO;

	if(!isset($_POST['ajax'])) {
		
		if (isset($_SERVER['PHP_AUTH_USER']) && ($_SERVER['PHP_AUTH_USER'] == 'faqsol_spider')) {
			// don't display contact link/text when indexing
		} else {
			switch($vendor) {
				case '1031':
					if(strstr($_SERVER['REQUEST_URI'],'aw-support-centre') == FALSE) {
						$contact_link = '../../contact';
					} else {
						$contact_link = '../../contact/';
					}
					break;
				case '3183':
					$contact_link = 'http://www.officeworks.com.au/retail/b2c/landingWithMenuForward.do?page=photobookcontact&left=menu_photobooks" target="_top';
					//echo "\t\t\t",'<div class="contact_link"><span class="contact_link_pre"><strong>We\'ve recently upgraded our software. Please ensure you have the latest version on your PC.</strong></span></div>',"\n";
					echo "\t\t\t",'<div class="contact_link"><span class="contact_link_pre"></span></div>',"\n";
					break;
				case '3255':
					$contact_link = 'https://www.albumworks.com.au/target-photobooks/contact.html" target="_top';
					break;
			}
			echo "\t\t\t",'<div class="contact_link"><span class="contact_link_pre">Can\'t find what you\'re looking for?</span><a class="contact_link_a" href="'.$contact_link.'">Contact Us</a><a class="back_link_a" href="https://www.albumworks.com.au/support">Back</a><span class="poweredby_image">Powered by albumworks</span></div>',"\n";
		}
		wrapper_end_html();
	}

} else {
	header('HTTP/1.1 404 Not Found');
}


?>