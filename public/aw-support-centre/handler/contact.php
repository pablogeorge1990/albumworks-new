<?php

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

require_once 'wrapper_out.php';

$vendor=$_GET['faqsol_vendor'];

switch($vendor) {
	case '1031':
		$contact_form_desc = "If you would like to contact us, please send an email to:<br /><br /> <strong><a href=\"mailto:service@albumworks.com.au?subject=Enquiry from website\">service@albumworks.com.au</a></strong><br /><br /> and our friendly support staff will get back to you as soon as possible. <br />If your enquiry is regarding your order, please include your AlbumID.";
		break;
	case '3255':
		$contact_form_desc = "If you would like to contact us, please send an email to:<br /><br /> <strong><a href=\"mailto:service@photobooks.target.com.au?subject=Enquiry from website\">service@photobooks.target.com.au</a></strong><br /><br /> and our friendly support staff will get back to you as soon as possible. <br />If your enquiry is regarding your order, please include your AlbumID.";
		break;
	case '3183':
		$contact_form_desc = "If you would like to contact us, please send an email to:<br /><br /> <strong><a href=\"mailto:productsupport@photobooks.officeworks.com.au?subject=Enquiry from website\">productsupport@photobooks.officeworks.com.au</a></strong><br /><br /> and our friendly support staff will get back to you as soon as possible. <br />If your enquiry is regarding your order, please include your AlbumID.";
		break;
	}

if ($vendor == '1031') {
	$hide_tabs = FALSE;
} else {
	$hide_tabs = TRUE;
}
	
function contact_form($error_message=NULL,$def_name='',$def_email='',$def_email_conf='',$def_sub='',$def_albumid='',$def_body='') {
	global $vendor;
	global $hide_tabs;
	global $contact_form_desc;
	wrapper_start_html($vendor,'Contact Us','contact','',TRUE,NULL,'',FALSE,$hide_tabs);
	echo <<<ENDECHO
			<div class="contact_form_container">
				<div class="contact_form_head">
					<span class="contact_form_title">Contact Us</span>
					<span class="contact_form_desc">$contact_form_desc</span>
				</div>
				<!-- <div class="contact_form_body">
					<form action="../contact/" method="post">
						<fieldset>
							<label for="contact_form_name">Name:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="50" name="contact_form_name" class="rem" id="contact_form_name" value="$def_name" />
							<input type="hidden" name="status" value="New" />
							<hr class="contact_input_divider" />
							
							<label for="contact_form_email">Email:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="100" name="contact_form_email" class="rem" id="contact_form_email" value="$def_email" />
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_email_conf">Confirm Email:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="100" name="contact_form_email_conf" class="rem" id="contact_form_email_conf" value="$def_email_conf" />
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_subject">Subject:<span class="contact_form_label_star">*</span></label>
							<input type="text" maxlength="200" name="contact_form_subject" class="rem" id="contact_form_subject" value="$def_sub" />
							
							<hr class="contact_input_divider" />
							
							<label for="contact_form_albumid">AlbumID (optional):</label>
							<input type="text" maxlength="50" name="contact_form_albumid" class="rem" id="contact_form_albumid" title="9 digit number starting with 1031" value="$def_albumid" />
							
							<hr class="contact_input_divider" />
							
							<label for="faqsol_search_input_contact">Query/Message:<span class="contact_form_label_star">*</span></label>
							<textarea name="contact_form_body" id="faqsol_search_input_contact" class="maxlimit3k rem" rows="6" cols="50">$def_body</textarea>
							
							<input type="submit" id="contact_form_submit" name="contact_form_submit" value="Submit" />
						</fieldset>
					</form>
				</div> -->
			</div>

ENDECHO;
	wrapper_end_html();
}

function submit_to_salesforce() {

	global $vendor;
	global $hide_tabs;
	
		$checked_faqsol_search_query 	= (isset($_POST['checked_contact_form_body'])) ? strip_tags(trim($_POST['checked_contact_form_body'])) 		: '';
		$checked_faqsol_search_name 	= (isset($_POST['checked_contact_form_name'])) ? strip_tags(trim($_POST['checked_contact_form_name'])) 		: '';
		$checked_faqsol_search_email 	= (isset($_POST['checked_contact_form_email'])) ? strip_tags(trim($_POST['checked_contact_form_email'])) 	: '';
		$checked_faqsol_search_email_conf 	= (isset($_POST['checked_contact_form_email_conf'])) ? strip_tags(trim($_POST['checked_contact_form_email_conf'])) 	: '';
		$checked_faqsol_search_subject 	= (isset($_POST['checked_contact_form_subject'])) ? strip_tags(trim($_POST['checked_contact_form_subject'])): '';
		$checked_faqsol_search_albumid 	= (isset($_POST['checked_contact_form_albumid']) && $_POST['checked_contact_form_albumid'] != '') ? strip_tags(trim($_POST['checked_contact_form_albumid'])): '(not provided)';
		
		if (($checked_faqsol_search_query == '') || ($checked_faqsol_search_name == '') || ($checked_faqsol_search_email == '') || ($checked_faqsol_search_subject == '') || (preg_match("/[a-zA-Z0-9._%-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}/", $checked_faqsol_search_email) == 0)) {
			echo 'There appears to be a problem with the information you\'ve submtted through this form. Please hit the back button and try again';
			die;
		}
		
		$db_connect = mysql_connect('localhost', 'albumpri_dbuser', 'kghV%80BGD$');
		$db_select 	= mysql_select_db('albumpri_db',$db_connect);
		$db_insert 	= mysql_query("INSERT INTO support_contact_log (name, email, email_conf, subject, albumid, query, datetime, vendor) VALUES(
			'".mysql_real_escape_string($checked_faqsol_search_name)."',
			'".mysql_real_escape_string($checked_faqsol_search_email)."',
			'".mysql_real_escape_string($checked_faqsol_search_email_conf)."',
			'".mysql_real_escape_string($checked_faqsol_search_subject)."',
			'".mysql_real_escape_string($checked_faqsol_search_albumid)."',
			'".mysql_real_escape_string($checked_faqsol_search_query)."',
			'".date("j/m/Y H:i:s")."',
			'".mysql_real_escape_string($vendor)."'
		) ");
		mysql_close($db_connect);
		
		$url = 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';
		$params = "orgid=00D36000000oZE6&external=&submit=submit&email=".urlsafechars($checked_faqsol_search_email)."&subject=".urlsafechars($checked_faqsol_search_subject)."&description=Name:%20".urlsafechars($checked_faqsol_search_name)."\nAlbumID:%20".urlsafechars($checked_faqsol_search_albumid)."\n\n".urlsafechars($checked_faqsol_search_query)."&00N20000001Qe3d=$vendor";
		$user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		$result = curl_exec ($ch);
		curl_close ($ch);
		
		wrapper_start_html($vendor,'Contact Us','contact','',TRUE,NULL,'',FALSE,$hide_tabs);
		echo <<<ENDECHO
			<div class="contact_form_container">
				<div class="contact_form_head">
					<span class="contact_form_title">Contact Us - Thankyou</span>
					<span class="contact_form_desc">Thank you for taking the time to contact us. One of our team members will be in touch shortly.<br />&nbsp;<br />You will receive a confirmation email sent to <strong>$checked_faqsol_search_email</strong> in the next few minutes. Please make sure this address is correct, so we can properly assist you.<br />&nbsp;<br />In the mean time, why not browse around our <a href="../">support centre</a> where most answers can be found.</span>
					<span class="contact_form_err"></span>
				</div>
			</div>

ENDECHO;
		wrapper_end_html();

}

function urlsafechars($input) {
	return str_replace(array(' ','<','>','#','%','{','}','|','\\','^','~','[',']','`',';','/','?',':','@','=','&','$','\''),array(' ','%3C','%3E','%23','%25','%7B','%7D','%7C','%5C','%5E','%7E','%5B','%5D','%60','%3B','%2F','%3F','%3A','%40','%3D','%26','%24','%20'),$input);
}


if (isset($_GET['show_form']) && ($_GET['show_form'] == 'true')) {

	if (isset($_POST['checked_contact_form_submit'])) {
	
		submit_to_salesforce();
	
	} elseif (isset($_POST['contact_form_submit'])) {
		
		require_once 'wrapper_out.php';
		require_once 'contact.php';
		
		$isearch_path = dirname(__FILE__).'/..';
		define('IN_ISEARCH', TRUE);
		
		require_once "$isearch_path/inc/core.inc.php";
		require_once "$isearch_path/inc/search.inc.php";
		
		isearch_open(TRUE);
		
		$faqsol_search_query = (isset($_POST['contact_form_body'])) ? strip_tags(trim($_POST['contact_form_body'])) : '';
		$faqsol_search_name = (isset($_POST['contact_form_name'])) ? strip_tags(trim($_POST['contact_form_name'])) : '';
		$faqsol_search_email = (isset($_POST['contact_form_email'])) ? strip_tags(trim($_POST['contact_form_email'])) : '';
		$faqsol_search_email_conf = (isset($_POST['contact_form_email_conf'])) ? strip_tags(trim($_POST['contact_form_email_conf'])) : '';
		$faqsol_search_subject = (isset($_POST['contact_form_subject'])) ? strip_tags(trim($_POST['contact_form_subject'])) : '';
		$faqsol_search_albumid = (isset($_POST['contact_form_albumid'])) ? strip_tags(trim($_POST['contact_form_albumid'])) : '';
		
		global $vendor;
		global $hide_tabs;
		
		if 	((preg_match('#[\\x0d\\x0a]#', $faqsol_search_query)) ||
				(preg_match('#content-type multipartmixed#', $faqsol_search_query))) {
			// probably an attempt by a spammer to abuse a formmail script
		    header('HTTP/1.1 404 Not Found');
		} else if (($faqsol_search_query != '') && ($faqsol_search_name != '') && ($faqsol_search_email != '') && ($faqsol_search_email == $faqsol_search_email_conf) && ($faqsol_search_subject != '') && (preg_match("/[a-zA-Z0-9._%-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}/", $faqsol_search_email) > 0)) {
		
		####################
			// skip step 2 for the moment
			foreach ($_POST as $key => $value) {
			$_POST['checked_'.$key] = $value;
			unset($_POST[$key]);
			}
			submit_to_salesforce();
			die;
		####################
		
			wrapper_start_html($vendor,'Contact Us','contact','',TRUE,NULL,'',FALSE,$hide_tabs);
			echo <<<ENDECHO
			<div class="contact_form_container">
				<div class="contact_form_head">
					<span class="contact_form_title">Contact Us - Step 2/3</span>
					<span class="contact_form_desc">Before we submit your question to our team, why not have a look at the following solutions, to see if they help you. If not, click the button below, and your message will be passed onto our team.</span>				
				</div>

ENDECHO;
		    $faqsol_search_query = isearch_cleanSearchString($faqsol_search_query);
		    $numResults = isearch_find($faqsol_search_query,$vendor);
		    $results = isearch_getResultArray(1, 20);
			
		    if (count($results) == 0) {
				echo "\t\t\t\t",'<span class="faqsol_contact_result_nomatch">Err... actually sorry, we couldnt find any solutions to help you so you\'ll just have to click the button below then hey...</span>'."\n";
		    } else {
				$i = 0;
				echo '				<ul class="contact_result_body">'."\n";
		        foreach ($results as $result) {
					if ($i < 5) {
						$result_url = $result['url'];
						$result_title = preg_replace('#\[cat=[-0-9a-zA-Z/ ]+\]#','',$result['title']);

						echo <<<ENDECHO
						<li class="faqsol_contact_result_container">
							<a href="$result_url" class="faqsol_contact_result_link" rel="external">$result_title</a>
						</li>

ENDECHO;
						unset($result_url);
						unset($result_title);
						unset($result_desc);
						unset($result_cat);
						$i++;
					}
		        }
				echo "\n\t\t\t\t</ul>\n";
		    }
			echo "\t\t\t</div>\n";
			echo "\t\t\t".'<form action="../contact/" method="post">',"\n";
			echo "\t\t\t\t".'<fieldset class="contact_form_checked_submit">',"\n";
			foreach($_POST as $key => $value) {
				echo "\t\t\t\t\t".'<input type="hidden" name="checked_'.$key.'" id="'.$key.'" value="'.$value.'" />',"\n";
			}
			echo "\t\t\t\t\t".'<input type="submit" value="This hasn\'t helped, please just submit my query" />',"\n";
			echo "\t\t\t\t".'</fieldset>',"\n";
			echo "\t\t\t".'</form>',"\n";
			wrapper_end_html();
		} else {
			contact_form('Please ensure all required fields are completed and your email is correct.',$faqsol_search_name,$faqsol_search_email,$faqsol_search_email_conf,$faqsol_search_subject,$faqsol_search_albumid,$faqsol_search_query);
		}
		isearch_close();
		
	} else {
		contact_form();
	}
	
}

?>