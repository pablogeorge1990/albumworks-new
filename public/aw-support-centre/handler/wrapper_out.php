<?php

if (strstr($_SERVER['REQUEST_URI'],'handler') != FALSE) { header('HTTP/1.1 404 Not Found'); die; }

$global_vendor = '';

function wrapper_start_html($vendor,$title,$section,$onload_js='',$full_title=TRUE,$meta_keywords=NULL,$pseudorandid_tag='',$hide_header=FALSE,$hide_tabs=FALSE) {
	
	if(strstr($_SERVER['REQUEST_URI'],'aw-support-centre') == FALSE) {
		$search_action = '../support';
		$slashend = '';
	} else {
		$search_action = '../'.$vendor.'/';
		$slashend = '/';
	}
	
	global $global_vendor;
	$global_vendor = $vendor;

	$search_id_ext = 'an';
	$browse_id_ext = 'an';
	$disp_subtitle = ' - '.$title;
	$search_link = '../../support'.$slashend;
	$browse_link = '../browse'.$slashend;
	$js_link = '../media/support_js/';
	
	if ($section == 'search') {
		$search_id_ext = 'aa';
		$css_path = 'media/search_css/';
		$search_link = $search_action;
		$browse_link = 'browse'.$slashend;
		$js_link = 'media/support_js/';
		
	} elseif ($section == 'browse') {
		$browse_id_ext = 'aa';
		$css_path = '../media/browse_css/';
		$search_link = '../'.$search_action;
		
	} elseif ($section == 'solution') {
		$css_path = '../../media/solution_css/';
		$disp_subtitle = '';
		$search_link = '../../'.$search_action;
		$browse_link = '../../browse'.$slashend;
		$js_link = '../../media/support_js/';
		
	} elseif ($section == 'contact') {
		$css_path = '../media/contact_css/';
		
		if ($vendor == '3255') {
			$search_link = 'https://www.albumworks.com.au/target-photobooks/support.html" target="_top';
		} elseif ($vendor == '3183') {
			$search_link = 'https://www.officeworks.com.au/photobooks/support" target="_top';
		} elseif ($vendor == '1031') {
			if(strstr($_SERVER['REQUEST_URI'],'aw-support-centre') == FALSE) {
				$search_link = '../../support';
			} else {
				$search_link = '../';
			}
		}
		
	}
	
	switch ($vendor) {
		case '1031':
			$track_url 	= 'https://www.albumworks.com.au/track';
            $manual_url = 'https://www.albumworks.com.au/aw-support-centre/manual/albumworks user guide.pdf';
			$blog_url	= 'https://www.albumworks.com.au/blog';
			break;
		case '3183':
			header('P3P: CP="ALL DSP COR CUR CONi OUR STP ONL"');
			$track_url 	= 'https://www.officeworks.com.au/photobooks/track" target="_top';
			$manual_url	= 'https://www.albumworks.com.au/aw-support-centre/manual/Officeworks%20Photobooks%20user%20manual.pdf" target="_top';
			break;
		case '3255':
			$track_url 	= 'https://www.albumworks.com.au/target-photobooks/track.html" target="_top';
			$manual_url	= 'https://www.albumworks.com.au/aw-support-centre/manual/Target%20Photobooks%20user%20manual.pdf" target="_top';
			break;
	}
	

	$vendor_code = array(1031 => 'albumworks', 3183 => 'Officeworks Photobooks', 3255 => 'Target Photobooks');
	
	$generated_title = ($full_title==TRUE ? $vendor_code[$vendor].' Support Centre - '.$title : $title);
	$generated_keywords = ($meta_keywords==NULL ? '' : "\n\t\t<meta name=\"keywords\" content=\"$meta_keywords\" />");


	echo <<<ENDECHO
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "https://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en">

	<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />{$generated_keywords}{$pseudorandid_tag}
		<link rel="stylesheet" type="text/css" media="screen" title="Default Style" href="$css_path" />
		<script src="$js_link" type="text/javascript"></script>
		<title>$generated_title</title>
	</head>

	<body>

ENDECHO;

	if ($hide_header == FALSE) {
		echo <<<ENDECHO
	<div class="faqsol_container">
			<div class="faqsol_header">
                <!--
				    <h1 class="faqsol_head_title">$vendor_code[$vendor]</h1>
				    <h2 class="faqsol_head_subtitle contentheading">Support Centre $disp_subtitle</h2>
                -->
				<span class="faqsol_head_tabs">
					<a href="$search_link" title="Search through solutions using keywords" class="faqsol_head_tabs_search faqsol_head_tabs_$search_id_ext">Search</a>

ENDECHO;

		if ($hide_tabs == FALSE) {
			echo <<<ENDECHO
					<span class="faqsol_head_tabs_divider"> | </span>
					<a href="$browse_link" title="Browse all our solutions by category" class="faqsol_head_tabs_browse faqsol_head_tabs_$browse_id_ext">Browse</a>

ENDECHO;
		}

		?>
				</span>
			</div>
            <!--
            <span class="faqsol_head_otherlinks">
                <a href="<?=$manual_url?>" rel="top" class="faqsol_head_otherlinks_manual">Manual</a>
                <span class="faqsol_head_otherlinks_divider"> | </span>
                <a href="<?=$track_url?>" rel="top" class="faqsol_head_otherlinks_trackorder">Track My Order</a>
                <? if($blog_url): ?>
                    <a href="<?=$blog_url?>" rel="top" class="faqsol_head_otherlinks_blog">Blog</a>
                <? endif; ?>
            </span>            
            -->
        <?
	}

}                                                                                                             

function wrapper_end_html() {
	global $global_vendor;
	if($global_vendor != '1031') {
		$tracking = '

	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	try{
	var pageTracker = _gat._getTracker("UA-1338422-1");
	pageTracker._trackPageview();
	} catch(err) {}
	</script>

';
	} else {
		$tracking = '';
	}
	echo <<<ENDECHO
		</div>
{$tracking}
	</body>

</html>
ENDECHO;
}

?>
