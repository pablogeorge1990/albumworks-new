<?php

// Redirect to new version
Header( "HTTP/1.1 301 Moved Permanently" );
Header( "Location: https://api.photo-products.com.au/productionApp" );
exit(0);

?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Cover Text Match</title>
        <script type="text/javascript" src="mootools-core-1.3-full-nocompat-yc.js"></script>
		<script type="text/javascript" src="mootools-more.js"></script>
		<script type="text/javascript">
			Element.implement({
				flash: function(to,from,reps,prop,dur) {
					
					//defaults
					if(!reps) { reps = 1; }
					if(!prop) { prop = 'background-color'; }
					if(!dur) { dur = 250; }
					
					//create effect
					var effect = new Fx.Tween(this, {
							duration: dur,
							link: 'chain'
						})
					
					//do it!
					for(x = 1; x <= reps; x++) {
						effect.start(prop,from,to).start(prop,to,from);
					}
				}
			});
				
			window.addEvent('domready', function() {
				var audioElements=new Array();
				audioElements[0] = document.createElement('audio');
				audioElements[0].src = '3beep.ogg';
				audioElements[0].type = 'audio/ogg';
				audioElements[0].load();
				audioElements[1] = document.createElement('audio');
				audioElements[1].src = 'wrong.ogg';
				audioElements[1].type = 'audio/ogg';
				audioElements[1].load();
				audioElements[2] = document.createElement('audio');
				audioElements[2].src = 'beep.ogg';
				audioElements[2].type = 'audio/ogg';				
				audioElements[2].load();
				audioElements[99] = document.createElement('audio');
				audioElements[99].src = 'dubdub.ogg';
				audioElements[99].type = 'audio/ogg';
				audioElements[99].load();			

				var textarea = $('inputText'), log = $('outputText'), cur = $('currentText');
				textarea.focus();
				// We define the highlight morph we're going to
				// use when firing an event
				var highlight = new Fx.Morph(log, {
					duration: 15000,
					link: 'cancel',
					transition: 'quad:out'
				});
								
				function playSound(i) {
				    audioElements[i].pause();
				    //audioElements[i].currentTime=0; 
				    audioElements[i].play();		
				}
				function positiveMatch() {
					updateOutput('MATCH','#76DF38');
					playSound(0);
				}	
				function negativeMatch() {
					updateOutput('NOT A MATCH','#FF5454');
					playSound(1);
					doFlash();
				}
				function doFlash() {
					$(document.body).flash('#FF5454','#FFFFFF',8,'background-color',100);  				
				}
				function updateOutput(text, color) {
					// When the textarea contains one of the magic words
					// we reset textarea value and set the log with text
					textarea.value = ''; log.set('html', text);
					log.setStyle('background-color',color);
					// then we start the highlight morphing
//					highlight.start({
//						backgroundColor: ['#76DF38', '#fff'],
//						opacity: [1, 0]
//					});					
				}
				
				var lastScan = null;
				var lastBlockChar = null;
				textarea.addEvents({
					focus: function() {
						// When focusing, if the textarea contains value "Type here", we
						// simply clear it.
						if (textarea.value.contains('Type here')) textarea.value = '';
					},
					
					keyup: function() {
						if 	(textarea.value.contains('clear me')) textarea.fireEvent('burn', 'hello world!');
						else if (textarea.value.contains('let there be dub')) textarea.fireEvent('dubdub');
						else if (textarea.value.contains('\n')) {
							playSound(2);

							curScan = textarea.value.replace(/[^\w\.]/g,"").toLowerCase();
							blockChar = curScan.replace(/[^a-zA-Z]/g,"");
							
							if(lastScan != null) {
								if(blockChar == 'c') {	
									if(curScan == 'c'+lastScan) {
										positiveMatch();
									} else {
										negativeMatch();
									}
								} else {
									if(curScan == lastScan.replace(/[a-zA-Z]/g,"") && lastBlockChar != blockChar) {
										positiveMatch();
									} else {
										negativeMatch();
									}
								}

								lastScan = null;
								lastBlockChar = null;
								log.value = '';
								textarea.setStyle('background-color','');
							} else {                  
								lastScan = curScan;
								lastBlockChar = blockChar;
								textarea.setStyle('background-color','#76DF38');
								log.setStyle('background-color','');
								log.set('html', '');
								cur.set('html',curScan);
							}
							textarea.value = '';
							textarea.fireEvent('burn', 'newline');
						}

					},
					burn: function(text) {
						textarea.value = '';
					},
					dubdub: function() {
						textarea.value = '';
						playSound(99);
					}
				});
			});
		</script>
		<style>
			* {
				font-family: Arial, Helvetica, sans-serif;
			}
			body {
				margin: 0;
				padding: 0;
			}
			.clear {
				clear: both;
			}
			.header {
				width:100%;
				background: #ff8040;
				text-align: center;
				padding: 15px 0 15px 0;	
				margin: 0 0 20px 0;		
			}
			.header h1 {
				padding: 0px;
				margin: 0px;
			}
			.main-wrap {
				width: 500px;
				margin: 0 auto 0 auto;
			}
			#inputText {
				float:left;
				height: 24px;
				width: 40%;
				font-size: 18px;
			}
			#currentText {
				padding: 4px 0 0 10px;
				float:right;
				height: 24px;
				width: 40%;
				font-size: 18px;
			}
			#outputText {
				margin: 20px 0 0 0;
				font-size: 24px;
				font-weight: bold;
				padding: 10px;
				text-align:center;
			}
		</style>
    </head>
    <body>
    	<div class="header"><h1>Cover Text Match</h1></div>
		<div class="main-wrap">
			
			<textarea id="inputText"></textarea>
			<div id="currentText"></div>
			<div class="clear"></div>
			
			
			<div id="outputText"></div>
		</div>
        
    </body>
</html>
