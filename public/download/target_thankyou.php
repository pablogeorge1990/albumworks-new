<?
if(isset($_GET['multiOS'])) {
	
	ob_start();
	include_once('download_location.txt');
	$base = ob_get_contents();
	ob_end_clean();
	$winFile = $base.'TargetPhotobookEditorSetup.exe';
	$macFile = $base.'TargetPhotobookEditorSetup.dmg';
?>
<html>
<head>
</head>

<link href="target.css" rel="stylesheet" type="text/css"/>

<body>
<div>
	
	<div style="text-align:left;font-weight: normal;padding-top: 20px;">
		<br /><br />
		Thank you for registering your details.<br /><br />
		Please download the Target Photobooks editor via one of the following links:<br />
		<div style="margin: 0 0 0 90px">
			<a href="<?=$winFile?>" class="dl_os_link"><img src="/images/icons/dl_windows.png"><span>Windows</span></a>
			<a href="<?=$macFile?>" class="dl_os_link"><img src="/images/icons/dl_apple.png"><span>Mac</span></a>
	    </div>
		<div style="clear:both;"></div>
    </div>
</div>

</body>
</html>
<? } else { ?>
<html>
<head>
</head>

<link href="target.css" rel="stylesheet" type="text/css"/>

<body>
<div>
	<div class="cjbox">

	<div style="text-align:left;font-weight: normal;padding-top: 20px;">
	<br /><br />
	Thank you for registering your details.<br /><br />
	Please download the Target Photobooks editor via the following button:<br />
	<br /><br />
	<br />
    <div style="text-align:center">
    	<?

    	ob_start();
    	include_once('download_location.txt');
    	$filename = ob_get_contents();
    	ob_end_clean();
    	$filename .= 'TargetPhotobooks.exe';
    //	echo('<iframe height="0" width="0" style="display:none" src="'.$filename.'"></iframe>');
    	echo("<a href='".$filename."'><img src='images/btn_download.gif' border='0'></a>");	?>
    </div>
    </div>
</div>

</body>
</html>
<? } ?>