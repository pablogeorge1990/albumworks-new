<?
if(isset($_GET['multiOS'])) {
	ob_start();
	include_once('download_location.txt');
	$base = ob_get_contents();
	ob_end_clean();
	$winFile = $base.'albumworksSetup.exe';
	$macFile = $base.'albumworksSetup.dmg';
	
	if(preg_match('/Mac/',$_SERVER['HTTP_USER_AGENT']) > 0) $defaultFile = $macFile;
	else $defaultFile = $winFile;
	
?>

	<body style="text-align:center;">
	<div style="padding: 20px 0 0 0;">	
	
				<p style="text-align:center;color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px">Thank you for downloading our software. We hope you have a lot of fun producing your books.</p>
				<p style="text-align:center;color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px">If your download doesn't start in the next few seconds, you can download the editor via the following link:</p>
				<a style="text-decoration: none; border: 0; color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px" href="<?=$winFile?>"><img style="border:0" src="/images/icons/dl_windows.png" /><span>Windows</span></a>
				<a style="text-decoration: none; border: 0; color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px" href="<?=$macFile?>"><img style="border:0" src="/images/icons/dl_apple.png" /><span>Mac</span></a>
				<p><a style="font-weight:normal;font-size:12px;text-decoration:none;color:#47451F;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;" href="http://www.personaleditionpublishing.com.au/" target="_top">Click here to return Home</a></p>
				<iframe src="<?=$defaultFile?>" width="1" height="1" border="0" style="border: 0" ></iframe>
	
	</div>
	</body>
	
<? } else {?>

	<body style="text-align:center;">
	<div style="padding: 20px 0 0 0;">	
	
				<p style="text-align:center;color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px">Thank you for downloading our software. We hope you have a lot of fun producing your books.</p>
				<p style="text-align:center;color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px">If your download doesn't start in the next few seconds, you can download the editor via the following link:</p>
				<a style="text-decoration: none; border: 0; color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px" href="http://www.albumworks.com.au/albumworksSetup.exe"><img style="border:0" src="/images/icons/dl_windows.png" /><span>Windows</span></a>
				<a style="text-decoration: none; border: 0; color:#3D4B51;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;font-size:12px" href="http://www.albumworks.com.au/albumworksSetup.dmg"><img style="border:0" src="/images/icons/dl_apple.png" /><span>Mac</span></a>
				<p><a style="font-weight:normal;font-size:12px;text-decoration:none;color:#47451F;font-family:Verdana,Geneva,Arial,Helvetica,Sans-Serif;" href="http://www.personaleditionpublishing.com.au/" target="_top">Click here to return Home</a></p>
				<iframe src="https://www.albumworks.com.au/albumworksSetup.exe" width="1" height="1" border="0" style="border: 0" ></iframe>
	
	</div>
	</body>

<? } ?>