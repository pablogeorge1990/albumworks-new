<?	
	$winFile = 'https://s3-ap-southeast-1.amazonaws.com/albumprinter-editors/MomentsByMockingbird.exe';
	$macFile = 'https://s3-ap-southeast-1.amazonaws.com/albumprinter-editors/MomentsByMockingbird.dmg';
?>
<html>
<head>
</head>

<link href="mockingbird.css" rel="stylesheet" type="text/css"/>

<body>
<div>
	
	<div style="text-align:left;font-weight: normal;padding-top: 20px; padding-left:12px">
            <img src="mockingbird-thankyou.jpg" /><br>
			<a href="<?=$winFile?>" class="dl_os_link"><img src="/images/icons/dl_windows.png"><span>Windows</span></a>
			<a href="<?=$macFile?>" class="dl_os_link"><img src="/images/icons/dl_apple.png"><span>Mac</span></a>
		<div style="clear:both;"></div>
    </div>
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
        (function(){var a={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(a){for(var b=0;b<a.length;b++){var c=a[b].string;var d=a[b].prop;this.versionSearchString=a[b].versionSearch||a[b].identity;if(c){if(c.indexOf(a[b].subString)!=-1)return a[b].identity}else if(d)return a[b].identity}},searchVersion:function(a){var b=a.indexOf(this.versionSearchString);if(b==-1)return;return parseFloat(a.substring(b+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};a.init();window.jQuery.client={os:a.OS,browser:a.browser}})()
        jQuery(document).ready(function(){
            var download_link = '<?=$winFile?>';
            if(jQuery.client.os == 'Mac')
                download_link = '<?=$macFile?>';
            jQuery('body').append('<iframe id="downloadiframe" style="position:absolute; width:1px; height:1px; top:-999em; left:-999em;" src="'+download_link+'"></iframe>');
        });    
    </script>
</div>

</body>
</html>