<?

/**
 * Keeps the Albumprinter download live.
 * If the primary location for some reason goes offline, change to the
 * backup location, and when primary comes back online, revert back.
 * 
 * Chris Duell
 */

// Location of the download, primary and secondary
$primary = "http://www.albumprinter.com/au/";
//$secondary = "http://www.salsastaging.com.au/albumprinter_dl/";
$secondary = "http://www.albumprinter.com.au/";
$test_file = "do_not_delete.txt";
$dl_location_file = "/home/albumpri/www/download/download_location.txt";
$location = $primary;

$primary_live = true;


// Check if the test file in the download directory can be read.
$url=$primary.$test_file;
$addy=parse_url($url);
$addy['port']=80;
$sh=@fsockopen($addy['host'],$addy['port']);
@fputs($sh,"HEAD {$addy['path']} HTTP/1.1\r\nHost: {$addy['host']}\r\n\r\n");

while($line=@fgets($sh)){
	echo("checking preg match on:<br>");
	print_r($line);
	echo("<br>");
	//if(preg_match('/^Content-Length:/',$line)){
	if(preg_match('/404 Not Found/',$line)){
		echo("MATCH - site is down");
		$primary_live = false;
	}
}


// Make sure the backup location is selected if the primary is down.
attempt($primary_live);
if(!$primary_live){
	if(get_attempt_count()>2)
		$location = $secondary;
}

//REMOVE BELOW LINE WHEN PRIMARY LOCATION KNOWN!!!!!
$location = $secondary;	

// Write the location of the best available location to file.
if (is_writable($dl_location_file)) {

   if (!$handle = fopen($dl_location_file, 'w')) {
         exit;
   }

   // Write $somecontent to our opened file.
   if (fwrite($handle, $location) === FALSE) {
       exit;
   }
  
   fclose($handle);
}
else{
	echo("Cannot write to $dl_location_file");
}



// Add one to attempt count if no success, or set back to zero
function attempt($success){
	echo('Primary: ');
	echo($success?'LiVe':'Down');
	echo('<br>');
	$count = get_attempt_count()-0; // Leave '-0', this converts value to integer
	$filename = "/home/albumpri/www/download/attempts.txt";
	if(!$success){
		$count++;
		echo("<br>Incremented to: " . $count);
	}
	else
		$count = 0;
	
	// Write new count to file.
	if (!$handle = fopen($filename, 'w')) {
         exit;
   }
   // Write $somecontent to our opened file.
   if (fwrite($handle, $count) === FALSE) {
       exit;
   }
   
   echo("<br>New count: " . $count);
   
   fclose($handle);
}



// Read how many bad attempts there have been in succession
function get_attempt_count(){
	// get contents of a file into a string
	$filename = "/home/albumpri/www/download/attempts.txt";
	$handle = fopen($filename, "r");
	$count = fread($handle, filesize($filename));
	fclose($handle);
	
	echo("<br>Current count: " . $count);
	
	return $count;
}
?>
