<?
	$winFile = 'https://s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/PhotoLoungePROSetup.exe';
	$macFile = 'https://s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/PhotoLoungePROSetup.dmg';
?>
<html>
<head>
</head>

<link href="target.css" rel="stylesheet" type="text/css"/>

<body>
<div>
	
	<div style="text-align:left;font-weight: normal;padding-top: 0;">
        <h2 style="color: #E2002C; font: normal 21px/1 Frutigeb,Arial,Helvetica,Sans-serif;margin: 0 0 0.5em;">Thank you!</h2>
		Thank you for registering your details.<br /><br />
		Please download the Ted's Print &amp; Create Editor via one of the following links:<br />
		<div style="margin: 0 0 0 90px">
			<a href="<?=$winFile?>" class="dl_os_link"><img src="/images/icons/dl_windows.png"> <span>Windows</span></a>
			<a href="<?=$macFile?>" class="dl_os_link"><img src="/images/icons/dl_apple.png"> <span>Mac</span></a>
	    </div>
		<div style="clear:both;"></div>
    </div>
</div>

</body>
</html>
