<?php
//$retURL = "http://192.168.1.2/albumprinter2/download-thankyou.html";
// changed by Adam - 25 Jan 07 11.13PM
// changed by Max - 22 January 2013

header('P3P: CP="ALL DSP COR CUR CONi OUR STP ONL"');

## $retURL = "http://www.albumworks.com.au/download-thankyou.html";
$leadSrc = "Website";
$dl_param_cookie_store = 'false';
$refPromo = '';
$skip_os_check = false;
if(isset($_GET['ow'])){
    $dl_param_cookie_vendor = 'ow';
##     $retURL = "http://www.albumworks.com.au/download/officeworks_thankyou.php";
    $leadSrc = "Officeworks";
    if($_SERVER["REMOTE_ADDR"] == '203.6.240.254')
    echo('<link href="office_works-temp.css" rel="stylesheet" type="text/css"/>');
    else
    echo('<link href="office_works.css" rel="stylesheet" type="text/css"/>');
}
else if(isset($_GET['br'])){
	$dl_param_cookie_vendor = 'br';
## 	$retURL = "http://www.albumworks.com.au/download/officeworks_thankyou.php";
	$leadSrc = "Britz";
	echo('<link href="britz.css" rel="stylesheet" type="text/css"/>');
}
else if(isset($_GET['edsoft'])){
	$dl_param_cookie_vendor = 'es';
## 	$retURL = "http://www.albumworks.com.au/download/edsoft_thankyou.php";
	$leadSrc = "Edsoft";
	echo('<link href="edsoft.css" rel="stylesheet" type="text/css"/>');
}
else if(isset($_GET['target'])){
    $dl_param_cookie_vendor = 'tg';
##     $retURL = "http://www.albumworks.com.au/download/target_thankyou.php";
    $leadSrc = "Target";
    echo('<link href="target.css" rel="stylesheet" type="text/css"/>');
}
else if(isset($_GET['teds'])){
	$dl_param_cookie_vendor = 'td';
## 	$retURL = "http://www.albumworks.com.au/download/target_thankyou.php";
	$leadSrc = "Ted's PhotoLounge PRO";
	echo('<link href="teds.css" rel="stylesheet" type="text/css"/>');
	$skip_os_check = true;
}
else if(isset($_GET['target2'])){
    $dl_param_cookie_vendor = 'tg';
##     $retURL = "http://www.albumworks.com.au/download/target_thankyou.php";
    $leadSrc = "Target2";
    echo 
    '<link href="target.css" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="target-ie.css" />
    <![endif]-->';
}
else if(isset($_GET['mockingbird'])){
	$dl_param_cookie_vendor = 'mb';
## 	$retURL = "http://www.albumworks.com.au/download/target_thankyou.php";
	$leadSrc = "Mockingbird";
	echo 
	'<link href="mockingbird.css" rel="stylesheet" type="text/css"/>
	<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="mockingbird-ie.css" />
	<![endif]-->';
}
else if(isset($_GET['pp'])){
	$dl_param_cookie_vendor = 'pp';
## 	$retURL = "http://www.albumworks.com.au/download/officeworks_thankyou.php";
	$leadSrc = "PEP";
	echo('<link href="pep.css" rel="stylesheet" type="text/css"/>');
}
else {
	$dl_param_cookie_vendor = 'ap';
	if(isset($_COOKIE['source'])){
		$leadSrc = $_COOKIE['source'];
	}
}
if(isset($_GET['promotion']) && $_GET['promotion']!=null) {
	$promos = array('babyBumperthonSep09');
	foreach($promos as $promo) {
		if($_GET['promotion'] == $promo) {
			$refPromo = $_GET['promotion'];
			break;
		}
	}
	
}
// the above retURLs were commented out as we need to redirect users to the script below to complete the download process actions on the customers Account/Contact
// this redirection will be momentary, and the cust will be pushed on to the appropriate download page in a split second
$retURL = 'https://www.albumworks.com.au/download/dl_process.php?'.$dl_param_cookie_vendor.'&store='.$dl_param_cookie_store;
$sfUrl = "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8";
//echo $leadSrc;
?>

<script type="text/javascript">

function validateForm(){

	// set dl params cookie - update ~mitch
	if (document.getElementById("emailOptOut").checked == true) {
		option_mail = 0;
	} else {
		option_mail = 1;
	}
	
	if (document.getElementById("00N200000012y40").checked == true) {
		option_updates = 1;
	} else {
		option_updates = 0;
	}

	name = document.getElementById("first_name").value;
	email = document.getElementById("email").value;
	name = 	name.replace("&", "");
	email = email.replace("&", "");
	name = 	name.replace(";", "");
	email = email.replace(";", "");
	name = 	name.replace(":", "");
	email = email.replace(":", "");
	name = 	name.replace("=", "");
	email = email.replace("=", "");
	name = 	name.replace("|", "");
	email = email.replace("|", "");
	name = 	name.replace("'", "");
	email = email.replace("'", "");
	name = 	name.replace('"', "");
	email = email.replace('"', "");
	document.cookie = "download_params=vendor:<? echo $dl_param_cookie_vendor; ?>|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:"+option_updates+"|store:<? echo $dl_param_cookie_store; ?> ; path=/; domain=.albumworks.com.au";
	// end set dl params
	
	// check cookie (and in doing so check JS). if its all ok, set 00N3600000LosKl // Web-to-Lead Bad JS/Cookie to 0 - update ~mitch
	var iscookie = readCookie('download_params');
	if (iscookie) {
		document.getElementById("00N3600000LosKl").value = 0;
	}
	// end check cookie
	
	// set referring promotion
	var refpromo = readCookie('APrefpromo');
	if (refpromo) {
		document.getElementById("00N3600000LosAC").value = refpromo;
	}
	
	var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var pass = pattern.test(document.getElementById("email").value);
	if(document.dlform.email.value=='' || !pass){
		alert('Please enter a valid email address');
		return false;
	}
    if(document.dlform.first_name.value==''){
        alert('Please enter your first name');
        return false;
    }
    if(document.dlform.last_name.value==''){
        alert('Please enter your last name');
        return false;
    }
<?php if(!$skip_os_check){ ?>
	if(!document.getElementById('os_Windows').checked && !document.getElementById('os_MacOS').checked) {
		alert('Please select your operating system');
		return false;
	}
<?php } ?>

	var dlf = document.getElementById('dlformID');
	dlf.style.visibility = 'hidden';

	var email = document.dlform.emailOptOut;
	var optin2 = document.getElementById("00N200000012y40");

    /*
	if(email.checked) {
//alert('Thankyou');
		email.checked = false;
		optin2.checked = false;

	} else if(!email.checked && !optin2.checked){
		email.checked = true;
//alert('Thankyou.');
	} else {
//alert('Thankyou..');

	}
    */
	
	return true;
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}


</script>

<?php if($leadSrc == "Website") { 
// -----------------------------------------------------------------------------------------------------
// WEBSITE
// -----------------------------------------------------------------------------------------------------
?>
<div align="center">
<div align="center" style="padding-left:0px">
<h3>Download the<br />free software</h3>
<img src="images/dloadPage/arrow.gif" />
<form action="<?=$sfUrl?>" method="POST" name="dlform" id="dlformID"  onsubmit="return validateForm()">
<!------------------
    This HTML is defaulted to DEBUG mode so that your web master can test the online Web-to-Lead page from the desktop.  Once your web master has finished testing please remember to remove the entire "debug" and "debugEmail" lines of code prior to adding this HTML to your website.
<input type="hidden" name="debug" value=1>
<input type="hidden" name="debugEmail" value="chris@salsainternet.com.au">-->
<input type="hidden" name="sfga" value="00D36000000oZE6"/>
<input type="hidden" name="oid" value="00D36000000oZE6">
<input type="hidden" name="retURL" value="<?=$retURL?>">
<input type="hidden" name="lead_source" value="<?=$leadSrc?>">
<input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
<input type="hidden" name="00N3600000BOyGd" value="Download">
<!-- Store value --><input type="hidden" id="00N20000001nmem" name="00N20000001nmem" value="" />
<!-- Referring Promotion --><input type="hidden" id="00N3600000LosAC" name="00N3600000LosAC" value="" />
<input type="hidden" name="00N3600000BOyAt" value="AP" />
<input type="hidden" value="PG" name="00N3600000Loh5K">

<!-- The following field is for Web-to-Lead Bad JS/Cookie. It will be set to 0 if the JS and Cookie set and check goes OK -->
<input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
<!-- End Web-to-Lead Bad JS/Cookie check -->

<!-- <?php $sendDateTime =  date('d/m/Y G:i:s a');
echo $sendDateTime;
?>  -->
<input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
<table border="0" cellpadding="0" cellspacing="0">
<tr><td rowspan="3" width="30px">&nbsp;</td>
<td align="left">Name:<br />
<input  maxlength="40" type="text" id="first_name" size="20" name="first_name"></td></tr>
<tr><td align="left">Email:<br />
<input  maxlength="80" type="text" id="email" size="20" name="email"></td></tr>
<tr><td colspan="2"><br />
<input type="image" src="images/dloadPage/dloadButton.gif" name="submit"/></td></tr>
<tr><td colspan="3"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Keep me up to date with special offers and software updates</label></td></tr>
<tr><td colspan="3" id="optin2"><input  id="00N200000012y40" name="00N200000012y40" checked="checked" type="checkbox" value="1" /><label for="00N200000012y40">Only email me notification of product and software updates</label>
</td></tr>
</table>
</form>
</div>
</div>


<?php } 

else if($leadSrc == "Officeworks") {
// -----------------------------------------------------------------------------------------------------
// OFFICEWORKS
// -----------------------------------------------------------------------------------------------------
    if($_SERVER["REMOTE_ADDR"] == '203.6.240.254') {
        ?>
        <div class="form-heading">
            <h2>Download Free Software</h2>
            <div>
                Required information <span class="asterix">*</span>
            </div>
        </div>
        <div class="form-body">
        <form action="<?=$sfUrl?>" method="POST" id='dlformID' name="dlform" onsubmit="return validateForm()">

            <!------------------
                This HTML is defaulted to DEBUG mode so that your web master can test the online Web-to-Lead page from the desktop.  Once your web master has finished testing please remember to remove the entire "debug" and "debugEmail" lines of code prior to adding this HTML to your website.
            <input type="hidden" name="debug" value=1>
            <input type="hidden" name="debugEmail" value="chris@salsainternet.com.au">-->
            <input type="hidden" name="sfga" value="00D36000000oZE6"/>
            <input type="hidden" name="oid" value="00D36000000oZE6">
            <input type="hidden" name="retURL" value="<?=$retURL?>">
            <input type="hidden" name="lead_source" value="<?=$leadSrc?>">
            <input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
            <input type="hidden" name="00N3600000BOyGd" value="Download">
            <!-- The following field is for Web-to-Lead Bad JS/Cookie. It will be set to 0 if the JS and Cookie set and check goes OK -->
            <input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
            <!-- End Web-to-Lead Bad JS/Cookie check -->
            <input type="hidden" name="00N200000013fDx" value="OW" />
            <input type="hidden" value="PG" name="00N3600000Loh5K">
            <!-- <?php $sendDateTime =  date('d/m/Y G:i:s a');
            echo $sendDateTime;
             ?>  -->
            <input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
            
            <div class="input-field"><label for="first_name">First Name<span class="asterix">*</span></label>
                <input type="text" value="" maxlength="50" name="first_name" rel="First name" class="rem" id="first_name" /></div>
            <div class="input-field"><label for="last_name">Last Name<span class="asterix">*</span></label>
                <input type="text" value="" maxlength="50" name="last_name" rel="Last name" class="rem" id="last_name" /></div>
            <div class="input-field"><label for="email">Email<span class="asterix">*</span></label>
                <input type="text" value="" maxlength="100" name="email" rel="Email" class="rem" id="email" /></div>
            <div class="boxes">
                <div class="check-area"><input value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" />
                <label class="check-label" for="emailOptOut">Keep me up to date with special offers, promotions and software updates</label></div>
                <div class="check-area"><input id="00N200000012y40" name="00N200000012y40" checked="checked" type="checkbox" value="1" />
                <label class="check-label" for="00N200000012y40">Only email me notification of software updates and product changes</label></div>
            </div>
            <input id="dbtn" type="submit" value="Download" />
        </form>
        </div>
        <div class="form-bottom">
        </div>
        <?php 
    } 
    else {
        ?>
        <div style="padding-left:25px">
        <form action="<?=$sfUrl?>" method="POST" id='dlformID' name="dlform" onsubmit="return validateForm()">

        <!------------------
            This HTML is defaulted to DEBUG mode so that your web master can test the online Web-to-Lead page from the desktop.  Once your web master has finished testing please remember to remove the entire "debug" and "debugEmail" lines of code prior to adding this HTML to your website.
        <input type="hidden" name="debug" value=1>
        <input type="hidden" name="debugEmail" value="chris@salsainternet.com.au">-->
        <input type="hidden" name="sfga" value="00D36000000oZE6"/>
        <input type="hidden" name="oid" value="00D36000000oZE6">
        <input type="hidden" name="retURL" value="<?=$retURL?>">
        <input type="hidden" name="lead_source" value="<?=$leadSrc?>">
        <input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
        <input type="hidden" name="00N3600000BOyGd" value="Download">
        <!-- The following field is for Web-to-Lead Bad JS/Cookie. It will be set to 0 if the JS and Cookie set and check goes OK -->
        <input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
        <!-- End Web-to-Lead Bad JS/Cookie check -->
        <input type="hidden" name="00N200000013fDx" value="OW" />
        <input type="hidden" value="PG" name="00N3600000Loh5K">
        <!-- <?php $sendDateTime =  date('d/m/Y G:i:s a');
        echo $sendDateTime;
         ?>  -->
        <input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
        <table border="0">
        <tr><td><label for="first_name">First Name:</label></td><td><input  maxlength="40" type="text" id="first_name" size="20" name="first_name"></td></tr>
        <tr><td><label for="last_name">Last Name:</label></td><td><input  maxlength="40" type="text" id="last_name" size="20" name="last_name"></td></tr>
        <tr><td><label for="email">Email:</label></td><td><input  maxlength="80" type="text" id="email" size="20" name="email"></td></tr>

        <tr>
            <td colspan="2" class="os_select">
                <input type="radio" value="Windows" id="os_Windows" name="00N3600000Los6F">
                <label id="label_win" for="os_Windows"><img src="/images/icons/dl_windows.png" alt="Windows">Windows</label>
                <input type="radio" value="MacOS" id="os_MacOS" name="00N3600000Los6F" class="last">
                <label id="label_mac" for="os_MacOS"><img src="/images/icons/dl_apple.png" alt="MacOS">Mac</label>                
            </td>
        </tr>        

        <tr><td colspan="2"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Keep me up to date with special offers, promotions and software updates</label></td></tr>
        <tr><td colspan="3" id="optin2"><input  id="00N200000012y40" name="00N200000012y40" checked="checked" type="checkbox" value="1" /><label for="00N200000012y40">Only email me notification of software updates and product changes</label>
        </td></tr>
        <tr><td colspan="2"><br><br><input type="submit" value="Download"></td></tr>
        </table>
        </form>
        </div>
        <?php
    }
} 




else if($leadSrc == "Britz") {
// -----------------------------------------------------------------------------------------------------
// OFFICEWORKS
// -----------------------------------------------------------------------------------------------------
        ?>
        <div style="padding-left:25px">
        <form action="<?=$sfUrl?>" method="POST" id='dlformID' name="dlform" onsubmit="return validateForm()">

        <!------------------
            This HTML is defaulted to DEBUG mode so that your web master can test the online Web-to-Lead page from the desktop.  Once your web master has finished testing please remember to remove the entire "debug" and "debugEmail" lines of code prior to adding this HTML to your website.
        <input type="hidden" name="debug" value=1>
        <input type="hidden" name="debugEmail" value="chris@salsainternet.com.au">-->
        <input type="hidden" name="sfga" value="00D36000000oZE6"/>
        <input type="hidden" name="oid" value="00D36000000oZE6">
        <input type="hidden" name="retURL" value="<?=$retURL?>">
        <input type="hidden" name="lead_source" value="<?=$leadSrc?>">
        <input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
        <input type="hidden" name="00N3600000BOyGd" value="Download">
        <!-- The following field is for Web-to-Lead Bad JS/Cookie. It will be set to 0 if the JS and Cookie set and check goes OK -->
        <input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
        <!-- End Web-to-Lead Bad JS/Cookie check -->
        <input type="hidden" name="00N200000013fDx" value="BR" />
        <input type="hidden" value="PG" name="00N3600000Loh5K">
        <!-- <?php $sendDateTime =  date('d/m/Y G:i:s a');
        echo $sendDateTime;
         ?>  -->
        <input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
        <table border="0">
        <tr><td><label for="first_name">First Name:</label></td><td><input  maxlength="40" type="text" id="first_name" size="20" name="first_name"></td></tr>
        <tr><td><label for="last_name">Last Name:</label></td><td><input  maxlength="40" type="text" id="last_name" size="20" name="last_name"></td></tr>
        <tr><td><label for="email">Email:</label></td><td><input  maxlength="80" type="text" id="email" size="20" name="email"></td></tr>

        <tr>
            <td colspan="2" class="os_select">
                <input type="radio" value="Windows" id="os_Windows" name="00N3600000Los6F">
                <label id="label_win" for="os_Windows"><img src="/images/icons/dl_windows.png" alt="Windows"> Windows</label>
                <input type="radio" value="MacOS" id="os_MacOS" name="00N3600000Los6F" class="last">
                <label id="label_mac" for="os_MacOS"><img src="/images/icons/dl_apple.png" alt="MacOS"> Mac</label>                
            </td>
        </tr>        

        <tr><td colspan="2"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Keep me up to date with special offers, promotions and software updates</label></td></tr>
        <tr><td colspan="3" id="optin2"><input  id="00N200000012y40" name="00N200000012y40" checked="checked" type="checkbox" value="1" /><label for="00N200000012y40">Only email me notification of software updates and product changes</label>
        </td></tr>
        <tr><td colspan="2"><br><button type="submit" class="button">Download</button></td></tr>
        </table>
        </form>
        </div>
        <?php
} 





else if($leadSrc == "Target") {
// -----------------------------------------------------------------------------------------------------
// TARGET
// -----------------------------------------------------------------------------------------------------
?>

<script type="text/javascript">

  function target_submit(){
      if(validateForm()){
          dlform.submit();
      }
  }

</script>

<form action="<?=$sfUrl?>" method="POST" id='dlformID' name="dlform">

    <!------------------
        This HTML is defaulted to DEBUG mode so that your web master can test the online Web-to-Lead page from the desktop.  Once your web master has finished testing please remember to remove the entire "debug" and "debugEmail" lines of code prior to adding this HTML to your website.
    <input type="hidden" name="debug" value=1>
    <input type="hidden" name="debugEmail" value="chris@salsainternet.com.au">-->
    <input type="hidden" name="sfga" value="00D36000000oZE6"/>
    <input type="hidden" name="oid" value="00D36000000oZE6">
    <input type="hidden" name="retURL" value="<?=$retURL?>">
    <input type="hidden" name="lead_source" value="<?=$leadSrc?>">
    <input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
    <input type="hidden" name="00N3600000BOyGd" value="Download">
    <!-- Referring Promotion --><input type="hidden" id="00N3600000LosAC" name="00N3600000LosAC" value="<?=$refPromo?>" />
    <!-- The following field is for Web-to-Lead Bad JS/Cookie. It will be set to 0 if the JS and Cookie set and check goes OK -->
    <input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
    <!-- End Web-to-Lead Bad JS/Cookie check -->
    <input type="hidden" name="00N200000013fDx" value="TG" />
    <input type="hidden" value="PG" name="00N3600000Loh5K">
    <!-- <?php $sendDateTime =  date('d/m/Y G:i:s a');
    echo $sendDateTime;
     ?>  -->
    <input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
    <table border="0">
        <tr>
            <td colspan="2"><img src="images/title_registerdetails.gif" /></td>
        </tr>
        <tr>
            <td><label for="first_name">First Name:</label></td>
            <td width="207px"><input  maxlength="40" type="text" id="first_name" name="first_name" style="width:100%" ></td>
        </tr>
        <tr>
            <td><label for="last_name">Last Name:</label></td>
            <td><input  maxlength="40" type="text" id="last_name" name="last_name" style="width:100%"></td>
        </tr>
        <tr>
            <td><label for="email">Email:</label></td>
            <td><input  maxlength="80" type="text" id="email" name="email" style="width:100%"></td>
        </tr>
        <tr>
            <td colspan="2"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Keep me up to date with special offers, <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;promotions and software updates</label></td>
        </tr>
        <tr>
            <td colspan="3" id="optin2"><input  id="00N200000012y40" name="00N200000012y40" checked="checked" type="checkbox" value="1" /><label for="00N200000012y40">Only email me notification of software<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;updates and product changes</label></td>
        </tr>
    </table>
    <br>
    <div style="text-align:center">
        <a href="#" onclick="target_submit();return false;"><img src="images/btn_continue.gif" style="border:0px" /></a>
    </div>
</form>


<?php } else if($leadSrc == "Ted's PhotoLounge PRO") {
// -----------------------------------------------------------------------------------------------------
// TED'S (but based on TARGET)
// -----------------------------------------------------------------------------------------------------
?>

<script type="text/javascript">

  function teds_submit(){
      if(validateForm()){
          dlform.submit();
      }
  }

</script>

<form action="<?=$sfUrl?>" method="POST" id='dlformID' name="dlform"> <!-- style="overflow:hidden; overflow-y:scroll; height:240px;"> -->

	<!------------------
	    This HTML is defaulted to DEBUG mode so that your web master can test the online Web-to-Lead page from the desktop.  Once your web master has finished testing please remember to remove the entire "debug" and "debugEmail" lines of code prior to adding this HTML to your website.
	<input type="hidden" name="debug" value=1>
	<input type="hidden" name="debugEmail" value="chris@salsainternet.com.au">-->
	<input type="hidden" name="sfga" value="00D36000000oZE6"/>
	<input type="hidden" name="oid" value="00D36000000oZE6">
	<input type="hidden" name="retURL" value="<?=$retURL?>">
	<input type="hidden" name="lead_source" value="<?=$leadSrc?>">
	<input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
	<input type="hidden" name="00N3600000BOyGd" value="Download">
	<!-- Referring Promotion --><input type="hidden" id="00N3600000LosAC" name="00N3600000LosAC" value="<?=$refPromo?>" />
	<!-- The following field is for Web-to-Lead Bad JS/Cookie. It will be set to 0 if the JS and Cookie set and check goes OK -->
	<input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
	<!-- End Web-to-Lead Bad JS/Cookie check -->
	<input type="hidden" name="00N3600000BOyAt" value="TD" />
    <input type="hidden" value="PG" name="00N3600000Loh5K">
	<!-- <?php $sendDateTime =  date('d/m/Y G:i:s a');
	echo $sendDateTime;
	 ?>  -->
	<input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
    <h2 style="color: #E2002C; font: normal 21px/1 Frutigeb,Arial,Helvetica,Sans-serif;margin: 0 0 10px;">Ted's Print &amp; Create Editor Download</h2>
    <p style="margin:5px 0 0">You're just seconds away from designing a beautiful photo product.</p>
    <p style="margin:0">Register below to download our easy-to-use Editor.</p>
    <p style="margin:0 0 5px">Once downloaded, you can open the Editor, select your product type and get started!    </p>
	<table border="0">
		<tr>
			<td><label for="first_name">First Name:</label></td>
			<td width="207px"><input  maxlength="40" type="text" id="first_name" name="first_name" style="width:100%" ></td>
		</tr>
		<tr>
			<td><label for="last_name">Last Name:</label></td>
			<td><input  maxlength="40" type="text" id="last_name" name="last_name" style="width:100%"></td>
		</tr>
		<tr>
			<td><label for="email">Email:</label></td>
			<td><input  maxlength="80" type="text" id="email" name="email" style="width:100%"></td>
		</tr>
	</table>
	<div style="text-align:left">
            <p style="margin:5px 0"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Keep me up to date with special offers, promotions and software updates</label></p>
            <p style="margin:5px 0" id="optin2"><input  id="00N200000012y40" name="00N200000012y40" checked="checked" type="checkbox" value="1" /><label for="00N200000012y40">Only email me notification of software updates and product changes</label></p>
		<button onclick="teds_submit();return false;">Submit</button>
	</div>
</form>


<?php } else if($leadSrc == "Target2") {
// -----------------------------------------------------------------------------------------------------
// TARGET2
// -----------------------------------------------------------------------------------------------------
$leadSrc = "Target";
$retURL .= '&multiOS';
?>

<script type="text/javascript">
    function validateForm2(){
    
        // set dl params cookie - update ~mitch
        if (document.getElementById("emailOptOut").checked == true) {
            option_mail = 0;
            
        } else {
            option_mail = 1;
        }
        option_updates = 0;
        name = document.getElementById("first_name").value;
        email = document.getElementById("email").value;
        name =     name.replace("&", "");
        email = email.replace("&", "");
        name =     name.replace(";", "");
        email = email.replace(";", "");
        name =     name.replace(":", "");
        email = email.replace(":", "");
        name =     name.replace("=", "");
        email = email.replace("=", "");
        name =     name.replace("|", "");
        email = email.replace("|", "");
        name =     name.replace("'", "");
        email = email.replace("'", "");
        name =     name.replace('"', "");
        email = email.replace('"', "");
        email = email.replace("+", "%2B");
        document.cookie = "download_params=vendor:<? echo $dl_param_cookie_vendor; ?>|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:"+option_updates+"|store:<? echo $dl_param_cookie_store; ?> ; path=/; domain=.albumworks.com.au";
        // end set dl params
        
        // check cookie (and in doing so check JS). if its all ok, set 00N3600000LosKl // Web-to-Lead Bad JS/Cookie to 0 - update ~mitch
        var iscookie = readCookie('download_params');
        if (iscookie) {
            document.getElementById("00N3600000LosKl").value = 0;
        }
        // end check cookie
        
        // set referring promotion
        var refpromo = readCookie('APrefpromo');
        if (refpromo) {
            document.getElementById("00N3600000LosAC").value = refpromo;
        }
        
        var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var pass = pattern.test(document.getElementById("email").value);
        if(document.dlform.email.value=='' || !pass){
            alert('Please enter a valid email address');
            return false;
        }
        
        if(!document.getElementById('os_Windows').checked && !document.getElementById('os_MacOS').checked) {
            alert('Please select your operating system');
            return false;
        }
        var dlf = document.getElementById('dlformID');
        dlf.style.visibility = 'hidden';
    
        var email = document.dlform.emailOptOut;
        //email.checked = !email.checked;
            
        return true;
    }

    function target_submit(){
        if(validateForm2()){    
            dlform.submit();
        }
    }
</script>
<br>
<form action="<?=$sfUrl?>" method="POST" id='dlformID' name="dlform" style="width: 480px;">

    <input type="hidden" name="sfga" value="00D36000000oZE6"/>
    <input type="hidden" name="oid" value="00D36000000oZE6">
    <input type="hidden" name="retURL" value="<?=$retURL?>">
    <input type="hidden" name="lead_source" value="<?=$leadSrc?>">
    <input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
    <input type="hidden" name="00N3600000BOyGd" value="Download">
    <input type="hidden" id="00N3600000LosAC" name="00N3600000LosAC" value="<?=$refPromo?>" />
    <input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
    <input type="hidden" name="00N200000013fDx" value="TG" />
    <input type="hidden" value="PG" name="00N3600000Loh5K">

    <input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
    <table border="0">
        <tr>
            <td colspan="2"><img src="images/title_registerdetails.gif" /></td>
        </tr>
        <tr>
            <td><label for="first_name">First Name:</label></td>
            <td width="207px"><input  maxlength="40" type="text" id="first_name" name="first_name" style="width:100%" ></td>
        </tr>
        <tr>
            <td><label for="last_name">Last Name:</label></td>
            <td width="207px"><input  maxlength="40" type="text" id="last_name" name="last_name" style="width:100%"></td>
        </tr>
        <tr>
            <td><label for="email">Email:</label></td>
            <td width="207px"><input  maxlength="80" type="text" id="email" name="email" style="width:100%"></td>
        </tr>
        <tr>
            <td colspan="2" class="os_select">
                <input type="radio" value="Windows" id="os_Windows" name="00N3600000Los6F">
                <label id="label_win" for="os_Windows"><img src="/images/icons/dl_windows.png" alt="Windows">Windows</label>
                <input type="radio" value="MacOS" id="os_MacOS" name="00N3600000Los6F" class="last">
                <label id="label_mac" for="os_MacOS"><img src="/images/icons/dl_apple.png" alt="MacOS">Mac</label>                
            </td>
        </tr>                    
        <tr>
            <td colspan="2"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Notify me of software updates and special offers</label></td>
        </tr>
    </table>
    <br>
    <div style="text-align:center">
        <a href="#" onclick="target_submit();return false;"><img src="images/btn_continue.gif" style="border:0px" /></a>
    </div>
</form>

<?php } else if($leadSrc == "Mockingbird") {
// -----------------------------------------------------------------------------------------------------
// TARGET2
// -----------------------------------------------------------------------------------------------------
$leadSrc = "Mockingbird";
$retURL .= '&multiOS';
?>

<script type="text/javascript">
	function validateForm2(){
	
		// set dl params cookie - update ~mitch
		if (document.getElementById("emailOptOut").checked == true) {
			option_mail = 0;
			
		} else {
			option_mail = 1;
		}
		option_updates = 0;
		name = document.getElementById("first_name").value;
		email = document.getElementById("email").value;
		name = 	name.replace("&", "");
		email = email.replace("&", "");
		name = 	name.replace(";", "");
		email = email.replace(";", "");
		name = 	name.replace(":", "");
		email = email.replace(":", "");
		name = 	name.replace("=", "");
		email = email.replace("=", "");
		name = 	name.replace("|", "");
		email = email.replace("|", "");
		name = 	name.replace("'", "");
		email = email.replace("'", "");
		name = 	name.replace('"', "");
		email = email.replace('"', "");
		email = email.replace("+", "%2B");
		document.cookie = "download_params=vendor:<? echo $dl_param_cookie_vendor; ?>|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:"+option_updates+"|store:<? echo $dl_param_cookie_store; ?> ; path=/; domain=.albumworks.com.au";
		// end set dl params
		
		// check cookie (and in doing so check JS). if its all ok, set 00N3600000LosKl // Web-to-Lead Bad JS/Cookie to 0 - update ~mitch
		var iscookie = readCookie('download_params');
		if (iscookie) {
			document.getElementById("00N3600000LosKl").value = 0;
		}
		// end check cookie
		
		// set referring promotion
		var refpromo = readCookie('APrefpromo');
		if (refpromo) {
			document.getElementById("00N3600000LosAC").value = refpromo;
		}
		
		var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var pass = pattern.test(document.getElementById("email").value);
		if(document.dlform.email.value=='' || !pass){
			alert('Please enter a valid email address');
			return false;
		}
        if(document.dlform.first_name.value==''){
            alert('Please enter your first name');
            return false;
        }
        if(document.dlform.last_name.value==''){
            alert('Please enter your last name');
            return false;
        }
        
		
		if(!document.getElementById('os_Windows').checked && !document.getElementById('os_MacOS').checked) {
			alert('Please select your operating system');
			return false;
		}
		var dlf = document.getElementById('dlformID');
		dlf.style.visibility = 'hidden';
	
		var email = document.dlform.emailOptOut;
		//email.checked = !email.checked;
			
		return true;
	}

	function target_submit(){
		if(validateForm2()){	
			dlform.submit();
		}
	}
</script>
<br>
<form action="<?=$sfUrl?>" method="POST" id='dlformID' name="dlform" style="">

	<input type="hidden" name="sfga" value="00D36000000oZE6"/>
	<input type="hidden" name="oid" value="00D36000000oZE6">
	<input type="hidden" name="retURL" value="<?=$retURL?>">
	<input type="hidden" name="lead_source" value="<?=$leadSrc?>">
	<input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
	<input type="hidden" name="00N3600000BOyGd" value="Download">
	<input type="hidden" id="00N3600000LosAC" name="00N3600000LosAC" value="<?=$refPromo?>" />
	<input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
	<input type="hidden" name="00N200000013fDx" value="MB" />
    <input type="hidden" value="PG" name="00N3600000Loh5K">

	<input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
	<table border="0">
		<tr>
			<td><label for="first_name">First Name:</label></td>
			<td width="207px"><input  maxlength="40" type="text" id="first_name" name="first_name" style="width:100%" ></td>
		</tr>
		<tr>
			<td><label for="last_name">Last Name:</label></td>
			<td width="207px"><input  maxlength="40" type="text" id="last_name" name="last_name" style="width:100%"></td>
		</tr>
		<tr>
			<td><label for="email">Email:</label></td>
			<td width="207px"><input  maxlength="80" type="text" id="email" name="email" style="width:100%"></td>
		</tr>
		<tr>
			<td colspan="2" class="os_select">
				<input type="radio" value="Windows" id="os_Windows" name="00N3600000Los6F">
				<label id="label_win" for="os_Windows"> <img src="/images/icons/dl_windows.png" alt="Windows"> Windows</label>
				<input type="radio" value="MacOS" id="os_MacOS" name="00N3600000Los6F" class="last">
				<label id="label_mac" for="os_MacOS"> <img src="/images/icons/dl_apple.png" alt="MacOS"> Mac</label>				
			</td>
		</tr>					
		<tr>
			<td colspan="2"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Notify me of software updates and special offers</label></td>
		</tr>
	</table>
	<div style="text-align:left; padding-left:218px;">
        <button onclick="target_submit();return false;">Download</button>
	</div>
</form>  

<?php } 
//<a href="http://www.albumprinter.com.au/target-photobooks/comp.html" target="_top" style="position: absolute; top: 300px; left: 200px; width: 30px; height: 15px; text-indent: -9999px;">here</a>
?>




<?php if($leadSrc == "PEP") { 
// -----------------------------------------------------------------------------------------------------
// PERSONAL EDITION PRINTING
// -----------------------------------------------------------------------------------------------------
$retURL .= '&multiOS';
?>
<script type="text/javascript">
	function validateForm2(){
	
		// set dl params cookie - update ~mitch
		if (document.getElementById("emailOptOut").checked == true) {
			option_mail = 1;
			option_updates = 1;
		} else {
			option_mail = 0;
			option_updates = 0;
		}
	
		name = document.getElementById("first_name").value;
		email = document.getElementById("email").value;
		name = 	name.replace("&", "");
		email = email.replace("&", "");
		name = 	name.replace(";", "");
		email = email.replace(";", "");
		name = 	name.replace(":", "");
		email = email.replace(":", "");
		name = 	name.replace("=", "");
		email = email.replace("=", "");
		name = 	name.replace("|", "");
		email = email.replace("|", "");
		name = 	name.replace("'", "");
		email = email.replace("'", "");
		name = 	name.replace('"', "");
		email = email.replace('"', "");
		email = email.replace("+", "%2B");
		document.cookie = "download_params=vendor:<? echo $dl_param_cookie_vendor; ?>|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:"+option_updates+"|store:<? echo $dl_param_cookie_store; ?> ; path=/; domain=.albumworks.com.au";
		// end set dl params
		
		// check cookie (and in doing so check JS). if its all ok, set 00N3600000LosKl // Web-to-Lead Bad JS/Cookie to 0 - update ~mitch
		var iscookie = readCookie('download_params');
		if (iscookie) {
			document.getElementById("00N3600000LosKl").value = 0;
		}
		// end check cookie
		
		// set referring promotion
		var refpromo = readCookie('APrefpromo');
		if (refpromo) {
			document.getElementById("00N3600000LosAC").value = refpromo;
		}
		
		var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var pass = pattern.test(document.getElementById("email").value);
		if(document.dlform.email.value=='' || !pass){
			alert('Please enter a valid email address');
			return false;
		}
		
		var dlf = document.getElementById('dlformID');
		dlf.style.visibility = 'hidden';
	
		var email = document.dlform.emailOptOut;
		//email.checked = !email.checked;
			
		return true;
	}

	function target_submit(){
		if(validateForm2()){	
			dlform.submit();
		}
	}
</script>
<div align="center">
	<div align="center" style="padding-left:0px">
		<h3>Download the free software</h3>
		<form action="<?=$sfUrl?>" method="POST" name="dlform" id="dlformID"  onsubmit="return validateForm()">

			<input type="hidden" name="sfga" value="00D36000000oZE6"/>
			<input type="hidden" name="oid" value="00D36000000oZE6">
			<input type="hidden" name="retURL" value="<?=$retURL?>">
			<input type="hidden" name="lead_source" value="<?=$leadSrc?>">
			<input type="hidden" name="00N20000001STqA" value="Web-to-Lead">
			<input type="hidden" name="00N3600000BOyGd" value="Download">
			<!-- Referring Promotion <input type="hidden" id="00N3600000LosAC" name="00N3600000LosAC" value="" />-->
			<!-- Store Association --><input type="hidden" id="00N20000001nmem" name="00N20000001nmem" value="<?=$dl_param_cookie_store?>" />
			<input type="hidden" name="00N200000013fDx" value="PP" />
            <input type="hidden" value="PG" name="00N3600000Loh5K">
			<!-- The following field is for Web-to-Lead Bad JS/Cookie. It will be set to 0 if the JS and Cookie set and check goes OK -->
			<input  type="hidden" id="00N3600000LosKl" name="00N3600000LosKl" value="1" />
			<!-- End Web-to-Lead Bad JS/Cookie check -->
			<!-- <?php $sendDateTime =  date('d/m/Y G:i:s a');
				echo $sendDateTime;
				?>  -->
			<input type="hidden" id="00N200000012yck" name="00N200000012yck" size="18" type="text" value="<?php echo $sendDateTime; ?>" style="display:none" />
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><label for="first_name">First Name:</label></td>
					<td width="207px"><input maxlength="40" type="text" id="first_name" name="first_name" style="width:100%" ></td>
				</tr>
				<tr>
					<td><label for="last_name">Last Name:</label></td>
					<td><input maxlength="40" type="text" id="last_name" name="last_name" style="width:100%"></td>
				</tr>
				<tr>
					<td><label for="email">Email:</label></td>
					<td><input  maxlength="80" type="text" id="email" name="email" style="width:100%"></td>
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-bottom: 5px;" ><br /><input type="image" src="images/pep_downloadicon.png" name="submit"/></td>
				</tr>
				<tr>
					<td colspan="3"><input  value="1" checked="checked" type="checkbox" id="emailOptOut" name="emailOptOut" /><label for="emailOptOut">Notify me of software updates and special offers</label></td>
				</tr>
	
			</table>
		</form>
	</div>
</div>
<?php }
?>
