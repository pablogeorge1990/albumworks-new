<?php
define('SFNAMESPACE_REST', ''); //apexrest namespace for packaged sf
define('SFNAMESPACE_SOQL', ''); //soql namespace for packaged sf

function upsertCustomer($object){
    try{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://pwg:MCgC_riGqERn2oERhEK@api.photo-products.com.au/zenpanel/api/sf/upsertCustomer');
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$object]));
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if(!in_array($status, [0, 200]))
            throw new Exception($response);

        return $response;
    }
    catch(Exception $e){
        die('ERROR: '.$e->getMessage());
    }
}

function send_email($to, $subject, $message, $add_template = true, $attachment = false, $fromemail = 'service@albumworks.com.au', $fromname = 'albumworks'){
    // add template
    if($add_template){
        $message = '<!DOCTYPE HTML><html><head></head><body style="background:white; margin:0; padding:0;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" valign="top"><table cellpadding="0" cellspacing="0" width="600" style="border-left:1px solid #8b8f8e;border-right:1px solid #8b8f8e;"><tr><td align="center" valign="top" style="background:#f2efdc"><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="background:#1e8bd4" align="left"><img src="https://www.albumworks.com.au/images/share_email_logo.jpg" width="163" height="84" alt="albumworks" /></td></tr><tr><td align="left" valign="top" style="border:1px solid #f2efdc;"><table cellpadding="0" cellspacing="0" width="600"><tr><td width="15">&nbsp;</td><td align="left" valign="top">'.$message.'</td><td width="15">&nbsp;</td></tr></table></td></tr></table><br><br><br></td><td width="15">&nbsp;</td></tr></table></td></tr></table></td></tr></table></td></tr></table></body></html>';
        $message = str_replace('<h1>','<h1 style="background:#f2efdc; margin:0; padding:5px 0; font-family:Arial; font-size:18px; font-weight:normal;">', $message);
        $message = str_replace('</h1>','</h1><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="background:white; border:1px solid #8B8F8E;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="15">&nbsp;</td><td align="left" valign="top">', $message);

        // style text
        $replacers = array(
            '<p>' => '<p style="font-family:Arial; font-size:14px;">',
        );
        foreach($replacers as $f => $t) $message = str_replace($f, $t, $message);
    }

    require_once('../lib/phpmailer/class.phpmailer.php');
    $mail = new PHPMailer(true);
    $mail->IsSMTP();        
    try {      
        $mail->SingleTo   = true;                 
        $mail->SMTPDebug  = 0; 
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'tls';
        $mail->Host       = getenv('MAIL_HOST');
        $mail->Port       = getenv('MAIL_PORT');
        $mail->Username   = getenv('MAIL_USERNAME');
        $mail->Password   = getenv('MAIL_PASSWORD');
        $mail->AddAddress($to);
        $mail->SetFrom($fromemail, $fromname);
        $mail->Subject = $subject;
        $mail->MsgHTML($message);      

        if($attachment) $mail->AddAttachment($attachment);
        $mail->Send(); 
        return false;
    } catch (phpmailerException $e) {
        return $e->errorMessage();
    }
    return 'ERROR';
}

ini_set('error_reporting', E_ALL);
error_reporting(E_ALL);

ini_set('display_errors', 1);
//ini_set('error_log', 'errors.log');
ini_set('soap.wsdl_cache_enabled', '0');

define('INCLUDE_DIR', 'soapclient');
 
require_once(INCLUDE_DIR.'/SforcePartnerClient.php');
require_once(INCLUDE_DIR.'/SforceHeaderOptions.php');

$landingdata = null;
   
if(isset($_GET['forcekk'])){
    $_POST = array(
        'email' => 'sambrent+9999991@gmail.com',
        'vendorId' => '5555',
        'firstName' => 'Sammy',
        'lastName' => 'Brent',
        'optIn' => 1,
    );
}
   
if(isset($_POST['vendorId']) && $_POST['vendorId'] != '' && isset($_POST['email']) && $_POST['email'] != '') {
	$cust['email'] = trim($_POST['email']);
    $cust['vendorId'] = $_POST['vendorId'];
	$cust['subOrganisation'] = "PG";
    $cust['currencyCode'] = "AUD";
	$cust['firstName'] = $_POST['firstName'];
	$cust['lastName'] = $_POST['lastName'];
	$cust['newDownload'] = true;
	$cust['optOut'] = (($_POST['optIn'] == 1) ? false : true);
	$cust['affiliate'] = isset($_POST['affiliate']) ? $_POST['affiliate'] : '';

    $response = upsertCustomer(['customer' => $cust]);
    
    die('OK');
		
}
elseif(isset($_COOKIE['download_params']) && (preg_match_all('/((?<=:)[\w@\-\+. \/]+)/', $_COOKIE['download_params'], $cookie_array))) {
	// make sure cookie exists and is in proper format
		
	// ALBUMWORKS:	vendor:ap|name:Tom Gangemi|email:tom.gangemi@albumprinter.com.au|om:0|ou:0
	// OW/TG: 		vendor:tg|name:fnTEST|email:tom.gangemi@albumprinter.com.au|om:1|ou:1|store:false
	// 1=vendor, 2=name, 3=email, 4=optOut, 5=emailUpdatesOnly, 6=landing_data
	
	$cookie_array = $cookie_array[0];
	array_unshift($cookie_array, "blank");		// adjust so that cookies are read by the old process (starting at key 1)
	
	$cookie_array[1] = strtolower($cookie_array[1]);			// lowercase vendor
	$cookie_array[2] = ucwords(strtolower($cookie_array[2]));	// Capitalize Name
	$cookie_array[3] =  str_replace(' ', '+', strtolower($cookie_array[3]));	//Convert space into + sign (should fix with url encode)
	
	$vendorId = false;
	switch ($cookie_array[1]) {
        case 'ae':
		case 'ap':
			$vendorId = '1031';
			break;
        case 'tg':
            $vendorId = '3255';
            break;
		case 'mb':
			$vendorId = '1955';
			break;
        case 'ow':
            $vendorId = '3183';
            break;
        case 'td':
            $vendorId = '7383';
            break;
        case 'br':
            $vendorId = '9000';
            break;
        case 'kk':
            $vendorId = '5555';
            break;
        case 'mr':
            $vendorId = '4084';
            break;
	}
	
	// make sure cookie vendor matches url get vendor
	if(isset($_GET[$cookie_array[1]])) {

		$cust['email'] = trim($cookie_array[3]);
		$cust['vendorId'] = $vendorId;
        $cust['subOrganisation'] = "PG";
        $cust['currencyCode'] = "AUD";
		$cust['firstName'] = $cookie_array[2];
		$cust['newDownload'] = true;
		$cust['optOut'] = $cookie_array[4];
		$cust['affiliate'] = '(none)';
		if($vendorId == '3183' || $vendorId == '7383') {		// Only use emailUpdatesOnly for officeworks     and teds
			$cust['emailUpdatesOnly'] = $cookie_array[5];	
		}
		if(isset($_GET['awsignup']) || isset($_GET['signuponly'])) {	// only a signup not a download
			$cust['newDownload'] = false;
		}
		if(isset($cookie_array[6]) and $cookie_array[4]) {
			$cust['landingData'] = $landingdata = $cookie_array[6];
		}

		if(isset($_COOKIE['download_params2']) && (preg_match_all('/((?<=:)[\w@\-\+. \/]+)/', $_COOKIE['download_params2'], $cookie_array2))){
			$cookie_array2 = $cookie_array2[0];
			array_unshift($cookie_array2, "blank");		// adjust so that cookies are read by the old process (starting at key 1)

			if(isset($cookie_array2[2]) && trim($cookie_array2[2]) <> 'undefined') {
				$cust['referringPromotion'] = trim($cookie_array2[2]);
			}
		}
		
		if(!isset($_GET['ltc'])) {
            $response = upsertCustomer(['customer' => $cust]);
		}

        // now set up the autoresponder
        if(!$cust['optOut'] and (!isset($_GET['referringPromotion']) or !in_array($_GET['referringPromotion'], array('WAPF','FLIGHTCENTRE')))){
            $url = 'https://api.photo-products.com.au/zenresponse/create/'.urlencode($cust['firstName']).'/'.urlencode($cust['email']).'/'.urlencode($vendorId).'/'.urlencode(isset($_GET['referringPromotion']) ? $_GET['referringPromotion'] : '');
            file_get_contents($url);
        }
		
	} else {
		// cookie vendor does not match url get vendor, so just forward to download page
		error_log($cookie_array[3].' - cookie vendor not match url get vendor');
        $cust['email'] = trim($cookie_array[3]);
	}
}
else {

}

run_preforward_tasks($cookie_array, $landingdata);
forward_to_download($cust);

###########################################################
function forward_to_download($cust) {                  
	
    if(isset($_GET['ow'])){
         $retURL = "https://www.albumworks.com.au/download/officeworks_thankyou.php";
    }
	elseif(isset($_GET['br'])){
	 	$retURL = "https://www.albumworks.com.au/download/britz_thankyou.php";
	}
    elseif(isset($_GET['kk'])){

        $retURL = "https://create.kikki-k.com/make-your-book/thank-you/";
        if(isset($_GET['signuponly']))
            $retURL = "https://create.kikki-k.com/thanks-for-your-subscription/";
    }
    elseif(isset($_GET['mr'])){
        $retURL = "https://www.albumworks.com.au/download-thankyou.html?email=";
    }
    elseif(isset($_GET['es'])){

         $retURL = "https://www.albumworks.com.au/download/edsoft_thankyou.php";
    }
	elseif(isset($_GET['cj'])){
	 	$retURL = "https://www.albumworks.com.au/download/crazyjohn_thankyou.php";
	}
    elseif(isset($_GET['tg'])){
        if(isset($_GET['multiOS'])) {
            $retURL = "https://www.albumworks.com.au/download/target_thankyou.php?multiOS";
        } else {
            $retURL = "https://www.albumworks.com.au/download/target_thankyou.php";
        }         
    }    
    elseif(isset($_GET['mb'])){
        if(isset($_GET['multiOS'])) {
            $retURL = "https://www.albumworks.com.au/download/mockingbird_thankyou.php?multiOS";
        } else {
            $retURL = "https://www.albumworks.com.au/download/mockingbird_thankyou.php";
        }         
    }    
	elseif(isset($_GET['td'])){
        if(isset($_GET['multiOS'])) {
            $retURL = "https://www.albumworks.com.au/download/teds_thankyou.php?multiOS";
        } else {
            $retURL = "https://www.albumworks.com.au/download/teds_thankyou.php";
        }         
	}	
	elseif(isset($_GET['hw'])){
	 	$retURL = "https://www.albumworks.com.au/download/harvey_world_thankyou.php";
	}
    elseif(isset($_GET['pp'])){
         $retURL = "https://www.albumworks.com.au/download/pep_thankyou.php";
    }
    elseif(isset($_GET['awsignup'])){
         $retURL = "https://www.albumworks.com.au/thank-you-for-signing-up.html";
    }
    elseif(isset($_GET['iphone'])){
         $retURL = "https://www.albumworks.com.au/mobile/thankyou-iphone.html";
    }
    elseif(isset($_GET['unbounce'])){
         if(isset($_GET['taller']))
            $retURL = "https://www.albumworks.com.au/downloadform-thankyou.php?taller&".time();
         else $retURL = "https://www.albumworks.com.au/downloadform-thankyou.php?".time();
    }
    elseif(isset($_GET['thankyou']) and $_GET['thankyou'] == 'omega'){
        $retURL = "https://www.albumworks.com.au/downloadform-thankyou.php?omega&".time();
    }
    elseif(isset($_GET['iframe'])){
         $retURL = "https://www.albumworks.com.au/downloadform-thankyou.php?iframe=".$_GET['iframe'];
    }
    elseif(isset($_GET['tponline'])){
         $retURL = "https://www.albumworks.com.au/thank-you-beta";
    }
    elseif(isset($_GET['landing-fc17'])){
         $path = 'https://api.photo-products.com.au/giftvoucher/gvhandler.php?promo=FLIGHTCENTRE&email='.urlencode($cust['email']).'&name='.urlencode($cust['firstName']);
         $response = file_get_contents($path);
         if(strpos($response, 'ERROR') !== FALSE)
             $retURL = "https://www.albumworks.com.au/landing-fc17-terms";
         else $retURL = "https://www.albumworks.com.au/landing-fc17-thanks";
    }
	else {
        if(isset($_GET['thankyou']))
            $retURL = "https://www.albumworks.com.au/download-thankyou.html?".$_GET['thankyou']."&email=";
		// else $retURL = "https://www.albumworks.com.au/download-thankyou.html?email=";
        else if (isset($_GET['referringPromotion']) && $_GET['referringPromotion'] == 'FORTYB')
            $retURL = "https://www.albumworks.com.au/download-thankyou-forty20b?email=";
        else if (isset($_GET['referringPromotion']) && $_GET['referringPromotion'] == 'FORTYD')
            $retURL = "https://www.albumworks.com.au/download-thankyou-forty20d?email=";
        else if (isset($_GET['referringPromotion']) && $_GET['referringPromotion'] == 'FORTYE')
            $retURL = "https://www.albumworks.com.au/download-thankyou-forty20e?email=";
        else
            $retURL = "https://www.albumworks.com.au/download-thankyou.html?email=";
	}
	
	// Override URL via get param - removed from march 2023
    /*
	if(isset($_GET['ret']) && !empty($_GET['ret'])) {
		$retURL = urldecode($_GET['ret']);
	}
    */
    
    if(substr($retURL, -6) == 'email='){
        $retURL .= trim($cust['email']);
    }
    
	// Do the forward
	header("Location: $retURL");
}

// Runs tasks that require pre-processing that must occur prior to forwarding the user
function run_preforward_tasks($cookie_array, $landingdata){

    if(isset($_GET['thankyou']) and $_GET['thankyou'] == 'omega'){
        $message = '';
        $to = (isset($_GET['emailonly']) ? $_GET['emailonly'] : $cookie_array[3]);
        switch($_GET['promotion']){
            case '22PDC':
                ob_start();
                ?>
                    <h1>Here's your 22% Discount Code</h1>
                    <p>Hi!</p>
                    <p>Thanks for dropping by and giving the <em>albumworks</em> Editor a try!</p>
                    <p>We hope you've had a chance to play with the <em>albumworks</em> Editor, and are even on your way to creating your first Photo Book!</p>
                    <p>Don't forget that as our special First Time Offer, you are entitled to a <b>22% discount off your first order</b>!</p>
                    <p>To take advantage of this offer, simply enter the following Voucher Code at checkout:<br><b>DISCOUNTAUG13-CJB</b></p>
                    <p>Note: This offer expires <b>9pm AEST Mon 30 Sep 2013</b>. This Voucher Code cannot be used in conjunction with any other promotion or Voucher Code.</p>
                    <p>Finally, can we help at all? If you're feeling stuck or just need to ask us a question or two, why not give us a call on 1300 553 448.</p>
                    <p>We would be happy to help to make your book the best it can be!</p>
                    <p>All the best!</p>
                    <p>The <em>albumworks</em> team</p>
                    <p>-----------------------------------------------------------<br><em>albumworks</em><br>Customer Service<br>Email: <a href="mailto:service@albumworks.com.au">service@albumworks.com.au</a></p>
                <?
                $message = ob_get_clean();
                $subject = 'Here\'s your 22% Discount Code (inside)';
                $add_template = true;
                break;
        }
        
        if($message == '') return;        
    }
    else if(isset($_GET['thankyou']) and $_GET['thankyou'] == 'racv'){
        $to = $cookie_array[3];        
        ob_start();
        ?> <!DOCTYPE html><html lang="en" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><head style='font-family:"Trebuchet MS", Helvetica, sans-serif;'></head><body style='font-family:"Trebuchet MS", Helvetica, sans-serif;font-weight:normal;font-size:16px;text-align:center;background:#e8f5fe;color:#8b949b;padding:0;margin:0;'>    <p class="link" style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:1em;font-size:12px;'>Having difficulty viewing this email? <a href="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140605/index.htm" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'>View this email as a webpage</a></p>     <table cellpadding="0" cellspacing="0" width="100%" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>        <table cellpadding="0" cellspacing="0" width="580" class="header" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="left" valign="top" width="260" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                    <a href="http://www.albumworks.com.au" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140605/01.jpg" alt="albumworks" width="260" height="56" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;'></a>                </td>                <td align="right" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                    <br style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><table cellpadding="0" cellspacing="0" width="175" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td width="80" align="left" valign="bottom" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                                <img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140523/findus.png" width="68" height="11" alt="FIND US" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;'></td>                            <td width="32" align="right" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                                <a href="http://www.facebook.com/albumworks" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140523/fb.png" width="26" height="26" alt="facebook" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;'></a>                            </td>                            <td width="32" align="right" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                                <a href="http://www.twitter.com/albumworks" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140523/twit.png" width="26" height="26" alt="twitter" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;'></a>                            </td>                            <td width="32" align="right" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                                <a href="http://www.pinterest.com/albumworks" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140523/pin.png" width="26" height="26" alt="pinterest" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;'></a>                            </td>                        </tr><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td colspan="4" align="right" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                                <p class="phone" style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:0.5em 0;'><img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140523/phone.png" width="103" height="13" alt="1300 553 448" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;'></p>                            </td>                        </tr></table></td>            </tr></table></td></tr></table><table class="light" cellpadding="0" cellspacing="0" width="100%" style='font-family:"Trebuchet MS", Helvetica, sans-serif;background:white;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>        <table cellpadding="0" cellspacing="0" width="580" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="left" valign="top" class="center" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-align:center;'>            <img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140605/02.jpg" width="580" height="66" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;margin:0 auto;'></td></tr></table></td></tr></table><table class="purple" cellpadding="0" cellspacing="0" width="100%" style='font-family:"Trebuchet MS", Helvetica, sans-serif;background:#86b6e6;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>        <table cellpadding="0" cellspacing="0" width="580" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="left" valign="top" class="center" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-align:center;'>            <img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140605/03.jpg" width="580" height="164" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;margin:0 auto;'></td></tr></table></td></tr></table><table class="aqua" cellpadding="0" cellspacing="0" width="100%" style='font-family:"Trebuchet MS", Helvetica, sans-serif;background:#e8f5fe;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>        <table cellpadding="0" cellspacing="0" width="580" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="left" valign="top" class="center" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-align:center;'>            <img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140605/04.jpg" width="580" height="475" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;margin:0 auto;'></td></tr></table></td></tr></table><table class="light" cellpadding="0" cellspacing="0" width="100%" style='font-family:"Trebuchet MS", Helvetica, sans-serif;background:white;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>        <table cellpadding="0" cellspacing="0" width="580" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" class="center" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-align:center;'>            <br style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><p style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:0 0 1em;color:#1988b5;'><a href="http://www.albumworks.com.au/photo-books" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140605/05.jpg" width="279" height="55" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;margin:0 auto;'></a></p>            <h4 style='font-family:"Trebuchet MS", Helvetica, sans-serif;font-size:16px;font-weight:normal;color:#333333;'>Simply enter the code below in the Voucher Code field in<br style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>the Shopping Cart when ordering your Photo Book:</h4>            <h3 style='font-family:"Trebuchet MS", Helvetica, sans-serif;font-size:34px;font-weight:normal;color:#333333;margin:0.5em 0;'>RACV20BOOK</h3>            <p style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:0 0 1em;color:#1988b5;'><img src="https://s3-ap-southeast-1.amazonaws.com/albumprinter-data/emails/aw_20140605/06.jpg" width="314" height="142" style='font-family:"Trebuchet MS", Helvetica, sans-serif;display:block;margin:0 auto;'></p>            <br style='font-family:"Trebuchet MS", Helvetica, sans-serif;'></td></tr></table></td></tr></table><table cellpadding="0" cellspacing="0" width="100%" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" class="footer" style='font-family:"Trebuchet MS", Helvetica, sans-serif;background:#f3f3f3;'>        <table cellpadding="0" cellspacing="0" width="580" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><tr style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>                    <br style='font-family:"Trebuchet MS", Helvetica, sans-serif;'><p style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:0 0 1em;font-size:11px;'>Not valid with any other voucher codes or offers. Only one voucher code can be redeemed per order.</p>                    <p style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:0 0 1em;font-size:11px;'>You are receiving this email because you clicked "Notify me of software updates and special<br style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>offers" when you downloaded the Albumworks editor.</p>                    <p style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:0 0 1em;font-size:11px;'>Pictureworks Group Pty Ltd trading as albumworks TM<br style='font-family:"Trebuchet MS", Helvetica, sans-serif;'>323 Williamstown Rd, Port Melbourne VIC 3207, Victoria, Australia</p>                    <p style='font-family:"Trebuchet MS", Helvetica, sans-serif;margin:0 0 1em;font-size:11px;'>For Service and Support 1300 553 448 or visit <a href="http://www.albumworks.com.au" style='font-family:"Trebuchet MS", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'>www.albumworks.com.au</a></p>                </td>            </tr></table></td></tr></table></body></html> <?
        $message = ob_get_clean();
        $subject = 'Your 20% off an albumworks Photo Book Discount';
        $add_template = false;
        
        if($message == '') return;         
    }
    
    if(@$message){
        send_email($to, $subject, $message, $add_template);
    }
}

###########################################################
function date_now(){
	// calc php5 style (with colen between hours and minutes) timezone difference (i.e. +10:00)
	$tzdiff = date("O");
	$tzdiff[3] = ':';
	$tzdiff[5] = '0';
	
	if(isset($date_now)) {
		return $date_now;
	} else {
		$date_now = date("Y-m-d\TH:i:s.000$tzdiff");
		return $date_now;
	}
}

###########################################################
function enough_time_since_prev($prev_date) {
	// make sure that a new download by a customer is not within six days of the previous, or don't count it on total
	$prev_date = split('T',$prev_date);
	$prev_date = $prev_date[0];

	$now_date = split('T',date_now());
	$now_date = $now_date[0];

	$prev_date = strtotime($prev_date);
	$now_date = strtotime($now_date);
	
	// 24 hours x 60 minutes x 60 seconds is 86400, x 6 days is 518400
	if(($now_date-$prev_date) > 518400) {
		return TRUE;
	} else {
		return FALSE;
	}
}

###########################################################
function get_refpromo() {
	if(isset($_COOKIE['APrefpromo'])) {
		return $_COOKIE['APrefpromo'];
	} else {
		return '';
	}
}

?>
