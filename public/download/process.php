<?php
    if(isset($_POST['email'])){        
        $cleanPOST = array();    
        foreach ($_POST as $key=>$value){
            $cleanPOST[stripslashes($key)] = stripslashes($value);
        }
        $ch = curl_init();    
        curl_setopt($ch, CURLOPT_URL, "https://webto.salesforce.com/servlet/servlet.WebToLead");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($cleanPOST));                
        curl_exec($ch);
        curl_close($ch);
        
        $ch = curl_init('https://www.albumworks.com.au/download/dl_process.php?mr');
        $data = array(
            'firstName' => $_POST['first_name'],
            'lastName' => $_POST['last_name'],
            'optIn' => (!isset($_POST['emailOptOut']) ? 0 : 1),
            'email' => $_POST['email'],
            'vendorId' => 4084,
        );                
        
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);       
        curl_close($ch);
        
        header("Location: https://www.albumworks.com.au/download-thankyou.html");
        die;
    }