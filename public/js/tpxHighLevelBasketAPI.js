/*
Taopix Multi Line Basket API Example
Version 2.0.1 - Thursday, 1st March 2018
For use with Taopix 2018r1
Copyright 2011 - 2018 Taopix Limited
*/

var kServerURL = 'https://orders.photo-products.com.au/'; // kServerURL needs to be set to the weburl for the corresponding brand in Taopix Control Centre.
var gBasketCount = 0;
var gBasketLoaded = false;
var gProjectListLoaded = false;
var gProjectListCount = 0;
var gOldBasketCount = 0;
var gContinueShoppingMessageEnabled = 1;

var kStr_LabelMyProjects = "en My Projects<p>cs Moje projekty<p>da Mine projekter<p>de Meine Projekte<p>es Mis Proyectos<p>fi Omat projektit<p>fr Mes projets<p>it I Miei Progetti<p>ja ãƒžã‚¤ãƒ—ãƒ­ã‚¸ã‚§ã‚¯ãƒˆ<p>ko ë‚´ í”„ë¡œì íŠ¸<p>nl Mijn projecten<p>no Mine prosjekter<p>pl Moje projekty<p>pt Meus projetos<p>ru ÐœÐ¾Ð¸ Ð¿Ñ€Ð¾ÐµÐºÑ‚Ñ‹<p>sv Mina projekt<p>th à¹‚à¸›à¸£à¹€à¸ˆà¹‡à¸„à¸‚à¸­à¸‡à¸‰à¸±à¸™<p>zh_cn æˆ‘çš„é¡¹ç›®<p>zh_tw æˆ‘çš„é …ç›®<p>mk ÐœÐ¾Ð¸ Ð¿Ñ€Ð¾ÐµÐºÑ‚Ð¸";
var kStr_LabelBasket = "en Basket<p>cs KoÅ¡Ã­k<p>da IndkÃ¸bskurv<p>de Warenkorb<p>es Carrito<p>fi Kori<p>fr Panier<p>it Carrello<p>ja ãƒã‚¹ã‚±ãƒƒãƒˆ<p>ko ìž¥ë°”êµ¬ë‹ˆ<p>nl Winkelmandje<p>no Handlekurv<p>pl Koszyk<p>pt Carrinho<p>ru ÐšÐ¾Ñ€Ð·Ð¸Ð½Ð°<p>sv Korg<p>th à¸•à¸°à¸à¸£à¹‰à¸²<p>zh_cn è´­ç‰©è½¦<p>zh_tw è³¼ç‰©è»Š<p>mk ÐšÐ¾ÑˆÐ½Ð¸Ñ‡ÐºÐ°";
var kStr_LabelRemoveFromBasket = "en Remove From Basket<p>cs Vyjmout z koÅ¡Ã­ku<p>da Fjern fra indkÃ¸bskurv<p>de aus dem Warenkorb entfernen<p>es Eliminar del Carrito<p>fi Poista korista<p>fr Supprimer du panier<p>it Rimuovi Dal Carrello<p>ja ãƒã‚¹ã‚±ãƒƒãƒˆã‹ã‚‰å‰Šé™¤ã™ã‚‹<p>ko ìž¥ë°”êµ¬ë‹ˆì—ì„œ ì‚­ì œ<p>nl Uit winkelmandje verwijderen<p>no Fjern fra handlekurv<p>pl UsuÅ„ z koszyka<p>pt Remover do carrinho<p>ru Ð£Ð´Ð°Ð»Ð¸Ñ‚ÑŒ Ð¸Ð· ÐºÐ¾Ñ€Ð·Ð¸Ð½Ñ‹<p>sv Ta bort frÃ¥n korg<p>th à¸¥à¸šà¸­à¸­à¸à¸ˆà¸²à¸à¸•à¸°à¸à¸£à¹‰à¸²<p>zh_cn ä»Žè´­ç‰©è½¦åˆ é™¤<p>zh_tw å¾žè³¼ç‰©è»Šåˆªé™¤<p>mk Ð˜Ð·Ð²Ð°Ð´Ð¸ Ð¾Ð´ ÐºÐ¾ÑˆÐ½Ð¸Ñ‡ÐºÐ°";
var kStr_ButtonEmptyBasket = "en Empty Basket<p>cs PrÃ¡zdnÃ½ koÅ¡Ã­k<p>da TÃ¸m indkÃ¸bskurv<p>de leerer Warenkorb<p>es Vaciar Carrito<p>fi Tyhjenn kori<p>fr Vider le panier<p>it Svuota Carrello<p>ja ãƒã‚¹ã‚±ãƒƒãƒˆã‚’ç©ºã«ã™ã‚‹<p>ko ë¹ˆ ìž¥ë°”êµ¬ë‹ˆ<p>nl Winkelmandje legen<p>no TÃ¸m handlekurv<p>pl Pusty koszyk<p>pt Esvaziar carrinho<p>ru ÐžÑ‡Ð¸ÑÑ‚Ð¸Ñ‚ÑŒ ÐºÐ¾Ñ€Ð·Ð¸Ð½Ñƒ<p>sv TÃ¶m korg<p>th à¸¥à¹‰à¸²à¸‡à¸•à¸°à¸à¸£à¹‰à¸²<p>zh_cn æ¸…ç©ºè´­ç‰©è½¦<p>zh_tw æ¸…ç©ºè³¼ç‰©è»Š<p>mk ÐŸÑ€Ð°Ð·Ð½Ð° ÐºÐ¾ÑˆÐ½Ð¸Ñ‡ÐºÐ°";
var kStr_ButtonCheckout = "en Checkout<p>cs Pokladna<p>da Til kassen<p>de Abmelden<p>es Finalizar Compra<p>fi Kassa<p>fr Paiement<p>it Procedi<p>ja ãƒã‚§ãƒƒã‚¯ã‚¢ã‚¦ãƒˆ<p>ko ì ê²€<p>nl Afrekenen<p>no GÃ¥ til kassen<p>pl ZamÃ³w<p>pt Finalizar pedido<p>ru ÐžÑ„Ð¾Ñ€Ð¼Ð¸Ñ‚ÑŒ Ð·Ð°ÐºÐ°Ð·<p>sv GÃ¥ till kassan<p>th à¹€à¸Šà¹‡à¸„à¹€à¸­à¹‰à¸²à¸—à¹Œ<p>zh_cn ç»“è´¦<p>zh_tw çµè³¬<p>mk ÐžÐ´Ñ˜Ð°Ð²Ð°";
var kStr_LabelSignIn = "en Sign In<p>cs PÅ™ihlÃ¡sit<p>da Log pÃ¥<p>de Anmelden<p>es Registrarse<p>fi Kirjaudu<p>fr Connectez-vous<p>it Accedere<p>ja ã‚µã‚¤ãƒ³ã‚¤ãƒ³<p>ko ë¡œê·¸ì¸<p>nl Aanmelden<p>no Logg inn<p>pl Zaloguj<p>pt Entre<p>ru Ð’Ñ…Ð¾Ð´<p>sv Logga in<p>th à¹€à¸‚à¹‰à¸²à¸ªà¸¹à¹ˆà¸£à¸°à¸šà¸š<p>zh_cn ç™»å½•<p>zh_tw ç™»å…¥";
var kStr_LabelLogout = "en Log Out<p>cs OdhlÃ¡sit<p>da Log af<p>de Abmelden<p>es Salir<p>fi Kirjaudu ulos<p>fr DÃ©connexion<p>it Log Out<p>ja ãƒ­ã‚°ã‚¢ã‚¦ãƒˆ<p>ko ë¡œê·¸ ì•„ì›ƒ<p>nl Uitloggen<p>no Logg ut<p>pl Wyloguj<p>pt Logout<p>ru Ð’Ñ‹Ð¹Ñ‚Ð¸<p>sv Logga ut<p>th à¸­à¸­à¸à¸ˆà¸²à¸à¸£à¸°à¸šà¸š<p>zh_cn é€€å‡º<p>zh_tw ç™»å‡º";
var kStr_LabelEdit = "en Edit<p>cs Editovat<p>da Rediger<p>de Bearbeiten<p>es Editar<p>fi Muokkaa<p>fr Editer<p>it Modificare<p>ja ç·¨é›†<p>ko íŽ¸ì§‘<p>nl Bewerken<p>no Endre<p>pl Edytuj<p>pt Editar<p>ru Ð ÐµÐ´Ð°ÐºÑ‚Ð¸Ñ€Ð¾Ð²Ð°Ñ‚ÑŒ<p>sv Redigera<p>th à¹à¸à¹‰à¹„à¸‚<p>zh_cn ç¼–è¾‘<p>zh_tw ç·¨è¼¯";
var kStr_LabelRename = "en Rename<p>cs PÅ™ejmenovat<p>da OmdÃ¸b<p>de Umbenennen<p>es Renombrar<p>fi NimeÃ¤ uudelleen<p>fr Renommer<p>it Rinomina<p>ja ãƒªãƒãƒ¼ãƒ <p>ko ì´ë¦„ ë°”ê¾¸ê¸°<p>nl Hernoem project<p>no Rename<p>pl ZmieÅ„ nazwÄ™<p>pt Renomear<p>ru ÐŸÐµÑ€ÐµÐ¸Ð¼ÐµÐ½Ð¾Ð²Ð°Ñ‚ÑŒ<p>sv Byt namn<p>th à¹€à¸›à¸¥à¸µà¹ˆà¸¢à¸™à¸Šà¸·à¹ˆà¸­<p>zh_cn é‡å‘½å<p>zh_tw é‡å‘½å";
var kStr_LabelDuplicate = "en Duplicate<p>cs Duplikovat<p>da Dupliker<p>de Duplizieren<p>es Duplicar<p>fi Monista<p>fr Dupliquer<p>it Duplica<p>ja è¤‡è£½<p>ko ì¤‘ë³µ<p>nl Dupliceer project<p>no Duplicate<p>pl Powiel<p>pt Duplicar<p>ru Ð”ÑƒÐ±Ð»Ð¸Ñ€Ð¾Ð²Ð°Ñ‚ÑŒ<p>sv Kopiera<p>th à¸—à¸³à¸‹à¹‰à¸³<p>zh_cn å¤åˆ¶<p>zh_tw è¤‡è£½";
var kStr_LabelDelete = "en Delete<p>cs Smazat<p>da Slet<p>de LÃ¶schen<p>es Borrar<p>fi Poista<p>fr Supprimer<p>it Cancella<p>ja å‰Šé™¤<p>ko ì‚­ì œ<p>nl Verwijder project<p>no Delete<p>pl UsuÅ„<p>pt Deletar<p>ru Ð£Ð´Ð°Ð»Ð¸Ñ‚ÑŒ<p>sv Ta bort<p>th à¸¥à¸š<p>zh_cn åˆ é™¤<p>zh_tw åˆªé™¤";
var kStr_LabelLayoutName = "en Layout Name<p>cs NÃ¡zev layoutu<p>da Layoutnavn<p>de LayoutName<p>es Nombre de plantilla<p>fi Asettelun nimi<p>fr Nom de mise en page<p>it Nome Layout<p>ja ãƒ¬ã‚¤ã‚¢ã‚¦ãƒˆå<p>ko ë ˆì´ì•„ì›ƒëª…<p>nl Layout naam<p>no Layout-navn<p>pl Nazwa szablonu<p>pt Nome do layout<p>ru Ð˜Ð¼Ñ Ð¼Ð°ÐºÐµÑ‚Ð°<p>sv Layoutnamn<p>th à¸Šà¸·à¹ˆà¸­à¹€à¸¥à¸¢à¹Œà¹€à¸­à¸²à¸—à¹Œ<p>zh_cn æ¨¡æ¿åå­—<p>zh_tw æ¨¡æ¿åå­—";
var kStr_LabelMyAccount = "en My Account<p>cs MÅ¯j ÃºÄet<p>da Min konto<p>de Mein Konto<p>es Mi Cuenta<p>fi Oma tili<p>fr Mon compte<p>it Il Mio Account<p>ja ãƒžã‚¤ã‚¢ã‚«ã‚¦ãƒ³ãƒˆ<p>ko ë§ˆì´ íŽ˜ì´ì§€<p>nl Mijn account<p>no Min konto<p>pl Moje konto<p>pt Minha Conta<p>ru ÐœÐ¾Ñ ÑƒÑ‡ÐµÑ‚Ð½Ð°Ñ Ð·Ð°Ð¿Ð¸ÑÑŒ<p>sv Mitt konto<p>th à¸šà¸±à¸à¸Šà¸µà¸‚à¸­à¸‡à¸‰à¸±à¸™<p>zh_cn æˆ‘çš„è´¦å·<p>zh_tw æˆ‘çš„å¸³æˆ¶";
var kStr_LabelRegister = "en Register<p>cs Registrovat<p>da Registrer<p>de Registrieren<p>es Registro<p>fi RekisterÃ¶idy<p>fr S'enregistrer<p>it Registrazione<p>ja ç™»éŒ²<p>ko íšŒì›ê°€ìž…<p>nl Registreer<p>no Registrer deg<p>pl Rejestracja<p>pt Registrar<p>ru Ð—Ð°Ñ€ÐµÐ³Ð¸ÑÑ‚Ñ€Ð¸Ñ€Ð¾Ð²Ð°Ñ‚ÑŒÑÑ<p>sv Registrera<p>th à¸¥à¸‡à¸—à¸°à¹€à¸šà¸µà¸¢à¸™<p>zh_cn æ³¨å†Œ<p>zh_tw è¨»å†Š";
var kStr_LabelRenameProject = "en Rename Project<p>cs PÅ™ejmenovat projekt<p>da OmdÃ¸b projekt<p>de Projekt umbenennen<p>es Cambiar el nombre de Proyecto<p>fi NimeÃ¤ projekti uudelleen<p>fr Renommer le projet<p>it Rinomina il Progetto<p>ja ãƒ—ãƒ­ã‚¸ã‚§ã‚¯ãƒˆåã‚’ãƒªãƒãƒ¼ãƒ <p>ko ì´ë¦„ ë°”ê¾¸ê¸°<p>nl Project hernoemen<p>no Gi nytt navn til prosjekt<p>pl Zmiana nazwy projektu<p>pt Renomear projeto<p>ru ÐŸÐµÑ€ÐµÐ¸Ð¼ÐµÐ½Ð¾Ð²Ð°Ñ‚ÑŒ Ð¿Ñ€Ð¾ÐµÐºÑ‚<p>sv Byt namn pÃ¥ projekt<p>th à¹€à¸›à¸¥à¸µà¹ˆà¸¢à¸™à¸Šà¸·à¹ˆà¸­à¹‚à¸›à¸£à¹€à¸ˆà¹‡à¸„<p>zh_cn é‡å‘½åä½œå“<p>zh_tw é‡å‘½åä½œå“";
var kStr_LabelProjectName = "en Project Name<p>cs NÃ¡zev projektu<p>da Projektnavn<p>de Projektname<p>es Nombre del Proyecto<p>fi Projektin nimi<p>fr Nom du projet<p>it Nome Del Progetto<p>ja ãƒ—ãƒ­ã‚¸ã‚§ã‚¯ãƒˆå<p>ko í”„ë¡œì íŠ¸ ì´ë¦„<p>nl Projectnaam<p>no Prosjektnavn<p>pl Nazwa projektu<p>pt Nome do projeto<p>ru ÐÐ°Ð·Ð²Ð°Ð½Ð¸Ðµ Ð¿Ñ€Ð¾ÐµÐºÑ‚Ð°<p>sv Projektnamn<p>th à¸Šà¸·à¹ˆà¸­à¸‡à¸²à¸™<p>zh_cn ä½œå“åç§°<p>zh_tw ä½œå“åç¨±";
var kStr_ButtonDoNotContinue = "en Do Not Continue<p>cs NepokraÄovat<p>da FortsÃ¦t ikke<p>de Nicht Weiter<p>es No Continuar<p>fi Ã„lÃ¤ jatka<p>fr Abandonner<p>it Interrompere<p>ja ç¶šã‘ãªã„<p>ko ì§„í–‰ ì¤‘ë‹¨<p>nl Ga niet verder<p>no Ikke fortsett<p>pl Nie kontynuuj<p>pt NÃ£o continuar<p>ru ÐÐµ Ð¿Ñ€Ð¾Ð´Ð¾Ð»Ð¶Ð°Ñ‚ÑŒ<p>sv FortsÃ¤tt inte<p>th à¸«à¹‰à¸²à¸¡à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£à¸•à¹ˆà¸­<p>zh_cn ä¸ç»§ç»­<p>zh_tw ä¸ç¹¼çºŒ";
var kStr_ButtonContinue = "en Continue<p>cs PokraÄovat<p>da FortsÃ¦t<p>de Weiter<p>es Continuar<p>fi Jatka<p>fr Continuer<p>it Continuare<p>ja ç¶šã‘ã‚‹<p>ko ê³„ì† ì§„í–‰<p>nl Ga verder<p>no Fortsett<p>pl Kontynuuj<p>pt Continuar<p>ru ÐŸÑ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸Ñ‚ÑŒ<p>sv FortsÃ¤tt<p>th à¸•à¹ˆà¸­à¹„à¸›<p>zh_cn ä¸‹ä¸€æ­¥<p>zh_tw ç¹¼çºŒ";
var kStr_LabelPleaseConfirm = "en Please Confirm!<p>cs PotvrÄte prosÃ­m!<p>da BekrÃ¦ft!<p>de Bitte bestÃ¤tigen<p>es Â¡Confirme por favor!<p>fi Ole hyvÃ¤ ja vahvista!<p>fr Merci de confirmer !<p>it Confermare!<p>ja ç¢ºèªã—ã¦ãã ã•ã„!<p>ko í™•ì¸í•´ì£¼ì„¸ìš”!<p>nl Bevestig deze actie<p>no Please Confirm!<p>pl Potwierdzenie usuniÄ™cia<p>pt Favor confirmar!<p>ru ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð° Ð¿Ð¾Ð´Ñ‚Ð²ÐµÑ€Ð´Ð¸Ñ‚Ðµ!<p>sv BekrÃ¤fta.<p>th à¸à¸£à¸¸à¸“à¸²à¸¢à¸·à¸™à¸¢à¸±à¸™!<p>zh_cn è¯·ç¡®è®¤<p>zh_tw è«‹ç¢ºèª";
var kStr_ButtonYes = "en Yes<p>cs Ano<p>da Ja<p>de Ja<p>es SÃ­<p>fi KyllÃ¤<p>fr Oui<p>it Si<p>ja ã¯ã„<p>ko ì˜ˆ<p>nl Ja<p>no Ja<p>pl Tak<p>pt Sim<p>ru Ð”Ð°<p>sv Ja<p>th à¹ƒà¸Šà¹ˆ<p>zh_cn æ˜¯<p>zh_tw æ˜¯";
var kStr_ButtonNo = "en No<p>cs Ne<p>da Nej<p>de Nein<p>es No<p>fi Ei<p>fr Non<p>it No<p>ja ã„ã„ãˆ<p>ko ì•„ë‹ˆì˜¤<p>nl Nee<p>no Nei<p>pl Nie<p>pt NÃ£o<p>ru ÐÐµÑ‚<p>sv Nej<p>th à¹„à¸¡à¹ˆà¹ƒà¸Šà¹ˆ<p>zh_cn å¦<p>zh_tw å¦";
var kStr_ButtonCancel = "en Cancel<p>cs Storno<p>da Anuller<p>de Abbrechen<p>es Cancelar<p>fi Peruuta<p>fr Annuler<p>it Annullare<p>ja ã‚­ãƒ£ãƒ³ã‚»ãƒ«<p>ko ì·¨ì†Œ<p>nl Annuleren<p>no Avbryt<p>pl Anuluj<p>pt Cancelar<p>ru ÐžÑ‚Ð¼ÐµÐ½Ð°<p>sv Avbryt<p>th à¸¢à¸à¹€à¸¥à¸´à¸<p>zh_cn å–æ¶ˆ<p>zh_tw å–æ¶ˆ";
var kStr_ButtonOK = "en OK<p>cs OK<p>da OK<p>de OK<p>es OK<p>fi OK<p>fr OK<p>it OK<p>ja OK<p>ko ì¢‹ìŠµë‹ˆë‹¤<p>nl OK<p>no OK<p>pl OK<p>pt OK<p>ru ÐžÐºÐµÐ¹<p>sv OK<p>th à¸•à¸à¸¥à¸‡<p>zh_cn ç¡®å®š<p>zh_tw ç¢ºå®š";
var kStr_MessageDeleteProjectConfirmation = "en Are you sure you want to delete ^0?<p>cs SkuteÄnÄ› chcete odstranit ^0?<p>da Er du sikker pÃ¥, at du vil slette ^0?<p>de Sind Sie sicher, dass Sie dieses Projekt lÃ¶schen wollen: ^0?<p>es Â¿Seguro que quieres eliminar ^0?<p>fi Haluatko varmasti poistaa seuraavan: ^0?<p>fr Etes-vous sÃ»r de vouloir supprimer ^0 ?<p>it Vuoi veramente cancellare ^0?<p>ja ^0ã‚’æœ¬å½“ã«å‰Šé™¤ã—ã¾ã™ã‹ï¼Ÿ<p>ko ì •ë§ ^0 í”„ë¡œì íŠ¸ë¥¼ ì‚­ì œí•˜ì‹œê² ìŠµë‹ˆê¹Œ?<p>nl Weet je zeker dat je ^0 wilt verwijderen?<p>no Are you sure you want to delete ^0?<p>pl JesteÅ› pewien, Å¼e chcesz usunÄ…Ä‡ projekt: ^0?<p>pt EstÃ¡ seguro que deseja deletar ^0?<p>ru Ð’Ñ‹ ÑƒÐ²ÐµÑ€ÐµÐ½Ñ‹, Ñ‡Ñ‚Ð¾ Ñ…Ð¾Ñ‚Ð¸Ñ‚Ðµ ÑƒÐ´Ð°Ð»Ð¸Ñ‚ÑŒ ^0?<p>sv Ã„r du sÃ¤ker pÃ¥ att du vill ta bort ^0?<p>th à¸„à¸¸à¸“à¸•à¹‰à¸­à¸‡à¸à¸²à¸£à¸¥à¸šà¹‚à¸›à¸£à¹€à¸ˆà¹‡à¸„^0?<p>zh_cn æ‚¨ç¡®è®¤æ‚¨è¦åˆ é™¤^0å—?<p>zh_tw æ‚¨ç¢ºå®šæ‚¨è¦åˆªé™¤å—Ž^0";
var kStr_MessageProjectOpenInShoppingCart = "en You have recently tried to order this project. It is recommended that you do not continue without first checking the order within the previously opened browser window.<br><br>Are you sure you wish to continue?<p>cs NedÃ¡vno jste se pokusil otevÅ™Ã­m tento projekt. Je doporuÄeno nepokraÄovat bez pÅ™edchozÃ­ kontroly objednÃ¡vky v oknÄ› prohlÃ­Å¾eÄe, kterÃ© bylo pÅ™edtÃ­m otevÅ™eno.<br><br>UrÄitÄ› chcete pokraÄovat?<p>da Du har for nylig forsÃ¸gt at bestille dette projekt. Det anbefales, at du ikke fortsÃ¦tter, fÃ¸r du har kontrolleret ordren i det tidligere Ã¥bnede browservindue.<br><br>Er du sikker pÃ¥, at du vil fortsÃ¦tte?<p>de Sie haben bereits versucht, dieses Projekt zu bestellen. Bevor Sie fortfahren, empfehlen wir Ihnen die geÃ¶ffneten Fenster in Ihrem Browser zu prÃ¼fen.<br><br>MÃ¶chten Sie wirklich fortfahren?<p>es Usted ha tratado recientemente de ordenar este proyecto. Se recomienda que usted no continÃºe sin revisar primero la orden en la ventana del navegador abierta previamente. <br><br>Â¿Seguro que desea continuar?<p>fi Olet Ã¤skettÃ¤in yrittÃ¤nyt tilata tÃ¤mÃ¤n projektin. Ennen kuin jatkat, suosittelemme, ettÃ¤ tarkistat tilauksen aiemmin avatussa selainikkunassa.<br><br>Haluatko varmasti jatkaa?<p>fr Vous avez rÃ©cemment essayÃ© de passer commande de ce projet. Il est recommandÃ© de ne pas continuer avant d'avoir d'abord vÃ©rifiÃ© la commande dans la fenÃªtre de navigation ouverte prÃ©cÃ©demment.<br><br>Etes-vous sÃ»r de vouloir continuer ?<p>it Hai recentemente cercato di ordinare questo progetto. Si raccomanda di non continuare senza prima aver verificato l'ordine nella finestra del browser aperta precedentemente.<br><br>Vuoi veramente continuare?<p>ã“ã®ãƒ—ãƒ­ã‚¸ã‚§ã‚¯ãƒˆã‚’ã‚ªãƒ¼ãƒ€ãƒ¼ã—ã‚ˆã†ã¨ã—ã¦ã„ã¾ã™ã€‚ã‚ªãƒ¼ãƒ€ãƒ¼ã™ã‚‹å‰ã«ã€ä»¥å‰é–‹ã„ã¦ã„ãŸãƒ–ãƒ©ã‚¦ã‚¶ã‚¦ã‚£ãƒ³ãƒ‰ã‚¦å†…ã§ã®ã‚ªãƒ¼ãƒ€ãƒ¼ã‚’ç¢ºèªã™ã‚‹ã“ã¨ã‚’ãŠå‹§ã‚ã—ã¾ã™ã€‚<br><br>ç¶šã‘ã¦ã‚ˆã‚ã—ã„ã§ã™ã‹ï¼Ÿ<p>ko ìž¥ë°”êµ¬ë‹ˆì—ì„œ í”„ë¡œì íŠ¸ ì—´ê¸°<p>mk Ð’Ð¸Ðµ Ð½ÐµÐ¾Ð´Ð°Ð¼Ð½Ð° ÑÐµ Ð¾Ð±Ð¸Ð´Ð¾Ð²Ñ‚Ðµ Ð´Ð° Ð³Ð¾ Ð½Ð°Ñ€Ð°Ñ‡Ð°Ñ‚Ðµ Ð¾Ð²Ð¾Ñ˜ Ð¿Ñ€Ð¾ÐµÐºÑ‚. ÐÐ¸Ðµ Ð¿Ñ€ÐµÐ¿Ð¾Ñ€Ð°Ñ‡ÑƒÐ²Ð°Ð¼Ðµ Ð´Ð° Ð½Ðµ Ð¿Ñ€Ð¾Ð´Ð¾Ð»Ð¶ÑƒÐ²Ð°Ñ‚Ðµ ÑÐ¾ Ð¿Ð¾Ñ€Ð°Ñ‡ÐºÐ°Ñ‚Ð° Ð¿Ñ€ÐµÐ´ Ð¿Ñ€Ð¾Ð²ÐµÑ€ÐºÐ° Ð½Ð° Ð½Ð°Ñ€Ð°Ñ‡ÐºÐ°Ñ‚Ð° Ð²Ð¾ Ð¿Ñ€ÐµÑ‚Ñ…Ð¾Ð´Ð½Ð¾ Ð¾Ñ‚Ð²Ð¾Ñ€ÐµÐ½Ð¸Ð¾Ñ‚ Ð¿Ñ€Ð¾Ð·Ð¾Ñ€ÐµÑ†.<br><br>Ð°Ð»Ð¸ ÑÑ‚Ðµ ÑÐ¸Ð³ÑƒÑ€Ð½Ð¸ Ð´ÐµÐºÐ° ÑÐ°ÐºÐ°Ñ‚Ðµ Ð´Ð° Ð¿Ñ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸Ñ‚Ðµ??<p>nl Je hebt onlangs geprobeerd dit project te bestellen. We raden aan niet verder te gaan en eerst het project te bekijken in het eerder geopende scherm. <br><br> Weet je zeker dat je door wilt gaan?<p>no Du har nylig forsÃ¸kt Ã¥ bestille dette prosjektet. Vi anbefaler at du ikke fortsetter uten at du fÃ¸rst kontrollerer bestillingen i nettleservinduet som du Ã¥pnet tidligere.<br><br>Er du sikker pÃ¥ at du vil fortsette?<p>pl Niedawno prÃ³bowano zamÃ³wiÄ‡Â ten projekt. Zaleca siÄ™, aby nie kontynuowaÄ‡ do momentu sprawdzenia projektu w drugim oknie.<br><br>Czy na pewno chcesz kontynuowaÄ‡?<p>pt VocÃª tentou finalizar esse pedido recentemente. Recomenda-se nÃ£o continuar sem verificar o pedido na janela anterior aberta no browser.<br><br>Deseja continuar?<p>ru Ð’Ñ‹ Ð½ÐµÐ´Ð°Ð²Ð½Ð¾ Ð¿Ñ‹Ñ‚Ð°Ð»Ð¸ÑÑŒ Ð¾Ñ„Ð¾Ñ€Ð¼Ð¸Ñ‚ÑŒ ÑÑ‚Ð¾Ñ‚ Ð·Ð°ÐºÐ°Ð·. Ð ÐµÐºÐ¾Ð¼ÐµÐ½Ð´ÑƒÐµÑ‚ÑÑ Ð½Ðµ Ð¿Ñ€Ð¾Ð´Ð¾Ð»Ð¶Ð°Ñ‚ÑŒ, Ð½Ðµ Ð¿Ñ€Ð¾Ð²ÐµÑ€Ð¸Ð² ÑÑ‚Ð°Ñ‚ÑƒÑ Ð·Ð°ÐºÐ°Ð·Ð° Ð² Ñ€Ð°Ð½ÐµÐµ Ð¾Ñ‚ÐºÑ€Ñ‹Ñ‚Ð¾Ð¼ Ð¾ÐºÐ½Ðµ Ð±Ñ€Ð°ÑƒÐ·ÐµÑ€Ð°.<br><br>Ð’Ñ‹ ÑƒÐ²ÐµÑ€ÐµÐ½Ñ‹, Ñ‡Ñ‚Ð¾ Ñ…Ð¾Ñ‚Ð¸Ñ‚Ðµ Ð¿Ñ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸Ñ‚ÑŒ?<p>sv Du har nyligen fÃ¶rsÃ¶kt bestÃ¤lla det hÃ¤r projektet. Vi rekommenderar att du inte fortsÃ¤tter om du inte fÃ¶rst kontrollerar bestÃ¤llningen i det tidigare Ã¶ppnade webblÃ¤sarfÃ¶nstret.<br><br>Ã„r du sÃ¤ker pÃ¥ att du vill fortsÃ¤tta?<p>th à¸„à¸¸à¸“à¹„à¸”à¹‰à¸—à¸³à¸à¸²à¸£à¸ªà¸±à¹ˆà¸‡à¸‹à¸·à¹‰à¸­à¹‚à¸›à¸£à¹€à¸ˆà¹‡à¸„à¸™à¸µà¹‰à¹„à¸› à¹à¸™à¸°à¸™à¸³à¸§à¹ˆà¸²à¹„à¸¡à¹ˆà¸„à¸§à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£à¸•à¹ˆà¸­à¸«à¸²à¸à¸„à¸¸à¸“à¸¢à¸±à¸‡à¹„à¸¡à¹ˆà¹„à¸”à¹‰à¸•à¸£à¸§à¸ˆà¸ªà¸­à¸šà¸à¸²à¸£à¸ªà¸±à¹ˆà¸‡à¸‹à¸·à¹‰à¸­à¹ƒà¸™à¸«à¸™à¹‰à¸²à¸•à¹ˆà¸²à¸‡à¸­à¸·à¹ˆà¸™à¸—à¸µà¹ˆà¸„à¸¸à¸“à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£à¹„à¸§à¹‰.<br><br>à¸•à¹‰à¸­à¸‡à¸à¸²à¸£à¸”à¸³à¹€à¸™à¸´à¸™à¸à¸²à¸£à¸•à¹ˆà¸­à¸«à¸£à¸·à¸­à¹„à¸¡à¹ˆ?<p>zh_cn ä½ æœ€è¿‘å°è¯•è®¢è´­æ­¤é¡¹ç›®ã€‚é¦–å…ˆå»ºè®®æ‚¨ä¸è¦ç»§ç»­ï¼Œå…ˆæ£€æŸ¥æ‰“å¼€æµè§ˆå™¨çª—å£ä¸­çš„è®¢è´­ã€‚<br><br>ç‚¹å‡»çœ‹ä½ ï¼Œç¡®å®šè¦ç»§ç»­å—ï¼Ÿ<p>zh_tw ä½ æœ€è¿‘å˜—è©¦è¨‚è³¼è©²é …ç›®ã€‚é¦–å…ˆå»ºè­°æ‚¨ä¸è¦ç¹¼çºŒï¼Œå…ˆæª¢æŸ¥æ‰“é–‹ç€è¦½å™¨çª—å£ä¸­çš„è¨‚è³¼ã€‚<br><br>é»žæ“ŠæŸ¥çœ‹ï¼Œä½ ç¢ºå®šè¦ç¹¼çºŒå—Žï¼Ÿ"
var kStr_MessageProjectAddedToBasket = "en Project added to basket<p>cs Projekt byl pÅ™idÃ¡n do koÅ¡Ã­ku<p>de Projekt wurde zum Warenkorb hinzugefÃ¼gt<p>ja ãƒã‚¹ã‚±ãƒƒãƒˆã«è¿½åŠ ã•ã‚ŒãŸãƒ—ãƒ­ã‚¸ã‚§ã‚¯ãƒˆ<p>ru ÐŸÑ€Ð¾ÐµÐºÑ‚ Ð´Ð¾Ð±Ð°Ð²Ð»ÐµÐ½ Ð² ÐºÐ¾Ñ€Ð·Ð¸Ð½Ñƒ<p>zh_cn æŠŠä½œå“æ·»åŠ åˆ°è´­ç‰©ç¯®<p>zh_tw æŠŠé …ç›®æ·»åŠ åˆ°è³¼ç‰©ç±ƒ<p>mk ÐŸÑ€Ð¾ÐµÐºÑ‚Ð¾Ñ‚ Ðµ Ð´Ð¾Ð´Ð°Ð´ÐµÐ½ Ð²Ð¾ ÐºÐ¾ÑˆÐ½Ð¸Ñ‡ÐºÐ°Ñ‚Ð°<p>nl Project toegevoegd aan mandje<p>es Proyecto agregado a la cesta<p>pt Projeto enviado para o carrinho<p>it Progetto aggiunto al carrello<p>sv Projekt lagt i varukorgen<p>pl Projetk dodany do koszyka<p>ko í”„ë¡œì íŠ¸ê°€ ìž¥ë°”êµ¬ë‹ˆì— ì¶”ê°€ë˜ì—ˆìŠµë‹ˆë‹¤.<p>fr Projet ajoutÃ© au panier";
var kStr_ButtonCheckoutNow = "en Checkout Now<p>cs Objednat nynÃ­<p>de Jetzt Abmelden<p>ja ä»Šãƒã‚§ãƒƒã‚¯ã‚¢ã‚¦ãƒˆ<p>ru ÐžÑ„Ð¾Ñ€Ð¼Ð¸Ñ‚ÑŒ Ð·Ð°ÐºÐ°Ð· cÐµÐ¹Ñ‡Ð°Ñ<p>zh_cn çŽ°åœ¨ç»“è´¦<p>zh_tw ç¾åœ¨åŽ»ä»˜æ¬¾<p>mk ÐžÐ´Ñ˜Ð°Ð²Ð° ÑÐµÐ³Ð°<p>nl Naar de kassa<p>es Pagar ahora<p>pt Finalizar pedido<p>it Concludi Adesso<p>sv Checka ut nu<p>pl SprawdÅº teraz<p>ko ì§€ê¸ˆ ê²°ì œí•˜ê¸°<p>fr Finaliser la commande";
var kStr_ButtonContinueShopping = "en Continue Shopping<p>cs PokraÄovat v nÃ¡kupu<p>de Weiter einkaufen<p>ja ã‚·ãƒ§ãƒƒãƒ”ãƒ³ã‚°ã‚’ç¶šã‘ã‚‹<p>ru ÐŸÑ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸Ñ‚ÑŒ Ð¿Ð¾ÐºÑƒÐ¿ÐºÐ¸<p>zh_cn ç»§ç»­è´­ç‰©<p>zh_tw ç¹¼çºŒè³¼ç‰©<p>mk ÐŸÑ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸ ÑÐ¾ ÐºÑƒÐ¿ÑƒÐ²Ð°ÑšÐµ<p>nl Verder winkelen<p>es Seguir comprando<p>pt Continuar comprando<p>it Prosegui con gli Acquisti<p>sv FortsÃ¤tt handla<p>pl Kontynuuj zakupy<p>ko ì‡¼í•‘ ê³„ì†í•˜ê¸°<p>fr Continuer vos achats";
var kSSOOff = 0;
var kSSOSignIn = 1;
var kSSOAutomatic = 2;
var kBasketInternalError = 33;
var kBasketExpired = 34;
var kBasketSessionExpired = 35;

var gSSOEnabled = kSSOOff;
var gSSOToken = '';

function tpxGetBrowserLocale()
{
    // determine the browser locale either from the browser
    var browserLanguage = 'en';

    if (navigator.userLanguage)
    {
        browserLanguage = navigator.userLanguage;
    }
    else if (navigator.language)
    {
        browserLanguage = navigator.language;
    }

    // get the first (main) browser language
    var browserLanguageArray = browserLanguage.split(",");
    browserLanguage = browserLanguageArray[0].toLowerCase();

    switch (browserLanguage)
    {
        case 'zh-tw':
        case 'zh-cn':
        {
            // don't do anything if the language is set to Chinese as we need to detect
            // if it is Chinese Traditional or Chinese Simplified

            break;
        }
        default:
        {
            // if the language code is longer than 2 characters (i.e en-GB) then
            // we only need the first 2 characters (en) for the language code

            if (browserLanguage.length > 2)
            {
                browserLanguage = browserLanguage.substring(0, 2);
            }
            break;
        }
    }

    return browserLanguage.replace('-', '_');
}

function tpxGetUrlVar(key)
{
    var result = new RegExp(key + "=([^&]*)", "i").exec(window.location.search);
    return result && unescape(result[1]) || "";
}

function tpxGetLocaleString(pLocalizedString)
{
    // return the correct language string
    var result = '';
    var firstAvailable = '';
    var defaultLanguage = '';

    var locale = tpxGetBrowserLocale();

    var locale2 = locale.substring(0, 2);

    var localizedStringList = pLocalizedString.split('<p>');
    var localizedCount = localizedStringList.length;

    for (var i = 0; i < localizedCount; i++)
    {
        // split each language item into its code and name
        var charPos = localizedStringList[i].indexOf(' ');
        var localizedItemCode = localizedStringList[i].substring(0, charPos);
        var localizedItemCode2 = localizedItemCode.substring(0, 2);
        var localizedItemString = localizedStringList[i].substring(charPos + 1);

        if ((firstAvailable == '') && (localizedItemString != ''))
        {
            firstAvailable = localizedItemString;
        }

        if (localizedItemCode == 'en')
        {
            defaultLanguage = localizedItemString;
        }

        // check for english as a last resort
        // if not chinese, attempt to match based on the first two characters to handle regions
        // if we get an exact match on the full language code or the first two characters of the input code we always use this one
        if ((result == '') && (localizedItemCode2 == 'en'))
        {
            result = localizedItemString;
        }
        else if ((localizedItemCode2 == locale2) && (locale2 != 'zh'))
        {
            result = localizedItemString;
        }
        else if (localizedItemCode == locale)
        {
            result = localizedItemString;
            break;
        }
        else if (localizedItemCode == locale2)
        {
            result = localizedItemString;
            break;
        }
    }

    if (result == '')
    {
        if (defaultLanguage != '')
        {
            result = defaultLanguage;
        }
        else
        {
            result = firstAvailable;
        }
    }

    return result;
}

function tpxGetXMLHTTP()
{
    var xhttp;

    if (window.XMLHttpRequest)
    {
        xhttp = new XMLHttpRequest();
    }
    else
    {
        // code for IE6, IE5
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    return xhttp;
}

function tpxReadCookie(pName)
{
    var nameEQ = pName + "=";
    var ca = document.cookie.split(";");
    for (var i = 0;i < ca.length; i++)
    {
        var c = ca[i];
        while (c.charAt(0) == ' ')
        {
            c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) == 0)
        {
            return c.substring(nameEQ.length, c.length);
        }
    }

    return null;
}

function tpxCreateBasketCountCookie()
{
    if (gContinueShoppingMessageEnabled)
    {
        var expireDate = new Date();
        expireDate.setTime(expireDate.getTime() + (30 * 24 * 60 * 60 * 1000));
        var basketCountExpires = expireDate.toUTCString();

        tpxCreateCookie('mawhlbc', gBasketCount, basketCountExpires);
    }
}


function tpxCreateCookie(name, value, expires)
{
    document.cookie = name + "=" + value + "; expires=" + expires + "; path=/";
}

function tpxDeleteCookie(name)
{
    tpxCreateCookie(name, null, "Thu, 01 Jan 1970 00:00:00 UTC");
}

function tpxGenerateID()
{
    var result = '';
    var date = new Date();
    var timeStamp = String(date.getTime());
    var len = timeStamp.length;
    var charCode = 0;

    for (var i = 0; i < len; i++)
    {
        charCode = timeStamp.charCodeAt(i);
        result += 138 - charCode - i;
    }

    return result;
}

function tpxCreateMAWHLUIDCookie()
{
    var date = new Date();
    date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000));
    var value = tpxGenerateID();

    tpxCreateCookie("mawhluid", value, date.toGMTString());
}

function tpxIsEmpty(obj)
{
    return (Object.getOwnPropertyNames(obj).length === 0);
}

function tpxParamString(pSourceString)
{
    var args = arguments;

    for(var i = 0; i < arguments.length; i++)
    {
        pSourceString = pSourceString.replace("^" + String(i-1), args[i]);
    }

    return pSourceString;
}

function tpxCreateRandomString(pLength)
{
    return Math.round((Math.pow(36, pLength + 1) - Math.random() * Math.pow(36, pLength))).toString(36).slice(1);
}

function tpxAddGETParam(pURL, pKey, pValue)
{
    var connector = "?";

    if (/[?&]/.test(pURL))
    {
        connector = "&";
    }

    return pURL += connector + pKey + "=" + pValue;
}

function tpxHighLevelProcessRequest(pRequestFunction, pSetCookie, pParams, pSSOParams)
{
    var serverPage = '';
    var fsAction = '';
    var callback = '';
    var requestMethod = 'POST';
    var performRequest = true;
    var theDate = new Date();
    var timestamp = Math.round((theDate.getTime()) / 1000);
    var basketRef = '';
    var mawID = 0;
    var mawIDLookup = tpxReadCookie('mawhluid');
    var basketRefLookUpValue = tpxReadCookie('mawebhlbr');

    // if the unique high level cookie has not been created then we must create it.
    if ((mawIDLookup != null) || (mawIDLookup != ''))
    {
        mawID = mawIDLookup;
    }

    if ((basketRefLookUpValue != null) && (basketRefLookUpValue != ''))
    {
        basketRef = basketRefLookUpValue;
    }

    if ((gSSOEnabled != kSSOOff) && (pSetCookie))
    {
        var cookieData = pRequestFunction + "|" + JSON.stringify(pParams);

        tpxCreateCookie("mawssoa", cookieData, "Fri, 31 Dec 9999 23:59:59 GMT");

    }

    switch (pRequestFunction)
    {
        case 'tpxHighLevelCheckUserSessionControl':
            fsAction = '?fsaction=OnlineAPI.checkUserSession';
            callback = tpxHighLevelCheckUserSessionView;
            break;
        case 'tpxHighLevelCreateProjectControl':
            requestMethod = 'GET';
            callback = tpxHighLevelCreateProjectView;
            fsAction = '?fsaction=OnlineAPI.createProject3&' + pParams['id'] + '&ssoenabled=' + pParams['ssoenabled'] + '&mawebhlbr=' + basketRef + '&prtz=' + timestamp;
            break;
        case 'tpxHighLevelEditProjectControl':
            fsAction = '?fsaction=OnlineAPI.hlEditProject';
            callback = tpxHighLevelEditProjectView;
            break;
        case 'tpxHighLevelDuplicateProjectControl':
            fsAction = '?fsaction=OnlineAPI.hlDuplicateProject';
            callback = tpxHighLevelDuplicateProjectView;
            break;
        case 'tpxHighLevelRenameProjectControl':
            fsAction = '?fsaction=OnlineAPI.hlRenameProject';
            callback = tpxHighLevelRenameProjectView;
            break;
        case 'tpxHighLevelDeleteProjectControl':
            fsAction = '?fsaction=OnlineAPI.hlDeleteProject';
            callback = tpxHighLevelDeleteProjectView;
            break;
        case 'tpxHighLevelGetBasketContentsControl':
            fsAction = '?fsaction=OnlineAPI.basketInit';
            callback = tpxHighLevelGetBasketContentsView;

            if (gBasketLoaded)
            {
                performRequest = false
            }

            break;
        case 'tpxHighLevelGetProjectListControl':
            fsAction = '?fsaction=OnlineAPI.hlViewProjectsList';
            callback = tpxHighLevelGetProjectListView;

            if (gProjectListLoaded)
            {
                performRequest = false
            }

            break;
        case 'tpxHighLevelEmptyBasketControl':
            fsAction = '?fsaction=OnlineAPI.emptyBasket';
            callback = tpxHighLevelEmptyBasketView;
            break;
        case 'tpxHighLevelRemoveItemFromBasketControl':
            fsAction = '?fsaction=OnlineAPI.removeItemFromBasket';
            callback = tpxHighLevelRemoveItemFromBasketView;
            break;
        case 'tpxHighLevelCheckoutControl':
            fsAction = '?fsaction=OnlineAPI.checkout';
            callback = tpxHighLevelCheckoutView;
            break;
        case 'tpxHighLevelSignInInitControl':
        case 'tpxHighLevelSignInInitControl2':
            fsAction = '?fsaction=OnlineAPI.signInInit';

            if (pRequestFunction == 'tpxHighLevelSignInInitControl')
            {
                callback = tpxHighLevelSignInInitView;
            }
            else
            {
                callback = tpxHighLevelCheckUserSessionView;
            }

            break;
        case 'tpxHighLevelRegisterInitControl':
            fsAction = '?fsaction=OnlineAPI.registerInit';
            callback = tpxHighLevelRegisterInitView;
            break;
        case 'tpxHighLevelMyAccountInitControl':
            fsAction = '?fsaction=OnlineAPI.myAccountInit';
            callback = tpxHighLevelMyAccountInitView;
            break;
        case 'tpxHighLevelLogoutControl':
            fsAction = '?fsaction=OnlineAPI.hlLogout';
            callback = tpxHighLevelLogoutView;
            break;
    }

    serverPage = kServerURL + fsAction;

    for (var key in pSSOParams)
    {
        serverPage = tpxAddGETParam(serverPage, key, encodeURIComponent(pSSOParams[key]));
    }

    if (performRequest)
    {
        /* get an XMLHttpRequest object for use */
        /* make xmlhttp local so we can run simlutaneous requests */
        var xmlhttp = tpxGetXMLHTTP();

        if (requestMethod == 'POST')
        {
            pParams['mawebhluid'] = mawID;
            pParams['mawebhlbr'] = basketRef;
            pParams['prtz'] = timestamp;
            pParams['browserlocale'] = tpxGetBrowserLocale();
            pParams['ssotoken'] = gSSOToken;

            xmlhttp.open('POST', serverPage, false);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

            var postParams = '';

            for (var key in pParams)
            {
                postParams += '&' + key + '=' + encodeURIComponent(pParams[key]);
            }
        }
        else
        {
            serverPage = tpxAddGETParam(serverPage, 'dummy', new Date().getTime());
            serverPage = tpxAddGETParam(serverPage, 'ssotoken', gSSOToken);

            xmlhttp.open("GET", serverPage, true);
        }

        xmlhttp.onreadystatechange = function()
        {
            if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            {
                var responseObj = JSON.parse(xmlhttp.responseText);

                if ((responseObj.result == kBasketInternalError) || (responseObj.result == kBasketExpired) || (responseObj.result == kBasketSessionExpired))
                {
                    gBasketCount = 0;

                    tpxDeleteCookie("mawuli");
                    tpxDeleteCookie("mawebhlbr");

                    if (pRequestFunction == 'tpxHighLevelCreateProjectControl')
                    {
                        // if the basket or session has expired then only for a create project action we must try to replay the createProject.
                        // as we have deleted the basket cookie a new cookie will be generated when the action is replayed.
                        tpxHighLevelProcessRequest('tpxHighLevelCreateProjectControl', true, pParams, {});
                        return false;
                    }
                    else
                    {
                        tpxHighLevelLoggedInStatusCallBack(0);
                    }
                }

                switch (pRequestFunction)
                {
                    case 'tpxHighLevelSignInInitControl':

                        if (responseObj.result == -2)
                        {
                            // set basketcookie based of the token
                            basketRef = responseObj.basketref;
                            basketCookieExpiryTime = responseObj.basketcookieexpirytime;
                            userCookieExpiryTime = responseObj.usercookieexpirytime;

                            var date = new Date();
                            date.setTime(basketCookieExpiryTime * 1000);

                            tpxCreateCookie("mawebhlbr", basketRef, date.toGMTString());

                            var date = new Date();
                            date.setTime(userCookieExpiryTime * 1000);

                            tpxCreateCookie("mawuli", 1, date.toGMTString());

                            gSSOToken = responseObj.ssotoken;

                            gBasketCount = 0;
                            gBasketLoaded = false;
                            gProjectListLoaded = false;
                            gProjectListCount = 0;

                        }
                        break;
                    case 'tpxHighLevelSignInInitControl2':
                    case 'tpxHighLevelCheckUserSessionControl':
                        if (responseObj.result == 0)
                        {
                            // set basketcookie based of the token
                            basketRef = responseObj.basketref;
                            basketCookieExpiryTime = responseObj.basketcookieexpirytime;
                            userCookieExpiryTime = responseObj.usercookieexpirytime;
                            gContinueShoppingMessageEnabled = responseObj.continueshoppingmessageenabled;
                            gContinueShoppingMessageEnabled = parseInt(gContinueShoppingMessageEnabled);

                            if (isNaN(gContinueShoppingMessageEnabled))
                            {
                                gContinueShoppingMessageEnabled = 1;
                            }

                            var date = new Date();
                            date.setTime(basketCookieExpiryTime * 1000);

                            tpxCreateCookie("mawebhlbr", basketRef, date.toGMTString());

                            var date = new Date();
                            date.setTime(userCookieExpiryTime * 1000);

                            tpxCreateCookie("mawuli", 1, date.toGMTString());

                            gSSOToken = responseObj.ssotoken;

                            gBasketCount = responseObj.basketcount;
                            tpxCreateBasketCountCookie();

                            tpxHighLevelLoggedInStatusCallBack(1);
                        }
                        else if ((responseObj.result == -1) || (responseObj.result > 0))
                        {
                            tpxDeleteCookie("mawuli");

                            if (((tpxGetUrlVar('odlo') == 1) && (responseObj.result == -1)) || (responseObj.result > 0) || ((responseObj.result == -1) && (gSSOEnabled != kSSOOff)))
                            {
                                tpxDeleteCookie("mawebhlbr");
                            }
                        }
                        else if (responseObj.result == -2)
                        {
                            document.location = responseObj.ssoredirect;
                        }

                        break;
                    case 'tpxHighLevelCreateProjectControl':

                        if (responseObj.result == 0)
                        {
                            basketRef = responseObj.basketref;
                            cookieExpiryTime = responseObj.cookieexpirytime;

                            var date = new Date();
                            date.setTime(cookieExpiryTime * 1000);

                            tpxCreateCookie("mawebhlbr", basketRef, date.toGMTString());
                        }

                        break;
                    case 'tpxHighLevelGetBasketContentsControl':
                        gBasketLoaded = true;
                        gBasketCount = responseObj.basketcount;

                        break;
                    case 'tpxHighLevelGetProjectListControl':

                        if (responseObj.result != 0)
                        {
                            basketRef = responseObj.basketref;
                            cookieExpiryTime = responseObj.cookieexpirytime;

                            var date = new Date();
                            date.setTime(cookieExpiryTime * 1000);

                            tpxCreateCookie("mawebhlbr", basketRef, date.toGMTString());
                            tpxDeleteCookie("mawuli");
                        }

                        gProjectListLoaded = true;
                        gProjectListCount = responseObj.basketcount;

                        break;
                    case 'tpxHighLevelLogoutControl':

                        tpxDeleteCookie("mawuli");
                        tpxDeleteCookie("mawebhlbr");

                        gBasketCount = 0;
                        gProjectListCount = 0;

                        break;
                    case 'tpxHighLevelCheckoutControl':
                        if (responseObj.result != 0)
                        {
                            tpxDeleteCookie("mawuli");
                            tpxDeleteCookie("mawebhlbr");

                            gBasketCount = 0;
                            gProjectListCount = 0;
                        }

                        break;
                    case 'tpxHighLevelEditProjectControl':

                        if ((responseObj.result != 0) && (responseObj.ssoerror))
                        {
                            tpxDeleteCookie("mawuli");
                            tpxDeleteCookie("mawebhlbr");

                            gBasketCount = 0;
                            gProjectListCount = 0;
                        }

                        break;
                    case 'tpxHighLevelRemoveItemFromBasketControl':
                        if ((gBasketCount > 0) && (responseObj.result === 0))
                        {
                            gBasketCount--;
                        }

                        gProjectListLoaded = false;

                        break;
                    case 'tpxHighLevelEmptyBasketControl':
                        gBasketCount = 0;
                        gProjectListLoaded = false;
                        break;
                }

                if (pRequestFunction == 'tpxHighLevelRenameProjectControl')
                {
                    callback(responseObj, pParams['fromprojectlist']);
                }
                else
                {
                    callback(responseObj);
                }
            }
        };

        if (requestMethod == 'POST')
        {
            xmlhttp.send(postParams);
        }
        else
        {
            xmlhttp.send(null);
        }
    }
    else
    {
        responseObj = {};
        callback(responseObj);
    }
}

function tpxHighLevelCreateProjectControl(pURLParams)
{
    var paramArray = new Object();
    paramArray['id'] = pURLParams;
    paramArray['ssoenabled'] = gSSOEnabled;

    tpxHighLevelProcessRequest('tpxHighLevelCreateProjectControl', true, paramArray, {});

    return false;
}

function tpxHighLevelCreateProjectView(pJsonResponseObject)
{
    if ((pJsonResponseObject.result == 0) || (pJsonResponseObject.result == -2))
    {
        onlineDesignURL = pJsonResponseObject.designurl;

        if (pJsonResponseObject.result == -2)
        {
            onlineDesignURL = onlineDesignURL;
        }

        window.location = onlineDesignURL;
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: function()
                    {
                        if ((pJsonResponseObject.redirecturl) && (pJsonResponseObject.redirecturl != ''))
                        {
                            document.location = pJsonResponseObject.redirecturl;
                        }
                        else
                        {
                            basicModal.close();
                        }
                    }
                }
            }
        }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelCheckUserSessionControl(pLookUpToken)
{
    var paramArray = new Object();
    paramArray['lookuptoken'] = pLookUpToken;
    paramArray['ssoenabled'] = gSSOEnabled;

    tpxHighLevelProcessRequest('tpxHighLevelCheckUserSessionControl', true, paramArray, {});

    return false;
}

function tpxHighLevelCheckUserSessionView(pJsonResponseObject)
{
    if (pJsonResponseObject.result > 0)
    {
        var resultAlert =
            {
                body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
                buttons:
                    {
                        action:
                            {
                                title: tpxGetLocaleString(kStr_ButtonOK),
                                fn: function()
                                {
                                    document.location = pJsonResponseObject.redirecturl;
                                }
                            }
                    }
            }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelBasketLocalise()
{
    var basketButtonWrapper = document.getElementById('tpx-basketButtonWrapper');

    if (basketButtonWrapper)
    {
        basketButtonWrapper.innerHTML = '<a class="tpx tpx-button tpx-basketButton" href="#" id="tpx-basketlink" onClick="tpxBasketOnClick()" ><span class="tpx tpx-basketCount" id="tpx-basketButtonCount">' + gBasketCount  + '</span><span class="tpx tpx-basketLabel">' + tpxGetLocaleString(kStr_LabelBasket) + '</span></a>';
    }

    var emptyBasketButton = document.getElementById('tpx-emptyBasketButton');

    if (emptyBasketButton)
    {
        emptyBasketButton.innerHTML = tpxGetLocaleString(kStr_ButtonEmptyBasket);
    }

    var checkoutbutton = document.getElementById('tpx-checkoutbutton');

    if (checkoutbutton)
    {
        checkoutbutton.innerHTML = tpxGetLocaleString(kStr_ButtonCheckout);
    }

    var signIn = document.getElementById('tpx-signIn');

    if (signIn)
    {
        signIn.innerHTML = tpxGetLocaleString(kStr_LabelSignIn);
    }

    var register = document.getElementById('tpx-register');

    if (register)
    {
        register.innerHTML = tpxGetLocaleString(kStr_LabelRegister);
    }

    var projectslist = document.getElementById('tpx-projectslist');

    if (projectslist)
    {
        projectslist.innerHTML = tpxGetLocaleString(kStr_LabelMyProjects);
    }

    return false;
}

function tpxHighLevelEditProjectControl(pProjectRef, pCanUnlock, pForceKill)
{
    var paramArray = new Object();
    paramArray['projectref'] = pProjectRef;
    paramArray['forcekill'] = pForceKill;
    paramArray['canunlock'] = pCanUnlock;
    paramArray['ssoenabled'] = gSSOEnabled;

    tpxHighLevelProcessRequest('tpxHighLevelEditProjectControl', true, paramArray, {});

    return false;
}

function tpxHighLevelEditProjectView(pJsonResponseObject)
{
    if (pJsonResponseObject.result == 0)
    {
        onlineDesignURL = pJsonResponseObject.designurl;

        if (pJsonResponseObject.result == -2)
        {
            onlineDesignURL = onlineDesignURL;
        }

        window.location = onlineDesignURL;
    }
    else if (pJsonResponseObject.result == 31)
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action:
                    {
                        title: tpxGetLocaleString(kStr_ButtonContinue),
                        fn: function()
                        {
                            basicModal.close();
                        }
                    }
            }
        }
        basicModal.show(resultAlert);
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action:
                    {
                        title: tpxGetLocaleString(kStr_ButtonContinue),
                        fn: function()
                        {
                            if (pJsonResponseObject.result == 6)
                            {
                                tpxHighLevelEditProjectControl(pJsonResponseObject.projectref, 1, 1);
                            }
                            else
                            {
                                if ((pJsonResponseObject.redirecturl) && (pJsonResponseObject.redirecturl != ''))
                                {
                                    document.location = pJsonResponseObject.redirecturl;
                                }
                                else
                                {
                                    basicModal.close();
                                }
                            }
                        }
                    },
                cancel:
                    {
                        title: tpxGetLocaleString(kStr_ButtonCancel),
                        fn: basicModal.close
                    }
            }
        }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelDuplicateProjectControl(pProjectRef, pCurrentProjectName)
{
    var paramArray = new Object();
    paramArray['projectref'] = pProjectRef;

    var projectNameInput = {
        body: '<p>' + tpxGetLocaleString(kStr_LabelProjectName) +':</p><input class="basicModal__text" type="text" name="tpxprojectname" placeholder="'+ tpxGetLocaleString(kStr_LabelProjectName) + ' "value="' + pCurrentProjectName + '">',
        buttons: {
            cancel: {
                title: tpxGetLocaleString(kStr_ButtonCancel),
                fn: basicModal.close
            },
            action: {
                title: tpxGetLocaleString(kStr_ButtonContinue),
                fn: function(data) {

                    if (data.tpxprojectname.length<1)
                    {
                        return basicModal.error('tpxprojectname');
                    }
                    else
                    {
                        paramArray['projectname'] = data.tpxprojectname;
                        tpxHighLevelProcessRequest('tpxHighLevelDuplicateProjectControl', false, paramArray, {});
                    }

                    basicModal.close();
                }
            }
        }
    }

    basicModal.show(projectNameInput);

    return false;
}

function tpxHighLevelDuplicateProjectView(pJsonResponseObject)
{
    if (pJsonResponseObject.result == 0)
    {
        tpxHighLevelEditProjectControl(pJsonResponseObject.projectref, 1, 1);
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelRenameProjectControl(pItemID, pProjectRef, pFromProjectList)
{
    var paramArray = new Object();

    paramArray['projectref'] = pProjectRef;
    paramArray['basketitemidtoupdate'] = pItemID;

    var itemIDPrefix = '';

    if (pFromProjectList)
    {
        itemIDPrefix = 'projectlist'
    }

    var projectName = document.getElementById('tpx-'+ itemIDPrefix + 'item-renameproject-' + pItemID).getAttribute("data-projectname");

    var projectNameInput = {
        body: '<p>' + tpxGetLocaleString(kStr_LabelRenameProject) +':</p><input class="basicModal__text" type="text" name="tpxprojectname" placeholder="'+ tpxGetLocaleString(kStr_LabelProjectName) + '" value="' + projectName + '">',
        buttons: {
            cancel: {
                title: tpxGetLocaleString(kStr_ButtonCancel),
                fn: basicModal.close
            },
            action: {
                title: tpxGetLocaleString(kStr_ButtonContinue),
                fn: function(data) {

                    if (data.tpxprojectname.length<1)
                    {
                        return basicModal.error('tpxprojectname');
                    }
                    else
                    {
                        paramArray['newname'] = data.tpxprojectname;
                        paramArray['fromprojectlist'] = pFromProjectList;
                        tpxHighLevelProcessRequest('tpxHighLevelRenameProjectControl', false, paramArray, {});
                    }

                    basicModal.close();
                }
            }
        }
    }

    basicModal.show(projectNameInput);
}

function tpxHighLevelRenameProjectView(pJsonResponseObject, pFromProjectList)
{
    if (pJsonResponseObject.result == 0)
    {
        var itemIDPrefix = '';

        if (pFromProjectList)
        {
            itemIDPrefix = 'projectlist'
        }

        document.getElementById('tpx-'+ itemIDPrefix + 'item-projectname-' + pJsonResponseObject.basketitemidtoupdate).innerHTML = pJsonResponseObject.newprojectname;
        document.getElementById('tpx-'+ itemIDPrefix + 'item-renameproject-' + pJsonResponseObject.basketitemidtoupdate).setAttribute('data-projectname', pJsonResponseObject.newprojectname);
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelDeleteProjectControl(pItemID, pProjectRef, pProjectName, pCanUnlock, pForceKill)
{
    var paramArray = new Object();
    paramArray['projectref'] = pProjectRef;
    paramArray['forcekill'] = pForceKill;
    paramArray['canunlock'] = pCanUnlock;
    paramArray['itemtoremoveid'] = pItemID;

    var projectName = document.getElementById('tpx-projectlistitem-renameproject-' + pItemID).getAttribute("data-projectname");

    var deleteProjectPrompt = {
        body: '<p>' + tpxParamString(tpxGetLocaleString(kStr_MessageDeleteProjectConfirmation), projectName) + '</p>',
        buttons: {
            cancel: {
                title: tpxGetLocaleString(kStr_ButtonNo),
                fn: basicModal.close
            },
            action: {
                title: tpxGetLocaleString(kStr_ButtonYes),
                fn: function(data) {

                    tpxHighLevelProcessRequest('tpxHighLevelDeleteProjectControl', false, paramArray, {});

                    basicModal.close();
                }
            }
        }
    }

    basicModal.show(deleteProjectPrompt);

    if (gProjectListCount === 0)
    {
        document.getElementById('tpx-empty-state').style.visibility = 'visible'
    }

    return false;
}

function tpxHighLevelDeleteProjectView(pJsonResponseObject)
{

    if (pJsonResponseObject.result == 0)
    {
        var parentNode = document.getElementById('tpx-projectsItemList');
        var nodeToRemove = document.getElementById('tpx-projectlistitem' + pJsonResponseObject.itemtoremoveid);

        parentNode.removeChild(nodeToRemove);
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }

    tpxCreateBasketCountCookie();

    return false;
}

function tpxHighLevelBasketInitialise()
{
    var mawIDLookup = tpxReadCookie('mawhluid');
    var lookUpToken = tpxGetUrlVar('mawbt');
    var basketRef = '';
    var basketRefLookUpValue = tpxReadCookie('mawebhlbr');
    var hlCreateID = tpxGetUrlVar('mawebhlcreate');
    var ssoAction = '';
    var ssoActionParam = [];
    var ssoKey = tpxGetUrlVar('ssokey');
    var gwmParam = tpxGetUrlVar('gwm');
    var languageParam = tpxGetUrlVar('l');
    var gdParam = tpxGetUrlVar('gd');
    gOldBasketCount = tpxReadCookie('mawhlbc');

    if (languageParam == '')
    {
        languageParam = tpxGetBrowserLocale();
    }

    // if the unique high level cookie has not been created then we must create it.
    if ((mawIDLookup == null) || (mawIDLookup == ''))
    {
        tpxCreateMAWHLUIDCookie();
    }

    // Single item workflow has been invoked with Multi Line Basket enabled on the brand
    // we need to take the user straight into the project
    if (hlCreateID != '')
    {
        var URLParams = 'id=' + hlCreateID;

        URLParams += (gwmParam != '') ? '&gwm=' + gwmParam : '';
        URLParams += (languageParam != '') ? '&l=' + languageParam : '';
        URLParams += (gdParam != '') ? '&gd=' + gdParam : '';

        tpxHighLevelCreateProjectControl(URLParams);
        return false;
    }

    tpxHighLevelBasketLocalise();

    // check to see if we have a basket cookie and if we have use the cookie value for the basketref.
    if ((basketRefLookUpValue != null) && (basketRefLookUpValue != ''))
    {
        basketRef = basketRefLookUpValue;
    }

    if ((gSSOEnabled != kSSOOff) && (ssoKey != ''))
    {
        var cookieData = tpxReadCookie('mawssoa');

        if (cookieData != null)
        {
            var cookieDataArray = cookieData.split('|');

            if (cookieDataArray.length == 2)
            {
                ssoAction = cookieDataArray[0];

                if (cookieDataArray[1] != '')
                {
                    ssoActionParam = JSON.parse(cookieDataArray[1]);
                }
            }
        }
    }

    tpxDeleteCookie("mawssoa");

    if ((ssoAction != '') && (ssoKey != ''))
    {
        if ((ssoAction == 'tpxHighLevelSignInInitControl') && (gSSOEnabled == kSSOAutomatic))
        {
            ssoAction = 'tpxHighLevelSignInInitControl2';
        }

        tpxHighLevelProcessRequest(ssoAction, false, ssoActionParam, {'sso': 2, 'ssokey': ssoKey});
    }
    else
    {
        // if the session cookie has a legitimate value && we have been redirected back to the product selector,
        // or we have a basket ref and the session cookie is still not a legitimate session ref
        // then we must check to see if the session is still active
        if ((lookUpToken != '') || ((basketRef != '') || (tpxGetUrlVar('odlo') == 1) || (gSSOEnabled == kSSOAutomatic)))
        {
            if (tpxGetUrlVar('odlo') == 1)
            {
                tpxDeleteCookie("mawebhlbr");
            }

            tpxHighLevelCheckUserSessionControl(lookUpToken);
        }
    }
}

function tpxHighLevelLoggedInStatusCallBack(pIsSignedIn)
{
    var signInLabel = tpxGetLocaleString(kStr_LabelSignIn);
    var registerLabel = tpxGetLocaleString(kStr_LabelRegister);
    var signInLogoutButtonAction = tpxHighLevelSignInInitControl;
    var registerMyAccountButtonAction = tpxHighLevelRegisterInitControl;

    if (pIsSignedIn == 1)
    {
        signInLabel = tpxGetLocaleString(kStr_LabelLogout);
        registerLabel = tpxGetLocaleString(kStr_LabelMyAccount);
        signInLogoutButtonAction = tpxHighLevelLogoutControl;
        registerMyAccountButtonAction = tpxHighLevelMyAccountInitControl;

        var basketButtonWrapper = document.getElementById('tpx-basketButtonWrapper');

        if (basketButtonWrapper)
        {
            basketButtonWrapper.innerHTML = '<a class="tpx tpx-button tpx-basketButton" href="#" id="tpx-basketlink" onClick="tpxBasketOnClick()" ><span class="tpx tpx-basketCount" id="tpx-basketButtonCount">' + gBasketCount  + '</span><span class="tpx tpx-basketLabel">' + tpxGetLocaleString(kStr_LabelBasket) + '</span></a>';
        }
    }
    else
    {
        var basketCountElement = document.getElementById('tpx-basketcountbadgeinner');

        if (basketCountElement)
        {
            basketCountElement.innerHTML = gBasketCount;
        }

        var basketList = document.getElementById('tpx-basketItemList');

        if (basketList)
        {
            basketList.innerHTML = '';
        }

        var projectList = document.getElementById('tpx-projectsItemList');

        if (projectList)
        {
            projectList.innerHTML = '';
        }
    }

    var signInButton = document.getElementById('tpx-signIn');

    if (signInButton)
    {
        signInButton.innerHTML = signInLabel;
        signInButton.onclick = signInLogoutButtonAction;
    }

    var registerButton = document.getElementById('tpx-register');

    if (registerButton)
    {
        registerButton.innerHTML = registerLabel;
        registerButton.onclick = registerMyAccountButtonAction;
    }

    var basketButtonWrapper = document.getElementById('tpx-basketButtonWrapper');

    if (basketButtonWrapper)
    {
        document.getElementById('tpx-basketButtonCount').innerHTML = gBasketCount;
    }

    var newBasketCount = gBasketCount;

    if ((newBasketCount > gOldBasketCount) && (!tpxGetUrlVar('mawbt')) && (gContinueShoppingMessageEnabled) && (gOldBasketCount !== null))
    {
        basicModal.show(
            {
                body: "<p>" + tpxGetLocaleString(kStr_MessageProjectAddedToBasket) + "</p>",
                closable: true,
                buttons:
                    {
                        cancel:
                            {
                                title: tpxGetLocaleString(kStr_ButtonContinueShopping),
                                fn: basicModal.close
                            },
                        action:
                            {
                                title: tpxGetLocaleString(kStr_ButtonCheckoutNow),
                                fn: tpxHighLevelCheckoutControl
                            }
                    }
            });
    }
}

function tpxHighLevelLogoutControl()
{
    tpxHighLevelProcessRequest('tpxHighLevelLogoutControl', false, {}, {});

    return false;
}

function tpxHighLevelLogoutView(pJsonResponseObject)
{
    if ((pJsonResponseObject.result == 0) || (pJsonResponseObject.result == -2))
    {
        if ((pJsonResponseObject.result == -2) && (pJsonResponseObject.ssoredirect != ''))
        {
            document.location = pJsonResponseObject.ssoredirect;
        }
        else
        {
            tpxHighLevelLoggedInStatusCallBack(0);
        }
    }

    return false;
}

function tpxHighLevelGetBasketContentsControl()
{
    tpxHighLevelProcessRequest('tpxHighLevelGetBasketContentsControl', false, {}, {});

    return false;
}

function tpxHighLevelGetBasketContentsView(pJsonResponseObject)
{
    var responseEmpty = tpxIsEmpty(pJsonResponseObject);

    if ((gBasketCount > 0) && (! responseEmpty))
    {
        var basketItems = pJsonResponseObject.items;

        var basketCountBadgeInner = document.getElementById('tpx-basketcountbadgeinner');

        if (basketCountBadgeInner)
        {
            basketCountBadgeInner.innerHTML = gBasketCount;
        }

        for (var i = 0; i < gBasketCount; i++)
        {
            var currentProjectRef = basketItems[i].projectref;

            var listItemElement = document.createElement('li');
            listItemElement.className = "tpx-clearfix";
            listItemElement.id = "tpx-basketitem" + (i + 1);
            listItemElement.setAttribute('data-projectref', basketItems[i].projectref);

            var projectInfoContainerElement = document.createElement('div');
            projectInfoContainerElement.className = "tpx-projectinfocontainer";

            var infoContainerElemet = document.createElement('div');
            infoContainerElemet.className = "tpx-infocontainer";

            var projectNameElement = document.createElement('span');

            projectNameElement.className = "tpx-item-projectname";
            projectNameElement.id = "tpx-item-projectname-" + (i + 1);

            var projectNameText = document.createTextNode(basketItems[i].projectname);
            projectNameElement.appendChild(projectNameText);

            var layoutNameElement = document.createElement('span');
            layoutNameElement.className = "tpx-item-layoutname";

            var layoutNameText = document.createTextNode(tpxGetLocaleString(kStr_LabelLayoutName) + ': ' + tpxGetLocaleString(basketItems[i].layoutname));
            layoutNameElement.appendChild(layoutNameText);

            var projectActionsContainer = document.createElement('div');
            projectActionsContainer.id = "tpx-projectactionscontainer";

            var editProjectLink = document.createElement('a');
            editProjectLink.className = "tpx-projectaction";
            editProjectLink.href = "#";
            editProjectLink.innerHTML = tpxGetLocaleString(kStr_LabelEdit);

            editProjectLink.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;
                return function()
                {
                    tpxHighLevelEditProjectControl(currentProjectRef, 1, 1);
                }
            })();

            var renameProjectLink = document.createElement('a');
            renameProjectLink.id = "tpx-item-renameproject-" + (i + 1);
            renameProjectLink.className = "tpx-projectaction";
            renameProjectLink.href = "#";
            renameProjectLink.innerHTML = tpxGetLocaleString(kStr_LabelRename);
            renameProjectLink.setAttribute('data-projectname', basketItems[i].projectname);
            renameProjectLink.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;

                return function()
                {
                    tpxHighLevelRenameProjectControl((currentCount + 1), currentProjectRef, false);
                }
            })();

            var duplicateProjectLink =  document.createElement('a');
            duplicateProjectLink.className = "tpx-projectaction";
            duplicateProjectLink.href = "#";
            duplicateProjectLink.innerHTML = tpxGetLocaleString(kStr_LabelDuplicate);

            duplicateProjectLink.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;
                var currentProjectName = basketItems[i].projectname;
                return function()
                {
                    tpxHighLevelDuplicateProjectControl(currentProjectRef, currentProjectName);
                }
            })();


            projectActionsContainer.appendChild(editProjectLink);
            projectActionsContainer.appendChild(renameProjectLink);
            projectActionsContainer.appendChild(duplicateProjectLink);


            var removeFromBasketElement =  document.createElement('div');
            removeFromBasketElement.className = "tpx-removefrombasket";

            removeFromBasketElement.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;
                return function()
                {
                    tpxHighLevelRemoveItemFromBasketControl('tpx-basketitem' + (currentCount + 1), currentProjectRef, 0);
                }
            })();

            infoContainerElemet.appendChild(projectNameElement);
            infoContainerElemet.appendChild(layoutNameElement);
            infoContainerElemet.appendChild(projectActionsContainer);

            projectInfoContainerElement.appendChild(infoContainerElemet);
            projectInfoContainerElement.appendChild(removeFromBasketElement);

            listItemElement.appendChild(projectInfoContainerElement);

            var basketItemContainer = document.getElementById('tpx-basketItemList');
            basketItemContainer.appendChild(listItemElement);

            document.getElementById('tpx-basketcountbadgeinner').style.visibility = 'visible';
            document.getElementById('tpx-emptyBasketButton').style.visibility = 'visible';
            document.getElementById('tpx-checkoutbutton').style.visibility = 'visible';
            document.getElementById('tpx-empty-cart').style.visibility = 'hidden';
        }
    }
    else if (gBasketCount === 0)
    {
        document.getElementById('tpx-basketcountbadgeinner').style.visibility = 'hidden';
        document.getElementById('tpx-emptyBasketButton').style.visibility = 'hidden';
        document.getElementById('tpx-checkoutbutton').style.visibility = 'hidden';
        document.getElementById('tpx-empty-cart').style.visibility = 'visible';
    }

    // hide the loading spinner
    document.getElementById('tpx-loadingspinnercontainer').style.display = 'none';

    return false;
}

function tpxHighLevelGetProjectListControl()
{
    tpxHighLevelProcessRequest('tpxHighLevelGetProjectListControl', false, {}, {});

    return false;
}

function tpxHighLevelGetProjectListView(pJsonResponseObject)
{
    var responseEmpty = tpxIsEmpty(pJsonResponseObject);

    if ((gProjectListCount > 0) && (! responseEmpty))
    {
        var projectListItemContainer = document.getElementById('tpx-projectsItemList');
        projectListItemContainer.innerHTML = '';

        var basketItems = pJsonResponseObject.items;

        for (var i = 0; i < gProjectListCount; i++)
        {
            var currentProjectRef = basketItems[i].projectref;

            var listItemElement = document.createElement('li');
            listItemElement.className = "tpx-clearfix";
            listItemElement.id = "tpx-projectlistitem" + (i + 1);
            listItemElement.setAttribute('data-projectref', basketItems[i].projectref);

            var projectInfoContainerElement = document.createElement('div');
            projectInfoContainerElement.className = "tpx-projectinfocontainer";

            var infoContainerElemet = document.createElement('div');
            infoContainerElemet.className = "tpx-infocontainer";

            var projectNameElement = document.createElement('span');
            projectNameElement.className = "tpx-item-projectname";
            projectNameElement.id = "tpx-projectlistitem-projectname-" + (i + 1);

            var projectNameText = document.createTextNode(basketItems[i].projectname);
            projectNameElement.appendChild(projectNameText);

            var layoutNameElement = document.createElement('span');
            layoutNameElement.className = "tpx-item-layoutname";

            var layoutNameText = document.createTextNode(tpxGetLocaleString(kStr_LabelLayoutName) + ': ' + tpxGetLocaleString(basketItems[i].layoutname));
            layoutNameElement.appendChild(layoutNameText);

            var projectActionsContainer = document.createElement('div');
            projectActionsContainer.id = "tpx-projectlistactionscontainer";

            var editProjectLink = document.createElement('a');
            editProjectLink.className = "tpx-projectaction";
            editProjectLink.href = "#";
            editProjectLink.innerHTML = tpxGetLocaleString(kStr_LabelEdit);

            editProjectLink.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;
                return function()
                {
                    tpxHighLevelEditProjectControl(currentProjectRef, 1, 1);
                }
            })();

            var renameProjectLink = document.createElement('a');
            renameProjectLink.id = "tpx-projectlistitem-renameproject-" + (i + 1);
            renameProjectLink.className = "tpx-projectaction";
            renameProjectLink.href = "#";
            renameProjectLink.innerHTML = tpxGetLocaleString(kStr_LabelRename);
            renameProjectLink.setAttribute('data-projectname', basketItems[i].projectname);

            renameProjectLink.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;

                return function()
                {
                    tpxHighLevelRenameProjectControl((currentCount + 1), currentProjectRef, true);
                }
            })();

            var duplicateProjectLink =  document.createElement('a');
            duplicateProjectLink.className = "tpx-projectaction";
            duplicateProjectLink.href = "#";
            duplicateProjectLink.innerHTML = tpxGetLocaleString(kStr_LabelDuplicate);

            duplicateProjectLink.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;
                var currentProjectName = basketItems[i].projectname;
                return function()
                {
                    tpxHighLevelDuplicateProjectControl(currentProjectRef, currentProjectName);
                }
            })();

            var deleteProjectLink =  document.createElement('a');
            deleteProjectLink.className = "tpx-projectaction";
            deleteProjectLink.href = "#";
            deleteProjectLink.innerHTML = tpxGetLocaleString(kStr_LabelDelete);

            deleteProjectLink.onclick = (function()
            {
                var currentCount = i;
                var currentProjectRef = basketItems[i].projectref;
                var currentProjectName = basketItems[i].projectname;
                return function()
                {
                    tpxHighLevelDeleteProjectControl((currentCount + 1), currentProjectRef, currentProjectName, 1, 0);
                }
            })();


            projectActionsContainer.appendChild(editProjectLink);
            projectActionsContainer.appendChild(renameProjectLink);
            projectActionsContainer.appendChild(duplicateProjectLink);
            projectActionsContainer.appendChild(deleteProjectLink);

            infoContainerElemet.appendChild(projectNameElement);
            infoContainerElemet.appendChild(layoutNameElement);
            infoContainerElemet.appendChild(projectActionsContainer);

            projectInfoContainerElement.appendChild(infoContainerElemet);

            listItemElement.appendChild(projectInfoContainerElement);
            projectListItemContainer.appendChild(listItemElement);

            document.getElementById('tpx-empty-state').style.visibility = 'hidden';
        }
    }
    else if(gProjectListCount === 0)
    {
        document.getElementById('tpx-empty-state').style.visibility = 'visible';
    }

    // hide the loading spinner
    document.getElementById('tpx-projectloadingspinnercontainer').style.display = 'none';

    return false;
}

function tpxHighLevelRemoveItemFromBasketControl(pItemID, pProjectRef, pForceKill)
{
    var paramArray = new Object();
    paramArray['itemtoremoveid'] = pItemID;
    paramArray['projectref'] = pProjectRef;
    paramArray['forcekill'] = pForceKill;

    tpxHighLevelProcessRequest('tpxHighLevelRemoveItemFromBasketControl', false, paramArray, {});

    return false;
}

function tpxHighLevelRemoveItemFromBasketView(pJsonResponseObject)
{
    if (pJsonResponseObject.result == 32)
    {
        var removeProjectPrompt = {
            body: '<p>' + tpxParamString(tpxGetLocaleString(kStr_MessageProjectOpenInShoppingCart)) + '</p>',
            buttons: {
                cancel: {
                    title: tpxGetLocaleString(kStr_ButtonCancel),
                    fn: basicModal.close
                },
                action: {
                    title: tpxGetLocaleString(kStr_ButtonContinue),
                    fn: function(data) {

                        tpxHighLevelRemoveItemFromBasketControl(pJsonResponseObject.itemtoremoveid, pJsonResponseObject.projectref, 1);

                        basicModal.close();
                    }
                }
            }
        }

        basicModal.show(removeProjectPrompt);
    }
    else if (pJsonResponseObject.result == 0)
    {
        var parentNode = document.getElementById('tpx-basketItemList');
        var nodeToRemove = document.getElementById(pJsonResponseObject.itemtoremoveid);

        parentNode.removeChild(nodeToRemove);

        var basketCountElement = document.getElementById('tpx-basketcountbadgeinner');
        basketCountElement.innerHTML = gBasketCount;

        var basketButtonWrapper = document.getElementById('tpx-basketButtonWrapper');

        if (basketButtonWrapper)
        {
            document.getElementById('tpx-basketButtonCount').innerHTML = gBasketCount;
        }
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }

    tpxCreateBasketCountCookie();

    return false;
}

function tpxHighLevelEmptyBasketControl()
{
    var paramArray = new Object();
    paramArray['forcekill'] = 0;
    tpxHighLevelProcessRequest('tpxHighLevelEmptyBasketControl', false, paramArray, {});
    tpxCreateBasketCountCookie();

    document.getElementById('tpx-basketcountbadgeinner').style.visibility = 'hidden';
    document.getElementById('tpx-emptyBasketButton').style.visibility = 'hidden';
    document.getElementById('tpx-checkoutbutton').style.visibility = 'hidden';
    document.getElementById('tpx-empty-cart').style.visibility = 'visible';

    return false;
}

function tpxHighLevelEmptyBasketView(pJsonResponseObject)
{
    if (pJsonResponseObject.result == 32)
    {
        var removeProjectPrompt = {
            body: '<p>' + tpxParamString(tpxGetLocaleString(kStr_MessageProjectOpenInShoppingCart)) + '</p>',
            buttons: {
                cancel: {
                    title: tpxGetLocaleString(kStr_ButtonCancel),
                    fn: basicModal.close
                },
                action: {
                    title: tpxGetLocaleString(kStr_ButtonContinue),
                    fn: function(data) {
                        tpxHighLevelProcessRequest('tpxHighLevelEmptyBasketControl', false, {forcekill: 1}, {});
                        basicModal.close();
                    }
                }
            }
        }

        basicModal.show(removeProjectPrompt);
    }

    if (pJsonResponseObject.result == 0)
    {
        var basketList = document.getElementById('tpx-basketItemList');
        basketList.innerHTML = '';

        var basketCountElement = document.getElementById('tpx-basketcountbadgeinner');
        basketCountElement.innerHTML = gBasketCount;

        var basketButtonWrapper = document.getElementById('tpx-basketButtonWrapper');

        if (basketButtonWrapper)
        {
            document.getElementById('tpx-basketButtonCount').innerHTML = gBasketCount;
        }
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelCheckoutControl()
{
    var paramArray = new Object();
    paramArray['ssoenabled'] = gSSOEnabled;

    tpxHighLevelProcessRequest('tpxHighLevelCheckoutControl', true, paramArray, {});

    return false;
}

function tpxHighLevelCheckoutView(pJsonResponseObject)
{
    if (pJsonResponseObject.result == 0)
    {
        shoppingCartURL = pJsonResponseObject.shoppingcarturl;

        window.location.replace(shoppingCartURL);
    }
    else
    {
        var resultAlert =
            {
                body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
                buttons:
                    {
                        action:
                            {
                                title: tpxGetLocaleString(kStr_ButtonOK),
                                fn: basicModal.close
                            }
                    }
            }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelSignInInitControl()
{
    var paramArray = new Object();
    paramArray['groupcode'] = '';
    paramArray['ssoenabled'] = gSSOEnabled;

    tpxHighLevelProcessRequest('tpxHighLevelSignInInitControl', true, paramArray, {});

    return false;
}

function tpxHighLevelSignInInitView(pJsonResponseObject)
{
    if ((pJsonResponseObject.result == 0) || (pJsonResponseObject.result == -2))
    {
        var signInURL = pJsonResponseObject.signinurl;

        if (signInURL != '')
        {
            window.location = signInURL;
        }
        else
        {
            var isLoggedInLookUpValue = tpxReadCookie('mawuli');

            tpxHighLevelLoggedInStatusCallBack(isLoggedInLookUpValue);
        }
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }


    return false;
}

function tpxHighLevelRegisterInitControl()
{
    var paramArray = new Object();
    paramArray['groupcode'] = '';

    tpxHighLevelProcessRequest('tpxHighLevelRegisterInitControl', false, paramArray, {});

    return false;
}

function tpxHighLevelRegisterInitView(pJsonResponseObject)
{
    if (pJsonResponseObject.result == 0)
    {
        registerURL = pJsonResponseObject.signinurl;
        window.location = registerURL;
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }

    return false;
}

function tpxHighLevelMyAccountInitControl()
{
    tpxHighLevelProcessRequest('tpxHighLevelMyAccountInitControl', false, {}, {});

    return false;
}

function tpxHighLevelMyAccountInitView(pJsonResponseObject)
{
    if (pJsonResponseObject.result == 0)
    {
        myAccountURL = pJsonResponseObject.myaccounturl;
        window.location = myAccountURL;
    }
    else
    {
        var resultAlert = {
            body: '<p>' + pJsonResponseObject.resultmessage + '</p>',
            buttons: {
                action: {
                    title: tpxGetLocaleString(kStr_ButtonOK),
                    fn: basicModal.close
                }
            }
        }

        basicModal.show(resultAlert);
    }


    return false;
}

function tpxBasketOnClick()
{
    var cartContainer = document.getElementById('tpx-shoppingcartcontents');

    if (cartContainer.style.display == 'block')
    {
        var basketItemContainer = document.getElementById('tpx-basketItemList');
        cartContainer.style.display = "none";

        document.getElementById('tpx-loadingspinnercontainer').style.display = 'block';
    }
    else
    {
        cartContainer.style.display = "block";
        tpxHighLevelGetBasketContentsControl();
    }

    return false;
}

function tpxMyProjectsOnClick()
{
    var projectListContainer = document.getElementById('tpx-projectlistcontents');

    if (projectListContainer.style.display == 'block')
    {
        var projectItemContainer = document.getElementById('tpx-projectsItemList');
        projectListContainer.style.display = "none";

        document.getElementById('tpx-projectloadingspinnercontainer').style.display = 'block';
    }
    else
    {
        projectListContainer.style.display = "block";
        tpxHighLevelGetProjectListControl();

        var basketBarInner = document.getElementById('tpx-basket-bar-inner').getBoundingClientRect();
        var myProjectsButtonBounds =  document.getElementById('tpx-projectslist').getBoundingClientRect();
        var projectContainerBounds = projectListContainer.getBoundingClientRect();

        var left = ((myProjectsButtonBounds.left + myProjectsButtonBounds.width) - (projectContainerBounds.width)) - (basketBarInner.left);
        var right = projectContainerBounds.width + left;

        if (left < 0)
        {
            left = 10;
        }

        var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        if ((viewportWidth < 780))
        {
            left = (basketBarInner.width - projectContainerBounds.width) / 2;
        }

        document.getElementById('tpx-projectlistcontents').style.left = left + 'px';
    }
}