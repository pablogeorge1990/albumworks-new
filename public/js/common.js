var mobile = false;
var stickyon = false;

$(document).ready(function(){
    var windowwidth = $(window).width();
    mobile = windowwidth < 959;

    $('form.webtolead').not('#splitchoice_form').attr('action','https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8');

    $('.tooltip').each(function(){
        var contents = $(this).html();
        $(this).html('<span>'+contents+'</span>');
        if(!mobile){
            $(this).tooltipster({
                maxWidth: 200,
                theme: ['tooltipster-aw', 'tooltipster-aw-customized'],
                contentAsHTML: true
            });
        }
    });

    $('#onlineoffline').change(function(){
        switch($(this).val()){
            case 'Offline':
                $('.productblock:first').parent().hide();
                $('.downloadhead').text('Download our Advanced Editor to get the full range of products');
                $('.onlineonly').hide();
                $('.offlineonly').show();
                break;

            case 'Online':
                $('.productblock:first').parent().show();
                $('.downloadhead').text('Or download our Advanced Editor to get the full range of products');
                $('.onlineonly').show();
                $('.offlineonly').hide();
                break;
        }
    });

    $('.hoverstuff li').hover(function(){
        var $li = $(this);
        var $parent = $li.closest('.hoverstuff');
        if(typeof $li.data('image') !== 'undefined'){
            $parent.find('.previewcol .image img').attr('src', $li.data('image'));
            $parent.find('.previewcol .image img').attr('alt', $li.text());
        }
        if(typeof $li.data('heading') !== 'undefined'){
            $parent.find('.previewcol .heading strong').html($li.data('heading'));
        }
        if(typeof $li.data('body') !== 'undefined'){
            $parent.find('.previewcol .body').html($li.data('body'));
        }
    });
    
    $('.noempties').submit(function(){
        var $form = $(this);
        var error = false;
        $form.find('.inputbox').each(function(){
            if(!$(this).hasClass('canempty') && $(this).val().trim() === '')
                error = true;
        });
        
        if(error){
            alert('Please ensure all fields have a value');
            return false;
        }
        
        if($form.hasClass('norobot')){
            $form.append('<input type="hidden" name="email__norobot" value="687eda030d9cd142552813104ec35be5" />');
        }
        
        if($form.find('.validate-email').length && !emailIsValid($form.find('.validate-email').val())){
            alert('Please check your email address and try again');
            return false;            
        }
    });
    
    /*
    $('.panel.hastext').each(function(){
        if($(this).find('a').length){
            $(this).click(function(){
                $(this).find('a').click();
            });
        }
    });
    */

    $('.parallax').parallax('50%', 0.5);
    
    $('.panel.hastext .text').each(function(){
        $block = $(this);
        $panel = $block.parent();
    });
    
    $('.persistent a').click(function(event){
        $a = $(this);                
        
        if($a.attr('href').substring(0,1) === '#'){
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($a.attr('href')).position().top + 1
            }, 500);
        }
    });

    $('.hoverchanger li img').hover(function(){
        var $img = $(this);
        var $viewport = $img.closest('.content').siblings('img');
        var bigsrc = $img.attr('big');
        $viewport.attr('src', bigsrc);
    });
    
    $('.dropdown').each(function(){
        var $dd = $(this);
        $dd.prev('a').click(function(event){
            event.preventDefault();
            
            var $a = $(this);
            if($dd.is(':visible'))
                close_dropdown($dd);
            else open_dropdown($dd);
        });
    });
    $('.dropdown > a').click(function(){
        close_dropdown($(this).closest('.dropdown'));
    });
    
	if(!mobile){
	    window.setTimeout(function() {
            $('.leftright .group > div, .vertmid').each(function () {
                var $this = $(this);
                $this.css('margin-top', ($this.parent().height() / 2 - $this.height() / 2) + 'px');
            });
        }, 1000);
	}

	$('.filtergallery').each(function(){
	    var $fg = $(this);
	    var $filter = $fg.find('.filter');
	    var $gallery = $fg.find('.gallery');
	    $gallery.find('li').each(function(){
	       var $li = $(this);
	       if($gallery.hasClass('debossgallery') || $gallery.hasClass('materialgallery')){
               $li.css('background-image', 'url('+$li.attr('hover')+')');
           }
	       else{
               $li.css('background-image', 'url('+$li.attr('hover')+')').css('background-image', 'url('+$li.attr('default')+')'); // preload
               $li.mouseenter(function(){
                   $li.css('background-image', 'url('+$li.attr('hover')+')');
               });
               $li.mouseleave(function(){
                   $li.css('background-image', 'url('+$li.attr('default')+')');
               });
           }
        });

        $filter.find('input').each(function(){
            $(this).change(function(){
                if($filter.find('input:checked').length <= 0){
                    $gallery.find('li').show();
                }
                else {
                    $gallery.find('li').hide();
                    $filter.find('input').each(function () {
                        if ($(this).is(':checked'))
                            $('.filter_' + $(this).val()).show();
                    });
                }
            });
        });
    });
    
    init_sliders(true);
    $('.slider').each(function(){
	   var $slider = $(this);
	   var $slideholder = $slider.find('.slideholder');
	   var $slides = $slideholder.find('.slide'); 
	   var $nava = $slider.find('nav a');
	  
	   $nava.each(function(i){
		  var $a = $(this); 
		  $a.click(function(){
		  	  var newpos = (i*$(window).width()*-1)+'px';
		  	  $slideholder.animate({ left: newpos }, 500);
		  	  $a.siblings().removeClass('active');
		  	  $a.addClass('active');
		  });
	   });
	   
	   $slider.find('nav a:first').click();
    });
    
    if(!mobile){
        $('.picker').each(function(){
		    var $picker = $(this);
		    $picker.find('nav a:first').click();
        });
    }
    
    /* ========================= RETAIL ========================== */
    $('.ecommerce').each(function(){
        var $e = $(this);
        var $images = $e.children('.images');
        var $others = $images.children('.others');
        
        $others.find('img').click(function(){
            $(this).closest('.images').find('.primary').attr('src', $(this).attr('src'));
        });
        
        $('.category').change(function(){
	        var products = $('.category option[value='+$('.category').val()+']').data('products');
	        $('.primaryfield').html('');
	        
			$.each(products, function(code,proddata){
				var extras = '';
				$.each(proddata, function(field,value){
					if(field != 'label')
						extras = extras + 'data-'+field+'="'+value+'" ';
				});
		        $('.primaryfield').append('<option value="'+code+'" '+extras+'>'+proddata.label+'</option>');	        				
			});
			$('.primaryfield').change();			
        });
        $('.primaryfield').change(function(){
            var val = $(this).val();
            $.each($(this).find('option[value='+val+']').data(), function(k,v){
	            if(k == 'newimage'){
					$('.images .others .dummy').remove();
					$('.images .others').append('<img class="dummy" src="'+v+'" onclick="$(\'.images .primary\').attr(\'src\', \''+v+'\');" />');
					$('.images .primary').attr('src', v);
	            }
                else if(k === 'url' && (complicated_urls === false)){
                    $e.find('.url_container .cta').attr('href', v);
                    $('#sitemenu > span > .cta').attr('href', v).attr('target', '_blank');
                }
                else if(k == 'image')
                    $('img[imgid='+v+']').click();
                else if(k != 'price') 
                	$e.find('.'+k+'_container .likeabox').text(v);
                    
                if($('.productspecific').length){
                    $('.productspecific').each(function(){
                        var thisid = $(this).attr('id');
                        if(thisid.indexOf('field_'+val+'_') === 0)
                            $(this).show().siblings('.productspecific').hide();
                    });
                }
            });
            calculate_ecommerce_price();
        });
        $('.category').change();        
        $('.primaryfield').change();
        
        $('select.extras').change(function(){
            calculate_ecommerce_price();	        
        });
        
        if(complicated_urls !== false && complicated_urls !== 'ignore'){
            $('.ecommerce select').change(function(){
                var parts = complicated_urls.split("|");
                var key = '';
                $.each(parts, function(i,field){
                    key = key+'|'+$('select[name='+field+']:visible option:selected').text();
                });
                key = key.substring(1);
                
                $e.find('.url_container .cta').attr('href', urls[key]);
                $('#sitemenu > span > .cta').attr('href', urls[key]).attr('target', '_blank');
            });
            $('.primaryfield').change();
        }
        
        $('.url_container .cta, #sitemenu > span > .cta').click(function(e){
            if(mobile && $('main.nomobileworkflow').length){
                e.preventDefault();
                launch_download_lightbox();
            } 
        });
        
        $others.find('img:first').click();
    });
    
    /* ========================= CALC ============================ */
    if($('#calculator').length){
        $('.radiooption').click(function(){
            var $this = $(this);
            var $radio = $this.find('input[type=radio]');
            $radio.prop("checked", true);
            
            $('input[name='+$radio.attr('name')+']').each(function(){
                $(this).closest('.radiooption').removeClass('active');
            });
            $this.addClass('active');
            
            if($radio.attr('name') === 'binding'){
                if($('input[name=binding]:checked').val() === 'mb'){
                    $('#paper').slideUp();
                    $('#paper .active').removeClass('active');
                    $('input[name=paper][value=silk]').prop('checked', true).closest('.radiooption').addClass('active');
                }
                else if($('input[name=binding]:checked').val() === 'lf'){
                    $('#type_hd').addClass('hidden');
                    $('#type_hdlf').addClass('hidden');
                    $('#type_sdlf').removeClass('hidden');
                    $('#type_sd').addClass('hidden');
                    $(".radioselecter .radiooption3:nth-child(n)").removeClass('active');
                    $(".radioselecter .radiooption3:nth-child(1)").addClass('active');
                    $('#field_paperscl').prop("checked", true);
                    $('#paper').slideDown();
                }
                else{
                    $('#type_hd').addClass('hidden');
                    $('#type_hdlf').addClass('hidden');
                    $('#type_sdlf').addClass('hidden');
                    $('#type_sd').removeClass('hidden');
                    $(".radioselecter .radiooption:nth-child(n)").removeClass('active');
                    $(".radioselecter .radiooption:nth-child(1)").addClass('active');
                    $('#field_papersilk').prop("checked", true);
                    $('#paper').slideDown();
                }
            }
            else if($radio.attr('name') === 'paperType'){
                if($radio.attr('value') === 'sd') {
                    if($('input[name=binding]:checked').val() === 'lf'){
                        $('#type_hd').addClass('hidden');
                        $('#type_hdlf').addClass('hidden');
                        $('#type_sdlf').removeClass('hidden');
                        $('#type_sd').addClass('hidden');
                        $(".radioselecter .radiooption3:nth-child(n)").removeClass('active');
                        $(".radioselecter .radiooption3:nth-child(1)").addClass('active');
                        $('#field_paperscl').prop("checked", true);
                    }
                    else {
                        $('#type_hd').addClass('hidden');
                        $('#type_hdlf').addClass('hidden');
                        $('#type_sdlf').addClass('hidden');
                        $('#type_sd').removeClass('hidden');
                        $(".radioselecter .radiooption:nth-child(n)").removeClass('active');
                        $(".radioselecter .radiooption:nth-child(1)").addClass('active');
                        $('#field_papersilk').prop("checked", true);
                    }
                }
                else if($radio.attr('value') === 'hd') {
                    if($('input[name=binding]:checked').val() === 'lf'){
                        $('#type_sd').addClass('hidden');
                        $('#type_hdlf').removeClass('hidden');
                        $('#type_sdlf').addClass('hidden');
                        $('#type_hd').addClass('hidden');
                        $(".radioselecter .radiooption4:nth-child(n)").removeClass('active');
                        $(".radioselecter .radiooption4:nth-child(1)").addClass('active');
                        $('#field_paperhcl').prop("checked", true);
                    }
                    else {
                        $('#type_sd').addClass('hidden');
                        $('#type_hdlf').addClass('hidden');
                        $('#type_sdlf').addClass('hidden');
                        $('#type_hd').removeClass('hidden');
                        $(".radioselecter .radiooption2:nth-child(n)").removeClass('active');
                        $(".radioselecter .radiooption2:nth-child(1)").addClass('active');
                        $('#field_paperlustre').prop("checked", true);
                    }
                }
            }
                        
            recalculate_prices();
        });

        $('.radiooption2').click(function(){
            var $this = $(this);
            var $radio = $this.find('input[type=radio]');
            $radio.prop("checked", true);

            $('input[name='+$radio.attr('name')+']').each(function(){
                $(this).closest('.radiooption2').removeClass('active');
            });
            $this.addClass('active');

            recalculate_prices();
        });

        $('.radiooption3').click(function(){
            var $this = $(this);
            var $radio = $this.find('input[type=radio]');
            $radio.prop("checked", true);

            $('input[name='+$radio.attr('name')+']').each(function(){
                $(this).closest('.radiooption3').removeClass('active');
            });
            $this.addClass('active');

            recalculate_prices();
        });

        $('.radiooption4').click(function(){
            var $this = $(this);
            var $radio = $this.find('input[type=radio]');
            $radio.prop("checked", true);

            $('input[name='+$radio.attr('name')+']').each(function(){
                $(this).closest('.radiooption4').removeClass('active');
            });
            $this.addClass('active');

            recalculate_prices();
        });
        
        $('.intonly').change(function(){
            var $this = $(this);
            var reg = /^\d+$/;
            
            if(!reg.test($this.val()) || parseInt($this.val()) === 0)
                $this.val($this.attr('min'));
            else if($this.val() > parseInt($this.attr('max')))
                $this.val($this.attr('max'));
            else if($this.val() < parseInt($this.attr('min')))
                $this.val($this.attr('min'));
            else if(parseInt($this.val()) % parseInt($this.attr('increments')))
                $this.val(parseInt($this.val())+1);
                
            recalculate_prices();
        });
        
        $('#pages_optionsslipcase').click(function(){
            if($(this).is(':checked')){
                $('#pages_optionspresbox').prop('checked', false);
                $('#pages_optionspresboxhs').prop('checked', false).attr('disabled', true);
                $('#pages_optionsslipcasehs').prop('checked', false).attr('disabled', false);
            }
            else{
                $('#pages_optionspresbox').prop('checked', false);
                $('#pages_optionspresboxhs').prop('checked', false).attr('disabled', true);
                $('#pages_optionsslipcasehs').prop('checked', false).attr('disabled', true);
            }
                    
            recalculate_prices();
        });
        
        $('#pages_optionspresbox').click(function(){
            if($(this).is(':checked')){
                $('#pages_optionsslipcase').prop('checked', false);
                $('#pages_optionsslipcasehs').prop('checked', false).attr('disabled', true);
                $('#pages_optionspresboxhs').prop('checked', false).attr('disabled', false);
            }
            else{
                $('#pages_optionsslipcase').prop('checked', false);
                $('#pages_optionsslipcasehs').prop('checked', false).attr('disabled', true);
                $('#pages_optionspresboxhs').prop('checked', false).attr('disabled', true);
            }
                        
            recalculate_prices();
        });
        
        $('#options input').not('#pages_optionsslipcase, #pages_optionspresbox').click(function(){
            recalculate_prices();
        });
        
        $('#poboxind').click(function(){
            recalculate_prices();
        });

        $('#products tbody td').click(function(){
            var $this = $(this);
            
            if(new_product_exists($this.parent(), $this)){
                $this.closest('table').find('.active').removeClass('active');
                $this.closest('table').find('.activechild').removeClass('activechild');
                
                $this.parent().addClass('activechild');
                $this.addClass('active');
                
                recalculate_prices();
            }
        });
        
        $('#products .default').click();
    }
    /* ============================================================= */

    var timer_id;
    $(window).resize(function() {
        onresize();
        /*
        clearTimeout(timer_id);
        timer_id = setTimeout(function() {
            onresize();
        }, 300);
        */
    });
    onresize();
    $(window).scroll();
    
    // animations
    /*
    $('.kenburns').each(function(){
        var $kb = $(this);
        var kbsw = parseInt($kb.attr('kbsw'));
        var kbsh = parseInt($kb.attr('kbsh'));
        var kbsr = kbsh / kbsw;
        
        if($kb.width() * kbsr > $kb.height())
            $kb.addClass('kenburns_animatew');
        else $kb.addClass('kenburns_animateh');
    });
    */
});

$(window).scroll(function (event) {    
    var scroll = $(window).scrollTop();

    if($('.persistent').length){
        if(scroll >= $('.persistent').position().top){
            $('.persistent').addClass('sticky');
            $('.dummypers').css('display', 'block');
        }
        
        if(scroll < $('.dummypers').position().top){
            $('.dummypers').css('display', 'none');
            $('.persistent').removeClass('sticky');
        }
                
        var current_panel = $('.persistent a:first').attr('href');
        $('.persistent a').each(function(){
            var butt = $(this).attr('href');            
            if(butt.substring(0,1) === '#' && $(butt).length){
                if(scroll >= $(butt).position().top){
                    current_panel = butt;
                }
            }
        });
        $('.persistent a').removeClass('active');
        $('.persistent a[href=\\'+current_panel+']').addClass('active');
    }
});    

function onresize(){
	var windowwidth = $(window).width();
	mobile = (windowwidth < 959);
	
    $('.slider').each(function(){
	   var $slider = $(this);
	   $slider.find('nav a:first').click();
	});
   init_sliders(false);
   
   $('.prodpanel').each(function(){
       var $panel = $(this);
       var imagearea = parseInt($panel.attr('imagearea'));
       var $text = $(this).find('.text');
       var leftimg = $panel.hasClass('leftimg');

       if(mobile){
           $text.css('width', '80%');
       }
       else{
           $text.css('width', '100%');
           $panel.css('background-size','auto 100%');
           var safearea = imagearea + $text.width();
           var position = windowwidth >= safearea ? 0 : (windowwidth - safearea);
           $panel.css('background-position', leftimg ? 'left '+position+'px bottom 0' : 'right '+position+'px bottom');
           
           var visibleimg = imagearea - Math.abs(position);
           var textspace = Math.round((windowwidth - visibleimg - $text.width())/2);           
           while(textspace <= 10){
               $text.width($text.width() - 1);
               textspace = Math.round((windowwidth - visibleimg - $text.width())/2);
           }
           $text.css(leftimg ? 'right' : 'left', textspace+'px');
       }       
   });
}

function calculate_ecommerce_price(){
    var price = $('.primaryfield').find('option[value='+$('.primaryfield').val()+']').data('price');
    $('.extras.productspecific:visible').each(function(){
        price += parseFloat($('#'+md5($(this).attr('id')+'_'+$(this).val())).data('price'));
    });
    $('.extras').not('.productspecific').each(function(){
		var $extra = $(this);
		if($extra.find('option[id='+md5($extra.val())+']').length){
			$.each($extra.find('option[id='+md5($extra.val())+']').data(), function(k,v){
				var bits = k.split("_");
				if($('select[name='+bits[0]+']').val() == bits[1])
					price += parseFloat(v);   
            });
		}
		else price += parseFloat($extra.val());
    });
	$('.price_container .likeabox').text('$'+price);
}

function open_dropdown($dd){
    var mobile = $(window).width() <= 850;

    if($dd.closest('nav').find('.dropdown:visible').length){
        $dd.closest('nav').find('.dropdown:visible').slideUp(function(){
            $dd.slideDown(function(){
                if(mobile) {
                    $('#sitemenu').css('overflow-y', 'hidden');
                }
            });
        });
    }
    else $dd.slideDown(function(){
        if(mobile) {
            $('#sitemenu').css('overflow-y', 'hidden');
        }
    });

    add_ddmask();
}

function close_dropdown($dd){
    $dd.slideUp(function(){
        if(mobile) {
            $('#sitemenu').css('overflow-y', 'scroll');
        }
    });

    if(!$('#hamburger').hasClass('open'))
        remove_ddmask();
}

function init_sliders(firsttime){
	var windowwidth = $(window).width();
    $('.slider').each(function(){
	   var $slider = $(this);
       var $slidewindow = $slider.find('.slidewindow');
	   var $slideholder = $slider.find('.slideholder');
	   var $slides = $slideholder.find('.slide'); 
	   
	   $slideholder.width($slides.length * windowwidth);
	   
       var maxheight = 0;
	   $slides.each(function(){
		  var $slide = $(this);
		  var $content = $slide.find('.content');
		  $slide.width(windowwidth); 
          
          if($slide.height() > maxheight)
            maxheight = $slide.height();
	   });
       
       if(maxheight){
           if($slider.parent().hasClass('vidslider'))
              $slidewindow.height(mobile ? maxheight + 20 : 655);
       }
	   
	    if(firsttime){
		    $slider.append('<a href="javascript:void(0)" class="arrow leftarrow" onclick="slide_left(this)">left</a><a href="javascript:void(0)" class="arrow rightarrow" onclick="slide_right(this)">right</a>');
	    }
    });		
}

function picker_switch(switchto, btn){
	var $btn = $(btn);
	var $picker = $btn.closest('.picker');	
	
	$('#'+switchto).siblings().hide();
	$('#'+switchto).show();
	
	$btn.siblings().removeClass('active');
	$btn.addClass('active');
}

function slide_left(btn){
	var $btn = $(btn);
	var $slider = $btn.closest('.slider');
	var $nextbtn = $slider.find('nav a:nth-child('+($slider.find('nav a.active').index())+')');
	if($nextbtn.length)
		$nextbtn.click();
	else $slider.find('nav a:last').click();
}

function slide_right(btn){
	var $btn = $(btn);
	var $slider = $btn.closest('.slider');
	var $nextbtn = $slider.find('nav a:nth-child('+($slider.find('nav a.active').index() + 2)+')');
	if($nextbtn.length)
		$nextbtn.click();
	else $slider.find('nav a:first').click();
}

function selector_select(btn, imgsrc, price){
	var $btn = $(btn);
	var $selector = $btn.closest('.selector');
	var $clickers = $btn.closest('.clickers');
	var $price = $selector.find('.price');
	
	$selector.find('.visual').attr('src', imgsrc);
	$clickers.find('a').removeClass('active');
	$btn.addClass('active');
	
	$price.text(price);
}

function get_product_data(tr, td){
    var error = false;

    var binding = $('input[name=binding]:checked').val();
    eval('if(typeof(__pricing.'+binding+') === \'undefined\') error = true;');
    if(error) return false;
    
    var paperquality = $('input[name=paper]:checked').val();
    eval('if(typeof(__pricing.'+binding+'.'+paperquality+') === \'undefined\') error = true;');
    if(error) return false;
    
    var cover = $('#prodheadings th:eq('+(td.index() - 1)+')').attr('code');
    eval('if(typeof(__pricing.'+binding+'.'+paperquality+'.'+cover+') === \'undefined\') error = true;');
    if(error) return false;
    
    var product = tr.attr('id');    
    eval('if(typeof(__pricing.'+binding+'.'+paperquality+'.'+cover+'.'+product+') === \'undefined\') error = true;');
    if(error) return false;
    
    eval('var data = __pricing.'+binding+'.'+paperquality+'.'+cover+'.'+product+';');
    
    return data;
}

function get_cover(td){
    return $('#prodheadings th:eq('+(td.index() - 1)+') label').text();
}

function new_product_exists(newtr, newtd){
    var test = get_product_data(newtr, newtd);
    return (test !== false);
}

function this_product_doesnt_exist(){
    var test = get_product_data($('#products .activechild'), $('#products .active'));
    return (test === false);
}

function set_current_product(){
    __currentprod = get_product_data($('#products .activechild'), $('#products .active'));
    if(__currentprod !== false)
        __currentcover = get_cover($('#products .active'));
}

function splitchoice(onlinelink){
    if(onlinelink == 'popular')
        onlinelink = 'popular-'+(mobile ? 'mobile' : 'desktop');

    if(mobile){
        window.location = onlinelink;
    }
    else {
        $('#splittheatre').width($(document).width()).height($(document).height()).fadeIn();
        $('#splittheatre').click(function () {
            hide_splitchoice();
        });
        show_splitchoice(onlinelink);
    }
}

function hide_splitchoice(){
    $('.lightbox, #splittheatre').hide();
    $('#splitchoice').hide();
}

function show_splitchoice(onlinelink){
    if(typeof gtag !== 'undefined') {
        gtag('event', 'Open', {
            'event_category': 'Download Form',
            'event_label': 'Opened from "'+current_page+'"'
        });
    }

    $('#splitchoice #splitaltcta').attr('href', onlinelink);
    $('#splitchoice')
        .css('left', (($(window).width()/2)-(($('#splitchoice').width()+20)/2))+'px')
        .css('top', ($(window).scrollTop()+20)+'px')
        .fadeIn();
}

function pbsplitchoice(onlinelink){
    if(mobile){
        window.location = onlinelink;
    }
    else {
        $('#splittheatre').width($(document).width()).height($(document).height()).fadeIn();
        $('#splittheatre').click(function () {
            hide_pbsplitchoice();
            $(this).unbind('click');
        });
        show_pbsplitchoice(onlinelink);
    }
}

function hide_pbsplitchoice(){
    $('.lightbox, #splittheatre').hide();
    $('#pbsplitchoice').hide();
}

function show_pbsplitchoice(onlinelink){
    if(typeof gtag !== 'undefined') {
        gtag('event', 'Open', {
            'event_category': 'Download Form',
            'event_label': 'Opened from "'+current_page+'"'
        });
    }

    $('#pbsplitaltcta').attr('href', onlinelink);
    $('#pbsplitchoice')
        .css('left', (($(document).width()/2)-(($('#pbsplitchoice').width()+20)/2))+'px')
        .css('top', ($(window).scrollTop()+20)+'px')
        .fadeIn();
}

function checkscform(form){
    var $form = $(form);

     if(($form.find('input[name=email]').val() !== '') && !emailIsValid($form.find('input[name=email]').val())){
         alert('Please enter a valid email address and try again.')
         return false;
     }

    var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
    $form.find('input[name=00N3600000Los6F]').val(os_select);
    document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";

    var email = $form.find('input[name=email]').val();
    var name = ($form.find('input[name=first_name]').val() === '') ? email : $form.find('input[name=first_name]').val();
    var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
    if(email !== '')
        document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";
    else document.cookie = "download_params=vendor:ap|ou:0; ; path=/; domain=.albumworks.com.au";
    
    var iscookie = readCookie('download_params');
    if(iscookie)
        $form.find("input[00N3600000LosKl]").val(0);

    var refpromo = readCookie('APrefpromo');
    if(refpromo)
        $form.find("input[00N3600000LosAC]").val(refpromo);
    refpromo = $form.find('input[00N3600000LosAC]').val();
    document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";

    if(typeof gtag !== 'undefined') {
        gtag('config', 'UA-1338422-2', {'page_path': '/download_start'});
    }

    localStorage.setItem('DL_THANKYOU_NAME', name);
    localStorage.setItem('DL_EMAIL_ADDRESS', email);
    localStorage.setItem('DL_EMAIL_OPTION', option_mail);

    return true;
}

var __toggling = false;
function toggleburger(){
    var $burger = $('#hamburger');
    var open = $burger.hasClass('open');

    if(!__toggling) {
        __toggling = true;
        if (open) {
            close_dropdown($('.dropdown:visible'))

            $burger.removeClass('open');
            $('#sitemenu').animate({
                left: '-250px'
            }, 400, function(){ __toggling = false; $('#sitemenu').css('left', ''); });

            remove_ddmask();
        }
        else {
            $burger.addClass('open');
            $('#sitemenu').animate({
                left: '0'
            }, 400, function(){ __toggling = false; });

            add_ddmask();
        }
    }
}

function add_ddmask(){
    if($('#ddmask').length === 0){
        $('body').append('<div id="ddmask" onclick="close_dropdown($(\'.dropdown:visible\'))">&nbsp;</div>');
        $('#ddmask').fadeIn();
    }
}

function remove_ddmask(){
    if($('#ddmask').length > 0){
        $('#ddmask').fadeOut(function(){
            $('#ddmask').remove();
        });
    }
}

function recalculate_prices(){
    // if the current choices don't all work, choose another similar product
    if(this_product_doesnt_exist()){
        var success = false;
        $('#prodvals .activechild td').each(function(){
            if(!success){
                var $td = $(this);
                if(new_product_exists($td.parent(), $td)){
                    $td.click();
                    success = true;
                }
            }
        });
        
        // couldnt find a product of the same prodcode so lets just click whatever we can find
        if(!success){
            $('#prodvals td').each(function(){
                if(!success){
                    var $td = $(this);
                    if(new_product_exists($td.parent(), $td)){
                        $td.click();
                        success = true;
                    }
                }
            });            
        }
        
        // this should never happen
        if(!success){
            alert('Error');
        }
    }
    
    set_current_product();
    set_product_options();
    
    // set product prices
    $('#prodvals td').each(function(){
        var $td = $(this);
        var thisprod = get_product_data($td.parent(), $td);        
        if(thisprod !== false){
            $td.text(curr(thisprod.baseprice, false));
        }
        else $td.text('-');
    });
    
    // set extra pages price
    if(!mobile)
        $('#extrapagesind').tooltipster('content', $('#extrapagesind').attr('template').replace('{PRICE}', curr(__currentprod.extrapageprice, false)));
    extrapagesprice = Math.max(0, (parseInt($('#field_pagecount').val()) - parseInt(__currentprod.included))) * parseFloat(__currentprod.extrapageprice);
    $('#extrapageprice').text(curr(extrapagesprice, true));
    
    // set optional extra prices
    $('#container_pages_optionsslipcase > span').text(curr(__currentprod.scprice, true));
    $('#container_pages_optionsslipcasehs > span').text(curr(__currentprod.schsprice, true));
    $('#container_pages_optionspresbox > span').text(curr(__currentprod.pbprice, true));
    $('#container_pages_optionspresboxhs > span').text(curr(__currentprod.pbhsprice, true));
    $('#container_pages_optionshs > span').text(curr(__currentprod.hsprice, true));
    $('#container_pages_optionsvip > span').text(curr('29.95', true));
    
    // set details prices
    var subtotal = 0;
    $('#details li').remove();
    $('#details ul').append('<li>&#8226; '+($('#binding .active label span').text())+'</li>');
    $('#details ul').append('<li><span>'+curr(__currentprod.baseprice, false)+'</span> '+$('#products .activechild').attr('productname')+' '+__currentcover+'</li>');
    subtotal += decurr(__currentprod.baseprice);
    
    if($('input[name=binding]:checked').attr('papertype') !== '')
        $('#details ul').append('<li><span>'+curr(extrapagesprice, true)+'</span> '+$('input[name=binding]:checked').attr('papertype')+' Extra Pages</li>');
    else $('#details ul').append('<li><span>'+curr(extrapagesprice, true)+'</span> '+' '+($('#paper .active:visible label span').text())+' Extra Pages</li>');
    subtotal += decurr(extrapagesprice);
    
    $('#options p:visible input').each(function(){
        var $option = $(this);
        if($option.prop('checked')){
            $('#details ul').append('<li><span>'+$option.next().text()+'</span> '+$option.prev().text()+'</li>');        
            subtotal += decurr($option.next().text());
        }
    });

    // set subtotal price
    $('#subtotal').text(curr(subtotal, false));
    
    // set shipping price
    /*
    if(!mobile)
        $('#shippingind').tooltipster('content', $('#shippingind').attr('template').replace('{FIRST}', curr(__currentprod.shippingfirstprice, false)).replace('{ADDITIONAL}', curr(__currentprod.shippingaddtlprice, false)));
       */
    var shipprice = parseFloat(__currentprod.shippingfirstprice) + ((parseInt($('#field_quantity').val()) - 1) * parseFloat(__currentprod.shippingaddtlprice));
    if($('#poboxind').prop('checked'))
        shipprice += 5;
    $('#shipping').text('+'+curr(shipprice, false));

    // set total subtracting discount
    $('#total').text(curr(((subtotal * parseInt($('#field_quantity').val())) + shipprice) - parseFloat(decurr($('#discount').text()))));
    
    // now re-define the discount
    $('#discount').text(curr(0, false));
    $('#total').text(curr(((subtotal * parseInt($('#field_quantity').val())) + shipprice) - parseFloat(decurr($('#discount').text()))));
    validate_promocode(false);
}

function set_product_options(){
    // set page count field minmax
    var reg = /^\d+$/;    
    $('#field_pagecount').attr('min', __currentprod.min).attr('max', __currentprod.max);
    if(!reg.test($('#field_pagecount').val()))
        $('#field_pagecount').val(__currentprod.included);
    else if(parseInt($('#field_pagecount').val()) > __currentprod.max)
        $('#field_pagecount').val(__currentprod.max);
    else if(parseInt($('#field_pagecount').val()) < __currentprod.min)
        $('#field_pagecount').val(__currentprod.min);
    
    // set included
    $('#papinc').text(__currentprod.included);
    
    // set min
    $('#papmin').text(__currentprod.min);
    
    // set max
    $('#papmax').text(__currentprod.max);
    
    // set optional extras
    if((typeof(__currentprod.scprice) !== 'undefined') && (__currentprod.scprice !== '')){
        $('#container_pages_optionsslipcase').show();
        
        if((typeof(__currentprod.schsprice) !== 'undefined') && (__currentprod.schsprice !== ''))
            $('#container_pages_optionsslipcasehs').show();
        else $('#container_pages_optionsslipcasehs').hide();
        
        if(!$('#pages_optionsslipcase').prop('checked'))
            $('#pages_optionsslipcasehs').attr('disabled', true);
    }
    else{
        $('#container_pages_optionsslipcase').hide();
        $('#container_pages_optionsslipcasehs').hide();
    }    
    if((typeof(__currentprod.pbprice) !== 'undefined') && (__currentprod.pbprice !== '')){
        $('#container_pages_optionspresbox').show();
        
        if((typeof(__currentprod.pbhsprice) !== 'undefined') && (__currentprod.pbhsprice !== ''))
            $('#container_pages_optionspresboxhs').show();
        else $('#container_pages_optionspresboxhs').hide();
        
        if(!$('#pages_optionspresbox').prop('checked'))
            $('#pages_optionspresboxhs').attr('disabled', true);
    }
    else{
        $('#container_pages_optionspresbox').hide();
        $('#container_pages_optionspresboxhs').hide();
    }
    if((typeof(__currentprod.hsprice) !== 'undefined') && (__currentprod.hsprice !== ''))
        $('#container_pages_optionshs').show();
    else $('#container_pages_optionshs').hide();
    
    // untick optional extras that are now hidden
    $('#options p:hidden input').prop('checked', false);
}

function validate_promocode(recalc){
	if($('#field_promocode').val().trim() != ''){
		$('#promoerror').hide();
		$('#field_promocode').attr("disabled", true);
		$('#promocode_button').attr("disabled", true).html('<img src="img/ajax-loader.gif" />');
		
        $.ajax({
          type: "POST",
          url: 'endpoints/getdiscount.php',
          data: {
              promocode: $('#field_promocode').val().trim(),
              prodcode: __currentprod.prodcode,
              papercode: __currentprod.papercode,
              covertype: __currentcover,
              prodtype: $('#products .activechild').attr('id').replace('p', ''),
              pagecount: parseInt($('#field_pagecount').val()),
              slipcase: $('#pages_optionsslipcase').prop('checked'),
              slipcasehs: $('#pages_optionsslipcasehs').prop('checked'),
              presbox: $('#pages_optionspresbox').prop('checked'),
              presboxhs: $('#pages_optionspresboxhs').prop('checked'),
              hotstamping: $('#pages_optionshs').prop('checked'),
              vip: $('#pages_optionsvip').prop('checked'),
              subtotal: parseFloat(decurr($('#subtotal').text())),
              quantity: parseInt($('#field_quantity').val()),
              shipping: parseFloat(decurr($('#shipping').text())),
              total: parseFloat(decurr($('#total').text())),
              thisprod: __currentprod,
          },
          success: function(discountval){
            // test if promocode is valid
            if(discountval > 0){ // if it is, hide field and button, show discount amount with [x]
                $('#discount').text(curr(discountval, false));
                $('#promocodefield').hide();
                $('#promocoderesult').show();
                if(recalc)
                    recalculate_prices();
                else $('#total').text(curr(parseFloat(decurr($('#subtotal').text())) * parseInt($('#field_quantity').val()) + parseFloat(decurr($('#shipping').text())) - parseFloat(decurr($('#discount').text()))));
            }
            else{ // if it is not, put error underneath
                $('#discount').text(curr(0, false));
                $('#promoerror').fadeIn();
                if(recalc)
                    recalculate_prices();
                else $('#total').text(curr(parseFloat(decurr($('#subtotal').text())) * parseInt($('#field_quantity').val()) + parseFloat(decurr($('#shipping').text())) - parseFloat(decurr($('#discount').text()))));
            }              
          },
          complete: function(){
              $('#field_promocode').attr("disabled", false);
              $('#promocode_button').attr("disabled", false).html('+');
          },
          dataType: 'json'
        });
	}
}

function edit_promocode(){
	$('#promocoderesult').hide();
	$('#promocodefield').show();
	$('#promoerror').hide();
	$('#field_promocode').val('');
	$('#discount').text(curr(0, false));
	recalculate_prices();
}

function try_promocode(){
	$('#promoerror').hide(); 
	$('#field_promocode').val(''); 
	$('#field_promocode').focus();
}

function curr(value, addplus){
    if(value === '')
        return '-';        
	return (addplus ? '+' : '')+'$'+parseFloat(value).toFixed(2);
}

function decurr(value){
    if(typeof(value) === 'string')
        return parseFloat(value.replace('$','').replace('+','').replace(',',''));
    return value;
}

function checkdlform(form){
    var $form = $(form);
    var errors = false;
    $form.find('.inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });
    
    // if(errors){
    //     alert('Please ensure all fields are complete and try again.');
    //     return false;
    // }

    // if(!nameIsValid($form.find('input[name=first_name]').val())){
    //     alert('Please enter a valid name and try again.')
    //     return false;
    // }
    
    // if(!emailIsValid($form.find('input[name=email]').val())){
    //     alert('Please enter a valid email address and try again.')
    //     return false;
    // }
    
    var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
    $form.find('input[name=00N3600000Los6F]').val(os_select);
    document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";    
    
    var email = $form.find('input[name=email]').val();
    var name = ($form.find('input[name=first_name]').val() === '') ? email : $form.find('input[name=first_name]').val();
    var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
    if(email !== '')
        document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";

    var iscookie = readCookie('download_params');
    if(iscookie)
        $form.find("input[00N3600000LosKl]").val(0);

    var refpromo = readCookie('APrefpromo');
    if(refpromo)
        $form.find("input[00N3600000LosAC]").val(refpromo);
    refpromo = $form.find('input[00N3600000LosAC]').val();
    document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";

    if(typeof gtag !== 'undefined') { gtag('config', 'UA-1338422-2', {'page_path': '/download_start'}); }
    
    localStorage.setItem('DL_THANKYOU_NAME', name);
    
    return true;
}

function checkmobileform(form){
    var $form = $(form);
    var errors = false;
    $form.find('.inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });
    
    // if(errors){
    //     alert('Please ensure all fields are complete and try again.');
    //     return false;
    // }
    
    // if(!emailIsValid($form.find('input[name=email]').val())){
    //     alert('Please enter a valid email address and try again.')
    //     return false;
    // }
    // else if($form.find('#situfield_lightbox_mobile_first_name').length) {
    //     $('#situfield_lightbox_mobile_first_name').val($form.find('input[name=email]').val());
    // }
    
    var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
    $form.find('input[name=00N3600000Los6F]').val(os_select);
    document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";    
    
    // var name = $form.find('input[name=first_name]').val();
    // var email = $form.find('input[name=email]').val();    
    // var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
    // document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";
    document.cookie = "download_params=vendor:ap|ou:0; ; path=/; domain=.albumworks.com.au";

    var iscookie = readCookie('download_params');
    if(iscookie)
        $form.find("input[00N3600000LosKl]").val(0);

    var refpromo = readCookie('APrefpromo');
    if(refpromo)
        $form.find("input[00N3600000LosAC]").val(refpromo);
    refpromo = $form.find('input[00N3600000LosAC]').val();
    document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";
    
    var product = 'albumworks Product';
    var link = 'https://www.albumworks.com.au/download';
    if($('main.retail').length){
        product = $('h1:first').text();
        link = window.location.href;
    }
    $form.find('.mobreturl').attr('value', $form.find('.mobreturl').attr('value')+'&ret='+urlencode('https://www.albumworks.com.au/endpoints/mailform.php?mobileform&email__norobot=687eda030d9cd142552813104ec35be5&product='+urlencode(product)+'&link='+urlencode(link)+'&email__to='+email+'&email__returl=thankyou-mobile'));
    
    localStorage.setItem('DL_THANKYOU_NAME', name);

    return true;
}

function checkmlform(form){
    var $form = $(form);
    var errors = false;
    $form.find('.inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });
    
    // if(errors){
    //     alert('Please ensure all fields are complete and try again.');
    //     return false;
    // }
    
    // if(!emailIsValid($form.find('input[name=email]').val())){
    //     alert('Please enter a valid email address and try again.')
    //     return false;
    // }
    
    var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
    $form.find('input[name=00N3600000Los6F]').val(os_select);
    document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";    
    
    // var name = $form.find('input[name=first_name]').val();
    // var email = $form.find('input[name=email]').val();    
    // var option_mail = 0;
    // document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";
    document.cookie = "download_params=vendor:ap|ou:0; ; path=/; domain=.albumworks.com.au";
    
    var iscookie = readCookie('download_params');
    if(iscookie)
        $form.find("input[00N3600000LosKl]").val(0);

    var refpromo = readCookie('APrefpromo');
    if(refpromo)
        $form.find("input[00N3600000LosAC]").val(refpromo);
    refpromo = $form.find('input[00N3600000LosAC]').val();
    document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";
    
    if(typeof gtag !== 'undefined') { gtag('config', 'UA-1338422-2', {'page_path': '/download_start'}); }
    
    localStorage.setItem('DL_THANKYOU_NAME', name);
    
    return true;
}

function emailIsValid(inputvalue){    
    var pattern = /^(([^<>()[\]\\.,;:\s@\"#]+(\.[^<>()[\]\\.,;:\s@\"#]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return pattern.test(inputvalue);
}

function nameIsValid(inputvalue){
    return /^[A-Za-z\s]+$/.test(inputvalue);
}

function launch_popup_lightbox(){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });                    
    $('#lightbox_mailinglist')
        .css('left', (($(document).width()/2)-(($('#lightbox_mailinglist').width()+20)/2))+'px')
        .css('top', $(window).scrollTop()+20+'px')
    .fadeIn();
}

function launch_download_lightbox_or_create_now_on_mobile(createnowpath){
    if(mobile)
        return window.location = createnowpath;
    else return launch_download_lightbox();
}
function launch_download_lightbox(){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });                    
    show_downloadform();
}
function hide_hoverform(){
    $('.lightbox, #theatre').hide();
    $('#lightboxiframehover').fadeOut(function(){
        $('#lightboxiframehover').remove();
        $('#mobilecta').remove();
    });    
    $('#lightboximghover').fadeOut(function(){
        $('#lightboximghover').remove();
        $('#mobilecta').remove();
    });  
}

function show_downloadform(){
    if(mobile){
        if(typeof gtag !== 'undefined') {
            gtag('event', 'Open', {
                'event_category': 'Mobile Form',
                'event_label': 'Opened from "'+current_page+'"'
            });
        }

        $('#lightbox_mobile')
            .css('left', 'calc(5% - 20px)')
            .css('top', $(window).scrollTop()+'px')
        .fadeIn();        
    }
    else{
        if(typeof gtag !== 'undefined') {
            gtag('event', 'Open', {
                'event_category': 'Download Form',
                'event_label': 'Opened from "'+current_page+'"'
            });
        }

        $('#lightbox_download')
            .css('left', (($(document).width()/2)-(($('#lightbox_download').width()+20)/2))+'px')
            .css('top', '20px')
        .fadeIn();
    }
}

function lightbox_video(url){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });                        
    
    $('body').append('<div class="lightbox" id="lightboxiframehover"><iframe allowtransparency="true" src="'+url+'" width="100%" height="100%" scrolling="auto" align="top" frameborder="0" allowfullscreen></iframe>');
    $('#lightboxiframehover').css('left', (($(document).width()/2)-(($('#lightboxiframehover').width()+20)/2))+'px').css('top', (($(window).height()/2)-(($('#lightboxiframehover').height()+20)/2))+'px').fadeIn();
}

function lightbox_image(url, label, width, height){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(event){
        event.preventBubble=true
        hide_hoverform();
    });                        
    
    $('body').append('<div class="lightbox" id="lightboximghover"><img src="'+url+'"/><p>'+label+'</p></div>');
    $('#lightboximghover').css('left', (($(window).width()/2)-((width+20)/2))+'px').css('top', (($(window).height()/2)-((height+50)/2))+'px').fadeIn();
}

function lightbox_mobilecta(){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });                        
    
    $('body').append('\
        <div class="lightbox" id="mobilecta" style="text-align:center">\
            <a href="javascript:void(0)" class="close" onclick="$(\'#theatre\').click();">Close</a>\
            <p><img src="'+BASEPATH+'img/popup/aw.jpg" style="width:80%" /></p>\
            <p class="header">I want to...</p>\
            <p><a class="cta" href="javascript:void(0)" onclick="lightbox_mobilecta2()">MAKE A PHOTO BOOK</a></p>\
            <p><a class="cta" href="'+BASEPATH+'all-products">MAKE ANOTHER PRODUCT</a></p>\
            <p><a style="font-size:16px" href="javascript:void(0)" onclick="$(\'#theatre\').click();"><img style="margin-right:5px;" src="'+BASEPATH+'img/popup/x.jpg" />Close to browse</a></p>\
            ');
    var topval = (($(window).height()/2)-(($('#mobilecta').height())/2)) < 20 ? 20 : (($(window).height()/2)-(($('#mobilecta').height())/2));
    $('#mobilecta').css('left', (($(document).width()/2)-(($('#mobilecta').width()+20)/2))+'px').css('top', topval+'px').fadeIn();
}

function lightbox_mobilecta2(){
    if(!$('#theatre').is(':visible')){
        $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
        $('#theatre').click(function(){
            hide_hoverform();
        });                        
    }
    
    $('#mobilecta').remove();
    $('body').append('\
        <div class="lightbox" id="mobilecta" style="text-align:center">\
            <a href="javascript:void(0)" class="close" onclick="$(\'#theatre\').click();">Close</a>\
            <p class="header">Want to make a Photo Book?</p>\
            <p><img src="'+BASEPATH+'img/popup/p1.jpg" width="80%" /><br>Go to our website on a desktop computer or laptop.</p>\
            <p><img src="'+BASEPATH+'img/popup/p2.jpg" width="80%" /><br>Click on GET STARTED NOW</p>\
            <p><img src="'+BASEPATH+'img/popup/p3.jpg" width="80%" /><br>Download our easy to use Editor.</p>\
            <hr/>\
            <p class="header">Want to</p>\
            <p><a class="cta" href="'+BASEPATH+'buy-now-make-later">BUY NOW, MAKE LATER?</a></p>\
            <hr/>\
            <p class="header">Want to make another product using mobile or tablet?</p>\
            <p><a class="cta" href="'+BASEPATH+'all-products">ALL PRODUCTS</a></p>\
            <p><a style="font-size:16px" href="javascript:void(0)" onclick="$(\'#theatre\').click();"><img style="margin-right:5px;" src="'+BASEPATH+'img/popup/x.jpg" />Close to browse</a></p>\
            ');
    var topval = 20;//(($(window).height()/2)-(($('#mobilecta').height())/2)) < 20 ? 20 : (($(window).height()/2)-(($('#mobilecta').height())/2));
    $('#mobilecta').css('left', (($(document).width()/2)-(($('#mobilecta').width()+20)/2))+'px').css('top', topval+'px').fadeIn();
}

function validaterewards(){
    var errors = false;
    $('#rewardForm .inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });
    
    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }
    
    if(!emailIsValid($('#rewardForm #contact_email').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }    

    window.location = BASEPATH+'mybalance?user='+$('#rewardForm #contact_email').val();
    return false;
}

function validateGvForm() {
    var fields = ['gv_first_name','gv_email','gv_giftvoucher'];
    var val = '';
    var fail = false;
    $.each(fields, function() {
        if($("#" + this).val() == '') {
            fail = true;
        }
    });
    if(fail == true) {
        alert('Please fill in all fields and try again');
        return false;
    }
    if(!emailIsValid($('#gv_email').val())){
        alert('Please check your email address and try again');
        return false;
    }
    return true;
}

function md5 ( str ) {

    var RotateLeft = function(lValue, iShiftBits) {
            return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
        };

    var AddUnsigned = function(lX,lY) {
            var lX4,lY4,lX8,lY8,lResult;
            lX8 = (lX & 0x80000000);
            lY8 = (lY & 0x80000000);
            lX4 = (lX & 0x40000000);
            lY4 = (lY & 0x40000000);
            lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
            if (lX4 & lY4) {
                return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
            }
            if (lX4 | lY4) {
                if (lResult & 0x40000000) {
                    return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                } else {
                    return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                }
            } else {
                return (lResult ^ lX8 ^ lY8);
            }
        };

    var F = function(x,y,z) { return (x & y) | ((~x) & z); };
    var G = function(x,y,z) { return (x & z) | (y & (~z)); };
    var H = function(x,y,z) { return (x ^ y ^ z); };
    var I = function(x,y,z) { return (y ^ (x | (~z))); };

    var FF = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var GG = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var HH = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var II = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var ConvertToWordArray = function(str) {
            var lWordCount;
            var lMessageLength = str.length;
            var lNumberOfWords_temp1=lMessageLength + 8;
            var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
            var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
            var lWordArray=Array(lNumberOfWords-1);
            var lBytePosition = 0;
            var lByteCount = 0;
            while ( lByteCount < lMessageLength ) {
                lWordCount = (lByteCount-(lByteCount % 4))/4;
                lBytePosition = (lByteCount % 4)*8;
                lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount)<<lBytePosition));
                lByteCount++;
            }
            lWordCount = (lByteCount-(lByteCount % 4))/4;
            lBytePosition = (lByteCount % 4)*8;
            lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
            lWordArray[lNumberOfWords-2] = lMessageLength<<3;
            lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
            return lWordArray;
        };

    var WordToHex = function(lValue) {
            var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
            for (lCount = 0;lCount<=3;lCount++) {
                lByte = (lValue>>>(lCount*8)) & 255;
                WordToHexValue_temp = "0" + lByte.toString(16);
                WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
            }
            return WordToHexValue;
        };

    var x=Array();
    var k,AA,BB,CC,DD,a,b,c,d;
    var S11=7, S12=12, S13=17, S14=22;
    var S21=5, S22=9 , S23=14, S24=20;
    var S31=4, S32=11, S33=16, S34=23;
    var S41=6, S42=10, S43=15, S44=21;

//    str = this.utf8_encode(str);
    x = ConvertToWordArray(str);
    a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

    for (k=0;k<x.length;k+=16) {
        AA=a; BB=b; CC=c; DD=d;
        a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
        d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
        c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
        b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
        a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
        d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
        c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
        b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
        a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
        d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
        c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
        b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
        a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
        d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
        c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
        b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
        a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
        d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
        c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
        b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
        a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
        d=GG(d,a,b,c,x[k+10],S22,0x2441453);
        c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
        b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
        a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
        d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
        c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
        b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
        a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
        d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
        c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
        b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
        a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
        d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
        c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
        b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
        a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
        d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
        c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
        b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
        a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
        d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
        c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
        b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
        a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
        d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
        c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
        b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
        a=II(a,b,c,d,x[k+0], S41,0xF4292244);
        d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
        c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
        b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
        a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
        d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
        c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
        b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
        a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
        d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
        c=II(c,d,a,b,x[k+6], S43,0xA3014314);
        b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
        a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
        d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
        c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
        b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
        a=AddUnsigned(a,AA);
        b=AddUnsigned(b,BB);
        c=AddUnsigned(c,CC);
        d=AddUnsigned(d,DD);
    }

    var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

    return temp.toLowerCase();
}

(function(){var a={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(a){for(var b=0;b<a.length;b++){var c=a[b].string;var d=a[b].prop;this.versionSearchString=a[b].versionSearch||a[b].identity;if(c){if(c.indexOf(a[b].subString)!=-1)return a[b].identity}else if(d)return a[b].identity}},searchVersion:function(a){var b=a.indexOf(this.versionSearchString);if(b==-1)return;return parseFloat(a.substring(b+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};a.init();window.jQuery.client={os:a.OS,browser:a.browser}})();

function urlencode (str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
    replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function start_editor(url){
    gtag('config', 'UA-1338422-2', {'page_path': '/editor-start'});
    window.setTimeout(function(){
        window.location = url;
    }, 500);
}

function pb_start_editor(url){
    gtag('config', 'UA-1338422-2', {'page_path': '/editor-start'});
    pbsplitchoice(url);
}