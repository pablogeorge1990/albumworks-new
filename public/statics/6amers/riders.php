<? include('header.php'); ?>

    <h1>Riders</h1>
    
    <h2>Paul Devereux</h2>
    <p>Vying for elder statesman status, Paul joined the 6amers in the early days and quickly learnt what "fast on the flat and fit in the ferns" was all about! Although the group's tagline has changed to "no dimmer switch on commitment", the challenge of riding through the hills remains.....and there will be plenty of them on this forthcoming ride.</p>
    <p>Always a sports fanatic, Paul has always enjoyed a challenge, whether its running a marathon, tackling endurance swims, or competing in sailing regattas or cycling events and so it was a quick "yes" to come on board for this ride for beyondblue. </p>
    <p>Workwise, his career in finance oriented roles has taken him from large public companies, both domestically and internationally, to the world of private equity and then to advising and consulting. To leverage his experience and background, he has most recently co founded a practice commercialising and accelerating mid stage start-ups. He has a most understanding wife and three kids....and two pups!</p>
 
    <h2>Peter Dikschei</h2>
    <p>At almost 54, Peter is the current elder statesman of the 6amers, feeding off the youthful enthusiasm of the bunch. </p>
    <p>He has served with Victoria Police in a variety of operational and administrative roles for over 32 years and is currently working in the Communications Division of the State Emergencies and Security Command.  </p>
    <p>He is married to Sandy with 2 young adult children, enjoys participating and watching most sports, is a regular bushwalker and enjoys helping others achieve their goals. He is also looking forward to retirement when he and his wife can spend more time at their high country retreat near Corryong Victoria.  </p>
    <p>Until recently he was on the Board of the Victorian Sentencing Advisory Council and currently serves on the fundraising committee of Habitat for Humanity Victoria - who provide low cost housing for those in need.  Quietly spoken and with a strong endurance background, he is looking forward to completing this challenge with like-minded mates.  </p>
 
    <h2>Nick Eynon</h2>
    <p>Nick is a 34 year old design manager with a Melbourne based mechanical services company, D&amp;E Airconditioning and has spent the past 3 years leading up an engineering and drafting team for the new Victorian Cancer Centre hospital.  </p>
    <p>Coming from a background of elite level tennis and golf, his nickname of (happy) "gilmore" is well founded and he can still be found occasionally on the golf course knocking out close to par rounds.  When time permits, he is out on the bike before and after work clocking up midweek kms on nearby Kew Boulevard and Beach Rd, while heading further afield to Melbourne hotspots of Mt Dandenong, Kinglake and Mornington Peninsula on weekend rides.  </p>
    <p>He has been a 5 year 6amer and is known as one of the endurance climbers of the bunch, a trait which suits the Sydney to Melbourne parcours perfectly. Having recently completed a top 10 finish in the audax alpine classic 2015 in a time of 7h24, the climbing legs are there and well prepared for the rigours of S2M.  The opportunity to participate in a 10 day cycling event through his favourite ski fields taking in a host of fresh cycling routes was always going to appeal to this climber</p>

    <h2>Brenton Kaitler</h2>
    <p>While being the youngest of the quintet, Brenton is one of the longer-term 6amer members, having ridden with the group for seven years.  He enjoys both his Road riding and Mountain Biking, and between him and fiance Sally they have a total of 8 bikes! In his non-cycling life he has had recent senior roles at General Electric and Suncorp and was recently based in a combination of Melbourne and Vienna as General Manager for UBIMET, a leading private weather company. He now heads up Innovation for Intelematics, global vehicle telemetry provider, which also is a dual Australian and European role.</p>
    <p>During the late 2000s Brenton also drove for and managed a team in the Australian Rally Championship, with the highlight being an outright podium position in 2008.</p>
    <p>Brenton is fortunate to have done a fair bit of riding in the European Alps and is looking forward to this next epic ride for beyondblue in the Australian Alps.</p>

    <h2>Andrew Smith</h2>
    <p>Andrew is Managing Director of Pictureworks Group, one of Australia's leading photobook and photo product businesses.  He is married (to a very understanding wife) with two children in their early 20's.  </p>
    <p>After many years playing State League Hockey and then moving into triathlon he was late to the world of competitive cycling and did not start racing until he turned 35.  Since then he has competed widely in events ranging from State Championships to classic races such as the Melbourne to Warrnambool, World Masters Games, Baw Baw Classic, Tour of Bright and countless club races.  </p>
    <p>After 15 years as a 6amer and turning 50 later on this year, when approached about participating in this ride he believed what better way to celebrate your 50th year than a quiet 1,600km from Sydney to Melbourne over a few mountain ranges along the way.</p>
    <? include('footer.php'); ?>