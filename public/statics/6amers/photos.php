<? include('header.php'); ?>

    <?
        function gallery($day, $last){
            ?>
                <h2 style="clear:both">Day <?=$day?></h2>
                <p>
                    <? for($i=1; $i<=$last; $i++): ?>
                        <a class="fancybox" rel="day<?=$day?>" href="<?=getenv('BASEPATH')?>statics/6amers/gallery/D<?=$day?>_<?=str_pad($i, 2, '0', STR_PAD_LEFT)?>.jpg"><img src="<?=getenv('BASEPATH')?>statics/6amers/gallery/D<?=$day?>_<?=str_pad($i, 2, '0', STR_PAD_LEFT)?>.jpg" alt="" /></a>
                    <? endfor; ?>
                </p>
            <?
        }
    ?>

    <h1>Photos</h1>
    
    <? gallery(0,1); ?>
    <? gallery(1,4); ?>
    <? gallery(2,2); ?>
    <? gallery(3,15); ?>
    <div style="clear:both">
        <br><br><br>
    </div>
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" href="<?=getenv('BASEPATH')?>statics/6amers/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/6amers/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });    
    </script>
    <? include('footer.php'); ?>