<? include('header.php'); ?>

    <div class="col">
        <h1>Five 6amers, Nine days, 1600km &amp; 21000 metres of climbing</h1>
        <p>On Good Friday, April 3rd 2015, five members of the 6amers, are setting out to ride from Sydney to Melbourne commencing from the Sydney Opera House and finishing on the banks of the Yarra River on Sunday, April 12th.  </p>
        <p>However, for the riders, Paul Devereux, Peter Dikschei, Nick Eynon, Brenton Kaitler and Andrew Smith, the ride comes with a (vertical) twist.</p>
        <p>The 6amers team are taking a "detour" from the direct route and heading to Melbourne across the Great Dividing Range taking in Canberra, Jindabyne, Corryong, Mitta Mitta and Bright before heading for Melbourne.  The net result is a 'lot' of climbing and a lot more km's than the normal route.</p>
        <p>In total the team is riding 1,600km's &amp; 21,000 vertical metres over 9 days, to bring attention to men's health, the challenge of anxiety and depression and to support beyondblue. </p>
        <p>The team will be finishing on the banks of the Yarra River on Sunday 12th April in Melbourne. </p>
        <p style="font-style:italic">Join us in supporting beyondblue as we ride from Sydney to Melbourne in Support of this important cause.        </p>
    </div> 
    
    <div class="last col">
        <iframe src="https://www.google.com/maps/d/embed?mid=z9F_do3jxNjY.k845ts9c9Dkw" width="420" height="400"></iframe>
        <p style="padding-top:10px;"><a href="https://give.everydayhero.com/au/6amers-S2M" target="_blank"><img src="<?=getenv('BASEPATH')?>statics/6amers/donate-big.jpg" /></a></p>
    </div>

<? include('footer.php'); ?>