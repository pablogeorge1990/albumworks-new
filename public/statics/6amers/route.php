<? include('header.php'); ?>
    <h1>Route</h1>
    <iframe style="float:right; margin:0 0 10px 10px"" src="https://www.google.com/maps/d/embed?mid=z9F_do3jxNjY.k845ts9c9Dkw" width="420" height="400"></iframe>
    <h2>Fri 3 April - Sydney to Nowra, 212km, 2790m climbing</h2>
    <p>Day 1 is a 6am start on the forecourt of the iconic Sydney Opera House. The route heads south through Sydney and out of the city via the Royal National Park. The first 100km are essentially flat, following the coast to Wollongong. </p>
    <p>From the 'Gong the route heads up to 500m elevation at Mr Keira, then back down to follow the coast until Illawara Airport / Albion Park. Here the group heads inland, up a 700m climb into Macquarie National Park, a plateu, then a 6km -9% sweeping drop into Kangaroo Valley at the 175km mark. </p>
    <p>Finally after over 200km the group arrive at Nowra, ready for dinner and to start it all again on Day 2.</p>

    <h3>Stats Update:</h3>
    <dl>
        <dt>ETA</dt><dd>5:35pm</dd>
        <dt>Daily KM</dt><dd>212</dd>
        <dt>Daily Climbing</dt><dd>2790m</dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>212</dd>
        <dt>Cumulative Climbing</dt><dd>2790</dd>
    </dl>

    <h2>Sat 4 April - Nowra to Canberra, 193km, 2030m climbing</h2>
    <p>A 7am departure for the trip to Canberra. The team immediately head inland, up through the Yerriyong State Forest with the first 50km being a gradual climb to 750m. The day then oscillates between 500 and 700m elevation as the route picks up the small towns of Nerriga, Tarago and Bungendore before a roll in to Canberra from the south east with a lap of Capital Hill and Parliament House.</p>

    <h3>Stats Update:</h3>
    <dl>
        <dt>ETA</dt><dd>5:16pm</dd>
        <dt>Daily KM</dt><dd>193</dd>
        <dt>Daily Climbing</dt><dd>2030m</dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>405</dd>
        <dt>Cumulative Climbing</dt><dd>4820</dd>
    </dl>

    <h2>Sun 5 April - Canberra to Jindabyne, 189km, 2600m climbing</h2>
    <p>Day 3 is another long day, and today also has the second-highest amount of climbing, with 2600 vertical metres gained.</p>
    <p>After a 7am depart, the route heads south, utilising Boboyan Rd which runs parallel to the Monaro Highway. The ACT/NSW border is crossed after 80km, at 1400m elevation. A long rolling descent of 55km takes the team to Cooma, where they then head south-west, to finish in Jindabyne.</p>

    <h3>Stats Update:</h3>
    <dl>
        <dt>ETA</dt><dd>5:30pm</dd>
        <dt>Daily KM</dt><dd>189</dd>
        <dt>Daily Climbing</dt><dd>2600m </dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>594</dd>
        <dt>Cumulative Climbing</dt><dd>7420</dd>
    </dl>

    <h2>Mon 6 April - Jindabyne to Jindabyne, 78km, 1150 climbing</h2>
    <p>Today is the shortest day - and the teams 'rest day' - with an out and back route to the highest vehicular road in Australia; Charlotte's Pass.  Today's route goes via the venue made famous for Roy & HG's "Australian Winter Olympic" bid of Smiggen Holes(!), followed by Perisher Valley and through to Charlotte's Pass. The final 5km is a dirt track upon which the team will be standing at just under 2000m elevation. </p>

    <h3>Stats Update:</h3>
    <dl>
        <dt>ETA</dt><dd>2.00pm</dd>
        <dt>Daily KM</dt><dd>78</dd>
        <dt>Daily Climbing</dt><dd>1150m</dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>672</dd>
        <dt>Cumulative Climbing</dt><dd>8570</dd>
    </dl>

    <h2>Tue 7 April - Jindabyne to Corryong, 135km, 2590m climbing</h2>
    <p>By now the team should really be 'in the groove' of riding and especially climbing. Day 5 starts with a gradual 40km climb, from 900m to 1600m elevation. The ride via Thredbo, Tom Groggin, Geehi and Khancoban is simply stunning. Twice the route drops down to 400m before climbs up to 800 and 1050. The finish at Corryong a reasonably gentle affair, finishing at 300m.</p>
    
    <h3>Stats Update</h3>
    <dl>
        <dt>ETA</dt><dd>3.55pm</dd>
        <dt>Daily KM</dt><dd>135</dd>
        <dt>Daily Climbing</dt><dd>2590m</dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>807</dd>
        <dt>Cumulative Climbing</dt><dd>11,160</dd>
    </dl>

    <h2>Wed 8 April, Corryong to Mitta Mitta, 135km, 1570m climbing</h2>
    <p>Day 6 is arguably the easiest day of the ride and the team have allowed themselves a large R'n'R with a 10am start for their bodies to recover, with only 1 climb of significance (still up to 750m on the run from Corryong to Tallangatta though!) After the 40km mark it's a 25km descent down to 250m, with a reasonably flat and scenic run past the Hume Weir then following the Mitta Mitta river, finishing in Mitta Mitta.</p>
    
    <h3>Stats Update</h3>
    <dl>
        <dt>ETA</dt><dd>5.22pm</dd>
        <dt>Daily KM</dt><dd>135</dd>
        <dt>Daily Climbing</dt><dd>1570m</dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>942</dd>
        <dt>Cumulative Climbing</dt><dd>12,730</dd>
    </dl>    

    <h2>Thu 9 April, Mitta Mitta to Bright, 160km, 3510m climbing</h2>
    <p>If yesterday was the easiest day, today's ride from Mitta Mitta to Bright is the toughest! The team will roll out from Mitta Mitta at 7.00am. The first 40km climb from 200m to 1400m. There's then a descent on the Omeo Highway down to 750m, before the hellish climb up the back of Falls Creek to 1700m. The route then takes the team via Mt Beauty and over Tawonga Gap to finish at Bright. While the distance may 'only' be 160km, it's the 3500m climbing that will really hurt!</p>
    
    <h3>Stats Update</h3>
    <dl>
        <dt>ETA</dt><dd>4.07pm</dd>
        <dt>Daily KM</dt><dd>160</dd>
        <dt>Daily Climbing</dt><dd>3510m</dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>1102</dd>
        <dt>Cumulative Climbing</dt><dd>16,240</dd>
    </dl>     

    <h2>Fri 10 April, Bright to Mansfield, 161km, 1420m climbing</h2>
    <p>Getting into country well-known by 6amers now, with today's stage a big reduction from yesterday's "Queen Stage". We start with a run from Bright up to Porepunkah and Myrtleford before taking a westward turn and a roll on the flat to Buffalo River. A westside hemi-navigation of Lake Buffalo is pedalled followed by a ride through the often ill-enunciated Dandongadale!</p>
    <p>It is via the 'back-way' of Rose River and Cheshunt that the team then embark up the magnificent Whitfield climb, up from 280m to 900m. The final run encompasses the Tolmie plateu and a gorgeous descent in Mansfield.</p>

    <h3>Stats Update</h3>
    <dl>
        <dt>ETA:</dt><dd>5.00pm</dd>
        <dt>Daily KM</dt><dd>161</dd>
        <dt>Daily Climbing</dt><dd>1420m</dd>
        <dt>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>1263</dd>
        <dt>Cumulative Climbing</dt><dd>17,660</dd>
    </dl>    

    <h2>Sat 11 April, Mansfield to Marysville, 138km, 1970m climbing</h2>
    <p>On screen this may appear to be a short day, but don't let looks deceive! With an 8.30am rollout, the 6amers travel to the home of Australian-Champion and Tour de France yellow jersey wearer Simon Gerrans - Jamieson. From Jamieson the aptly named Mt Terrible is climbed, with 60km of twisting, undulating tarmac taking the team towards Eildon. The final run is bringing the team ever nearer to home, riding past the engaging Cathedral Ranges and Buxton (with a strong hope from some of the team that we'll throw in a lap of the MTB park!). And so the penultimate day comes to an inclining close with a gentle but gorgeous climb up to Marysville.</p>

    <h3>Stats Update</h3>
    <dl>
        <dt>ETA:</dt><dd>3.45pm</dd>
        <dt>Daily KM</dt><dd>138</dd>
        <dt>Daily Climbing</dt><dd>1970m</dd>
        <dt>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>1401</dd>
        <dt>Cumulative Climbing</dt><dd>19,630</dd>  
    </dl>    

    <h2>Sun 12 April, Marysville to Melbourne, 151km, 1640 climbing</h2> 
    <p>The Grand Finale! A day for the exultant team to share a glass of champagne, chaperoned by those 6amers who unfortunately didn't qualify for S2M Team Selection.</p>
    <p>And what a way to bring it home! Straight into the 10%+ climb from Marysville up to Lake Mountain road and Cumberland Junction at over 1100m. Then into one of the very best descents in Victoria, the 20km downhill-undulating Reefton Spur. The fun doesn't end there, with Warburton, Yarra Junction and Yellingbo. In to the absolute heart of "6amer Territory" now, with Monbulk, a run up the aptly named 'Wall, to the top of the Dandenongs at 550m. Getting closer now... Down to Olinda, Sassafras, and a descent of the 6amer-stronghold: the 1-in-20.</p>
    <p>It's then some of Melbourne's most eligible addresses - Croydon, Bayswater and Wantirna. Burwood Highway, taking the team straight past their perennial 'home-meeting-point', corner of Toorak Rd and Warrigal Rd.</p>
    <p>It's then left into Warrigal, right in High St (can you feel it now, oh so close!) Up High St to Armadale, then a run in via Toorak, hugging the mystical Yarra River ever so close. Nearly there!! Finally, a ride past the rowing sheds of the elite South Yarra schools, past the Botanic Gardens and - look, there it is! - a mega finish in the heart of Southbank, likely surrounded by throngs of fans!</p>
    <p>And so will complete the ride! Sydney to Melbourne. A distance of nearly DOUBLE the Hume Highway. The climbing equivalent of ascending Mt Buller 22.5 times. Mega!</p>

    <h3>Stats Update</h3>
    <dl>
        <dt>ETA:</dt><dd>4.17pm</dd>
        <dt>Daily KM</dt><dd>151</dd>
        <dt>Daily Climbing</dt><dd>1640m</dd>
        <dt>&nbsp;</dd>
        <dt>Cumulative KM</dt><dd>1552</dd>
        <dt>Cumulative Climbing</dt><dd>21,270</dd>     
    </dl>    
    
<? include('footer.php'); ?>