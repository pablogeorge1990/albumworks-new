<!DOCTYPE html>
<html>
    <head>
        <title>Across "The Divide" for beyondblue | 6am-ers | albumworks</title>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,300,400,600,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/6amers/common.css?v2" />
    </head>
<body>
    <div id="container">
        <div id="header">
            <a class="logo" target="_blank" href="https://www.albumworks.com.au/">albumworks</a>
            <a href="https://give.everydayhero.com/au/6amers-S2M" target="_blank"><img src="<?=getenv('BASEPATH')?>statics/6amers/donate-small.png" /></a>
        </div>
        <div id="body">
            <p style="margin-top:20px"><a href="6amers"><img id="headimg" src="<?=getenv('BASEPATH')?>statics/6amers/header.jpg?v2" alt="$900 in prizes!" /></a></p>
            <p style="text-align:center; margin-top:20px">
                <a href="6amers-route"><img src="<?=getenv('BASEPATH')?>statics/6amers/button-route.jpg" /></a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="6amers-riders"><img src="<?=getenv('BASEPATH')?>statics/6amers/button-riders.jpg" /></a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="6amers-blog"><img src="<?=getenv('BASEPATH')?>statics/6amers/button-blog.jpg" /></a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="6amers-photos"><img src="<?=getenv('BASEPATH')?>statics/6amers/button-photos.jpg" /></a>
            </p>