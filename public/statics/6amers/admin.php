<?
    include('header.php'); 
    $mysqli = mysqli_connect('127.0.0.1', '6amers', 'ZW7PcZcYn48KjaYd', '6amers');
?>
    
    <h1>Admin</h1>
    <? $password = 'preston'; ?>
    <? if(!isset($_POST['password']) or $_POST['password'] != $password): ?>
        <form method="post">
            <input class="password" type="password" name="password" placeholder="Password" />
            <input type="submit" value="Go" />
        </form>
    <? else: ?>
        <?
            if(isset($_POST['action']) and $_POST['action'] == 'edit'){
                $fields = array('publishdate', 'title', 'content');
                $allempty = true;
                $anyempty = false;
                
                foreach($fields as $key){
                    if($_POST[$key] == '')
                        $anyempty = true;
                    else $allempty = false;
                }
                
                if($allempty){
                    $mysqli->query('DELETE FROM blog WHERE id = '.$_POST['id']);
                    echo '<p style="background:red; color:white; padding:10px">Entry deleted!</p>';
                }
                else{
                    foreach($fields as $key){
                        $mysqli->query('UPDATE blog SET '.$key.' = "'.$mysqli->real_escape_string($_POST[$key]).'" WHERE id = '.$_POST['id']);
                    }
                    echo '<p style="background:green; color:white; padding:10px">Entry updated!</p>';
                }
            }
            else if(isset($_POST['action']) and $_POST['action'] == 'add'){
                $mysqli->query('INSERT INTO blog (publishdate, title, content) VALUES("'.$mysqli->real_escape_string($_POST['publishdate']).'", "'.$mysqli->real_escape_string($_POST['title']).'", "'.$mysqli->real_escape_string($_POST['content']).'")');
                echo '<p style="background:green; color:white; padding:10px">Entry added!</p>';
            }
        ?>
        <p>To create a new entry, use the empty form at the bottom.</p>
        <p>To delete an entry, remove all values in it and press [Edit Existing Entry].</p>
        <p>To upload an image:</p>
        <ul>
            <li>Put your cursor where you want an image</li>
            <li>Click the [Image] button</li>
            <li>Click the [Browse] button</li>
            <li>Log in with admin/admin</li>
            <li>Click [6amers] folder</li>
            <li>Click [Upload] button</li>
            <li>Select the image/s you want to upload</li>
            <li>Click [Upload]</li>
            <li>Click [Close]</li>
            <li>Click [Insert]</li>
            <li>Click [OK]</li>
        </ul>
        <hr/>
        <?
            if($result = $mysqli->query('SELECT * FROM blog ORDER BY publishdate DESC')){
                while($post = $result->fetch_object()){
                    ?>
                        <h1>Edit <em>"<?=$post->title?>"</em></h1>
                        <form method="post">
                            <input type="hidden" name="password" value="<?=$password?>" />
                            <input type="hidden" name="id" value="<?=$post->id?>" />
                            <input type="hidden" name="action" value="edit" />
                            <div class="post postform">
                                <p class="datetime"><span>Publish Date</span><br><input type="text" class="datepicker" name="publishdate" value="<?=$post->publishdate?>" /></p>
                                <h2><span>Title</span><br><input type="text" name="title" value="<?=$post->title?>" /></h2>
                                <div class="postcontent"><span>Content</span><br><textarea class="wysiwyg" name="content"><?=$post->content?></textarea></div>
                                <input type="submit" value="Edit Existing Entry" />
                            </div>
                        </form>
                    <?
                }
            }
        ?>  
        
        <h1>Add New Post</h1>
        <form method="post">
            <input type="hidden" name="password" value="<?=$password?>" />
            <input type="hidden" name="action" value="add" />
            <div class="post postform">
                <p class="datetime"><span>Publish Date</span><br><input type="text" class="datepicker" name="publishdate" value="" /></p>
                <h2><span>Title</span><br><input type="text" name="title" value="" /></h2>
                <div class="postcontent"><span>Content</span><br><textarea class="wysiwyg" name="content"></textarea></div>
                <input type="submit" value="Add New Entry" />
            </div>
        </form>    
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>    
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
        <script src="admin/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.datepicker').datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    dateFormat: 'yy-mm-dd 00:00:00'
                });
            });
            tinymce.init({
                selector:'.wysiwyg',
                paste_as_text: true,
                plugins: "moxiemanager image code link",
                image_advtab: true,
                extended_valid_elements : "style,span,a[class|name|href|target|title|onclick|rel],script[type|src],iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]"
            });            
        </script>
    <? endif; ?>
                                                       
<? include('footer.php'); ?>