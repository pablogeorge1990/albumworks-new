<?
    if(!empty($_POST)){
        if(isset($_POST['verifier'])){
            // send to competition db
            $url = 'https://api.photo-products.com.au/competitions/add.php?promo=xmas2014';
            foreach($_POST as $key => $val){
                if(!in_array($key, array('over18', 'verifier', 'optin'))){
                    $url .= '&'.urlencode($key).'='.urlencode($val);
                }
            }
            file_get_contents($url);
                        
            // send to salesforce if the customer wanted to opt in
            if(isset($_POST['optin'])){
                $kv = array();
                $kv['first_name'] = urlencode($_POST['firstname']);
                $kv['last_name'] = urlencode($_POST['lastname']);
                $kv['email'] = urlencode($_POST['email']);
                $kv['sfga'] = '00D36000000oZE6';
                $kv['oid'] = '00D36000000oZE6';
                $kv['retURL'] = 'http://www.albumworks.com.au/ultimate-christmas-thanks';
                $kv['lead_source'] = 'XMAS2014 Promo';
                $kv['00N20000001STqA'] = 'Web-to-Lead';
                $kv['00N3600000BOyGd'] = 'Promotion';
                $kv['00N200000013fDx'] = 'AP';
                $kv['00N3600000Loh5K'] = 'PG';
                $kv['00N3600000LosAC'] = '';
                $kv['00N3600000LosKl'] = '1';
                $kv['00N3600000Los6F'] = '';
                $kv['emailOptOut'] = '1';
                
                $params = array();
                foreach ($kv as $key => $value) {
                    $params[] = stripslashes($key)."=".stripslashes($value);
                }                
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8');
                curl_setopt($ch, CURLOPT_POSTFIELDS, join("&", $params));
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $result = curl_exec($ch);
                curl_close($ch);                
            }
        }
        header('Location: ultimate-christmas-thanks');
        return; die; exit(1);
    }
    
include('header.php'); ?>
    <div class="twocols">
        <div class="col first">
            <h1>Tell us the story of your "Ultimate Christmas" and you could win:</h1>
            <p><strong>1 x premium HD Photo Book worth up to $250:</strong></p>
            <ul>
                <li>Luxurious Canon Photo Paper</li>
                <li>Unparalleled HD photo printing</li>
                <li>Quality book craftsmanship</li>
                <li>Local Customer Service</li>
            </ul>
            <br>

            <p><strong>1 x Ultimate Christmas Hamper from<br>Interhamper worth $650:</strong></p>
            <ul>
                <li>Glenrothes Speyside Whiskey</li>
                <li>Moet &amp; Chandon Brut</li>
                <li>Penfolds Bin 8 Shiraz</li>
                <li>Pudding on the Ritz Christmas Pudding</li>
                <li>Ernest Hillier Milk &amp; Dark Heritage Chocolates and lots more*</li>
            </ul>
            
            <p class="smaller"><a href="ultimate-christmas-terms" target="_blank">Terms and conditions.</a></p>
            
            <p class="smaller">*Hamper contains: Glenrothes Speyside Single Malt Whisky 700ml � Moet &amp; Chandon Brut N.V. 750ml � Penfolds Bin 8 Cabernet Shiraz 750ml � Baileys The Original Irish Cream Liqueur 700ml � Tempus Two Pewter Botrytis Semillon 250ml � Pudding on the Ritz Christmas Pudding 500g � The Cakemen Florentine Nut Cake 500g � Patons Premium Chocolate Macadamia Gold 170g � Mac�s Butter Shortbread Tin 400g � Jindi Quince Paste 100g � Carr's Entertainment Cracker Collection 200g � Coffex Ground Coffee 250g � Granforno Grissini Breadsticks 125g � Cobram Extra Virgin Australian Olive Oil 250ml � Ernest Hillier Milk &amp; Dark Heritage Chocolates 175g � Snax with Attitude Salted Cashews 100g � Alchemy Coffee Syrup 250ml � Maille Balsamic Vinegar 250ml � Bonne Maman Jam 370g � Maille French Mustard 210g � JC'S Quality Foods Pistachios 150g � Bahlsen Deloba Pastry Biscuits 100g � Patons Macadamia Dry Roast 50g � Kez's Kitchen Florentines 185g � Trentham Tucker Almond Bread 150g � Walkers Biscuits 150g � Robins Old Style Brandy Sauce 250ml � Yorkshire Tea 50 pack � 4 x Designer Mugs � 3 piece Cocktail Kit</p>
        </div>
        <div class="col last">
            <div class="thanksmate">
                <h1>Thanks so much for your entry!</h1>
                <p>The winner will be notified by email.</p>
                <p>Follow us on <a href="https://www.facebook.com/albumworks">Facebook</a> for great photography and Photo Book tips, and to stay up-to-date with our offers and promotions.</p>
                <p>Learn more about our stunning <a href="http://www.albumworks.com.au/photo-books">Photo Books</a></p>
                <iframe width="360" height="233" src="//www.youtube.com/embed/1oITuX7ar2g" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="clr"> </div>
    </div>
<? include('footer.php'); ?>