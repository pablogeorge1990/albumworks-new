<? include('header.php'); ?>
    <div class="twocols">
        <div class="col first">
            <h1>Tell us the story of your "Ultimate Christmas" and you could win:</h1>
            <p><strong>1 x premium HD Photo Book worth up to $250:</strong></p>
            <ul>
                <li>Luxurious Canon Photo Paper</li>
                <li>Unparalleled HD photo printing</li>
                <li>Quality book craftsmanship</li>
                <li>Local Customer Service</li>
            </ul>
            <br>

            <p><strong>1 x Ultimate Christmas Hamper from<br>Interhamper worth $650:</strong></p>
            <ul>
                <li>Glenrothes Speyside Whiskey</li>
                <li>Moet &amp; Chandon Brut</li>
                <li>Penfolds Bin 8 Shiraz</li>
                <li>Pudding on the Ritz Christmas Pudding</li>
                <li>Ernest Hillier Milk &amp; Dark Heritage Chocolates and lots more*</li>
            </ul>
            
            <p class="smaller"><a href="ultimate-christmas-terms" target="_blank">Terms and conditions.</a></p>
            
            <p class="smaller">*Hamper contains: Glenrothes Speyside Single Malt Whisky 700ml  Moet &amp; Chandon Brut N.V. 750ml  Penfolds Bin 8 Cabernet Shiraz 750ml  Baileys The Original Irish Cream Liqueur 700ml  Tempus Two Pewter Botrytis Semillon 250ml  Pudding on the Ritz Christmas Pudding 500g  The Cakemen Florentine Nut Cake 500g  Patons Premium Chocolate Macadamia Gold 170g  Macs Butter Shortbread Tin 400g  Jindi Quince Paste 100g  Carr's Entertainment Cracker Collection 200g  Coffex Ground Coffee 250g  Granforno Grissini Breadsticks 125g  Cobram Extra Virgin Australian Olive Oil 250ml  Ernest Hillier Milk &amp; Dark Heritage Chocolates 175g  Snax with Attitude Salted Cashews 100g  Alchemy Coffee Syrup 250ml  Maille Balsamic Vinegar 250ml  Bonne Maman Jam 370g  Maille French Mustard 210g  JC'S Quality Foods Pistachios 150g  Bahlsen Deloba Pastry Biscuits 100g  Patons Macadamia Dry Roast 50g  Kez's Kitchen Florentines 185g  Trentham Tucker Almond Bread 150g  Walkers Biscuits 150g  Robins Old Style Brandy Sauce 250ml  Yorkshire Tea 50 pack  4 x Designer Mugs  3 piece Cocktail Kit</p>
        </div>
        <div class="col last">
            <form action="ultimate-christmas-thanks" method="POST" onsubmit="return validate_form()">
                <div class="field">
                    <label for="handler_firstname">First Name*:</label>
                    <input type="text" name="firstname" id="handler_firstname" label="First Name" placeholder="First Name" class="inputbox" />
                </div>
                <div class="field">
                    <label for="handler_lastname">Last Name*:</label>
                    <input type="text" name="lastname" id="handler_lastname" label="Last Name" placeholder="Last Name" class="inputbox" />
                </div>
                <div class="field">
                    <label for="handler_email">Email*:</label>
                    <input type="text" name="email" id="handler_email" label="Email" placeholder="Email" class="inputbox" />
                </div>
                <div class="field">
                    <label for="handler_entry">SHARE YOUR "ULTIMATE CHRISTMAS" STORY*:</label>
                    <textarea name="entry" id="handler_entry" label="Your Ultimate Christmas Story" placeholder="My Ultimate Christmas was when..." class="inputbox"></textarea>
                    <p>(100 words or less)</p>
                </div>
                <div class="field">
                    <input type="checkbox" name="optin" id="handler_optin" class="checkbox" />
                    <label for="handler_optin" class="checklabel">SIGN ME UP TO HEAR ABOUT FUTURE COMPETITIONS &amp; OFFERS</label>
                </div>                
                <div class="field">
                    <input type="checkbox" name="over18" id="handler_over18" class="checkbox" />
                    <label for="handler_over18" class="checklabel">I AM OVER 18*</label>
                </div>                
                <button type="submit">ENTER</button>
            </form>
            
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
            <script type="text/javascript">
                function is_valid_email(email){ 
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                }        
                     
                function validate_form(){
                    var errors = false;
                    $('.inputbox').each(function(){
                        var $this = $(this);
                        if($this.val() === ''){
                            alert('Sorry, but "'+$this.attr('label')+'" is a required field, please enter a value and submit again.');
                            errors = true;
                            return false;
                        }
                    });
                    if(errors) return false;
                    
                    if(!is_valid_email($('#handler_email').val())){
                        alert('Sorry, but your email doesn\'t appeart to be entered correctly. Please check it and submit again');
                        return false;
                    }

                    if(!$('#handler_over18').is(':checked')){
                        alert('Sorry, you must be over 18 to enter this competition');
                        return false;
                    }
                    
                    $('form').append('<input type="hidden" name="verifier" value="gogo" />');
                    return true;
                }
            </script>
        </div>
        <div class="clr"> </div>
    </div>
<? include('footer.php'); ?>