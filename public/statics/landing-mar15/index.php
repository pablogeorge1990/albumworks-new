<? include('header.php'); ?>
    <div class="carousel controls">
        <a class="left arrow" href="javascript:void(0)" onclick="stop_carousel($(this).closest('.carousel')); scroll_carousel(-1, $(this).closest('.carousel'))"><img src="<?=getenv('BASEPATH')?>statics/landing-mar15/arrow-left.png" /></a>
        <div class="sliderwrapper">
            <div class="slider">
                <? if(time() < mktime(21,0,0,4,27,2015)): ?>
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-mar15/mday-header01.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-mar15/mday-header02.jpg" />
                <? else: ?>
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-mar15/01.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-mar15/02.jpg" />
                <? endif; ?>
            </div>
        </div>
        <a class="right arrow" href="javascript:void(0)" onclick="stop_carousel($(this).closest('.carousel')); scroll_carousel(1, $(this).closest('.carousel'))"><img src="<?=getenv('BASEPATH')?>statics/landing-mar15/arrow-right.png" /></a>
    </div>
    <div class="callout">
        <p><strong>If this is your first order with <em>albumworks</em>, welcome!</strong></p>
        <p>We love our products and we know you will too.</p>
    </div>
    
    <h3>To say thanks for choosing us, we'd like to give you the choice of two great "first order" special offers:</h3>
    
    <table cellpadding="0" cellspacing="0" width="100%" class="twoopts">
        <tbody>
            <tr valign="top">
                <td width="50%">
                    <p><img src="<?=getenv('BASEPATH')?>statics/landing-mar15/04.jpg" /></p>
                    <h4>Perfect for making big, beautiful Photo Books</h4>
                    <p>Depending on size, our Photo Books come with 20 or 40 pages. But if you're making a big travel or wedding book, or you just have lots of gorgeous photos, you're going to need more pages!</p>
                    <p>Usually extra pages cost more, but with our offer, you can get up to 20 extra pages free! That's up to <strong>$60 of value!</strong></p>
                </td>
                <td width="50%">
                    <p><img src="<?=getenv('BASEPATH')?>statics/landing-mar15/05.jpg" /></p>
                    <h4>Want to make a small Photo Book? Or a Canvas? Or a cute pack of our Instagram Cards?</h4>
                    <p>Use our Download Editor to make a Photo Book, or our fast, simple online Editor to make any of the other great products in our range, and get 30% off!</p>
                    <p>We know you'll love the quality of all our products, and our Customer Service team are there to help every step of the way.</p>
                </td>
            </tr>
        </tbody>
    </table>
    
    <p style="text-align: center;"><img style="width:100%; max-width:862px" src="<?=getenv('BASEPATH')?>statics/landing-mar15/08.jpg" /></p>
    
    <h3>No sign up fee, no obligation, just a community of 80,000 people who love photo books!</h3>
    
    <form class="box" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
        <a name="join"></a>
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&landing-mar15" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="SPLITOFFER" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
        
        <!-- Actual fields -->
        <input type="text" value="NAME" name="first_name" class="firstname smartedit inputbox" alt="NAME">
        <input type="text" value="EMAIL" name="email" class="email smartedit inputbox" alt="EMAIL">
        <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">JOIN NOW<span>and get our free editor</span></a>
        <div class="checkarea">
            <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
            <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
        </div>
        <div class="clr"> </div>
    </form>
    
    <hr />
    
    <div class="clr" style="text-align: center;">
        <h3>Why choose <em>albumworks</em>:</h3>
        <div class="blocks">
            <img src="<?=getenv('BASEPATH')?>statics/landing-mar15/adwords-02.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-mar15/adwords-03.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-mar15/adwords-04.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-mar15/adwords-05.jpg" />
            <div class="clr"></div>
        </div>
        
        <h3 style="margin-top:20px">Upload photos from:</h3>
        <p><img src="<?=getenv('BASEPATH')?>statics/landing-mar15/13.jpg" style="width:100%; max-width:593px;" /></p>
        <p class="smaller">*Available only on online Editor</p>
    </div>
    
    

    
<? include('footer.php'); ?>