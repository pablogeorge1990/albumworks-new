$(document).ready(function(){
    $('.carousel').each(function(){
        var $carousel = $(this);
        var $wrapper = $(this).find('.sliderwrapper');
        var $slider = $(this).find('.slider');
        
        if($slider.length){
            window.setTimeout(function(){
                $slider.width($slider.children().length * 1060).height($carousel.height());
                $wrapper.height($carousel.height());            
                $carousel.height($carousel.height()).data({
                    animating: false,
                    current: 1,
                    autoplay: true
                });
                
                window.setTimeout(function(){
                    window.setInterval(function(){
                        if($carousel.data('autoplay')){
                            scroll_carousel(1, $carousel);
                            window.setTimeout("", 400);    // account for the slide duration
                        }
                    }, 5000) // move every 5 seconds
                }, 400); // account for the initial slide duration        
            }, 1000);
        }
    });   
    
    // INTERACTITVITY
    $('.smartedit').each(function(){
        if($(this).is('textarea'))
            $(this).attr('rel', $(this).val());
        else if($(this).attr('alt') == '')
            $(this).attr('alt', $(this).val());
    }).focus(function(){               
        if($(this).val() == $(this).attr('alt') || $(this).val() == $(this).attr('rel'))
            $(this).val('').addClass('activesmart');
    }).blur(function(){
        if($(this).val() == '')
            if($(this).attr('rel') != undefined && $(this).attr('rel') != '')
                 $(this).val($(this).attr('rel')).removeClass('activesmart');
            else $(this).val($(this).attr('alt')).removeClass('activesmart');
    });      
});

function stop_carousel($carousel){
    $carousel.data('autoplay', false);
}

function carousel_goto(newpos, $carousel){
    var currentpos = parseInt($carousel.find('.dot.active').attr('rel')); 
    scroll_carousel((newpos - currentpos), $carousel);
}

function scroll_carousel(direction, $carousel){
    var $slider = $carousel.find('.slider');
    var count = $carousel.find('.item').length;
    var blockwidth = $carousel.find('.item:first').width();    
    var current = (parseInt($carousel.data('current')) + direction);
    if((parseInt($carousel.data('current')) + direction) < 1)
        current = count;
    else if((parseInt($carousel.data('current')) + direction) > count)
        current = 1;
    
    if(!$carousel.data('animating')){
        $carousel.data('animating', true);    
        $carousel.data('current', current);
        
        if(direction < 0){ 
            for(i=0; i<Math.abs(direction); i++) $carousel.find('.item:last').prependTo($slider);
            $slider.css('left', (direction*blockwidth)+'px');
            $slider.animate({left:'0px'}, 400, function(){
                if($carousel.find('.dot.active').length){                    
                    var currentpos = parseInt($carousel.find('.dot.active').attr('rel'));  
                    var newpos = ((currentpos + direction) <= 0) ? $carousel.find('.dot').length - Math.abs(currentpos + direction) : (currentpos + direction);
                    $carousel.find('.dot.active').removeClass('active');
                    $carousel.find('.dot:nth-child('+newpos+')').addClass('active');
                }
                $carousel.data('animating', false);
            });
        }
        else if(direction > 0){
            $slider.animate({left:'-'+(direction*blockwidth)+'px'}, 400, function(){
                for(i=0; i<Math.abs(direction); i++) $carousel.find('.item:first').appendTo($slider);
                $slider.css('left', '0px');
                if($carousel.find('.dot.active').length){                    
                    var currentpos = parseInt($carousel.find('.dot.active').attr('rel')); 
                    var newpos = ((currentpos + direction) > $carousel.find('.dot').length) ? Math.abs(currentpos + direction) - $carousel.find('.dot').length : (currentpos + direction);
                    $carousel.find('.dot.active').removeClass('active');
                    $carousel.find('.dot:nth-child('+newpos+')').addClass('active');
                }                
                $carousel.data('animating', false);            
            });
        }
    }    
}

function is_valid_email(email){ 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}        
     
function checkdlform(form){ 
    var $form = $(form);
    var errors = false;
    $form.find('.inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });
    
    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }
    
    if(!is_valid_email($form.find('input[name=email]').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }
    
    var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
    $form.find('input[name=00N3600000Los6F]').val(os_select);
    document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";    
    
    var name = $form.find('input[name=first_name]').val();
    var email = $form.find('input[name=email]').val();    
    var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
    document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";
    
    var iscookie = readCookie('download_params');
    if(iscookie)
        $form.find("input[00N3600000LosKl]").val(0);

    var refpromo = readCookie('APrefpromo');
    if(refpromo)
        $form.find("input[00N3600000LosAC]").val(refpromo);
    refpromo = $form.find('input[00N3600000LosAC]').val();
    document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";
    
    if(typeof _gaq !== 'undefined') { _gaq.push(['_trackPageview', '/download_start']); }
    
    return true;
}

function urlencode (str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
    replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}            
