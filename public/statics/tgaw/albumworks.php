<!DOCTYPE html>
<html>
<head>
    <title>Welcome to albumworks</title>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,300,400,600,700' rel='stylesheet' type='text/css'>
    <style type="text/css">
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed, 
        figure, figcaption, footer, header, hgroup, 
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
        }
        /* HTML5 display-role reset for older browsers */
        article, aside, details, figcaption, figure, 
        footer, header, hgroup, menu, nav, section {
            display: block;
        }
        body {
            line-height: 1;
        }
        ol, ul {
            list-style: none;
        }
        blockquote, q {
            quotes: none;
        }
        blockquote:before, blockquote:after,
        q:before, q:after {
            content: '';
            content: none;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }    
        
        body{
            background:#de1e00;
        }
        
        .content{
            margin:50px auto 0; 
            width:900px;
            background:white url(<?=getenv('BASEPATH')?>statics/tgaw/aw.jpg) top right no-repeat;
            border:5px solid white;
            padding:40px 30px;
        }
        
        p{
            font-family: 'Open Sans', sans-serif;
            color:#4c4c4c;            
            font-size:15px;
            width:550px;
            margin:0 0 20px;
            line-height:1.2;
        }           
        
        em{
            font-style: italic;
        }     
        
        strong{
            font-weight:bold;
        }
        
    </style>
</head>

<body>

    <div class="content">
        <p><img src="<?=getenv('BASEPATH')?>statics/tgaw/target-logo.jpg" /></p>

        <p style="font-size:21px">From 20 June 2016, the Target Photobook desktop software, a service provided by Pictureworks Group Pty Ltd, is no longer being offered by Target and is now being directly provided by Pictureworks Group through its service <em>albumworks</em>&reg;</p>

        <p>Pictureworks has been providing this service since 2008 and we'll continue to be here to support all of your current, past and future projects. We are passionate about creating books of the highest quality, and we really care about your memories.</p>

        <p>If you've already downloaded the Target Photobooks desktop software, simply continue to use this editor the same way as you always have, and <em>albumworks</em> will be there every step of the way.</p>

        <p><strong>Use the Voucher Code: </strong>WELCOME30<strong> and get 30% off your next order.</strong></p>

        <p><a href="https://www.albumworks.com.au/photo-books"><img src="<?=getenv('BASEPATH')?>statics/tgaw/getstarted.jpg" /></a></p>
    </div>

</body>

</html>