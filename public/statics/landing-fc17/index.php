<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Flight Centre | albumworks</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-fc17/common.css" />
        
        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '742cc0afc69d8afb67a6ac020f80347eee46766e');
        </script>        
    </head>
    <body>
    <header>
        <section id="strip" class="group">
            <nav id="topmenu" class="group">
                <a href="rewards" id="toprewards">
                    REWARDS
                </a>
                <a href="track" id="toptrack">
                    TRACK
                </a>
                <a href="faq" id="topfaq">
                    FAQs
                </a>
                <a href="contact" id="topcontact">
                    CONTACT
                    <span>1300 553 448</span>
                </a>
            </nav>
        </section>
    </header>
                   
    <div id="intro" class="panel">
        <p style="margin-bottom:10px"><img style="width:90%; max-width:452px" src="<?=getenv('BASEPATH')?>statics/landing-fc17/fclogo.jpg" /></p>
        <h1><strong>FREE $30 VOUCHER</strong></h1>
        <p class="subtext">TO USE ON ANY PHOTO BOOK PRODUCT!</p>
        <p class="subtext2">OR GET A FREE* 40 PAGE 8x6” SOFTCOVER PHOTOBOOK</p>
    </div>

    <div class="steps group">
        <div class="step">
            <p class="center"><img src="<?=getenv('BASEPATH')?>statics/landing-fc17/step1.jpg" alt="1" /></p>
            <p>Fill out the form below to receive your $30 Voucher and download the desktop Editor</p>
        </div>
        <div class="step">
            <p class="center"><img src="<?=getenv('BASEPATH')?>statics/landing-fc17/step2.jpg" alt="2" /></p>
            <p>Create your Photo Book using the desktop Editor</p>
        </div>
        <div class="step">
            <p class="center"><img src="<?=getenv('BASEPATH')?>statics/landing-fc17/step3.jpg" alt="3" /></p>
            <p>Use your Gift Voucher Code when placing your order</p>
        </div>
    </div>
                       
    <div id="footer" name="footer" class="panel">
        <div class="content">                               
            <p class="fancy">Enter your details to receive your $30 Voucher and get started!</p>
            <form class="box" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&landing-fc17&referringPromotion=FLIGHTCENTRE" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                        <input type="hidden" value="Download" name="00N3600000BOyGd">
                        <input type="hidden" value="AP" name="00N3600000BOyAt">
                        <input type="hidden" value="PG" name="00N3600000Loh5K">
                        <input type="hidden" name="00N3600000Los6F" value="Windows">
                        <input type="hidden" value="FLIGHTCENTRE" name="00N3600000LosAC">
                        <input type="hidden" value="1" name="00N3600000LosKl">
                        <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                        
                        <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                        <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                        <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                        <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                        <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                        <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                        <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                        <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                        <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
                
                <input type="text" placeholder="NAME" name="first_name" class="firstname smartedit inputbox" alt="NAME">
                <input type="text" placeholder="EMAIL" name="email" class="email smartedit inputbox" alt="EMAIL">
                <input type="text" placeholder="BOOKING NUMBER" name="bookingnum" class="bookingnum smartedit inputbox" alt="BOOKING NUMBER">
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET FREE VOUCHER</a>
                <div class="checkarea">                                                                                                                                 
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check" style="position:fixed; left:-9999em; top:-9999em">               
                    <label>By downloading the <em>albumworks</em> editor you agree to receive communication on special offers and software updates. You can unsubscribe at any time.</label>
                </div>
                <div class="clr"> </div>
            </form>
        </div>
    </div>

    <p class="tandc">*Terms and Conditions<br/>1. Offer valid for a $30 voucher to be used on any albumworks product.  2. Postage and handling fees apply.  3. Your $30 gift voucher will be instantly emailed to your entered email address.  4. Gift voucher is valid for 9 months from date of issue.  4. Offer is valid for one time use and any amount left over is non-refundable and cannot be used on a future order. If your order totals more than the value of your Gift Voucher, you will be prompted to pay for any difference during the ordering process.  5. Offer cannot be changed or redeemed for cash.  6. Offer valid until stock lasts.</p>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/landing-fc17/common.js"></script>
    
    
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1338422-2']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        var host = window.location.host;
    </script>

                

        <div style="position:fixed">
            <script type="text/javascript">
                var current_page = 'landing-fc17';
                var BASEPATH = 'https://www.albumworks.com.au/';
                //var BASEPATH = host;



                <? if(isset($_GET['alertmsg'])): ?>
                window.setTimeout(function(){
                    alert("<?=addslashes($_GET['alertmsg'])?>");
                }, 1000);
                <? elseif(isset($_GET['download'])): ?>
                $(document).ready(function(){
                    launch_download_lightbox();
                })
                <? endif; ?>
            </script>

            <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="https://www.albumworks.com.au/css/ie.css" />
            <script type="text/javascript" src="https://www.albumworks.com.au/js/selectivizr-min.js"></script>
            <script type="text/javascript" src="https://www.albumworks.com.au/js/css3-mediaqueries.js"></script>
            <![endif]-->
            <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js#username=albumprinter"></script>

            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-1338422-2']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
            <script type="text/javascript">var _kiq = _kiq || [];</script>
            <script type="text/javascript" src="//s3.amazonaws.com/ki.js/20502/40J.js" async="true"></script>
            <script type="text/javascript">
                var _ga = new Object;
                _ga.campaign = '';
                _ga.term = '';
                _ga.search= '';
                _ga.referrer = '';
                _ga.group = '';
                _ga.keywords = '';

                var gc = '';
                var c_name = "__utmz";
                if (document.cookie.length>0){
                    c_start=document.cookie.indexOf(c_name + "=");
                    if (c_start!=-1){
                        c_start=c_start + c_name.length+1;
                        c_end=document.cookie.indexOf(";",c_start);
                        if (c_end==-1) c_end=document.cookie.length;
                        gc = unescape(document.cookie.substring(c_start,c_end));
                    }
                }

                if(gc != ""){
                    var z = gc.split('.');
                    if(z.length >= 4){
                        var y = z[4].split('|');
                        for(i=0; i<y.length; i++){
                            if(y[i].indexOf('utmccn=') >= 0) _ga.campaign = y[i].substring(y[i].indexOf('=')+1);
                            if(y[i].indexOf('utmctr=') >= 0) _ga.term     = y[i].substring(y[i].indexOf('=')+1);
                        }
                    }
                }


                <?
                    function cleanit($string){
                        //return preg_replace("/[^A-Za-z0-9]/", '', strtolower($string));
                        return trim(strtolower($string));
                    }

                    if(strpos($_SERVER['REQUEST_URI'], '?') !== FALSE){
                        $querystring = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?')+1);
                        $queryparts = explode('&', $querystring);
                        foreach($queryparts as $part){
                            list($key, $val) = explode('=', $part);
                            $_GET[$key] = urldecode($val);
                        }
                        $_GET['q'] = $_SERVER['REQUEST_URI'];
                    }

                    if(isset($_SERVER['HTTP_REFERER'])){
                        $parts = parse_url($_SERVER['HTTP_REFERER']);
                        $_SERVER['HTTP_REFERER'] = $parts['host'];
                    }

                    if(isset($_GET['campaign'])) $_GET['campaign'] = cleanit($_GET['campaign']);
                    if(isset($_GET['group'])) $_GET['group'] = cleanit($_GET['group']);
                ?>


                <? if(isset($_GET['q'])): ?> _ga.search = "<?=addslashes($_GET['q'])?>"; <? endif; ?>

                <? if(isset($_GET['_kk'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kk']))?>"; <? endif; ?>
                <? if(isset($_GET['_kw'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kw']))?>"; <? endif; ?>

                <? if(isset($_GET['group'])): ?> _ga.group = "<?=addslashes(cleanit($_GET['group']))?>"; <? endif; ?>
                <? if(isset($_GET['campaign'])): ?> _ga.campaign = "<?=addslashes(($_GET['campaign'] == "notset") ? "" : cleanit($_GET['campaign']))?>"; <? endif; ?>
                <? if(isset($_GET['network'])): ?> _ga.network = "<?=((($_GET['network'] == 'g') or ($_GET['network'] == 's')) ? 'Search' : 'Content')?>"; <? endif; ?>

                <? if(isset($_SERVER['HTTP_REFERER'])): ?> _ga.referrer = "<?=addslashes($_SERVER['HTTP_REFERER'])?>"; <? endif; ?>
                <? if(isset($_GET['placement'])): ?> _ga.referrer = "<?=addslashes($_GET['placement'])?>"; <? endif; ?>
                <? if(isset($_GET['_placement'])): ?> _ga.referrer = "<?=addslashes($_GET['_placement'])?>"; <? endif; ?>

                <? if(isset($_GET['target'])): ?> _ga.target = "<?=addslashes($_GET['target'])?>"; <? endif; ?>
                <? if(isset($_GET['position'])): ?> _ga.position = "<?=addslashes($_GET['position'])?>"; <? endif; ?>
                <? if(isset($_GET['match'])): ?> _ga.match = "<?=addslashes($_GET['match'])?>"; <? endif; ?>

                <? if(isset($_GET['gclid'])): ?>
                createCookie('gclid','<?=addslashes($_GET['gclid'])?>',90);
                <? endif; ?>
                <? if(isset($_GET['msclkid'])): ?>
                createCookie('gclid','bing<?=addslashes($_GET['msclkid'])?>',90);
                <? endif; ?>

                jQuery('input[name=00N3600000BOyGd]').each(function(){
                    var $form = $(this).closest('form');
                    if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
                        $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
                    }
                });
            </script>

            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1060947230;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1060947230/?guid=ON&amp;script=0"/>
            </div>
            </noscript>            

            
            <!--Start of Tawk.to Script-->
            <script type="text/javascript">var Tawk_API=Tawk_API||{},Tawk_LoadStart=new Date();(function(){var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true;s1.src='https://embed.tawk.to/588ff051af9fa11e7aa51d60/default';s1.charset='UTF-8';s1.setAttribute('crossorigin','*');s0.parentNode.insertBefore(s1,s0);})();</script>
            <!--End of Tawk.to Script-->             
            
<!--  MouseStats:Begin  -->
<script type="text/javascript">var MouseStats_Commands=MouseStats_Commands?MouseStats_Commands:[]; (function(){function b(){if(void 0==document.getElementById("__mstrkscpt")){var a=document.createElement("script");a.type="text/javascript";a.id="__mstrkscpt";a.src=("https:"==document.location.protocol?"https://ssl":"http://www2")+".mousestats.com/js/5/2/5298657782534596732.js?"+Math.floor((new Date).getTime()/6E5);a.async=!0;a.defer=!0;(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(a)}}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load", b,!1);"complete"===document.readyState&&b()})(); </script>
<!--  MouseStats:End  -->
            
        </div>
    </body>
</html>