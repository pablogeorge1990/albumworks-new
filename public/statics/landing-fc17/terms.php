<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flight Centre | albumworks</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-fc17/common.css" />

    <script type="text/javascript">
        window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', '742cc0afc69d8afb67a6ac020f80347eee46766e');
    </script>
</head>
<body>
<header>
    <section id="strip" class="group">
        <nav id="topmenu" class="group">
            <a href="rewards" id="toprewards">
                REWARDS
            </a>
            <a href="track" id="toptrack">
                TRACK
            </a>
            <a href="faq" id="topfaq">
                FAQs
            </a>
            <a href="contact" id="topcontact">
                CONTACT
                <span>1300 553 448</span>
            </a>
        </nav>
    </section>
</header>

<div id="intro" class="panel">
    <p style="margin-bottom:10px"><img style="width:90%; max-width:452px" src="<?=getenv('BASEPATH')?>statics/landing-fc17/fclogo.jpg" /></p>
    <h1><strong>FREE $30 VOUCHER</strong></h1>
    <p class="subtext">TO USE ON ANY PHOTO BOOK PRODUCT!</p>
    <p class="subtext2">OR GET A FREE* 40 PAGE 8x6” SOFTCOVER PHOTOBOOK</p>
</div>

<div class="tycontent">
    <h2>Voucher already claimed!</h2>
    <p>Sorry - looks like you’ve already claimed your $30 Gift Voucher. Make sure to check your inbox, including your junk email folder, for your Gift Voucher.</p>

    <br/><br/>
    <h2>Got multiple people in your booking?</h2>
    <p>You can claim another Gift Voucher for other people in your booking. Just be sure to use their email address when you’re claiming their Gift Voucher!</p>
    <div class="leftright">
        <p style="text-align: center"><a href="https://www.albumworks.com.au/landing-fc17" class="cta">TRY AGAIN</a></p>
    </div>
</div>

<br/><br/>
<p class="tandc">*Terms and Conditions<br/>1. Offer valid for a $30 voucher to be used on any albumworks product.  2. Postage and handling fees apply.  3. Your $30 gift voucher will be instantly emailed to your entered email address.  4. Gift voucher is valid for 9 months from date of issue.  4. Offer is valid for one time use and any amount left over is non-refundable and cannot be used on a future order. If your order totals more than the value of your Gift Voucher, you will be prompted to pay for any difference during the ordering process.  5. Offer cannot be changed or redeemed for cash.  6. Offer valid until stock lasts.</p>

</body>
</html>