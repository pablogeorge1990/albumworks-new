<!DOCTYPE html>
<html>
    <head>
       
    
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create any Photo Book and get 30% off! | albumworks</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-feb17c/common.css?<?=time()?>" />
        
        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '742cc0afc69d8afb67a6ac020f80347eee46766e');
        </script>        
    </head>
    <body>
                   
    <div id="intro" class="panel">
        <? if(time() < mktime(0,0,0,12,20,2017)): ?>
            <header>
                <section id="promostrip" class="group">
                    <div id="promostripcontents" class="group">
                        <p class="heading">CHRISTMAS ORDER DEADLINES</p>
                        <div class="promoitem group">
                            <img src="https://www.albumworks.com.au/img/promo/xmas2017/16dec.png" alt="16 December" />
                            <p class="nomobile">Products delivered before Christmas Day to <strong>metropolitan</strong> areas*<br/>Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                            <p class="mobileonlyblock">Products delivered before Christmas Day to <strong>metropolitan</strong> areas* Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                        </div>
                        <div class="promoitem group">
                            <img src="https://www.albumworks.com.au/img/promo/xmas2017/14dec.png" alt="14 December" />
                            <p class="nomobile">Products delivered before Christmas Day to <strong>rural and remote</strong> areas*<br/>Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                            <p class="mobileonlyblock">Products delivered before Christmas Day to <strong>rural and remote</strong> areas* Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                        </div>
                    </div>
                </section>
            </header>
        <? else: ?>
            <div id="header">
                <div class="contain">
                    <a class="logo" target="_blank" href="https://www.albumworks.com.au/">
                        <img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/nav.png" />
                    </a>
                    <div id="maincta" class="nomobile">
                        <a class="cta" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <h1><strong>30% OFF</strong> ANY PRODUCT!</h1>
        <p class="subtext">FOR FIRST TIME ORDERS ONLY</p>
        <p class="centered"><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
        <div class="bottom">
            <div id="testimonials" class="contain">
                <img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/customer1.png" id="testimonial1" />
                <img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/customer2.png" id="testimonial2" />
                <div class="clr"> </div>
            </div>
            <h2>Welcome to <em>albumworks</em></h2>
            <p class="fancy">We're the experts in Photo Books. But don't just take our word for it, find out why below!</p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(1);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/down.png" /></a></p>
            <p class="onlymobile"><a class="down" href="#panel1"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/down.png" /></a></p>
        </div>
    </div>

    <div id="panel1" class="panel">
        <div class="content">
            <h3>#1</h3>
            <p class="subtext">FREE DESKTOP SOFTWARE<br>EDITOR - SIMPLE TO USE</p>
            <p>Design your Photo Book exactly how YOU want it. Choose from our pre-designed themes, or start from scratch with our easy-to-use desktop Editor. Work offline in your own time and upload photos in seconds to start creating instantly!</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(2);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/down.png" /></a></p>
        </div>
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-feb17c/cropped-computer.gif" /></p>
    </div>
  
    <div id="panel2" class="panel">
        <div class="content">
            <h3>#2</h3>
            <p class="subtext">FRIENDLY, LOCAL<br>CUSTOMER SERVICE</p>
            <p>Our amazing and friendly Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible. We're even here till 6PM on weekdays.</p>
            <p class="nomobile"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/cs-01.png" /></p>
            <p class="nomobile"><a target="_blank" href="http://www.albumworks.com.au/contact"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/cs-02.png" /></a></p>
            <p class="nomobile"><a target="_blank" href="http://www.albumworks.com.au/contact"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/cs-03.png" /></a></p>
            <p class="nomobile"><a href="javascript:void(0)" onclick="Tawk_API.showWidget(); Tawk_API.maximize();"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/cs-04.png" /></a></p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(3);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/down.png" /></a></p>
        </div>        
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-feb17c/cropped-service.jpg" /></p>
    </div>
    
    <div id="panel3" class="panel">
        <div class="content">
            <h3>#3</h3>
            <p class="subtext">GET YOUR ORDER WITHIN<br>7 BUSINESS DAYS</p>
            <p>Hate waiting? With more than 98% of all orders being dispatched within 4 business days, you'll never have to wait again!</p>
            <p class="centered"><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <div class="vidholder">
                <video width="100%" loop autoplay muted preload>
                    <source src="<?=getenv('BASEPATH')?>statics/landing-feb17c/book-vid.mp4" type="video/mp4">
                    <img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/screencap.jpg" width="100%" height="100%" />
                </video>                
            </div>
        </div>        
        <p class="bottom centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(4);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/down.png" /></a></p>
    </div>
 
    <div id="panel4" class="panel">
        <div class="content">
            <h3>#4</h3>
            <p class="subtext">MORE CHOICE: OVER 500<br>PHOTO BOOK COMBINATIONS</p>
            <p>We've expanded from 97 to 562 Photo Book cover and size combinations! Choose from 3 different book binding methods, including Classic PUR binding and Layflat binding. From fully customised Photocovers to elegant Premium Material Covers - you'll be spoilt for choice!</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(5);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/down.png" /></a></p>
        </div>        
        <img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/books.png" id="bookfloater" class="floater" />
    </div>    
             
    <div id="panel5" class="panel">
        <div class="content">
            <h3>#5</h3>
            <p class="subtext">STUNNING BOOK QUALITY</p>
            <p>We are passionate about printing top quality Photo Books. All our books are printed to international colour standards. Optionally upgrade to High Definition - stunning full photographic printing that is unmatched in Australia.</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="go_to_footer()"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/down.png" /></a></p>
        </div>        
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-feb17c/cropped-hd.jpg" /></p>
    </div>    
    
    <div id="footer" name="footer" class="panel">
        <div class="content">
            <h1><strong>30% OFF</strong> ANY PRODUCT!</h1>
            <p class="subtext">FOR FIRST TIME ORDERS ONLY</p>
            
            <h2 style="font-size:22px">Use the following Voucher Code when ordering:</h2>
            <p><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/firstorderthirty.jpg" alt="FIRSTORDERTHIRTY" /></p>
            <p class="fancy">Download our free Editor and get started today!</p>
            <p><a class="cta inline" target="_blank" href="download-thankyou.html">GET OUR FREE EDITOR</a></p>
            <p id="botlog"><a href="http://www.albumworks.com.au"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17c/logo-bottom.jpg" width="246" height="56" alt="albumworks" /></a></p>
        </div>
    </div>
                   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/landing-feb17c/common.js"></script>
    
    
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1338422-2']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        var host = window.location.host;
    </script>

                

        <div style="position:fixed">
            <script type="text/javascript">
                var current_page = 'landing-feb17c';
                var BASEPATH = 'https://www.albumworks.com.au/';
                //var BASEPATH = host;



                <? if(isset($_GET['alertmsg'])): ?>
                window.setTimeout(function(){
                    alert("<?=addslashes($_GET['alertmsg'])?>");
                }, 1000);
                <? elseif(isset($_GET['download'])): ?>
                $(document).ready(function(){
                    launch_download_lightbox();
                })
                <? endif; ?>
            </script>

            <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="https://www.albumworks.com.au/css/ie.css" />
            <script type="text/javascript" src="https://www.albumworks.com.au/js/selectivizr-min.js"></script>
            <script type="text/javascript" src="https://www.albumworks.com.au/js/css3-mediaqueries.js"></script>
            <![endif]-->
            <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js#username=albumprinter"></script>

            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-1338422-2']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
            <script type="text/javascript">var _kiq = _kiq || [];</script>
            <script type="text/javascript" src="//s3.amazonaws.com/ki.js/20502/40J.js" async="true"></script>
            <script type="text/javascript">
                var _ga = new Object;
                _ga.campaign = '';
                _ga.term = '';
                _ga.search= '';
                _ga.referrer = '';
                _ga.group = '';
                _ga.keywords = '';

                var gc = '';
                var c_name = "__utmz";
                if (document.cookie.length>0){
                    c_start=document.cookie.indexOf(c_name + "=");
                    if (c_start!=-1){
                        c_start=c_start + c_name.length+1;
                        c_end=document.cookie.indexOf(";",c_start);
                        if (c_end==-1) c_end=document.cookie.length;
                        gc = unescape(document.cookie.substring(c_start,c_end));
                    }
                }

                if(gc != ""){
                    var z = gc.split('.');
                    if(z.length >= 4){
                        var y = z[4].split('|');
                        for(i=0; i<y.length; i++){
                            if(y[i].indexOf('utmccn=') >= 0) _ga.campaign = y[i].substring(y[i].indexOf('=')+1);
                            if(y[i].indexOf('utmctr=') >= 0) _ga.term     = y[i].substring(y[i].indexOf('=')+1);
                        }
                    }
                }


                <?
                    function cleanit($string){
                        //return preg_replace("/[^A-Za-z0-9]/", '', strtolower($string));
                        return trim(strtolower($string));
                    }

                    if(strpos($_SERVER['REQUEST_URI'], '?') !== FALSE){
                        $querystring = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?')+1);
                        $queryparts = explode('&', $querystring);
                        foreach($queryparts as $part){
                            list($key, $val) = explode('=', $part);
                            $_GET[$key] = urldecode($val);
                        }
                        $_GET['q'] = $_SERVER['REQUEST_URI'];
                    }

                    if(isset($_SERVER['HTTP_REFERER'])){
                        $parts = parse_url($_SERVER['HTTP_REFERER']);
                        $_SERVER['HTTP_REFERER'] = $parts['host'];
                    }

                    if(isset($_GET['campaign'])) $_GET['campaign'] = cleanit($_GET['campaign']);
                    if(isset($_GET['group'])) $_GET['group'] = cleanit($_GET['group']);
                ?>


                <? if(isset($_GET['q'])): ?> _ga.search = "<?=addslashes($_GET['q'])?>"; <? endif; ?>

                <? if(isset($_GET['_kk'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kk']))?>"; <? endif; ?>
                <? if(isset($_GET['_kw'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kw']))?>"; <? endif; ?>

                <? if(isset($_GET['group'])): ?> _ga.group = "<?=addslashes(cleanit($_GET['group']))?>"; <? endif; ?>
                <? if(isset($_GET['campaign'])): ?> _ga.campaign = "<?=addslashes(($_GET['campaign'] == "notset") ? "" : cleanit($_GET['campaign']))?>"; <? endif; ?>
                <? if(isset($_GET['network'])): ?> _ga.network = "<?=((($_GET['network'] == 'g') or ($_GET['network'] == 's')) ? 'Search' : 'Content')?>"; <? endif; ?>

                <? if(isset($_SERVER['HTTP_REFERER'])): ?> _ga.referrer = "<?=addslashes($_SERVER['HTTP_REFERER'])?>"; <? endif; ?>
                <? if(isset($_GET['placement'])): ?> _ga.referrer = "<?=addslashes($_GET['placement'])?>"; <? endif; ?>
                <? if(isset($_GET['_placement'])): ?> _ga.referrer = "<?=addslashes($_GET['_placement'])?>"; <? endif; ?>

                <? if(isset($_GET['target'])): ?> _ga.target = "<?=addslashes($_GET['target'])?>"; <? endif; ?>
                <? if(isset($_GET['position'])): ?> _ga.position = "<?=addslashes($_GET['position'])?>"; <? endif; ?>
                <? if(isset($_GET['match'])): ?> _ga.match = "<?=addslashes($_GET['match'])?>"; <? endif; ?>

                <? if(isset($_GET['gclid'])): ?>
                createCookie('gclid','<?=addslashes($_GET['gclid'])?>',90);
                <? endif; ?>
                <? if(isset($_GET['msclkid'])): ?>
                createCookie('gclid','bing<?=addslashes($_GET['msclkid'])?>',90);
                <? endif; ?>

                jQuery('input[name=00N3600000BOyGd]').each(function(){
                    var $form = $(this).closest('form');
                    if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
                        $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
                    }
                });
            </script>

            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1060947230;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1060947230/?guid=ON&amp;script=0"/>
            </div>
            </noscript>            


            
            <!--Start of Tawk.to Script-->
            <script type="text/javascript">var Tawk_API=Tawk_API||{},Tawk_LoadStart=new Date();(function(){var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true;s1.src='https://embed.tawk.to/588ff051af9fa11e7aa51d60/default';s1.charset='UTF-8';s1.setAttribute('crossorigin','*');s0.parentNode.insertBefore(s1,s0);})();</script>
            <!--End of Tawk.to Script-->                  
            
<!--  MouseStats:Begin  -->
<script type="text/javascript">var MouseStats_Commands=MouseStats_Commands?MouseStats_Commands:[]; (function(){function b(){if(void 0==document.getElementById("__mstrkscpt")){var a=document.createElement("script");a.type="text/javascript";a.id="__mstrkscpt";a.src=("https:"==document.location.protocol?"https://ssl":"http://www2")+".mousestats.com/js/5/2/5298657782534596732.js?"+Math.floor((new Date).getTime()/6E5);a.async=!0;a.defer=!0;(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(a)}}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load", b,!1);"complete"===document.readyState&&b()})(); </script>
<!--  MouseStats:End  -->
            
        </div>
    </body>
</html>