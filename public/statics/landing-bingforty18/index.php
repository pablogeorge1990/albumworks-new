<!DOCTYPE html>
<html>
    <head>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TS5XCJX');</script>
        <!-- End Google Tag Manager -->

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create any Photo Book and get 40% off! | albumworks</title>
        <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-bingforty18/common.css?<?=time()?>" />

        <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5220373"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5220373&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '742cc0afc69d8afb67a6ac020f80347eee46766e');
        </script>        
    </head>
    <body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TS5XCJX"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="intro" class="panel">
        <? if(time() < mktime(0,0,0,12,20,2017)): ?>
            <header>
                <section id="promostrip" class="group">
                    <div id="promostripcontents" class="group">
                        <p class="heading">CHRISTMAS ORDER DEADLINES</p>
                        <div class="promoitem group">
                            <img src="https://www.albumworks.com.au/img/promo/xmas2017/16dec.png" alt="16 December" />
                            <p class="nomobile">Products delivered before Christmas Day to <strong>metropolitan</strong> areas*<br/>Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                            <p class="mobileonlyblock">Products delivered before Christmas Day to <strong>metropolitan</strong> areas* Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                        </div>
                        <div class="promoitem group">
                            <img src="https://www.albumworks.com.au/img/promo/xmas2017/14dec.png" alt="14 December" />
                            <p class="nomobile">Products delivered before Christmas Day to <strong>rural and remote</strong> areas*<br/>Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                            <p class="mobileonlyblock">Products delivered before Christmas Day to <strong>rural and remote</strong> areas* Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                        </div>
                    </div>
                </section>
            </header>
        <? else: ?>
            <div id="header">
                <div class="contain">
                    <a class="logo" target="_blank" href="https://www.albumworks.com.au/">
                        <img src="<?=getenv('BASEPATH')?>img/logo.jpg" />
                    </a>
                    <div id="maincta" class="nomobile">
                        <a class="cta" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <h1><strong>40% OFF</strong> ANY PRODUCT!</h1>
        <p class="subtext">FOR FIRST TIME ORDERS ONLY</p>
        <p class="centered"><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
        <div class="bottom">
            <div id="testimonials" class="contain">
                <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/customer1.png" id="testimonial1" />
                <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/customer2.png" id="testimonial2" />
                <div class="clr"> </div>
            </div>
            <h2>Welcome to <em>albumworks</em></h2>
            <p class="fancy">We're the experts in Photo Books. But don't just take our word for it, find out why below!</p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(1);"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/down.png" /></a></p>
            <p class="onlymobile"><a class="down" href="#panel1"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/down.png" /></a></p>
        </div>
    </div>

    <div id="panel1" class="panel">
        <div class="content">
            <h3>#1</h3>
            <p class="subtext">EASY TO USE EDITOR</p>
            <p>Design your Photo Book exactly how YOU want it. Choose from our pre-designed themes, or start from scratch with our easy-to-use Editors. Choose to make your Photo Book using our Download Editor or make Online on any device of your choice!</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(2);"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/down.png" /></a></p>
        </div>
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/cropped-computer.gif" /></p>
    </div>
  
    <div id="panel2" class="panel">
        <div class="content">
            <h3>#2</h3>
            <p class="subtext">FRIENDLY, LOCAL<br>CUSTOMER SERVICE</p>
            <p>Our amazing and friendly Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible. We're even here till 6PM on weekdays.</p>
            <p class="nomobile"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/cs-01.png" /></p>
            <p class="nomobile"><a target="_blank" href="https://www.albumworks.com.au/contact"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/cs-02.png" /></a></p>
            <p class="nomobile"><a target="_blank" href="https://www.albumworks.com.au/contact"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/cs-03.png" /></a></p>
            <p class="nomobile"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/cs-04.png" /></p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(3);"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/down.png" /></a></p>
        </div>        
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/cropped-service.jpg" /></p>
    </div>
    
    <div id="panel3" class="panel">
        <div class="content">
            <h3>#3</h3>
            <p class="subtext">GET YOUR ORDER WITHIN<br>7 BUSINESS DAYS</p>
            <p>Hate waiting? With more than 98% of all orders being dispatched within 4 business days, you'll never have to wait again!</p>
            <p class="centered"><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <div class="vidholder">
                <video width="100%" loop autoplay muted preload>
                    <source src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/book-vid.mp4" type="video/mp4">
                    <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/screencap.jpg" width="100%" height="100%" />
                </video>                
            </div>
        </div>        
        <p class="bottom centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(4);"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/down.png" /></a></p>
    </div>
 
    <div id="panel4" class="panel">
        <div class="content">
            <h3>#4</h3>
            <p class="subtext">MORE CHOICE: OVER 500<br>PHOTO BOOK COMBINATIONS</p>
            <p>We've expanded from 97 to 562 Photo Book cover and size combinations! Choose from 3 different book binding methods, including Classic PUR binding and Layflat binding. From fully customised Photocovers to elegant Premium Material Covers - you'll be spoilt for choice!</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(5);"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/down.png" /></a></p>
        </div>        
        <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/books.png" id="bookfloater" class="floater" />
    </div>    
             
    <div id="panel5" class="panel">
        <div class="content">
            <h3>#5</h3>
            <p class="subtext">STUNNING BOOK QUALITY</p>
            <p>We are passionate about printing top quality Photo Books. All our books are printed to international colour standards. Optionally upgrade to High Definition - stunning full photographic printing that is unmatched in Australia.</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="go_to_footer()"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/down.png" /></a></p>
        </div>        
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/cropped-hd.jpg" /></p>
    </div>    
    
    <div id="footer" name="footer" class="panel">
        <div class="content">
            <h1 style="margin-top:0"><strong>40% OFF</strong> ANY PRODUCT!</h1>
            <p class="subtext">FOR FIRST TIME ORDERS ONLY</p>
            <p class="mobileonlyblock"><a class="cta" href="https://www.albumworks.com.au/mobile-books">GET STARTED NOW</a></p>
            <p class="nomobile"><a style="font-size:18px" class="cta" href="javascript:void(0)" onclick="splitchoice('photo-book-picker')">GET STARTED NOW</a></p>
            <p id="botlog"><a href="https://www.albumworks.com.au"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/logo-bottom.jpg" width="246" height="56" alt="albumworks" /></a></p>
            <table cellspacing="0" cellpadding="0" class="nomobile floaters" style="width:880px; margin:40px auto">
                <tr>
                    <td colspan="6"><p class="subtext" style="color:#669fd2; padding-bottom:10px;">Got questions?</p></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/01-phone.jpg" style="margin-bottom:7px" />
                    </td>
                    <td colspan="3">
                        <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/02-email.jpg" style="margin-bottom:7px; float:right; margin-right:-4px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/03-staff.jpg" /></td>
                    <td colspan="2">
                        <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/05-logo.jpg" style="float:none;margin:10px auto;" />
                        <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/06-service.png" style="float:none;margin:10px auto;" />
                        <img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/07%20chat.png" style="float:none;margin:10px auto;" />
                    </td>
                    <td colspan="2"><img src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/04-staff.jpg" style="float:right" /></td>
                </tr>
            </table>
        </div>
    </div>
                   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/landing-bingforty18/common.js"></script>
    
    
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1338422-2']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        var host = window.location.host;
    </script>


    <style type="text/css">
        body{
            position:relative;
        }

        #splittheatre{
            width:100px;
            height:100px;
            position:fixed;
            z-index:99998;
            background:black;
            opacity:0.7;
            filter:alpha(opacity=70);
            top:0;
            left:0;
            display:none;
        }

        #splitchoice{
            text-align: left;
            padding:20px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            width:90%;
            max-width:660px;
            position:fixed;
            top:120px;
            background:white;
            z-index:99999;
            display:none;
            padding:10px 20px;
        }

        #splitchoice h3{
            font-family: 'Merriweather', serif;
            font-weight:400;
            font-size:28px;
            text-align:center;
            margin:0.5em;
        }

        #splitchoice p{
            font-weight:300;
            margin:1em 0;
        }
        #splitchoice p.dlbl{
            text-align: center;
            margin:0 auto;
            color:#0f65ba;
            font-size:12px;
            width:100%;
            font-weight:500;
            line-height:1.5;
        }

        #splitchoice .dlopt{
            width:100%;
            padding:0;
            margin-bottom:20px;
            text-align:center;
        }
        #splitchoice .dlopt:first-of-type{
            border-bottom:1px solid #0f63b9;
            border-top:3px solid #0f63b9;
            margin-top:20px;
            margin-bottom:0px;
        }

        #splitchoice .dlopt h3{
            margin:0;
        }

        #splitchoice .dlopt .dlbl{
            margin:0 0 0.5em;
        }

        #splitchoice .editorimg{
            float:left;
        }

        #splitchoice h4{
            font-size:16px;
            margin-bottom:1em;
        }

        #splitchoice .inputbox{
            border:1px solid #a0c1e4;
            padding:5px 10px;
            display:block;
            width:calc(50% - 30px);
            margin-bottom:1em;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
            font-size:16px;
            color:#0f65ba;
            font-family:'Open Sans', 'Source Sans Pro', sans-serif;
        }

        #splitchoice .inputboxp{
            margin:10px 30px 0;
        }

        #splitchoice .checkarea{
            margin:0 auto;
            border:0;
            width:50%;
            display:block;
        }

        #splitchoice .checkarea .check{
            float:left;
            margin-right:5px;
        }

        #splitchoice .checkarea label{
            font-size:9px;
            opacity:0.7;
            float:left;
            display:block;
            padding-top:5px;
            color:#0f63b9;
        }

        #splitchoice .smallprint{
            font-size:10px;
            color:#0f63b9;
        }
    </style>
    <div id="splittheatre"> </div>
    <div id="splitchoice" class="lightbox">
        <form id="splitchoice_form" class="webtolead" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkscform(this)">
            <input type="hidden" value="00D36000000oZE6" name="sfga">
            <input type="hidden" value="00D36000000oZE6" name="oid">
            <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=FORTYFIRSTB" name="retURL">
            <input type="hidden" value="Website" name="lead_source">
            <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
            <input type="hidden" value="Download" name="00N3600000BOyGd">
            <input type="hidden" value="AP" name="00N3600000BOyAt">
            <input type="hidden" value="PG" name="00N3600000Loh5K">
            <input type="hidden" name="00N3600000Los6F" value="Windows">
            <!-- Referring Promotion --><input type="hidden" value="FORTYFIRSTB" name="00N3600000LosAC">
            <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
            <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
            <!-- Adword fields -->
            <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
            <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
            <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
            <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
            <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
            <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
            <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
            <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
            <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
            <!-- Actual fields -->

            <h3>How would you like to get started?</h3>
            <p class="dlbl">CHOOSE TO MAKE YOUR PRODUCT WITH OUR HIGH PERFORMANCE</p>
            <p class="dlbl">DOWNLOAD EDITOR OR ONLINE IN YOUR WEB BROWSER</p>

            <div class="group">
                <div class="dlopt">
                    <p><img src="https://www.albumworks.com.au/img/splitchoice/download.jpg" /></p>
                    <p class="dlbl">&bull; PERFECT FOR PROJECTS BIG AND SMALL</p>
                    <p class="dlbl">&bull; WIDEST RANGE OF DESIGN TOOLS AND ABILITIES</p>
                    <p class="dlbl">&bull; SUPER FAST PERFORMANCE</p>

                    <p class="inputboxp group">
                        <input type="text" id="field_splitchoice_download_first_name" name="first_name" class="firstname inputbox" placeholder="Name" style="float:left">
                        <input type="text" id="field_splitchoice_download_email" name="email" class="email inputbox" placeholder="Email" style="float:right">
                    </p>
                    <div class="checkarea group">
                        <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
                        <label for="field_lightbox_download_emailoptout">Keep me updated with special offers and software updates</label>
                    </div>
                    <a style="margin-top:10px; padding:10px 30px; font-size: 17px;" class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">DOWNLOAD OUR FREE EDITOR</a>
                    <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and Conditions of this service</p>
                </div>
                <div class="dlopt">
                    <img src="https://www.albumworks.com.au/img/splitchoice/online.jpg" style="float:left; margin-top:20px;" />
                    <div style="float:left;margin:20px 0 0 20px">
                        <p style="margin:0 0 0.5em" class="dlbl">&bull; GREAT FOR SMALL/MEDIUM PROJECTS</p>
                        <p style="margin:0 0 0.5em" class="dlbl">&bull; CREATE ON ALL TYPES OF DEVICES</p>
                        <p style="margin:0 0 0.5em" class="dlbl">&bull; USE PHOTOS FROM YOUR DEVICE</p>
                        <a style="margin-top:1em; font-size:14px" class="cta" id="splitaltcta" href="javascript:void(0)">CREATE PRODUCTS ONLINE</a>
                        <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and<br>Conditions of this service</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        function splitchoice(onlinelink){
            $('#splittheatre').width($(document).width()).height($(document).height()).fadeIn();
            $('#splittheatre').click(function(){
                hide_splitchoice();
            });
            show_splitchoice(onlinelink);
        }

        function hide_splitchoice(){
            $('.lightbox, #splittheatre').hide();
            $('#splitchoice').hide();
        }

        function show_splitchoice(onlinelink){
            if(typeof ga !== 'undefined') { ga('send', 'event', 'Download Form', 'Open', 'Opened from "'+current_page+'"'); }

            $('#splitchoice #splitaltcta').attr('href', onlinelink);
            $('#splitchoice')
                .css('left', (($(document).width()/2)-(($('#splitchoice').width()+20)/2))+'px')
                .css('top', '20px')
                .fadeIn();
        }

        function checkscform(form){
            var $form = $(form);

            if(($form.find('input[name=email]').val() !== '') && !emailIsValid($form.find('input[name=email]').val())){
                alert('Please enter a valid email address and try again.')
                return false;
            }

            var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
            $form.find('input[name=00N3600000Los6F]').val(os_select);
            document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";

            var email = $form.find('input[name=email]').val();
            var name = ($form.find('input[name=first_name]').val() === '') ? email : $form.find('input[name=first_name]').val();
            var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
            if(email !== '')
                document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";
            else document.cookie = "download_params=vendor:ap|ou:0; ; path=/; domain=.albumworks.com.au";

            var iscookie = readCookie('download_params');
            if(iscookie)
                $form.find("input[00N3600000LosKl]").val(0);

            var refpromo = readCookie('APrefpromo');
            if(refpromo)
                $form.find("input[00N3600000LosAC]").val(refpromo);
            refpromo = $form.find('input[00N3600000LosAC]').val();
            document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";

            if(typeof gtag !== 'undefined') {
                gtag('config', 'UA-1338422-2', {'page_path': '/download_start'});
            }

            localStorage.setItem('DL_THANKYOU_NAME', name);

            return true;
        }
    </script>


        <div style="position:fixed">
            <script type="text/javascript">
                var current_page = 'landing-bingforty18';
                var BASEPATH = 'https://www.albumworks.com.au/';
                //var BASEPATH = host;



                <? if(isset($_GET['alertmsg'])): ?>
                window.setTimeout(function(){
                    alert("<?=addslashes($_GET['alertmsg'])?>");
                }, 1000);
                <? elseif(isset($_GET['download'])): ?>
                $(document).ready(function(){
                    launch_download_lightbox();
                })
                <? endif; ?>
            </script>

            <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="https://www.albumworks.com.au/css/ie.css" />
            <script type="text/javascript" src="https://www.albumworks.com.au/js/selectivizr-min.js"></script>
            <script type="text/javascript" src="https://www.albumworks.com.au/js/css3-mediaqueries.js"></script>
            <![endif]-->
            <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js#username=albumprinter"></script>

            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1060947230;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1060947230/?guid=ON&amp;script=0"/>
            </div>
            </noscript>            
            
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"> </script>
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-1338422-2']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
            <script type="text/javascript">var _kiq = _kiq || [];</script>
            <script type="text/javascript" src="//s3.amazonaws.com/ki.js/20502/40J.js" async="true"></script>
            <script type="text/javascript">
                var _ga = new Object;
                _ga.campaign = '';
                _ga.term = '';
                _ga.search= '';
                _ga.referrer = '';
                _ga.group = '';
                _ga.keywords = '';

                var gc = '';
                var c_name = "__utmz";
                if (document.cookie.length>0){
                    c_start=document.cookie.indexOf(c_name + "=");
                    if (c_start!=-1){
                        c_start=c_start + c_name.length+1;
                        c_end=document.cookie.indexOf(";",c_start);
                        if (c_end==-1) c_end=document.cookie.length;
                        gc = unescape(document.cookie.substring(c_start,c_end));
                    }
                }

                if(gc != ""){
                    var z = gc.split('.');
                    if(z.length >= 4){
                        var y = z[4].split('|');
                        for(i=0; i<y.length; i++){
                            if(y[i].indexOf('utmccn=') >= 0) _ga.campaign = y[i].substring(y[i].indexOf('=')+1);
                            if(y[i].indexOf('utmctr=') >= 0) _ga.term     = y[i].substring(y[i].indexOf('=')+1);
                        }
                    }
                }


                <?
                    function cleanit($string){
                        //return preg_replace("/[^A-Za-z0-9]/", '', strtolower($string));
                        return trim(strtolower($string));
                    }

                    if(strpos($_SERVER['REQUEST_URI'], '?') !== FALSE){
                        $querystring = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?')+1);
                        $queryparts = explode('&', $querystring);
                        foreach($queryparts as $part){
                            list($key, $val) = explode('=', $part);
                            $_GET[$key] = urldecode($val);
                        }
                        $_GET['q'] = $_SERVER['REQUEST_URI'];
                    }

                    if(isset($_SERVER['HTTP_REFERER'])){
                        $parts = parse_url($_SERVER['HTTP_REFERER']);
                        $_SERVER['HTTP_REFERER'] = $parts['host'];
                    }

                    if(isset($_GET['campaign'])) $_GET['campaign'] = cleanit($_GET['campaign']);
                    if(isset($_GET['group'])) $_GET['group'] = cleanit($_GET['group']);
                ?>


                <? if(isset($_GET['q'])): ?> _ga.search = "<?=addslashes($_GET['q'])?>"; <? endif; ?>

                <? if(isset($_GET['_kk'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kk']))?>"; <? endif; ?>
                <? if(isset($_GET['_kw'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kw']))?>"; <? endif; ?>

                <? if(isset($_GET['group'])): ?> _ga.group = "<?=addslashes(cleanit($_GET['group']))?>"; <? endif; ?>
                <? if(isset($_GET['campaign'])): ?> _ga.campaign = "<?=addslashes(($_GET['campaign'] == "notset") ? "" : cleanit($_GET['campaign']))?>"; <? endif; ?>
                <? if(isset($_GET['network'])): ?> _ga.network = "<?=((($_GET['network'] == 'g') or ($_GET['network'] == 's')) ? 'Search' : 'Content')?>"; <? endif; ?>

                <? if(isset($_SERVER['HTTP_REFERER'])): ?> _ga.referrer = "<?=addslashes($_SERVER['HTTP_REFERER'])?>"; <? endif; ?>
                <? if(isset($_GET['placement'])): ?> _ga.referrer = "<?=addslashes($_GET['placement'])?>"; <? endif; ?>
                <? if(isset($_GET['_placement'])): ?> _ga.referrer = "<?=addslashes($_GET['_placement'])?>"; <? endif; ?>

                <? if(isset($_GET['target'])): ?> _ga.target = "<?=addslashes($_GET['target'])?>"; <? endif; ?>
                <? if(isset($_GET['position'])): ?> _ga.position = "<?=addslashes($_GET['position'])?>"; <? endif; ?>
                <? if(isset($_GET['match'])): ?> _ga.match = "<?=addslashes($_GET['match'])?>"; <? endif; ?>

                <? if(isset($_GET['gclid'])): ?>
                createCookie('gclid','<?=addslashes($_GET['gclid'])?>',90);
                <? endif; ?>
                <? if(isset($_GET['msclkid'])): ?>
                createCookie('gclid','bing<?=addslashes($_GET['msclkid'])?>',90);
                <? endif; ?>

                jQuery('input[name=00N3600000BOyGd]').each(function(){
                    var $form = $(this).closest('form');
                    if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
                        $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
                    }
                });

            </script>

            <!-- Start of LiveChat (www.livechatinc.com) code -->
            <script type="text/javascript">
                window.__lc = window.__lc || {};
                window.__lc.license = 10035965;
                (function() {
                    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
                })();
            </script>
            <noscript>
                <a href="https://www.livechatinc.com/chat-with/10035965/">Chat with us</a>,
                powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
            </noscript>
            <!-- End of LiveChat code -->
            
<!--  MouseStats:Begin  -->
<script type="text/javascript">var MouseStats_Commands=MouseStats_Commands?MouseStats_Commands:[]; (function(){function b(){if(void 0==document.getElementById("__mstrkscpt")){var a=document.createElement("script");a.type="text/javascript";a.id="__mstrkscpt";a.src=("https:"==document.location.protocol?"https://ssl":"http://www2")+".mousestats.com/js/5/2/5298657782534596732.js?"+Math.floor((new Date).getTime()/6E5);a.async=!0;a.defer=!0;(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(a)}}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load", b,!1);"complete"===document.readyState&&b()})(); </script>
<!--  MouseStats:End  -->
            
        </div>
    </body>
</html>