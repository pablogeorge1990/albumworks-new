<? if(isset($_GET['product'])): ?>
    <!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Flight Centre | albumworks</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-fc18/common.css" />
    </head>
    <body>
        <header>
            <section id="strip" class="group">
                <nav id="topmenu" class="group">
                    <a href="rewards" id="toprewards">
                        REWARDS
                    </a>
                    <a href="track" id="toptrack">
                        TRACK
                    </a>
                    <a href="faq" id="topfaq">
                        FAQs
                    </a>
                    <a href="contact" id="topcontact">
                        CONTACT
                        <span>1300 553 448</span>
                    </a>
                </nav>
            </section>
        </header>

        <div id="intro" class="panel">
            <p style="margin-bottom:10px"><img style="width:90%; max-width:452px" src="<?=getenv('BASEPATH')?>statics/landing-fc18/fclogo.jpg" /></p>
            <h1 style="margin-top:50px;"><div class="lds-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div><strong style="display:block">Loading NAB Hosted Payments</strong></h1>
        </div>

        <?
            switch($_GET['product']){
                case '2':
                    ?>
                        <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" style="position:fixed; width:1px; height:1px; overflow:hidden; top:-999em; left:-999em;">
                            <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" />
                            <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
                            <input type="hidden" name="vendor_name" value="6010010" />
                            <input type="hidden" name="gst_rate" value="10">
                            <input type="hidden" name="gst_added" value="true">
                            <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
                            <input type="hidden" name="reply_link_url" id="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=COMPLEX:FCENA||40||1&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
                            <input type="hidden" name="return_link_text" value="Return to albumworks" />
                            <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/directsale-thankyou2-aug18a" />

                            <div class="field">
                                <input name="First Name" id="gv_first_name" type="text" class="inputbox" placeholder="NAME" value="<?=$_GET['name']?>" />
                                <input type="hidden" name="information_fields" value="First Name" />
                                <input type="hidden" name="required_fields" value="First Name" />
                            </div>

                            <div class="field">
                                <input name="Email" id="gv_email" type="text" class="inputbox" placeholder="EMAIL" value="<?=$_GET['email']?>" />
                                <input type="hidden" name="information_fields" value="Email" />
                                <input type="hidden" name="required_fields" value="Email" />
                                <input type="hidden" name="receipt_address" value="<<Email>>" />
                            </div>

                            <input type="hidden" name="Gift Voucher" id="gvfield" value="40.00" />

                            <div class="clr"></div>
                            <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style="">BUY NOW</a>
                            <div class="clr"></div>
                        </form>
                    <?
                    break;

                case '3':
                    ?>
                        <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" style="position:fixed; width:1px; height:1px; overflow:hidden; top:-999em; left:-999em;">
                            <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" />
                            <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
                            <input type="hidden" name="vendor_name" value="6010010" />
                            <input type="hidden" name="gst_rate" value="10">
                            <input type="hidden" name="gst_added" value="true">
                            <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
                            <input type="hidden" name="reply_link_url" id="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=COMPLEX:FCENB||40||1&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
                            <input type="hidden" name="return_link_text" value="Return to albumworks" />
                            <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/directsale-thankyou2-aug18b" />

                            <div class="field">
                                <input name="First Name" id="gv_first_name" type="text" class="inputbox" placeholder="NAME" value="<?=$_GET['name']?>"  />
                                <input type="hidden" name="information_fields" value="First Name" />
                                <input type="hidden" name="required_fields" value="First Name" />
                            </div>

                            <div class="field">
                                <input name="Email" id="gv_email" type="text" class="inputbox" placeholder="EMAIL" value="<?=$_GET['email']?>" />
                                <input type="hidden" name="information_fields" value="Email" />
                                <input type="hidden" name="required_fields" value="Email" />
                                <input type="hidden" name="receipt_address" value="<<Email>>" />
                            </div>

                            <input type="hidden" name="Gift Voucher" id="gvfield" value="50.00" />

                            <div class="clr"></div>
                            <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style="">BUY NOW</a>
                            <div class="clr"></div>
                        </form>
                    <?
                    break;
            }
        ?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('form').submit();
            });
        </script>
    </body>
</html>
<? else: ?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Flight Centre | albumworks</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-fc18/common.css" />
    </head>
    <body>
    <header>
        <section id="strip" class="group">
            <nav id="topmenu" class="group">
                <a href="rewards" id="toprewards">
                    REWARDS
                </a>
                <a href="track" id="toptrack">
                    TRACK
                </a>
                <a href="faq" id="topfaq">
                    FAQs
                </a>
                <a href="contact" id="topcontact">
                    CONTACT
                    <span>1300 553 448</span>
                </a>
            </nav>
        </section>
    </header>
                   
    <div id="intro" class="panel">
        <p style="margin-bottom:10px"><img style="width:90%; max-width:452px" src="<?=getenv('BASEPATH')?>statics/landing-fc18/fclogo.jpg" /></p>
        <h1><strong>SELECT YOUR PHOTO BOOK OFFER!</strong></h1>
        <p class="subtext">Choose the right book for your project</p>

        <div class="bookopts group">
            <div class="bookopt">
                <img src="<?=getenv('BASEPATH')?>statics/landing-fc18/products/01.jpg"/>
                <h4>8x6" SOFTCOVER PHOTO BOOK</h4>
                <p>&nbsp;</p>
                <h5>Pay: $0.00*</h5>
                <h6><span>Worth: $29.95</span></h6>
                <p><input type="checkbox" id="checkbox1" retURL="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&landing-fc17&referringPromotion=FLIGHTCENTRE" /></p>
            </div>
            <div class="bookopt selected">
                <img src="<?=getenv('BASEPATH')?>statics/landing-fc18/products/02.jpg" />
                <h4>11x8.5" PHOTOCOVER PHOTO BOOK</h4>
                <p>with Free Shipping</p>
                <h5>Pay: $40.00^</h5>
                <h6><span>Worth: $81.95</span></h6>
                <p><input type="checkbox" id="checkbox2" checked="checked" retURL="https://www.albumworks.com.au/landing-fc18?product=2" /></p>
            </div>
            <div class="bookopt">
                <img src="<?=getenv('BASEPATH')?>statics/landing-fc18/products/03.jpg" />
                <h4>12x12" PHOTOCOVER PHOTO BOOK</h4>
                <p>with Free Shipping</p>
                <h5>Pay: $50.00^</h5>
                <h6><span>Worth: $101.90</span></h6>
                <p><input type="checkbox" id="checkbox3" retURL="https://www.albumworks.com.au/landing-fc18?product=3" /></p>
            </div>
        </div>
    </div>

    <p class="tandc" style="font-size:14px; margin-top:0;">
        * Shipping charged at $8.95<br>
        ^ Valid for 40 Standard Definition pages. Any upgrades or extra pages will be charged.
    </p>

    <div class="steps group">
        <div class="step">
            <p class="center"><img src="<?=getenv('BASEPATH')?>statics/landing-fc18/step1.jpg" alt="1" /></p>
            <p>Fill out the form below toreceive your chosen offer anddownload the desktop Editor</p>
        </div>
        <div class="step">
            <p class="center"><img src="<?=getenv('BASEPATH')?>statics/landing-fc18/step2.jpg" alt="2" /></p>
            <p>Create your Photo Book using the albumworks Editor</p>
        </div>
        <div class="step">
            <p class="center"><img src="<?=getenv('BASEPATH')?>statics/landing-fc18/step3.jpg" alt="3" /></p>
            <p>Use your Gift Voucher Codewhen placing your order</p>
        </div>
    </div>
                       
    <div id="footer" name="footer" class="panel">
        <div class="content">                               
            <p class="fancy">Enter your details to receive your Photo Book Voucher and get started!</p>
            <form class="box" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/landing-fc18?product=2" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <input type="hidden" value="FLIGHTCENTRE" name="00N3600000LosAC">
                <input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                <input type="text" placeholder="NAME" name="first_name" class="firstname smartedit inputbox" alt="NAME">
                <input type="text" placeholder="EMAIL" name="email" class="email smartedit inputbox" alt="EMAIL">
                <input type="text" placeholder="BOOKING NUMBER" name="bookingnum" class="bookingnum smartedit inputbox" alt="BOOKING NUMBER">
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET YOUR VOUCHER</a>
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check" style="position:fixed; left:-9999em; top:-9999em">
                    <label>By downloading the <em>albumworks</em> editor you agree to receive communication on special offers and software updates. You can unsubscribe at any time.</label>
                </div>
                <div class="clr"> </div>
            </form>
        </div>
    </div>

    <p class="tandc">*Terms and Conditions<br/>1. Offer valid for specific Photo Book chosen. However Gift Voucher value can be used on any product for the total Voucher amount.  2. Postage and handling fees apply.  3. Your Gift Voucher will be instantly emailed to your entered email address.  4. Gift voucher is valid for 9 months from date of issue.  4. Offer is valid for one time use and any amount left over is non-refundable and cannot be used on a future order. If your order totals more than the value of your Gift Voucher, you will be prompted to pay for any difference during the ordering process.  5. Offer cannot be changed or redeemed for cash.  6. Offer valid until stock lasts.</p>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/landing-fc18/common.js"></script>

        <div style="position:fixed">
            <script type="text/javascript">
                var current_page = 'landing-fc18';
                var BASEPATH = 'https://www.albumworks.com.au/';

                $(document).ready(function(){
                    $('.bookopt h4, .bookopt h5, .bookopt h6, .bookopt img').click(function(){
                        $(this).siblings('p').find('input').click();
                    });

                    $('input[type=checkbox]').change(function(e){
                        var allunticked = true;
                        $('input[type=checkbox]').each(function(){
                            if($(this).prop('checked')){
                                allunticked = false;
                            }
                        });
                        if(allunticked){
                            $(this).prop('checked', true);
                        }

                        if($(this).prop('checked')) {
                            $('input[type=checkbox]').prop('checked', false);
                            $(this).prop('checked', true);
                            $('.bookopt').removeClass('selected');
                            $(this).closest('.bookopt').addClass('selected');
                            $('form input[name=retURL]').val($(this).attr('retURL'));
                        }
                    });
                });
            </script>

            <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="https://www.albumworks.com.au/css/ie.css" />
            <script type="text/javascript" src="https://www.albumworks.com.au/js/selectivizr-min.js"></script>
            <script type="text/javascript" src="https://www.albumworks.com.au/js/css3-mediaqueries.js"></script>
            <![endif]-->

            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-1338422-2']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
        </div>
    </body>
</html>
<? endif; ?>