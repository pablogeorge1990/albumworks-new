var __fadeasel = null;
var __windowwidth = 0;
var __windowheight = 0;

;(function(d){var k=d.scrollTo=function(a,i,e){d(window).scrollTo(a,i,e)};k.defaults={axis:'xy',duration:parseFloat(d.fn.jquery)>=1.3?0:1};k.window=function(a){return d(window)._scrollable()};d.fn._scrollable=function(){return this.map(function(){var a=this,i=!a.nodeName||d.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!i)return a;var e=(a.contentWindow||a).document||a.ownerDocument||a;return e.documentElement})};d.fn.scrollTo=function(n,j,b){if(typeof j=='object'){b=j;j=0}if(typeof b=='function')b={onAfter:b};if(n=='max')n=9e9;b=d.extend({},k.defaults,b);j=j||b.speed||b.duration;b.queue=b.queue&&b.axis.length>1;if(b.queue)j/=2;b.offset=p(b.offset);b.over=p(b.over);return this._scrollable().each(function(){var q=this,r=d(q),f=n,s,g={},u=r.is('html,body');switch(typeof f){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(f)){f=p(f);break}f=d(f,this);case'object':if(f.is||f.style)s=(f=d(f)).offset()}d.each(b.axis.split(''),function(a,i){var e=i=='x'?'Left':'Top',h=e.toLowerCase(),c='scroll'+e,l=q[c],m=k.max(q,i);if(s){g[c]=s[h]+(u?0:l-r.offset()[h]);if(b.margin){g[c]-=parseInt(f.css('margin'+e))||0;g[c]-=parseInt(f.css('border'+e+'Width'))||0}g[c]+=b.offset[h]||0;if(b.over[h])g[c]+=f[i=='x'?'width':'height']()*b.over[h]}else{var o=f[h];g[c]=o.slice&&o.slice(-1)=='%'?parseFloat(o)/100*m:o}if(/^\d+$/.test(g[c]))g[c]=g[c]<=0?0:Math.min(g[c],m);if(!a&&b.queue){if(l!=g[c])t(b.onAfterFirst);delete g[c]}});t(b.onAfter);function t(a){r.animate(g,j,b.easing,a&&function(){a.call(this,n,b)})}}).end()};k.max=function(a,i){var e=i=='x'?'Width':'Height',h='scroll'+e;if(!d(a).is('html,body'))return a[h]-d(a)[e.toLowerCase()]();var c='client'+e,l=a.ownerDocument.documentElement,m=a.ownerDocument.body;return Math.max(l[h],m[h])-Math.min(l[c],m[c])};function p(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);
(function(){var a={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(a){for(var b=0;b<a.length;b++){var c=a[b].string;var d=a[b].prop;this.versionSearchString=a[b].versionSearch||a[b].identity;if(c){if(c.indexOf(a[b].subString)!=-1)return a[b].identity}else if(d)return a[b].identity}},searchVersion:function(a){var b=a.indexOf(this.versionSearchString);if(b==-1)return;return parseFloat(a.substring(b+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};a.init();window.jQuery.client={os:a.OS,browser:a.browser}})();
         
$(document).ready(function(){
   __windowwidth = $(window).width();
    
   if(__windowwidth <= 800){
        $('.sizeme').width(__windowwidth);
    }

});

function go_to_footer(){  
    $('input[name=first_name]').focus();
}

function showsel(a){
    window.clearInterval(__fadeasel);

    var $li = $(a).parent();
    if(!$li.hasClass('active')){
        $li.siblings().removeClass('active');
        $li.addClass('active');

        var index = $li.attr('index');
        var $fadeasel = $li.parent().prev();
        var $a = $fadeasel.children('a[index='+index+']');
        $a.hide();
        $fadeasel.prepend($a);
        $a.fadeIn(1000);
    }
}

function count_substring(haystack, needle){
    return haystack.match(new RegExp(needle, "g")).length
}

function stop_boxes($boxset){
    $boxset.data('autoplay', false);
}

function scroll_boxes(direction, arrow){
    var $boxset = $(arrow).closest('.boxes');
    var $slider = $boxset.find('.boxslider');
    var count = $boxset.find('.boxwrapper').length;
    var blockwidth = $boxset.find('.boxwrapper:first').width()+10;
    var current = (parseInt($boxset.data('current')) + direction);
    if((parseInt($boxset.data('current')) + direction) < 1)
        current = count;
    else if((parseInt($boxset.data('current')) + direction) > count)
        current = 1;

    if(!$boxset.data('animating')){
        $boxset.data('animating', true);
        $boxset.data('current', current);

        if(direction < 0){
            for(i=0; i<Math.abs(direction); i++) $boxset.find('.boxwrapper:last').prependTo($slider);
            $slider.css('left', (direction*blockwidth)+'px');
            $slider.animate({left:'0px'}, 400, function(){
                $boxset.data('animating', false);
            });
        }
        else if(direction > 0){
            $slider.animate({left:'-'+(direction*blockwidth)+'px'}, 400, function(){
                for(i=0; i<Math.abs(direction); i++) $boxset.find('.boxwrapper:first').appendTo($slider);
                $slider.css('left', '0px');
                $boxset.data('animating', false);
            });
        }
    }
}

function stop_carousel($carousel){
    $carousel.data('autoplay', false);
}

function carousel_goto(newpos, $carousel){
    var currentpos = parseInt($carousel.find('.dot.active').attr('rel'));
    scroll_carousel((newpos - currentpos), $carousel);
}

function scroll_carousel(direction, $carousel){
    var $slider = $carousel.find('.slider');
    var count = $carousel.find('.item').length;
    var blockwidth = $carousel.find('.item:first').width();
    var current = (parseInt($carousel.data('current')) + direction);
    if((parseInt($carousel.data('current')) + direction) < 1)
        current = count;
    else if((parseInt($carousel.data('current')) + direction) > count)
        current = 1;

    if(!$carousel.data('animating')){
        $carousel.data('animating', true);
        $carousel.data('current', current);

        if(direction < 0){
            for(i=0; i<Math.abs(direction); i++) $carousel.find('.item:last').prependTo($slider);
            $slider.css('left', (direction*blockwidth)+'px');
            $slider.animate({left:'0px'}, 400, function(){
                if($carousel.find('.dot.active').length){
                    var currentpos = parseInt($carousel.find('.dot.active').attr('rel'));
                    var newpos = ((currentpos + direction) <= 0) ? $carousel.find('.dot').length - Math.abs(currentpos + direction) : (currentpos + direction);
                    $carousel.find('.dot.active').removeClass('active');
                    $carousel.find('.dot:nth-child('+newpos+')').addClass('active');
                }
                $carousel.data('animating', false);
            });
        }
        else if(direction > 0){
            $slider.animate({left:'-'+(direction*blockwidth)+'px'}, 400, function(){
                for(i=0; i<Math.abs(direction); i++) $carousel.find('.item:first').appendTo($slider);
                $slider.css('left', '0px');
                if($carousel.find('.dot.active').length){
                    var currentpos = parseInt($carousel.find('.dot.active').attr('rel'));
                    var newpos = ((currentpos + direction) > $carousel.find('.dot').length) ? Math.abs(currentpos + direction) - $carousel.find('.dot').length : (currentpos + direction);
                    $carousel.find('.dot.active').removeClass('active');
                    $carousel.find('.dot:nth-child('+newpos+')').addClass('active');
                }
                $carousel.data('animating', false);
            });
        }
    }
}

function checkdlform(form){
    var $form = $(form);
    var errors = false;
    $form.find('.inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });

    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }

    if(!emailIsValid($form.find('input[name=email]').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }

    var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
    $form.find('input[name=00N3600000Los6F]').val(os_select);
    document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";

    var name = $form.find('input[name=first_name]').val();
    var email = $form.find('input[name=email]').val();                                
    document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:0|ou:0; ; path=/; domain=.albumworks.com.au";

    var iscookie = readCookie('download_params');
    if(iscookie)
        $form.find("#00N3600000LosKl").val(0);

    var refpromo = readCookie('APrefpromo');
    if(refpromo)
        $form.find("#00N3600000LosAC").val(refpromo);
    refpromo = $form.find('#00N3600000LosAC').val();
    document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";

    if(typeof _gaq !== 'undefined') { _gaq.push(['_trackPageview', '/download_start']); }
           
    return addfields();
}

function addfields(){
    $('form input[name=retURL]').val($('form input[name=retURL]').val()+"&name="+urlencode($('form input[name=first_name]').val()));
    $('form input[name=retURL]').val($('form input[name=retURL]').val()+"&email="+urlencode($('form input[name=email]').val()));
}

function emailIsValid(inputvalue){
    var pattern = /^(([^<>()[\]\\.,;:\s@\"#]+(\.[^<>()[\]\\.,;:\s@\"#]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return pattern.test(inputvalue);
}

function launch_download_lightbox(){
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ){
        lightbox_mobilecta();
        return;
    }
    else{
        $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
        $('#theatre').click(function(){
            hide_hoverform();
        });
        show_downloadform();
    }
}
function hide_hoverform(){
    $('.lightbox, #theatre').fadeOut();
    $('#lightboxiframehover').fadeOut(function(){
        $('#lightboxiframehover').remove();
        $('#mobilecta').remove();
    });
}

function show_downloadform(){
    _gaq.push(['_trackEvent', 'Download Form', 'Open', 'Opened from "'+current_page+'"']);

    $('#lightbox_download')
        .css('left', (($(document).width()/2)-(($('#lightbox_download').width()+20)/2))+'px')
        .css('top', '20px')
        .fadeIn();
}

function lightbox_video(url){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });

    $('body').append('<div class="lightbox" id="lightboxiframehover"><iframe allowtransparency="true" src="'+url+'" width="100%" height="100%" scrolling="auto" align="top" frameborder="0" allowfullscreen></iframe>');
    $('#lightboxiframehover').css('left', (($(document).width()/2)-(($('#lightboxiframehover').width()+20)/2))+'px').css('top', (($(window).height()/2)-(($('#lightboxiframehover').height()+20)/2))+'px').fadeIn();
}

function lightbox_mobilecta(){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });

    $('body').append('\
        <div class="lightbox" id="mobilecta" style="text-align:center">\
            <a href="javascript:void(0)" class="close" onclick="$(\'#theatre\').click();">Close</a>\
            <p><img src="'+BASEPATH+'img/popup/aw.jpg" style="width:80%" /></p>\
            <p class="header">I want to...</p>\
            <p><a class="cta" href="javascript:void(0)" onclick="lightbox_mobilecta2()">MAKE A PHOTO BOOK</a></p>\
            <p><a class="cta" href="'+BASEPATH+'all-products">MAKE ANOTHER PRODUCT</a></p>\
            <p><a style="font-size:16px" href="javascript:void(0)" onclick="$(\'#theatre\').click();"><img style="margin-right:5px;" src="'+BASEPATH+'img/popup/x.jpg" />Close to browse</a></p>\
            ');
    var topval = (($(window).height()/2)-(($('#mobilecta').height())/2)) < 20 ? 20 : (($(window).height()/2)-(($('#mobilecta').height())/2));
    $('#mobilecta').css('left', (($(document).width()/2)-(($('#mobilecta').width()+20)/2))+'px').css('top', topval+'px').fadeIn();
}

function lightbox_mobilecta2(){
    if(!$('#theatre').is(':visible')){
        $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
        $('#theatre').click(function(){
            hide_hoverform();
        });
    }

    $('#mobilecta').remove();
    $('body').append('\
        <div class="lightbox" id="mobilecta" style="text-align:center">\
            <a href="javascript:void(0)" class="close" onclick="$(\'#theatre\').click();">Close</a>\
            <p class="header">Want to make a Photo Book?</p>\
            <p><img src="'+BASEPATH+'img/popup/p1.jpg" width="80%" /><br>Go to our website on a desktop computer or laptop.</p>\
            <p><img src="'+BASEPATH+'img/popup/p2.jpg" width="80%" /><br>Click on GET STARTED NOW</p>\
            <p><img src="'+BASEPATH+'img/popup/p3.jpg" width="80%" /><br>Download our easy to use Editor.</p>\
            <hr/>\
            <p class="header">Want to</p>\
            <p><a class="cta" href="'+BASEPATH+'buy-now-make-later">BUY NOW, MAKE LATER?</a></p>\
            <hr/>\
            <p class="header">Want to make another product using mobile or tablet?</p>\
            <p><a class="cta" href="'+BASEPATH+'all-products">ALL PRODUCTS</a></p>\
            <p><a style="font-size:16px" href="javascript:void(0)" onclick="$(\'#theatre\').click();"><img style="margin-right:5px;" src="'+BASEPATH+'img/popup/x.jpg" />Close to browse</a></p>\
            ');
    var topval = 20;//(($(window).height()/2)-(($('#mobilecta').height())/2)) < 20 ? 20 : (($(window).height()/2)-(($('#mobilecta').height())/2));
    $('#mobilecta').css('left', (($(document).width()/2)-(($('#mobilecta').width()+20)/2))+'px').css('top', topval+'px').fadeIn();
}

function validatecontact(){
    var errors = false;
    $('#emailForm .inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });

    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }

    if(!emailIsValid($('#emailForm #contact_email').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }

    return true;
}

function validaterewards(){
    var errors = false;
    $('#rewardForm .inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });

    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }

    if(!emailIsValid($('#rewardForm #contact_email').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }

    window.location = BASEPATH+'mybalance/'+$('#rewardForm #contact_email').val();
    return false;
}

function validateGvForm() {
    var fields = ['gv_first_name','gv_email','gv_giftvoucher'];
    var val = '';
    var fail = false;
    $.each(fields, function() {
        if($("#" + this).val() == '') {
            fail = true;
        }
    });
    if(fail == true) {
        alert('Please fill in all fields and try again');
        return false;
    }
    if(!emailIsValid($('#gv_email').val())){
        alert('Please check your email address and try again');
        return false;
    }
    return true;
}

function open_pricingfold(product, foldup){
    var $foldup = $('#'+product+'_pricing .foldup.'+foldup);
    var $handle = $foldup.next();
    if(!$foldup.is(':visible')){
        $.scrollTo('a[name='+foldup+']', 0);
        $foldup.slideDown(function(){
            //$.scrollTo('a[name='+foldup+']', 0);
            $handle.addClass('active');
        });
    }
}

function close_pricingfold(product, foldup){
    var $foldup = $('#'+product+'_pricing .foldup.'+foldup);
    var $handle = $foldup.next();
    if($foldup.is(':visible')){
        $foldup.slideUp(function(){
            $handle.removeClass('active');
        });
    }
}

function toggle_pricingfold(product, foldup){
    var $foldup = $('#'+product+'_pricing .foldup.'+foldup);
    if($foldup.is(':visible'))
        close_pricingfold(product, foldup);
    else open_pricingfold(product, foldup);
}

function update_price(){
    var total = 0;
    $('.cost').each(function(){
        total += parseFloat($(this).find('option:selected').data('price'))
    });
    $('#total').text('$'+total.toFixed(2));
}

function toggle_menu(){
    var $handle = $('#menuburger');
    var $left = $('#topmenu');
    if($left.css('position') == 'absolute'){
        if(parseInt($left.css('left')) == 0){
            $left.animate({
                left:'-195px'
            }, 300);
            $handle.animate({
                left:'195px'
            }, 300);
        }
        else if(parseInt($left.css('left')) == -195){
            $left.animate({
                left:'0px'
            }, 300);
            $handle.animate({
                left:'385px'
            }, 300);
        }
    }
}

function brochurelink_activate(link, url){
    $(link).css('font-weight', 600);
    $(link).siblings('a').css('font-weight', 300);
    $(link).closest('.brochure').css('background-image', 'url('+url+')');
}

function urlencode (str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
        replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}
