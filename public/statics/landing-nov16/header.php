<!DOCTYPE html>
<html>
    <head>
        <title>Create any Photo Book and get 30% off! | albumworks</title>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,300,400,600,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-nov15/common.css?v2" />
    </head>
<body>
    <div id="container">
        <div id="header">
            <a class="logo" target="_blank" href="https://www.albumworks.com.au/">albumworks</a>
            <img class="logoside" src="<?=getenv('BASEPATH')?>statics/landing-nov15/logo-side.png" width="401" height="66" />
            <div id="maincta">
                <a class="cta" href="#join">GET OUR FREE EDITOR</a>
            </div>
        </div>
        <div id="body">