<? include('header.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a4ls/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a4ls/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a4ls/03.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a4ls/04.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>30% OFF</span><br>11 x 8.5" Landscape Softcover Book - 40 pages</h2>
        <h3><em>Buy Now, Make Later!</em><br>Every page tells your story, in stunning <em>albumworks</em> quality.</h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook.</li>
            <li>Most Photo Books come with 20 pages, but our gorgeous books come with 40 pages!</li>
            <li>Full colour, premium Matte laminate softcover</li>
            <li>Printed on 190gsm E-Photo Lustre paper</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Buy Now, Make Later (3 months to make book)</li>
        </ul>

        <h3>Enter your details below and we'll email you a voucher for the amount of your Photo Book.</h3>
        <?
            $promo = 'GAWA4LS';
            $product = 'a4ls';
            $faceprice = '34.95';
            $wasprice = '49.95';
            
            include('footer.php');