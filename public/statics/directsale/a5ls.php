<? include('header.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a5ls/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a5ls/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a5ls/03.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/a5ls/04.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>30% OFF</span><br>8 x 6" Landscape Softcover Photo Book</h2>
        <h3><em>Buy Now, Make Later!</em><br>Every page tells your story, in stunning <em>albumworks</em> quality.</h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook.</li>
            <li>40 full colour pages</li>
            <li>Printed on 190gsm E-Photo Lustre paper</li>
            <li>Full colour, premium Matte laminate softcover</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Buy Now, Make Later (3 months to make book)</li>
        </ul>

        <h3>Enter your details below and we'll email you a voucher for the amount of your Photo Book.</h3>
        <?
            $promo = 'GAWA5LS';
            $product = 'a5ls';
            $faceprice = '13.95';
            $wasprice = '19.95';
            
            include('footer.php');