<? include('header.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gawa4lstrav/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gawa4lstrav/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gawa4lstrav/03.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gawa4lstrav/04.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>30% OFF</span><br>11 x 8.5" Landscape Travel Photo Book</h2>
        <h3><em>Buy Now, Make Later!</em><br>Every page tells your story, in stunning <em>albumworks</em> quality.</h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook.</li>
            <li>Most Photo Books come with 20 pages, but our gorgeous books come with 40 pages!</li>
            <li>Full colour, premium Matte laminate softcover</li>
            <li>Printed on 190gsm E-Photo Lustre paper</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Upgrade to HD, upsize, or add more pages in the Editor*</li>
            <li>Full local, friendly support to help make your book</li>
            <li>Buy Now, Make Later (3 months to make book)</li>
        </ul>
        <p class="note">*This voucher will cover the cost of an SD 11x8.5" Landscape Softcover Photo Book with 40 pages. You will be able to upgrade to HD, upsize, or add pages in the Editor, and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll email you a voucher for the amount of your Photo Book.</h3>
        <?
            $promo = 'GAWA4LSTRAV';
            $product = 'gawa4lstrav';
            $faceprice = '34.95';
            $wasprice = '49.95';
            
            include('footer.php');