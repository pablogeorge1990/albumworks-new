<? include('header.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/cal17/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/cal17/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/cal17/03.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>30% OFF</span><br>8 x 11" Wall Calendar</h2>
        <h3><em>Buy Now, Make Later!</em><br>Every month tells your story, in stunning <em>albumworks</em> quality.</h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook</li>
            <li>Printed on thick, 270gsm Uncoated Matte paper - pen friendly surface</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Upgrade to HD for stunning 250gsm Glossy photo paper*</li>
            <li>Full local, friendly support to help you create your project</li>
            <li>Buy Now, Make Later (3 months to make your Calendar)</li>
        </ul>

        <p style="font-size:80%; font-style: italic; opacity:0.85">*This voucher will cover the cost of an SD 8x11” Wall Calendar. You will be able to upgrade to HD or add extra copies when ordering and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll email you a voucher for the amount of your Calendar.</h3>
<?
$promo = 'GAWCAL17';
$product = 'cal17';
$faceprice = '19.57';
$wasprice = '27.95';

include('footer.php');