<? include('header.php'); ?>
    <div class="viewer">
        <div class="primary" style="background-image: url(<?=getenv('BASEPATH')?>statics/directsale/<?=str_replace('.php', '', $_GET['product'])?>/01.jpg);"></div>
    </div>
    <div class="copy">
        <h2>Thanks so much for your purchase.</h2>
        <h3>A Voucher for the cost of your selected products is being emailed to you. </h3>
        <h3>To start making your project, you just need to download our simple-to-use Editor, by clicking "Get Started".</h3>
        
        <p><a href="javascript:void(0)" onclick="launch_download_lightbox()" class="button cta inline" type="submit" style="">GET STARTED</a></p>

        <p><br>More information on <a style="padding:0 20px 0 5px" href="https://www.albumworks.com.au/photo-books-howto" class="cta inline green2">HOW TO MAKE YOUR PHOTO BOOK</a> </p>

        <p>If you have any problems, please don't hesitate to contact our Customer Service team on 1300 553 448.</p>
        
    </div>
</div></div>

    <div id="theatre"> </div>
    <div id="lightbox_download" class="lightbox">
        <form id="lightbox_dlform_form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
            <input type="hidden" value="00D36000000oZE6" name="sfga">
            <input type="hidden" value="00D36000000oZE6" name="oid">
            <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap" name="retURL">
            <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
            
            <!-- Actual fields -->
            <img src="<?=getenv('BASEPATH')?>statics/directsale/lighbox_download.png" alt="GET STARTED NOW" />
            <p>You're seconds away from designing a beautiful keepsake using your photos!</p>
            <p>Enter your details below to create an <em>albumworks</em> account, and to begin working on your project.</p>                
            <h4>GET STARTED</h4>
            <label for="field_lightbox_download_first_name">NAME:</label>
            <input type="text" id="field_lightbox_download_first_name" name="first_name" class="firstname smartedit inputbox">
            <label for="field_lightbox_download_email">EMAIL:</label>
            <input type="text" name="email" id="field_lightbox_download_email" class="email smartedit inputbox">
            <div class="checkarea">
                <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
                <label for="field_lightbox_download_emailoptout">Keep me updated with special offers, promotions and software updates</label>
            </div>
            <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">DOWNLOAD EDITOR FOR FREE</a>
            <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and Conditions of this service</p>
            <div class="clr"> </div>
        </form>
    </div>
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/directsale/common.js"></script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1338422-2']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';                
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script> 
</body>
</html>