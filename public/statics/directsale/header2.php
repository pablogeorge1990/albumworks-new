<!DOCTYPE html>
<html>
<head>
    <title>Photo Books by albumworks</title>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,300,400,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/directsale/common.css?v2" />
</head>
<body style="background:white">
<header>
    <section id="strip" class="group">
        <nav id="topmenu" class="group">
            <a href="{{env('BASEPATH')}}rewards" id="toprewards">
                REWARDS
            </a>
            <a href="{{env('BASEPATH')}}track" id="toptrack">
                TRACK
            </a>
            <a href="{{env('BASEPATH')}}faq" id="topfaq">
                FAQs
            </a>
            <a href="{{env('BASEPATH')}}contact" id="topcontact">
                CONTACT
                <span>1300 553 448</span>
            </a>
        </nav>
    </section>
    <section id="logo">
        <a href="https://www.albumworks.com.au"><img src="https://www.albumworks.com.au/img/whitelogo.jpg" alt="albumworks photo books" /></a>
    </section>
</header>
<div id="container">
    <div id="body">