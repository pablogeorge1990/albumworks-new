<?php if(time() > mktime(0,0,0,1,8,2022)) header('Location: https://www.albumworks.com.au'); ?>
<? include('header2.php'); ?>
<?
$promo = 'NOV21C';
$product = 'nov21c';

?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/nov21c/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/nov21c/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/nov21c/03.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2 style="font-size:36px"><span style="font-size:42px; color:#eb481d">16 x 12" Photo Book</span><br>Photocover</h2>
        <h3 style="margin:1em 0 0.5em"><em>Buy Now, Make Later!</em><br><br>WHAT'S INCLUDED:</h3>
        <ul>
            <li>SD 150gsm Premium Silk paper</li>
            <li>Full colour hard photocover. Choose premium Matte or Glossy finish</li>
            <li>Local, friendly support to help make your book</li>
            <li>Buy Now, Make Later. 6 months from date of purchase</li>
        </ul>

        <h5>Customise your book:</h5>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="width:135px; padding:5px 0"><h5 style="font-size:22px">Pages:</h5></td>
                <td style="padding:5px 0">
                    <select name="pages" id="field_pages" onchange="change_price()" style="width: 100%; padding: 5px; font-size: 16px; border: 1px solid silver;">
                        <option value="40">40 Pages</option>
                        <option value="70">70 Pages</option>
                        <option value="100">100 Pages</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="padding:5px 0"><h5 style="font-size:22px">Quantity*:</h5></td>
                <td style="padding:5px 0">
                    <select name="quantity" id="field_quantity" onchange="change_price()" style="width: 100%; padding: 5px; font-size: 16px; border: 1px solid silver;">
                        <? for($i = 1; $i <= 20; $i++): ?>
                            <option value="<?=$i?>"><?=$i?></option>
                        <? endfor; ?>
                    </select>
                </td>
            </tr>
        </table>

        <p class="buysub" style="margin:1em 0; color:#e65b44">
            This voucher will cover the cost of an SD 16x12" Photocover Photo Book with your selected amount of pages and quantity. You will be able to upgrade to HD or add extra copies when ordering and you will simply pay the difference at the time of ordering.
            <br><br>
            *Quantity refers to the number of identical copies you wish to purchase.<br>
            If you plan on making separate individual Photo Books, you will need to purchase separate individual pre-paid Vouchers.
        </p>

        <h2 id="wasnow">Was <s>$124.95</s> <strong style="color:#e84b18">Now $87.50</strong></h2>

        <h3>Enter your details below and we'll email you a voucher to use when ordering your product.</h3>

        <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" onsubmit="return validateGvForm()"> <!-- test to live -->
            <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" />
            <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
            <input type="hidden" name="vendor_name" value="6010010" />
            <input type="hidden" name="gst_rate" value="10">
            <input type="hidden" name="gst_added" value="true">
            <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
            <input type="hidden" name="reply_link_url" id="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=COMPLEX:NOV21C||40||1&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
            <input type="hidden" name="return_link_text" value="Return to albumworks" />
            <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/directsale-thankyou2-<?=$product?>" />

            <div class="field">
                <input name="First Name" id="gv_first_name" type="text" class="inputbox" placeholder="NAME" />
                <input type="hidden" name="information_fields" value="First Name" />
                <input type="hidden" name="required_fields" value="First Name" />
            </div>

            <div class="field">
                <input name="Email" id="gv_email" type="text" class="inputbox" placeholder="EMAIL" />
                <input type="hidden" name="information_fields" value="Email" />
                <input type="hidden" name="required_fields" value="Email" />
                <input type="hidden" name="receipt_address" value="<<Email>>" />
            </div>

            <input type="hidden" name="Gift Voucher" id="gvfield" value="87.50" />

            <div class="clr"></div>
            <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style="">BUY NOW</a>
            <div class="clr"></div>
        </form>

        <p class="note">Your voucher will be delivered to you via email, free of charge. Normal shipping rates will apply to the delivery of your product upon order.</p>
    </div>
<div class="clr"> </div>
<p class="hidesmall" style="text-align:center; border-top:1px solid #cccccc; border-bottom:1px solid #cccccc; padding:30px 50px; width:80%; margin:50px auto; clear:both;">
    Want to make a different product? To choose from our full range &nbsp;
    <a style="display:inline; padding-left:0" class="cta dloe" onclick="launch_download_lightbox()" href="javascript:void(0)">DOWNLOAD OUR EDITOR</a>
</p>

<div class="threecols group clr">
    <div>
        <img src="img/photobookwizard/01.jpg" alt="man and woman looking at computer smiling" />
        <h3>How much does a Photo Book cost?</h3>
        <p>Our Photo Books start from just $19.95. There is a book for every budget. Size, cover type and print definition all play a part in the cost of your Photo Book. Use our handy calculator to find the right book for you.</p>
        <a href="https://www.albumworks.com.au/calculator" class="cta">LEARN MORE</a>
    </div>
    <div>
        <img src="img/photobookwizard/02.jpg" alt="handing a photobook with deboss cover from generation to generation" />
        <h3>Dispatched within just 7 days</h3>
        <p>Our priority is to make all your products as fast as we can - but never at the expense of quality. In fact 95% off all items are manufactured and dispatched within 2-4 business days.</p>
        <a href="https://www.albumworks.com.au/shipping" class="cta">LEARN MORE</a>
    </div>
    <div>
        <img src="img/photobookwizard/03.jpg" alt="an array of photo books, some open and some closed, showing their covers and contents" />
        <h3>Need help?</h3>
        <p>Help is never far away! Our Melbourne based support team is there to support you. Call us on 1300 553 448 or email service@albumworks.com.au - we would love to help.</p>
        <a href="https://www.albumworks.com.au/contact" class="cta">LEARN MORE</a>
    </div>
</div>
<div class="clr">&nbsp;</div>
</div> <!-- body -->
</div> <!-- container -->

<div id="theatre"> </div>
<div id="lightbox_download" class="lightbox">
    <form id="lightbox_dlform_form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
        <input type="hidden" value="Download" name="00N3600000BOyGd">
        <input type="hidden" value="AP" name="00N3600000BOyAt">
        <input type="hidden" value="PG" name="00N3600000Loh5K">
        <input type="hidden" name="00N3600000Los6F" value="Windows">
        <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
        <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
        <!-- Link To Contact -->
        <input type="hidden" value="1" name="00N3600000RTz4Z">
        <!-- Adword fields -->
        <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
        <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
        <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
        <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
        <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
        <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
        <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
        <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
        <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>

        <!-- Actual fields -->
        <img src="<?=getenv('BASEPATH')?>statics/directsale/lighbox_download.png" alt="GET STARTED NOW" />
        <p>You're seconds away from designing a beautiful keepsake using your photos!</p>
        <p>Enter your details below to create an <em>albumworks</em> account, and to begin working on your project.</p>
        <h4>GET STARTED</h4>
        <label for="field_lightbox_download_first_name">NAME:</label>
        <input type="text" id="field_lightbox_download_first_name" name="first_name" class="firstname smartedit inputbox">
        <label for="field_lightbox_download_email">EMAIL:</label>
        <input type="text" name="email" id="field_lightbox_download_email" class="email smartedit inputbox">
        <div class="checkarea">
            <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
            <label for="field_lightbox_download_emailoptout">Keep me updated with special offers, promotions and software updates</label>
        </div>
        <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">DOWNLOAD EDITOR FOR FREE</a>
        <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and Conditions of this service</p>
        <div class="clr"> </div>
    </form>
</div>

<script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/directsale/common.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-1338422-2']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    <? if(isset($_GET['gclid'])): ?>
    createCookie('gclid','<?=addslashes($_GET['gclid'])?>',90);
    <? endif; ?>
    <? if(isset($_GET['msclkid'])): ?>
    createCookie('gclid','bing<?=addslashes($_GET['msclkid'])?>',90);
    <? endif; ?>

    jQuery('input[name=00N3600000BOyGd]').each(function(){
        var $form = $(this).closest('form');
        if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
            $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
        }
    });

    function curr(value, addplus){
        if(value === '')
            return '-';
        return (addplus ? '+' : '')+'$'+parseFloat(value).toFixed(2);
    }

    function change_price(){
        var pages = parseInt($('#field_pages').val());
        var was = 999999;
        var now = 999999;
        switch(pages){
            case 40:  was = 124.95; now = 87.50; break;
            case 70:  was = 177.45; now = 110; break;
            case 100: was = 229.95; now = 125.95; break;
        }

        var quantity = 1;
        if($('#field_quantity').val() !== ''){
            quantity = parseInt($('#field_quantity').val());
        }

        var price = (now * quantity).toFixed(2);
        $('#gvfield').val(price);

        var promo = 'COMPLEX:<?=$promo?>||'+pages+'||'+quantity;
        $('#reply_link_url').val('https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo='+promo+'&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=');

        $('#wasnow s').text(curr(was * quantity));
        $('#wasnow strong').text('Now '+curr(now * quantity));
    }

    $(document).ready(function(){
        <? if(isset($_GET['40'])): ?>
            document.getElementById('field_pages').value = '40';
        <? elseif(isset($_GET['70'])): ?>
            document.getElementById('field_pages').value = '70';
        <? elseif(isset($_GET['100'])): ?>
            document.getElementById('field_pages').value = '100';
        <? endif; ?>
        change_price();
    });
</script>
</body>
</html>
