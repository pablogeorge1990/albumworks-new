<? include('header.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gaw3030wed/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gaw3030wed/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gaw3030wed/03.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/gaw3030wed/04.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>30% OFF</span><br>30x30 Wedding Photo Book</h2>
        <h3><em>Buy Now, Make Later!</em><br>Every page tells the story of your wedding, in stunning <em>albumworks</em> quality.</h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook.</li>
            <li>Most 30x30 Photo Books come with 20 pages, but our gorgeous books come with 40 pages!</li>
            <li>Full colour, gloss hard photocover</li>
            <li>Printed on 170gsm Satin photo paper</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Upgrade to HD, upsize to A3 or add more pages in the Editor*</li>
            <li>Full local, friendly support to help make your book</li>
            <li>Buy Now, Make Later (3 months to make book)</li>
        </ul>
        
        <p class="note">*This voucher will cover the cost of an SD 30x30 Photo Book with 40 pages. You will be able to upgrade to HD, upsize to A3, or add pages in the Editor, and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll email you a voucher for the amount of your Photo Book.</h3>
        <?
            $promo = 'GAW3030WED';
            $product = 'gaw3030wed';
            $faceprice = '60.00';
            $wasprice = '86.95';
            
            include('footer.php');