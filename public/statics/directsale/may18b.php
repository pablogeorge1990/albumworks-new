<? include('header2.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18b/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18b/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18b/03.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>SAVE $56</span><br>11 x 8.5" Photo Book<br>100 pages</h2>
        <h3><em>Buy Now, Make Later!</em></h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook</li>
            <li>Full colour hard photocover. Choose premium matte or glossy finish</li>
            <li>Printed on 150gsm Premium Silk paper</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Upgrade to HD, upsize, or add more pages in the Editor*</li>
            <li>Full local, friendly support to help make your book</li>
            <li>Buy Now, Make Later. 6 months from date of purchase</li>
        </ul>

        <p class="buysub">*This voucher will cover the cost of an SD 11x8.5" Photocover Photo Book with 100 pages. You will be able to add extra copies when ordering and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll email you a voucherto use when ordering your product.</h3>
<?
$promo = 'MAY18B';
$product = 'may18b';
$faceprice = '79.95';
$wasprice = '135.95';
$smallprint = 'Your voucher will be delivered to you via email, free of charge. Normal shipping rates will apply to the delivery of your product upon order.';

include('footer2.php');