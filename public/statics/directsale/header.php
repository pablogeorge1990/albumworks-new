<!DOCTYPE html>
<html>
    <head>
        <title>Photo Books by albumworks</title>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,300,400,600,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/directsale/common.css?v2" />
    </head>
<body>
    <div id="container">
        <div id="header">
            <a class="logo" target="_blank" href="https://www.albumworks.com.au/">albumworks</a>
            <img class="logoside" src="<?=getenv('BASEPATH')?>statics/directsale/01.png" width="255" height="66" />
            <div id="maincta">
                <a class="cta" onclick="launch_download_lightbox()" href="javascript:void(0)">DOWNLOAD NOW <img style="margin:0 0 -2px" src="<?=getenv('BASEPATH')?>statics/directsale/arrow-green.png" /><span>and get our free editor</span></a>
            </div>
        </div>
        <div id="body">