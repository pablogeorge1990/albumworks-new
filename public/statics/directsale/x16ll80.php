<? 
    include('header.php');
    $parts = pathinfo($_SERVER['SCRIPT_NAME']);
    $promo = strtoupper($parts['filename']);
    $product = strtolower($parts['filename']);
    
    $wasprice = '236.95';
    $faceprice = '118.47';
    $product_name = '12x12" Layflat Photo Book<br>80 pages';
    $colour = 'ea3158';
?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/<?=$product?>/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/<?=$product?>/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/<?=$product?>/03.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span style="color:#<?=$colour?>">50% OFF</span><br><?=$product_name?></h2>
        <h3><em>Buy Now, Make Later! <span style="color:#<?=$colour?>">by Friday 16th Dec, 2016</span></em><br>Every page tells your story, in stunning <em>albumworks</em> quality.</h3>
        <ul>   
            <li>Use all your own photos, easy to upload from your computer or Facebook.</li>
            <li>Full colour, premium matte hard photocover</li>
            <li>Printed on thick, 400gsm E-Photo Lustre paper</li>
            <li>Photos flow seamlessly from one page to the next, in a book that lays completely flat</li>
            <li>Upsize or add more pages in the Editor*</li>
            <li>Full local, friendly support to help make your book</li>
        </ul>
        <p class="note">*This voucher will cover the cost of a <?=str_replace('<br>', ' with ', $product_name)?>. You will be able to upsize, or add pages in the Editor, and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll <strong style="text-decoration: underline;">email you a voucher</strong><br>for the amount of your Photo Book.</h3>
        <?
include('footer.php');