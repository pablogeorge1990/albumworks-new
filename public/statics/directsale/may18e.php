<? include('header2.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18e/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18e/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18e/03.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>SAVE $114.95</span><br>16 x 12" HD Photo Book<br>Material Cover - 80 pages</h2>
        <h3><em>Buy Now, Make Later!</em></h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook</li>
            <li>Premium material cover - choose from 30 different colours</li>
            <li>Printed on 250gsm HD Lustre paper</li>
            <li>Highest quality photo printing on Canon Dreamlabo 5000 Press</li>
            <li>Option to add more pages in the Editor*</li>
            <li>Full local, friendly support to help make your book</li>
            <li>Buy Now, Make Later. 6 months from date of purchase</li>
        </ul>

        <p class="buysub">*This voucher will cover the cost of an HD 16x12" Photocover Photo Book with 70 pages. You will be able to add extra copies when ordering and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll email you a voucherto use when ordering your product.</h3>
<?
$promo = 'MAY18E';
$product = 'may18e';
$faceprice = '160.00';
$wasprice = '274.95';
$smallprint = 'Your voucher will be delivered to you via email, free of charge. Normal shipping rates will apply to the delivery of your product upon order.';

include('footer2.php');