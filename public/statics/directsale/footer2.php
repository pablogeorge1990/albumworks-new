<form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" onsubmit="return validateGvForm()"> <!-- test to live -->
    <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" />
    <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
    <input type="hidden" name="vendor_name" value="6010010" />
    <input type="hidden" name="gst_rate" value="10">
    <input type="hidden" name="gst_added" value="true">
    <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
    <input type="hidden" name="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=<?=$promo?>&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
    <input type="hidden" name="return_link_text" value="Return to albumworks" />
    <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/directsale-thankyou2-<?=$product?>" />

    <div class="field">
        <input name="First Name" id="gv_first_name" type="text" class="inputbox" placeholder="NAME" />
        <input type="hidden" name="information_fields" value="First Name" />
        <input type="hidden" name="required_fields" value="First Name" />
    </div>

    <div class="field">
        <input name="Email" id="gv_email" type="text" class="inputbox" placeholder="EMAIL" />
        <input type="hidden" name="information_fields" value="Email" />
        <input type="hidden" name="required_fields" value="Email" />
        <input type="hidden" name="receipt_address" value="<<Email>>" />
    </div>

    <input type="hidden" name="Gift Voucher" value="<?=$faceprice?>" />

    <div class="clr"></div>
    <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style="">BUY NOW</a>
    <div class="clr"></div>
</form>

<? if(!isset($noprice) or !$noprice): ?>
    <h2>Was <s>$<?=$wasprice?></s> <strong>Now $<?=$faceprice?></strong></h2>
<? endif; ?>
<? if(isset($smallprint)): ?>
    <p class="note"><?=$smallprint?></p>
<? else: ?>
    <p class="note">Your voucher will be delivered to you via email, free of charge. Normal shipping rates will apply to the delivery of your product, upon order.</p>
<? endif; ?>
</div>
<div class="clr"> </div>
<p class="hidesmall" style="text-align:center; border-top:1px solid #cccccc; border-bottom:1px solid #cccccc; padding:30px 50px; width:80%; margin:50px auto; clear:both;">
    Want to make a different product? To choose from our full range &nbsp;
    <a style="display:inline; padding-left:0" class="cta dloe" onclick="launch_download_lightbox()" href="javascript:void(0)">DOWNLOAD OUR EDITOR</a>
</p>

<div class="clr" style="text-align: center;">
    <h3>Why choose <em>albumworks</em>:</h3>
    <div class="blocks">
        <img src="<?=getenv('BASEPATH')?>statics/directsale/05.jpg" />
        <img src="<?=getenv('BASEPATH')?>statics/directsale/06.jpg" />
        <img src="<?=getenv('BASEPATH')?>statics/directsale/why-03.jpg" />
        <img src="<?=getenv('BASEPATH')?>statics/directsale/08.jpg" />
        <div class="clr"></div>
    </div>

    <h3 style="margin-top:20px">Upload photos from:</h3>
    <p><img src="<?=getenv('BASEPATH')?>statics/directsale/09.jpg" style="width:100%; max-width:593px;" /></p>
    <p class="smaller">*Available only on online Editor</p>
</div>
</div> <!-- body -->
</div> <!-- container -->

<div id="theatre"> </div>
<div id="lightbox_download" class="lightbox">
    <form id="lightbox_dlform_form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
        <input type="hidden" value="Download" name="00N3600000BOyGd">
        <input type="hidden" value="AP" name="00N3600000BOyAt">
        <input type="hidden" value="PG" name="00N3600000Loh5K">
        <input type="hidden" name="00N3600000Los6F" value="Windows">
        <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
        <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
        <!-- Link To Contact -->
        <input type="hidden" value="1" name="00N3600000RTz4Z">
        <!-- Adword fields -->
        <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
        <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
        <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
        <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
        <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
        <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
        <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
        <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
        <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>

        <!-- Actual fields -->
        <img src="<?=getenv('BASEPATH')?>statics/directsale/lighbox_download.png" alt="GET STARTED NOW" />
        <p>You're seconds away from designing a beautiful keepsake using your photos!</p>
        <p>Enter your details below to create an <em>albumworks</em> account, and to begin working on your project.</p>
        <h4>GET STARTED</h4>
        <label for="field_lightbox_download_first_name">NAME:</label>
        <input type="text" id="field_lightbox_download_first_name" name="first_name" class="firstname smartedit inputbox">
        <label for="field_lightbox_download_email">EMAIL:</label>
        <input type="text" name="email" id="field_lightbox_download_email" class="email smartedit inputbox">
        <div class="checkarea">
            <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
            <label for="field_lightbox_download_emailoptout">Keep me updated with special offers, promotions and software updates</label>
        </div>
        <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">DOWNLOAD EDITOR FOR FREE</a>
        <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and Conditions of this service</p>
        <div class="clr"> </div>
    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/directsale/common.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-1338422-2']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    <? if(isset($_GET['gclid'])): ?>
    createCookie('gclid','<?=addslashes($_GET['gclid'])?>',90);
    <? endif; ?>
    <? if(isset($_GET['msclkid'])): ?>
    createCookie('gclid','bing<?=addslashes($_GET['msclkid'])?>',90);
    <? endif; ?>

    jQuery('input[name=00N3600000BOyGd]').each(function(){
        var $form = $(this).closest('form');
        if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
            $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
        }
    });

</script>
</body>
</html>