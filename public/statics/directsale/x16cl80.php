<? 
    include('header.php');
    $parts = pathinfo($_SERVER['SCRIPT_NAME']);
    $promo = strtoupper($parts['filename']);
    $product = strtolower($parts['filename']);
    
    $wasprice = '142.95';
    $faceprice = '78.62';
    $product_name = '12x12" Classic Photo Book<br>80 pages';
    $colour = 'ea3158';
?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/<?=$product?>/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/<?=$product?>/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/<?=$product?>/03.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span style="color:#<?=$colour?>">45% OFF</span><br><?=$product_name?></h2>
        <h3><em>Buy Now, Make Later! <span style="color:#<?=$colour?>">by Friday 16th Dec, 2016</span></em><br>Every page tells your story, in stunning <em>albumworks</em> quality.</h3>
        <ul>   
            <li>Use all your own photos, easy to upload from your computer or Facebook.</li>
            <li>Full colour, premium matte hard photocover</li>
            <li>Printed on 180gsm Premium Silk paper</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Upgrade to HD, upsize, or add more pages in the Editor*</li>
            <li>Full local, friendly support to help make your book</li>
            <li>Buy Now, Make Later. Make by 9PM (AEST) 16 Dec, 2016.</li>
        </ul>
        <p class="note">*This voucher will cover the cost of a SD <?=str_replace('<br>', ' with ', $product_name)?>. You will be able to upgrade to HD, upsize, or add pages in the Editor, and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll <strong style="text-decoration: underline;">email you a voucher</strong><br>for the amount of your Photo Book.</h3>
        <?
include('footer.php');