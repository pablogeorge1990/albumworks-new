<? include('header2.php'); ?>
    <div class="viewer">
        <div class="primary"></div>
        <div class="secondary">
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18a/01.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18a/02.jpg"></div>
            <div class="icon" image="<?=getenv('BASEPATH')?>statics/directsale/may18a/03.jpg"></div>
        </div>
    </div>
    <div class="copy">
        <h2><span>36% OFF</span><br>11 x 8.5" Photo Book<br>40 pages</h2>
        <h3><em>Buy Now, Make Later!</em></h3>
        <ul>
            <li>Use all your own photos, easy to upload from your computer or Facebook</li>
            <li>Full colour hard photocover. Choose premium matte or glossy finish</li>
            <li>Printed on 150gsm Premium Silk paper</li>
            <li>Quality photo printing on HP Indigo Press</li>
            <li>Upgrade to HD, upsize, or add more pages in the Editor*</li>
            <li>Full local, friendly support to help make your book</li>
            <li>Buy Now, Make Later. 6 months from date of purchase</li>
        </ul>

        <p class="buysub">*This voucher will cover the cost of an SD 11x8.5" Photocover Photo Book with 40 pages. You will be able to upgrade to add extra copies when ordering and you will simply pay the difference in the Shopping Cart.</p>

        <h3>Enter your details below and we'll email you a voucherto use when ordering your product.</h3>
<?
$promo = 'MAY18A';
$product = 'may18a';
$faceprice = '44.95';
$wasprice = '69.95';
$smallprint = 'Your voucher will be delivered to you via email, free of charge. Normal shipping rates will apply to the delivery of your product upon order.';

include('footer2.php');