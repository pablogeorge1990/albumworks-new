<? include('header.php'); ?>
    <div class="col">
        <h1>Bigger book, better value!</h1>
        <p>Depending on size, our Photo Books come with between 20 and 40 pages. Most customers then add extra pages in the Editor to accommodate all their fantastic photos.</p>
        <p>Usually each additional page increases the cost of your book. However, with our special offer, you can get up to 20 extra pages for free! That's up to $60 of value.</p>
        
        <p style="text-align:center"><br><img src="<?=getenv('BASEPATH')?>statics/landing-feb15/adwords-01.jpg" /></p>
        <br><br>
    </div>
    
    <div class="dlform">
        <form id="lightbox_dlform_form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
            <h1>DOWNLOAD OUR EDITOR TO GET 20 FREE EXTRA PAGES</h1>
            <input type="hidden" value="00D36000000oZE6" name="sfga">
            <input type="hidden" value="00D36000000oZE6" name="oid">
            <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&landing-feb15" name="retURL">
            <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
            
            <!-- Actual fields -->
            <div class="field">
                <label for="field_lightbox_download_first_name">YOUR NAME:</label>
                <input type="text" id="field_lightbox_download_first_name" name="first_name" class="firstname smartedit inputbox">
            </div>
            <div class="field">
                <label for="field_lightbox_download_email">YOUR EMAIL ADDRESS:</label>
                <input type="text" name="email" id="field_lightbox_download_email" class="email smartedit inputbox">
            </div>
            <a class="button" href="javascript:void(0)" onclick="$(this).closest('form').submit();">DOWNLOAD FREE EDITOR</a>
            <div class="checkarea field">
                <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
                <label class="checklabel" for="field_lightbox_download_emailoptout">Keep me updated with special offers, promotions &amp; app updates</label>
            </div>                        
            <div class="clr"> </div>
            
            <p style="text-align: center;"><br><strong>Your voucher code will be emailed to you instantly.</strong></p>
            <p style="text-align: center;">Our Editor will begin downloading automatically. For information on how to install our Editor go to our <a target="_blank" href="https://www.albumworks.com.au/faq">FAQs</a>.</p>
            <p class="smaller" style="text-align: center;">* offer only available on first order<br><br></p>
        </form>
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
        <script type="text/javascript">
            function is_valid_email(email){ 
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }        
                 
            function checkdlform(form){ 
                var $form = $(form);
                var errors = false;
                $form.find('.inputbox').each(function(){
                    if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
                        errors = true;
                    }
                });
                
                if(errors){
                    alert('Please ensure all fields are complete and try again.');
                    return false;
                }
                
                if(!is_valid_email($form.find('input[name=email]').val())){
                    alert('Please enter a valid email address and try again.')
                    return false;
                }
                
                var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
                $form.find('input[name=00N3600000Los6F]').val(os_select);
                document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";    
                
                var name = $form.find('input[name=first_name]').val();
                var email = $form.find('input[name=email]').val();    
                var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
                document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";
                
                var iscookie = readCookie('download_params');
                if(iscookie)
                    $form.find("input[00N3600000LosKl]").val(0);

                var refpromo = readCookie('APrefpromo');
                if(refpromo)
                    $form.find("input[00N3600000LosAC]").val(refpromo);
                refpromo = $form.find('input[00N3600000LosAC]').val();
                document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";
                
                if(typeof _gaq !== 'undefined') { _gaq.push(['_trackPageview', '/download_start']); }
                
                return true;
            }
            
            function urlencode (str) {
                str = (str + '').toString();
                return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
                replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
            }

            function createCookie(name,value,days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime()+(days*24*60*60*1000));
                    var expires = "; expires="+date.toGMTString();
                }
                else var expires = "";
                document.cookie = name+"="+value+expires+"; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            }

            function eraseCookie(name) {
                createCookie(name,"",-1);
            }            
        </script>
    </div>    
    
    <div class="clr">
        <h3>Why our Photo Books are so special:</h3>
        <div class="blocks">
            <img src="<?=getenv('BASEPATH')?>statics/landing-feb15/adwords-02.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-feb15/adwords-03.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-feb15/adwords-04.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-feb15/adwords-05.jpg" />
            <div class="clr"></div>
        </div>
        <p style="text-align:center; margin:1em;">
            <a class="cta inline green" target="_blank" href="https://www.albumworks.com.au/photo-books">LEARN MORE ABOUT OUR PHOTO BOOKS</a>
        </p>
        
        <hr/>
        
        <h3><br>How It Works:</h3>
        <p style="text-align:center">Our Photo Books have a basic price for each size, and come with 20 pages (15x15, A5, 21x21) or 40 pages (A4, 30x30, A3). With our special offer you get an extra 20 pages for free! See our basic prices, and how much you could save below:</p>
        <p style="text-align:center"><br><img src="<?=getenv('BASEPATH')?>statics/landing-feb15/adwords-06.jpg" /></p>
        
    </div>

    <div class="brochure video">
        <h5>ABOUT <em>albumworks</em></h5>
        <div class="vidholder">
            <iframe src="https://www.youtube.com/embed/1oITuX7ar2g" frameborder="0" allowfullscreen=""></iframe>
        </div>                                        
    </div>        
    
<? include('footer.php'); ?>