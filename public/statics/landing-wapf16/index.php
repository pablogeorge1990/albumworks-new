<? include('header.php'); ?>
    <br/>
    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-wapf16/header_wapf.jpg" />
    <div class="callout" style="padding-top:27px;!important;">
        <p><strong>Special offer for WAPF members only!</strong></p>
        <p>Get 30% off year round from albumworks</p>
        <span style="font-size:14px; font-weight:bold;">Valid until 31 December, 2017</span>
    </div>

    <p style="text-align: center;"><img style="width:100%; max-width:862px" src="<?=getenv('BASEPATH')?>statics/landing-wapf16/join.jpg" /></p>
    
    <span class="topheader"><h3><span style="font-style: italic;">albumworks</span> is a community of over 100,000 people who love photo books!</h3></span>
    
    <form class="box" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
        <a name="join"></a>
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" value="http://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=WAPF" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="THIRTYFREE" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
        
        <!-- Actual fields -->
        <input type="text" placeholder="NAME" name="first_name" class="firstname smartedit inputbox" alt="NAME">
        <input type="text" placeholder="EMAIL" name="email" class="email smartedit inputbox" alt="EMAIL">
        <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET OUR <br/>FREE EDITOR</span></a>
        <div class="checkarea">
            <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
            <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
        </div>
        <div class="clr"> </div>
    </form>
    
    <hr />

    <div class="clr" style="text-align: center;">
        <span class="topheader"><h3>Why choose <em>albumworks</em>:</h3></span><br/>
        <div class="brochures" style="width: 90%; margin: 0px auto;">
            <div class="brochure" style="background-image:url(img/products/photobooks/hd/01.png?v2)">
                <div class="content">
                    <h3>The Next Level In Photo Books.</h3>
                    <h4>Our High Definition (or HD) Photo Books are printed on the world-class, Canon DreamLabo 5000 digital printing press. DreamLabo technology redefines the limits of photoprint technology.</h4>
                    <h4><a href="photo-books-craftsmanship#dl" class="cta green transparent inline">LEARN MORE</a></h4>
                </div>
            </div>
            <div class="brochure odd" style="background-image:url(img/products/photobooks/hd/02.png?v2)">
                <div class="content">
                    <h3>Vibrant Colour.</h3>
                    <h4>Get the full spectrum of colours with HD. While standard colour printing uses 4 CMYK inks, our HD Printing uses 7 dye-based ink colour technology, to allow for the most pure, bright and vibrant colours available in Photo Book printing.</h4>
                </div>
            </div>
            <div class="brochure" style="background-image:url(img/products/photobooks/hd/03.png)">
                <div class="content">
                    <h3>Definition, Magnified.</h3>
                    <h4>Thanks to a revolutionary new 2,400 dots-per-inch High Definition Fine Lithographic print resolution (invented for unforgable bank notes), our HD Photo Books are so sharp they can even feature text that can only be read under a magnifying glass!</h4>
                </div>
            </div>
            <div class="brochure odd" style="background-image:url(img/products/photobooks/hd/04.png)">
                <div class="content">
                    <h3>Look Closer.</h3>
                    <h4>Traditional digital printing uses millions of tiny dots to create an image - which means close-up the photo appears grainy or has a "flyscreen" look. Our HD Photo Books are printed using Stochastic Screening, which eliminates the flyscreen, and prints in glorious, continuous colour.</h4>
                    <h4><a href="photo-books-craftsmanship" class="cta green transparent inline">LEARN MORE</a></h4>
                </div>
            </div>
            <div class="brochure" style="background-image:url(img/products/photobooks/hd/05.png?v2)">
                <div class="content">
                    <h3>Luxury in Paper.</h3>
                    <h4>Our HD Canon papers really do take your Photo Book to the next level. Choose from luxurious 250gsm Luster, or rich and vibrant 250gsm Glossy.</h4>
                    <h4><a href="photo-books-craftsmanship" class="cta green transparent inline">LEARN MORE</a></h4>
                </div>
            </div>
            <div class="brochure odd" style="background-image:url(img/products/photobooks/hd/06.png)">
                <div class="content">
                    <h3>Your Memories Forever. </h3>
                    <h4>Tested via ISO 18924 to maintain its quality for 300 years, you can be confident that your story in an HD Photo Book will stand the test of time, leaving you free to go ahead and record history within these pages.</h4>
                </div>
            </div>
            <div class="brochure" style="background-image:url(img/products/photobooks/hd/07.png)">
                <div class="content">
                    <h3>The Art of Bookmaking.</h3>
                    <h4>The only thing we're as passionate about as your photos, is crafting beautiful Books. With expert bookbinders and technicians producing all our Photo Books, we strive to create state-of-the-art Books, with classic bookmaking features.</h4>
                    <h4><a href="photo-books-craftsmanship" class="cta green transparent inline">LEARN MORE</a></h4>
                </div>
            </div>
        </div>
    </div>




<? include('footer.php'); ?>
