<? include('header.php'); ?>
    <div class="col">
        <h1>Bigger book, better value!</h1>

        <p style="text-align:center"><br><img src="<?=getenv('BASEPATH')?>statics/landing-may15/adwords-01.jpg" /></p>
        <br><br>
    </div>
    
    <div class="dlform">
        <form id="lightbox_dlform_form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
            <h1>THANK YOU FOR DOWNLOADING OUR EDITOR</h1>
            <h2><br><br>YOUR VOUCHER CODE WILL BE EMAILED TO YOU<br><br><br></h2>
            
            <p style="text-align: center;">Get information on <a target="_blank" href="https://www.albumworks.com.au/photo-books-howto">How To Make</a> your Photo Book, or how to <a target="_blank" href="https://www.albumworks.com.au/support/how-do-i-download-and-install-the-editor/viewSolution.php?solution_ID=30010">install our Editor</a></p>
            <p style="text-align: center;"><br>Learn more about <a target="_blank" href="https://www.albumworks.com.au/about"><em>albumworks</em></a>.<br><br><br><br></p>
        </form>

    </div>    
    
    <div class="clr"></div>

    <div class="brochure video">
        <h5>ABOUT <em>albumworks</em></h5>
        <div class="vidholder">
            <iframe src="https://www.youtube.com/embed/1oITuX7ar2g" frameborder="0" allowfullscreen=""></iframe>
        </div>                                        
    </div>        
    
    <!--
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
    <script type="text/javascript">
        (function(){var a={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(a){for(var b=0;b<a.length;b++){var c=a[b].string;var d=a[b].prop;this.versionSearchString=a[b].versionSearch||a[b].identity;if(c){if(c.indexOf(a[b].subString)!=-1)return a[b].identity}else if(d)return a[b].identity}},searchVersion:function(a){var b=a.indexOf(this.versionSearchString);if(b==-1)return;return parseFloat(a.substring(b+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};a.init();window.jQuery.client={os:a.OS,browser:a.browser}})();
        $(document).ready(function(){
            var download_link = 'https://s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/albumworksSetup.exe';
            if($.client.os == 'Mac')
                download_link = 'https://s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/albumworksSetup.dmg';
            $('body').append('<iframe id="downloadiframe" style="position:absolute; width:1px; height:1px; top:-999em; left:-999em;" src="'+download_link+'"></iframe>');
        });
    </script>        
    -->
    <iframe src="https://www.albumworks.com.au/download-thankyou.html" style="display:none"> </iframe>
    
<? include('footer.php'); ?>