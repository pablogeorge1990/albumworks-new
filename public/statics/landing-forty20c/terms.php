<? include('header.php'); ?>
    <div class="onecol" id="terms">
        <h2>
            Terms &amp; Conditions of Entry
        </h2>
        <p>
            The <em>albumworks</em> "Win the Ultimate Christmas" Competition (the "Promotion") is a promotion conducted by the Promoter.
        </p>
        <p>
            1. Information on how to enter the Promotion and a description of the prizes that may be won in the Promotion form part of these Terms &amp; Conditions of
            Entry. Participation in the Promotion constitutes acceptance of these Terms &amp; Conditions of Entry. Entries not complying with these Terms &amp;
            Conditions of Entry are ineligible and the Promoter reserves its absolute right to disqualify any entries do not comply with these Terms &amp; Conditions
            of Entry.
        </p>
        <p>
            2. The Promotion is open to Australian residents who are over 18. Entry into the Promotion is free. Directors, management and employees of <em>albumworks</em> and
            its agencies directly associated with this Promotion are ineligible to enter.These Terms &amp; Conditions of Entry will be displayed at
            www.albumworks.com.au/christmas-terms at the commencement of the Promotion.
        </p>
        <p>
            3. Definitions within this Promotion:
        </p>
        <p>
            Promoter - Pictureworks Group Pty Ltd, trading as <em>albumworks</em>&#8482; ABN 28 119 011 933 of 323 Williamstown Road, Port Melbourne VIC 3207
        </p>
        <p>
            Promotion - means "Win the Ultimate Christmas" promotion governed by these Terms &amp; Conditions of Entry and conducted as a promotion by the Promoter.
        </p>
        <p>
            Promotional Period - means 09:00 AEST on Monday, 3 November, 2014 until 21:00 AEST on Friday, 28 November, 2014
        </p>
        <p>
            Prize - means the Ultimate Christmas Hamper' containing Glenrothes Speyside Single Malt Whisky 700ml - Moet &amp; Chandon Brut N.V. 750ml - Penfolds Bin 8
            Cabernet Shiraz 750ml - Baileys The Original Irish Cream Liqueur 700ml - Tempus Two Pewter Botrytis Semillon 250ml - Pudding on the Ritz Christmas Pudding
            500g - The Cakemen Florentine Nut Cake 500g - Patons Premium Chocolate Macadamia Gold 170g - Mac's Butter Shortbread Tin 400g - Jindi Quince Paste 100g -
            Carr's Entertainment Cracker Collection 200g - Coffex Ground Coffee 250g - Granforno Grissini Breadsticks 125g - Cobram Extra Virgin Australian Olive Oil
            250ml - Ernest Hillier Milk &amp; Dark Heritage Chocolates 175g - Snax with Attitude Salted Cashews 100g - Alchemy Coffee Syrup 250ml - Maille Balsamic
            Vinegar 250ml - Bonne Maman Jam 370g - Maille French Mustard 210g - JC'S Quality Foods Pistachios 150g - Bahlsen Deloba Pastry Biscuits 100g - Patons
            Macadamia Dry Roast 50g - Kez's Kitchen Florentines 185g - Trentham Tucker Almond Bread 150g - Walkers Biscuits 150g - Robins Old Style Brandy Sauce 250ml
            - Yorkshire Tea 50 pack - 4 x Designer Mugs - 3 piece Cocktail Kit valued at $650.
        </p>
        <p>
            Plus 1 x <em>albumworks</em> voucher worth $250 for use on any Photo Book.
        </p>
        <p>
            4. The Promotion commences and ends as specified in the Promotional Period.
        </p>
        <p>
            5. To enter the Promotion, you need to visit the "Win the Ultimate Christmas" competition page on albumworks.com.au/landing-nov15 and sign-up to the <em>albumworks</em> emails. Entrants must enter the tell us in 100 words or less about their
            Ultimate Christmas memory. You will then to be automatically entered into the draw. One entry will be recorded for each unique email address. By signing up to
            receive emails from <em>albumworks</em> online, you agree to the following conditions. All entries will be entered into a database and <em>albumworks</em> may use the
            entrant's names and email addresses for future marketing communications. If entrants no longer consent to their details being used for future marketing
            communications, the entrant should contact <em>albumworks</em>. Any request to update, modify or delete the entrant's details should be directed to
            albumworks.com.au.
        </p>
        <p>
            6. The judging of the Prize will take place at 323 Williamstown Road, Port Melbourne VIC 3207, on Monday 1st December 2014. Winners will be notified by
            email and will be published on the <em>albumworks</em> Facebook page and blog. This is a game of skill and chance plays no part in determining the winners. Each entry will be individually judged
            based on the most creative and uniqueness.
        </p>
        <p>
            a. An entry will be deemed valid if the entrant has entered an Ultimate Christmas memory, and filled in their details correctly.
        </p>
        <p>
            b. A valid entry will the judged and the most creative entry as determined by the judges will be deemed as winner.
        </p>
        <p>
            c. The Promoter's decision is final and no correspondence will be entered into.
        </p>
        <p>
            7. There will be a total of 1 prize. Prize is not transferable or exchangeable and cannot be redeemed for cash.
        </p>
        <p>
            8. The winner will also be notified in writing via email within 14 days of the judging taking place. If the winning entry is invalid, that entry will be discarded and the prize will be awarded to the next creative entry in accordance with these
            Terms &amp; Conditions of Entry.
        </p>
        <p>
            9. If a winner does not claim their prize within 14 days from the date of determination of that winner, their entry will be deemed invalid.
        </p>
        <p>
            10. The Promoter accepts no responsibility for any tax implications that may arise from winning of the prizes. Independent financial advice should be
            sought.
        </p>
        <p>
            11. The Promoter reserves the right to disqualify all entries who is in breach of the Terms &amp; Conditions of Entry or who manipulates, seeks to
            manipulate or benefits from manipulating, the entry process or the Promotion.
        </p>
        <p>
            12. The Promoter and its agencies and companies associated with this Promotion will not be liable for any loss (including but not limited to indirect or
            consequential loss), damage or personal injury which is suffered or sustained (including without limitation to that caused by any person's negligence)
            relating to the Promotion or the awarding or taking of the prizes except for any liability which cannot be excluded by law (in which case liability is
            limited to the minimum amount allowable by law).
        </p>
        <p>
            13. The Promoter may use any personal information that the entrant has provided for the purpose of running the Promotion and also in advertisements,
            publications, media statements and other promotional material associated with the Promotion. For purposes of public statements and advertisements the
            Promoter will only publish the winner's surname, first initial and state. The Promoter may disclose the information for those purposes to its related
            bodies corporate and contractors. The Promoter may use personal information a entrant has provided to send the entrant information about <em>albumworks</em>
            products and services (including via electronic means), and may disclose the information to its related bodies corporate, agencies and contractors
            (including call centres, advertising agencies and direct mail houses) the Promoter engages for that purpose. The Promoter is bound by the National Privacy
            Principles contained in the Privacy Act Cth) 1988.
        </p>
        <p>
            14. In the event there is a dispute concerning the conduct of the Promotion, the decision of the Promoter is final and no correspondence will be entered
            into.
        </p>        
    </div>
<? include('footer.php'); ?>