var __fadeasel = null;
var __windowwidth = 0;
var __windowheight = 0;

;(function(d){var k=d.scrollTo=function(a,i,e){d(window).scrollTo(a,i,e)};k.defaults={axis:'xy',duration:parseFloat(d.fn.jquery)>=1.3?0:1};k.window=function(a){return d(window)._scrollable()};d.fn._scrollable=function(){return this.map(function(){var a=this,i=!a.nodeName||d.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!i)return a;var e=(a.contentWindow||a).document||a.ownerDocument||a;return e.documentElement})};d.fn.scrollTo=function(n,j,b){if(typeof j=='object'){b=j;j=0}if(typeof b=='function')b={onAfter:b};if(n=='max')n=9e9;b=d.extend({},k.defaults,b);j=j||b.speed||b.duration;b.queue=b.queue&&b.axis.length>1;if(b.queue)j/=2;b.offset=p(b.offset);b.over=p(b.over);return this._scrollable().each(function(){var q=this,r=d(q),f=n,s,g={},u=r.is('html,body');switch(typeof f){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(f)){f=p(f);break}f=d(f,this);case'object':if(f.is||f.style)s=(f=d(f)).offset()}d.each(b.axis.split(''),function(a,i){var e=i=='x'?'Left':'Top',h=e.toLowerCase(),c='scroll'+e,l=q[c],m=k.max(q,i);if(s){g[c]=s[h]+(u?0:l-r.offset()[h]);if(b.margin){g[c]-=parseInt(f.css('margin'+e))||0;g[c]-=parseInt(f.css('border'+e+'Width'))||0}g[c]+=b.offset[h]||0;if(b.over[h])g[c]+=f[i=='x'?'width':'height']()*b.over[h]}else{var o=f[h];g[c]=o.slice&&o.slice(-1)=='%'?parseFloat(o)/100*m:o}if(/^\d+$/.test(g[c]))g[c]=g[c]<=0?0:Math.min(g[c],m);if(!a&&b.queue){if(l!=g[c])t(b.onAfterFirst);delete g[c]}});t(b.onAfter);function t(a){r.animate(g,j,b.easing,a&&function(){a.call(this,n,b)})}}).end()};k.max=function(a,i){var e=i=='x'?'Width':'Height',h='scroll'+e;if(!d(a).is('html,body'))return a[h]-d(a)[e.toLowerCase()]();var c='client'+e,l=a.ownerDocument.documentElement,m=a.ownerDocument.body;return Math.max(l[h],m[h])-Math.min(l[c],m[c])};function p(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);
(function(){var a={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(a){for(var b=0;b<a.length;b++){var c=a[b].string;var d=a[b].prop;this.versionSearchString=a[b].versionSearch||a[b].identity;if(c){if(c.indexOf(a[b].subString)!=-1)return a[b].identity}else if(d)return a[b].identity}},searchVersion:function(a){var b=a.indexOf(this.versionSearchString);if(b==-1)return;return parseFloat(a.substring(b+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};a.init();window.jQuery.client={os:a.OS,browser:a.browser}})();

// scrollify
!function(a,b){"use strict";"function"==typeof define&&define.amd?define(["jquery"],function(c){return b(c,a,a.document)}):"object"==typeof module&&module.exports?module.exports=b(require("jquery"),a,a.document):b(jQuery,a,a.document)}("undefined"!=typeof window?window:this,function(a,b,c,d){"use strict";function e(c,d,e,f){if(r===c&&(e=!1),z===!0)return!0;if(n[c]){if(w=!1,e&&G.before(c,o),s=1,E=m[c],C===!1&&r>c&&f===!1&&p[c]&&(s=parseInt(o[c].outerHeight()/u.height()),E=parseInt(m[c])+(o[c].outerHeight()-u.height())),G.updateHash&&G.sectionName&&(C!==!0||0!==c))if(history.pushState)try{history.replaceState(null,null,n[c])}catch(a){b.console&&console.warn("Scrollify warning: Page must be hosted to manipulate the hash value.")}else b.location.hash=n[c];if(C&&(G.afterRender(),C=!1),r=c,d)a(G.target).stop().scrollTop(E),e&&G.after(c,o);else{if(x=!0,a().velocity?a(G.target).stop().velocity("scroll",{duration:G.scrollSpeed,easing:G.easing,offset:E,mobileHA:!1}):a(G.target).stop().animate({scrollTop:E},G.scrollSpeed,G.easing),b.location.hash.length&&G.sectionName&&b.console)try{a(b.location.hash).length&&console.warn("Scrollify warning: ID matches hash value - this will cause the page to anchor.")}catch(a){}a(G.target).promise().done(function(){x=!1,C=!1,e&&G.after(c,o)})}}}function f(a){function b(b){for(var c=0,d=a.slice(Math.max(a.length-b,1)),e=0;e<d.length;e++)c+=d[e];return Math.ceil(c/b)}var c=b(10),d=b(70);return c>=d}function g(a,b){for(var c=n.length;c>=0;c--)"string"==typeof a?n[c]===a&&(q=c,e(c,b,!0,!0)):c===a&&(q=c,e(c,b,!0,!0))}var h,i,j,k,l,m=[],n=[],o=[],p=[],q=0,r=0,s=1,t=!1,u=a(b),v=u.scrollTop(),w=!1,x=!1,y=!1,z=!1,A=[],B=(new Date).getTime(),C=!0,D=!1,E=0,F="onwheel"in c?"wheel":c.onmousewheel!==d?"mousewheel":"DOMMouseScroll",G={section:".section",sectionName:"section-name",interstitialSection:"",easing:"easeOutExpo",scrollSpeed:1100,offset:0,scrollbars:!0,target:"html,body",standardScrollElements:!1,setHeights:!0,overflowScroll:!0,updateHash:!0,touchScroll:!0,before:function(){},after:function(){},afterResize:function(){},afterRender:function(){}},H=function(d){function g(b){a().velocity?a(G.target).stop().velocity("scroll",{duration:G.scrollSpeed,easing:G.easing,offset:b,mobileHA:!1}):a(G.target).stop().animate({scrollTop:b},G.scrollSpeed,G.easing)}function r(){var b=G.section;p=[],G.interstitialSection.length&&(b+=","+G.interstitialSection),G.scrollbars===!1&&(G.overflowScroll=!1),a(b).each(function(b){var c=a(this);G.setHeights?c.is(G.interstitialSection)?p[b]=!1:c.css("height","auto").outerHeight()<u.height()||"hidden"===c.css("overflow")?(c.css({height:u.height()}),p[b]=!1):(c.css({height:c.height()}),G.overflowScroll?p[b]=!0:p[b]=!1):c.outerHeight()<u.height()||G.overflowScroll===!1?p[b]=!1:p[b]=!0})}function C(c,d){var f=G.section;G.interstitialSection.length&&(f+=","+G.interstitialSection),m=[],n=[],o=[],a(f).each(function(c){var d=a(this);c>0?m[c]=parseInt(d.offset().top)+G.offset:m[c]=parseInt(d.offset().top),G.sectionName&&d.data(G.sectionName)?n[c]="#"+d.data(G.sectionName).toString().replace(/ /g,"-"):d.is(G.interstitialSection)===!1?n[c]="#"+(c+1):(n[c]="#",c===a(f).length-1&&c>1&&(m[c]=m[c-1]+parseInt(d.height()))),o[c]=d;try{a(n[c]).length&&b.console&&console.warn("Scrollify warning: Section names can't match IDs - this will cause the browser to anchor.")}catch(a){}b.location.hash===n[c]&&(q=c,t=!0)}),!0===c&&e(q,!1,!1,!1)}function E(){return!p[q]||(v=u.scrollTop(),!(v>parseInt(m[q])))}function H(){return!p[q]||(v=u.scrollTop(),!(v<parseInt(m[q])+(o[q].outerHeight()-u.height())-28))}D=!0,a.easing.easeOutExpo=function(a,b,c,d,e){return b==e?c+d:d*(-Math.pow(2,-10*b/e)+1)+c},j={handleMousedown:function(){return z===!0||(w=!1,void(y=!1))},handleMouseup:function(){return z===!0||(w=!0,void(y&&j.calculateNearest(!1,!0)))},handleScroll:function(){return z===!0||(h&&clearTimeout(h),void(h=setTimeout(function(){return y=!0,w!==!1&&(w=!1,void j.calculateNearest(!1,!0))},200)))},calculateNearest:function(a,b){v=u.scrollTop();for(var c,d=1,f=m.length,g=0,h=Math.abs(m[0]-v);d<f;d++)c=Math.abs(m[d]-v),c<h&&(h=c,g=d);(H()&&g>q||E())&&(q=g,e(g,a,b,!1))},wheelHandler:function(c){if(z===!0)return!0;if(G.standardScrollElements&&(a(c.target).is(G.standardScrollElements)||a(c.target).closest(G.standardScrollElements).length))return!0;p[q]||c.preventDefault();var d=(new Date).getTime();c=c||b.event;var g=c.originalEvent.wheelDelta||-c.originalEvent.deltaY||-c.originalEvent.detail,h=Math.max(-1,Math.min(1,g));if(A.length>149&&A.shift(),A.push(Math.abs(g)),d-B>200&&(A=[]),B=d,x)return!1;if(h<0){if(q<m.length-1&&H()){if(!f(A))return!1;c.preventDefault(),q++,x=!0,e(q,!1,!0,!1)}}else if(h>0&&q>0&&E()){if(!f(A))return!1;c.preventDefault(),q--,x=!0,e(q,!1,!0,!1)}},keyHandler:function(a){return z===!0||x!==!0&&void(38==a.keyCode||33==a.keyCode?q>0&&E()&&(a.preventDefault(),q--,e(q,!1,!0,!1)):40!=a.keyCode&&34!=a.keyCode||q<m.length-1&&H()&&(a.preventDefault(),q++,e(q,!1,!0,!1)))},init:function(){G.scrollbars?(u.on("mousedown",j.handleMousedown),u.on("mouseup",j.handleMouseup),u.on("scroll",j.handleScroll)):a("body").css({overflow:"hidden"}),u.on(F,j.wheelHandler),u.on("keydown",j.keyHandler)}},k={touches:{touchstart:{y:-1,x:-1},touchmove:{y:-1,x:-1},touchend:!1,direction:"undetermined"},options:{distance:30,timeGap:800,timeStamp:(new Date).getTime()},touchHandler:function(b){if(z===!0)return!0;if(G.standardScrollElements&&(a(b.target).is(G.standardScrollElements)||a(b.target).closest(G.standardScrollElements).length))return!0;var c;if("undefined"!=typeof b&&"undefined"!=typeof b.touches)switch(c=b.touches[0],b.type){case"touchstart":k.touches.touchstart.y=c.pageY,k.touches.touchmove.y=-1,k.touches.touchstart.x=c.pageX,k.touches.touchmove.x=-1,k.options.timeStamp=(new Date).getTime(),k.touches.touchend=!1;case"touchmove":k.touches.touchmove.y=c.pageY,k.touches.touchmove.x=c.pageX,k.touches.touchstart.y!==k.touches.touchmove.y&&Math.abs(k.touches.touchstart.y-k.touches.touchmove.y)>Math.abs(k.touches.touchstart.x-k.touches.touchmove.x)&&(b.preventDefault(),k.touches.direction="y",k.options.timeStamp+k.options.timeGap<(new Date).getTime()&&0==k.touches.touchend&&(k.touches.touchend=!0,k.touches.touchstart.y>-1&&Math.abs(k.touches.touchmove.y-k.touches.touchstart.y)>k.options.distance&&(k.touches.touchstart.y<k.touches.touchmove.y?k.up():k.down())));break;case"touchend":k.touches[b.type]===!1&&(k.touches[b.type]=!0,k.touches.touchstart.y>-1&&k.touches.touchmove.y>-1&&"y"===k.touches.direction&&(Math.abs(k.touches.touchmove.y-k.touches.touchstart.y)>k.options.distance&&(k.touches.touchstart.y<k.touches.touchmove.y?k.up():k.down()),k.touches.touchstart.y=-1,k.touches.touchstart.x=-1,k.touches.direction="undetermined"))}},down:function(){q<=m.length-1&&(H()&&q<m.length-1?(q++,e(q,!1,!0,!1)):Math.floor(o[q].height()/u.height())>s?(g(parseInt(m[q])+u.height()*s),s+=1):g(parseInt(m[q])+(o[q].height()-u.height())))},up:function(){q>=0&&(E()&&q>0?(q--,e(q,!1,!0,!1)):s>2?(s-=1,g(parseInt(m[q])+u.height()*s)):(s=1,g(parseInt(m[q]))))},init:function(){c.addEventListener&&G.touchScroll&&(c.addEventListener("touchstart",k.touchHandler,!1),c.addEventListener("touchmove",k.touchHandler,!1),c.addEventListener("touchend",k.touchHandler,!1))}},l={refresh:function(a,b){clearTimeout(i),i=setTimeout(function(){r(),C(b,!1),a&&G.afterResize()},400)},handleUpdate:function(){l.refresh(!1,!0)},handleResize:function(){l.refresh(!0,!1)},handleOrientation:function(){l.refresh(!0,!0)}},G=a.extend(G,d),r(),C(!1,!0),!0===t?e(q,!1,!0,!0):setTimeout(function(){j.calculateNearest(!0,!1)},200),m.length&&(j.init(),k.init(),u.on("resize",l.handleResize),c.addEventListener&&b.addEventListener("orientationchange",l.handleOrientation,!1))};return H.move=function(b){return b!==d&&(b.originalEvent&&(b=a(this).attr("href")),void g(b,!1))},H.instantMove=function(a){return a!==d&&void g(a,!0)},H.next=function(){q<n.length&&(q+=1,e(q,!1,!0,!0))},H.previous=function(){q>0&&(q-=1,e(q,!1,!0,!0))},H.instantNext=function(){q<n.length&&(q+=1,e(q,!0,!0,!0))},H.instantPrevious=function(){q>0&&(q-=1,e(q,!0,!0,!0))},H.destroy=function(){return!!D&&(G.setHeights&&a(G.section).each(function(){a(this).css("height","auto")}),u.off("resize",l.handleResize),G.scrollbars&&(u.off("mousedown",j.handleMousedown),u.off("mouseup",j.handleMouseup),u.off("scroll",j.handleScroll)),u.off(F,j.wheelHandler),u.off("keydown",j.keyHandler),c.addEventListener&&G.touchScroll&&(c.removeEventListener("touchstart",k.touchHandler,!1),c.removeEventListener("touchmove",k.touchHandler,!1),c.removeEventListener("touchend",k.touchHandler,!1)),m=[],n=[],o=[],void(p=[]))},H.update=function(){return!!D&&void l.handleUpdate()},H.current=function(){return o[q]},H.disable=function(){z=!0},H.enable=function(){z=!1,D&&j.calculateNearest(!1,!1)},H.isDisabled=function(){return z},H.setOptions=function(c){return!!D&&void("object"==typeof c?(G=a.extend(G,c),l.handleUpdate()):b.console&&console.warn("Scrollify warning: setOptions expects an object."))},a.scrollify=H,H});

$(document).ready(function(){
   __windowwidth = $(window).width();
    
   if(__windowwidth > 800){
        $.scrollify({
            section : ".panel",
            scrollSpeed:900,
            setHeights:true,
            updateHash:false
        });    
    }
    else{
        $('.sizeme').width(__windowwidth);
    }
    
    Tawk_API.onLoad = function(){
        Tawk_API.hideWidget();
    };
    
    // NON ESSENTIALS
    $('#emailForm').append('<input type="hidden" name="spam" value="nothankyou" />');
    $('.dlform_group').val(_ga.group);
    $('.dlform_campaign').val(_ga.campaign);
    $('.dlform_term').val(_ga.term);
    $('.dlform_keyword').val(_ga.keywords);
    $('.dlform_network').val(_ga.network);
    $('.dlform_referrer').val(_ga.referrer);
    $('.dlform_target').val(_ga.target);
    $('.dlform_position').val(_ga.position);
    $('.dlform_match').val(_ga.match);

});

function go_to_footer(){
    if(__windowwidth > 800)
        $.scrollify.move(6);
    else window.location = '#footer';
}

function showsel(a){
    window.clearInterval(__fadeasel);

    var $li = $(a).parent();
    if(!$li.hasClass('active')){
        $li.siblings().removeClass('active');
        $li.addClass('active');

        var index = $li.attr('index');
        var $fadeasel = $li.parent().prev();
        var $a = $fadeasel.children('a[index='+index+']');
        $a.hide();
        $fadeasel.prepend($a);
        $a.fadeIn(1000);
    }
}

function count_substring(haystack, needle){
    return haystack.match(new RegExp(needle, "g")).length
}

function stop_boxes($boxset){
    $boxset.data('autoplay', false);
}

function scroll_boxes(direction, arrow){
    var $boxset = $(arrow).closest('.boxes');
    var $slider = $boxset.find('.boxslider');
    var count = $boxset.find('.boxwrapper').length;
    var blockwidth = $boxset.find('.boxwrapper:first').width()+10;
    var current = (parseInt($boxset.data('current')) + direction);
    if((parseInt($boxset.data('current')) + direction) < 1)
        current = count;
    else if((parseInt($boxset.data('current')) + direction) > count)
        current = 1;

    if(!$boxset.data('animating')){
        $boxset.data('animating', true);
        $boxset.data('current', current);

        if(direction < 0){
            for(i=0; i<Math.abs(direction); i++) $boxset.find('.boxwrapper:last').prependTo($slider);
            $slider.css('left', (direction*blockwidth)+'px');
            $slider.animate({left:'0px'}, 400, function(){
                $boxset.data('animating', false);
            });
        }
        else if(direction > 0){
            $slider.animate({left:'-'+(direction*blockwidth)+'px'}, 400, function(){
                for(i=0; i<Math.abs(direction); i++) $boxset.find('.boxwrapper:first').appendTo($slider);
                $slider.css('left', '0px');
                $boxset.data('animating', false);
            });
        }
    }
}

function stop_carousel($carousel){
    $carousel.data('autoplay', false);
}

function carousel_goto(newpos, $carousel){
    var currentpos = parseInt($carousel.find('.dot.active').attr('rel'));
    scroll_carousel((newpos - currentpos), $carousel);
}

function scroll_carousel(direction, $carousel){
    var $slider = $carousel.find('.slider');
    var count = $carousel.find('.item').length;
    var blockwidth = $carousel.find('.item:first').width();
    var current = (parseInt($carousel.data('current')) + direction);
    if((parseInt($carousel.data('current')) + direction) < 1)
        current = count;
    else if((parseInt($carousel.data('current')) + direction) > count)
        current = 1;

    if(!$carousel.data('animating')){
        $carousel.data('animating', true);
        $carousel.data('current', current);

        if(direction < 0){
            for(i=0; i<Math.abs(direction); i++) $carousel.find('.item:last').prependTo($slider);
            $slider.css('left', (direction*blockwidth)+'px');
            $slider.animate({left:'0px'}, 400, function(){
                if($carousel.find('.dot.active').length){
                    var currentpos = parseInt($carousel.find('.dot.active').attr('rel'));
                    var newpos = ((currentpos + direction) <= 0) ? $carousel.find('.dot').length - Math.abs(currentpos + direction) : (currentpos + direction);
                    $carousel.find('.dot.active').removeClass('active');
                    $carousel.find('.dot:nth-child('+newpos+')').addClass('active');
                }
                $carousel.data('animating', false);
            });
        }
        else if(direction > 0){
            $slider.animate({left:'-'+(direction*blockwidth)+'px'}, 400, function(){
                for(i=0; i<Math.abs(direction); i++) $carousel.find('.item:first').appendTo($slider);
                $slider.css('left', '0px');
                if($carousel.find('.dot.active').length){
                    var currentpos = parseInt($carousel.find('.dot.active').attr('rel'));
                    var newpos = ((currentpos + direction) > $carousel.find('.dot').length) ? Math.abs(currentpos + direction) - $carousel.find('.dot').length : (currentpos + direction);
                    $carousel.find('.dot.active').removeClass('active');
                    $carousel.find('.dot:nth-child('+newpos+')').addClass('active');
                }
                $carousel.data('animating', false);
            });
        }
    }
}

function checkdlform(form){
    var $form = $(form);
    var errors = false;
    $form.find('.inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });

    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }

    if(!emailIsValid($form.find('input[name=email]').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }

    var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
    $form.find('input[name=00N3600000Los6F]').val(os_select);
    document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";

    var name = $form.find('input[name=first_name]').val();
    var email = $form.find('input[name=email]').val();
    var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
    document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";

    var iscookie = readCookie('download_params');
    if(iscookie)
        $form.find("#00N3600000LosKl").val(0);

    var refpromo = readCookie('APrefpromo');
    if(refpromo)
        $form.find("#00N3600000LosAC").val(refpromo);
    refpromo = $form.find('#00N3600000LosAC').val();
    document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";

    if(typeof _gaq !== 'undefined') { _gaq.push(['_trackPageview', '/download_start']); }

    return true;
}

function emailIsValid(inputvalue){
    var pattern = /^(([^<>()[\]\\.,;:\s@\"#]+(\.[^<>()[\]\\.,;:\s@\"#]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return pattern.test(inputvalue);
}

function launch_download_lightbox(){
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ){
        lightbox_mobilecta();
        return;
    }
    else{
        $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
        $('#theatre').click(function(){
            hide_hoverform();
        });
        show_downloadform();
    }
}
function hide_hoverform(){
    $('.lightbox, #theatre').fadeOut();
    $('#lightboxiframehover').fadeOut(function(){
        $('#lightboxiframehover').remove();
        $('#mobilecta').remove();
    });
}

function show_downloadform(){
    _gaq.push(['_trackEvent', 'Download Form', 'Open', 'Opened from "'+current_page+'"']);

    $('#lightbox_download')
        .css('left', (($(document).width()/2)-(($('#lightbox_download').width()+20)/2))+'px')
        .css('top', '20px')
        .fadeIn();
}

function lightbox_video(url){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });

    $('body').append('<div class="lightbox" id="lightboxiframehover"><iframe allowtransparency="true" src="'+url+'" width="100%" height="100%" scrolling="auto" align="top" frameborder="0" allowfullscreen></iframe>');
    $('#lightboxiframehover').css('left', (($(document).width()/2)-(($('#lightboxiframehover').width()+20)/2))+'px').css('top', (($(window).height()/2)-(($('#lightboxiframehover').height()+20)/2))+'px').fadeIn();
}

function lightbox_mobilecta(){
    $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
    $('#theatre').click(function(){
        hide_hoverform();
    });

    $('body').append('\
        <div class="lightbox" id="mobilecta" style="text-align:center">\
            <a href="javascript:void(0)" class="close" onclick="$(\'#theatre\').click();">Close</a>\
            <p><img src="'+BASEPATH+'img/popup/aw.jpg" style="width:80%" /></p>\
            <p class="header">I want to...</p>\
            <p><a class="cta" href="javascript:void(0)" onclick="lightbox_mobilecta2()">MAKE A PHOTO BOOK</a></p>\
            <p><a class="cta" href="'+BASEPATH+'all-products">MAKE ANOTHER PRODUCT</a></p>\
            <p><a style="font-size:16px" href="javascript:void(0)" onclick="$(\'#theatre\').click();"><img style="margin-right:5px;" src="'+BASEPATH+'img/popup/x.jpg" />Close to browse</a></p>\
            ');
    var topval = (($(window).height()/2)-(($('#mobilecta').height())/2)) < 20 ? 20 : (($(window).height()/2)-(($('#mobilecta').height())/2));
    $('#mobilecta').css('left', (($(document).width()/2)-(($('#mobilecta').width()+20)/2))+'px').css('top', topval+'px').fadeIn();
}

function lightbox_mobilecta2(){
    if(!$('#theatre').is(':visible')){
        $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
        $('#theatre').click(function(){
            hide_hoverform();
        });
    }

    $('#mobilecta').remove();
    $('body').append('\
        <div class="lightbox" id="mobilecta" style="text-align:center">\
            <a href="javascript:void(0)" class="close" onclick="$(\'#theatre\').click();">Close</a>\
            <p class="header">Want to make a Photo Book?</p>\
            <p><img src="'+BASEPATH+'img/popup/p1.jpg" width="80%" /><br>Go to our website on a desktop computer or laptop.</p>\
            <p><img src="'+BASEPATH+'img/popup/p2.jpg" width="80%" /><br>Click on GET STARTED NOW</p>\
            <p><img src="'+BASEPATH+'img/popup/p3.jpg" width="80%" /><br>Download our easy to use Editor.</p>\
            <hr/>\
            <p class="header">Want to</p>\
            <p><a class="cta" href="'+BASEPATH+'buy-now-make-later">BUY NOW, MAKE LATER?</a></p>\
            <hr/>\
            <p class="header">Want to make another product using mobile or tablet?</p>\
            <p><a class="cta" href="'+BASEPATH+'all-products">ALL PRODUCTS</a></p>\
            <p><a style="font-size:16px" href="javascript:void(0)" onclick="$(\'#theatre\').click();"><img style="margin-right:5px;" src="'+BASEPATH+'img/popup/x.jpg" />Close to browse</a></p>\
            ');
    var topval = 20;//(($(window).height()/2)-(($('#mobilecta').height())/2)) < 20 ? 20 : (($(window).height()/2)-(($('#mobilecta').height())/2));
    $('#mobilecta').css('left', (($(document).width()/2)-(($('#mobilecta').width()+20)/2))+'px').css('top', topval+'px').fadeIn();
}

function validatecontact(){
    var errors = false;
    $('#emailForm .inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });

    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }

    if(!emailIsValid($('#emailForm #contact_email').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }

    return true;
}

function validaterewards(){
    var errors = false;
    $('#rewardForm .inputbox').each(function(){
        if($(this).val()=='' || $(this).val()==$(this).attr('alt')){
            errors = true;
        }
    });

    if(errors){
        alert('Please ensure all fields are complete and try again.');
        return false;
    }

    if(!emailIsValid($('#rewardForm #contact_email').val())){
        alert('Please enter a valid email address and try again.')
        return false;
    }

    window.location = BASEPATH+'mybalance/'+$('#rewardForm #contact_email').val();
    return false;
}

function validateGvForm() {
    var fields = ['gv_first_name','gv_email','gv_giftvoucher'];
    var val = '';
    var fail = false;
    $.each(fields, function() {
        if($("#" + this).val() == '') {
            fail = true;
        }
    });
    if(fail == true) {
        alert('Please fill in all fields and try again');
        return false;
    }
    if(!emailIsValid($('#gv_email').val())){
        alert('Please check your email address and try again');
        return false;
    }
    return true;
}

function open_pricingfold(product, foldup){
    var $foldup = $('#'+product+'_pricing .foldup.'+foldup);
    var $handle = $foldup.next();
    if(!$foldup.is(':visible')){
        $.scrollTo('a[name='+foldup+']', 0);
        $foldup.slideDown(function(){
            //$.scrollTo('a[name='+foldup+']', 0);
            $handle.addClass('active');
        });
    }
}

function close_pricingfold(product, foldup){
    var $foldup = $('#'+product+'_pricing .foldup.'+foldup);
    var $handle = $foldup.next();
    if($foldup.is(':visible')){
        $foldup.slideUp(function(){
            $handle.removeClass('active');
        });
    }
}

function toggle_pricingfold(product, foldup){
    var $foldup = $('#'+product+'_pricing .foldup.'+foldup);
    if($foldup.is(':visible'))
        close_pricingfold(product, foldup);
    else open_pricingfold(product, foldup);
}

function update_price(){
    var total = 0;
    $('.cost').each(function(){
        total += parseFloat($(this).find('option:selected').data('price'))
    });
    $('#total').text('$'+total.toFixed(2));
}

function toggle_menu(){
    var $handle = $('#menuburger');
    var $left = $('#topmenu');
    if($left.css('position') == 'absolute'){
        if(parseInt($left.css('left')) == 0){
            $left.animate({
                left:'-195px'
            }, 300);
            $handle.animate({
                left:'195px'
            }, 300);
        }
        else if(parseInt($left.css('left')) == -195){
            $left.animate({
                left:'0px'
            }, 300);
            $handle.animate({
                left:'385px'
            }, 300);
        }
    }
}

function brochurelink_activate(link, url){
    $(link).css('font-weight', 600);
    $(link).siblings('a').css('font-weight', 300);
    $(link).closest('.brochure').css('background-image', 'url('+url+')');
}

function urlencode (str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
        replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}
