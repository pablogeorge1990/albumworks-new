<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create any Photo Book and get 40% off! | albumworks</title>
        <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-nov18/common.css?<?=time()?>" />

        <script type="text/javascript"src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js"async></script>

        <script type="text/javascript">
            window.smartlook||(function(d) {
                var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
                var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
                c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '742cc0afc69d8afb67a6ac020f80347eee46766e');
        </script>
    </head>
    <body>

    <div id="header" class="fullwidth">
        <div id="minitp" class="nomobile">
            <div class="trustpilot-widget" data-locale="en-AU" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="55f980f20000ff000583623f" data-style-height="68px" data-style-width="120px" data-theme="light">
                <a href="https://au.trustpilot.com/review/albumworks.com.au"target="_blank">Trustpilot</a>
            </div>
        </div>
        <a class="logo" target="_blank" href="https://www.albumworks.com.au/">
            <img src="<?=getenv('BASEPATH')?>img/logo.jpg" />
        </a>
    </div>
    <div id="topcontainer">
        <div id="top" class="fullwidth">
            <div id="agent" style="background-image:url(<?=getenv('BASEPATH')?>statics/landing-nov18/F-sep-adwords-02_03.jpg)">
                <h3 style="background-image:url(<?=getenv('BASEPATH')?>statics/landing-nov18/02-cs-icon.png)">Hi and welcome to <em>albumworks</em>.</h3>
                <p>I often get asked what makes us different? For me there are two reasons. First, our Photo Books are carefully crafted and include a generous 40 pages in every book. Plus we provide more printing options than almost anyone else.</p>
                <p>The second reason is "us" and how we work with you.</p>
                <p>We know that we are not making "just another book"... but instead we are bringing your memories to life. And it just has to be right. You may not be sure about which style or what paper is the right choice and our job is to make sure you are 100% comfortable with your decision.</p>
                <p>So, if you need some advice or assistance - please reach out. We’re here on <strong>1300 553 448</strong>, on chat or email us at <a href="mailto:&#115;&#101;&#114;&#118;&#105;&#099;&#101;&#064;&#097;&#108;&#098;&#117;&#109;&#119;&#111;&#114;&#107;&#115;&#046;&#099;&#111;&#109;&#046;&#097;&#117;">&#115;&#101;&#114;&#118;&#105;&#099;&#101;&#064;&#097;&#108;&#098;&#117;&#109;&#119;&#111;&#114;&#107;&#115;&#046;&#099;&#111;&#109;&#046;&#097;&#117;</a>.</p>
                <p><strong>- Kim.</strong></p>
            </div>
            <div id="imagearea" style="background-image:url(<?=getenv('BASEPATH')?>statics/landing-nov18/01-header.jpg)">
                <h1>40% OFF ANY PRODUCT</h1>
                <h2>FOR FIRST TIME ORDERS ONLY</h2>
                <p><a href="javascript:void(0)" onclick="if(__mobile) { window.location = 'mobile-books'; } else { splitchoice('photo-book-picker'); }"><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/04-CTA.png" alt="GET STARTED NOW" /></a></p>
            </div>
        </div>
    </div>
    <div id="voucher" class="fullwidth">
        <p>
            40% OFF WITH VOUCHER CODE:
            <span class="nomobile">&nbsp;&nbsp;&nbsp;</span>
            <span class="mobileonlyblock"><br/></span>
            <span id="code">FORTYFREE</span>
            <span class="nomobile">&nbsp;&nbsp;&nbsp;</span>
            <span class="mobileonlyblock"><br/></span>
            <a href="javascript:void(0)" onclick="if(__mobile) { window.location = 'mobile-books'; } else { splitchoice('photo-book-picker'); }"><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/04-CTA.png" alt="GET STARTED NOW" /></a>
        </p>
    </div>

    <div id="sizesandprices" class="fullwidth">
        <h1 style="margin-top:0;">SIZES &amp; PRICES</h1>

        <div class="carousel controls">
            <a class="left arrow" href="javascript:void(0)" onclick="stop_carousel($(this).closest('.carousel')); scroll_carousel(1, $(this).closest('.carousel'))"><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/arrow-left.png" /></a>
            <div class="sliderwrapper">
                <div class="slider">
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov18/01-6x6.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov18/02-8x6.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov18/03-8x8.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov18/04-8x11.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov18/05-11x85.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov18/06-12x12.jpg" />
                    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov18/07-6x12.jpg" />
                </div>
            </div>
            <a class="right arrow" href="javascript:void(0)" onclick="stop_carousel($(this).closest('.carousel')); scroll_carousel(-1, $(this).closest('.carousel'))"><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/arrow-right.png" /></a>
        </div>

        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <th><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/icon-book.png" /></th>
                <td><p>Classic Photo Books come with 40 pages included. Books may have minimum 30 pages, or maximum 200 pages.</p></td>
            </tr>
            <tr>
                <th><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/icon-ship.png" /></th>
                <td><p>95% off all items are manufactured and dispatched within 2-4 business days. We recommend allowing 7 business days for dispatch for rare occasions when your order doesn't pass our quality control.</p></td>
            </tr>
            <tr>
                <th><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/icon-globe.png" /></th>
                <td><p>Send your Photo Book to Australia, Europe, New Zealand, USA, Canada and South East Asia.</p></td>
            </tr>
        </table>
    </div>

    <div id="trustpilot" class="fullwidth">
        <div class="border group">
            <div id="animated">
                <video width="100%" loop autoplay muted preload>
                    <source src="<?=getenv('BASEPATH')?>statics/landing-forty18/book-vid.mp4" type="video/mp4">
                    <img src="<?=getenv('BASEPATH')?>statics/landing-forty18/screencap.jpg" width="100%" height="100%" />
                </video>
            </div>
            <div id="tp">
                <div class="trustpilot-widget" data-locale="en-AU" data-template-id="539ad0ffdec7e10e686debd7" data-businessunit-id="55f980f20000ff000583623f" data-style-height="277px" data-style-width="100%" data-theme="light" data-stars="4,5">
                    <a href="https://au.trustpilot.com/review/albumworks.com.au"target="_blank">Trustpilot</a>
                </div>
            </div>
        </div>
    </div>

    <div id="footer" class="fullwidth">
        <h1>40% OFF ANY PRODUCT</h1>
        <h2>FOR FIRST TIME ORDERS ONLY</h2>
        <p><a href="javascript:void(0)" onclick="if(__mobile) { window.location = 'mobile-books'; } else { splitchoice('photo-book-picker'); }"><img src="<?=getenv('BASEPATH')?>statics/landing-nov18/04-CTA.png" alt="GET STARTED NOW" /></a></p>
    </div>

    <div id="whitevoucher" class="voucher fullwidth">
        <p>
            40% OFF WITH VOUCHER CODE:
            <span class="nomobile">&nbsp;&nbsp;&nbsp;</span>
            <span class="mobileonlyblock"><br/></span>
            <span class="code">FORTYFREE</span>
        </p>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/landing-nov18/common.js"></script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1338422-2']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        var host = window.location.host;
    </script>

    <style type="text/css">
        body{
            position:relative;
        }

        #splittheatre{
            width:100px;
            height:100px;
            position:fixed;
            z-index:99998;
            background:black;
            opacity:0.7;
            filter:alpha(opacity=70);
            top:0;
            left:0;
            display:none;
        }

        #splitchoice{
            text-align: left;
            padding:20px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            width:90%;
            max-width:660px;
            position:fixed;
            top:120px;
            background:white;
            z-index:99999;
            display:none;
            padding:10px 20px;
        }

        #splitchoice h3{
            color: #3381c7;
            font-family: 'Merriweather', serif;
            font-weight:400;
            font-size:28px;
            text-align:center;
            margin:0.5em;
            padding:0;
            background:none;
            height:auto;
        }

        #splitchoice p{
            font-weight:300;
            margin:1em 0;
        }
        #splitchoice p.dlbl{
            text-align: center;
            margin:0 auto;
            color:#0f65ba;
            font-size:12px;
            width:100%;
            font-weight:500;
            line-height:1.5;
        }

        #splitchoice .dlopt{
            width:100%;
            padding:0;
            margin-bottom:20px;
            text-align:center;
        }
        #splitchoice .dlopt:first-of-type{
            border-bottom:1px solid #0f63b9;
            border-top:3px solid #0f63b9;
            margin-top:20px;
            margin-bottom:0px;
        }

        #splitchoice .dlopt h3{
            margin:0;
        }

        #splitchoice .dlopt .dlbl{
            margin:0 0 0.5em;
        }

        #splitchoice .editorimg{
            float:left;
        }

        #splitchoice h4{
            font-size:16px;
            margin-bottom:1em;
        }

        #splitchoice .inputbox{
            border:1px solid #a0c1e4;
            padding:5px 10px;
            display:block;
            width:calc(50% - 30px);
            margin-bottom:1em;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
            font-size:16px;
            color:#0f65ba;
            font-family:'Open Sans', 'Source Sans Pro', sans-serif;
        }

        #splitchoice .inputboxp{
            margin:10px 30px 0;
        }

        #splitchoice .checkarea{
            margin:0 auto;
            border:0;
            width:50%;
            display:block;
        }

        #splitchoice .checkarea .check{
            float:left;
            margin-right:5px;
        }

        #splitchoice .checkarea label{
            font-size:9px;
            opacity:0.7;
            float:left;
            display:block;
            padding-top:5px;
            color:#0f63b9;
        }

        #splitchoice .smallprint{
            font-size:10px;
            color:#0f63b9;
        }
    </style>
    <div id="splittheatre"> </div>
    <div id="splitchoice" class="lightbox">
        <h1 style="font-family:'Merriweather', serif; background: #3775c2;color: white;margin: 0;margin-left: -20px;margin-top: -10px;width: 100%;padding-top: 20px;padding-bottom: 20px;font-weight: 600;border-top-left-radius: 5px;border-top-right-radius: 5px;font-size: 28px;">Here's your Voucher Code <span style="display: inline-block;margin-left: 20px;border: 2px dashed white;padding: 5px;font-family:'Open Sans', sans-serif; font-weight:800;">FORTYFREE</span></h1>
        <form id="splitchoice_form" class="webtolead" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkscform(this)">
            <input type="hidden" value="00D36000000oZE6" name="sfga">
            <input type="hidden" value="00D36000000oZE6" name="oid">
            <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=FORTYFREE" name="retURL">
            <input type="hidden" value="Website" name="lead_source">
            <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
            <input type="hidden" value="Download" name="00N3600000BOyGd">
            <input type="hidden" value="AP" name="00N3600000BOyAt">
            <input type="hidden" value="PG" name="00N3600000Loh5K">
            <input type="hidden" name="00N3600000Los6F" value="Windows">
            <!-- Referring Promotion --><input type="hidden" value="FORTYFREE" name="00N3600000LosAC">
            <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
            <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
            <!-- Adword fields -->
            <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
            <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
            <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
            <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
            <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
            <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
            <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
            <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
            <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
            <!-- Actual fields -->

            <h3>How would you like to get started?</h3>
            <p class="dlbl">CHOOSE TO MAKE YOUR PRODUCT WITH OUR HIGH PERFORMANCE</p>
            <p class="dlbl">DOWNLOAD EDITOR OR ONLINE IN YOUR WEB BROWSER</p>

            <div class="group">
                <div class="dlopt">
                    <p><img src="https://www.albumworks.com.au/img/splitchoice/download.jpg" /></p>
                    <p class="dlbl">&bull; PERFECT FOR PROJECTS BIG AND SMALL</p>
                    <p class="dlbl">&bull; WIDEST RANGE OF DESIGN TOOLS AND ABILITIES</p>
                    <p class="dlbl">&bull; ENTER YOUR DETAILS TO RECEIVE YOUR VOUCHER CODE</p>

                    <p class="inputboxp group">
                        <input type="text" id="field_splitchoice_download_first_name" name="first_name" class="firstname inputbox" placeholder="Name" style="float:left">
                        <input type="text" id="field_splitchoice_download_email" name="email" class="email inputbox" placeholder="Email" style="float:right">
                    </p>
                    <div class="checkarea group">
                        <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
                        <label for="field_lightbox_download_emailoptout">Keep me updated with special offers and software updates</label>
                    </div>
                    <a style="margin-top:10px; padding:10px 30px; font-size: 17px;" class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">DOWNLOAD OUR FREE EDITOR</a>
                    <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and Conditions of this service</p>
                </div>
                <div class="dlopt">
                    <img src="https://www.albumworks.com.au/img/splitchoice/online.jpg" style="float:left; margin-top:20px;" />
                    <div style="float:left;margin:20px 0 0 20px">
                        <p style="margin:0 0 0.5em" class="dlbl">&bull; GREAT FOR SMALL/MEDIUM PROJECTS</p>
                        <p style="margin:0 0 0.5em" class="dlbl">&bull; CREATE ON ALL TYPES OF DEVICES</p>
                        <p style="margin:0 0 0.5em" class="dlbl">&bull; USE PHOTOS FROM YOUR DEVICE</p>
                        <a style="margin-top:1em; font-size:14px" class="cta" id="splitaltcta" href="javascript:void(0)">CREATE PRODUCTS ONLINE</a>
                        <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and<br>Conditions of this service</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        var __mobile = false;
        $(document).ready(function(){
            __mobile = $(window).width() < 959;
        });

        function splitchoice(onlinelink){
            $('#splittheatre').width($(document).width()).height($(document).height()).fadeIn();
            $('#splittheatre').click(function(){
                hide_splitchoice();
            });
            show_splitchoice(onlinelink);
        }

        function hide_splitchoice(){
            $('.lightbox, #splittheatre').hide();
            $('#splitchoice').hide();
        }

        function show_splitchoice(onlinelink){
            if(typeof ga !== 'undefined') { ga('send', 'event', 'Download Form', 'Open', 'Opened from "'+current_page+'"'); }

            $('#splitchoice #splitaltcta').attr('href', onlinelink);
            $('#splitchoice')
                .css('left', (($(document).width()/2)-(($('#splitchoice').width()+20)/2))+'px')
                .css('top', '20px')
                .fadeIn();
        }

        function checkscform(form){
            var $form = $(form);

            if(($form.find('input[name=email]').val() !== '') && !emailIsValid($form.find('input[name=email]').val())){
                alert('Please enter a valid email address and try again.')
                return false;
            }

            var os_select = (navigator.platform === 'MacIntel') ? 'MacOS' : 'Windows';
            $form.find('input[name=00N3600000Los6F]').val(os_select);
            document.cookie = "os_select="+os_select+"; ; path=/; domain=.albumworks.com.au";

            var email = $form.find('input[name=email]').val();
            var name = ($form.find('input[name=first_name]').val() === '') ? email : $form.find('input[name=first_name]').val();
            var option_mail = ($form.find('input[name=emailOptOut]').is(':checked')) ? 0 : 1;
            if(email !== '')
                document.cookie = "download_params=vendor:ap|name:"+name+"|email:"+email+"|om:"+option_mail+"|ou:0; ; path=/; domain=.albumworks.com.au";
            else document.cookie = "download_params=vendor:ap|ou:0; ; path=/; domain=.albumworks.com.au";

            var iscookie = readCookie('download_params');
            if(iscookie)
                $form.find("input[00N3600000LosKl]").val(0);

            var refpromo = readCookie('APrefpromo');
            if(refpromo)
                $form.find("input[00N3600000LosAC]").val(refpromo);
            refpromo = $form.find('input[00N3600000LosAC]').val();
            document.cookie = "download_params2=vendor:ap|referringPromotion:"+refpromo+"; ; path=/; domain=.albumworks.com.au";

            if(typeof gtag !== 'undefined') {
                gtag('config', 'UA-1338422-2', {'page_path': '/download_start'});
            }

            localStorage.setItem('DL_THANKYOU_NAME', name);

            return true;
        }
    </script>


        <div style="position:fixed">
            <script type="text/javascript">
                var current_page = 'landing-nov18';
                var BASEPATH = 'https://www.albumworks.com.au/';
                //var BASEPATH = host;

                <? if(isset($_GET['alertmsg'])): ?>
                window.setTimeout(function(){
                    alert("<?=addslashes($_GET['alertmsg'])?>");
                }, 1000);
                <? elseif(isset($_GET['download'])): ?>
                $(document).ready(function(){
                    launch_download_lightbox();
                })
                <? endif; ?>
            </script>

            <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="https://www.albumworks.com.au/css/ie.css" />
            <script type="text/javascript" src="https://www.albumworks.com.au/js/selectivizr-min.js"></script>
            <script type="text/javascript" src="https://www.albumworks.com.au/js/css3-mediaqueries.js"></script>
            <![endif]-->
            <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js#username=albumprinter"></script>

            <script type="text/javascript">
                var _ga = new Object;
                _ga.campaign = '';
                _ga.term = '';
                _ga.search= '';
                _ga.referrer = '';
                _ga.group = '';
                _ga.keywords = '';

                var gc = '';
                var c_name = "__utmz";
                if (document.cookie.length>0){
                    c_start=document.cookie.indexOf(c_name + "=");
                    if (c_start!=-1){
                        c_start=c_start + c_name.length+1;
                        c_end=document.cookie.indexOf(";",c_start);
                        if (c_end==-1) c_end=document.cookie.length;
                        gc = unescape(document.cookie.substring(c_start,c_end));
                    }
                }

                if(gc != ""){
                    var z = gc.split('.');
                    if(z.length >= 4){
                        var y = z[4].split('|');
                        for(i=0; i<y.length; i++){
                            if(y[i].indexOf('utmccn=') >= 0) _ga.campaign = y[i].substring(y[i].indexOf('=')+1);
                            if(y[i].indexOf('utmctr=') >= 0) _ga.term     = y[i].substring(y[i].indexOf('=')+1);
                        }
                    }
                }


                <?
                    function cleanit($string){
                        //return preg_replace("/[^A-Za-z0-9]/", '', strtolower($string));
                        return trim(strtolower($string));
                    }

                    if(strpos($_SERVER['REQUEST_URI'], '?') !== FALSE){
                        $querystring = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?')+1);
                        $queryparts = explode('&', $querystring);
                        foreach($queryparts as $part){
                            list($key, $val) = explode('=', $part);
                            $_GET[$key] = urldecode($val);
                        }
                        $_GET['q'] = $_SERVER['REQUEST_URI'];
                    }

                    if(isset($_SERVER['HTTP_REFERER'])){
                        $parts = parse_url($_SERVER['HTTP_REFERER']);
                        $_SERVER['HTTP_REFERER'] = $parts['host'];
                    }

                    if(isset($_GET['campaign'])) $_GET['campaign'] = cleanit($_GET['campaign']);
                    if(isset($_GET['group'])) $_GET['group'] = cleanit($_GET['group']);
                ?>


                <? if(isset($_GET['q'])): ?> _ga.search = "<?=addslashes($_GET['q'])?>"; <? endif; ?>

                <? if(isset($_GET['_kk'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kk']))?>"; <? endif; ?>
                <? if(isset($_GET['_kw'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kw']))?>"; <? endif; ?>

                <? if(isset($_GET['group'])): ?> _ga.group = "<?=addslashes(cleanit($_GET['group']))?>"; <? endif; ?>
                <? if(isset($_GET['campaign'])): ?> _ga.campaign = "<?=addslashes(($_GET['campaign'] == "notset") ? "" : cleanit($_GET['campaign']))?>"; <? endif; ?>
                <? if(isset($_GET['network'])): ?> _ga.network = "<?=((($_GET['network'] == 'g') or ($_GET['network'] == 's')) ? 'Search' : 'Content')?>"; <? endif; ?>

                <? if(isset($_SERVER['HTTP_REFERER'])): ?> _ga.referrer = "<?=addslashes($_SERVER['HTTP_REFERER'])?>"; <? endif; ?>
                <? if(isset($_GET['placement'])): ?> _ga.referrer = "<?=addslashes($_GET['placement'])?>"; <? endif; ?>
                <? if(isset($_GET['_placement'])): ?> _ga.referrer = "<?=addslashes($_GET['_placement'])?>"; <? endif; ?>

                <? if(isset($_GET['target'])): ?> _ga.target = "<?=addslashes($_GET['target'])?>"; <? endif; ?>
                <? if(isset($_GET['position'])): ?> _ga.position = "<?=addslashes($_GET['position'])?>"; <? endif; ?>
                <? if(isset($_GET['match'])): ?> _ga.match = "<?=addslashes($_GET['match'])?>"; <? endif; ?>

                <? if(isset($_GET['gclid'])): ?>
                createCookie('gclid','<?=addslashes($_GET['gclid'])?>',90);
                <? endif; ?>
                <? if(isset($_GET['msclkid'])): ?>
                createCookie('gclid','bing<?=addslashes($_GET['msclkid'])?>',90);
                <? endif; ?>

                jQuery('input[name=00N3600000BOyGd]').each(function(){
                    var $form = $(this).closest('form');
                    if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
                        $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
                    }
                });

            </script>

            <!-- Start of LiveChat (www.livechatinc.com) code -->
            <script type="text/javascript">
                window.__lc = window.__lc || {};
                window.__lc.license = 10035965;
                (function() {
                    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
                })();
            </script>
            <noscript>
                <a href="https://www.livechatinc.com/chat-with/10035965/">Chat with us</a>,
                powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
            </noscript>
            <!-- End of LiveChat code -->

        </div>
    </body>
</html>