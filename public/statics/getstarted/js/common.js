var __breadcrumbs = new Array();
var __choices = new Array();

$(document).ready(function(){
    preload(); 
    onresize();
        
    $('.option').each(function(){
        var $option = $(this);
        var icon = $option.attr('icon');
        
        $option.css('background-image', 'url('+icon+')');
        
        if($option.is(':nth-child(4n-3)')) $option.addClass('startofline');
        if($option.is(':nth-child(4n)')) $option.addClass('endofline');
        if($option.is(':nth-last-child(1)')) $option.addClass('endofline');
    });
    
    $(document).on('click', '.option', function(){
        var $option = $(this);
        if(!$option.hasClass('active')){
            $('.option.active').removeClass('active');
            $option.addClass('active');
            show_zoom();
        }
        else{
            $('#zoomwrapper').hide('blind', 'up', 200, function(){
                $('.option.active').removeClass('active');
                $('#zoomwrapper').remove();
            });
        }
    });
});

$(window).resize(function(){
    onresize();
})

function onresize(){
    $('body').css('min-height', ($(window).height() - 178)+'px');
}

function show_zoom(){
    var $option = $('.option.active');
    var next_endofline_index = $option.hasClass('endofline') ? $option.index() : $option.nextAll('.option.endofline').index();
    var zoom_is_after_next_endofline = ($('#zoomwrapper').index() > next_endofline_index);
    var zoom_is_on_same_row = (($('#zoomwrapper').index() - next_endofline_index) == 1);
    
    if($('#zoomwrapper').length && zoom_is_on_same_row){
        var activeindex = ($option.index() % 4);    
        $('#zoomslider').animate({left:'-'+(activeindex*600)+'px'}, 500);
    }
    else{    
        $('#zoomwrapper').remove();
        var activeindex = ($option.index() % 4);    
        
        var finished = false;
        $curoption = $option.hasClass('startofline') ? $option : $option.prevAll('.startofline');
        console.log($curoption);
        var zoomhtml = '<div id="zoomwrapper"><div id="zoomslider" style="left:-'+(activeindex*600)+'px">';                
        while(!finished){
            zoomhtml += '<div class="zoom"><div class="arrow" style="left:'+((($curoption.index() % 4) * 149) + 58)+'px"> </div><a class="button" href="javascript:void(0)" onclick="next_choice()">Select this product</a><a class="image" href="javascript:void(0)" onclick="next_choice()"><img src="'+$curoption.attr('zoom')+'" /></a></div>';
            if($curoption.hasClass('endofline') || ($curoption.next().length <= 0)) finished = true;
            $curoption = $curoption.next();
        }
        zoomhtml += '</div></div>';        
        
        var $after = $option.parent().find('.option:last');    
        if($option.hasClass('endofline'))
            $after = $option;
        else if($option.nextAll('.endofline').length)
            $after = $option.nextAll('.endofline:first');                
        $after.after(zoomhtml);
        $('#zoomwrapper').show('blind', 'down', 500);    
    }
}

function next_choice(){   
    var $option = $('.option.active');
    var category = $option.parent().attr('category');
    var choice = $option.attr('rel');
    var choicelabel = $option.html().replace('<br>', ' ');
    var next = $option.attr('next');
    
    var choiceobject = new Object();
    eval('choiceobject.'+category+' = choice;');
    __choices.push(choiceobject);
    
    drop_breadcrumb();
    add_breadcrumb(choicelabel, category);
    
    $('#zoomwrapper').hide('blind', 'up', 200, function(){
        $('#zoomwrapper').remove();
        if(next == 'submit'){
            var choiceobject = new Object();
            choiceobject.vendorid = '1031';
            __choices.push(choiceobject);
            
            _gaq.push(['_trackPageview', '/web_editor_launch']);
            $('#loadingtheatre').width($(window).width()).height($(window).height()).fadeIn(500);
            var gotourl = 'https://api.photo-products.com.au/redirector/index.php?data='+JSON.stringify(__choices);
            window.parent.location = gotourl;
            return;
        }
        else{
            window.scrollTo(0,0);
            $('#choices .choose').wrap('<div id="choiceslider" />');
            $('#options .choose[category='+next+']').clone().addClass('active').appendTo($('#choiceslider'));
            $('#choiceslider').animate({left: '-600px'}, 500, function(){
                activate_choice(next);
            });
        }
    });    
}

function prev_choice(category, choicenum){
    if($('#zoomwrapper').length){
        $('#zoomwrapper').hide('blind', 'up', 200, function(){
            window.scrollTo(0,0);
            $('#zoomwrapper').remove();
            slide_backwards(category, choicenum);
        });        
    }
    else{
        window.scrollTo(0,0);
        slide_backwards(category, choicenum);
    }
}

function slide_backwards(category, choicenum){
    $('#choices .choose').wrap('<div id="choiceslider" class="leftslider" />');
    
    var panels_to_slide = __choices.length - choicenum;
    for(i = __choices.length; i >= choicenum; i--){
        for(var prepender in __choices[i]){
            if(__choices[i].hasOwnProperty(prepender)){
                $('#options .choose[category='+prepender+']').clone().addClass('active').appendTo($('#choiceslider'));
            }
        }
    }
    $('#choiceslider').animate({right: '-'+(panels_to_slide*600)+'px'}, 500, function(){
        while(__choices.length > choicenum) __choices.pop();
        while(__breadcrumbs.length > choicenum) drop_breadcrumb();        
        activate_choice(category);
    });    
}

function drop_breadcrumb(){
    __breadcrumbs.pop();    // the sep
    __breadcrumbs.pop();    // the crumb
}

function add_breadcrumb(label, link){
    if(__breadcrumbs.length > 0){    
        var crumbobject = new Object();
        crumbobject.label = '&gt;';
        crumbobject.link = 'sep';
        __breadcrumbs.push(crumbobject);
    }
    
    var crumbobject = new Object();
    crumbobject.label = label;
    crumbobject.link = link;
    __breadcrumbs.push(crumbobject);        
}

function activate_choice(choice){
    add_breadcrumb($('#options .choose[category='+choice+']').attr('label'), '');
    $('#choices').html('').append($('#options .choose[category='+choice+']').clone().addClass('active'));

    $('#breadcrumbs').html('');
    $.each(__breadcrumbs, function(index, value){
        if(value.link === 'sep')
            $('#breadcrumbs').append('<li class="sep">'+value.label+'</li>');
        else if(value.link === '')
            $('#breadcrumbs').append('<li>'+value.label+'</li>');
        else $('#breadcrumbs').append('<li><a href="javascript:void(0)" onclick="prev_choice(\''+value.link+'\', '+index+')">'+value.label+'</a></li>');
    });
    $('#breadcrumbs li:last').addClass('active');
}

function preload(){
    $('body').append('<img class="preload" src="img/getstarted/white_notch.png" />');
    $('body').append('<img class="preload" src="img/getstarted/bar_notch.jpg" />');
    $('body').append('<img class="preload" src="img/getstarted/button.jpg" />');
    $('body').append('<img class="preload" src="img/getstarted/zoombg.png" />');
}