<!DOCTYPE html>
<html>
<head>
    <title>Get Started</title>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" type="text/css" />
    <link href="https://www.albumworks.com.au/img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?=getenv('BASEPATH')?>statics/getstarted/css/common.css?<?=time()?>">
</head>
<body>
    <div id="container">
        <div id="header">
            <a href="https://www.albumworks.com.au"><img src="https://www.albumworks.com.au/img/getstarted/logo_wizard.png" alt="albumworks" /></a>
        </div>
        <div id="wizard">
            <ul id="breadcrumbs"></ul>
            <div id="choices"></div>
            <div id="options">
                <div class="choose" category="product" label="Choose Product">
                    <div class="option"             icon="wizard/photobook/112x112/1/Category_Photobooks_112x112.jpg"   zoom="wizard/photobook/600x400/1/Category_Photobooks.jpg"   rel="photobook"     next="theme">Photo Books</div>
                    <div class="option"             icon="wizard/calendar/112x112/1/Category_Cal_112x112.jpg"           zoom="wizard/calendar/600x400/1/Category_Cal.jpg"           rel="calendar"      next="calendars">Calendars</div>
                    <div class="option"             icon="wizard/canvas/112x112/1/Category_Canvas_112x112.jpg"          zoom="wizard/canvas/600x400/1/Category_Canvas.jpg"          rel="canvas"        next="canvas_collection">Canvas Prints</div>
                    <div class="option"             icon="wizard/poster/112x112/1/Category_Posters_112x112.jpg"         zoom="wizard/poster/600x400/1/Category_Posters.jpg"         rel="poster"        next="poster_collection">Poster Prints</div>
                    <div class="option twolines"    icon="wizard/diary/112x112/1/Category_DiariesNotebooks_112x112.jpg" zoom="wizard/diary/600x400/1/Category_DiariesNotebooks.jpg" rel="diary"         next="diary_collection">Diaries and<br>Notebooks</div>
                    <div class="option"             icon="wizard/photogift/112x112/1/Category_PhotoGifts_112x112.jpg"   zoom="wizard/photogift/600x400/1/Category_PhotoGifts.jpg"   rel="photogift"     next="photogift">Photo Gifts</div>
                </div>
                
                <!-- PHOTOBOOK -->
                    <div class="choose" category="theme" label="Choose Theme">
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Blank_112x112.jpg"           zoom="wizard/photobook/600x400/2/Theme_Blank.jpg"           rel="PB-CONT-BLANK"     next="layout">Blank</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Classic_112x112.jpg"         zoom="wizard/photobook/600x400/2/Theme_Classic.jpg"         rel="PB-LEGACY"         next="layout">Classic</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Contemporary_112x112.jpg"    zoom="wizard/photobook/600x400/2/Theme_Contemporary.jpg"    rel="PB-CONTEMPORARY"   next="layout">Contemporary</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_FullBleed_112x112.jpg"       zoom="wizard/photobook/600x400/2/Theme_FullBleed.jpg"       rel="PB-CONT-FULLBLEED" next="layout">Full Bleed</div>
                        <div class="option twolines"    icon="wizard/photobook/112x112/2/Theme_BlackWhite_112x112.jpg"      zoom="wizard/photobook/600x400/2/Theme_BlackWhite.jpg"      rel="PB-CONT-WK"        next="layout">Black with<br>White Line</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Filmstrip_112x112.jpg"       zoom="wizard/photobook/600x400/2/Theme_Filmstrip.jpg"       rel="PB-CONT-FILMSTRIP" next="layout">Filmstrip</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Love_112x112.jpg"            zoom="wizard/photobook/600x400/2/Theme_Love.jpg"            rel="PB-LOVE"           next="layout">Love</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_OldSchool_112x112.jpg"       zoom="wizard/photobook/600x400/2/Theme_OldSchool.jpg"       rel="PB-SCATTER-K"      next="layout">Scatter</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Timeline_112x112.jpg"        zoom="wizard/photobook/600x400/2/Theme_Timeline.jpg"        rel="PB-TIMELY"         next="layout">Timeline</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Travel_112x112.jpg"          zoom="wizard/photobook/600x400/2/Theme_Travel.jpg"          rel="PB-TRAVEL"         next="layout">Travel</div>
                        <div class="option"             icon="wizard/photobook/112x112/2/Theme_Wedding_112x112.jpg"         zoom="wizard/photobook/600x400/2/Theme_Wedding.jpg"         rel="PB-WEDDING"        next="layout">Wedding</div>
                    </div>
                    <div class="choose" category="layout" label="Choose Size / Orientation">
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Landscape_A3_112x112.jpg"     zoom="wizard/photobook/600x400/3/Size_Landscape_A3.jpg"     rel="A3A3LB"    next="cover">A3 Landscape</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Portrait_A3_112x112.jpg"      zoom="wizard/photobook/600x400/3/Size_Portrait_A3.jpg"      rel="A3A3PB"    next="cover">A3 Portrait</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Square_30x30_112x112.jpg"     zoom="wizard/photobook/600x400/3/Size_Square_30x30.jpg"     rel="3030SB"    next="cover">30x30cm Square</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Landscape_A4_112x112.jpg"     zoom="wizard/photobook/600x400/3/Size_Landscape_A4.jpg"     rel="A4A4LB"    next="cover">A4 Landscape</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Portrait_A4_112x112.jpg"      zoom="wizard/photobook/600x400/3/Size_Portrait_A4.jpg"      rel="A4A4PB"    next="cover">A4 Portrait</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Square_21x21_112x112.jpg"     zoom="wizard/photobook/600x400/3/Size_Square_21x21.jpg"     rel="2121SB"    next="cover">21x21cm Square</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Landscape_A5_112x112.jpg"     zoom="wizard/photobook/600x400/3/Size_Landscape_A5.jpg"     rel="A5A5LB"    next="cover">A5 Landscape</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Portrait_A5_112x112.jpg"      zoom="wizard/photobook/600x400/3/Size_Portrait_A5.jpg"      rel="A5A5PB"    next="cover">A5 Portrait</div>
                        <div class="option"             icon="wizard/photobook/112x112/3/Size_Square_15x15_112x112.jpg"     zoom="wizard/photobook/600x400/3/Size_Square_15x15.jpg"     rel="1515SB"    next="cover">15x15cm Square</div>
                    </div>
                    <div class="choose" category="cover" label="Choose Cover">
                        <div class="option"             icon="wizard/photobook/112x112/4/Cover_Photocover_112x112.jpg"      zoom="wizard/photobook/600x400/4/Cover_Photocover.jpg"      rel="_H"    next="submit">Photocover</div>
                        <div class="option twolines"    icon="wizard/photobook/112x112/4/Cover_DustJacket_112x112.jpg"      zoom="wizard/photobook/600x400/4/Cover_DustJackt.jpg"       rel="_D"    next="submit">Material Cover w/<br>Opt. Dust Jacket</div>
                    </div>
                
                <!-- CALENDARS -->
                    <div class="choose" category="calendars" label="Choose Size / Orientation">
                        <div class="option"             icon="wizard/calendar/112x112/3/Size_HD_Skyscraper_112x112.jpg"     zoom="wizard/calendar/600x400/3/Size_HD_Skyscraper.jpg" rel="CAL-300550"  next="calendar_layout_CAL__300550">Skyscraper</div>
                        <div class="option"             icon="wizard/calendar/112x112/3/Size_HD_A3_112x112.jpg"             zoom="wizard/calendar/600x400/3/Size_HD_A3.jpg"         rel="CAL-A3P"     next="calendar_layout_CAL__A3P">A3 Portrait</div>
                        <div class="option"             icon="wizard/calendar/112x112/3/Size_HD_A4_112x112.jpg"             zoom="wizard/calendar/600x400/3/Size_HD_A4.jpg"         rel="CAL-A4P"     next="calendar_layout_CAL__A4P">A4 Portrait</div>
                        <div class="option"             icon="wizard/calendar/112x112/3/Size_HD_A5_112x112.jpg"             zoom="wizard/calendar/600x400/3/Size_HD_A5.jpg"         rel="CAL-A5L"     next="calendar_layout_CAL__A5L">A5 Landscape</div>
                    </div>
                    <div class="choose" category="calendar_layout_CAL__A3P" label="Choose Style">
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A3_Full_Bleed_112x112.jpg"       zoom="wizard/calendar/600x400/4/HD_A3_Full_Bleed.jpg"       rel="AWF_A3A3PT_1"   next="submit">Full Bleed</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A3_Compact_Grid_112x112.jpg"     zoom="wizard/calendar/600x400/4/HD_A3_Compact_Grid.jpg"     rel="AWN_A3A3PT_1"   next="submit">Compact Grid</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A3_Large_Grid_112x112.jpg"       zoom="wizard/calendar/600x400/4/HD_A3_Large_Grid.jpg"       rel="AWG_A3A3PT_1"   next="submit">Large Grid</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A3_Perpetual_112x112.jpg"        zoom="wizard/calendar/600x400/4/HD_A3_Perpetual.jpg"        rel="AWP_A3A3PT_1"   next="submit">Perpetual</div>
                    </div>
                    <div class="choose" category="calendar_layout_CAL__A4P" label="Choose Style">
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A4_Full_Bleed_112x112.jpg"       zoom="wizard/calendar/600x400/4/HD_A4_Full_Bleed.jpg"       rel="AWF_A4A4PT_1"   next="submit">Full Bleed</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A4_Compact_Grid_112x112.jpg"     zoom="wizard/calendar/600x400/4/HD_A4_Compact_Grid.jpg"     rel="AWN_A4A4PT_1"   next="submit">Compact Grid</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A4_Large_Grid_112x112.jpg"       zoom="wizard/calendar/600x400/4/HD_A4_Large_Grid.jpg"       rel="AWG_A4A4PT_1"   next="submit">Large Grid</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A4_Perpetual_112x112.jpg"        zoom="wizard/calendar/600x400/4/HD_A4_Perpetual.jpg"        rel="AWP_A4A4PT_1"   next="submit">Perpetual</div>
                    </div>
                    <div class="choose" category="calendar_layout_CAL__A5L" label="Choose Style">
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A5_Full_Bleed_112x112.jpg"       zoom="wizard/calendar/600x400/4/HD_A5_Full_Bleed.jpg"       rel="AWF_A5A5LT_1"   next="submit">Full Bleed</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_A5_Compact_Grid_112x112.jpg"     zoom="wizard/calendar/600x400/4/HD_A5_Compact_Grid.jpg"     rel="AWN_A5A5LT_1"   next="submit">Compact Grid</div>
                    </div>
                    <div class="choose" category="calendar_layout_CAL__300550" label="Choose Style">
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_Skyscraper_Full_Bleed_112x112.jpg"       zoom="wizard/calendar/600x400/4/HD_Skyscraper_Full_Bleed.jpg"       rel="AWF_300550PT_1"   next="submit">Full Bleed</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_Skyscraper_Compact_Grid_112x112.jpg"     zoom="wizard/calendar/600x400/4/HD_Skyscraper_Compact_Grid.jpg"     rel="AWN_300550PT_1"   next="submit">Compact Grid</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_Skyscraper_Large_Grid_112x112.jpg"       zoom="wizard/calendar/600x400/4/HD_Skyscraper_Large_Grid.jpg"       rel="AWG_300550PT_1"   next="submit">Large Grid</div>
                        <div class="option"             icon="wizard/calendar/112x112/4/HD_Skyscraper_Perpetual_112x112.jpg"        zoom="wizard/calendar/600x400/4/HD_Skyscraper_Perpetual.jpg"        rel="AWP_300550PT_1"   next="submit">Perpetual</div>
                    </div>
                    
                <!-- CANVAS -->
                    <div class="choose" category="canvas_collection" label="Choose Orientation">
                        <div class="option"             icon="wizard/canvas/112x112/2/Orientation_Panorama_112x112.jpg"     zoom="wizard/canvas/600x400/2/Orientation_Panorama.jpg"     rel="PANORAMA"    next="canvas_panorama">Panorama</div>
                        <div class="option"             icon="wizard/canvas/112x112/2/Orientation_Landscape_112x112.jpg"    zoom="wizard/canvas/600x400/2/Orientation_Landscape.jpg"    rel="LANDSCAPE"   next="canvas_landscape">Landscape</div>
                        <div class="option"             icon="wizard/canvas/112x112/2/Orientation_Square_112x112.jpg"       zoom="wizard/canvas/600x400/2/Orientation_Square.jpg"       rel="SQUARE"      next="canvas_square">Square</div>
                        <div class="option"             icon="wizard/canvas/112x112/2/Orientation_Portrait_112x112.jpg"     zoom="wizard/canvas/600x400/2/Orientation_Portrait.jpg"     rel="PORTRAIT"    next="canvas_portrait">Portrait</div>
                    </div>                
                    <div class="choose" category="canvas_landscape" label="Choose Size">
                        <div class="option"             icon="wizard/canvas/112x112/3/Landscape_40x30_112x112.jpg"     zoom="wizard/canvas/600x400/3/Landscape_40x30.jpg"     rel="4030"   next="submit">40x30cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Landscape_53x40_112x112.jpg"     zoom="wizard/canvas/600x400/3/Landscape_53x40.jpg"     rel="5340"   next="submit">53x40cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Landscape_80x60_112x112.jpg"     zoom="wizard/canvas/600x400/3/Landscape_80x60.jpg"     rel="8060"   next="submit">80x60cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Landscape_100x75_112x112.jpg"    zoom="wizard/canvas/600x400/3/Landscape_100x75.jpg"    rel="10075"  next="submit">100x75cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Landscape_133x100_112x112.jpg"   zoom="wizard/canvas/600x400/3/Landscape_133x100.jpg"   rel="133100" next="submit">133x100cm</div>
                    </div>
                    <div class="choose" category="canvas_panorama" label="Choose Size">
                        <div class="option"             icon="wizard/canvas/112x112/3/Panorama_60x30_112x112.jpg"     zoom="wizard/canvas/600x400/3/Panorama_60x30.jpg"     rel="6030"   next="submit">60x30cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Panorama_80x40_112x112.jpg"     zoom="wizard/canvas/600x400/3/Panorama_80x40.jpg"     rel="8040"   next="submit">80x40cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Panorama_120x60_112x112.jpg"    zoom="wizard/canvas/600x400/3/Panorama_120x60.jpg"    rel="12060"  next="submit">120x60cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Panorama_150x75_112x112.jpg"    zoom="wizard/canvas/600x400/3/Panorama_150x75.jpg"    rel="15075"  next="submit">150x75cm</div>
                    </div>
                    <div class="choose" category="canvas_portrait" label="Choose Size">
                        <div class="option"             icon="wizard/canvas/112x112/3/Portrait_30x40_112x112.jpg"     zoom="wizard/canvas/600x400/3/Portrait_30x40.jpg"     rel="3040"   next="submit">30x40cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Portrait_40x53_112x112.jpg"     zoom="wizard/canvas/600x400/3/Portrait_40x53.jpg"     rel="4053"   next="submit">40x53cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Portrait_60x80_112x112.jpg"     zoom="wizard/canvas/600x400/3/Portrait_60x80.jpg"     rel="6080"   next="submit">60x80cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Portrait_75x100_112x112.jpg"    zoom="wizard/canvas/600x400/3/Portrait_75x100.jpg"    rel="75100"  next="submit">75x100cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Portrait_100x133_112x112.jpg"   zoom="wizard/canvas/600x400/3/Portrait_100x133.jpg"   rel="100133" next="submit">100x133cm</div>
                    </div>
                    <div class="choose" category="canvas_square" label="Choose Size">
                        <div class="option"             icon="wizard/canvas/112x112/3/Square_30x30_112x112.jpg"     zoom="wizard/canvas/600x400/3/Square_30x30.jpg"     rel="3030"   next="submit">30x30cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Square_40x40_112x112.jpg"     zoom="wizard/canvas/600x400/3/Square_40x40.jpg"     rel="4040"   next="submit">40x40cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Square_60x60_112x112.jpg"     zoom="wizard/canvas/600x400/3/Square_60x60.jpg"     rel="6060"   next="submit">60x60cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Square_75x75_112x112.jpg"     zoom="wizard/canvas/600x400/3/Square_75x75.jpg"     rel="7575"   next="submit">75x75cm</div>
                        <div class="option"             icon="wizard/canvas/112x112/3/Square_100x100_112x112.jpg"   zoom="wizard/canvas/600x400/3/Square_100x100.jpg"   rel="100100" next="submit">100x100cm</div>
                    </div>
                    
                <!-- POSTER -->
                    <div class="choose" category="poster_collection" label="Choose Orientation">
                        <div class="option"             icon="wizard/poster/112x112/2/Orientation_Panorama_112x112.jpg"     zoom="wizard/poster/600x400/2/Orientation_Panorama.jpg"     rel="PANORAMA"    next="poster_panorama">Panorama</div>
                        <div class="option"             icon="wizard/poster/112x112/2/Orientation_Landscape_112x112.jpg"    zoom="wizard/poster/600x400/2/Orientation_Landscape.jpg"    rel="LANDSCAPE"   next="poster_landscape">Landscape</div>
                        <div class="option"             icon="wizard/poster/112x112/2/Orientation_Square_112x112.jpg"       zoom="wizard/poster/600x400/2/Orientation_Square.jpg"       rel="SQUARE"      next="poster_square">Square</div>
                        <div class="option"             icon="wizard/poster/112x112/2/Orientation_Portrait_112x112.jpg"     zoom="wizard/poster/600x400/2/Orientation_Portrait.jpg"     rel="PORTRAIT"    next="poster_portrait">Portrait</div>
                    </div>                
                    <div class="choose" category="poster_landscape" label="Choose Size">
                        <div class="option"             icon="wizard/poster/112x112/3/Landscape_40x30_112x112.jpg"     zoom="wizard/poster/600x400/3/Landscape_40x30.jpg"     rel="4030"   next="submit">40x30cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Landscape_53x40_112x112.jpg"     zoom="wizard/poster/600x400/3/Landscape_53x40.jpg"     rel="5340"   next="submit">53x40cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Landscape_80x60_112x112.jpg"     zoom="wizard/poster/600x400/3/Landscape_80x60.jpg"     rel="8060"   next="submit">80x60cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Landscape_100x75_112x112.jpg"    zoom="wizard/poster/600x400/3/Landscape_100x75.jpg"    rel="10075"  next="submit">100x75cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Landscape_133x100_112x112.jpg"   zoom="wizard/poster/600x400/3/Landscape_133x100.jpg"   rel="133100" next="submit">133x100cm</div>
                    </div>
                    <div class="choose" category="poster_panorama" label="Choose Size">
                        <div class="option"             icon="wizard/poster/112x112/3/Panorama_60x30_112x112.jpg"     zoom="wizard/poster/600x400/3/Panorama_60x30.jpg"     rel="6030"   next="submit">60x30cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Panorama_80x40_112x112.jpg"     zoom="wizard/poster/600x400/3/Panorama_80x40.jpg"     rel="8040"   next="submit">80x40cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Panorama_120x60_112x112.jpg"    zoom="wizard/poster/600x400/3/Panorama_120x60.jpg"    rel="12060"  next="submit">120x60cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Panorama_150x75_112x112.jpg"    zoom="wizard/poster/600x400/3/Panorama_150x75.jpg"    rel="15075"  next="submit">150x75cm</div>
                    </div>
                    <div class="choose" category="poster_portrait" label="Choose Size">
                        <div class="option"             icon="wizard/poster/112x112/3/Portrait_30x40_112x112.jpg"     zoom="wizard/poster/600x400/3/Portrait_30x40.jpg"     rel="3040"   next="submit">30x40cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Portrait_40x53_112x112.jpg"     zoom="wizard/poster/600x400/3/Portrait_40x53.jpg"     rel="4053"   next="submit">40x53cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Portrait_60x80_112x112.jpg"     zoom="wizard/poster/600x400/3/Portrait_60x80.jpg"     rel="6080"   next="submit">60x80cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Portrait_75x100_112x112.jpg"    zoom="wizard/poster/600x400/3/Portrait_75x100.jpg"    rel="75100"  next="submit">75x100cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Portrait_100x133_112x112.jpg"   zoom="wizard/poster/600x400/3/Portrait_100x133.jpg"   rel="100133" next="submit">100x133cm</div>
                    </div>
                    <div class="choose" category="poster_square" label="Choose Size">
                        <div class="option"             icon="wizard/poster/112x112/3/Square_30x30_112x112.jpg"     zoom="wizard/poster/600x400/3/Square_30x30.jpg"     rel="3030"   next="submit">30x30cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Square_40x40_112x112.jpg"     zoom="wizard/poster/600x400/3/Square_40x40.jpg"     rel="4040"   next="submit">40x40cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Square_60x60_112x112.jpg"     zoom="wizard/poster/600x400/3/Square_60x60.jpg"     rel="6060"   next="submit">60x60cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Square_75x75_112x112.jpg"     zoom="wizard/poster/600x400/3/Square_75x75.jpg"     rel="7575"   next="submit">75x75cm</div>
                        <div class="option"             icon="wizard/poster/112x112/3/Square_100x100_112x112.jpg"   zoom="wizard/poster/600x400/3/Square_100x100.jpg"   rel="100100" next="submit">100x100cm</div>
                    </div>
                    
                <!-- DIARIES AND NOTEBOOKS -->
                    <div class="choose" category="diary_collection" label="Choose Type">
                        <div class="option"             icon="wizard/diary/112x112/2/Style_Business_112x112.jpg"        zoom="wizard/diary/600x400/2/Style_Business.jpg"       rel="BUSINESS"  next="diary_business_size">Business Diary</div>
                        <div class="option"             icon="wizard/diary/112x112/2/Style_School_112x112.jpg"          zoom="wizard/diary/600x400/2/Style_School.jpg"         rel="SCHOOL"    next="diary_school_size">School Diary</div>
                        <div class="option"             icon="wizard/diary/112x112/2/Style_CustomNotebook_112x112.jpg"  zoom="wizard/diary/600x400/2/Style_CustomNotebook.jpg" rel="NOTEBOOK"  next="diary_notebook_size">Notebook</div>
                    </div>                
                    <div class="choose" category="diary_business_size" label="Choose Size">
                        <div class="option"             icon="wizard/diary/112x112/3/Size_Business_A4_112x112.jpg"      zoom="wizard/diary/600x400/3/Size_Business_A4.jpg"  rel="A4"  next="submit">A4</div>
                        <div class="option"             icon="wizard/diary/112x112/3/Size_Business_A5_112x112.jpg"      zoom="wizard/diary/600x400/3/Size_Business_A5.jpg"  rel="A5"  next="submit">A5</div>
                    </div>                
                    <div class="choose" category="diary_notebook_size" label="Choose Size">
                        <div class="option"             icon="wizard/diary/112x112/3/Size_CustomNotebook_A4_112x112.jpg"      zoom="wizard/diary/600x400/3/Size_CustomNotebook_A4.jpg"  rel="A4"  next="submit">A4</div>
                        <div class="option"             icon="wizard/diary/112x112/3/Size_CustomNotebook_A5_112x112.jpg"      zoom="wizard/diary/600x400/3/Size_CustomNotebook_A5.jpg"  rel="A5"  next="submit">A5</div>
                    </div>                
                    <div class="choose" category="diary_school_size" label="Choose Size">
                        <div class="option"             icon="wizard/diary/112x112/3/Size_School_A4_112x112.jpg"      zoom="wizard/diary/600x400/3/Size_School_A4.jpg"  rel="A4"  next="submit">A4</div>
                        <div class="option"             icon="wizard/diary/112x112/3/Size_School_A5_112x112.jpg"      zoom="wizard/diary/600x400/3/Size_School_A5.jpg"  rel="A5"  next="submit">A5</div>
                    </div>     
                    
                <!-- PHOTOGIFTS -->
                    <div class="choose" category="photogift" label="Choose Type">
                        <div class="option"             icon="wizard/photogift/112x112/2/Product_HDPhotoFlip_112x112.jpg"               zoom="wizard/photogift/600x400/2/Product_HDPhotoFlip.jpg"               rel="DESKFLIP"          next="DESKFLIP">Photo Flip</div>
                        <div class="option twolines"    icon="wizard/photogift/112x112/2/Product_PhotocoverPresentationBox_112x112.jpg" zoom="wizard/photogift/600x400/2/Product_PhotocoverPresentationBox.jpg" rel="PRESBOX-PC"        next="PRESBOX__PC">Photocover<br>Presentation Box</div>
                        <div class="option twolines"    icon="wizard/photogift/112x112/2/Product_BlackPresentationBox_112x112.jpg"      zoom="wizard/photogift/600x400/2/Product_BlackPresentationBox.jpg"      rel="PRESBOX-MATERIAL"  next="PRESBOX__MATERIAL">Black<br>Presentation Box</div>
                        <!-- <div class="option twolines"    icon="wizard/photogift/112x112/2/Product_DustJacket_112x112.jpg"                zoom="wizard/photogift/600x400/2/Product_DustJacket.jpg"                rel="AW-DUSTJACKETS"    next="AW__DUSTJACKETS">Standalone<br>Dustjacket</div> -->
                    </div>                
                    <div class="choose" category="PRESBOX__MATERIAL" label="Choose Size">
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Square_15x15_112x112.jpg"     zoom="wizard/photogift/600x400/3/BlackPresBox_Square_15x15.jpg"  rel="1515S"  next="submit">15x15cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Landscape_A5_112x112.jpg"     zoom="wizard/photogift/600x400/3/BlackPresBox_Landscape_A5.jpg"  rel="A5A5L"  next="submit">A5 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Portrait_A5_112x112.jpg"      zoom="wizard/photogift/600x400/3/BlackPresBox_Portrait_A5.jpg"   rel="A5A5P"  next="submit">A5 Portrait</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Square_21x21_112x112.jpg"     zoom="wizard/photogift/600x400/3/BlackPresBox_Square_21x21.jpg"  rel="2121S"  next="submit">21x21cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Landscape_A4_112x112.jpg"     zoom="wizard/photogift/600x400/3/BlackPresBox_Landscape_A4.jpg"  rel="A4A4L"  next="submit">A4 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Portrait_A4_112x112.jpg"      zoom="wizard/photogift/600x400/3/BlackPresBox_Portrait_A4.jpg"   rel="A4A4P"  next="submit">A4 Portrait</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Square_30x30_112x112.jpg"     zoom="wizard/photogift/600x400/3/BlackPresBox_Square_30x30.jpg"  rel="3030S"  next="submit">30x30cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Landscape_A3_112x112.jpg"     zoom="wizard/photogift/600x400/3/BlackPresBox_Landscape_A3.jpg"  rel="A3A3L"  next="submit">A3 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/BlackPresBox_Portrait_A3_112x112.jpg"      zoom="wizard/photogift/600x400/3/BlackPresBox_Portrait_A3.jpg"   rel="A3A3P"  next="submit">A3 Portrait</div>
                    </div>                     
                    <div class="choose" category="PRESBOX__PC" label="Choose Size">
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Square_15x15_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Square_15x15.jpg"  rel="1515S"  next="submit">15x15cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Landscape_A5_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Landscape_A5.jpg"  rel="A5A5L"  next="submit">A5 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Portrait_A5_112x112.jpg"      zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Portrait_A5.jpg"   rel="A5A5P"  next="submit">A5 Portrait</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Square_21x21_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Square_21x21.jpg"  rel="2121S"  next="submit">21x21cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Landscape_A4_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Landscape_A4.jpg"  rel="A4A4L"  next="submit">A4 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Portrait_A4_112x112.jpg"      zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Portrait_A4.jpg"   rel="A4A4P"  next="submit">A4 Portrait</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Square_30x30_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Square_30x30.jpg"  rel="3030S"  next="submit">30x30cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Landscape_A3_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Landscape_A3.jpg"  rel="A3A3L"  next="submit">A3 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotocoverPresBox_Portrait_A3_112x112.jpg"      zoom="wizard/photogift/600x400/3/PhotocoverPresBox_Portrait_A3.jpg"   rel="A3A3P"  next="submit">A3 Portrait</div>
                    </div>                     
                    <div class="choose" category="AW__DUSTJACKETS" label="Choose Size">
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Square_15x15_112x112.jpg"     zoom="wizard/photogift/600x400/3/DustJacket_Square_15x15.jpg"  rel="1515S"  next="submit">15x15cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Landscape_A5_112x112.jpg"     zoom="wizard/photogift/600x400/3/DustJacket_Landscape_A5.jpg"  rel="A5A5L"  next="submit">A5 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Portrait_A5_112x112.jpg"      zoom="wizard/photogift/600x400/3/DustJacket_Portrait_A5.jpg"   rel="A5A5P"  next="submit">A5 Portrait</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Square_21x21_112x112.jpg"     zoom="wizard/photogift/600x400/3/DustJacket_Square_21x21.jpg"  rel="2121S"  next="submit">21x21cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Landscape_A4_112x112.jpg"     zoom="wizard/photogift/600x400/3/DustJacket_Landscape_A4.jpg"  rel="A4A4L"  next="submit">A4 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Portrait_A4_112x112.jpg"      zoom="wizard/photogift/600x400/3/DustJacket_Portrait_A4.jpg"   rel="A4A4P"  next="submit">A4 Portrait</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Square_30x30_112x112.jpg"     zoom="wizard/photogift/600x400/3/DustJacket_Square_30x30.jpg"  rel="3030S"  next="submit">30x30cm Square</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Landscape_A3_112x112.jpg"     zoom="wizard/photogift/600x400/3/DustJacket_Landscape_A3.jpg"  rel="A3A3L"  next="submit">A3 Landscape</div>
                        <div class="option" icon="wizard/photogift/112x112/3/DustJacket_Portrait_A3_112x112.jpg"      zoom="wizard/photogift/600x400/3/DustJacket_Portrait_A3.jpg"   rel="A3A3P"  next="submit">A3 Portrait</div>
                    </div>                     
                    <div class="choose" category="DESKFLIP" label="Choose Size">
                        <div class="option" icon="wizard/photogift/112x112/3/PhotoFlip_Large_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotoFlip_Large.jpg"  rel="AWHD_190280GDF_1"  next="submit">Large Glossy Photo Flip</div>
                        <div class="option" icon="wizard/photogift/112x112/3/PhotoFlip_Small_112x112.jpg"     zoom="wizard/photogift/600x400/3/PhotoFlip_Small.jpg"  rel="AWHD_A5A5GDF_1"  next="submit">Small Glossy Photo Flip</div>
                    </div>                
                             
            </div>
            <div id="byline">This web editor is perfect for small projects. Do you want to create a big book with lots of photos?<br>Try our <a href="../?download">downloadable editor for Mac or Windows</a>.</div>
        </div>
    </div>
    
    <div id="loadingtheatre"> </div>
    
    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="js/common.js?<?=time()?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var category = 'product';
            <?
                if(isset($_GET['options'])){
                    $options = explode('/', $_GET['options']);
                    foreach($options as $getopt){
                        if($getopt){
                            ?> 
                                var $option = $('#options .choose[category='+category+'] .option[rel=<?=$getopt?>]');
                                var choicelabel = $option.html().replace('<br>', ' ');
                                
                                var choiceobject = new Object();
                                eval('choiceobject.'+category+' = "<?=$getopt?>";');
                                __choices.push(choiceobject);
                                
                                add_breadcrumb(choicelabel, category);
                                
                                category = $option.attr('next');
                            <?
                        }
                    }
                    ?> activate_choice(category);  <?
                }
                else{
                    ?> activate_choice('product'); <?
                }
            ?>    
        });
    </script>
    <script type="text/javascript">    
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-1338422-2']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>    

    <?
        $_GET['conversion'] = isset($_GET['conversion']) ? $_GET['conversion'] : '';
        switch($_GET['conversion']){
            case 'fb':
                ?>
                    <script type="text/javascript">
                        var fb_param = {};
                        fb_param.pixel_id = '6010440259726';
                        fb_param.value = '0.00';
                        fb_param.currency = 'AUD';
                    </script>
                    <script type="text/javascript" async="true" src="//connect.facebook.net/en_US/fp.js"></script>
                    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6010440259726&amp;value=0&amp;currency=AUD" /></noscript>
                <?
                break;
            
            case 'ggl':
                ?>
                    <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion_async.js"></script>
                    <script type="text/javascript">
                        goog_snippet_vars = function() {
                            var w = window;
                            w.google_conversion_id = 1060947230;
                            w.google_conversion_label = "6VmdCIKu_wYQnorz-QM";
                            w.google_conversion_value = 5;
                            w.google_remarketing_only = false;
                        }
                          // DO NOT CHANGE THE CODE BELOW.
                        goog_report_conversion = function() {
                            goog_snippet_vars();
                            window.google_conversion_format = "3";
                            window.google_is_call = true;
                            var opt = new Object();
                            opt.onload_callback = function() {
                            if (typeof(url) != 'undefined') {
                              //window.location = url;
                            }
                          }
                          var conv_handler = window['google_trackConversion'];
                          if (typeof(conv_handler) == 'function') {
                            conv_handler(opt);
                          }
                        }
                        goog_report_conversion();
                    </script>
                <?
                break;
        }
    ?>
</body>
</html>
