<!DOCTYPE html>
<html>
    <head>
    
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create any Photo Book and get 30% off! | albumworks</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="<?=getenv('BASEPATH')?>statics/landing-feb17f/common.css?<?=time()?>" />

        <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5220373"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5220373&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '742cc0afc69d8afb67a6ac020f80347eee46766e');
        </script>        
    </head>
    <body>
                   
    <div id="intro" class="panel">
        <? if(time() < mktime(0,0,0,12,20,2017)): ?>
            <header>
                <section id="promostrip" class="group">
                    <div id="promostripcontents" class="group">
                        <p class="heading">CHRISTMAS ORDER DEADLINES</p>
                        <div class="promoitem group">
                            <img src="https://www.albumworks.com.au/img/promo/xmas2017/16dec.png" alt="16 December" />
                            <p class="nomobile">Products delivered before Christmas Day to <strong>metropolitan</strong> areas*<br/>Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                            <p class="mobileonlyblock">Products delivered before Christmas Day to <strong>metropolitan</strong> areas* Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                        </div>
                        <div class="promoitem group">
                            <img src="https://www.albumworks.com.au/img/promo/xmas2017/14dec.png" alt="14 December" />
                            <p class="nomobile">Products delivered before Christmas Day to <strong>rural and remote</strong> areas*<br/>Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                            <p class="mobileonlyblock">Products delivered before Christmas Day to <strong>rural and remote</strong> areas* Order and uploaded by 11:59PM. <span>*Issues beyond our control may cause delay</span></p>
                        </div>
                    </div>
                </section>
            </header>
        <? else: ?>
            <div id="header">
                <div class="contain">
                    <a class="logo" target="_blank" href="https://www.albumworks.com.au/">
                        <img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/nav.png" />
                    </a>
                    <div id="maincta" class="nomobile">
                        <a class="cta" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <h1><strong>Stunning Photo Paper Options.</strong></h1>
        <p class="subtext">30% OFF WITH YOUR FIRST ORDER</p>
        <p class="centered"><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
        <div class="bottom">
            <div id="testimonials" class="contain">
                <img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/customer1.png" id="testimonial1" />
                <img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/customer2.png" id="testimonial2" />
                <div class="clr"> </div>
            </div>
            <h2>Welcome to <em>albumworks</em></h2>
            <p class="fancy">We're the experts in Photo Books. But don't just take our word for it, find out why below!</p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(1);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/down.png" /></a></p>
            <p class="onlymobile"><a class="down" href="#panel1"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/down.png" /></a></p>
        </div>
    </div>

    <div id="panel1" class="panel">
        <div class="content">
            <h3>#1</h3>
            <p class="subtext">FREE DESKTOP SOFTWARE<br>EDITOR - SIMPLE TO USE</p>
            <p>Design your Photo Book exactly how YOU want it. Choose from our pre-designed themes, or start from scratch with our easy-to-use desktop Editor. Work offline in your own time and upload photos in seconds to start creating instantly!</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(2);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/down.png" /></a></p>
        </div>
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-feb17f/cropped-computer.gif" /></p>
    </div>
  
    <div id="panel2" class="panel">
        <div class="content">
            <h3>#2</h3>
            <p class="subtext">FRIENDLY, LOCAL<br>CUSTOMER SERVICE</p>
            <p>Our amazing and friendly Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible. We're even here till 6PM on weekdays.</p>
            <p class="nomobile"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/cs-01.png" /></p>
            <p class="nomobile"><a target="_blank" href="http://www.albumworks.com.au/contact"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/cs-02.png" /></a></p>
            <p class="nomobile"><a target="_blank" href="http://www.albumworks.com.au/contact"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/cs-03.png" /></a></p>
            <p class="nomobile"><a href="javascript:void(0)" onclick="Tawk_API.showWidget(); Tawk_API.maximize();"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/cs-04.png" /></a></p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(3);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/down.png" /></a></p>
        </div>        
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-feb17f/cropped-service.jpg" /></p>
    </div>
    
    <div id="panel3" class="panel">
        <div class="content">
            <h3>#3</h3>
            <p class="subtext">GET YOUR ORDER WITHIN<br>7 BUSINESS DAYS</p>
            <p>Hate waiting? With more than 98% of all orders being dispatched within 4 business days, you'll never have to wait again!</p>
            <p class="centered"><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <div class="vidholder">
                <video width="100%" loop autoplay muted preload>
                    <source src="<?=getenv('BASEPATH')?>statics/landing-feb17f/book-vid.mp4" type="video/mp4">
                    <img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/screencap.jpg" width="100%" height="100%" />
                </video>                
            </div>
        </div>        
        <p class="bottom centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(4);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/down.png" /></a></p>
    </div>
 
    <div id="panel4" class="panel">
        <div class="content">
            <h3>#4</h3>
            <p class="subtext">MORE CHOICE: OVER 500<br>PHOTO BOOK COMBINATIONS</p>
            <p>We've expanded from 97 to 562 Photo Book cover and size combinations! Choose from 3 different book binding methods, including Classic PUR binding and Layflat binding. From fully customised Photocovers to elegant Premium Material Covers - you'll be spoilt for choice!</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="$.scrollify.move(5);"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/down.png" /></a></p>
        </div>        
        <img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/books.png" id="bookfloater" class="floater" />
    </div>    
             
    <div id="panel5" class="panel">
        <div class="content">
            <h3>#5</h3>
            <p class="subtext">STUNNING BOOK QUALITY</p>
            <p>We are passionate about printing top quality Photo Books. All our books are printed to international colour standards. Optionally upgrade to High Definition - stunning full photographic printing that is unmatched in Australia.</p>
            <p><a class="cta fit" href="javascript:void(0)" onclick="go_to_footer()">GET STARTED NOW</a></p>
            <p class="centered nomobile"><a class="down" href="javascript:void(0)" onclick="go_to_footer()"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/down.png" /></a></p>
        </div>        
        <p class="onlymobile"><img class="sizeme" src="<?=getenv('BASEPATH')?>statics/landing-feb17f/cropped-hd.jpg" /></p>
    </div>    
    
    <div id="footer" name="footer" class="panel">
        <div class="content">
            <h1><strong>30% OFF</strong> ANY PRODUCT!</h1>
            <p class="subtext">FOR FIRST TIME ORDERS ONLY</p>
            <p class="fancy">Download our free Editor to get started. Your 30% off Voucher Code will be emailed to you!</p>
            <form class="box" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&landing-feb17f&referringPromotion=THIRTYOFF" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                        <input type="hidden" value="Download" name="00N3600000BOyGd">
                        <input type="hidden" value="AP" name="00N3600000BOyAt">
                        <input type="hidden" value="PG" name="00N3600000Loh5K">
                        <input type="hidden" name="00N3600000Los6F" value="Windows">
                        <input type="hidden" value="THIRTYOFF" name="00N3600000LosAC">
                        <input type="hidden" value="1" name="00N3600000LosKl">
                        <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                        
                        <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                        <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                        <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                        <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                        <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                        <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                        <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                        <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                        <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
                
                <input type="text" placeholder="NAME" name="first_name" class="firstname smartedit inputbox" alt="NAME">
                <input type="text" placeholder="EMAIL" name="email" class="email smartedit inputbox" alt="EMAIL">
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET OUR FREE EDITOR</a>
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
                    <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
                </div>
                <div class="clr"> </div>
            </form>  
            <p id="botlog"><a href="http://www.albumworks.com.au"><img src="<?=getenv('BASEPATH')?>statics/landing-feb17f/logo-bottom.jpg" width="246" height="56" alt="albumworks" /></a></p>
        </div>
    </div>
                   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=getenv('BASEPATH')?>statics/landing-feb17f/common.js"></script>
    
    
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1338422-2']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        var host = window.location.host;
    </script>

                

        <div style="position:fixed">
            <script type="text/javascript">
                var current_page = 'landing-feb17f';
                var BASEPATH = 'https://www.albumworks.com.au/';
                //var BASEPATH = host;



                <? if(isset($_GET['alertmsg'])): ?>
                window.setTimeout(function(){
                    alert("<?=addslashes($_GET['alertmsg'])?>");
                }, 1000);
                <? elseif(isset($_GET['download'])): ?>
                $(document).ready(function(){
                    launch_download_lightbox();
                })
                <? endif; ?>
            </script>

            <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="https://www.albumworks.com.au/css/ie.css" />
            <script type="text/javascript" src="https://www.albumworks.com.au/js/selectivizr-min.js"></script>
            <script type="text/javascript" src="https://www.albumworks.com.au/js/css3-mediaqueries.js"></script>
            <![endif]-->
            <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js#username=albumprinter"></script>

            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1060947230;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1060947230/?guid=ON&amp;script=0"/>
            </div>
            </noscript>            
            
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"> </script>
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-1338422-2']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
            <script type="text/javascript">var _kiq = _kiq || [];</script>
            <script type="text/javascript" src="//s3.amazonaws.com/ki.js/20502/40J.js" async="true"></script>
            <script type="text/javascript">
                var _ga = new Object;
                _ga.campaign = '';
                _ga.term = '';
                _ga.search= '';
                _ga.referrer = '';
                _ga.group = '';
                _ga.keywords = '';

                var gc = '';
                var c_name = "__utmz";
                if (document.cookie.length>0){
                    c_start=document.cookie.indexOf(c_name + "=");
                    if (c_start!=-1){
                        c_start=c_start + c_name.length+1;
                        c_end=document.cookie.indexOf(";",c_start);
                        if (c_end==-1) c_end=document.cookie.length;
                        gc = unescape(document.cookie.substring(c_start,c_end));
                    }
                }

                if(gc != ""){
                    var z = gc.split('.');
                    if(z.length >= 4){
                        var y = z[4].split('|');
                        for(i=0; i<y.length; i++){
                            if(y[i].indexOf('utmccn=') >= 0) _ga.campaign = y[i].substring(y[i].indexOf('=')+1);
                            if(y[i].indexOf('utmctr=') >= 0) _ga.term     = y[i].substring(y[i].indexOf('=')+1);
                        }
                    }
                }


                <?
                    function cleanit($string){
                        //return preg_replace("/[^A-Za-z0-9]/", '', strtolower($string));
                        return trim(strtolower($string));
                    }

                    if(strpos($_SERVER['REQUEST_URI'], '?') !== FALSE){
                        $querystring = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?')+1);
                        $queryparts = explode('&', $querystring);
                        foreach($queryparts as $part){
                            list($key, $val) = explode('=', $part);
                            $_GET[$key] = urldecode($val);
                        }
                        $_GET['q'] = $_SERVER['REQUEST_URI'];
                    }

                    if(isset($_SERVER['HTTP_REFERER'])){
                        $parts = parse_url($_SERVER['HTTP_REFERER']);
                        $_SERVER['HTTP_REFERER'] = $parts['host'];
                    }

                    if(isset($_GET['campaign'])) $_GET['campaign'] = cleanit($_GET['campaign']);
                    if(isset($_GET['group'])) $_GET['group'] = cleanit($_GET['group']);
                ?>


                <? if(isset($_GET['q'])): ?> _ga.search = "<?=addslashes($_GET['q'])?>"; <? endif; ?>

                <? if(isset($_GET['_kk'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kk']))?>"; <? endif; ?>
                <? if(isset($_GET['_kw'])): ?> _ga.keywords = "<?=addslashes(trim($_GET['_kw']))?>"; <? endif; ?>

                <? if(isset($_GET['group'])): ?> _ga.group = "<?=addslashes(cleanit($_GET['group']))?>"; <? endif; ?>
                <? if(isset($_GET['campaign'])): ?> _ga.campaign = "<?=addslashes(($_GET['campaign'] == "notset") ? "" : cleanit($_GET['campaign']))?>"; <? endif; ?>
                <? if(isset($_GET['network'])): ?> _ga.network = "<?=((($_GET['network'] == 'g') or ($_GET['network'] == 's')) ? 'Search' : 'Content')?>"; <? endif; ?>

                <? if(isset($_SERVER['HTTP_REFERER'])): ?> _ga.referrer = "<?=addslashes($_SERVER['HTTP_REFERER'])?>"; <? endif; ?>
                <? if(isset($_GET['placement'])): ?> _ga.referrer = "<?=addslashes($_GET['placement'])?>"; <? endif; ?>
                <? if(isset($_GET['_placement'])): ?> _ga.referrer = "<?=addslashes($_GET['_placement'])?>"; <? endif; ?>

                <? if(isset($_GET['target'])): ?> _ga.target = "<?=addslashes($_GET['target'])?>"; <? endif; ?>
                <? if(isset($_GET['position'])): ?> _ga.position = "<?=addslashes($_GET['position'])?>"; <? endif; ?>
                <? if(isset($_GET['match'])): ?> _ga.match = "<?=addslashes($_GET['match'])?>"; <? endif; ?>

                <? if(isset($_GET['gclid'])): ?>
                createCookie('gclid','<?=addslashes($_GET['gclid'])?>',90);
                <? endif; ?>
                <? if(isset($_GET['msclkid'])): ?>
                createCookie('gclid','bing<?=addslashes($_GET['msclkid'])?>',90);
                <? endif; ?>

                jQuery('input[name=00N3600000BOyGd]').each(function(){
                    var $form = $(this).closest('form');
                    if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
                        $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
                    }
                });

            </script>
            
            <!--Start of Tawk.to Script-->
            <script type="text/javascript">var Tawk_API=Tawk_API||{},Tawk_LoadStart=new Date();(function(){var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true;s1.src='https://embed.tawk.to/588ff051af9fa11e7aa51d60/default';s1.charset='UTF-8';s1.setAttribute('crossorigin','*');s0.parentNode.insertBefore(s1,s0);})();</script>
            <!--End of Tawk.to Script-->                  
            
<!--  MouseStats:Begin  -->
<script type="text/javascript">var MouseStats_Commands=MouseStats_Commands?MouseStats_Commands:[]; (function(){function b(){if(void 0==document.getElementById("__mstrkscpt")){var a=document.createElement("script");a.type="text/javascript";a.id="__mstrkscpt";a.src=("https:"==document.location.protocol?"https://ssl":"http://www2")+".mousestats.com/js/5/2/5298657782534596732.js?"+Math.floor((new Date).getTime()/6E5);a.async=!0;a.defer=!0;(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(a)}}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load", b,!1);"complete"===document.readyState&&b()})(); </script>
<!--  MouseStats:End  -->
            
        </div>
    </body>
</html>