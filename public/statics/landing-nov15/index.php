<? include('header.php'); ?>
    <img class="item" src="<?=getenv('BASEPATH')?>statics/landing-nov15/new-head.jpg" />
    <div class="callout">
        <p><strong>If this is your first order with <em>albumworks</em>, welcome!</strong></p>
        <p>Sign up today for FREE, and get 30% off your first order</p>
    </div>

    <p style="text-align: center;"><img style="width:100%; max-width:862px" src="<?=getenv('BASEPATH')?>statics/landing-nov15/join.jpg" /></p>
    
    <h3>No sign up fee, no obligation, just a community of more than 100,000 people who love photo books!</h3>
    
    <form class="box" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
        <a name="join"></a>
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&landing-may15&referringPromotion=MAY15OFFER" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="MAY15OFFER" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
        
        <!-- Actual fields -->
        <input type="text" placeholder="NAME" name="first_name" class="firstname smartedit inputbox" alt="NAME">
        <input type="text" placeholder="EMAIL" name="email" class="email smartedit inputbox" alt="EMAIL">
        <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">JOIN NOW<span>and get our free editor</span></a>
        <div class="checkarea">
            <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
            <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
        </div>
        <div class="clr"> </div>
    </form>
    
    <h3 style="font-size:14px; font-style:italic">Your 30% off voucher code will be emailed to you upon download.</h3>
    
    <hr />
    
    <div class="clr" style="text-align: center;">
        <h3>Why choose <em>albumworks</em>:</h3>
        <div class="blocks">
            <img src="<?=getenv('BASEPATH')?>statics/landing-nov15/adwords-02.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-nov15/adwords-03.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-nov15/cs-tile.jpg" />
            <img src="<?=getenv('BASEPATH')?>statics/landing-nov15/adwords-05.jpg" />
            <div class="clr"></div>
        </div>
        
        <h3 style="margin-top:20px">Upload photos from:</h3>
        <p><img src="<?=getenv('BASEPATH')?>statics/landing-nov15/13.jpg" style="width:100%; max-width:593px;" /></p>
        <p class="smaller">*Available only on online Editor</p>
    </div>
    
    

    
<? include('footer.php'); ?>
