<?php


    function die_num($value){
        die(''.$value);
    }

    /* ////////////// EXAMPLE ////////////////////////////    
        Array
        (
            [promocode] => asdda
            [prodcode] => MLC1185
            [papercode] => WW_STANDARD01
            [covertype] => Photocover
            [prodtype] => 1185,
            [pagecount] => 40
            [slipcase] => false
            [slipcasehs] => false
            [presbox] => false
            [presboxhs] => false
            [hotstamping] => false
            [guarantee] => false
            [subtotal] => 69.95
            [quantity] => 1
            [shipping] => 11.95
            [total] => 81.90
            [thisprod] => Array
                (
                    [prodcode] => MLC1185
                    [papercode] => WW_STANDARD01
                    [included] => 40
                    [min] => 30
                    [max] => 200
                    [baseprice] => 69.95
                    [extrapageprice] => 1.1
                    [scprice] => 29.95
                    [schsprice] => 14.95
                    [pbprice] => 60
                    [pbhsprice] => 14.95
                    [hsprice] => 
                    [shippingfirstprice] => 11.95
                    [shippingaddtlprice] => 5
                )

        )
    ====================================================== */

    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    // clean up the input before working
    foreach($_POST as $key => $val){
        if($val === 'true')
            $_POST[$key] = true;
        if($val === 'false')
            $_POST[$key] = false;        
    }
    
    switch(strtoupper($_POST['promocode'])){
        // DBNO campaign    33% discount any product
        case '33DISCOUNT':
            if(time() < mktime(0,0,0,3,12,2020))
                die_num($_POST['total'] * 0.33);
            break;

        case '2FOR1PHOTOBOOK':
            die_num(($_POST['thisprod']['baseprice'] + ($_POST['thisprod']['extrapageprice'] * max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])))) * floor($_POST['quantity']/2));
            break;

        case 'SILVER4T22WL':
        case 'GOLDVS6KW2J7':
        case 'XTRAPAGES':
            if($_POST['thisprod']['papercode'] == 'GP-250RD' or $_POST['thisprod']['papercode'] == 'DLLSTR')
                $free_pages = 40;
            else if(strpos($_POST['thisprod']['prodcode'], 'LF-') !== FALSE)
                $free_pages = 50;
            else $free_pages = 30;
            $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            $pricewithfree = max(0, ($_POST['pagecount'] - $free_pages - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            die_num(max(0, $currentpaperprice - $pricewithfree));
            break;

        // MUM2017 - 40 extra pages free all photo books
        case 'MUM2017':
        case 'SUMMERPAGES':
        case 'SUMMER40':
        case '13PAGES':
            $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            $pricewithfree = max(0, ($_POST['pagecount'] - 40 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            die_num(max(0, $currentpaperprice - $pricewithfree));
            break;

        case '34EXTRAPAGES':
        case '34EXTRAPAGESEXT':
        case 'GOLDB44MRP7Y':
        case 'SILVERCR62A8':
        case 'DAD34PAGES':
            $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            $pricewithfree = max(0, ($_POST['pagecount'] - 34 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            die_num(max(0, $currentpaperprice - $pricewithfree));
            break;

        // spend and save
        case '2019CHRISTMAS':
        case 'GOLDFPL769SW':
        case 'SILVERGPPGL4':
        case 'EXT2019CHRISTMAS':
            if(($_POST['subtotal']*$_POST['quantity']) < 160)
                die_num($_POST['subtotal']*$_POST['quantity']*0.32);
            die_num($_POST['subtotal']*$_POST['quantity']*0.42);

        case 'SUPER30':
            if((strpos($_POST['thisprod']['prodcode'], '1185') !== FALSE))
                die_num($_POST['subtotal']*$_POST['quantity']*0.3);
            break;

        case 'NEWYEAR30':
        case '30NEWYEAR':
        case '30NEWYEAREXT':
        case 'SILVERP26KCK':
        case 'GOLDSM34KRR7':
            die_num($_POST['subtotal']*$_POST['quantity']*0.3);
            break;

        case 'DAD2019':
        case 'BIGBIRTHDAY':
            die_num($_POST['subtotal']*$_POST['quantity']*0.38);
            break;

        // 30 free extra pages
        case 'XMASPAGES':
            if((strpos($_POST['thisprod']['prodcode'], '88') !== FALSE) or (strpos($_POST['thisprod']['prodcode'], '1212') !== FALSE)) {
                $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
                $pricewithfree = max(0, ($_POST['pagecount'] - 30 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
                die_num(max(0, $currentpaperprice - $pricewithfree));
            }
            break;

        // 32 free extra pages including flushes
        case 'EARLYBIRD2018':
            $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            $pricewithfree = max(0, ($_POST['pagecount'] - 32 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            die_num(max(0, $currentpaperprice - $pricewithfree));
            break;

        // $5 for 30 FEP SD Classic only
        case 'PAGESFIVER':
            if(in_array($_POST['thisprod']['papercode'], ['WW_STANDARD01', 'WW_STANDARD03'])){
                $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
                $pricewithfree = max(0, ($_POST['pagecount'] - 30 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
                die_num(max(0, ($currentpaperprice - $pricewithfree) - 5));
            }
            break;

        // 40 fep
        case 'SUPER40':
        case 'SUPER40EXT':
        case 'SILVER652WFC':
        case 'GOLD5ZK9MCVR':
        case 'EARLYBIRDB':
        case 'GOLD79T59Z8R':
        case 'SILVERWJK2K8':
        case 'EARLYBIRDBEXT':
            $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            $pricewithfree = max(0, ($_POST['pagecount'] - 40 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            die_num(max(0, ($currentpaperprice - $pricewithfree)));
            break;

        // spend more save more
        case 'FEBSAVE':
        case 'GOLDH29SSHCL':
        case 'SILVER2W2NDY':
        case 'FEBSAVEEXT':
        case 'DADLOVE':
        case 'DADLOVEEXT':
        case 'SILVERG56W2P':
        case 'GOLD6TSB8KL4':
        case 'SMSM':
        case 'MUM2021':
        case 'EARLYBIRD2021':
        case 'EARLYBIRD2021EXT':
        case 'GOLDWHN5FTZ6':
        case 'SILVERXT6VKL':
        case 'SILVERT4JA2Y':
        case 'GOLDCA6PB7Z9':
        case '16BIRTHDAYEXT':
        case 'SUMMER2023':
        case 'SUMMER2023EXT':
        case '17BIRTHDAY':
        case '17BIRTHDAYEXT':
        case 'GOLD9A8TW6':
        case 'SILVER9A8TW6':
        case 'GOLD65L7P2':
        case 'SILVER2A9B44':
        case 'EARLYBIRD2022':
        case 'EARLYBIRD2022EXT':
        case 'GOLD7LA2WC4P':
        case 'SILVER83TY4Q':
        case '16BIRTHDAY':
        case 'SUMMER2022':
        case 'SUMMER2022EXT':
        case 'GOLDTC3TR58X':
        case 'SILVERGZKY7F':
        case 'BESTTIMES':
        case 'BESTTIMESEXT':
        case 'GOLD2Z9NT622':
        case 'SILVERD4M3VY':
        case 'MUM2021EXT':
        case 'GOLDHRKBCBM4':
        case 'SILVERZZ23XS':
        case 'SUMMER2021':
        case 'SUMMER2021EXT':
        case 'GOLDSYV542JL':
        case 'SILVER52XBW8':
        case 'SMSMEXT':
        case 'GOLDRP54WBRR':
        case 'SILVER3C9FXF':
        case 'SAVINGSMAY':
        case 'SAVINGSMAYEXT':
        case 'GOLDYZKV9MGL':
        case 'SILVERZK5XTV':
            if(($_POST['subtotal']*$_POST['quantity']) < 50)
                die_num(0);
            if(($_POST['subtotal']*$_POST['quantity']) < 100)
                die_num(($_POST['subtotal']*$_POST['quantity']) * 0.25);
            if(($_POST['subtotal']*$_POST['quantity']) < 150)
                die_num(($_POST['subtotal']*$_POST['quantity']) * 0.33);
            if(($_POST['subtotal']*$_POST['quantity']) >= 150)
                die_num(($_POST['subtotal']*$_POST['quantity']) * 0.40);
            break;

        case 'GOBIGGER':
        case 'GOBIGGEREXT':
        case 'GOLD7S4PA2':
        case 'SILVER7S4PA2':
        case 'BONUSTREAT':
        case 'BONUSTREATEXT':
        case 'GOLDVC2SW6W2':
        case 'SILVERYG4HFG':
        case 'BIGBONUS':
        case 'BIGBONUSEXT':
        case 'GOLDM77YJ53L':
        case 'SILVERVTSK8Z':
            if(($_POST['subtotal']*$_POST['quantity']) < 150)
                die_num(($_POST['subtotal']*$_POST['quantity']) * 0.33);
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.4);
            break;

        case '2020CHRISTMAS':
        case '2020CHRISTMASEXT':
        case 'GOLDF6NGL386':
        case 'SILVER4JN4JT':
        case 'BONUSDISCOUNT':
        case 'BONUSDISCOUNTEXT':
        case 'GOLD2KCR3476':
        case 'SILVERX6TXSW':
            if(($_POST['subtotal']*$_POST['quantity']) < 160)
                die_num(($_POST['subtotal']*$_POST['quantity']) * 0.34);
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.42);
            break;

        // spend more save more
        case 'MYSAVINGS':
            if(($_POST['subtotal']*$_POST['quantity']) < 50)
                die_num(0);
            if(($_POST['subtotal']*$_POST['quantity']) < 100)
                die_num($_POST['subtotal']*$_POST['quantity'] * 0.26);
            if(($_POST['subtotal']*$_POST['quantity']) < 175)
                die_num($_POST['subtotal']*$_POST['quantity'] * 0.36);
            if(($_POST['subtotal']*$_POST['quantity']) >= 175)
                die_num($_POST['subtotal']*$_POST['quantity'] * 0.44);
            break;

        // 30% off mobile books (not including shipping)
        case 'EASYBOOKS':
            if(strpos($_POST['thisprod']['prodcode'], 'F_') === 0)
                die_num($_POST['subtotal']*$_POST['quantity']*0.3);
            break;

        // 36% off product only, whole range
        case 'MYMEMORIES':
        case 'MYMEMORIESEXT':
        case 'GOLDP9M9M79S':
        case 'SILVER54P6TF':
            die_num($_POST['subtotal']*$_POST['quantity']*0.36);

        // 41% off product only, whole range
        case 'CHATWITHME':
            die_num($_POST['subtotal']*$_POST['quantity']*0.41);

        // 34% off product only, whole range
        case '34OFF':
        case 'THIRTYFOUR':
            die_num($_POST['subtotal']*$_POST['quantity']*0.34);

        // 32% off product only, whole range
        case 'XMASDISCOUNT':
            die_num($_POST['subtotal']*$_POST['quantity']*0.32);

        // 35% off product only, whole range
        case 'EOYSALE':
        case 'MUM2019':
        case '13DISCOUNT':
        case 'FAMILYBOOK':
        case '35BOOKS':
        case 'MYSTORY':
            die_num($_POST['subtotal']*$_POST['quantity']*0.35);

        case 'EARLYBIRDA':
        case 'GOLDVX3WS5C6':
        case 'SILVERK3G6VP':
        case 'EARLYBIRDAEXT':
            die_num($_POST['subtotal']*$_POST['quantity']*0.40);

        case '38RANGE':
        case 'EARLYBIRD2020':
        case 'CHRISTMAS2021':
            die_num($_POST['subtotal']*$_POST['quantity']*0.38);

        // 35% off all books
        case 'OCTBOOK35':
        case 'EOYDISCOUNT':
        case 'YEARBOOK':
        case 'YEARBOOKEXT':
        case 'GOLD35GLTBMB':
        case 'SILVER4PWV3Y':
        case 'CLASSICISBEST':
        case 'BESTCLASSIC':
        case 'BESTCLASSICEXT':
        case 'GOLD6YNTST6H':
        case 'SILVERYHF7VN':
            die_num($_POST['subtotal']*$_POST['quantity']*0.35);

        // 40% off hard, 30% off soft
        case 'SOFTCOVER':
        case 'SOFTCOVEREXT':
        case 'GOLDAF72Q6R':
        case 'SILVERPK46V3':
        case 'HARDCOVER':
        case 'HARDCOVEREXT':
        case 'GOLDYH82R59U':
        case 'SILVERKC7RS2':
            if(strtoupper($_POST['covertype']) == 'SOFTCOVER')
                die_num($_POST['subtotal']*$_POST['quantity']*0.40);
            die_num($_POST['subtotal']*$_POST['quantity']*0.30);

        // 30% off softcovers
        case 'SUMMERSOFT':
        case 'SUMMERSC':
            if(strtoupper($_POST['covertype']) == 'SOFTCOVER')
                die_num($_POST['subtotal']*$_POST['quantity']*0.33);
            break;

        // 40% off 11x8.5
        case 'OCTBOOK40':
            if((strpos($_POST['thisprod']['prodcode'], '1185') !== FALSE) and ((strtoupper($_POST['covertype']) == 'DEBOSSED COVER') or (strtoupper($_POST['covertype']) == 'PHOTOCOVER') or (strtoupper($_POST['covertype']) == 'SOFTCOVER')) and (strpos($_POST['thisprod']['prodcode'], 'LF-') === FALSE) and (strpos($_POST['thisprod']['prodcode'], 'C300') === FALSE))
                die_num($_POST['subtotal']*$_POST['quantity']*0.40);
            break;

        // 35% off layflat
        case 'SUMMERLAYFLAT':
        case 'BESTLAYFLATEXT':
        case 'BESTLAYFLAT':
        case 'LAYFLATISBEST':
        case 'GOLDM7F4NMVX':
        case 'SILVER68YDYK':
            if(strpos($_POST['thisprod']['prodcode'], 'LF-') !== FALSE)
                die_num($_POST['subtotal']*$_POST['quantity']*0.4);
            break;

        // 35% off product only, whole range
        case '12PHOTOBOOK':
            die_num((($_POST['thisprod']['baseprice']*0.35)+((($_POST['pagecount'] - $_POST['thisprod']['included']) * $_POST['thisprod']['extrapageprice'])*0.35)) * $_POST['quantity']);

        // upgrade to hd for $5 - Extra pages charged at the corresponding HD paper type
        case 'HDFIVER':
            if(strpos($_POST['thisprod']['prodcode'], 'C300') !== FALSE)
                break;
            if(strpos($_POST['thisprod']['prodcode'], 'LF-') !== FALSE)
                break;

            if($_POST['thisprod']['papercode'] == 'DLLSTR') {
                if (strpos($_POST['thisprod']['prodcode'], 'XSSS66') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSS88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSC88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSH88') !== FALSE){
                    $upgradePrice = 22.45;
                } else if (strpos($_POST['thisprod']['prodcode'], 'SLC86') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SLS86') !== FALSE)  {
                    $upgradePrice = 20;
                } else if (strpos($_POST['thisprod']['prodcode'], 'MPH811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLH1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPS811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLS1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPC811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLC1185') !== FALSE)  {
                    $upgradePrice = 40;
                } else if (strpos($_POST['thisprod']['prodcode'], 'LSS1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSC1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSH1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDS1212') !== FALSE) {
                    $upgradePrice = 40;
                } else if (strpos($_POST['thisprod']['prodcode'], 'NXLLC1612') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDL1612') !== FALSE) {
                    $upgradePrice = 45;
                }
            }
            elseif($_POST['thisprod']['papercode'] == 'GP-250RD'){
                if (strpos($_POST['thisprod']['prodcode'], 'XSSS66') !== FALSE){
                    $upgradePrice = 30;
                } else if (strpos($_POST['thisprod']['prodcode'], 'SLC86') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SLS86') !== FALSE)  {
                    $upgradePrice = 30;
                } else if (strpos($_POST['thisprod']['prodcode'], 'SSS88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSC88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSH88') !== FALSE)  {
                    $upgradePrice = 40;
                } else if (strpos($_POST['thisprod']['prodcode'], 'MPH811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLH1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPS811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLS1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPC811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLC1185') !== FALSE)  {
                    $upgradePrice = 60;
                } else if (strpos($_POST['thisprod']['prodcode'], 'LSS1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSC1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSH1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDS1212') !== FALSE) {
                    $upgradePrice = 65;
                } else if (strpos($_POST['thisprod']['prodcode'], 'NXLLC1612') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDL1612') !== FALSE) {
                    $upgradePrice = 80;
                }
            }

            // this is the $5 off bit (above is the full price of the upgrade)
            $upgradePrice -= 5;

            $upgradePrice *= $_POST['quantity'];
            die_num($upgradePrice);

        // 40% off HD and layflat
        case 'BESTBOOKHD':
        case 'BESTBOOKHDEXT':
        case 'GOLDJWKT5NTY':
        case 'SILVERY8P72J':
        case 'HDBOOK40':
        case 'HDBOOK40EXT':
        case 'GOLDXGRN6TX8':
        case 'SILVERFNFB9D':
        case 'BESTBOOKSDEXT':
        case 'BESTBOOKSD':
        case 'GOLDTLBHHTFG':
        case 'SILVERSD7VWP':
        case 'SDBOOK35':
        case 'SDBOOK35EXT':
        case 'GOLD78V68PL':
        case 'SILVERKC3PTP':
        case '40BEST':
        case '40BESTEXT':
        case 'GOLDJ5R6GXW3':
        case 'SILVERLS42BB':
        case 'BEST40':
        case 'BEST40EXT':
        case 'BESTBOOK40':
        case 'BESTBOOKS40':
        case 'BESTBOOKS40EXT':
        case 'GOLD4VLJLZRZ':
        case 'SILVERWMS3D7':
            if($_POST['thisprod']['papercode'] == 'GP-250RD' or $_POST['thisprod']['papercode'] == 'DLLSTR')
                die_num(($_POST['subtotal']*$_POST['quantity']) * 0.4);
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.35);

        // 33% off hd
        case 'HD33':
            if($_POST['thisprod']['papercode'] == 'GP-250RD' or $_POST['thisprod']['papercode'] == 'DLLSTR')
                die_num(($_POST['subtotal']*$_POST['quantity']) * 0.33);
            break;

        // 33% off everything
        case 'MYYEARBOOK':
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.35);
            break;

        case '35BEST':
        case '35BESTEXT':
        case 'GOLDZ3P6YF45':
        case 'SILVER8XMPJ9':
        case 'BEST35':
        case 'BEST35EXT':
        case 'BESTBOOK33':
        case 'BESTBOOKS35':
        case 'BESTBOOKS35EXT':
        case 'GOLDT5VJ38X5':
        case 'SILVERLJVM6M':
        case 'OTHERBOOKS2023':
        case 'OTHERBOOKS2023EXT':
        case 'GOLD6TS9A3':
        case 'SILVER6TS9A3':
        case 'OTHERBOOKS33':
        case 'OTHERBOOKS33EXT':
        case 'GOLDA65TSY4P':
        case 'SILVERY49J2S':
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.33);
            break;

        // Upgrade to HD Luster or HD Glossy - Extra pages charged at the corresponding HD paper type
        case '1CENTHD':
        case '2017HD':
        case 'SPRING2017':
            if(strpos($_POST['thisprod']['prodcode'], 'C300') !== FALSE)
                break;
            if(strpos($_POST['thisprod']['prodcode'], 'LF-') !== FALSE)
                break;

            $upgradePrice = 0;
            if($_POST['thisprod']['papercode'] == 'DLLSTR') {
                if (strpos($_POST['thisprod']['prodcode'], 'XSSS66') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSS88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSC88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSH88') !== FALSE){
                    $upgradePrice = 22.45;
                } else if (strpos($_POST['thisprod']['prodcode'], 'SLC86') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SLS86') !== FALSE)  {
                    $upgradePrice = 20;
                } else if (strpos($_POST['thisprod']['prodcode'], 'MPH811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLH1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPS811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLS1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPC811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLC1185') !== FALSE)  {
                    $upgradePrice = 40;
                } else if (strpos($_POST['thisprod']['prodcode'], 'LSS1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSC1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSH1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDS1212') !== FALSE) {
                    $upgradePrice = 40;
                } else if (strpos($_POST['thisprod']['prodcode'], 'NXLLC1612') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDL1612') !== FALSE) {
                    $upgradePrice = 45;
                }
            }
            elseif($_POST['thisprod']['papercode'] == 'GP-250RD'){
                if (strpos($_POST['thisprod']['prodcode'], 'XSSS66') !== FALSE){
                    $upgradePrice = 30;
                } else if (strpos($_POST['thisprod']['prodcode'], 'SLC86') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SLS86') !== FALSE)  {
                    $upgradePrice = 30;
                } else if (strpos($_POST['thisprod']['prodcode'], 'SSS88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSC88') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'SSH88') !== FALSE)  {
                    $upgradePrice = 40;
                } else if (strpos($_POST['thisprod']['prodcode'], 'MPH811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLH1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPS811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLS1185') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MPC811') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'MLC1185') !== FALSE)  {
                    $upgradePrice = 60;
                } else if (strpos($_POST['thisprod']['prodcode'], 'LSS1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSC1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'NLSH1212') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDS1212') !== FALSE) {
                    $upgradePrice = 65;
                } else if (strpos($_POST['thisprod']['prodcode'], 'NXLLC1612') !== FALSE or strpos($_POST['thisprod']['prodcode'], 'PRO_BDL1612') !== FALSE) {
                    $upgradePrice = 80;
                }
            }
            
            $upgradePrice *= $_POST['quantity'];
            die_num($upgradePrice);
            
        case '11BDAY':
            if($_POST['total'] >= 200)
                die_num(80);
            if($_POST['total'] >= 150)
                die_num(45);
            if($_POST['total'] >= 100)
                die_num(25);
            if($_POST['total'] >= 50)
                die_num(10);
            break;

        // THIRTYFIRST - 30% off any order
        case 'THIRTYFIRST':
        // THIRTYFREE - 30% off any order
        case 'THIRTYFREE':
        // THIRTYOFF - 30% off any order
        case 'THIRTYOFF':
        // Autoresponder New Customer    30% off any product
        case 'THIRTY4ME':
            die_num($_POST['total'] * 0.3);
            break;

        case 'FORTYFIRST':
        case 'FORTYFIRSTB':
        case 'FORTYFREE':
        case 'FORTYFREEB':
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.4);
            break;

        case 'MEMORIES2022':
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.3);
            break;

        // DBNO campaign    50% discount any product
        case 'FIFTYLOVE':
            die_num($_POST['total'] * 0.5);
            break;

        // THIRTYFIVE - 35% off any photo book
        case 'THIRTYFIVE':
        case 'THIRTYFIVEEXT':
        case 'GOLDL5B2W47R':
        case 'SILVERS5WJVX':
            die_num($_POST['total'] * 0.35);
            break;

        case 'LOVEMUM2023':
        case 'WELOVEMUM':
        case '14BDAY':
        case 'CHRISTMAS2022':
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.38);
            break;

        // Professional Photographer Deal    10% off
        case 'CXRZDFM3TKDE':
            if(time() < mktime(0,0,0,1,1,2018))
                die_num($_POST['total'] * 0.1);
            break;

        // FSHIP51MBC6A41 - free shipping on any order
        case 'FSHIP51MBC6A41':
            die_num($_POST['shipping']);
            break;
            
        // $5 slipcase not including hot stamping
        case 'SLIPCASEFIVER':
            $discount = 0;
            if($_POST['slipcase'])
                $discount += $_POST['thisprod']['scprice'] * $_POST['quantity'];
            die_num($discount);
            break;

        // MUM8X8 - free slip case with foiling on 8x8" books only
        case 'MUM8X8':
            if($_POST['prodtype'] == '88'){
                $discount = 0;
                if($_POST['slipcase'])
                    $discount += $_POST['thisprod']['scprice'];
                if($_POST['slipcasehs'])
                    $discount += $_POST['thisprod']['schsprice'];
                die_num($discount);
            }
            break;

        // First order (Google ads)    20 extra pages free
        case 'FIRSTORDER20':
        case '1STORDER20':
        // Customer Service New Customer    20 Free Pages
        case 'ORDER20FREE':
            $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            $pricewithfree = max(0, ($_POST['pagecount'] - 20 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
            die_num(max(0, $currentpaperprice - $pricewithfree));
            break;

        // 20 fep on layflats with free slipcase
        case 'LAYFLATBONUS':
            if(strpos($_POST['thisprod']['prodcode'], 'LF-') !== FALSE) {
                $currentpaperprice = max(0, ($_POST['pagecount'] - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
                $pricewithfree = max(0, ($_POST['pagecount'] - 20 - $_POST['thisprod']['included'])) * $_POST['thisprod']['extrapageprice'] * $_POST['quantity'];
                $discount = max(0, $currentpaperprice - $pricewithfree);
                if($_POST['slipcase'])
                    $discount += $_POST['thisprod']['scprice'];
                die_num($discount);
            }
            break;


        // 1-1 Letters    20% off Photo Book, 2nd order
        case 'FIRST20NFDKNJQ':
        // Brochure    20% off Photo Book
        case '2NDORDER20':
        // Autoresponder New Customer    20% off any product
        case 'YK9N65FR987XE':
            die_num($_POST['total'] * 0.2);
            break;

        case 'BIGBOOKS':
        case 'THEBIGBOOKS':
        case 'THEBIGBOOKSEXT':
        case 'GOLDA23NKW6P':
        case 'SILVERBX55F7':
        case 'BIGBOOKSEXT':
        case 'GOLDL63NKW6Y':
        case 'SILVERWX85D3':
        case 'BIGBOOKS2023':
        case 'BIGBOOKS2023EXT':
        case 'GOLD4U22AF':
        case 'SILVER4U22AF':
        case 'BIGBOOKS40':
        case 'BIGBOOKS40EXT':
        case 'GOLD4C4MTB7W':
        case 'SILVER9C5X2D':
        if(strpos($_POST['thisprod']['prodcode'], '1212') or strpos($_POST['thisprod']['prodcode'], '1410') or strpos($_POST['thisprod']['prodcode'], '1612')){
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.40);
        }
        break;

        case 'OTHERBOOKS':
        case 'THEOTHERBOOKS':
        case 'THEOTHERBOOKSEXT':
        case 'GOLDWCT8ZFNA':
        case 'SILVER6PKSTV':
        case 'OTHERBOOKSEXT':
        case 'GOLDWCT8ZMML':
        case 'SILVER6PKTBY':
        case 'EOY2022':
            die_num(($_POST['subtotal']*$_POST['quantity']) * 0.35);
        break;

        case 'BIGGERBOOKS':
            $discVal = 0;
            if(strpos($_POST['thisprod']['prodcode'], '1212') or strpos($_POST['thisprod']['prodcode'], '1612')){
                $is_luster = $_POST['thisprod']['papercode'] == 'DLLSTR';
                $is_glossy = $_POST['thisprod']['papercode'] == 'GP-250RD';
                $is_sd = !$is_luster && !$is_glossy;
                
                if(strpos($_POST['thisprod']['prodcode'], '1212') !== FALSE){
                    $cover = 'MATERIAL';
                    $cover = strpos($_POST['thisprod']['prodcode'], 'LSS') ? 'SOFTCOVER' : $cover;
                    $cover = strpos($_POST['thisprod']['prodcode'], 'LSC') ? 'PHOTOCOVER' : $cover;
                    $cover = strpos($_POST['thisprod']['prodcode'], 'LSH') ? 'DEBOSS' : $cover;
                    $type = strpos($_POST['thisprod']['prodcode'], 'LF-') === FALSE ? 'PUR' : 'LF';
                    if($type == 'LF'){
                        switch($cover){
                            case 'PHOTOCOVER':
                                $discVal = (96.95 - 74.50);
                                break;
                            case 'DEBOSS':
                                $discVal = (106.95 - 82.50);
                                break;
                            case 'MATERIAL':
                                $discVal = (106.95 - 82.50);
                                break;
                        }                        
                    }
                    else if($is_sd){
                        switch($cover){
                            case 'SOFTCOVER':
                                $discVal = (66.95 - 37.50);
                                break;
                            case 'PHOTOCOVER':
                                $discVal = (86.95 - 57.50);
                                break;
                            case 'DEBOSS':
                                $discVal = (96.95 - 67.50);
                                break;
                            case 'MATERIAL':
                                $discVal = (96.95 - 67.50);
                                break;
                        }
                    }
                    else if($is_luster){
                        switch($cover){
                            case 'SOFTCOVER':
                                $discVal = (106.95 - 59.95);
                                break;
                            case 'PHOTOCOVER':
                                $discVal = (126.95 - 79.95);
                                break;
                            case 'DEBOSS':
                                $discVal = (136.95 - 89.95);
                                break;
                            case 'MATERIAL':
                                $discVal = (136.95 - 89.95);
                                break;
                        }
                    }
                    else if($is_glossy){
                        switch($cover){
                            case 'SOFTCOVER':
                                $discVal = (131.95 - 77.50);
                                break;
                            case 'PHOTOCOVER':
                                $discVal = (151.95 - 97.50);
                                break;
                            case 'DEBOSS':
                                $discVal = (161.95 - 107.50);
                                break;
                            case 'MATERIAL':
                                $discVal = (161.95 - 107.50);
                                break;
                        }                        
                    }
                }
                else{
                    $cover = 'MATERIAL';
                    $cover = strpos($_POST['thisprod']['prodcode'], 'LLC') ? 'PHOTOCOVER' : $cover;
                    $type = strpos($_POST['thisprod']['prodcode'], 'LF-') === FALSE ? 'PUR' : 'LF';
                    if($type == 'LF'){
                        switch($cover){
                            case 'PHOTOCOVER':
                                $discVal = (129.95 - 79.95);
                                break;
                            case 'MATERIAL':
                                $discVal = (139.95 - 89.95);
                                break;
                        }                        
                    }
                    else if($is_sd){
                        switch($cover){
                            case 'PHOTOCOVER':
                                $discVal = (119.95 - 69.95);
                                break;
                            case 'MATERIAL':
                                $discVal = (129.95 - 79.95);
                                break;
                        }
                    }
                    else if($is_luster){
                        switch($cover){
                            case 'PHOTOCOVER':
                                $discVal = (164.95 - 109.95);
                                break;
                            case 'MATERIAL':
                                $discVal = (174.95 - 119.95);
                                break;
                        }
                    }
                    else if($is_glossy){
                        switch($cover){
                            case 'PHOTOCOVER':
                                $discVal = (199.95 - 129.95);
                                break;
                            case 'MATERIAL':
                                $discVal = (209.95 - 139.95);
                                break;
                        }                        
                    }                    
                }
            }    
            die_num($discVal);        
            break;
            
    }
    die_num(0);