<?php

    function send_email($to, $subject, $message, $add_template = true, $attachment = false, $fromemail = 'service@albumworks.com.au', $fromname = 'albumworks'){
        // add template
        if($add_template){
            $message = '<!DOCTYPE HTML><html><head></head><body style="background:white; margin:0; padding:0;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" valign="top"><table cellpadding="0" cellspacing="0" width="600" style="border-left:1px solid #8b8f8e;border-right:1px solid #8b8f8e;"><tr><td align="center" valign="top" style="background:#f2efdc"><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="background:#1e8bd4" align="left"><img src="https://www.albumworks.com.au/images/share_email_logo.jpg" width="163" height="84" alt="albumworks" /></td></tr><tr><td align="left" valign="top" style="border:1px solid #f2efdc;"><table cellpadding="0" cellspacing="0" width="600"><tr><td width="15">&nbsp;</td><td align="left" valign="top">'.$message.'</td><td width="15">&nbsp;</td></tr></table></td></tr></table><br><br><br></td><td width="15">&nbsp;</td></tr></table></td></tr></table></td></tr></table></td></tr></table></body></html>';
            $message = str_replace('<h1>','<h1 style="background:#f2efdc; margin:0; padding:5px 0; font-family:Arial; font-size:18px; font-weight:normal;">', $message);
            $message = str_replace('</h1>','</h1><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="background:white; border:1px solid #8B8F8E;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="15">&nbsp;</td><td align="left" valign="top">', $message);

            // style text
            $replacers = array(
                '<p>' => '<p style="font-family:Arial; font-size:14px;">',
            );
            foreach($replacers as $f => $t) $message = str_replace($f, $t, $message);
        }

        require_once('../inc/phpmailer/class.phpmailer.php');
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try {
            $mail->SingleTo   = true;
            $mail->SMTPDebug  = 0;
            $mail->SMTPAuth   = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host       = getenv('MAIL_HOST');
            $mail->Port       = getenv('MAIL_PORT');
            $mail->Username   = getenv('MAIL_USERNAME');
            $mail->Password   = getenv('MAIL_PASSWORD');
            $mail->AddAddress($to);
            $mail->SetFrom($fromemail, $fromname);
            $mail->Subject = $subject;
            $mail->MsgHTML($message);

            if($attachment) $mail->AddAttachment($attachment);
            $mail->Send();
            return false;
        } catch (phpmailerException $e) {
            return $e->errorMessage();
        }
        return 'ERROR';
    }
    
    function send_mobile_email($product, $link, $to){
        $content = "<!DOCTYPE html><html lang=\"en\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><head style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'></head><body style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;font-weight:normal;font-size:16px;text-align:center;color:#1063ba;padding:0;margin:0;'> <br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><table class=\"headertable\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;background:#ffffff;height:110px;'><tr style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><td align=\"center\" valign=\"top\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'> <img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/albumworks_logo.jpg\" width=\"300\" height=\"70\" alt=\"ALBUMWORKS\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'><div class=\"headerIconGroup\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;padding-top:10px;padding-bottom:10px;'> <div class=\"headerIcon\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:inline-block;'> <a href=\"https://www.albumworks.com.au/photo-books\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/photobooks.jpg\" alt=\"PHOTOBOOKS\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'></a> </div><div class=\"headerIcon\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:inline-block;'> <a href=\"https://www.albumworks.com.au/promotions\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/promotions.jpg\" alt=\"PROMOTIONS\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'></a> </div><div class=\"headerIcon\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:inline-block;'> <a href=\"https://www.albumworks.com.au/contact\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/contact.jpg\" alt=\"CONTACT\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'></a> </div></div></td></tr></table><table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><tr style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><td align=\"center\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'> <table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" class=\"thebody\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;background:white;'><tr style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><td align=\"center\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'> <h2 style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;font-size:28px;font-weight:normal;margin:1em 0;'>Your personal link</h2> <p class=\"center\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;text-align:center;'><img src=\"https://www.albumworks.com.au/img/mobiledesk.jpg\" width=\"580\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;margin:0 auto;'></p><br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><table cellpadding=\"0\" cellspacing=\"0\" width=\"500\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><tr style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><td style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'> <p class=\"\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;'>You recently visited our website on a mobile device to make PRODPRODPROD. Make your product on any Desktop or Laptop computer by clicking the link below!</p></td></tr></table><br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><p class=\"center\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;text-align:center;'><a href=\"LINKLINKLINK\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://www.albumworks.com.au/img/mobilecta.jpg\" width=\"169\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;margin:0 auto;'></a></p><br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'></td></tr></table></td></tr></table><table class=\"footerblue\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;background:#1162BB;height:300px;'><tr style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><td align=\"center\" valign=\"top\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'> <img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/footer-01.jpg\" width=\"620\" height=\"85\" alt=\"HOW CAN WE HELP\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'><div class=\"footerIconGroup\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;padding-top:20px;padding-bottom:20px;'> <div class=\"footerIcon\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:inline-block;'> <a href=\"https://www.albumworks.com.au/calculator\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/footer-02.jpg\" width=\"150\" height=\"140\" alt=\"HOW MUCH DOES A PHOTO BOOK COST?\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'></a> </div><div class=\"footerIcon\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:inline-block;'> <a href=\"https://www.albumworks.com.au/shipping\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/footer-03.jpg\" width=\"150\" height=\"140\" alt=\"WHEN WILL I GET MY ORDER?\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'></a> </div><div class=\"footerIcon\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:inline-block;'> <a href=\"https://www.albumworks.com.au/faq\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/footer-04.jpg\" width=\"150\" height=\"140\" alt=\"I HAVE A QUESTION I'D LIKE ANSWERED\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'></a> </div><div class=\"footerIcon\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:inline-block;'> <a href=\"https://www.albumworks.com.au/rewards\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;color:#9cc8eb;'><img src=\"https://s3-ap-southeast-2.amazonaws.com/albumprinter-data-syd/emails/aw_20170607/footer-05.jpg\" width=\"150\" height=\"140\" alt=\"HOW CAN I SAVE ON MY NEXT ORDER?\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;display:block;'></a> </div></div></td></tr><tr style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><td class=\"footertd\" align=\"center\" valign=\"top\" style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;padding-top:20px;width:520px;color:#ffffff;font-size:11px;'> <br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'><p style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;'>Only one Voucher Code can be redeemed per order and can't be combined with other Voucher Codes. </p><p style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;'>You are receiving this email because you clicked \"Notify me of software updates and special<br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'>offers\" when you downloaded the Albumworks editor.</p><p style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;'><a href=\"*%7CUNSUB%7C*\" style='color:#9cc8eb;font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;'>Click here to Unsubscribe from this mailing list</a></p><p style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;'>Pictureworks Group Pty Ltd trading as albumworks TM<br style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;'>Level 2, 696 Bourke St, Melbourne VIC 3000, Victoria, Australia</p><p style='font-family:\"Trebuchet MS\", Helvetica, sans-serif;margin:0 0 1em;'>For Service and Support 1300 553 448 or visit <a href=\"http://www.albumworks.com.au\" style='color:#9cc8eb;font-family:\"Trebuchet MS\", Helvetica, sans-serif;text-decoration:underline;'>www.albumworks.com.au</a></p></td></tr></table></body></html>";
        $content = str_replace('PRODPRODPROD', $product, $content);
        $content = str_replace('LINKLINKLINK', $link, $content);
        
        send_email($to, 'Start Creating Your '.$product.'!', $content, false);
    }

    if(isset($_POST['g-recaptcha-response']) and $_POST['g-recaptcha-response']) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode('6LcHNrsZAAAAAKCLUmugnHDxYyEjuArF_3bzwC9H') .  '&response=' . urlencode($_POST['g-recaptcha-response']);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        if($responseKeys["success"]) {
            if (isset($_REQUEST['email__norobot']) and $_REQUEST['email__norobot'] == '687eda030d9cd142552813104ec35be5') {
                if ($_REQUEST['email__body'] == 'dump') {
                    $data = $_REQUEST;
                    foreach ($data as $key => $val) {
                        if (substr($key, 0, 7) == 'email__')
                            unset($data[$key]);
                    }
                    $_REQUEST['email__body'] = '<pre>' . print_r($data, true) . '</pre>';
                }

                switch ($_REQUEST['email__to']) {
                    case 'awservice':
                        $result = send_email('service@albumworks.com.au', $_REQUEST['email__subject'], $_REQUEST['email__body']);
                        break;

                    default:
                        if (isset($_REQUEST['mobileform'])) {
                            send_mobile_email($_REQUEST['product'], $_REQUEST['link'], str_replace(' ', '+', $_REQUEST['email__to']));
                        }
                        break;
                }
            }
            else header('Location: https://www.albumworks.com.au/?nor');
        }
        else header('Location: https://www.albumworks.com.au/?nog');
    }
    header('Location: '.'https://www.albumworks.com.au/'.$_REQUEST['email__returl']);