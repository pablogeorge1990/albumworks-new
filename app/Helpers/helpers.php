<?php

// all data must be exported from https://docs.google.com/spreadsheets/d/126wutjCZmHYwof-lo_pXUlGrc0wiZ82iJyuDCv1s0jA whenever pricing changes are made
function get_pricing_data(){
    $pricing = [];
    if (($handle = fopen(storage_path('app/data/pricing.tsv'), 'r')) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
            // binding
            if(!isset($pricing[$data[3]]))
                $pricing[$data[3]] = [];

            // paper/quality
            if(!isset($pricing[$data[3]][$data[4]]))
                $pricing[$data[3]][$data[4]] = [];

            // cover
            if(!isset($pricing[$data[3]][$data[4]][$data[5]]))
                $pricing[$data[3]][$data[4]][$data[5]] = [];

            // product
            if(!isset($pricing[$data[3]][$data[4]][$data[5]]['p'.$data[2]])){
                $pricing[$data[3]][$data[4]][$data[5]]['p'.$data[2]] = [
                    'prodcode' => $data[0],
                    'papercode' => $data[1],
                    'included' => $data[6],
                    'min' => $data[7],
                    'max' => $data[8],
                    'baseprice' => $data[9],
                    'extrapageprice' => $data[10],
                    'scprice' => $data[11],
                    'schsprice' => $data[12],
                    'pbprice' => $data[13],
                    'pbhsprice' => $data[14],
                    'hsprice' => $data[15],
                    'shippingfirstprice' => $data[16],
                    'shippingaddtlprice' => $data[17],
                ];
            }
            else{
                echo "<pre>duplicate found:\n".print_r($data, true);
                die;
            }
        }
        fclose($handle);
    }

    return $pricing;
}

function cleanit($string){
    //return preg_replace("/[^A-Za-z0-9]/", '', strtolower($string));
    return trim(strtolower($string));
}

function fetchFaqData(){
    $_GET['view'] = isset($_GET['view']) ? $_GET['view'] : '';

    if(isset($_GET['search']))
        $_GET['view'] = 'searchResults';

    if(isset($_GET['solution_ID']))
        $_GET['view'] = 'viewSolution';

    $_GET['search'] = str_replace(' ','+', @$_GET['search']);

    switch($_GET['view']){
        case 'rateSolution':
            $c['supportview'] = file('https://api.photo-products.com.au/FAQ/rateSolution.php?embed&vendor_ID=1031&rating='.$_GET['rating'].'&solution_ID='.$_GET['solution_ID']);
            break;
        case 'viewSolution':
            $c['supportview'] = file('https://api.photo-products.com.au/FAQ/viewSolution.php?embed&vendor_ID=1031&solution_ID='.$_GET['solution_ID'].(isset($_GET['rated']) ? '&rated='.$_GET['rated'] : ''));
            foreach($c['supportview'] as $line){
                if(strpos($line, 'faqsol_sol_title') !== FALSE){
                    //$c['meta_title'] = trim(strip_tags($line)).' | '.$c['meta_title'];
                }
            }
            break;
        case 'searchKeyWord':
            $c['supportview'] = file('https://api.photo-products.com.au/FAQ/searchKeyWord.php?embed&vendor_ID=1031&keyword='.$_GET['keyword'].'&search='.$_GET['search']);
            break;
        case 'searchResults':
            $c['supportview'] = file('https://api.photo-products.com.au/FAQ/searchResults.php?embed&vendor_ID=1031&search='.$_GET['search']);
            break;
        default:
            $c['supportview'] = file('https://api.photo-products.com.au/FAQ/index.php?embed&vendor_ID=1031');
            break;
    }
    return $c;
}

function fetchLandingPageMetaData($page){
    $__pagedata = [];
    switch($page){
        case 'desk-prints':
            $__pagedata = [
                'title' => 'Desk Prints',
                'document_title' => 'Desk Prints: 28 high prints in a desk flip | albumworks',
                'meta_description' => 'Get 28 high quality photo prints, spiral bound in a handy deskflip. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'DESK PRINTS',
                    'details' => 'THE DETAILS',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',

                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/deskprints/01-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPSHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => 'details',
                        'heading' => 'Desk Prints',
                        'subline' => '<em>"A photograph is life\'s pause button"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/deskprints/prestige-prints-desk-prints-03_02.jpg',
                        'mobileimage' => 'img/deskprints/images/prestige-prints-desk-prints-03_02.jpg',
                        'classes' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Desk Prints',
                        'content' => '<p>Stand your beautiful photos proudly on your desk with our hand made photo stand. The high quality stand is covered in a quality Black Wibalin Material and ring wire bound at the top. Each Desk Flip contains 28 pages which you can design in any way you like or use one of our handy templates.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/deskprints/prestige-prints-desk-prints-03_03.jpg',
                        'mobileimage' => 'img/deskprints/images/prestige-prints-desk-prints-03_03.jpg',
                        'classes' => 'leftimg',
                        'textclasses' => '',
                        'heading' => 'The details',
                        'content' => '<p>Printed on premium photo paper with exceptional colour and an archival rating, they make a stunning gift. Keep your precious photos with you at work or at home.</p><p>Choose a single image to a page or apply any of a wide range of page styles</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Available in 6x8" portrait or 11x5" landscape.</p><p><strong>6 x 8"</strong>  - (15 x 20cm)</p><p><strong>11 x 5"</strong>  - (28 x 12.5cm)</p>',
                        'image' => 'img/deskprints/04-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','DESK PRINTS'],
                        ['6 x 8"','$36.95'],
                        ['11 x 5"','$36.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00 each'],
                ],
            ];
            break;

        case 'desk-calendar':
            $__pagedata = [
                'title' => 'Desk Calendars',
                'document_title' => null,
                'meta_description' => null,
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'DESK CALENDARS',
                    'styles' => 'STYLES',
                    'paper' => 'PAPER',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/deskcalendar/2021/header-01.jpg',
                        'heading' => '<em>"Nobody is too busy, it\'s just a matter of priorities"</em>',
                        'subline' => 'BEAUTIFUL CALENDARS IN STANDARD COLOUR OR HI COLOUR',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Desk Calendars',
                        'subline' => '<em>"Beautiful calendars in Standard Colour or Hi Colour"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/deskcalendar/2021/01.jpg',
                        'mobileimage' => 'img/deskcalendar/images/02-desk.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Always on display',
                        'content' => '<p>A great way to keep your memories alive with you each day.  Stand your gorgeous photos proudly on your desk with our handmade black tent stand. The stand is covered in a black cardboard and cleverly folded to form a sturdy free standing structure. Desk Calendars are available in Standard Colour or Hi Colour printing.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/deskcalendar/2021/02.jpg',
                        'mobileimage' => 'img/deskcalendar/images/custom.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'styles',
                        'textclasses' => '',
                        'heading' => 'Total flexibility',
                        'content' => '<p>Our Editor allows you to choose either a Classic or Smart Desk Calendar.</p><p>Classic Calendars allow you to choose a Large stylish grid for each month, with ample room to write down important dates. Or choose a Compact and practical grid, allowing for even more photos in your Calendar!</p><p>Smart Calendars allow you to customise every date cell of your year. Mark important dates and change the colour of date cells or even add a photo to the date cell!</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/deskcalendar/20190830/03.jpg',
                        'mobileimage' => 'img/deskcalendar/images/04-paper.jpg',
                        'classes' => 'bookmark',
                        'id' => 'paper',
                        'textclasses' => 'leftside',
                        'heading' => 'Beautiful papers',
                        'content' => '<p>Desk Calendars are available in Standard Colour, printed on a heavy 300gsm paper, which is close to card weight. Or upgrade to Hi Colour printing on beautiful 260gsm Photo Luster paper.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Our Desk Calendars come in 2 different sizes. Sizes are measured by single page.</p><p><strong>300gsm Standard Colour: 10 x 5" </strong>  - (25.5 x 12.5cm)</p><p><strong>250gsm Hi Colour: 11 x 5"</strong>  - (28 x 12.5cm)</p>',
                        'image' => 'img/deskcalendar/20190830/sizes.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','Standard Colour<br>300gsm SOVEREIGN SILK','Hi Colour<br>260gsm PHOTO LUSTER'],
                        ['10 x 5”','$27.95','-'],
                        ['11 x 5"','-','$37.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['10 x 5”','+ $12.95','+ $3.00'],
                    ['11 x 5"','+ $12.95','+ $3.00'],
                ],
            ];
            break;

        case 'hd-calendar':
            $__pagedata = [
                'title' => 'HI COLOUR CALENDARS',
                'document_title' => null,
                'meta_description' => null,
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'HI COLOUR CALENDARS',
                    'paper' => 'PAPER',
                    'styles' => 'STYLES',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'image' => 'img/hdcal/2021/header-01.jpg',
                        'heading' => '<em>"Nobody is too busy, it\'s just a matter of priorities"</em>',
                        'subline' => 'BEAUTIFUL CALENDARS IN STANDARD COLOUR OR HI COLOUR',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Hi Colour Calendars',
                        'subline' => '<em>"Beautiful Calendars in stunning Hi Colour"</em>',
                    ],

                    [
                        'type' => 'prodpanel',
                        'image' => 'img/hdcal/02-hdcals.jpg',
                        'mobileimage' => 'img/hdcal/images/02-hdcals.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Hi Colour Calendars',
                        'content' => '<p>Upgrade your Calendar from Standard Colour to stunning Hi Colour printing. The paper used for Hi Colour printing is a beautiful heavy weight 260gsm Photo Luster with a very gentle eggshell finish.  It gives exceptional vibrancy and reflects light evenly for beautiful visual clarity. At 260gsm it has plenty of heft in the hand.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/hdcal/03-paper.jpg',
                        'mobileimage' => 'img/hdcal/images/03-paper.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'paper',
                        'textclasses' => '',
                        'heading' => 'Beautiful papers',
                        'content' => '<p>All Hi Colour Calendars are printed on stunning 260gsm Photo Luster.  This premium quality paper is optimised for colour stability to bring out the best in your photos.</p><p><img src="img/hd2022.png" /></p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/hdcal/02.jpg',
                        'mobileimage' => 'img/hdcal/20190830/02.jpg',
                        'classes' => 'bookmark',
                        'id' => 'styles',
                        'textclasses' => 'leftside',
                        'heading' => 'Total flexibility',
                        'content' => '<p>Our Editor allows you to choose either a Classic or Smart Calendar.</p><p>Classic Calendars allow you to choose a Large stylish grid for each month, with ample room to write down important dates. Or choose a Compact and practical grid, allowing for even more photos in your Calendar!</p><p>Smart Calendars allow you to customise every date cell of your year. Mark important dates and change the colour of date cells or even add a photo to the date cell!</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Our Hi Colour Calendars are available in 3 different sizes. Sizes are measured by single page.</p><p><strong>11 x 8"</strong>  - (28 x 20.5cm)</p><p><strong>12 x 12"</strong>  - (30.5 x 30.5cm)</p><p><strong>11 x 5"</strong>  - (28 x 12.5cm)</p>',
                        'image' => 'img/hdcal/20190830/SIZES.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','Standard Colour<br>300gsm SOVEREIGN SILK','Hi Colour<br>260gsm PHOTO LUSTER'],
                        ['11 x 5"','$27.95','$37.95'],
                        ['11 x 8"','$27.95','$37.95'],
                        ['12 x 12"','$37.95','$49.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['11 x 5"','+ $12.95','+ $3.00'],
                    ['11 x 8"','+ $12.95','+ $3.00'],
                    ['12 x 12"','+ $12.95','+ $3.00'],
                ],
            ];
            break;

        case 'wall-calendar':
            $__pagedata = [
                'title' => 'Wall Calendar',
                'document_title' => null,
                'meta_description' => null,
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'WALL CALENDARS',
                    'styles' => 'STYLES',
                    'features' => 'FEATURES',
                    'hdcal' => 'HI COLOUR',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/wallcalendar/2021/01-header.jpg',
                        'heading' => '<em>"Nobody is too busy, it\'s just a matter of priorities"</em>',
                        'subline' => 'BEAUTIFUL CALENDARS IN STANDARD COLOUR OR HI COLOUR',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Wall Calendars',
                        'subline' => '<em>"Beautiful calendars in Standard Colour or Hi Colour"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/wallcalendar/2021/01.jpg',
                        'mobileimage' => 'img/wallcalendar/images/calendars-wall-03_03.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Wall Calendars',
                        'content' => '<p>All our Wall Calendars have a conveniently placed hole for easy wall hanging. The Wall Calendars are bound in the middle with a quality white wire and are printed on both sides of the sheet.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/wallcalendar/2021/02.jpg',
                        'mobileimage' => 'img/wallcalendar/images/custom.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'styles',
                        'textclasses' => '',
                        'heading' => 'Total Flexibility',
                        'content' => '<p>Our Editor allows you to choose either a Classic or Smart Wall Calendar.</p><p>Classic Calendars allow you to choose a Large stylish grid for each month, with ample room to write down important dates. Or choose a Compact and practical grid, allowing for even more photos in your Calendar!</p><p>Smart Calendars allow you to customise every date cell of your year. Mark important dates and change the colour of date cells or even add a photo to the date cell!</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/wallcalendar/2021/03.jpg',
                        'mobileimage' => 'img/wallcalendar/images/calendars-wall-03_07.jpg',
                        'classes' => 'bookmark',
                        'id' => 'features',
                        'textclasses' => 'leftside',
                        'heading' => 'Pen friendly surface',
                        'content' => '<p>Want to make notes or mark important dates? Our calendars have a pen friendly surface for all writing purposes.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/wallcalendar/05-hd.jpg',
                        'mobileimage' => 'img/wallcalendar/images/calendars-wall-03_08.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'hdcal',
                        'textclasses' => '',
                        'heading' => 'Upgrade your Calendar',
                        'content' => '<p>Opt to upgrade your Calendar from Standard Colour to beautiful Hi Colour printing. The paper used for Hi Colour printing is a beautiful 260gsm Photo Luster with a gentle eggshell finish.  It gives exceptional vibrancy and reflects light evenly for beautiful visual clarity. At 260gsm it has plenty of heft in the hand.</p><p><img src="img/wallcalendar/05-hd-icon.png" /></p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Our Wall Calendars come in 2 different sizes. Sizes are measured by single page.</p><p><strong>11 x 8"</strong>  - (28 x 20.5cm)</p><p><strong>12 x 12"</strong>  - (30.5 x 30.5cm)</p>',
                        'image' => 'img/wallcalendar/05.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','Standard Colour<br>300gsm SOVEREIGN SILK','Hi Colour<br>260gsm PHOTO LUSTER'],
                        ['11 x 8"','$27.95','$37.95'],
                        ['12 x 12"','$37.95','$49.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['11 x 8"','+ $12.95','+ $3.00'],
                    ['12 x 12"','+ $12.95','+ $3.00'],
                ],
            ];
            break;

        case 'canvas-photo-prints':
            $__pagedata = [
                'title' => 'Canvas Prints',
                'document_title' => 'Canvas Prints: Your photos in a Canvas print | albumworks',
                'meta_description' => 'Use your own photos to make a top quality, stunning Canvas prints. Easy to use, free software. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'CANVAS PRINTS',
                    'construction' => 'CONSTRUCTION',
                    'features' => 'FEATURES',
                    'printing' => 'PRINTING',
                    'sizes' => 'SIZING',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'image' => 'img/canvas/canvas-prints-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPSHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Canvas Prints',
                        'subline' => '<em>"These are the moments to remember forever"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/canvas/home-decor-canvas-03_02.jpg',
                        'mobileimage' => 'img/canvas/images/02.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Traditional Canvas Prints',
                        'content' => '<p>Our online Editor makes it so quick and easy to make your Canvas Print. Choose one fantastic photo, or create a whole collage! Import directly from Instagram, Facebook or your device.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/canvas/canvas-prints-01.jpg',
                        'mobileimage' => 'img/canvas/images/mobile-01.jpg',
                        'classes' => 'leftimg',
                        'id' => 'construction',
                        'textclasses' => '',
                        'heading' => 'Classic Canvas construction',
                        'content' => '<p>Your Canvas Print starts with a premium poly-cotton canvas. Photo prints are sharp and vibrant with a satin finish, before being carefully hand stretched over a timber frame.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/canvas/canvas-prints-02.jpg',
                        'mobileimage' => 'img/canvas/images/mobile-02.jpg',
                        'classes' => '',
                        'id' => 'features',
                        'textclasses' => 'leftside',
                        'heading' => 'Thick and durable',
                        'content' => '<p>It’s all in the details - Canvas Prints are printed on a 370gsm canvas substrate - resulting in a thick, luscious surface with a recognisable canvas texture. Our timber frames are sturdy, measuring at a 3cm depth.</p><p>Our Canvas Prints are easy to hang with a sturdy string attached for larger sizes, and a hanging kit included for smaller sizes.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Available in a range of sizes and orientations.</p><p><strong>SQUARE</strong></p><p><strong>12 x 12"</strong>  - (30 x 30cm)</p><p><strong>16 x 16"</strong>  - (40 x 40cm)</p><p><strong>24 x 24"</strong>  - (61 x 61cm)</p><p><br><strong>LANDSCAPE / PORTRAIT</strong></p><p><strong>12 x 18"</strong>  - (30 x 45cm)</p><p><strong>16 x 20"</strong>  - (40 x 50cm)</p><p><strong>20 x 30"</strong>  - (50 x 76cm)</p><p><strong>30 x 40"</strong>  - (76 x 102cm)</p><p><br><strong>PANORAMA</strong></p><p><strong>24 x 12"</strong>  - (61 x 30cm)</p><p><strong>40 x 20"</strong>  - (102 x 50cm)</p>',
                        'image' => 'img/canvas/06-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','LANDSCAPE/PORTRAIT'],
                        ['12 x 18"','$59.95'],
                        ['16 x 20"','$89.95'],
                        ['20 x 30"','$169.95'],
                        ['30 x 40"','$199.95'],
                    ],
                    [
                        ['','SQUARE'],
                        ['12 x 12"','$49.95'],
                        ['16 x 16"','$69.95'],
                        ['24 x 24"','$154.95'],
                    ],
                    [
                        ['','PANORAMA'],
                        ['24 x 12"','$89.95'],
                        ['40 x 20"','$239.95'],
                    ],
                ],
                'shipping' => [
                    ['','ITEMS UP TO 30”','EXTRA ITEMS UP TO 30"', 'ITEMS OVER 30"', 'EXTRA ITEMS OVER 30"'],
                    ['EXPRESS SHIP','+ $24.95', '+ $14.95 ea', '+ $44.95', '+ $34.95 ea'],
                ],
                'below_shipping' => '<p>* Note: Canvas Prints cannot be shipped to a PO Box.</p>'
            ];
            break;


        case 'framed-prints':
            $__pagedata = [
                'title' => 'Framed Prints',
                'document_title' => 'Framed Prints: Your photos in a stylish wall hanging Framed Print | albumworks',
                'meta_description' => 'Make a Framed Print using your own photos. Use one or multiple photos. Perfect for decorating walls and gift giving. Easy to use, free software. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'FRAMED PRINTS',
                    'features' => 'FEATURES',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'image' => 'img/framed/01-framed-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPSHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Framed Prints',
                        'subline' => '<em>"These are the moments to remember forever"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/framed/02-framed.jpg',
                        'mobileimage' => 'img/framed/images/02-framed.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Framed Prints',
                        'content' => '<p>Frame the moment forever. Top quality framed print with a classic black wooden frame. A classic keepsake and the perfect way to showcase your favourite photos.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/framed/03-quality.jpg',
                        'mobileimage' => 'img/framed/images/03-quality.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'features',
                        'textclasses' => '',
                        'heading' => 'Features',
                        'content' => '<p>Available in both landscape and portrait orientation. Select from 2 different frame sizes, with a choice of 2 different mat boards to frame your photo.<br><br>All Framed Prints come with a convenient hook to hang on your wall and a handy desk stand to sit upright on tables. </p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Frames are available in 2 sizes, both landscape and portrait. Choose from 2 mat board sizes for each frame size.</p>',
                        'image' => 'img/framed/04-sizes.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','240gsm LUSTER'],
                        ['15 x 11" (12 x 8" PRINT)','$79.95'],
                        ['15 x 11" (10 x 8" PRINT)','$71.95'],
                        ['12 x 8" (8 x 6" PRINT)','$49.95'],
                        ['12 x 8" (7 x 5" PRINT)','$41.95'],
                    ]
                ],
                'shipping' => [
                    ['','EXPRESS<br>SHIP','EXTRA ITEM EXPRESS'],
                    ['ALL SIZES','+ $14.95','+ $10.00'],
                ],
            ];
            break;

        case 'instagram-prints':
            $__pagedata = [
                'title' => 'Instagram Prints',
                'document_title' => 'Instagram Prints: Photo cards from Instagram and Facebook photos | albumworks',
                'meta_description' => 'Turn your Instagram, Facebook and phone photos into printed photo cards. Fast and fun to make, perfect for gift giving or keep for yourself!',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'INSTAGRAM PRINTS',
                    'details' => 'THE DETAILS',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/instagram/01-insta-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPSHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => 'details',
                        'heading' => 'Instagram Prints',
                        'subline' => '<span style="display:inline-block; width:100%; max-width:700px"><em>"What I like about photographs is that they capture a moment that\'s gone forever, impossible to get to reproduce"</em> <span class="author"> - KARL LAGERFIELD</span></span>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/instagram/03-instagram.jpg',
                        'mobileimage' => 'img/instagram/images/03-instagram.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Instagram Prints',
                        'content' => '<p>Turn any of your photos into stylish Photo Cards. Create from your phone, tablet or desktop using your Instagram, Facebook or local photos. Printed on Uncoated Matte 270gsm paper (which means really thick). Prints have a stationery texture and a subtle weave to the paper.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/instagram/04-details.jpg',
                        'mobileimage' => 'img/instagram/images/04-details.jpg',
                        'classes' => 'leftimg',
                        'id' => '',
                        'textclasses' => '',
                        'heading' => 'The details',
                        'content' => '<p>The matte surface is completely pen friendly, so you can write your personal messages on either side!</p><p>Add a wood stand to display your Instagram prints - we think it\'s a great option for the Square and Large prints.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Available in Square, Rectangular, Polaroid-like or Photo-Strips.</p><p><strong>MINI</strong>  - 2.1 x 2.1" (5.5 x 5.5cm)</p><p><strong>PETITE</strong>  - 2.1 x 3.3" (5.5 x 8.5cm)</p><p><strong>SQUARE</strong>  - 4 x 4" (10 x 10cm)</p><p><strong>TALL</strong>  - 2.1 x 5.7" (5.5 x 14.5cm)</p><p><strong>LARGE</strong>  - 3.7 x 5.5" (9.5 x 14cm)</p>',
                        'image' => 'img/instagram/04-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','270gsm UNCOATED MATTE'],
                        ['2.1 x 2.1" (34 pcs)','$9.95'],
                        ['2.1 x 3.3" (24 pcs)','$9.95'],
                        ['4 x 4" (23 pc)','$9.95'],
                        ['2.1 x 5.7"  (14 pcs)','$9.95'],
                        ['3.7 x 5.5"  (17 pcs)','$9.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00'],
                ],
            ];
            break;

        case 'photo-magnets':
            $__pagedata = [
                'title' => 'Magnets',
                'document_title' => 'Photo Magnets: Personalised magnets from your photos | albumworks',
                'meta_description' => 'Create a set of quality Photo Magnets. Decorate your fridge with personalised magnets. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'PHOTO MAGNETS',
                    'details' => 'THE DETAILS',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/magnets/01-magnets-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPSHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => 'details',
                        'heading' => 'Photo Magnets',
                        'subline' => '<em>"Life is like a camera, focus on what\'s most important"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/magnets/02-magnets.jpg',
                        'mobileimage' => 'img/magnets/images/02-magnets.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Photo Magnets',
                        'content' => '<p>Display your photos on the fridge with Photo Magnets. 0.4mm thick and digitally printed for great print quality and vibrant colours.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/magnets/03-quality.jpg',
                        'mobileimage' => 'img/magnets/images/03-quality.jpg',
                        'classes' => 'leftimg',
                        'id' => '',
                        'textclasses' => '',
                        'heading' => 'The details',
                        'content' => '<p>Create full frame photos or choose from a choice of borders to reflect your style.</p><p>Finished with a matte, non-reflective surface.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Available in 6x4" or 5x7" in both landscape and portrait.</p><p><strong>6 x 4"</strong>  - (15 x 10cm)</p><p><strong>7 x 5"</strong>  - (25 x 20cm)</p>',
                        'image' => 'img/magnets/04-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','PHOTO MAGNET'],
                        ['6 x 4" (3pcs)','$14.95 (extra piece = $4.95)'],
                        ['7 x 5" (2pcs)','$14.95 (extra piece = $6.95)'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00'],
                ],
            ];
            break;

        case 'poster-printing':
            $__pagedata = [
                'title' => 'Premium Photo Posters',
                'document_title' => 'Premium Photo Posters: Personalised Posters from your photos | albumworks',
                'meta_description' => 'Make a poster using your own photos. Use one photo or a whole collage. Perfect for bedroom walls, engagements, farewells and well wishes. Easy to use, free software. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'PREMIUM PHOTO POSTERS',
                    'details' => 'THE DETAILS',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'image' => 'img/posters/01-posters-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPASHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => 'details',
                        'heading' => 'Premium Photo Posters',
                        'subline' => '<em>"It\'s time to remember what it\'s like to be alive"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/posters/02-posters.jpg',
                        'mobileimage' => 'img/posters/images/02-posters.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Premium Photo Posters',
                        'content' => '<p>Posters taken to the next level with photographic quality reproduction. These Posters take four times longer to print than Standard Colour posters but result in greater ink density, less grain and brighter, more vivid colours. See your photos in a whole new way.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/posters/03-quality.jpg',
                        'mobileimage' => 'img/posters/images/03-quality.jpg',
                        'classes' => 'leftimg',
                        'id' => '',
                        'textclasses' => '',
                        'heading' => 'The details',
                        'content' => '<p>From one huge image to a collage or even one of our stunning heart designs there is a template to fit every occasion.</p>
                                        <p>Choose from 3 paper types:</p>
                                        <p><strong>240gsm Glossy:</strong> Glossy produces sharp images with a velvety, smooth finish. Images are shiny and whites are bright and crisp.</p>
                                        <p><strong>245gsm Metallic:</strong> Metallic has a noticeable sheen that gives the overall look a metallic, iridescent finish.</p>
                                        <p><strong>250gsm Raster:</strong> Not as pearlescent as our Metallic paper, Raster has a subtle hexagonal texture for a more rugged look and feel.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>From 16x20" all the way up to 24 x 36" - in both landscape and portrait.</p><p><strong>16 x 20"</strong>  - (406 x 508mm)</p><p><strong>20 x 30"</strong>  - (508 x 762mm)</p><p><strong>24 x 36"</strong>  - (596 x 900mm)</p><p><em>Please note: sizes may shift 1-2mm during final trimming</em></p>',
                        'image' => 'img/posters/04-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','240gsm GLOSSY','245gsm METALLIC','250gsm RASTER'],
                        ['16 x 20"','$21.95','$21.95','$21.95'],
                        ['20 x 30"','$26.95','$26.95','$26.95'],
                        ['24 x 36"','$32.95','$32.95','$32.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $9.95','+ $3.00'],
                ],
            ];
            break;

        case 'premium-prints':
            $__pagedata = [
                'title' => 'Premium Prints',
                'document_title' => 'Premium Prints: Classic photo prints from your photos | albumworks',
                'meta_description' => 'Create a set of high quality photo prints in a range of different sizes. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'PREMIUM PRINTS',
                    'quality' => 'QUALITY',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/prints/01-premium-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPSHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Premium Prints',
                        'subline' => '<em>"A good snapshot keeps a moment from running away"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/prints/02-prints.jpg',
                        'mobileimage' => 'img/prints/images/02-prints.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Premium Prints',
                        'content' => '<p>Quality photo prints are back.  Bring your photos to life in a classic set of premium photo prints in beautiful quality with rich and deep colour on 240gsm premium paper.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/prints/03-quality.jpg',
                        'mobileimage' => 'img/prints/images/03-quality.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'quality',
                        'textclasses' => '',
                        'heading' => 'Quality',
                        'content' => '<p>Photos are printed on quality 240gsm paper on either glossy photo paper or glossy silver halide paper to provide maximum punch in colour and a lot of heft in the paper.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Sizes range from 6x4" all the way up to a massive 14x11"</p>',
                        'image' => 'img/prints/04-sizes.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','240gsm GLOSSY'],
                        ['6 x 4" (25 pcs)','$8.95'],
                        ['7 x 5" (4 pcs)','$5.95'],
                        ['10 x 8" (1 pc)','$3.95'],
                        ['14 x 11" (1 pc)','$8.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00'],
                ],
            ];
            break;

        case 'wood-prints':
            $__pagedata = [
                'title' => 'Wood Prints',
                'document_title' => 'Wood Prints: Your photos in a stylish wooden print | albumworks',
                'meta_description' => 'Make a stylish wooden print using your own photos. Use one or multiple photos. Perfect for decorating walls, weddings and gift giving. Easy to use, free software. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'WOOD PRINTS',
                    'details' => 'THE DETAILS',
                    'sizes' => 'SIZES',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/wood/01-woodprints-header.jpg',
                        'heading' => '<em>"Photography is about capturing souls not smiles"</em>',
                        'subline' => '- DRAGAN TAPSHANOV',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => 'details',
                        'heading' => 'Wood Prints',
                        'subline' => '<em>"A photograph is life\'s pause button"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/wood/02-woodprints.jpg',
                        'mobileimage' => 'img/wood/images/02-woodprints.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Wood Prints',
                        'content' => '<p>Printed directly onto Sycamore timber. Your prints are designed to reveal rather than obscure the beautiful natural grain of the Sycamore. The Sycamore is then mounted on a 12mm plywood with bevelled edges and grooves on the rear for easy hanging. </p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/wood/03-details.jpg',
                        'mobileimage' => 'img/wood/images/03-details.jpg',
                        'classes' => 'leftimg',
                        'id' => '',
                        'textclasses' => '',
                        'heading' => 'The details',
                        'content' => '<p>Choose your page layout from full frame images to collages. Add your own text or designs.</p><p>Hanging on the wall is easy, as it\'s designed to lay close to walls with minimal gap.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>From 12 x 8" all the way up to 30 x 20" - in both landscape and portrait.</p><p><strong>12 x 8"</strong>  - (30 x 20cm)</p><p><strong>18 x 12"</strong>  - (45 x 30cm)</p><p><strong>20 x 16"</strong>  - (50 x 40cm)</p><p><strong>30 x 20"</strong>  - (75 x 50cm)</p>',
                        'image' => 'img/wood/04-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','WOOD PRINTS'],
                        ['12 x 8"','$109.95'],
                        ['18 x 12"','$169.95'],
                        ['20 x 16"','$229.95'],
                        ['30 x 20"','$349.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['12 x 8"','+ $12.95','+ $8.00'],
                    ['18 x 12"','+ $12.95','+ $8.00'],
                    ['20 x 16"','+ $12.95','+ $8.00'],
                    ['30 x 20"','+ $24.95','+ $15.00'],
                ],
            ];
            break;

        case 'journals':
            $__pagedata = [
                'title' => 'Journals',
                'document_title' => 'Personalised Journals: Fully customised Journal notebooks | albumworks',
                'meta_description' => 'Use your own photos to make your very own personalised Journal with a stunning material cover. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'JOURNALS',
                    'quality' => 'QUALITY',
                    'templates' => 'TEMPLATES',
                    'premium' => 'PREMIUM TOUCHES',
                    'sizes' => 'SIZING',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'image' => 'img/journals/01-journals-header.jpg?v2',
                        'heading' => '<em>Premium Journals, Guest Books and Notebooks for every occasion</em>',
                        'subline' => '',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => 'journlid',
                        'heading' => 'Personalised Journals',
                        'subline' => '<em>"Keeping a journal is like keeping good care of one\'s heart"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/journals/02-journals.jpg',
                        'mobileimage' => 'img/journals/images/02-journals.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Journals',
                        'content' => '<p>A journal where every page can be customised in full colour. Add photos, quotes or whatever takes your fancy. But most importantly make it yours with 100% design freedom.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/journals/03-quality.jpg',
                        'mobileimage' => 'img/journals/images/03-qaulity.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'quality',
                        'textclasses' => '',
                        'heading' => 'Quality',
                        'content' => '<p>Our journals are made from premium materials. They are printed on high quality 190gsm E-Photo Matte paper, providing the perfect balance between good photographic reproduction and a beautiful writing surface with an earthy, stationery feel. All journals are covered in a material finish with a choice of 10 colour / finish combinations.</p><p style="color:red">PLEASE NOTE: Some colours may be out of stock when ordering</p><p> 
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/buckram/l/2021/EUROBUCKRAM404_500.jpg\', \'European Buckram White\', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_500.jpg" alt="European Buckram White 404/500" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/buckram/l/2021/CHROMO369_300.jpg\', \'European Buckram Metallic Pearl\', 400, 400)"><img src="img/photobooks/buckram/s/2021/CHROMO369_300.jpg" alt="European Buckram Metallic Pearl 369/308" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/buckram/l/2021/CHROMO369_302.jpg\', \'European Buckram Metallic Steel\', 400, 400)"><img src="img/photobooks/buckram/s/2021/CHROMO369_302.jpg" alt="European Buckram Metallic Steel 369/314" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/buckram/l/2021/EUROBUCKRAM404_526.jpg\', \'European Buckram Black\', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_526.jpg" alt="European Buckram Black 404/526" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/buckram/l/2021/EUROBUCKRAM404_505.jpg\', \'European Buckram Blue\', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_505.jpg" alt="European Buckram Blue 404/505" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/buckram/l/new/EUROBUCKRAM404_519.png\', \'European Buckram Vivid Burgundy\', 400, 400)"><img src="img/photobooks/buckram/s/new/EUROBUCKRAM404_519.png" alt="European Buckram Vivid Burgundy 404/519" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/covers/black2.jpg\', \'Black\', 400, 400)"><img src="img/photobooks/covers/black.jpg" alt="Black" style="width:50px; height:50px" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/covers/darkblue3l.jpg\', \'Dark Blue\', 400, 400)"><img src="img/photobooks/covers/darkblue3.jpg" alt="Dark Blue" style="width:50px; height:50px" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/covers/brown3l.jpg\', \'Brown\', 400, 400)"><img src="img/photobooks/covers/brown3.jpg" alt="Brown" style="width:50px; height:50px" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/covers/darkgrey3l.jpg\', \'Dark Grey\', 400, 400)"><img src="img/photobooks/covers/darkgrey3.jpg" alt="Dark Grey" style="width:50px; height:50px" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/covers/red3l.jpg\', \'Red\', 400, 400)"><img src="img/photobooks/covers/red3.jpg" alt="Red" style="width:50px; height:50px" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image(\'img/photobooks/covers/ivory3l.jpg\', \'Ivory\', 400, 400)"><img src="img/photobooks/covers/ivory3.jpg" alt="Ivory" style="width:50px; height:50px" /></a>
                            </p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/journals/04-templates.gif',
                        'mobileimage' => 'img/journals/images/04-templates.gif',
                        'classes' => 'bookmark',
                        'id' => 'templates',
                        'textclasses' => 'leftside',
                        'heading' => 'Templates',
                        'content' => '<p>Choose from a range of templates - reminder lists, travel memories, contemporary and artistic freeform pages. All designed for a truly customised Journal.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/journals/05-premium.jpg',
                        'mobileimage' => 'img/journals/images/05-premium.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'premium',
                        'textclasses' => '',
                        'heading' => 'Premium touches',
                        'content' => '<p>Upgrade your Journal with personalised hot text stamping. Choose from Gold, Silver or Blind foiling to leave a lasting impression.</p><p>Add a Slip Case to take your Journal to the next level. Or simply add the finishing touch to a truly personalised gift.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Available in 4  sizes to choose from. From 8 x 6" up to 8 x 11".</p><p><strong>8 x 6" Landscape</strong>  - (20.5 x 15cm)</p><p><strong>6 x 8" Portrait</strong>  - (15 x 20.5cm)</p><p><strong>8 x 8" Square</strong>  - (20.5 x 20.5cm)</p><p><strong>8 x 11" Portrait</strong>  - (20.5 x 27.5cm)</p>',
                        'image' => 'img/journals/05-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','190gsm E-Photo Matte paper'],
                        ['6 x 8" <span>(100 pages)</span>','$49.95'],
                        ['8 x 6" <span>(100 pages)</span>','$49.95'],
                        ['8 x 8" <span>(100 pages)</span>','$59.95'],
                        ['8 x 11" <span>(100 pages)</span>','$69.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $11.95','+ $5.00 each'],                    
                ],
            ];
            break;

        case 'notebooks':
            $__pagedata = [
                'title' => 'Notebooks',
                'document_title' => 'Notebooks: Make your own Notebook | albumworks',
                'meta_description' => 'Create your own Notebook with lined or blank pages. Fully customise your Notebook cover with your own photos and text. Fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'NOTEBOOKS',
                    'details' => 'THE DETAILS',
                    'templates' => 'TEMPLATES',
                    'sizes' => 'SIZING',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/notebooks/01-notebook-header.jpg',
                        'heading' => '<em>Premium Journals, Guest Books and Notebooks for every occasion</em>',
                        'subline' => '',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Notebooks',
                        'subline' => '<em>"Keep calm and make a list"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/notebooks/02-notebooks.jpg',
                        'mobileimage' => 'img/notebooks/images/02-notebooks.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Notebooks',
                        'content' => '<p>Bound just like a real book, fully lined with end papers and containing 80gsm notepaper internal pages it has a fully customisable cover. With it\'s small and compact size, this Notebook can be taken almost anywhere, ready to take notes when your are.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/notebooks/03-quality.jpg',
                        'mobileimage' => 'img/notebooks/images/03-quality.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'details',
                        'textclasses' => '',
                        'heading' => 'The details',
                        'content' => '<p>Customise your Notebook cover with a premium matte photocover - add your own photos and text. All Notebooks are complete with 100 pages and printed on acid-free 80gsm writing paper.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/notebooks/04-templates.jpg',
                        'mobileimage' => 'img/notebooks/images/3rd-panel-notebooks.jpg',
                        'classes' => 'bookmark',
                        'id' => 'templates',
                        'textclasses' => 'leftside',
                        'heading' => 'Templates',
                        'content' => '<p>Choose your preferred format - our Journal come with two inside templates. Choose lined pages for taking notes and making lists, or choose blank pages for an empty writing surface.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p><strong>6 x 8"</strong>  - (15 x 20.5cm)</p>',
                        'image' => 'img/notebooks/notebook-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','80gsm WRITING PAPER'],
                        ['6 x 8" <span>(100 pages)</span>','$29.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $11.95','+ $5.00 each'],                    
                ],

            ];
            break;

        case 'guest-books':
            $__pagedata = [
                'title' => 'Guest Books',
                'document_title' => 'Guest Books: Fully personalised Guest Books | albumworks',
                'meta_description' => 'Use your own photos to make your very own personalised Guest Book with a stunning material cover. Perfect for weddings, birthdays and all celebrations. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'GUEST BOOKS',
                    'templates' => 'TEMPLATES',
                    'quality' => 'QUALITY',
                    'sizes' => 'SIZING',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'image' => 'img/signature/01-signature-header.jpg',
                        'heading' => '<em>Premium Journals, Guest Books and Notebooks for every occasion</em>',
                        'subline' => '',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Guest Books',
                        'subline' => '<em>"The best thing to hold onto in life is each other"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/signature/02-signaturebooks.jpg',
                        'mobileimage' => 'img/signature/images/02-signaturebooks.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Guest Books',
                        'content' => '<p>Beautifully bound with genuine gold or silver foiling or blind debossing on the cover, the Guest Book is designed to capture all the thoughts of your guests.  Every page is customisable.  Add photos, add signing pages all in full colour.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/signature/03-templates.gif',
                        'mobileimage' => 'img/signature/images/03-templates.gif',
                        'classes' => 'leftimg bookmark',
                        'id' => 'templates',
                        'textclasses' => '',
                        'heading' => 'Templates',
                        'content' => '<p>Templates for Engagements, Christenings, bar Mitzvahs and of course Weddings, you can personalise each and every page in either black and white or full colour - from text to photos, each page is yours to make your own</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/signature/04-quality.jpg',
                        'mobileimage' => 'img/signature/images/04-quality.jpg',
                        'classes' => 'bookmark',
                        'id' => 'quality',
                        'textclasses' => 'leftside',
                        'heading' => 'Quality',
                        'content' => '<p>Printed on premium 190gsm E-Photo Matte paper, it offers the perfect compromise between great colour reproduction and an excellent writing surface.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Available in 5 versatile sizes. From 10 x 8" up to 16 x 12" in both landscape and portrait.</p><p><strong>8 x 6" Landscape</strong>  - (20.5 x 15cm)</p><p><strong>8 x 8" Square</strong>  - (20.5 x 20.5cm)</p><p><strong>11 x 8.5" Landscape</strong>  - (27.5 x 22cm)</p><p><strong>12 x 12" Square</strong>  - (30 x 30cm)</p><p><strong>16 x 12" Landscape</strong>  - (40.5 x 30cm)</p><p>All sizes come with 40 pages. Page count is set and can not be altered.</p>',
                        'image' => 'img/signature/signature-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','190gsm E-PHOTO MATTE PAPER'],
                        ['8 x 6" <span>(40 pages)</span>','$64.95'],
                        ['8 x 8" <span>(40 pages)</span>','$84.95'],
                        ['11 x 8.5" <span>(40 pages)</span>','$94.95'],
                        ['12 x 12" <span>(40 pages)</span>','$109.95'],
                        ['16 x 12" <span>(40 pages)</span>','$139.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['8 x 6"','+ $11.95', '+ $5.00'],
                    ['8 x 8"','+ $11.95', '+ $5.00'],
                    ['11 x 8.5"','+ $11.95', '+ $5.00'],
                    ['12 x 12"','+ $11.95', '+ $5.00'],
                    ['16 x 12"','+ $16.95', '+ $8.00'],
                ],
            ];
            break;

        case 'luggage-tags':
            $__pagedata = [
                'title' => 'Luggage Tags',
                'document_title' => 'Luggage Tag: Customise your own Luggage Tag | albumworks',
                'meta_description' => 'Create your own Luggage Tag using your own photos and text. Use one or multiple photos. Let your luggage stand out with a pesonalised Luggage Tag! Easy to use, free software. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'LUGGAGE TAGS',
                    'templates' => 'TEMPLATES',
                    'quality' => 'QUALITY',
                    'sizes' => 'SIZING',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'image' => 'img/luggage/01-luggage-header.jpg',
                        'heading' => '<em>Premium photo products for your travel needs.</em>',
                        'subline' => '',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Luggage Tags',
                        'subline' => '<em>"Travel the road more travelled"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/luggage/02-luggage.jpg',
                        'mobileimage' => 'img/luggage/images/02-luggage.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Luggage Tags',
                        'content' => '<p>Luggage tags are a tough and durable way to personalise your luggage. Your photos and design are directly bonded to a 1.5mm thick aluminium plate - it\'s strong. Each luggage tag has a slot for a luggage strap and comes supplied with a leather-like strap with buckle.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/luggage/03-templates.jpg',
                        'mobileimage' => 'img/luggage/images/03-templates.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'templates',
                        'textclasses' => '',
                        'heading' => 'Templates',
                        'content' => '<p>Templates make it easy to create a design that is unique, include photos, text, phone numbers but most importantly create something which stands out on the luggage carousel.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/luggage/04-quality.jpg',
                        'mobileimage' => 'img/luggage/images/04-quality.jpg',
                        'classes' => 'bookmark',
                        'id' => 'quality',
                        'textclasses' => 'leftside',
                        'heading' => 'Quality',
                        'content' => '<p>The bonding process used to produce your luggage tag means that it won\'t chip, crack or fade. </p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Choose a small or large Luggage Tag in both portrait or landscape orientation.</p><p><strong>1.7 x 3.5"</strong>  - (4.5 x 9cm)</p><p><strong>2.5 x 4.25"</strong>  - (6.5 x 11cm)</p>',
                        'image' => 'img/luggage/05-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','LUGGAGE TAG'],
                        ['1.7 x 3.5"','$9.95'],
                        ['2.5" x 4.25"','$13.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $3.00'],
                ],
            ];
            break;

        case 'tote-bags':
            $__pagedata = [
                'title' => 'Travel Tote Bags',
                'document_title' => 'Tote Bag: Customise your own Tote Bag | albumworks',
                'meta_description' => 'Make a personalised Tote Bag using your own photos and text. Use one or multiple photos. The perfect travel companion. Easy to use, free software. 100% design freedom, fast delivery and local support.',
                'createlink' => $page.'-create-now',
                'menu' => [
                    'overview' => 'TRAVEL TOTE BAGS',
                    'quality' => 'QUALITY',
                    'templates' => 'TEMPLATES',
                    'sizes' => 'SIZING',
                    'pricing' => 'PRICING',
                ],
                'contents' => [
                    [
                        'type' => 'parallax',
                        'textclass' => 'leftside',
                        'image' => 'img/tote/01-tote-header.jpg',
                        'heading' => '<em>Premium photo products for your travel needs.</em>',
                        'subline' => '',
                    ],
                    [
                        'type' => 'bigheader',
                        'id' => '',
                        'heading' => 'Travel Tote Bag',
                        'subline' => '<em>"Travel the road more travelled"</em>',
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/tote/02-totebag.jpg',
                        'mobileimage' => 'img/tote/images/02-totebag.jpg',
                        'classes' => '',
                        'id' => '',
                        'textclasses' => 'leftside',
                        'heading' => 'Travel Tote Bag',
                        'content' => '<p>Make travel personal with your very own Travel Tote Bag.  Personalise the design of your bag using your own photos and text on either 1 or 2 sides.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/tote/03-quality.jpg',
                        'mobileimage' => 'img/tote/images/03-quality.jpg',
                        'classes' => 'leftimg bookmark',
                        'id' => 'quality',
                        'textclasses' => '',
                        'heading' => 'Quality',
                        'content' => '<p>Machine washable, your bag is 15" wide at the top, a spacious 17" wide at the bottom and comes with two handles. It is made from Polyester Cotton and is tough and durable and designed for the road more travelled.</p>'
                    ],
                    [
                        'type' => 'prodpanel',
                        'image' => 'img/tote/04-templates.jpg',
                        'mobileimage' => 'img/tote/images/04-templates.jpg',
                        'classes' => 'bookmark',
                        'id' => 'templates',
                        'textclasses' => 'leftside',
                        'heading' => 'Templates',
                        'content' => '<p>Use one of our handy templates or come up with your own design to fully personalise your Travel Tote Bag.</p>'
                    ],
                    [
                        'type' => 'prodpres',
                        'id' => 'sizes',
                        'classes' => 'bookmark rightimg',
                        'heading' => 'Sizing',
                        'content' => '<p>Our Travel Tote Bags come in 1 size with your choice of a single side design or 2 side design.</p><p><strong>11 x 11"</strong>  - (28 x 28cm)</p>',
                        'image' => 'img/tote/05-sizing.jpg',
                    ]
                ],
                'prices' => [
                    [
                        ['','TRAVEL TOTE BAG'],
                        ['1 SIDE','$22.95'],
                        ['2 SIDES','$29.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $9.95','+ $4.00'],
                ],
            ];
            break;
    }
    return $__pagedata;
}


function fetchRetailPageMetaData($page){
    $__pagedata = [];
    switch($page){
        case 'desk-prints-create-now':
            $__pagedata = [
                'title' => 'Desk Prints',
                'document_title' => 'Desk Prints: Create your own set of Desk Prints | albumworks',
                'meta_description' => '28 high quality photo prints, spiral bound in a handy deskflip. Make online from just $36.95.',
                'images' => [
                    'img/deskprints/desk-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/deskprints/desk-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/deskprints/desk-6x8.jpg' => ['alt' => '', 'imgid' => 'AWHDPF_CPD68'],
                    'img/deskprints/desk-11x5.jpg' => ['alt' => '', 'imgid' => 'AWHDPF_CLD115'],
                ],
                'products' => [
                    'AWHDPF_CPD68' => [
                        'price' => '36.95',
                        'pages' => '28',
                        'paper' => '260gsm Photo Luster Paper',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.YTRmMDc3YmI,.XkI1oIt61Igp2RX7fp9UTtFWO90ivrlQyYhkORrnMh0Vm0O3_89f7w,,',
                        'image' => 'AWHDPF_CPD68',
                        'label' => '6 x 8"',
                    ],
                    'AWHDPF_CLD115' => [
                        'price' => '36.95',
                        'pages' => '28',
                        'paper' => '260gsm Photo Luster Paper',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.ZDQ5ZmJiYWI,.3ZjQz56YB-yM1joiT3WrLhg2cfxcF2Z3RboKW38ACLY8tH83ks4yrA,,',
                        'image' => 'AWHDPF_CLD115',
                        'label' => '11 x 5"',
                    ],
                ],
                'options' => [
                    'pages' => 'Pages',
                    'paper' => 'Paper',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-07') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF DESK PRINTS WITH VOUCHER CODE:',
                    'promocode' => 'DESK40'
                ] : ''),
                'about' => 'Fill this desktop companion with your precious photos. Printed in stunning High Definition on 250gsm Glossy photo paper.<br><br>With 28 pages, you can design each page yourself to create a one-of-a-kind memento.',
                'prices' => [
                    [
                        ['','DESK PRINTS'],
                        ['6 x 8"','$36.95'],
                        ['11 x 5"','$36.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00 each'],
                ],
            ];
            break;

        case 'desk-calendar-create-now':
            $__pagedata = [
                'title' => 'Desk Calendar',
                'document_title' => null,
                'meta_description' => null,
                'images' => [
                    'img/deskcalendar/20190903/desk-retail.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/deskcalendar/20190903/10x5-SD.jpg' => ['alt' => '', 'imgid' => '10x5'],
                    'img/deskcalendar/20190903/11x5-HD.jpg' => ['alt' => '', 'imgid' => '11x5'],
                ],
                'products' => [
                    '10x5' => [
                        'price' => '27.95',
                        'url' => '#',
                        'image' => '10x5',
                        'label' => '10 x 5" - Standard Colour',
                        'paper' => 'Standard Colour 300gsm SOVEREIGN SILK'
                    ],
                    '11x5' => [
                        'price' => '37.95',
                        'url' => '#',
                        'image' => '11x5',
                        'label' => '11 x 5" - Hi Colour',
                        'paper' => 'Hi Colour 260gsm Photo Luster'
                    ],
                ],
                'extras' => [
                    'design' => [
                        'Smart Calendar' => 0,
                        'Classic Compact Grid' => 0,
                        'Classic Large Grid' => 0,
                    ],
                ],
                'urls' => [
                    'formula' => 'size|design',
                    'values' => [
                        '10 x 5" - Standard Colour|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.YjI0MmUwYjk.cM597imK_SfqaLjf3Y0fhDlMh7Jf8snGjzAtvKhY0jPI2EiSCNMj2vj8ygX_GoFypt7Ttm-KrU0,,',
                        '10 x 5" - Standard Colour|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.NWZkYjczOWE.2ryhyXVe9ScC5VHyzJN5qi_ZGjD3Yarqle8RKlUDqhdg2TuzEV9BVJDXvcIcieIR9UyRLkKG9U0-ET3otfedTPnCMXe7Qv0k',
                        '10 x 5" - Standard Colour|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.OTU4MWYzMDg.Bz5DpU1Pc_wjGw2Wy-j7qHfHv_3YkQuKmWnZqV6ofE49a1Xu5DBvo_qiZXHpultgL_BxCn8bmV10XVrObtVlwva7AfPhzhnJ',
                        '11 x 5" - Hi Colour|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.MzIyZDE5MmY.ljKdk7MqjdMfjmfRV_01aLm8mB_kZEaaRg6IkqJkcz0ZJfsvp20JzI2hQA4zTO9ZU-Nn6D-Lxog',
                        '11 x 5" - Hi Colour|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.OGU4ZjhlZTI.7nUxANq5tEPOhcvoEblHkTjB3X7-ZnrARHJqFstbcCvx7Rng8mHdrneMApGYdOFOCt4KtpwyH7tAsPjeiDtD-LuxOy7xE8hS',
                        '11 x 5" - Hi Colour|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.ZTFmYjcyMzM.cUlHsQCm7frVTq2bEcUcVUvvmxFPb8J1uBpioIhVxs_RlzNLMuHNfAmX4DK5upmSP2LzHYg7kzNvraaWzNW9aOSStaJuTueV',
                    ]
                ],
                'options' => [
                    'design' => 'Design',
                    // 'pages' => 'Pages',
                     'paper' => 'Paper',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-04') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF CALENDARS WITH VOUCHER CODE:',
                    'promocode' => 'CALENDAR40'
                ] :  ((date('Y-m-d') == '2017-12-12') ? [
                    'message' => 'GET <strong>2 FOR 1</strong> CALENDARS WITH VOUCHER CODE:',
                    'promocode' => '2FOR1CAL'
                ] : '')),
                'about' => 'A handy 12 month Desktop Calendar filled with your precious photos and printed on premium photo papers.<br><br>Spiral wire bound with a convenient black tent stand to stand freely on your desk. Personalise with your own images and text.<br>13 pages in Standard Definition (single sided)<br>14 pages in High Definition (double sided)',
                'prices' => [
                    [
                        ['','Standard Colour<br>300gsm SOVEREIGN SILK','Hi Colour<br>260gsm PHOTO LUSTER'],
                        ['10 x 5”','$27.95','-'],
                        ['11 x 5"','-','$37.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['10 x 5”','+ $12.95','+ $3.00'],
                    ['11 x 5','+ $12.95','+ $3.00'],
                ],
            ];
            break;

        case 'wall-calendar-create-now':
            $__pagedata = [
                'title' => 'Wall Calendar',
                'document_title' => null,
                'meta_description' => null,
                'images' => [
                    'img/wallcalendar/20190903/wall-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/wallcalendar/20190903/wall-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/wallcalendar/20190903/11x8_03.jpg' => ['alt' => '', 'imgid' => '11x8'],
                    'img/wallcalendar/20190903/12x12_03.jpg' => ['alt' => '', 'imgid' => '12x12'],
                ],
                'products' => [
                    '11x8-SD' => [
                        'pages' => '28 (12 months)',
                        'price' => '27.95',
                        'url' => '#',
                        'image' => '11x8',
                        'label' => '11 x 8" - Standard Colour',
                        'paper' => 'Standard Colour 300gsm SOVEREIGN SILK',
                    ],
                    '11x8-HD' => [
                        'pages' => '28 (12 months)',
                        'price' => '37.95',
                        'url' => '#',
                        'image' => '11x8',
                        'label' => '11 x 8" - Hi Colour',
                        'paper' => 'Hi Colour 260gsm Photo Luster',
                    ],
                    '12x12-SD' => [
                        'pages' => '28 (12 months)',
                        'price' => '37.95',
                        'url' => '#',
                        'image' => '12x12',
                        'label' => '12 x 12" - Standard Colour',
                        'paper' => 'Standard Colour 300gsm SOVEREIGN SILK',
                    ],
                    '12x12-HD' => [
                        'pages' => '28 (12 months)',
                        'price' => '49.95',
                        'url' => '#',
                        'image' => '12x12',
                        'label' => '12 x 12" - Hi Colour',
                        'paper' => 'Hi Colour 260gsm Photo Luster',
                    ],
                ],
                'extras' => [
                    'design' => [
                        'Smart Calendar' => 0,
                        'Classic Compact Grid' => 0,
                        'Classic Large Grid' => 0,
                    ],
                ],
                'urls' => [
                    'formula' => 'size|design',
                    'values' => [
                        '12 x 12" - Standard Colour|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.OTQxMmFlYzg.srwP4h6saZ6EZENL1L8ZxSbjc2_N1WcUdPrJaCNNdjPrps6ED_piVl4jK8_H1Sn3nj5CdvnwKL3Rm8r5RVOX7w,,',
                        '12 x 12" - Standard Colour|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=67.Njg0OGRkM2U.3RBgE3DTQfCGwK3elUykPLQeBtajS8mtAWyLe28mRn2IiZ8dSC4CQcgbJxD4UP_fJfTITjoj6KYhzH3cixFAC6ROm1mKxgxy',
                        '12 x 12" - Standard Colour|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=67.NDQ2MGYzMjc.Sdjlw7a_7f0OI-lryNSBNamS_ib2KYS3_Wdc8B-sR-dkoyv7fYTcwCO6noRRykerz3330f1DE3RAujjBj8PcEZBRBSV4q7Bf',
                        '12 x 12" - Hi Colour|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.Mzk5MzdhNzM.NHuqTERBzsA06IaGu-7bacZ99xlP4__DxjHH6ALOH_LCQ-lhUxYWar0JUtk7OQcAM4C-iT1GnhQ9mH716cYiog',
                        '12 x 12" - Hi Colour|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=67.MjNlMDdhYTI.MyIN2aWU5gwEehf1Oj0SsosSh8y57TBkIu2O3izr2CZhe-J1y7XFG24Q6hEulqgjDrfzcB1UvSZ5983VuXYbAvDj2T6iG-Cw',
                        '12 x 12" - Hi Colour|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=67.ZmEwYmE4Mjg.eMr3r1zZZoXHjBAoThwlPQ-MhRsZMcEr09waOEs8QKPl2EM0Im0_KC59r9CnM_tV3DlgcyTtEYG0DXsERyCECRwf7b-oBtFy',
                        '11 x 8" - Standard Colour|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.ZTJmMDBhNDk.K3PdblbvvekkxywdTYO40F8XmXRpe7Vc5QI3twfZQNDOnJEmnY4J7Uak9z6uZzznBVRz6_6XV0efMmYqDc7c_A,,',
                        '11 x 8" - Standard Colour|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=66.OGRhNzc5ZGM.Fh3kTkJFB0aHSlUxlE8vN0P7KpVa_42BibA4DIZ4uTsDf9X5GhIJJalLCPW_zsun27ImxgZIQdWMMx8Uu056ruVs6g5xR2GV',
                        '11 x 8" - Standard Colour|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=66.Y2FkNTYxMGE.0wfk8cMVVJ9Cptp8PrmtuAHygj72VryH-IGu0dGQNtV1ufDJ9w4-ge4uxa6oj-geJWn4Ifv-Qi8KO_G3iQfD0naxOI4uzwLJ',
                        '11 x 8" - Hi Colour|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NDc4MjI2ODU.Htlg_2x9_mpE6Dc5_SGepmeCB_jXCIS2jXKkiWYVkPzHESpli088ftdGQkB0cQYFbkpFy7Pq7ew',
                        '11 x 8" - Hi Colour|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.NmYxMTYwZWI._ccj1iMOvqZl93oc-0buDlt28kBCaBKEjrwcPiVhqLi-3Zomm0uZ5IsnWwhH_J1qWShK1IRV-ZHusmyJJc9GUi8Zova1FJpa',
                        '11 x 8" - Hi Colour|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.MmYxZmIwN2M.hJXpvuKPXQy5XcwiDIXaPlXqgxRyXZ0w2iCQFEzSopVP9Y2nAjUHCCsGu5xmSmgZRy9wTHX0rrH8gcYIfuKUwyZ28AcXcNEt',
                    ]
                ],
                'options' => [
                    'design' => 'Design',
                    'paper' => 'Paper',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-04') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF CALENDARS WITH VOUCHER CODE:',
                    'promocode' => 'CALENDAR40'
                ] :  ((date('Y-m-d') == '2017-12-12') ? [
                    'message' => 'GET <strong>2 FOR 1</strong> CALENDARS WITH VOUCHER CODE:',
                    'promocode' => '2FOR1CAL'
                ] : '')),
                'about' => 'Beautiful 12 month personalised Wall Calendar filled with your precious photos and printed on premium photo papers.<br><br>Spiral wire bound, opens to a beautiful double page spread featuring your photos on top and calendar grid on bottom. Conveniently placed hole for easy wall hanging.',
                'prices' => [
                    [
                        ['','Standard Colour<br>300gsm SOVEREIGN SILK','Hi Colour<br>260gsm PHOTO LUSTER'],
                        ['11 x 8"','$27.95','$37.95'],
                        ['12 x 12"','$37.95','$49.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['11 x 8"','+ $12.95','+ $3.00'],
                    ['12 x 12"','+ $12.95','+ $3.00'],
                ],
            ];
            break;

        case 'canvas-photo-prints-create-now':
            $__pagedata = [
                'title' => 'Canvas Prints',
                'document_title' => 'Canvas Prints: Create your own Canvas print | albumworks',
                'meta_description' => 'Create a Canvas print using your very own photos and text. Top quality material, available in a range sizes. Make in an instant from just $49.95.',
                'images' => [
                    'img/canvas/canvas-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/canvas/canvas-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/canvas/canvas-03.jpg' => ['alt' => '', 'imgid' => ''],
                ],
                'categories' => [
                    'code' => 'orientation',
                    'label' => 'Orientation',
                    'values' => [
                        [
                            'code' => 'landscape',
                            'label' => 'Landscape',
                            'products' => [
                                '18x12' => [
                                    'price' => '59.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.YWY4ZGMzNmM.LeAL9RM0nMB_MMxxpuwkWXSqjwlV1VnXkue1kcyjgLq-ttnBXBS49B3zUNGq4qTkunjTtoz_SVY',
                                    'newimage' => 'img/canvas/lanscape.jpg',
                                    'label' => '18 x 12"',
                                ],
                                '20x16' => [
                                    'price' => '89.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.M2VjNjM0MmQ.GgY5LUAwZG328iBdBGufgPxD5Bkt7jQxHIjQSYeSAiLYVpOXSUcoX3zMR--DdckN3bs2UNsaq3Y',
                                    'newimage' => 'img/canvas/lanscape.jpg',
                                    'label' => '20 x 16"',
                                ],
                                '30x20' => [
                                    'price' => '169.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.NDI4OTUxYjQ.nTh_4gCHmzbtAc9BSoUXoiqlUACJz9dpu6-QH2FatleEb-sD5TBQCXH9yZz9BV7UcTEPre06sIw',
                                    'newimage' => 'img/canvas/lanscape.jpg',
                                    'label' => '30 x 20"',
                                ],
                                '40x30' => [
                                    'price' => '199.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ZWJlNTRmNGU.RY1aWcgDW7redJnbI2fi1LrVqTLm1AX-Z7iXV7gYr1B5n_MVJFLZuwNbCS_qmWGOUjyDR2-wvu4',
                                    'newimage' => 'img/canvas/lanscape.jpg',
                                    'label' => '40 x 30"',
                                ],
                            ],
                        ],
                        [
                            'code' => 'portrait',
                            'label' => 'Portrait',
                            'products' => [
                                '12x18' => [
                                    'price' => '59.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.MDFmMjkzMTE.MvcTnuIQTV_P4_hrW0dsuJb8tNlIAOKTJvw2lazpLI2YolfB7j_dQMQf9zs4bCfAazkmEqqT0P0',
                                    'newimage' => 'img/canvas/portrait.jpg',
                                    'label' => '12 x 18"',
                                ],
                                '16x20' => [
                                    'price' => '89.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.OTM5MTg4MzM.rcdBr3JRrnwn6LqXr3Pd6IDjYDipWRy1WGkA-IxdHE0-yISZ8kR1cNAyMM3Z_edd7wfVPaqJlEA',
                                    'newimage' => 'img/canvas/portrait.jpg',
                                    'label' => '16 x 20"',
                                ],
                                '20x30' => [
                                    'price' => '169.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.NzNiMWU4NGE.sqE-0DTyEmPxwqVktlAipFlt70atL_VadUjg0YwnzfhLN7tEqCeGv7PlHloX6GMu93VSLec1Miw',
                                    'newimage' => 'img/canvas/portrait.jpg',
                                    'label' => '20 x 30"',
                                ],
                                '30x40' => [
                                    'price' => '199.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.NzJjMDVhMjE.MuGu2gUIZBgLLRAk9wO-SzlFJJAq0eVjPnz-dlGzZuYueZtjOOUWOxeroCpyBf4-n9e8hLckNxs',
                                    'newimage' => 'img/canvas/portrait.jpg',
                                    'label' => '30 x 40"',
                                ],
                            ],
                        ],
                        [
                            'code' => 'square',
                            'label' => 'Square',
                            'products' => [
                                '12x12' => [
                                    'price' => '49.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.OWJhOGE5ODQ.9pD-xwi39itG79H6Be23O6SQmSBIekrw6dss-mfIg3CXJUNIRzbCxf-aZhG-JRXaq3_lkX1fX2Y',
                                    'newimage' => 'img/canvas/square.jpg',
                                    'label' => '12 x 12"',
                                ],
                                '16x16' => [
                                    'price' => '69.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.MzcwNWRkZjE._U8MsWy7vk2iuxhuss0en4lXfHfnq4p00gsJDk3UMAvir8jp5Acv2QmVJEGyF0MFX-_bCvH1vbs',
                                    'newimage' => 'img/canvas/square.jpg',
                                    'label' => '16 x 16"',
                                ],
                                '24x24' => [
                                    'price' => '154.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.ZDMwMjkzYWY.7C7uIJ5p9PAVK9TbNNZPQXWzv5FKCD6Red0lHi3jb3rKi_HbyLtBLoQR6RlZ9pY0nhnQqhrS6g8',
                                    'newimage' => 'img/canvas/square.jpg',
                                    'label' => '24 x 24"',
                                ],
                            ],
                        ],
                        [
                            'code' => 'panorama',
                            'label' => 'Panorama',
                            'products' => [
                                '24x12' => [
                                    'price' => '89.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ZWM3MTcyZmI.vvNP6ZwqIJ1RnrNzb2WuifwTIUZNXm5GzwOl_qXRGL_AIg1UedQsNxnKw4dNACM4iHBmui2N3wk',
                                    'newimage' => 'img/canvas/panorama.jpg',
                                    'label' => '24 x 12"',
                                ],
                                '40x20' => [
                                    'price' => '199.95',
                                    'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.Y2IwNzM5MWE.ktNbhJ2iyc0wZAohrUXwoa4CuRuUZipHONe2ZV80L_sy266HjVJhAGaOvwBx2XTMEY_-D0wpxkk',
                                    'newimage' => 'img/canvas/panorama.jpg',
                                    'label' => '40 x 20"',
                                ],
                            ],
                        ],
                    ]
                ],
                'extras' => [
                    // 'images' => [
                    //     '1' => 0,
                    // ],
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-02') ? [
                    'message' => 'SAVE <strong>50%*</strong> OFF CANVAS PRINTS WITH VOUCHER CODE:',
                    'promocode' => 'CANVAS50'
                ] : ((date('Y-m-d') == '2017-12-09') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF CANVAS PRINTS WITH VOUCHER CODE:',
                    'promocode' => 'CANVAS40'
                ] : ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''))),
                'post_messages' => [
                    'images' => 'Want to add more images? Add text to your designs? Download our desktop Editor for 100% design freedom.',
                ],
                'options' => [
                    // 'images' => 'Images',
                    'price' => 'Price',
                ],
                'about' => 'Your photos have never looked so amazing!<br><br>Printed on high quality poly-cotton canvas with a satin finish. Carefully hand stretched over a solid 3cm timber frame. Stunning colour reproduction lets you create your own gallery from your favourite photos. Available in a range of orientations and sizes.',
                'prices' => [
                    [
                        ['','LANDSCAPE/PORTRAIT'],
                        ['12 x 18"','$59.95'],
                        ['16 x 20"','$89.95'],
                        ['20 x 30"','$169.95'],
                        ['30 x 40"','$199.95'],
                    ],
                    [
                        ['','SQUARE'],
                        ['12 x 12"','$49.95'],
                        ['16 x 16"','$69.95'],
                        ['24 x 24"','$154.95'],
                    ],
                    [
                        ['','PANORAMA'],
                        ['24 x 12"','$89.95'],
                        ['40 x 20"','$199.95'],
                    ],
                ],
                'shipping' => [
                    ['','ITEMS UP TO 30”','EXTRA ITEMS UP TO 30"', 'ITEMS OVER 30"', 'EXTRA ITEMS OVER 30"'],
                    ['EXPRESS SHIP','+ $24.95', '+ $14.95 ea', '+ $44.95', '+ $34.95 ea'],
                ],
                'below_shipping' => '<p>* Note: Canvas Prints cannot be shipped to a PO Box.</p>'
            ];
            break;


        case 'framed-prints-create-now':
            $__pagedata = [
                'title' => 'Framed Prints',
                'document_title' => 'Framed Prints: Create your own Framed Print | albumworks',
                'meta_description' => 'Create Framed Prints using your very own photos. Different frame and layout options available. Printed on quality photo papers. Make in an instant from just $46.95.',
                'images' => [
                    'img/framed/create/retail-1.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/framed/create/retail-2.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/framed/create/8x12-portrait-5x7.jpg' => ['alt' => '', 'imgid' => '5x7'],
                    'img/framed/create/8x12-portrait-6x8.jpg' => ['alt' => '', 'imgid' => '6x8'],
                    'img/framed/create/11x15-portrait-8x10.jpg' => ['alt' => '', 'imgid' => '8x10'],
                    'img/framed/create/11x15-portrait-8x12.jpg' => ['alt' => '', 'imgid' => '8x12'],
                    'img/framed/create/12x8-landscape-7x5.jpg' => ['alt' => '', 'imgid' => '7x5'],
                    'img/framed/create/12x8-landscape-8x6.jpg' => ['alt' => '', 'imgid' => '8x6'],
                    'img/framed/create/15x11-landscape-10x8.jpg' => ['alt' => '', 'imgid' => '10x8'],
                    'img/framed/create/15x11-landscape-12x8.jpg' => ['alt' => '', 'imgid' => '12x8'],
                ],
                'products' => [
                    '5x7' => [
                        'price' => '41.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.OWM1ZDAyN2Y.Cx1jpqbNWVVoj3Ieq7rMjfvuEBN5fxcILnBgZIdrN4OQruEzL40zm0tmP0b7srq36v5xJuBrb_A',
                        'image' => '5x7',
                        'label' => '5x7" Portrait',
                        'paper' => '240gsm Ultra Luster',
                    ],
                    '6x8' => [
                        'price' => '49.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.YzQxMGNmNzg.fpqyjEJ5DT6AzUpTjpv1kBej7wufNYbBNI5jbfwrPjojF0vGNk4P48BUrhTzc0EvVnA-7shxMY0',
                        'image' => '6x8',
                        'label' => '6x8" Portrait',
                        'paper' => '240gsm Ultra Luster',
                    ],
                    '8x10' => [
                        'price' => '71.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.NjhmOThlOGU.Td2q9KmFNlMZrDxX982iHml9fvjXxqN1x84GarE4iu-caEEPOL9sk4P895K1Dfe2Qx8tTIxymjk',
                        'image' => '8x10',
                        'label' => '8x10" Portrait',
                        'paper' => '240gsm Ultra Luster',
                    ],
                    '8x12' => [
                        'price' => '79.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.NzBkNjI4N2M.Y3s8yKHfKs5S4Qa6Bcabp506XAb86slgDRs1rxKzvWZwuz4G-8Ig7XmwA48azpeuWn4nlg5wMVs',
                        'image' => '8x12',
                        'label' => '8 x 12" Portrait',
                        'paper' => '240gsm Ultra Luster',
                    ],
                    '7x5' => [
                        'price' => '41.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.OTc5ZGRmOGU.7Az24FOXiup7OUJ7dIG4dAaNI-SCdYMCCETdQE8VHLf-UrmRlx15KCwfUF01olmju3Yuhh2YrHo',
                        'image' => '7x5',
                        'label' => '7 x 5" Landscape',
                        'paper' => '240gsm Ultra Luster',
                    ],
                    '8x6' => [
                        'price' => '49.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.YWNhMmU0OTI.J9HPK181ZjD2jR29dkNqwdXfFjl56OpMwRs2JfwP0WmNoD5jAm3iLoytTO59paxLGDIa5rYpu40',
                        'image' => '8x6',
                        'label' => '8 x 6" Landscape',
                        'paper' => '240gsm Ultra Luster',
                    ],
                    '10x8' => [
                        'price' => '71.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.YzFjNTdmNjQ.JsyNaFsWxEZhSwi0hSpPNP-qGJrPmouWCOWmP_YOHA-Th8CmFroiKLQeOpYVK2IAWUVIpwzE6Hg',
                        'image' => '10x8',
                        'label' => '10 x 8" Landscape',
                        'paper' => '240gsm Ultra Luster',
                    ],
                    '12x8' => [
                        'price' => '79.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.Y2FlYzRhMjA.hM9JYnqSvZgHqHIeSYjWCZAUQMEaUHQ8tUjkIp1m9b7r801gacHGXo4rW_q7XvGInC_q9gjRrlI',
                        'image' => '12x8',
                        'label' => '12 x 8" Landscape',
                        'paper' => '240gsm Ultra Luster',
                    ],
                ],
                'extras' => [
                    'colours' => [
                        'type' => 'image',
                        'src' => 'img/framed/colours.jpg'
                    ]
                ],
                'options' => [
                    'colours' => 'Frame Colour',
                    'price' => 'Price',
                ],
                'flashpromotion' => '',
                'about' => 'Your photos have never looked so good!<br><br>Top quality framed print with classic black wooden frame. Showcase a single photo or create a whole collage using your own photos and text. Landscape or portrait frame with mat board - all Framed Prints come with a convenient hook to hang on your wall and a handy desk stand to sit upright on tables.',
                'prices' => [
                    [
                        ['','240gsm LUSTER'],
                        ['15 x 11" (12 x 8" PRINT)','$79.95'],
                        ['15 x 11" (10 x 8" PRINT)','$71.95'],
                        ['12 x 8" (8 x 6" PRINT)','$49.95'],
                        ['12 x 8" (7 x 5" PRINT)','$41.95'],
                    ]
                ],
                'shipping' => [
                    ['','EXPRESS<br>SHIP','EXTRA ITEM EXPRESS'],
                    ['ALL SIZES','+ $14.95','+ $10.00'],
                ],
            ];
            break;

        case 'instagram-prints-create-now':
            $__pagedata = [
                'title' => 'Instagram Prints',
                'document_title' => 'Instagram Prints: Create your own Instagram Prints | albumworks',
                'meta_description' => 'Turn your Instagram and phone photos into stylish printed photo cards. Easy to make and only $9.95.',
                'images' => [
                    'img/instagram/insta-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/instagram/insta-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/instagram/insta-03.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/instagram/insta-04.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/instagram/petite.jpg' => ['alt' => '', 'imgid' => 'petite'],
                    'img/instagram/mini.jpg' => ['alt' => '', 'imgid' => 'mini'],
                    'img/instagram/square.jpg' => ['alt' => '', 'imgid' => 'square'],
                    'img/instagram/tall.jpg' => ['alt' => '', 'imgid' => 'tall'],
                    'img/instagram/large.jpg' => ['alt' => '', 'imgid' => 'large'],
                ],
                'products' => [
                    'petite' => [
                        'paper' => '270gsm Uncoated Matte',
                        'quantity' => '34',
                        'price' => '9.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.YjRlMzdmYjE,.WhHZjS3IcuBHKh7b75yhMcINAYAy4IVvUyBgJzt2nICEOwzPeYelwQ,,',
                        'image' => 'mini',
                        'label' => 'Mini - 2.1 x 2.1”',
                    ],
                    'mini' => [
                        'paper' => '270gsm Uncoated Matte',
                        'quantity' => '24',
                        'price' => '9.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.NDhiMTk1NGY,.id9cgOTEkeaUw_rqo02agWSdvWXEYVjYBqsxf8LKsZIoZtXedsRPzQ,,',
                        'image' => 'petite',
                        'label' => 'Petite - 2.1 x 3.3”',
                    ],
                    'square' => [
                        'paper' => '270gsm Uncoated Matte',
                        'quantity' => '23',
                        'price' => '9.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.ZGI3ZmE1Mzk,.hjbOaTyS3HhFZqKck33OJFWDQyfP5bB-F7vLPFEDomS1w4tF3UNIWg,,',
                        'image' => 'square',
                        'label' => 'Square - 4 x 4”',
                    ],
                    'tall' => [
                        'paper' => '270gsm Uncoated Matte',
                        'quantity' => '14',
                        'price' => '9.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.OGMyMDIyZjY,.g38vzvGgX-PgrwaZBfopCDVOhVP3MAqaz8o3_LykmiezAnYYnWfYwA,,',
                        'image' => 'tall',
                        'label' => 'Tall - 2.1 x 5.5”',
                    ],
                    'large' => [
                        'paper' => '270gsm Uncoated Matte',
                        'quantity' => '17',
                        'price' => '9.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.MmI3ZWY0MzM,.wrJdn0jh1r8t7VcYlu3N-TWPnV0KUenew-8eKTRUBNIq-htGuvp-fg,,',
                        'image' => 'large',
                        'label' => 'Large - 3.7 x 5.5”',
                    ],
                ],
                'options' => [
                    'paper' => 'Paper Type',
                    'quantity' => 'Quantity',
                    'price' => 'Price',
                ],
                'about' => 'Turn any of your photos into a set of stylish Photo Cards.<br><br>Import your photos from Instagram or upload any photos directly from your computer. Printed on 270gsm Uncoated Smooth paper, prints have a stationery texture and a subtle weave to the paper.',
                'prices' => [
                    [
                        ['','270gsm UNCOATED MATTE'],
                        ['2.1 x 2.1" (34 pcs)','$9.95'],
                        ['2.1 x 3.3" (24 pcs)','$9.95'],
                        ['4 x 4" (23 pc)','$9.95'],
                        ['2.1 x 5.7"  (14 pcs)','$9.95'],
                        ['3.7 x 5.5"  (17 pcs)','$9.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00'],
                ],
            ];
            break;

        case 'photo-magnets-create-now':
            $__pagedata = [
                'title' => 'Photo Magnets',
                'document_title' => 'Photo Magnets: Create your own Photo Magnets | albumworks',
                'meta_description' => 'Make a set of Photo Magnets using your very own photos. Available in 6x4 or 7x5. Make in an instant from just $5.95.',
                'images' => [
                    'img/magnets/magnet-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/magnets/6x4.jpg' => ['alt' => '', 'imgid' => '6x4'],
                    'img/magnets/7x5.jpg' => ['alt' => '', 'imgid' => '7x5'],
                ],
                'products' => [
                    '6x4' => [
                        'price' => '14.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=27.NGMyOTMwOGI,._fK1_lIo_UsZDqFLjdSo7ze0Y2Os2VnKrQktaBhye2o,',
                        'image' => '6x4',
                        'label' => '6 x 4”',
                        'quantity' => 3,
                    ],
                    '7x5' => [
                        'price' => '14.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=25.ZTNiNTc4MGQ,.gYKiQR_Wu8CqpFepEZqfX3okoLqkgS7hy-qjhCnJis0,',
                        'image' => '7x5',
                        'label' => '7 x 5”',
                        'quantity' => 2,
                    ],
                    '4x6' => [
                        'price' => '14.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=27.OTljZTJjNzI,.IuskY_DETqy_2BImWycqTVOnUUyBTzsnTUqDqhEnCYk,',
                        'image' => '6x4',
                        'label' => '4 x 6”',
                        'quantity' => 3,
                    ],
                    '5x7' => [
                        'price' => '14.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=25.NmMxZGY2YTI,.DML4SEydyMye88uSWNvidDEo8T92-4PcFv1pgu-ag8Y,',
                        'image' => '7x5',
                        'label' => '5 x 7”',
                        'quantity' => 2,
                    ],
                ],
                'options' => [
                    'quantity' => 'Quantity',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Display your photos on the fridge with our magnetic Photo Magnets.<br><br>Create full frame photos or choose from a choice of borders to reflect your style. Finished with a matte, non-reflective surface. Printed on 0.4mm magnet stock.',
                'prices' => [
                    [
                        ['','PHOTO MAGNET'],
                        ['6 x 4" (3pcs)','$14.95 (extra piece = $4.95)'],
                        ['7 x 5" (2pcs)','$14.95 (extra piece = $6.95)'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00'],
                ],
            ];
            break;

        case 'poster-printing-create-now':
            $__pagedata = [
                'title' => 'Premium Photo Posters',
                'document_title' => 'Premium Photo Posters: Create your own Poster | albumworks',
                'meta_description' => 'Create a poster using your very own photos. Premium paper options in a range of sizes. Make online in an instant from just $21.95',
                'images' => [
                    'img/posters/poster-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/posters/poster-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/posters/16x20.jpg' => ['alt' => '', 'imgid' => '16x20'],
                    'img/posters/20x16.jpg' => ['alt' => '', 'imgid' => '20x16'],
                    'img/posters/20x30.jpg' => ['alt' => '', 'imgid' => '20x30'],
                    'img/posters/30x20.jpg' => ['alt' => '', 'imgid' => '30x20'],
                    'img/posters/24x36.jpg' => ['alt' => '', 'imgid' => '24x36'],
                    'img/posters/36x24.jpg' => ['alt' => '', 'imgid' => '36x24'],
                ],
                'products' => [
                    '16x20' => [
                        'price' => '21.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.NjEwNGZiNmU,.U35eKV9wXR2bbQ7Mq8vhZBYVK_ao7Pq9Cy-lGQR6v6QWuFIBpujJWw,,',
                        'image' => '16x20',
                        'label' => '16 x 20"',
                    ],
                    '20x16' => [
                        'price' => '21.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.MGEyOTg5NTQ,.MNtv6iFbIWypSX-Wj8ygBydQ0cGeeWWmB363ixWZat9OIbjlFyEg9g,,',
                        'image' => '20x16',
                        'label' => '20 x 16"',
                    ],
                    '20x30' => [
                        'price' => '26.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.MjE2YjJiNDQ,.1nae4tUhCtAty-F3Uvc3zL1Vy1kV3eqJYaJS3KTDthe0tclDbqoFwA,,',
                        'image' => '20x30',
                        'label' => '20 x 30"',
                    ],
                    '30x20' => [
                        'price' => '26.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.YTU1NWU4MTQ,.VGhS8nZpFUqfFk-XSLlrsBlGa9DRu7PuA2TxxqoRo9PrBQiTeRunOw,,',
                        'image' => '30x20',
                        'label' => '30 x 20"',
                    ],
                    '24x36' => [
                        'price' => '32.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.MGZhNjcxZTg,.jatvNoOc0CCMJLLmrR4S8nFT1t8BjuCfgGl4L-Hz55gqze1enlKI2Q,,',
                        'image' => '24x36',
                        'label' => '24 x 36"',
                    ],
                    '36x24' => [
                        'price' => '32.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.MDlkYmUzMWM,.HWgUhFHlWfau8woASVdfRRRZgV7VVCphlRa56RFMs1juUA5Ne5BRpg,,',
                        'image' => '36x24',
                        'label' => '36 x 24"',
                    ],
                ],
                'extras' => [
                    'paper' => [
                        '240gsm Glossy' => 0,
                        '245gsm Metallic' => 0,
                        '250gsm Raster' => 0,
                    ],
                    // 'images' => [
                    //     '1' => 0,
                    // ],
                ],
                'post_messages' => [
                    // 'images' => 'Want to add more images? Add text to your designs? Download our desktop Editor for 100% design freedom.',
                ],
                'options' => [
                    // 'images' => 'Images',
                    'paper' => 'Paper',
                    'price' => 'Price',
                ],
                'about' => 'Posters taken to the next level with photographic quality re-production.<br><br>Choose just one huge image or make a collage, the choice is yours. Available in a range of sizes and 3 luxurious photo papers.',
                'prices' => [
                    [
                        ['','240gsm GLOSSY','245gsm METALLIC','250gsm RASTER'],
                        ['16 x 20"','$21.95','$21.95','$21.95'],
                        ['20 x 30"','$26.95','$26.95','$26.95'],
                        ['24 x 36"','$32.95','$32.95','$32.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $9.95','+ $3.00'],
                ],
            ];
            break;

        case 'premium-prints-create-now':
            $__pagedata = [
                'title' => 'Premium Prints',
                'document_title' => 'Premium Prints: Create your own set of Premium Prints | albumworks',
                'meta_description' => 'Make a set of high quality photo prints in 6x4, 7x5 and more. Make online in an instant from just $3.80.',
                'images' => [
                    'img/prints/prints-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/prints/prints-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/prints/prints-03.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/prints/6x4.jpg' => ['alt' => '', 'imgid' => '6x4'],
                    'img/prints/7x5.jpg' => ['alt' => '', 'imgid' => '7x5'],
                    'img/prints/10x8.jpg' => ['alt' => '', 'imgid' => '10x8'],
                    'img/prints/14x11.jpg' => ['alt' => '', 'imgid' => '14x11'],
                ],
                'products' => [
                    '6x4' => [
                        'price' => '8.95',
                        'paper' => '240gsm Glossy Photo Paper',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.MGRiOWE3M2Q,.GLQK_7PWcxjrufO3TKwCPtGKQHV9IlItQa8mPNYup2JsEYBY5sTqgw,,',
                        'image' => '6x4',
                        'label' => '6 x 4" (25 pcs)',
                    ],
                    '7x5' => [
                        'price' => '5.95',
                        'paper' => '240gsm Glossy Photo Paper',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=35.MDI0MTQzMTc,.MNQxe19HMLo-4Ii7hjorFpkjmvhHSfODJf2aCYPHTTOM8ieqDkrJBQ,,',
                        'image' => '7x5',
                        'label' => '7 x 5" (4 pcs)',
                    ],
                    '10x8' => [
                        'price' => '3.95',
                        'paper' => '240gsm Glossy Photo Paper',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.ZWNjMWM3MWQ,.gsK9Zcse_NibCbybhzt71oSShDUDCK4sIbFDGCkjH4WpTIEqWLfUuw,,',
                        'image' => '10x8',
                        'label' => '10 x 8" (1 pc)',
                    ],
                    '14x11' => [
                        'price' => '8.95',
                        'paper' => '240gsm Glossy Photo Paper',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=35.MGRhZTRlNzQ,.rxyyFmfbpe29zW4Jp1c4pC5nNoo_ya_p28sAoyCH2S5ib1r2Sib52A,,',
                        'image' => '14x11',
                        'label' => '14 x 11" (1 pc)',
                    ],
                ],
                'options' => [
                    'paper' => 'Paper',
                    'price' => 'Price',
                ],
                'about' => 'Your photos have never looked so good!<br><br>All photos are printed on 240gsm glossy photo paper designed to provide maximum punch in colour and a lot of heft in the paper.',
                'prices' => [
                    [
                        ['','240gsm GLOSSY'],
                        ['6 x 4" (25 pcs)','$8.95'],
                        ['7 x 5" (4 pcs)','$5.95'],
                        ['10 x 8" (1 pc)','$3.95'],
                        ['14 x 11" (1 pc)','$8.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00'],
                ],
            ];
            break;

        case 'hd-calendar-create-now':
            $__pagedata = [
                'title' => 'Hi Colour Calendars',
                'document_title' => 'Hi Colour Calendars: Create your own Photo Calendar | albumworks',
                'meta_description' => null,
                'images' => [
                    'img/hdcal/20190903/hd-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/hdcal/20190903/hd-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/hdcal/20190903/11x5-HD.jpg' => ['alt' => '', 'imgid' => '11x5'],
                    'img/hdcal/20190903/11x8_03.jpg' => ['alt' => '', 'imgid' => '11x8'],
                    'img/hdcal/20190903/12x12_03.jpg' => ['alt' => '', 'imgid' => '12x12'],
                ],
                'products' => [
                    '11x5' => [
                        'pages' => '28 (12 months)',
                        'price' => '37.95',
                        'paper' => 'Hi Colour 260gsm Photo Luster',
                        'url' => '',
                        'image' => '11x5',
                        'label' => '11 x 5"',
                    ],
                    '11x8' => [
                        'pages' => '28 (12 months)',
                        'price' => '37.95',
                        'paper' => 'Hi Colour 260gsm Photo Luster',
                        'url' => '',
                        'image' => '11x8',
                        'label' => '11 x 8"',
                    ],
                    '12x12' => [
                        'pages' => '28 (12 months)',
                        'price' => '49.95',
                        'paper' => 'Hi Colour 260gsm Photo Luster',
                        'url' => '',
                        'image' => '12x12',
                        'label' => '12 x 12"',
                    ],
                ],
                'extras' => [
                    'design' => [
                        'Smart Calendar' => 0,
                        'Classic Compact Grid' => 0,
                        'Classic Large Grid' => 0,
                    ],
                ],
                'urls' => [
                    'formula' => 'size|design',
                    'values' => [
                        '12 x 12"|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.Mzk5MzdhNzM.NHuqTERBzsA06IaGu-7bacZ99xlP4__DxjHH6ALOH_LCQ-lhUxYWar0JUtk7OQcAM4C-iT1GnhQ9mH716cYiog',
                        '12 x 12"|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=67.MjNlMDdhYTI.MyIN2aWU5gwEehf1Oj0SsosSh8y57TBkIu2O3izr2CZhe-J1y7XFG24Q6hEulqgjDrfzcB1UvSZ5983VuXYbAvDj2T6iG-Cw',
                        '12 x 12"|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=67.ZmEwYmE4Mjg.eMr3r1zZZoXHjBAoThwlPQ-MhRsZMcEr09waOEs8QKPl2EM0Im0_KC59r9CnM_tV3DlgcyTtEYG0DXsERyCECRwf7b-oBtFy',
                        '11 x 8"|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NDc4MjI2ODU.Htlg_2x9_mpE6Dc5_SGepmeCB_jXCIS2jXKkiWYVkPzHESpli088ftdGQkB0cQYFbkpFy7Pq7ew',
                        '11 x 8"|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.NmYxMTYwZWI._ccj1iMOvqZl93oc-0buDlt28kBCaBKEjrwcPiVhqLi-3Zomm0uZ5IsnWwhH_J1qWShK1IRV-ZHusmyJJc9GUi8Zova1FJpa',
                        '11 x 8"|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.MmYxZmIwN2M.hJXpvuKPXQy5XcwiDIXaPlXqgxRyXZ0w2iCQFEzSopVP9Y2nAjUHCCsGu5xmSmgZRy9wTHX0rrH8gcYIfuKUwyZ28AcXcNEt',
                        '11 x 5"|Smart Calendar' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.MzIyZDE5MmY.ljKdk7MqjdMfjmfRV_01aLm8mB_kZEaaRg6IkqJkcz0ZJfsvp20JzI2hQA4zTO9ZU-Nn6D-Lxog',
                        '11 x 5"|Classic Compact Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.OGU4ZjhlZTI.7nUxANq5tEPOhcvoEblHkTjB3X7-ZnrARHJqFstbcCvx7Rng8mHdrneMApGYdOFOCt4KtpwyH7tAsPjeiDtD-LuxOy7xE8hS',
                        '11 x 5"|Classic Large Grid' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=65.ZTFmYjcyMzM.cUlHsQCm7frVTq2bEcUcVUvvmxFPb8J1uBpioIhVxs_RlzNLMuHNfAmX4DK5upmSP2LzHYg7kzNvraaWzNW9aOSStaJuTueV',
                    ]
                ],
                'options' => [
                    'design' => 'Design',
                    // 'pages' => 'Pages',
                    'paper' => 'Paper',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-04') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF CALENDARS WITH VOUCHER CODE:',
                    'promocode' => 'CALENDAR40'
                ] :  ((date('Y-m-d') == '2017-12-12') ? [
                    'message' => 'GET <strong>2 FOR 1</strong> CALENDARS WITH VOUCHER CODE:',
                    'promocode' => '2FOR1CAL'
                ] : '')),
                'about' => '<strong>Hi Colour WALL CALENDAR</strong><br><br>Beautiful 12 month personalised Wall Calendar filled with your precious photos and important dates, printed on stunning 260gsm Photo Luster paper.<br><br>Wall Calendars are spiral wire bound and open to a beautiful double page spread featuring your photos on top and calendar grid on the bottom. Conveniently placed hole for easy wall hanging. Pen friendly surface for writing notes during the year.<br><br><strong>Hi Colour DESK CALENDAR</strong><br><br>Desk Calendars are spiral wire bound with a convenient black tent stand to sit freely on your desk. Your personalised images displayed on one side and calendar grid on the other.',
                'prices' => [
                    [
                        ['','Hi Colour<br>260gsm PHOTO LUSTER'],
                        ['11 x 5"','$37.95'],
                        ['11 x 8"','$37.95'],
                        ['12 x 12"','$49.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['11 x 5"','+ $12.95','+ $3.00'],
                    ['11 x 8"','+ $12.95','+ $3.00'],
                    ['12 x 12"','+ $12.95','+ $3.00'],
                ],
            ];
            break;

        case 'wood-prints-create-now':
            $__pagedata = [
                'title' => 'Wood Prints',
                'document_title' => 'Wood Prints: Create your own Wood Print | albumworks',
                'meta_description' => 'Creat stylish wooden prints using your own photos. Printed on sycamore timber and available in a range of sizes. Make online in an instant.',
                'images' => [
                    'img/wood/wood-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/wood/wood-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/wood/wood-03.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/wood/12x8.jpg' => ['alt' => '', 'imgid' => '12x8'],
                    'img/wood/8x12.jpg' => ['alt' => '', 'imgid' => '8x12'],
                    'img/wood/18x12.jpg' => ['alt' => '', 'imgid' => '18x12'],
                    'img/wood/12x18.jpg' => ['alt' => '', 'imgid' => '12x18'],
                    'img/wood/20x16.jpg' => ['alt' => '', 'imgid' => '20x16'],
                    'img/wood/16x20.jpg' => ['alt' => '', 'imgid' => '16x20'],
                    'img/wood/30x20.jpg' => ['alt' => '', 'imgid' => '30x20'],
                    'img/wood/20x30.jpg' => ['alt' => '', 'imgid' => '20x30'],
                ],
                'products' => [
                    '8x12' => [
                        'price' => '109.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.Y2ZiZTNhNWI,.d3OJTtbHHl8xay4L2bDkv2YZY6c5sWEtOIsw4smts4A,',
                        'image' => '8x12',
                        'label' => '8 x 12"',
                    ],
                    '12x8' => [
                        'price' => '109.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.MTViNDAzM2I,.JkZuh29uFPCB75UmZi4bQ8kx0hx0s7M6Q9oNKr721nU,',
                        'image' => '12x8',
                        'label' => '12 x 8"',
                    ],
                    '18x12' => [
                        'price' => '169.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ZDFkNTFlODQ,.3vDihjI4vDhCvhAVV4eh0v_8UbgiFlB2ta4yhLpOqWA,',
                        'image' => '18x12',
                        'label' => '18 x 12"',
                    ],
                    '12x18' => [
                        'price' => '169.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ZmJiMWIxODI,.xLuSDOmT7ZAvuR8ZHXjH6jfFRFWIwDpheJcCIzv-Xwc,',
                        'image' => '12x18',
                        'label' => '12 x 18"',
                    ],
                    '20x16' => [
                        'price' => '229.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ODhlZjUzNjM,.F-F-W8qUP5Ihhbgev0xSmG8KWcG9agLu1PKCqo0g5c4,',
                        'image' => '20x16',
                        'label' => '20 x 16"',
                    ],
                    '16x20' => [
                        'price' => '229.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.NjY2Y2Y2ZTQ,.JymmYRQZVaGo2d_goprZMiz0iSoKFIabaWhRNwGN_Qc,',
                        'image' => '16x20',
                        'label' => '16 x 20"',
                    ],
                    '30x20' => [
                        'price' => '349.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.YWRjMmUxNDk,.cWjkqk-HO6xYSKGkeBandGDBruJkRHRIsyIHJ7isjto,',
                        'image' => '30x20',
                        'label' => '30 x 20"',
                    ],
                    '20x30' => [
                        'price' => '349.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ZDQ2ZGU4MDI,.7xagQPWzC3bIhbqkqB04cvDOqa68DRsEjmgjuKFEXh4,',
                        'image' => '20x30',
                        'label' => '20 x 30"',
                    ],
                ],
                'extras' => [
                    // 'images' => [
                    //     '1' => 0,
                    // ],
                ],                
                'post_messages' => [
                    // 'images' => 'Want to add more images? Add text to your designs? Download our desktop Editor for 100% design freedom.',
                ],
                'options' => [
                    // 'images' => 'Images',
                    'price' => 'Price',
                ],
                'about' => 'Turn your photos into a stylish Wood Print.<br><br>Printed directly onto Sycamore timber. Your prints are designed to reveal rather than obscure the beautiful natural grain of the Sycamore. Ideal for home decor, wedding signage and perfect for gift giving.',
                'prices' => [
                    [
                        ['','WOOD PRINTS'],
                        ['12 x 8"','$109.95'],
                        ['18 x 12"','$169.95'],
                        ['20 x 16"','$229.95'],
                        ['30 x 20"','$349.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['12 x 8"','+ $12.95','+ $8.00'],
                    ['18 x 12"','+ $12.95','+ $8.00'],
                    ['20 x 16"','+ $12.95','+ $8.00'],
                    ['30 x 20"','+ $24.95','+ $15.00'],
                ],
            ];
            break;

        case 'journals-create-now':
            $__pagedata = [
                'title' => 'Journal',
                'document_title' => 'Personalised Journals: Create your own Journal notebook | albumworks',
                'mainclass' => 'nomobileworkflow',
                'meta_description' => 'Make your own personalised Journal. Choose from a range of material covers and customise every single pages. 100 pages from just $49.95.',
                'images' => [
                    'img/journals/journal-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/journals/journal-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/journals/journal-03.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/journals/journal-04.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/journals/6x8.jpg' => ['alt' => '', 'imgid' => '6x8'],
                    'img/journals/8x6.jpg' => ['alt' => '', 'imgid' => '8x6'],
                    'img/journals/8x8.jpg' => ['alt' => '', 'imgid' => '8x8'],
                    'img/journals/8x11.jpg' => ['alt' => '', 'imgid' => '8x11'],
                ],
                'products' => [
                    '6x8' => [
                        'pages' => '100',
                        'paper' => '190gsm E-Photo Matte paper',
                        'price' => '49.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.YzRhZTBhZWY,.22qWzqS5ujZ5FMy6HmWuJPcGgKZC-cCJ7-wZDIxJePo,',
                        'image' => '6x8',
                        'label' => '6 x 8"',
                    ],
                    '8x6' => [
                        'pages' => '100',
                        'paper' => '190gsm E-Photo Matte paper',
                        'price' => '49.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.M2YxMWQ2ZjE,.tjMF78pDUBa5eNl4erY5d-PjmoJ7SUel7Te3WtxZDXY,',
                        'image' => '8x6',
                        'label' => '8 x 6"',
                    ],
                    '8x8' => [
                        'pages' => '100',
                        'paper' => '190gsm E-Photo Matte paper',
                        'price' => '59.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.NmRhN2QyYzc,.NuEIaDoQz6ks229_pJq189WoW5hKMTsWPa26Ft6VU5Y,',
                        'image' => '8x8',
                        'label' => '8 x 8"',
                    ],
                    '8x11' => [
                        'pages' => '100',
                        'paper' => '190gsm E-Photo Matte paper',
                        'price' => '69.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ODFmYmRiZTk,.iIbxW9_WLZpPuV831z9n2Y-lJe5y8txkJe5NJId5VmM,',
                        'image' => '8x11',
                        'label' => '8 x 11"',
                    ],
                ],
                'options' => [
                    'pages' => 'Pages',
                    'paper' => 'Paper Type',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-03') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF STATIONERY WITH VOUCHER CODE:',
                    'promocode' => 'STATIONERY40'
                ] : ''),
                'about' => 'A journal where every page can be customised in black and white or colour. Add photos, quotes or whatever takes your fancy. But most importantly make it yours.<br><br>All journals are covered in a material finish with a wide choice of colours in Buckram and Faux Leather',
                'prices' => [
                    [
                        ['','190gsm E-Photo Matte paper'],
                        ['6 x 8" <span>(100 pages)</span>','$49.95'],
                        ['8 x 6" <span>(100 pages)</span>','$49.95'],
                        ['8 x 8" <span>(100 pages)</span>','$59.95'],
                        ['8 x 11" <span>(100 pages)</span>','$69.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $11.95','+ $5.00 each'],                    
                ],
            ];
            break;

        case 'notebooks-create-now':
            $__pagedata = [
                'title' => 'Notebook',
                'document_title' => 'Notebooks: Create your own Notebook | albumworks',
                'meta_description' => 'Make your very own Notebook with lined or blank pages. 100 pages, complete with your own customised cover. Make in an instant from just $29.95.',
                'images' => [
                    'img/notebooks/notebook-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/notebooks/notebook-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/notebooks/6x8.jpg' => ['alt' => '', 'imgid' => '6x8'],
                    //'img/notebooks/8x6.jpg' => ['alt' => '', 'imgid' => '8x6'],
                    //'img/notebooks/8x8.jpg' => ['alt' => '', 'imgid' => '8x8'],
                    //'img/notebooks/8x11.jpg' => ['alt' => '', 'imgid' => '8x11'],
                ],
                'products' => [
                    '6x8' => [
                        'pages' => '100',
                        'paper' => '80gsm WRITING PAPER',
                        'price' => '29.95',
                        'url' => '',
                        'image' => '6x8',
                        'label' => '6 x 8"',
                    ],
                    /*
                    '8x6' => [
                        'pages' => '100',
                        'paper' => '80gsm WRITING PAPER',
                        'price' => '29.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.NTVmZjQ2NTE,.AFLBea5tS0MEj9sklfXWR6Y5czpc8Onu1xGogv31c8k,',
                        'image' => '8x6',
                        'label' => '8 x 6"',
                    ],
                    '8x8' => [
                        'pages' => '100',
                        'paper' => '148gsm Superfine Eggshell',
                        'price' => '39.95',
                        'url' => '#',
                        'image' => '8x8',
                        'label' => '8 x 8"',
                    ],
                    '8x11' => [
                        'pages' => '100',
                        'paper' => '148gsm Superfine Eggshell',
                        'price' => '49.95',
                        'url' => '#',
                        'image' => '8x11',
                        'label' => '8 x 11"',
                    ],
                    */
                ],
                'options' => [
                    'pages' => 'Pages',
                    'paper' => 'Paper Type',
                    'design' => 'Design',
                    'price' => 'Price',
                ],
                'extras' => [
                    'design' => [
                        'Lined Paper' => 0,
                        'Plain Paper' => 0,
                    ],
                ],                
                'urls' => [
                    'formula' => 'size|design',
                    'values' => [
                        '6 x 8"|Lined Paper' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.NTVmZjQ2NTE,.AFLBea5tS0MEj9sklfXWR6Y5czpc8Onu1xGogv31c8k,',
                        '6 x 8"|Plain Paper' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.ZGUwYmU3MTU,.4bZ-Xy9dkpyrujG-Ac49-OQWSXViXocy6DNxDFy5-b8,',
                    ]
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-03') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF STATIONERY WITH VOUCHER CODE:',
                    'promocode' => 'STATIONERY40'
                ] : ''),
                'about' => 'Bound just like a real book, fully lined with end papers and containing your choice of lined or blank internal pages. Customise your Notebook cover with your own personal photos and text.',
                'prices' => [
                    [
                        ['','80gsm WRITING PAPER'],
                        ['6 x 8" <span>(100 pages)</span>','$29.95'],
                        //['8 x 6"','$29.95'],
                        //['8 x 8"','$39.95'],
                        //['8 x 11"','$49.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $11.95','+ $5.00 each'],                    
                ],

            ];
            break;

        case 'guest-books-create-now':
            $__pagedata = [
                'title' => 'Guest Book',
                'document_title' => 'Guest Books: Create your own Guest Book | albumworks',
                'mainclass' => 'nomobileworkflow',
                'meta_description' => 'Create a personalised Guest Book for your wedding, birthday or celebration. Stunning print quality with luxurious material cover and custom cover message. Easy to make from just $59.95.',
                'images' => [
                    'img/signature/signature-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/signature/signature-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/signature/signature-03.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/signature/8x6.jpg' => ['alt' => '', 'imgid' => '8x6'],
                    'img/signature/8x8.jpg' => ['alt' => '', 'imgid' => '8x8'],
                    'img/signature/11x85.jpg' => ['alt' => '', 'imgid' => '11x85'],
                    'img/signature/12x12.jpg' => ['alt' => '', 'imgid' => '12x12'],
                    'img/signature/16x12.jpg' => ['alt' => '', 'imgid' => '16x12'],
                ],
                'products' => [
                    '8x6' => [
                        'pages' => '40',
                        'paper' => '190gsm E-Photo Matte paper',
                        'stamping' => 'Included',
                        'price' => '64.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=37.YzdlYmQ0MmQ,.MeHTd__XrSLxbmJ4tp3Mb9Nf0Bd7Z_qCRnFqwjIESATlMDjqeNvobQ,,',
                        'image' => '8x6',
                        'label' => '8 x 6"',
                    ],
                    '8x8' => [
                        'pages' => '40',
                        'paper' => '190gsm E-Photo Matte paper',
                        'stamping' => 'Included',
                        'price' => '84.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=37.NzBhNjk5ZTk,.p-LlbfpzUT6GImMt76n2tz7GloZ-3Rl3XugtPG62Bu8Okz9ZIN04DQ,,',
                        'image' => '8x8',
                        'label' => '8 x 8"',
                    ],
                    '11x85' => [
                        'pages' => '40',
                        'paper' => '190gsm E-Photo Matte paper',
                        'stamping' => 'Included',
                        'price' => '94.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=39.NzQxODVmODA,.Wdbt4LSxU-ezQVb-Jzlt45eLJNf4DbP4cTAO_mg0UC0DInSoJ237Lg,,',
                        'image' => '11x85',
                        'label' => '11 x 8.5"',
                    ],
                    '12x12' => [
                        'pages' => '40',
                        'paper' => '190gsm E-Photo Matte paper',
                        'stamping' => 'Included',
                        'price' => '109.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=40.MjllNmE1MjI,.W5WOKw8oy9CkjW8s9GsmJAB7aRQlynDbPfC2DjuN4r0hf9YMOin_3w,,',
                        'image' => '12x12',
                        'label' => '12 x 12"',
                    ],
                    '16x12' => [
                        'pages' => '40',
                        'paper' => '190gsm E-Photo Matte paper',
                        'stamping' => 'Included',
                        'price' => '139.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=40.OWQxZmYyOGY,.c2Jqky0zISrQ9cZSEHy9mD7qjd0quji4iHzkFWp-4TRS1W-voH8EwA,,',
                        'image' => '16x12',
                        'label' => '16 x 12"',
                    ],
                ],
                'options' => [
                    'pages' => 'Pages',
                    'paper' => 'Paper Type',
                    'stamping' => 'Hot Text Stamping',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-03') ? [
                    'message' => 'SAVE <strong>40%*</strong> OFF STATIONERY WITH VOUCHER CODE:',
                    'promocode' => 'STATIONERY40'
                ] : ''),
                'about' => 'Beautifully bound with genuine gold or silver foiling or blind debossing on the cover, the Guest Book is designed to capture all the thoughts of your guests. Every page is customisable. Add photos, add signing pages all in full colour.<br><br>All Guest Books are covered in a material finish and your own custom message in hot text stamping.',
                'prices' => [
                    [
                        ['','190gsm E-PHOTO MATTE PAPER'],
                        ['8 x 6" <span>(40 pages)</span>','$64.95'],
                        ['8 x 8" <span>(40 pages)</span>','$84.95'],
                        ['11 x 8.5" <span>(40 pages)</span>','$94.95'],
                        ['12 x 12" <span>(40 pages)</span>','$109.95'],
                        ['16 x 12" <span>(40 pages)</span>','$139.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['8 x 6"','+ $11.95', '+ $5.00'],
                    ['8 x 8"','+ $11.95', '+ $5.00'],
                    ['11 x 8.5"','+ $11.95', '+ $5.00'],
                    ['12 x 12"','+ $11.95', '+ $5.00'],
                    ['16 x 12"','+ $16.95', '+ $8.00'],
                ],
            ];
            break;

        case 'luggage-tags-create-now':
            $__pagedata = [
                'title' => 'Personalised Tags',
                'document_title' => 'Personalised Tags: Create your own personalised tags | albumworks',
                'images' => [
                    'img/tags/1.7x3.5_03.jpg' => ['alt' => '', 'imgid' => '17x35'],
                    'img/tags/2.5x4.25_03.jpg' => ['alt' => '', 'imgid' => '25x425'],
                    'img/tags/3.5x1.7_03.jpg' => ['alt' => '', 'imgid' => '35x17'],
                    'img/tags/4.25x2.5_03.jpg' => ['alt' => '', 'imgid' => '425x25'],
                    'img/tags/bone_03.jpg' => ['alt' => '', 'imgid' => 'bone'],
                    'img/tags/circle_03.jpg' => ['alt' => '', 'imgid' => 'circle'],
                    'img/tags/heart_03.jpg' => ['alt' => '', 'imgid' => 'heart'],
                ],
                'products' => [
                    '17x35' => [
                        'quantity' => '1',
                        'price' => '9.95',
                        'url' => '',
                        'image' => '17x35',
                        'label' => '1.7 x 3.5"',
                    ],
                    '35x17' => [
                        'quantity' => '1',
                        'price' => '9,95',
                        'url' => '',
                        'image' => '35x17',
                        'label' => '3.5" x 1.7"',
                    ],
                    '25x425' => [
                        'quantity' => '1',
                        'price' => '13.95',
                        'url' => '',
                        'image' => '25x425',
                        'label' => '2.5" x 4.25"',
                    ],
                    '425x25' => [
                        'quantity' => '1',
                        'price' => '13.95',
                        'url' => '',
                        'image' => '425x25',
                        'label' => '4.25 x 2.5"',
                    ],
                    'bone' => [
                        'quantity' => '1',
                        'price' => '12.95',
                        'url' => '',
                        'image' => 'bone',
                        'label' => 'Bone Pet Tag',
                    ],
                    'circle' => [
                        'quantity' => '1',
                        'price' => '12.95',
                        'url' => '',
                        'image' => 'circle',
                        'label' => 'Circle Pet Tag',
                    ],
                    'heart' => [
                        'quantity' => '1',
                        'price' => '12.95',
                        'url' => '',
                        'image' => 'heart',
                        'label' => 'Heart Pet Tag',
                    ],
                ],
                'extras' => [
                    'images' => [
                        'type' => 'productspecific',
                        'items' => [
                            '17x35' => [2=>0,3=>0,4=>0,5=>0],
                            '35x17' => [2=>0,3=>0,4=>0,5=>0],
                            '25x425' => [2=>0,3=>0,4=>0,5=>0],
                            '425x25' => [2=>0,3=>0,4=>0,5=>0],
                            'heart' => [1=>0],
                            'bone' => [1=>0],
                            'circle' => [1=>0],
                        ]
                    ],
                ],
                'urls' => [
                    'formula' => 'size|images',
                    'values' => [
                        '1.7 x 3.5"|2' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.MGQxYmRiNGU,.H2ZEfpQ9XWsArbzLPQMfxfJjaReEsuBBa9lfwRWlct0,',
                        '1.7 x 3.5"|5' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.YTA5NzEyZTQ,.hqnWx9s-w0Q8Jfgj5NyuBSy0ot7myb1bzQXH7V4QKKU,',
                        '1.7 x 3.5"|3' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.NTk2MWNhY2Q,.yu4LB_abev-63e6Tf7KNYMA0jG95Llwz-EpCsHHLXdI,',
                        '1.7 x 3.5"|4' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.YWM1YjhmZjc,.EeJXLeJqF-cACkdiV_KkqYkcy0IqOAxRKgieYe_x8gw,',
                        '2.5" x 4.25"|2' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.ZmIxZTRkYzQ,.XyqLFfmy7UYZWnHK8VdK5eL6kwaZInW2SjYu0mzUyzc,',
                        '2.5" x 4.25"|5' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.ZmIzMTRiMzQ,.wRoqcpLuHAoL9QNc7qtHvH3437Si_yhUek9i3EWomwM,',
                        '2.5" x 4.25"|3' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.NzZiNDlkYjU,.MxJWCsj2ZaFyPr4PiYfwEqchAqePgyk11zeltQOBQY0,',
                        '2.5" x 4.25"|4' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.Y2I2Zjc4ODU,._khHN7hCZlh_X92XbkhsAwNp_-C9PSN_fx89NBGEfm8,',
                        '3.5" x 1.7"|2' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.OTM0OTRlNGE,.IgbX7-cuxbdkZsiZHApoWYdbfvU9NmKjgKx6MHlNLZw,',
                        '3.5" x 1.7"|5' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.ZGQzNGIyOGE,.3tAmU5MxphX6HyWo6mGMmXSLMIqF5kxwk66krvzggMM,',
                        '3.5" x 1.7"|3' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.NjBkNDQ4MTM,.QnyHA-0P15CPnZcVadEUBBp8MsEjA7pevJ8m2IakiI0,',
                        '3.5" x 1.7"|4' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.MTg2Njc0MTE,.0Bqp2FDAh7D-2axCxKVAl-AlB4_pqgjNEw1kfAWtRrU,',
                        '4.25 x 2.5"|2' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.Nzc5ZmVmMWM,.MErwBIaYq-u_7Gza4KdDr4eV3xg-BjMDiXYB59vGLXk,',
                        '4.25 x 2.5"|5' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.ZGRiN2Y2MWQ,.OAkIA9sAo29_M47AzYuQLrHKugRJUOuh5aIYrCbQz8c,',
                        '4.25 x 2.5"|3' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.ZjcwNWNjZmM,.5BVb2RasY2eh20ZW5eq50hQAX_a9ES1Z5aGnISPCMHM,',
                        '4.25 x 2.5"|4' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.NzcxYTY1NmE,.6olAAdjoC8aQ0XLLvW36kMzoGqhUNstE0375S7y-m5o,',
                        'Heart Pet Tag|1' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.ZjIzOGMzOGE,.uX3XnrZwPVcJ-3doa4w-ohR0ESQa20EEg8gtat7M-EI,',
                        'Bone Pet Tag|1' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.MDc4YWJlNjE,.2_GzXsR4ZN1SsWuaw2r3rTwGlY2OGD2xs-8dlkea6Yg,',
                        'Circle Pet Tag|1' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.ZWUxNmE3OTU,.KmJLB5lWd4riDLfuOpHcUoJ9lnZlmvMYCuzrHDDtDJc,',
                    ]
                ],                 
                'options' => [
                    'images' => 'Images',
                    'quantity' => 'Quantity',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Personalised Tags are a tough and durable way to personalise your luggage or create your own Pet Tag. Customise with your own photos and text<br><br>Each Luggage Tag has a slot for a luggage strap and comes supplied with a leather-like strap with buckle.<br><br>Each Pet Tag comes with a metal bound ring for easy attachment to your pet’s collar.',
                'prices' => [
                    [
                        ['','PERSONALISED TAGS'],
                        ['1.7 x 3.5"','$9.95'],
                        ['3.5 x 1.7"','$9.95'],
                        ['2.5" x 4.25"','$13.95'],
                        ['4.25" x 2.5"','$13.95'],
                        ['HEART TAG','$12.95'],
                        ['BONE TAG','$12.95'],
                        ['CIRCLE TAG','$12.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $3.00'],
                ],
            ];
            break;

        case 'tote-bags-create-now':
            $__pagedata = [
                'title' => 'Travel Tote Bag',
                'document_title' => 'Tote Bag: Create your own personalised Tote Bag | albumworks',
                'meta_description' => 'Create your own Tote Bag. Customise with your own photos and text. The ideal travel companion. Make in an instant from just $22.95.',
                'images' => [
                    'img/tote/tote-01.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/tote/tote-02.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/tote/11x11.jpg' => ['alt' => '', 'imgid' => '11x11'],
                ],
                'products' => [
                    '11x11' => [
                        'quantity' => '1',
                        'price' => '22.95',
                        'url' => '#',
                        'image' => '11x11',
                        'label' => '11 x 11"',
                    ],
                ],
                'extras' => [
                    'images' => [
                        1 => 0,
                        2 => 0,
                        3 => 0,
                        4 => 0,
                    ],
                    'style' => [
                        'Single sided' => 0,
                        'Double sided' => 7,
                    ],
                ],
                'urls' => [
                    'formula' => 'size|style|images',
                    'values' => [
                        '11 x 11"|Single sided|1' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.ZWZiYWQxMzU,.qCMdYWiGeaXkO-lgLeSS3QOvSmWHSlER4Giw_7U8k-J-uAr8c-q5NQ,,',
                        '11 x 11"|Single sided|2' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.Njc5ZTA4MmU,.oVmRwE2IPFgrGQmRrWT3zWq2ZyhdHkzB4RATtGPORgTAReswA8HwAg,,',
                        '11 x 11"|Single sided|3' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.Y2RhYmVmZjg,.jMDjd73GjncKZiL4jT-A0SQlt7q4WOhi6V3pds3kvkH_TxtYVen0uw,,',
                        '11 x 11"|Single sided|4' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.YWFhNzU3ZmU,.XqwcQm9mTYQIulbV0C1z_iMNdTrXEqYt6_XCtzm0x9_25O1rmJorAA,,',
                        '11 x 11"|Double sided|1' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.NTY1NzhlMjc,.uveYfScglhhEA7u09-7vuOYR-Dv2Ee-R68DH04K1fVTQZuMF99yCOQ,,',
                        '11 x 11"|Double sided|2' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.N2JhOTY0Y2U,.yHKzD0SyJHVtBDVNA27ljI1PoLlknWAFEB723hMPpUIDsEzBaZ9Luw,,',
                        '11 x 11"|Double sided|3' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.OTk3Zjk4ZGE,.JWDXBSjxFwC7JSEiFemX0mp5cY4rFoL5nrzM_vxLvf8CmELFT9f-Hw,,',
                        '11 x 11"|Double sided|4' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.YmQ1YmVjODM,.Y-KTW2AkHPKNRsdqbjq-H5BejnmPNiCc4u7IFaZYGl-KWqmrF0oYmw,,',
                    ]
                ],
                'options' => [
                    'images' => 'Images',
                    'style' => 'Style',
                    'quantity' => 'Quantity',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-06') ? [
                    'message' => 'SAVE <strong>35%*</strong> OFF TOTES WITH VOUCHER CODE:',
                    'promocode' => 'TOTE35'
                ] : ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : '')),
                'about' => 'Make travel personal with your very own Travel Tote Bag. Personalise the design of your bag using your own photos and text on either 1 or 2 sides.<br><br>Made from Polyester Cotton, our Tote Bags are tough, durable and designed for the road more travelled.',
                'prices' => [
                    [
                        ['','TRAVEL TOTE BAG'],
                        ['1 SIDE','$22.95'],
                        ['2 SIDES','$29.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $9.95','+ $4.00'],
                ],
            ];
            break;
            
        case 'cushions-create-now':
            $__pagedata = [
                'title' => 'Cushions',
                'document_title' => 'Cushions | albumworks',
                'images' => [
                    'img/cushions/cushion_03.jpg' => ['alt' => '', 'imgid' => ''],
                ],
                'products' => [
                    '16x16' => [
                        'price' => '39.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.ZGNkMDkzMmM,.VsYLieTQv_4jWd4bWl6WBnRmsd_TKIhv7eCScpqw9JY,',
                        'label' => '16 x 16" Square',
                    ],
                ],
                'options' => [
                    'price' => 'Price',
                ],
                'precreate' => 'Back side of Cushion is solid black panel.',
                'flashpromotion' => ((date('Y-m-d') == '2017-12-05') ? [
                    'message' => 'SAVE <strong>35%*</strong> OFF CUSHIONS WITH VOUCHER CODE:',
                    'promocode' => 'CUSHION35'
                ] : ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : '')),
                'about' => 'Bring your living room to life with your own personalised Cushions. Use your photos and text to create a cozy Cushion - printed on a polyester cover with a black backing panel.<br><br>Cushions are sized at 16 x 16" (410 x 410mm) and make the perfect addition to your home decor. Cushion insert included.',
                'prices' => [
                    [
                        ['','16 x 16" CUSHION'],
                        ['CUSHION','$39.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $12.95','+ $8.00'],
                ],
            ];
            break;            
                      
        case 'desktop-plaques-create-now':
            $__pagedata = [
                'title' => 'Desktop Plaques',
                'document_title' => 'Desktop Plaques | albumworks',
                'images' => [
                    'img/plaques/6x6-square_03.jpg' => ['alt' => '', 'imgid' => '6x6'],
                    'img/plaques/5x7-portrait_03.jpg' => ['alt' => '', 'imgid' => '5x7'],
                    'img/plaques/7x5-landscape_03.jpg' => ['alt' => '', 'imgid' => '7x5'],
                    'img/plaques/8x10-portrait_03.jpg' => ['alt' => '', 'imgid' => '8x10'],
                    'img/plaques/10x8-landscape_03.jpg' => ['alt' => '', 'imgid' => '10x8'],
                ],
                'products' => [
                    '6x6' => [
                        'price' => '19.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.NWE1ODdiZmQ,.9WNBJX0CP-DnwZr3ZD7MZCu3QTrjwDvx7RgZrZ_RSJk,',
                        'image' => '6x6',
                        'label' => '6 x 6" Square',
                        'finish' => 'Sleek Gloss'
                    ],
                    '5x7' => [
                        'price' => '19.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.MTFhNGViNmU,.C_Iui12a5P-4bFH_fsEF6LYXM5u7k9smcUi1TjRDsv4,',
                        'image' => '5x7',
                        'label' => '5 x 7" Portrait',
                        'finish' => 'Sleek Gloss'
                    ],
                    '7x5' => [
                        'price' => '19.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.YTg4YWJhZjQ,.4mWKI3MYs7ym3P4rxdKkrZM0FbdZhOMWQ_uX9lxTZYk,',
                        'image' => '7x5',
                        'label' => '7 x 5" Landscape',
                        'finish' => 'Sleek Gloss'
                    ],                    
                    '8x10' => [
                        'price' => '29.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.Y2I5MDIzY2Q,.VmTkoXuOE-8BZrJAEmfYESmJXo7xTbrLW4Zzdipp4Qw,',
                        'image' => '8x10',
                        'label' => '8 x 10" Portrait',
                        'finish' => 'Sleek Gloss'
                    ],
                    '10x8' => [
                        'price' => '29.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.YTAxNDkyNjQ,.WzG9nMM-7jCHtbKCBPtdF3Nvxl0GlKap83z1fh2MnfY,',
                        'image' => '10x8',
                        'label' => '10 x 8" Landscape',
                        'finish' => 'Sleek Gloss'
                    ],
                ],
                'options' => [
                    'finish' => 'Finish',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Give your photos pride of place on your desktop with your own Desktop Plaque. Complete with a backing stand, these Desktop Plaques sit up just like a photo frame. Your photos will be displayed from edge to edge with a sleek gloss finish.<br><br>Landscape or portrait, large or small - the choice is yours. Showcase your own memories or create a truly special gift for loved ones. ',
                'prices' => [
                    [
                        ['','SLEEK GLOSS'],
                        ['6 x 6"','$19.95'],
                        ['5 x 7"','$19.95'],
                        ['7 x 5"','$19.95'],
                        ['8 x 10"','$29.95'],
                        ['10 x 8"','$29.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['6x6"','+ $12.95','+ $4.00'],
                    ['7x5"','+ $12.95','+ $4.00'],
                    ['10x8"','+ $12.95','+ $6.00'],
                ],
            ];
            break;            
                      
        case 'greeting-cards-create-now':
            $__pagedata = [
                'title' => 'Greeting Cards',
                'document_title' => 'Greeting Cards | albumworks',
                'images' => [
                   // 'img/cards/4x6-portrait_03.jpg' => ['alt' => '', 'imgid' => '4x6'],
                    'img/cards/5x7-portrait_03.jpg' => ['alt' => '', 'imgid' => '5x7'],
                    'img/cards/7x5-landscape_03.jpg' => ['alt' => '', 'imgid' => '7x5'],
                ],
                'products' => [
                /*
                    '4x6' => [
                        'price' => '3.35',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.NDdkYTZmZDE,.eU1k9HYhv9IDwjuQZ6lu2fKN4nndw6YsczSptW9G5G4,',
                        'image' => '4x6',
                        'label' => '4 x 6" Portrait',
                        'paper' => '270gsm Uncoated Matte',
                        'format' => 'Folded Card',
                    ], */
                    '5x7' => [
                        'price' => '4.05',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.Y2E3OGQ0ZDM,.RtkCf-kbMQEOZRmn2R54NAdoi81QifzVxEqncagWpxE,',
                        'image' => '5x7',
                        'label' => '5 x 7" Portrait',
                        'paper' => '270gsm Uncoated Matte',
                        'format' => 'Folded Card',
                    ],
                    '7x5' => [
                        'price' => '4.05',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=28.YWE0MjgyZmI,.oNtbVXcGd45YolXtrEPjF1xOIKYz7WVE-jjfKOxfUT0,',
                        'image' => '7x5',
                        'label' => '7 x 5" Landscape',
                        'paper' => '270gsm Uncoated Matte',
                        'format' => 'Folded Card',
                    ],
                ],
                'options' => [
                    'paper' => 'Paper',
                    'format' => 'Format',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Send a completely personalised message with a Photo Greeting Card. Customise using your own photos and text.<br><br>Printed on thick 270gsm paper, Greeting cards are perfect for life’s moments like birthdays, Christmas, anniversaries and more.',
                'prices' => [
                    [
                        ['','270gsm UNCOATED MATTE'],
                        //['4 x 6"','$3.35'],
                        ['5 x 7"','$4.05'],
                        ['7 x 5"','$4.05'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $7.95','+ $3.00'],
                ],
            ];
            break;            
                      
        case 'jigsaw-puzzles-create-now':
            $__pagedata = [
                'title' => 'Jigsaw Puzzles',
                'document_title' => 'Jigsaw Puzzles | albumworks',
                'images' => [
                    'img/puzzles/8x12_03.jpg' => ['alt' => '', 'imgid' => '8x12'],
                    'img/puzzles/12x8_03.jpg' => ['alt' => '', 'imgid' => '12x8'],
                    'img/puzzles/12x16_03.jpg' => ['alt' => '', 'imgid' => '12x16'],
                    'img/puzzles/16x12_03.jpg' => ['alt' => '', 'imgid' => '16x12'],
                ],
                'products' => [
                    '8x12' => [
                        'price' => '26.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.MGNmMjE4MzU,.8MJpTkAKbnAvSUiHfWm4yj8HD_Si7sDGmFQD8Mokdqg,',
                        'image' => '8x12',
                        'label' => '8 x 12" Portrait',
                        'pieces' => '120',
                    ],
                    '12x8' => [
                        'price' => '26.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.NmZhZTY3YWI,.hf5DPlJoRKAx3p7nvYIBoVsz-NYQGabaCUgTVeommws,',
                        'image' => '12x8',
                        'label' => '12 x 8" Landscape',
                        'pieces' => '120',
                    ],
                    '12x16' => [
                        'price' => '39.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.NjQ5ODc3YWM,.eSAm7_8DmLpAVuvQwj50RjLMskF-C3POkF5WWly1RkU,',
                        'image' => '12x16',
                        'label' => '12 x 16" Portrait',
                        'pieces' => '300',
                    ],
                    '16x12' => [
                        'price' => '39.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.ZjRhMjllYWQ,.4xmSGEdHErMbys27XTtKJJwnzWsJx2kF8OGSHzbtwJA,',
                        'image' => '16x12',
                        'label' => '16 x 12" Landscape',
                        'pieces' => '300',
                    ],
                ],
                'options' => [
                    'pieces' => 'Pieces',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Create your very own Jigsaw Puzzle. These photo puzzles are a unique gift idea for all ages and sure to bring lots of fun! Available in 2 sizes in both portrait and landscape.<br><br>Add a custom message to your Jigsaw Puzzle for announcements and surprises! Your message will be revealed piece by piece as your puzzle is put back together.',
                'prices' => [
                    [
                        ['','JIGSAW PUZZLE'],
                        ['8 x 12"','$26.95'],
                        ['12 x 8"','$26.95'],
                        ['12 x 16"','$39.95'],
                        ['16 x 12"','$39.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $8.95','+ $4.00'],
                ],
            ];
            break;            
                      
        case 'photo-mugs-create-now':
            $__pagedata = [
                'title' => 'Photo Mugs',
                'document_title' => 'Photo Mugs | albumworks',
                'images' => [
                    'img/mugs/white.jpg' => ['alt' => '', 'imgid' => 'white'],
                    'img/mugs/blue.jpg' => ['alt' => '', 'imgid' => 'blue'],
                    'img/mugs/red.jpg' => ['alt' => '', 'imgid' => 'red'],
                    'img/mugs/pink.jpg' => ['alt' => '', 'imgid' => 'pink'],
                ],
                'products' => [
                    'white' => [
                        'price' => '17.95',
                        'size' => '325mL',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.MWFmYmIzY2Y.o9SxIm4-LpJxph3WpiT6aNzM200dM93I1cPsYh-9qw_rIb9YgRt_AWoepTjTDEJkMMSlxMeX-6k',
                        'image' => 'white',
                        'label' => 'White',
                    ],
                    'blue' => [
                        'price' => '17.95',
                        'size' => '325mL',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.MTNlMjZhZjg.hXsBPTw-1pl4NAyLf7nCZNDvvJIpQorcAu4E0C4GpMUyDCDhXQIJesaE78qWzhvV0ApWJSYyx0M',
                        'image' => 'blue',
                        'label' => 'Blue',
                    ],
                    'red' => [
                        'price' => '17.95',
                        'size' => '325mL',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.Yzc0OWY2ZGM.0WssmMGh03xUgptUSyf3LRgmOo67GUatHiX1vAusdnR5ZKEHJGGFiOItmk4pQCwDTRPWRJb02no',
                        'image' => 'red',
                        'label' => 'Red',
                    ],
                    'pink' => [
                        'price' => '17.95',
                        'size' => '325mL',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.MzRkNGJlMjc.nZTsHvHfwQQLlFjqiNou_0uGaC6XPsgX9XCMWRfxE8JlLNEq6-JXUmtlreivN_B4GQEkq8aXStY',
                        'image' => 'pink',
                        'label' => 'Pink',
                    ],
                ],
                'options' => [
                    'size' => 'Size',
                    'price' => 'Price',
                ],
                'primary' => ['colour', 'Colour'],
                'flashpromotion' => '',
                'about' => 'Create your own personalised Photo Mug. A great gift idea or a great keepsake for yourself. Photo Mugs hold up to 325mL and are dishwasher and microwave safe.<br><br>With a complete 360 degree wraparound design you can choose multiple photos and even add your own personal message',
                'prices' => [
                    [
                        ['','PHOTO MUGS (ALL COLOURS)'],
                        ['325mL','$17.95'],
                    ]
                ],
                'shipping' => [
                    ['','EXPRESS<br>SHIP','EXTRA ITEM EXPRESS'],
                    ['ALL MUGS','+ $9.95','+ $6.00'],
                ],
            ];
            break;            
                      
        case 'phone-cases-create-now':
            $__pagedata = [
                'title' => 'Personalised Phone Case',
                'document_title' => 'Personalised Phone Case | albumworks',
                'images' => [
                    'img/phonecases/retail-pages-gifts-02_03.jpg' => ['alt' => '', 'imgid' => ''],
                    'img/phonecases/iphone6_03.jpg' => ['alt' => '', 'imgid' => 'i6'],
                    'img/phonecases/iphone6-Plus_03.jpg' => ['alt' => '', 'imgid' => 'i6p'],
                    'img/phonecases/iphone6S_03.jpg' => ['alt' => '', 'imgid' => 'i6s'],
                    'img/phonecases/iphone6S-Plus_03.jpg' => ['alt' => '', 'imgid' => 'i6sp'],
                    'img/phonecases/iphone7_03.jpg' => ['alt' => '', 'imgid' => 'i7'],
                    'img/phonecases/iphone7-Plus_03.jpg' => ['alt' => '', 'imgid' => 'i7p'],
                    //'img/phonecases/iphonex.jpg' => ['alt' => '', 'imgid' => 'ix'],
                    'img/phonecases/samsung-S7_03.jpg' => ['alt' => '', 'imgid' => 's7'],
                    'img/phonecases/Samsung-S7-Edge_03.jpg' => ['alt' => '', 'imgid' => 's7e'],
                ],
                'products' => [
                    'i6' => [
                        'price' => '39.95',
                        'url' => '#',
                        'image' => 'i6',
                        'label' => 'iPhone 6',
                        'finish' => 'Sleek Gloss',
                    ],
                    'i6p' => [
                        'price' => '44.95',
                        'url' => '#',
                        'image' => 'i6p',
                        'label' => 'iPhone 6 Plus',
                        'finish' => 'Sleek Gloss',
                    ],
                    'i6s' => [
                        'price' => '44.95',
                        'url' => '#',
                        'image' => 'i6s',
                        'label' => 'iPhone 6S',
                        'finish' => 'Sleek Gloss',
                    ],
                    'i6sp' => [
                        'price' => '49.95',
                        'url' => '#',
                        'image' => 'i6sp',
                        'label' => 'iPhone 6S Plus',
                        'finish' => 'Sleek Gloss',
                    ],
                    'i7' => [
                        'price' => '54.95',
                        'url' => '#',
                        'image' => 'i7',
                        'label' => 'iPhone 7',
                        'finish' => 'Sleek Gloss',
                    ],
                    'i7p' => [
                        'price' => '64.95',
                        'url' => '#',
                        'image' => 'i7p',
                        'label' => 'iPhone 7 Plus',
                        'finish' => 'Sleek Gloss',
                    ],
                    /*
                    'ix' => [
                        'price' => '54.95',
                        'url' => '#',
                        'image' => 'ix',
                        'label' => 'iPhone X',
                        'finish' => 'Sleek Gloss',
                    ],
                    */
                    's7' => [
                        'price' => '39.95',
                        'url' => '#',
                        'image' => 's7',
                        'label' => 'Samsung S7',
                        'finish' => 'Sleek Gloss',
                    ],
                    's7e' => [
                        'price' => '44.95',
                        'url' => '#',
                        'image' => 's7e',
                        'label' => 'Samsung S7 Edge',
                        'finish' => 'Sleek Gloss',
                    ],
                ],
                'extras' => [
                    'type' => [
                        'type' => 'productspecific',
                        'items' => [
                            'i6' => ['Slim Case' => 0],
                            'i6p' => ['Slim Case' => 0],
                            'i6s' => ['Slim Case' => 0, 'Tough Case' => 10],
                            'i6sp' => ['Slim Case' => 0],
                            'i7' => ['Slim Case' => 0, 'Tough Case' => 15],
                            //'ix' => ['Slim Case' => 0, 'Tough Case' => 15],
                            'i7p' => ['Slim Case' => 0, 'Tough Case' => 15],
                            's7' => ['Slim Case' => 0, 'Tough Case' => 5],
                            's7e' => ['Slim Case' => 0, 'Tough Case' => 5],
                        ],
                    ],
                ],
                'options' => [
                    'type' => 'Type',
                    'finish' => 'Finish',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Forget boring Phone Cases. Why not personalise your Phone Case with your own photos and text? <br><br>Make a statement with a custom Phone Case while protecting your phone at the same time. Choose "Slim" case for a sleek, stylish look. Or choose "Tough" case to ensure extra durability and protection for your phone.',
                'urls' => [
                    'formula' => 'size|type',
                    'values' => [
                        'iPhone 6|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=29.YmE0YzI2MDU,.aRZL_RHLMkQFO6g0jbDJuy3u9G3R-4x2_714HtITxjI,',
                        'iPhone 6 Plus|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=30.ODY2OGUxYTY,.63ND20FU9fPR-1_OGnJw2DwCyo1dt3Lq4-baOrbrxEI,',
                        'iPhone 6S|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.NjgwODA5N2M,.LDPHC9yviwUCBv2-H0vhFtRDAshfF6EDxNg72Dl12UY,',
                        'iPhone 6S Plus|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.ZDZkNTU4OGM,.R0Q5LrLhXGN7WW2RckzR2RlM_KUa2xmEB4eeR07Ud0GO3v_ccVhGEA,,',
                        'iPhone 7|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.ZDY4NDYxYmQ,.9ory3EwGxXVQYqvN1-1xMfKC_eIbwkQftqz1hEC2dvQ,',
                        'iPhone 7 Plus|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.YjUyMTYyMzY,.YPXQkEw0GOC0Mu3NWa9YEpyLsB3uk1FSRjoXVtd3t8I,',
                        //'iPhone X|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=46.YzdiYzNkODA,._1QIK-rI7ucKgokBOtxZ3MroPEJMLU160t3dSXJrwMpZ_58Vy7dvLSfRbCUwHPrm',
                        'Samsung S7|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.YjljNmUwNTA,.zCvuhhHhV-KRGfc6wYwk_BzmmOYNMJT9b7y3szHtAkc,',
                        'Samsung S7 Edge|Slim Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.MjdlYjg5YmE,.0naaOSx0tPwKZmxx2rBIwdUFu4urKuF_5BNG0yF_Wjs,',
                        'iPhone 6S|Tough Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.MzYyNjhkMjM,.I6AvqKbYoju0AT0wTCciI8wv_hQSPLK6yci9OPoE5Hqi8EfDEQz3gw,,',
                        'iPhone 7|Tough Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=35.YzU3ODcxYzI,.B-R4PUD2cC9M2wAlRTUcyjB7FmZi245bjOtCda4Al3hKCfZbnxJg3w,,',
                        'iPhone 7 Plus|Tough Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.NDkxYWU2ZTg,.qGU-wKM1nfxo50qgPjF3Dj7UVcrPsRFDy5sZE2pPSfQOX6MqLP6CEQ,,',
                        //'iPhone X|Tough Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.YjY5OTA1ODY,.EtTd1nF-X5s_m9CWl3nd8-AhOMrIVFDRopWwSpxiPS9ZFFVxY0hWxqfWw49TzV5YX2evDDklQg4,',
                        'Samsung S7|Tough Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=35.YTkxYTQwZTE,.xpVBFSMMiaaWJyUhum2XwuXAkOlrtL7hlss-LUeSWQoitRhXR0km7g,,',
                        'Samsung S7 Edge|Tough Case' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=36.MjQ3MmRjM2I,.S1-7UZ3M3JIYOGB4-v5Sa_435mi5OzIcMgNXCMoy4RfcWIRa7njhBA,,',                        
                    ]
                ],                
                'prices' => [
                    [
                        ['','SLIM CASE','TOUGH CASE'],
                        ['IPHONE 6','$39.95','N/A'],
                        ['IPHONE 6 PLUS','$44.95','N/A'],
                        ['IPHONE 6S','$44.95','$54.95'],
                        ['IPHONE 6S PLUS','$49.95','N/A'],
                        ['IPHONE 7','$54.95','$69.95'],
                        ['IPHONE 7 PLUS','$64.95','$79.95'],
                        //['IPHONE X','$54.95','$69.95'],
                        ['SAMSUNG S7','$39.95','$44.95'],
                        ['SAMSUNG S7 EDGE','$44.95','$49.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $7.95','+ $6.00'],
                ],
            ];
            break;            
                      
        case 'serving-trays-create-now':
            $__pagedata = [
                'title' => 'Serving Trays',
                'document_title' => 'Serving Trays | albumworks',
                'images' => [
                    'img/trays/serving-tray_03.jpg' => ['alt' => '', 'imgid' => ''],
                ],
                'products' => [
                    '15x10' => [
                        'price' => '59.95',
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=35.N2E3NWIzNDM,.zT7jmK8UQPqQXj2pWwD_8iYmbvwtooELMIVGrM725J_7Ed6YMyzUag,,',
                        'label' => '15 x 10" Serving Tray',
                    ],
                ],
                'options' => [
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Create a truly unique gift with your very own Serving Tray. Your photos and text are printed on the hardboard inlay panel. From breakfast in bed to dinner time, these Serving Trays make meal times that little more special.<br><br>Made from oak, Serving Trays come disassembled and will need a Phillips screwdriver to put together',
                'prices' => [
                    [
                        ['','SERVING TRAY'],
                        ['15 x 10"','$59.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $12.95','+ $8.00'],
                ],
            ];
            break;            
                      
        case 'tshirts-create-now':
            $__pagedata = [
                'title' => 'T-Shirts',
                'document_title' => 'T-Shirts | albumworks',
                'images' => [
                    'img/shirts/White_03.jpg' => ['alt' => '', 'imgid' => 'white'],
                    'img/shirts/Black_03.jpg' => ['alt' => '', 'imgid' => 'black'],
                    'img/shirts/Grey_03.jpg' => ['alt' => '', 'imgid' => 'grey'],
                    'img/shirts/sizes.jpg' => ['alt' => '', 'imgid' => ''],
                ],
                'products' => [
                    'white' => [
                        'price' => 39.95,
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.MTUxMDU5NjI,.pvxO6eq4GgqmrBHfhlXVail0xSSeci_4-tyIJLoj4D4,',
                        'label' => 'White',
                    ],
                    'black' => [
                        'price' => 49.95,
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.MDc3MzA1ZmQ,.23W5fZgSDZZy11V95A36HXfmZVJ4cCmnO2bOCgCeSGQ,',
                        'label' => 'Black',
                    ],
                    'grey' => [
                        'price' => 49.95,
                        'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.YWMzYjdlNjQ,.15-elmSYwB--v6EOquGnyjMbPS-N3l54JlzwbXHILys,',
                        'label' => 'Grey',
                    ],
                ],
                'primary' => ['colour', 'Colour'],
                'extras' => [
                    'size' => [
                            'XS' => 0,
                            'S' => 0,
                            'M' => 0,
                            'L' => 0,
                            'XL' => 0,
                            'XXL' => 5,
                            'XXXL' => 5,
                    ],
                ],
                'options' => [
                    'size' => 'Size',
                    'price' => 'Price',
                ],
                'flashpromotion' => ((date('Y-m-d') == '2017-12-10') ? [
                    'message' => 'SAVE <strong>30%*</strong> OFF GIFTS WITH VOUCHER CODE:',
                    'promocode' => 'GIFTS30'
                ] : ''),
                'about' => 'Personalise your wardrobe with your very own T-Shirt designs! Our T-Shirts are 100% cotton and are available in 3 base colours: White, Black and Grey. Design area is 297 x 297mm.<br><br>Create a one-of-a-kind gift or create a set of T-Shirts for your community event or sports team. With sizes from XS up to XXL, our T-Shirts are designed for everyone!',
                'prices' => [
                    [
                        ['','WHITE','BLACK','GREY'],
                        ['XS - XL','$39.95','$49.95','$49.95'],
                        ['XXL - XXXL','$44.95','$54.95','$54.95'],
                    ]
                ],
                'shipping' => [
                    ['','FIRST COPY','EXTRA COPIES'],
                    ['ALL SIZES','+ $7.95','+ $6.00'],
                ],
            ];
            break;            

    }

    return $__pagedata;
}

function fetchInspirationPageMetadData($page){
    $title = $description = $sf_url = $albumshare_url = null;
    switch($page) {
        case 'inspiration-travel':
            $title = 'Travel Photo Book';
            $description = "Have you had an amazing adventure, and now you have hundreds of beautiful photos? Don't leave them on your computer, turn them into a stunning Photo Book. The perfect way to remember all those sunsets, beaches and delicious meals. With a huge library of travel themed backgrounds and scrapbooking items your can put those perfect final touches to you very own Travel Photo Book.";
            $albumshare_url = 'https://shareframe.photo-products.com.au/p/5211652e0b6f9b12bdfa657deb5372919f689cbb&embed';
            break;
        case 'inspiration-weddings':
            $title = 'Wedding Photo Book';
            $description = "Remember your special day the way that it should be. Make a beautiful Photo Book and put all of your wonderful wedding photos together in one place. A perfect gift for your in-laws, and the best way to treasure the memories of a day you'll never forget. Fully customise your Photo Book with our library of backgrounds and scrapbooking items. Click “Get Started” to download our Editor.";
            $albumshare_url = 'https://shareframe.photo-products.com.au/p/3e38d84f571827ecc17f0be2c4c6e4606759e58f&embed';
            break;
        case 'inspiration-family':
            $title = 'Family Photo Book';
            $description = "You've got hundreds of photos of your family from birthdays, weekends at the beach, and just being hilarious around the house. Every moment precious, and memorable. Why not turn them into a beautiful Photo Book? Flick through our Gallery to get inspired, and download our Editor to “Get Started”.";
            $albumshare_url = 'https://shareframe.photo-products.com.au/p/f80f36fbfdd73213394dce9ecf30231cfbdd10c9&embed';
            break;
        case 'inspiration-baby':
            $title = 'Baby Photo Book';
            $description = "The magic moments when you first brought your new baby home, his first crawl, her first steps? You've captured them all in lots and lots of precious photos. Give them the justice they deserve, and turn them into a beautiful Photo Book. Flick through our Gallery to get inspired, and download our Editor to “Get Started”.";
            $albumshare_url = 'https://shareframe.photo-products.com.au/p/e4e11b516a6ff51c3d6a96dca62e9986952c55da&embed';
            break;
    }
    return $share_page_data = array(
                                'title' => $title,
                                'description' => $description,
                                'sf_url' => $sf_url,
                                'albumshare_url' => $albumshare_url,
                                'page' => $page
                            );
}

function convertToSQLite(){
    exec('rm db/albumworks.db; ./db/mysql2sqlite.sh -u '.getenv('DB_USERNAME').' -p'.getenv('DB_PASSWORD').' '.getenv('DB_DATABASE').' | sqlite3 db/albumworks.db 2>&1', $output1, $returnStatus1);
    if(!(empty($output1) or ((sizeof($output1) === 1) and (trim($output1[0]) === 'memory')))){
        return $output1;
    }
    else{
        return true;
    }

}

function commitChanges($commit_message){
    exec('git add -A 2>&1', $output2, $returnStatus2);
    exec('git commit . -m "'.str_replace('"', '', $commit_message).'" 2>&1', $output2, $returnStatus2);
    exec('git push 2>&1', $output3, $returnStatu3);
    return array($output2, $output3);
}

function custom_escape_string($value)
{
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $value);
}

function checkPageRedirection($page){
    $page_check = str_replace('-', '', $page);
    if(strpos($page_check, 'about') !== false)
        return 'about';
    elseif(strpos($page, 'gift-voucher-complete') !== false)
        return 'thankyou-vouchers';
    elseif(strpos($page_check, 'inspiration') !== false)
        return 'photo-books';
    elseif(strpos($page_check, 'support') !== false)//TODO : check this
        return 'faq';
    /*elseif(strpos($page_check, 'photobook') !== false)
        return 'photo-books';*/
    elseif(strpos($page_check, 'voucher') !== false) //TODO : check this
        return 'vouchers';
    elseif(strpos($page, 'calendar') !== false)
        return 'wall-calendar';
    elseif(strpos($page_check, 'canvas') !== false)
        return 'canvas-photo-prints';
    elseif(strpos($page_check, 'poster') !== false)
        return 'posterprinting';
    elseif(strpos($page_check, 'photoflip') !== false)
        return 'desk-prints';
    elseif(strpos($page_check, 'balance') !== false)
        return 'rewards';
    elseif(strpos($page_check, 'blog/tag/offers') !== false)
        return '1';
    elseif(strpos($page_check, 'blog/tag/tips') !== false)
        return '5';
    elseif(strpos($page_check, 'blog/tag/photography') !== false)
        return '6';
    elseif(strpos($page_check, 'blog/tag/photo+competitions') !== false)
        return '8';
    elseif(strpos($page_check, 'blog/tag/funny') !== false)
        return '4';
    elseif(strpos($page_check, 'blog/tag/protecting+your+memories') !== false)
        return '7';
    elseif(strpos($page_check, 'blog/tag/news') !== false)
        return '3';
    elseif(strpos($page_check, 'pricing') !== false) //TODO : check this
        return 'calculator';
    elseif(strpos($page_check, 'square') !== false)
        return 'instagram-prints';
    elseif(strpos($page_check, 'diary') !== false)
        return 'journals';
    elseif(strpos($page_check, 'cards') !== false)
        return 'journals';
    else
        return $page;


}