<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{
    protected $table = 'blog_tags';
    protected $fillable = [
        'blog_slug',
        'tag_id'
    ];
    public $primaryKey = false;
    public $incrementing = false;
    public $timestamps  = false;
}
