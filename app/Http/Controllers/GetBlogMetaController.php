<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogMeta;
use App\Pages;

class GetBlogMetaController extends Controller
{

    public function getBlogMeta($slug)
    {
        $meta = array();
        $blogMeta = new BlogMeta();
        $pages = new Pages();
        $pages->get_latest_blog();
        $result = $blogMeta->get_blog_meta($slug);
        foreach ($result as $key => $value) {
            // make url relative to current site; strip tags
            if($key == 'thumbnail') {
                $count   = 1;
                if(empty($value)) {
                    // if no thumbnail is specified, use the first image from content
                    $value = $pages->get_page_content($slug);
                    $value = '<p>'. strip_tags($value, '<img>') .'</p>';
                }
                $xml     = @simplexml_load_string($value);
                $json    = json_encode($xml);
                $array   = json_decode($json, true);
                $img_arr = @current($array);
                if(count($img_arr) > 1) $img_arr = current($img_arr);
                $meta[$key] = str_replace('../', '', $img_arr['@attributes'] ['src'], $count);
            }
            if($key == 'feature_text')
            {
                $meta[$key] = strlen($value) > 100 ? substr($value, 0, 100) . ' ..' : $value;
            }
        }

        return $meta;
        //return view('user.profile', ['user' => User::findOrFail($id)]);
    }


    public function getLatestBlog(){
        $blog_post = array();
        $pages = new Pages();
        $blog_post = $pages->get_latest_blog();
        if($blog_post) {
            $blog_post['meta'] = $this->getBlogMeta($blog_post['slug']);
        }
        return $blog_post;
    }
}
