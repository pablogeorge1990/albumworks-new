<?php

namespace App\Http\Controllers;

use App\BlogTags;
use Illuminate\Database\Console\Migrations\ResetCommand;
use Illuminate\Http\Request;
use App\Pages;
use App\BlogMeta;
use App\Tags;
use DB;

class AdminController extends Controller
{
    public function saveBlogTags($tags, $slug){
        $tagsModel = new Tags;
        $tagID = $tagsModel::select('id')->whereIn('name', $tags)->get()->toArray();
        foreach($tagID as $key => $value) {
            $tag_ids[] = $value['id'];
        }
        $blogTagsModel = new BlogTags;
        foreach($tag_ids as $key => $id) {
            $blogTagsModel::updateOrCreate(array('blog_slug' => $slug, 'tag_id' => $id));
        }
    }

    public function saveBlogMeta($thumbnail_path, $feature_text, $slug){
        $blogMetaModel = new BlogMeta;
        $blogMetaModel::updateOrCreate(array('blog_slug' => $slug, 'feature_text' => $feature_text, 'thumbnail' => $thumbnail_path));
    }

    public function showPushToStaging(){
        return view('admin.push');
    }

    public function cleanDB(){
        $result = DB::table('pages')->select('slug', 'content', 'introtext')->where('content', 'like', '%'."\t".'%')->get()->toArray();
        foreach($result as $row){
            DB::table('pages')->where('slug', $row['slug'])->update(['content' => str_replace("\t", " ", $row['content'])]);
            DB::table('pages')->where('slug', $row['slug'])->update(['introtext' => str_replace("\t", " ", $row['introtext'])]);
        }

        $result = DB::table('jos_content')->select('id', 'introtext', 'fulltext')->where('introtext', 'like', '%'."\t".'%')->orWhere('fulltext', 'like', '%'."\t".'%')->get()->toArray();
        foreach($result as $row){
            DB::table('jos_content')->where('id', $row['id'])->update(['introtext' => str_replace("\t", " ", $row['introtext'])]);
            DB::table('jos_content')->where('id', $row['id'])->update(['fulltext' => str_replace("\t", " ", $row['fulltext'])]);
        }
        $result = DB::table('pages')->where('content','=', "")->where('created', '=', '0000-00-00 00:00:00')->delete();
    }

    public function pushToStaging(Request $request){
        $commit_message = $request->get('commit');
        if(strlen($commit_message) == 0 )
            dd('Error : Please provide a valid commit message');
        $this->cleanDB();
        $convertDb = convertToSQLite();
        if($convertDb !== true){
            echo "Error : Converting to SQLite DB";
            dd($convertDb);
        }
        else{
            $commitChanges = commitChanges($commit_message);
        }
        return view('admin.commitchanges', ['commitChanges'=> $commitChanges]);
    }
    
    public function getBlogTagsMap($blogpage){
        $blog_tag = $blog_tag_name = null;
        $blogTagsModel = new BlogTags;
        $blog_tag_id = $blogTagsModel::select('tag_id')->where('blog_slug', $blogpage)->get()->toArray();


        $tagsModel = new Tags;
        $blog_tags_fetch_all = $tagsModel::all()->toArray();
        foreach($blog_tags_fetch_all as $key => $value) {
            $blog_tags_all[$key]['tag'] = $value['name'];
            $blog_tags_all[$key]['checked'] = '';
        }
        foreach($blog_tag_id as $key => $value) {
            $blog_tag_name[] = $tagsModel::find($value['tag_id'])->toArray();
        }
        if(sizeof($blog_tag_name) > 0) {
            foreach ($blog_tag_name as $tagkey => $tagvalue) {
                $blog_tag = $tagvalue['name'];
                foreach($blog_tags_all as $key => &$value){
                    if($value['tag'] == $blog_tag) {
                        $value['checked'] = 'checked';
                    }
                }
            }
        }
        return $blog_tags_all;
    }

    public function showAdmin($blogpage = null)
    {
        if($blogpage == null){
            $pagesModel = new Pages;
            $pages = $pagesModel->get_pages();
            return view('admin.index', ['pages' => $pages]);
        }
        else{//return blog page url

            $pagesModel = new Pages;
            $pages = $pagesModel->get_page_from_slug($blogpage);
            $blog_tags_all = $this->getBlogTagsMap($blogpage);

            $metaModel = new BlogMeta;
            $meta_data = $metaModel->get_blog_meta($blogpage);
            $blog_thumbnail = 'adsad.jpg';
            $blog_feature_text =  'test text';
            return view('admin.editblog', ['blogpage' => $blogpage, 'blogtags' => $blog_tags_all, 'pages' => $pages, 'blog_thumbnail' => $meta_data['thumbnail'], 'blog_feature_text' => $meta_data['feature_text']]);
        }
    }

    public function newBlogPage(Request $request){
        $page = $request->get('page');
        $pagesModel = new Pages;
        $pagesModel->slug = $page;
        $pagesModel->contentid = 0;
        $pagesModel->itemid = 0;
        $pagesModel->title = '';
        $pagesModel->content = '';
        $pagesModel->introtext = '';
        $pagesModel->type = '';
        $pagesModel->meta_title = '';
        $pagesModel->meta_desc = '';
        $pagesModel->meta_kw = '';
        $pagesModel->created = date('Y-m-d H:i:s');
        $pagesModel->last_modified = date('Y-m-d H:i:s');
        $pagesModel->publish_up = '2000-01-01 00:00:00';
        $pagesModel->publish_down = '2000-01-01 00:00:00';
        $pagesModel->status = 0;
        $pagesModel->hits = 0;
        $pagesModel->save();
        return redirect()->route('view_blog', ['blogpage' => $page]);
    }

    public function previewBlogPage(Request $request, $blogpage){
        $request_params = $request->except('_token');
        $pagesModel = new Pages;
        $pages = $pagesModel->get_page_from_slug($request['slug']);
        return view('admin.previewblog', ['params'=> $request_params, 'pages' => $pages, 'blogpage' => $blogpage]);
    }

    public function saveBlogPage(Request $request){
        $pagesModel = new Pages;
        $existing_blog = $pagesModel->get_page_from_slug(base64_decode($request['slug']));
        if(sizeof($existing_blog) > 0) {
            $existing_page = Pages::find(base64_decode($request['slug']));
            //$existing_page = Pages::where('slug', '=', base64_decode($request['slug']))->first();
            $existing_page->contentid = base64_decode($request['contentid']);
            $existing_page->itemid = base64_decode($request['itemid']);
            $existing_page->title = base64_decode($request['title']);
            $existing_page->content = base64_decode($request['content']);
            $existing_page->introtext = base64_decode($request['introtext']);
            $existing_page->type = base64_decode($request['type']);
            $existing_page->meta_title = base64_decode($request['meta_title']);
            $existing_page->meta_desc = base64_decode($request['meta_desc']);
            $existing_page->meta_kw = base64_decode($request['meta_kw']);
            $existing_page->created = base64_decode($request['created']);
            $existing_page->last_modified = base64_decode($request['last_modified']);
            $existing_page->publish_up = base64_decode($request['publish_up']);
            $existing_page->publish_down = base64_decode($request['publish_down']);
            $existing_page->status = base64_decode($request['status']);
            $existing_page->hits = base64_decode($request['hits']);
            $existing_page->save();
        }
        else{
            $pagesModel->slug = base64_decode($request['slug']);
            $pagesModel->contentid = base64_decode($request['contentid']);
            $pagesModel->itemid = base64_decode($request['itemid']);
            $pagesModel->title = base64_decode($request['title']);
            $pagesModel->content = base64_decode($request['content']);
            $pagesModel->introtext = base64_decode($request['introtext']);
            $pagesModel->type = base64_decode($request['type']);
            $pagesModel->meta_title = base64_decode($request['meta_title']);
            $pagesModel->meta_desc = base64_decode($request['meta_desc']);
            $pagesModel->meta_kw = base64_decode($request['meta_kw']);
            $pagesModel->created = base64_decode($request['created']);
            $pagesModel->last_modified = base64_decode($request['last_modified']);
            $pagesModel->publish_up = base64_decode($request['publish_up']);
            $pagesModel->publish_down = base64_decode($request['publish_down']);
            $pagesModel->status = base64_decode($request['status']);
            $pagesModel->hits = base64_decode($request['hits']);
            $pagesModel->save();

        }

        $tags = json_decode($request['tags']);

        if(sizeof($tags) > 0){
            $this->saveBlogTags($tags, base64_decode($request['slug']));
        }

        $this->saveBlogMeta(base64_decode($request['thumbnail']), base64_decode($request['feature_text']), base64_decode($request['slug']));
        return redirect()->route('view_blog', ['blogpage' => base64_decode($request['slug'])]);
    }
}
