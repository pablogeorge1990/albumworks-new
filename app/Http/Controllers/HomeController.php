<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages;
use App\Tags;

class HomeController extends Controller
{
    public function showHome(){
        $pagesModel = new Pages;
        $latest_blog = $pagesModel->get_latest_blog();
        return view('home', ['latest_blog' => $latest_blog]);
    }

    public function showBlog($slug){
        $pagesModel = new Pages;
        $blog_content = $pagesModel->get_page_content($slug);
        //update hits;
        $pagesModel->increment_view_count($slug);
        return view('pages.viewblog', ['content' => $blog_content]);
    }

    public function showBlogList(){
        $pagesModel = new Pages;
        $paginated_blog = $pagesModel->get_paginated_blog(2);
        $newest_blogs = $pagesModel->get_pages_by_youth(5);
        $tags = new Tags;
        $tag_list = $tags->get_tag_list();
        //dd($paginated_blog);
        return view('pages.blogs', ['paginated_blogs' => $paginated_blog, 'newest_blogs' => newest_blogs, 'tags' => $tag_list]);
    }

    public function showBlogListByTag($tag){
        $pagesModel = new Pages;
        $blog_by_tag = $pagesModel->get_pages_by_tag($tag);
        $newest_blogs = $pagesModel->get_pages_by_youth(5);
        $tags = new Tags;
        $tag_list = $tags->get_tag_list();
        return view('pages.blogs', ['paginated_blogs' => $blog_by_tag, 'newest_blogs' => $newest_blogs, 'tags' => $tag_list]);
    }
}
