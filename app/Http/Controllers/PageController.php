<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages;
use App\Tags;
use Illuminate\Support\Facades\View;
class PageController extends Controller
{

    public function showPage($page = null, $slug = null){

        $pagesModel = new Pages;
        $blog_slugs = $pagesModel->get_pages();

        $tags = new Tags;
        $tag_list_by_id = $tags->get_tag_list_by_id();

        $landing_pages = array( 'desk-prints',
                                'desk-calendar',
                                'hd-calendar',
                                'wall-calendar',
                                'canvas-photo-prints',
                                'framed-prints',
                                'instagram-prints',
                                'photo-magnets',
                                'poster-printing',
                                'premium-prints',
                                'wood-prints',
                                'journals',
                                'notebooks',
                                'guest-books',
                                'luggage-tags',
                                'tote-bags'
                            );

        $retail_pages = array( 'desk-prints-create-now',
                                'desk-calendar-create-now',
                                'hd-calendar-create-now',
                                'wall-calendar-create-now',
                                'canvas-photo-prints-create-now',
                                'framed-prints-create-now',
                                'instagram-prints-create-now',
                                'photo-magnets-create-now',
                                'poster-printing-create-now',
                                'premium-prints-create-now',
                                'wood-prints-create-now',
                                'journals-create-now',
                                'notebooks-create-now',
                                'guest-books-create-now',
                                'luggage-tags-create-now',
                                'tote-bags-create-now',
                                'photo-mugs-create-now',
                                'jigsaw-puzzles-create-now',
                                'photo-magnets-create-now',
                                'cushions-create-now',
                                'desktop-plaques-create-now',
                                'serving-trays-create-now',
                                'phone-cases-create-now',
                                'greeting-cards-create-now',                                
                            );

        $inspiration_pages = array( 'inspiration-travel',
                                    'inspiration-weddings',
                                    'inspiration-family',
                                    'inspiration-baby'
                            );
        //$inspiration_pages = array();
        
        if($page == null)
            return view('pages.homepage');
        if($page == 'calculator') {
            $pricing_data = get_pricing_data();
            return view('pages.calculator')->with('pricing', $pricing_data);
        }
        elseif($page == 'mybalance') {
            if(isset($_GET['user']))
                return view('pages.mybalance');
            else return redirect('rewards');
        }
        elseif($page == 'faq'){
            $c = fetchFaqData();
            return view('pages.faq', ['c' => $c]);
        }
        elseif(in_array($page, $landing_pages)) {
            $__pagedata = fetchLandingPageMetaData($page);
            return view('pages.landing', ['__pagedata' => $__pagedata]);
        }
        elseif(in_array($page, $retail_pages)){
            $__pagedata = fetchRetailPageMetaData($page);
            return view('pages.retail', ['__pagedata' => $__pagedata]);
        }
        elseif(in_array($page, $inspiration_pages)){
            $__pagedata = fetchInspirationPageMetadData($page);
            return view('pages.inspiration', ['__pagedata' => $__pagedata]);
        }
        elseif(in_array($page, $blog_slugs)){
            $pagesModel = new Pages;
            $blog_publishup = $pagesModel->get_page_publishup($page);
            $blog_title = $pagesModel->get_page_title($page);
            $blog_content = $pagesModel->get_page_content($page);
            $pagesModel->increment_view_count($page);
            return view('pages.viewblog', ['publish_up' => $blog_publishup, 'title' => $blog_title, 'content' => $blog_content]);
        }
        elseif(in_array($page, $tag_list_by_id)){
            $pagesModel = new Pages;
            $blog_by_tag = $pagesModel->get_pages_by_tag($page);
            $newest_blogs = $pagesModel->get_pages_by_youth(5);
            $tag_list_by_name = $tags->get_tag_list();
            return view('pages.blogs', ['paginated_blogs' => $blog_by_tag, 'newest_blogs' => $newest_blogs, 'tags' => $tag_list_by_name]);
        }
        elseif($page == 'blog' and $slug == null){
            $pagesModel = new Pages;
            $paginated_blog = $pagesModel->get_paginated_blog(2);
            $newest_blogs = $pagesModel->get_pages_by_youth(5);
            $tag_list_by_name = $tags->get_tag_list();//9568
            return view('pages.blogs', ['paginated_blogs' => $paginated_blog, 'newest_blogs' => $newest_blogs, 'tags' => $tag_list_by_name]);
        }
        elseif($page == 'track' and isset($_GET['orderid']) and isset($_GET['email'])){
            $dump = file_get_contents('http://zen-admin:hQi2k9nu_p@zen.photogro.com/pgtrack/orderStatus.php?orderid='.urlencode($_GET['orderid']).'&email='.urlencode($_GET['email']));
            $trackdata = json_decode($dump);

            if($trackdata === null && json_last_error() !== JSON_ERROR_NONE)
                return view('pages.track', ['error' => $dump]);
            return view('pages.track', ['trackdata' => $trackdata]);
        }
        elseif(file_exists(resource_path('views/pages/'.$page.'.blade.php'))){ //check if page exists
            $page = str_replace('.html', '', $page);
            return view('pages.'.$page);
        }
        else{ //check for old site redirection
            //$page = str_replace('.html', '', $page);
            //$page = checkPageRedirection($page);
            
            if(in_array($page, $landing_pages)) {
                $__pagedata = fetchLandingPageMetaData($page);
                return view('pages.landing', ['__pagedata' => $__pagedata]);
            }
            elseif(in_array($page, $retail_pages)){
                $__pagedata = fetchRetailPageMetaData($page);
                return view('pages.retail', ['__pagedata' => $__pagedata]);
            }
            elseif(file_exists(resource_path('views/pages/'.$page.'.blade.php'))) { //check if page exists
                $page = str_replace('.html', '', $page);
                return view('pages.' . $page);
            }
            else
                return view('pages.' . '404');
        }
    }

    function showConfirmedPage($orderId){
        if(empty($orderId))
            $orderId = "X-X-X-XX-XX-X";
        return view('pages.order-confirm', compact('orderId'));
    }
}
