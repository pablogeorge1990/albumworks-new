<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogMeta extends Model
{
    protected $table = 'blog_meta';
    protected $fillable = [
        'blog_slug',
        'feature_text',
        'thumbnail'
    ];

    public $primaryKey = 'blog_slug';
    public $timestamps  = false;
    public $incrementing = false;

    public function get_blog_meta($slug)
    {
        $blog_meta = new BlogMeta;
        $result = $blog_meta::where('blog_slug', $slug)->first(['feature_text', 'thumbnail']);
        if($result == null)
            return null;
        return $result->getAttributes();
    }

}
