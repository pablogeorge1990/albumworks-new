<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'tags';
    public $primaryKey = 'id';
    public $timestamps  = false;

    public function get_tag_list(){
        $result = Tags::all()->toArray();
        return $result;
    }
    public function get_tag_list_by_id(){
        $result = Tags::all()->pluck('id')->toArray();
        return $result;
    }

}
