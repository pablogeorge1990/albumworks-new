<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Pages extends Model
{
    protected $table = 'pages';
    public $primaryKey = 'slug';
    public $incrementing = false;
    public $timestamps  = false;

    public function get_page_title($slug)
    {
        $pages = new Pages;
        $result = $pages::where('slug', $slug)->first(['title']);
        return $result->getAttributes()['title'];
    }

    public function get_page_publishup($slug)
    {
        $pages = new Pages;
        $result = $pages::where('slug', $slug)->first(['publish_up']);
        return $result->getAttributes()['publish_up'];
    }

    public function get_page_content($slug)
    {
        $pages = new Pages;
        $result = $pages::where('slug', $slug)->first(['content']);
        return $result->getAttributes()['content'];
    }

    public function get_latest_blog(){
        $blog_post = array();
        $pages = new Pages;
        $result = $pages::where('type', 'blog')->orWhere('type', 'sandra')
                        ->where('publish_up', '<', date('Y-m-d H:i:s'))
                        //->where('publish_down', '0000-00-00 00:00:00')
                        //->orWhere('publish_down', date('Y-m-d H:i:s'))
                        ->where('status', 1)
                        ->orderBy('publish_up', 'desc')
                        ->first();
        return $result->getAttributes();
    }

    public function get_pages(){
        $pages = array();
        $result = Pages::get(['slug'])->toArray();
        foreach($result as $row){
            $pages[] = $row['slug'];
        }
        return $pages;
    }

    public function get_paginated_blog($results_per_page = 2){
        $paginated_blog = pages::where('type', 'blog')->orWhere('type', 'sandra')
            ->where('publish_up', '<', date('Y-m-d H:i:s'))
            ->where('status', 1)
            ->orderBy('publish_up', 'desc')->simplePaginate(2);
        return $paginated_blog;
    }
    public function get_page_from_slug($slug){
        $pages = array();
        $result = Pages::all()->where('slug', $slug)->toArray();
        return @current($result);
    }

    public function get_pages_by_hits($limit = 5){
        $pages = new Pages;
        $result = $pages::where('type', 'blog')->orWhere('type', 'sandra')
            ->where('publish_up', '<', date('Y-m-d H:i:s'))
            ->where('publish_down', '0000-00-00 00:00:00')
            ->orWhere('publish_down', date('Y-m-d H:i:s'))
            ->where('status', 1)
            ->orderBy('hits', 'desc')
            ->limit($limit)->get()->toArray();
        return $result;
    }

    public function get_pages_by_youth($limit = 5){
        $pages = new Pages;
        $result = $pages::where('type', 'blog')->orWhere('type', 'sandra')
            ->where('publish_up', '<', date('Y-m-d H:i:s'))
            ->where('status', 1)
            ->orderBy('publish_up', 'desc')
            ->limit($limit)->get()->toArray();
        return $result;
    }

    public function get_pages_by_tag($tag){
        $blogtags = new BlogTags;
        $blogs_by_tag = $blogtags::where('tag_id', $tag)->pluck('blog_slug')->values()->all();
        $result = Pages::where('type', 'blog')->orWhere('type', 'sandra')
            ->whereIn('slug', array_values($blogs_by_tag))
            ->where('publish_up', '<', date('Y-m-d H:i:s'))
            ->where('publish_down', '0000-00-00 00:00:00')
            ->orWhere('publish_down', date('Y-m-d H:i:s'))
            ->where('status', 1)
            ->simplePaginate(10);
        return $result;
    }

    public function increment_view_count($slug){
        $current_views = Pages::where('slug', $slug)->first(['hits']);
        $current_views = $current_views->getAttributes()['hits'];
        $updated_views = $current_views+1;
        Pages::where('slug', $slug)->update(['hits' => $updated_views]);
    }

}