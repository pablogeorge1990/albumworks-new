<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Middleware\VerifyAdmin;

/*\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
    var_dump($query->sql);
    var_dump($query->bindings);
    var_dump($query->time);
});*/

//admin group

Route::group(['middleware' => ['verifyadmin']], function (){
    Route::get('/admin/push', 'AdminController@showPushToStaging')->name('show_push_to_staging');
    Route::post('/admin/push', 'AdminController@pushToStaging')->name('push_to_staging');
    Route::get('/admin/{blogpage?}', 'AdminController@showAdmin')->name('view_blog');
    Route::get('/admin/create/newblogpage', 'AdminController@newBlogPage')->name('create_blog');
    Route::post('/admin/preview/{blogpage}', 'AdminController@previewBlogPage')->name('preview_blog');
    Route::post('/admin/save/{blogpage}', 'AdminController@saveBlogPage')->name('save_blog');
});

//Route::get('/blog', 'HomeController@showBlogList');
//Route::get('/blog/{slug}', 'HomeController@showBlog');
//Route::get('/blog/tags/{tag}', 'HomeController@showBlogListByTag');

Route::get('/addedtocart/hlcreate.html', function(Request $request){
    return redirect()->to('/hlcreate.html?mawebhlcreate='.$_GET['mawebhlcreate']);
});

Route::get('/addedtocart', function(Request $request){
    if(isset($_GET['odlo']) and $_GET['odlo']){
        return redirect()->to('/');
    }
    return view('pages/addedtocart');
});

Route::get('/empty-cart', function(Request $request){
    return view('pages/empty-cart');
});

Route::get('/order-confirmed/{orderId}', 'PageController@showConfirmedPage');


Route::get('/{page?}/{slug?}',  'PageController@showPage')->name('show_page');

Route::any('/', array( 'as' => 'home', 'uses' => 'PageController@showPage' ));