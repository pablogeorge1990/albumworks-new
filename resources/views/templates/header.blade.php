<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="google-site-verification" content="4JSuifECf7bPNYfL6a7c4A9sDdrQO4U36rnxelCqHRI" />
    <meta name="google-site-verification" content="ll9fy2PJ_be1gEHkmm-43SBEPZtrv1zt0HNIAJ0ZKiw" />
    <meta name="robots" content="index, follow" />

    <title>@yield('title', 'Photo Books: Make your own personalised photo keepsake with albumworks')</title>

    <link rel="shortcut icon" href="{{env('BASEPATH')}}favicon.ico" />
    <meta name="twitter:description" content="@yield('meta_description', '&#128216; Create beautiful personalised Photo Books, Calendars, Cards, Canvas & other photo keepsakes. 100% design freedom, fast delivery and local support.')" />
    <meta property="og:description" content="@yield('meta_description', '&#128216; Create beautiful personalised Photo Books, Calendars, Cards, Canvas & other photo keepsakes. 100% design freedom, fast delivery and local support.')" />
    <meta name="description" content="@yield('meta_description', '&#128216; Create beautiful personalised Photo Books, Calendars, Cards, Canvas & other photo keepsakes. 100% design freedom, fast delivery and local support.')" />

    <!-- facebook verification code -->
    <meta name="facebook-domain-verification" content="4fnk9nnm9ud9womsbbxlpz4dqf3p30" />

    <link rel="stylesheet" type="text/css" media="screen" href="{{env('BASEPATH')}}css/tooltipster.bundle.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{env('BASEPATH')}}css/common.css?<?=time()?>" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{env('BASEPATH')}}css/responsive.min.css?<?=time()?>" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{env('BASEPATH')}}css/basicModal.min.css" />

    <!-- Optimization -->
    <link rel="preconnect" href="https://manager.eu.smartlook.cloud">
    <link rel="preconnect" href="https://www.google-analytics.com">
    <link rel="preconnect" href="https://z.moatads.com">
    <link rel="preconnect" href="https://googleads.g.doubleclick.net">
    <link rel="preconnect" href="https://rec.smartlook.com">
    <link rel="preconnect" href="https://secure.livechatinc.com">
    <link rel="preconnect" href="https://api.livechatinc.com">
    <link rel="preconnect" href="https://adservice.google.com">
    <link rel="preconnect" href="https://adservice.google.com.ph">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://www.google.com">
    <link rel="preconnect" href="https://www.googletagmanager.com">
    <link rel="preconnect" href="https://www.google.com.ph">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://cdn.livechatinc.com">
    <link rel="preconnect" href="https://www.facebook.com">
    <link rel="preconnect" href="http://www2.mousestats.com">
    <link rel="preconnect" href="https://widget.trustpilot.com">
    <link rel="preconnect" href="http://8546855.fls.doubleclick.net">
    <link rel="preconnect" href="https://connect.facebook.net">
    <link rel="preconnect" href="https://bat.bing.com">
    <link rel="preconnect" href="https://stats.g.doubleclick.net">
    <link rel="preconnect" href="http://cdn.livechatinc.com">
    <link rel="preconnect" href="https://events-writer.smartlook.com">
    <link rel="preconnect" href="https://web-writer.sg.smartlook.cloud">

    <link rel="dns-prefetch" href="https://manager.eu.smartlook.cloud">
    <link rel="dns-prefetch" href="https://www.google-analytics.com">
    <link rel="dns-prefetch" href="https://z.moatads.com">
    <link rel="dns-prefetch" href="https://googleads.g.doubleclick.net">
    <link rel="dns-prefetch" href="https://rec.smartlook.com">
    <link rel="dns-prefetch" href="https://secure.livechatinc.com">
    <link rel="dns-prefetch" href="https://api.livechatinc.com">
    <link rel="dns-prefetch" href="https://adservice.google.com">
    <link rel="dns-prefetch" href="https://adservice.google.com.ph">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="dns-prefetch" href="https://www.google.com">
    <link rel="dns-prefetch" href="https://www.googletagmanager.com">
    <link rel="dns-prefetch" href="https://www.google.com.ph">
    <link rel="dns-prefetch" href="https://fonts.googleapis.com">
    <link rel="dns-prefetch" href="https://cdn.livechatinc.com">
    <link rel="dns-prefetch" href="https://www.facebook.com">
    <link rel="dns-prefetch" href="http://www2.mousestats.com">
    <link rel="dns-prefetch" href="https://widget.trustpilot.com">
    <link rel="dns-prefetch" href="http://8546855.fls.doubleclick.net">
    <link rel="dns-prefetch" href="https://connect.facebook.net">
    <link rel="dns-prefetch" href="https://bat.bing.com">
    <link rel="dns-prefetch" href="https://stats.g.doubleclick.net">
    <link rel="dns-prefetch" href="http://cdn.livechatinc.com">

    <!-- End of Optimization -->
    @yield('css')

    <!-- TrustBox script -->
    <script type="text/javascript"src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
    <!-- End Trustbox script -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TS5XCJX');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook Comments Code -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1065266906824339');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1065266906824339&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- BING -->
    <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5220373"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5220373&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

    <!-- RECAPTCHA -->
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <script type="text/javascript">
        window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', '742cc0afc69d8afb67a6ac020f80347eee46766e');
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
    <script type="text/javascript" src="{{env('BASEPATH')}}js/tpxMultilineBasketAPI.js?v3"></script>
    <script type="text/javascript" src="{{env('BASEPATH')}}js/basicModal.min.js"></script>
    <script type="text/javascript" src="{{env('BASEPATH')}}js/lazysizes.min.js"></script>
</head>
<body onLoad="tpxHighLevelBasketInitialise()">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TS5XCJX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
    <? if(time() < mktime(0,0,0,12,27,2022)): ?>
    <span id="topmsg" style="background: #039c5e; width:100%; padding: 10px 0" class="nomobile">
        <div style="margin:0 auto;">
            <div style="background:url({{env('BASEPATH')}}img/tree.png) right center no-repeat; height:45px; max-width:1020px; margin:0 auto; padding: 0 40px 0 0;">
                <div style="font-size:14px; background:url({{env('BASEPATH')}}img/tree.png) left center no-repeat; padding:0 0 0 40px;">
                    <strong style="color:white">
                        CHRISTMAS ORDER DEADLINE HAS PASSED - BUT WE'RE STILL HERE, MAKING YOUR PHOTO BOOKS AND OTHER PHOTO PRODUCTS EVERY DAY EXCEPT CHRISTMAS DAY, BOXING DAY AND NEW YEAR’S DAY!
                    </strong>
                </div>
            </div>
        </div>
    </span>
    <? endif; ?>


    <span id="topmsg" style="background: #cc0066;width:100%;" class="nomobile">
        <div style="margin:0 auto;">
            <p style="font-size:14px; padding-bottom:10px; padding-top:7px;">
                <img src="https://www.albumworks.com.au/img/hc.png" style="float: left;height: 49px;">
                <strong style="color: white;font-weight: 800;">NEW Hi Colour printing uses 7 photo colour inks to bring your photos to life with exceptional punch and beautiful depth of colour.<br>
                    <span style="font-size:11px;line-height: 2;font-weight: 400;">* You may notice a slight change to colour gamut and screen pattern with this new printing press from previous High Definition printing <a href="https://www.albumworks.com.au/hi-colour" style="text-decoration: underline;color: white;font-weight:400;font-size:11px;">Learn more</a></span>
                </strong>
            </p>
        </div>
    </span>


    <? if(time() < mktime(0,0,0,6,13,2023)): ?>
    <span id="topmsg" style="background:yellow; height:auto">
        <p style="font-size:14px; padding-bottom:10px; color:black">
           Please note that on Monday the 12th of June albumworks will be closed for the King's Birthday public holiday
        </p>
    </span>
    <?php endif; ?>


    <section id="strip" class="group">
        <!-- <span id="topmsg" style="padding-bottom:10px; background:#b13d4f; height:auto">
            <p style="font-size:14px;">
                SEVERE WEATHER HAS AFFECTED ONE OF OUR FACTORIES, DELAYING SOME ORDERS.<br>
                HD PHOTO BOOKS & HD CALENDARS ARE TAKING UP TO 10 EXTRA BUSINESS DAYS TO DISPATCH. ALL OTHER PRODUCTS ARE UNAFFECTED.<br>
            </p>
        </span> -->
        <nav id="topmenu" class="group">
            <a id="hamburger" onclick="toggleburger()">&nbsp;</a>
            <a class="left" href="{{env('BASEPATH')}}contact" id="topcontact">CONTACT <span>1300 553 448</span></a>
            <a class="left" href="{{env('BASEPATH')}}faq" id="topfaq">FAQs</a>
            <a class="left" href="{{env('BASEPATH')}}track" id="toptrack">TRACK</a>
            <a class="left" href="{{env('BASEPATH')}}rewards" id="toprewards">REWARDS</a>

            <div id="tpx-basket-bar" class="tpx">
                <div id="tpx-basket-bar-inner" class="tpx tpx-bar-container tpx-clearfix">
                    <div class="tpx tpx-accountLinks">
                        <a href="#" id="tpx-projectslist" onClick="tpxMyProjectsOnClick()" class="tpx right">My Projects</a>
                        <a href="#" id="tpx-register" onClick="tpxHighLevelRegisterInitControl()" class="tpx right">Register</a>
                        <a href="#" id="tpx-signIn" onClick="tpxHighLevelSignInInitControl()" class="tpx right">Sign In</a>

                        <section id="cart">
                            <div id="cartcontents">
                                <h3>CHECK OUT</h3>
                                <span id="basketlinkli"><a class="tpx tpx-button tpx-basketButton" href="#" id="tpx-basketlink" onClick="tpxBasketOnClick()">View items in Basket <span>&#9660;</span></a></span>
                                <div id="tpx-shoppingcartcontents" class="tpx tpx-shopping-cart">
                                    <div class="tpx tpx-shopping-cart-header group">
                                        <span id="tpx-basketcountbadgeinner" class="tpx tpx-badge">0</span>
                                        <a href="#" id="tpx-emptyBasketButton" onClick="tpxHighLevelEmptyBasketControl()" class="tpx tpx-button tpx-emptycartbutton cta">Empty Basket</a>
                                    </div>
                                    <div id="tpx-basketItemListContainer" class="tpx tpx-shopping-cart-items-container">
                                        <ul id="tpx-basketItemList" class="tpx tpx-shopping-cart-items"></ul>
                                        <div id="tpx-loadingspinnercontainer" class="tpx tpx-loadingspinnercontainer"></div>
                                        <div id="tpx-empty-cart">
                                            <p>Your basket is currently empty</p>
                                        </div>
                                    </div>
                                    <a href="#" id="tpx-checkoutbutton" onClick="tpxHighLevelCheckoutControl()" class="tpx tpx-button tpx-checkout-button cta">Checkout</a>
                                </div>
                                <div id="tpx-projectlistcontents" class="tpx tpx-projectlist">
                                    <div id="tpx-projectsItemListContainer" class="tpx tpx-projectlist-items-container">
                                        <ul id="tpx-projectsItemList" class="tpx tpx-shopping-cart-items"></ul>
                                        <div id="tpx-projectloadingspinnercontainer" class="tpx tpx-loadingspinnercontainer">
                                            <span id="tpx-projectloadingspinner" class="tpx tpx-loading-spinner"></span>
                                        </div>
                                        <div id="tpx-empty-state">
                                            <p>You don't have any saved projects.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </nav>
    </section>
    <section id="primary">
        <div id="trustpilot-topbox" class="nomobile">
            <!-- TrustBox widget - Mini -->
            <div class="trustpilot-widget" data-locale="en-AU" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="55f980f20000ff000583623f" data-style-height="68px" data-style-width="120px" data-theme="light">
                <a href="https://au.trustpilot.com/review/albumworks.com.au"target="_blank">Trustpilot</a>
            </div>
            <!-- End TrustBox widget -->
        </div>
        <a href="{{env('BASEPATH')}}" id="logo"><h2>photo books</h2></a>
        <nav id="sitemenu">
            <span id="photobookmenuitem">
                <a href="javascript:void(0)">Photo Books</a>
                <div class="dropdown hoverdropdown">
                    <ul class="submenu likepers">
                        <li><a href="{{env('BASEPATH')}}photo-books">OVERVIEW</a></li>
                        <li><a href="{{env('BASEPATH')}}photo-books#quality">QUALITY</a></li>
                        <li><a href="{{env('BASEPATH')}}photo-books#paper">PAPER</a></li>
                        <li><a href="{{env('BASEPATH')}}photo-books#covers">COVERS</a></li>
                        <li><a href="{{env('BASEPATH')}}photo-books#sizing">SIZING</a></li>
                        <li><a href="{{env('BASEPATH')}}photo-books#binding">BINDING</a></li>
                        <li><a href="{{env('BASEPATH')}}photo-books#accessories">ACCESSORIES</a></li>
                        <li><a href="{{env('BASEPATH')}}calculator">PRICING</a></li>
                        <li><a href="{{env('BASEPATH')}}photo-books-howto">HOW TO MAKE</a></li>
                    </ul>

                    <div class="hoverstuff group">
                        <div class="col">
                            <h3>SIZES</h3>
                            <ul>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/size-8x6.jpg" data-heading='8 x 6" Landscape' data-body='Available in Photocover, Premium Material Cover or Softcover.'><a href="{{env('BASEPATH')}}themes?size=8x6"><strong>8 x 6"</strong> (20.5 x 15cm)</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/size-8x8.jpg" data-heading='8 x 8" Square' data-body='Available in Photocover, Premium Material Cover or Softcover.'><a href="{{env('BASEPATH')}}themes?size=8x8"><strong>8 x 8"</strong> (20.5 x 20.5cm)</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/size-8x11.jpg" data-heading='8 x 11" Portrait' data-body='Available in Photocover, Premium Material Cover or Softcover.'><a href="{{env('BASEPATH')}}themes?size=8x11"><strong>8 x 11"</strong> (20.5 x 28cm)</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/size-11x85.jpg" data-heading='11 x 8.5" Landscape' data-body='Available in Photocover, Premium Material Cover or Softcover.'><a href="{{env('BASEPATH')}}themes?size=11x85"><strong>11 x 8.5"</strong> (28 x 21.5cm)</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/size-12x12.jpg" data-heading='12 x 12" Square' data-body='Available in Photocover, Premium Material Cover or Softcover. '><a href="{{env('BASEPATH')}}themes?size=12x12"><strong>12 x 12"</strong> (30 x 30cm)</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/size-14x10.jpg" data-heading='14 x 10" Landscape' data-body='Available in Photocover or Premium Material Cover. '><a href="{{env('BASEPATH')}}themes?size=14x10"><strong>14 x 10"</strong> (35 x 25cm)</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/size-16x12.jpg" data-heading='16 x 12" Landscape' data-body='Available in Photocover or Premium Material Cover.'><a href="{{env('BASEPATH')}}themes?size=16x12"><strong>16 x 12"</strong> (40.5 x 30cm)</a></li>
                            </ul>
                        </div>

                        <div class="col">
                            <h3>THEMES</h3>
                            <ul>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/themes-everyday.jpg" data-heading='Everyday' data-body='Choose from a range of themes for everyday use. Suitable for any occasion or story.'><a href="{{env('BASEPATH')}}themes?theme=everyday">Everyday</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/themes-travel.jpg" data-heading='Travel' data-body='Capture all of your travel photos in a Travel themed Photo Book.'><a href="{{env('BASEPATH')}}themes?theme=travel">Travel</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/themes-family.jpg" data-heading='Family' data-body='All of your family photos in a beautifully designed Photo Book.'><a href="{{env('BASEPATH')}}themes?theme=family">Family</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/themes-children.jpg" data-heading='Children' data-body='Celebrate your loved ones in a Photo Book full of modern page layouts.'><a href="{{env('BASEPATH')}}themes?theme=children">Children</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/themes-wedding.jpg" data-heading='Wedding' data-body='Relive your special day in a Photo Book full of elegant, wedding themed page layouts.'><a href="{{env('BASEPATH')}}themes?theme=wedding">Wedding</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/themes-memories.jpg" data-heading='Memories' data-body='Save all of your favourite memories in a beautiful Photo Book. Suitable for any occasions and story.'><a href="{{env('BASEPATH')}}themes?theme=memories">Memories</a></li>
                                <li class="learnmore" data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/themes-all.jpg" data-heading='All Themes' data-body='Explore the full range of themes for your Photo Book. Themes for all stories and occasions.'><a href="{{env('BASEPATH')}}themes">All Themes</a></li>
                            </ul>
                        </div>

                        <div class="col">
                            <h3>COVERS</h3>
                            <ul>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/covers-photocover.jpg" data-heading='Photocover' data-body='Design your own cover with your own text and images. Matte or Glossy finish.'><a href="{{env('BASEPATH')}}themes?cover=photocover">Photocover</a></li>
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/covers-softcover.jpg" data-heading='Softcover' data-body='Design your own cover with your own text and images. 190gsm flexible cover.'><a href="{{env('BASEPATH')}}themes?cover=softcover">Softcover</a></li>
                                {{-- <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/covers-debossed.jpg" data-heading='Debossed Cover' data-body='Buckram or Faux Leather material, with your image debossed into the front cover. Available in 8x8", 8x11", 11x8.5" and 12x12" sizes.'><a href="{{env('BASEPATH')}}themes?cover=deboss">Debossed Cover</a></li> --}}
                                <li data-image="{{env('BASEPATH')}}img/nav/2019/photobooks/covers-material.jpg" data-heading='Premium Material Cover' data-body='Buckram or Faux Leather material, with optional hot text stamping for a custom message. Available in 16x12" and 12x12" only.'><a href="{{env('BASEPATH')}}themes?cover=material">Premium Material Cover</a></li>
                                <li class="learnmore"><a href="{{env('BASEPATH')}}photo-books#covers">Learn about Covers</a></li>
                            </ul>
                        </div>

                        <div class="col previewcol">
                            <p class="image"><img src="{{env('BASEPATH')}}img/nav/2019/photobooks/size-11x85.jpg" alt='11 x 8.5" Landscape' /></p>
                            <p class="heading"><strong>11 x 8.5" Landscape</strong></p>
                            <p class="body">Available in Photocover, Softcover or Debossed Cover.</p>
                        </div>
                    </div>
                </div>
            </span>
            <!--
            <span>
                <a href="javascript:void(0)">Photo Books</a>
                <div class="dropdown hoverdropdown">
                    <div class="hoverstuff group">
                        <div class="col powercol">
                            <div class="powerheader" style="background-image:url({{env('BASEPATH')}}img/pbmenu1.jpg">
                                <h3>PHOTO BOOKS</h3>
                                <p>from $29.95</p>
                            </div>
                            <div class="powerbody">
                                <p><em>Create your own Photo Book using our free, easy-to-use Download Editor or Online Editor.</em></p>
                                <p><a href="{{env('BASEPATH')}}photo-books">About Photo Books</a></p>
                                <p><a href="{{env('BASEPATH')}}calculator">Photo Book Pricing</a></p>
                                <div class="navcta nomobile">
                                    <a href="javascript:void(0)" onclick="javascript:splitchoice('themes')" class="cta">DOWNLOAD EDITOR</a>
                                </div>
                                <div class="navcta">
                                    <a href="{{env('BASEPATH')}}themes" class="cta">CREATE ONLINE</a>
                                </div>
                            </div>
                        </div>

                        <div class="col powercol">
                            <div class="powerheader" style="background-image:url({{env('BASEPATH')}}img/pbmenu2.jpg">
                                <h3>DESIGN MY BOOK</h3>
                                <p>from <s>$69.95</s> $35.00</p>
                            </div>
                            <div class="powerbody">
                                <p><em>Let our team of designers create your Photo Book from scratch! Professionally designed within 3 business days.</em></p>
                                <p><a href="{{env('BASEPATH')}}design-my-book">About Design My Book</a></p>
                                <p><a href="{{env('BASEPATH')}}photo-books">About Photo Books</a></p>
                                <div class="navcta">
                                    <a href="{{env('BASEPATH')}}design-my-book" class="cta">GET STARTED</a>
                                </div>
                            </div>
                        </div>

                        <div class="col powercol">
                            <div class="powerheader" style="background-image:url({{env('BASEPATH')}}img/pbmenu3.jpg">
                                <h3>KEEP BOOKS</h3>
                                <p>from $11.95</p>
                            </div>
                            <div class="powerbody">
                                <p><em>A low cost, fast way to make Photo Books with our auto-create Editor. Create online in an instant.</em></p>
                                <p><a href="{{env('BASEPATH')}}keep-books">About Keep Books</a></p>
                                <p><a href="{{env('BASEPATH')}}calculator">Photo Book Pricing</a></p>
                                <div class="navcta">
                                    <a href="{{env('BASEPATH')}}keep-book-themes" class="cta">GET STARTED</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
            -->

            <span>
                <a href="{{env('BASEPATH')}}canvas-photo-prints">CANVAS</a>
            </span>

            <span class="nomobile @if(in_array(str_replace('-create-now', '', Request::path()), array('calendars', 'wall-calendar', 'desk-calendar', 'hd-calendar'))) active @endif">
                <a href="javascript:void(0)">Calendars</a>
                <div class="dropdown">
                    <a href="javascript:void(0)">Close</a>
                    <nav class="group">
                        <a href="{{env('BASEPATH')}}wall-calendar">
                            <img src="{{asset('img/nav/cal/01-wall.jpg')}}" />
                            <span>Wall Calendars</span>
                        </a>
                        <a href="{{env('BASEPATH')}}desk-calendar">
                            <img src="{{asset('img/nav/cal/02-desk.jpg')}}" />
                            <span>Desk Calendars</span>
                        </a>
                        <a href="{{env('BASEPATH')}}hd-calendar">
                            <img src="{{asset('img/nav/cal/03-hd.jpg')}}" />
                            <span>Hi Colour Calendars</span>
                        </a>
                    </nav>
                </div>
            </span>

            <span class="mobileonlyblock">
                <a href="{{env('BASEPATH')}}wall-calendar">CALENDARS</a>
            </span>

            <span class="mobileonlyblock">
                <a href="{{env('BASEPATH')}}canvas-photo-prints">CANVAS</a>
            </span>

            <span class="mobileonlyblock @if(in_array(str_replace('-create-now', '', Request::path()), array('prestige-prints', 'premium-prints', 'framed-prints', 'instagram-prints', 'photo-magnets', 'wood-prints', 'desk-prints','tote-bags','photo-mugs','jigsaw-puzzles','photo-magnets','cushions','desktop-plaques','serving-trays','luggage-tags','phone-cases','greeting-cards','vouchers'))) active @endif">
                <a href="{{env('BASEPATH')}}gifts">Gifts</a>
            </span>

            <span class="nomobile @if(in_array(str_replace('-create-now', '', Request::path()), array('stationery', 'journals', 'notebooks', 'guest-books'))) active @endif">
                <a href="javascript:void(0)">Stationery</a>
                <div class="dropdown">
                    <a href="javascript:void(0)">Close</a>
                    <nav class="group">
                        <a href="{{env('BASEPATH')}}journals">
                            <img src="{{asset('img/nav/stat/journals.jpg')}}" />
                            <span>Journals</span>
                        </a>
                        <a href="{{env('BASEPATH')}}notebooks">
                            <img src="{{asset('img/nav/stat/notebooks.jpg')}}" />
                            <span>Notebooks</span>
                        </a>
                        <a href="{{env('BASEPATH')}}guest-books">
                            <img src="{{asset('img/nav/stat/signature.jpg')}}" />
                            <span>Guest Books</span>
                        </a>
                        <a href="{{env('BASEPATH')}}wall-calendar">
                            <img src="{{asset('img/nav/cal/01-wall.jpg')}}" />
                            <span>Wall Calendars</span>
                        </a>
                        <a href="{{env('BASEPATH')}}desk-calendar">
                            <img src="{{asset('img/nav/cal/02-desk.jpg')}}" />
                            <span>Desk Calendars</span>
                        </a>
                        <a href="{{env('BASEPATH')}}hd-calendar">
                            <img src="{{asset('img/nav/cal/03-hd.jpg')}}" />
                            <span>Hi Colour Calendars</span>
                        </a>
                    </nav>
                </div>
            </span>

            <span class="nomobile @if(in_array(str_replace('-create-now', '', Request::path()), array('prestige-prints', 'premium-prints', 'framed-prints', 'instagram-prints', 'photo-magnets', 'wood-prints', 'desk-prints','tote-bags','photo-mugs','jigsaw-puzzles','photo-magnets','cushions','desktop-plaques','serving-trays','luggage-tags','phone-cases','greeting-cards','vouchers'))) active @endif">
                <a href="javascript:void(0)">Gifts</a>

                <div class="dropdown">
                    <a href="javascript:void(0)">Close</a>
                    <nav class="group">
                        <a href="{{env('BASEPATH')}}premium-prints">
                            <img src="{{asset('img/nav/prints/01-premiumprints.jpg')}}" />
                            <span>PREMIUM PRINTS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}photo-mugs-create-now">
                            <img src="{{asset('img/nav/gift/02-mug.jpg')}}" />
                            <span>MUGS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}cushions-create-now">
                            <img src="{{asset('img/nav/gift/03-cushions.jpg')}}" />
                            <span>CUSHIONS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}vouchers">
                            <img src="{{asset('img/nav/gift/04-vouchers.jpg')}}" />
                            <span>GIFT VOUCHERS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}instagram-prints">
                            <img src="{{asset('img/nav/prints/03-instagramprints.jpg')}}" />
                            <span>INSTAGRAM PRINTS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}framed-prints">
                            <img src="{{asset('img/nav/prints/02-framedprints.jpg')}}" />
                            <span>FRAMED PRINTS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}photo-magnets">
                            <img src="{{asset('img/nav/prints/04-photomagnets.jpg')}}" />
                            <span>MAGNETS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}canvas-photo-prints-create-now">
                            <img src="{{asset('img/nav/gift/09-canvas.jpg')}}" />
                            <span>PHOTO CANVAS</span>
                        </a>
                        <a href="{{env('BASEPATH')}}gifts" class="mobileonlyblock">
                            <span>VIEW ALL GIFTS</span>
                        </a>
                    </nav>
                    <div class="navcta">
                        <a href="{{env('BASEPATH')}}gifts" class="cta">VIEW ALL GIFTS</a>
                    </div>
                </div>
            </span>

            <span>
                <a class="pricinglink" href="{{env('BASEPATH')}}calculator">Pricing</a>
            </span>

            <span>
                <a class="promotionslink" href="{{env('BASEPATH')}}promotions">Promotions</a>
            </span>

            @if(!in_array(str_replace('-create-now', '', Request::path()), array('themes','photo-book-picker','download-thankyou')))
                <span>
                    <a href="javascript:splitchoice('popular')" class="cta">Create Now</a>
                </span>
            @endif
        </nav>
    </section>
</header>

<? if((isset($_SERVER['HTTP_HOST']) and (strpos($_SERVER['HTTP_HOST'], 'photopico') !== FALSE))): ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#theatre').width($(document).width()).height($(document).height()).fadeIn();
            $('#theatre').click(function(){
                hide_hoverform();
            });

            $('body').append('<div class="lightbox" id="lightboxiframehover" style="border-radius:20px; width:670px; padding:50px 60px;"><h2 style="margin-bottom:0.8em;">Important information regarding photoPICO</h2><p>From 1 October 2020, Canon Australia is no longer providing the photoPICO service. To ensure minimal impact on consumers with live projects, <strong>Pictureworks Group</strong> - Canon\'s trusted manufacturing partner - will fulfil photoPICO projects through its service, <strong>albumworks</strong> until 30 June 2021. We trust that you will continue to receive the same reliable service and high-quality imaging photo projects that you are used to.<br/><br/>If you continue to use the photoPICO editing and printing services from 1 October onward, your use of these services will be governed by Pictureworks Group\'s T&Cs which are available to review <a href="https://www.albumworks.com.au/terms">here</a>.<br/><br/>For further information or questions, please contact Canon\'s Customer Service team or for photoPICO technical support: &#112;&#104;&#111;&#116;&#111;&#112;&#105;&#099;&#111;&#064;&#097;&#108;&#098;&#117;&#109;&#119;&#111;&#114;&#107;&#115;&#046;&#099;&#111;&#109;&#046;&#097;&#117;</p><p style="text-align:center;"><br><a class="cta" href="javascript:void(0)" onclick="hide_hoverform()">CONTINUE</a></div>');
            $('#lightboxiframehover').css('left', (($(document).width()/2) - ((670+60)/2))+'px').css('top', '40px').fadeIn();
        });
    </script>
<? endif; ?>

@yield('body')


@include('templates.footer')