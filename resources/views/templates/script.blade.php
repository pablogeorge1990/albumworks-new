<script type="text/javascript">
    var current_page = 'homepage';
    var BASEPATH = '{{env('BASEPATH')}}';
    @if(isset($_GET['test']))
        window.setTimeout(function(){
                alert("{{addslashes( Request::input('test'))}}");
            }, 1000);
    @elseif(isset($_GET['download']))
        $(document).ready(function(){
                splitchoice('popular');
            });
    @endif
</script>

<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" media="screen" href="{{env('BASEPATH')}}css/ie.css" />
<script type="text/javascript" src="{{env('BASEPATH')}}js/selectivizr-min.js"></script>
<script type="text/javascript" src="{{env('BASEPATH')}}js/css3-mediaqueries.js"></script>
<![endif]-->
<script defer type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js#username=albumprinter"></script>
<script type="text/javascript" src="{{env('BASEPATH')}}js/jquery.prettyPhoto.js"></script>
<script src="{{env('BASEPATH')}}js/tooltipster.bundle.min.js"></script>
<script src="{{env('BASEPATH')}}js/jquery.parallax-1.1.3.js"></script>
<script src="{{env('BASEPATH')}}js/common.js?{{time()}}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-1338422-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-1338422-2', {
        'linker': {
            'accept_incoming': true,
            'domains': ['orders.photo-products.com.au','tponline.photo-products.com.au'],
        },
        'optimize_id': 'GTM-NNNH2S4'
    });
</script>


<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/20502/40J.js" async="true"></script>
<script type="text/javascript">
    var _ga = new Object;
    _ga.campaign = '';
    _ga.term = '';
    _ga.search= '';
    _ga.referrer = '';
    _ga.group = '';
    _ga.keywords = '';

    var gc = '';
    var c_name = "__utmz";
    if (document.cookie.length>0){
        c_start=document.cookie.indexOf(c_name + "=");
        if (c_start!=-1){
            c_start=c_start + c_name.length+1;
            c_end=document.cookie.indexOf(";",c_start);
            if (c_end==-1) c_end=document.cookie.length;
            gc = unescape(document.cookie.substring(c_start,c_end));
        }
    }

    if(gc != ""){
        var z = gc.split('.');
        if(z.length >= 4){
            var y = z[4].split('|');
            for(i=0; i<y.length; i++){
                if(y[i].indexOf('utmccn=') >= 0) _ga.campaign = y[i].substring(y[i].indexOf('=')+1);
                if(y[i].indexOf('utmctr=') >= 0) _ga.term     = y[i].substring(y[i].indexOf('=')+1);
            }
        }
    }

    /*
    <?


        if(strpos( Request::input('REQUEST_URI'), '?') !== FALSE){
            $querystring = substr( Request::input('REQUEST_URI'), strpos( Request::input('REQUEST_URI'), '?')+1);
            $queryparts = explode('&', $querystring);
            foreach($queryparts as $part){
                list($key, $val) = explode('=', $part);
                $_GET[$key] = urldecode($val);
            }
             $_GET['q'] =  Request::input('REQUEST_URI');
        }

        if(Request::input('HTTP_REFERER')  !== null){
            $parts = parse_url( Request::input('HTTP_REFERER'));
             $_GET['HTTP_REFERER'] = $parts['host'];
        }

        if(Request::input('campaign') !== null)  $_GET['campaign'] = cleanit( Request::input('campaign'));
        if(Request::input('group') !== null)  $_GET['group'] = cleanit( Request::input('group'));
    ?>
*/

    @if(Request::input('q') !== null)
        _ga.search = "{{addslashes( Request::input('q'))}}";
    @endif

    @if(Request::input('_kk') !== null) _ga.keywords = "{{addslashes(trim( Request::input('_kk')))}}"; @endif
    @if(Request::input('_kw') !== null) _ga.keywords = "{{addslashes(trim( Request::input('_kw')))}}"; @endif

    @if(Request::input('group') !== null) _ga.group = "{{addslashes(cleanit( Request::input('group')))}}"; @endif
    @if(Request::input('campaign') !== null) _ga.campaign = "{{addslashes(( Request::input('campaign') == "notset") ? "" : cleanit( Request::input('campaign')))}}"; @endif
    @if(Request::input('network') !== null) _ga.network = "{{((( Request::input('network') == 'g') or ( Request::input('network') == 's')) ? 'Search' : 'Content')}}"; @endif

    @if(Request::input('HTTP_REFERER') !== null) _ga.referrer = "{{addslashes( Request::input('HTTP_REFERER'))}}"; @endif
    @if(Request::input('placement') !== null) _ga.referrer = "{{addslashes( Request::input('placement'))}}"; @endif
    @if(Request::input('_placement') !== null) _ga.referrer = "{{addslashes( Request::input('_placement'))}}"; @endif

    @if(Request::input('target') !== null) _ga.target = "{{addslashes( Request::input('target'))}}"; @endif
    @if(Request::input('position') !== null) _ga.position = "{{addslashes( Request::input('position'))}}"; @endif
    @if(Request::input('match') !== null) _ga.match = "{{addslashes( Request::input('match'))}}"; @endif

    @if(Request::input('gclid') !== null) createCookie('gclid','{{addslashes( Request::input('gclid'))}}',90);     @endif
    @if(Request::input('msclkid') !== null) createCookie('gclid','{{addslashes( Request::input('msclkid'))}}',90);     @endif

    jQuery('input[name=00N3600000BOyGd]').each(function(){
        var $form = $(this).closest('form');
        if($form.find('input[name=00N3600000SikQU]').length <= 0 && readCookie('gclid')){
            $form.prepend('<input type="hidden" value="'+readCookie('gclid')+'" class="gclid" name="00N3600000SikQU"/>');
        }
    });

</script>

            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1060947230;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1060947230/?guid=ON&amp;script=0"/>
            </div>
            </noscript>            


<!-- ClickDesk Live Chat Service for websites <script type='text/javascript'>var _glc =_glc || []; _glc.push('all_ag9zfmNsaWNrZGVza2NoYXRyDwsSBXVzZXJzGIPxzPcBDA');var glcpath = (('https:' == document.location.protocol) ?'https://my.clickdesk.com/clickdesk-ui/browser/' : 'http://my.clickdesk.com/clickdesk-ui/browser/');var glcp = (('https:' == document.location.protocol) ? 'https://' : 'http://');var glcspt = document.createElement('script'); glcspt.type = 'text/javascript'; glcspt.async = true; glcspt.src = glcpath + 'livechat-new.js';var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(glcspt, s);</script> End of ClickDesk -->




@if(strpos(Request::path(), 'photo-books') !== FALSE)

@elseif(strpos(Request::path(), 'calculator') !== FALSE)

@else

@endif

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 10035965;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<noscript>
    <a href="https://www.livechatinc.com/chat-with/10035965/">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
</noscript>
<script async src="//cdn.livechatinc.com/qb/qb-10035965-light-160.js"></script>
<!-- End of LiveChat code -->

<!--  MouseStats:Begin  -->
<script type="text/javascript">var MouseStats_Commands=MouseStats_Commands?MouseStats_Commands:[]; (function(){function b(){if(void 0==document.getElementById("__mstrkscpt")){var a=document.createElement("script");a.type="text/javascript";a.id="__mstrkscpt";a.src=("https:"==document.location.protocol?"https://ssl":"http://www2")+".mousestats.com/js/5/2/5298657782534596732.js?"+Math.floor((new Date).getTime()/6E5);a.async=!0;a.defer=!0;(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(a)}}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load", b,!1);"complete"===document.readyState&&b()})(); </script>
<!--  MouseStats:End  -->
