<footer>
    <section id="footercontainer">
        <section id="footercols" class="group">
            <div class="col">
                <h5>PHOTO BOOKS</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}photo-books">Our Photo Books</a></li>
                    <li><a href="{{env('BASEPATH')}}photo-books-howto">How to Make</a></li>
                    <li><a href="{{env('BASEPATH')}}calculator">Price &amp; Range</a></li>
                </ul>

                <h5>PRESTIGE PRINTS</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}premium-prints">Premium Prints</a></li>
                    <li><a href="{{env('BASEPATH')}}framed-prints">Framed Prints</a></li>
                    <li><a href="{{env('BASEPATH')}}instagram-prints">Instagram Prints</a></li>
                    <li><a href="{{env('BASEPATH')}}desk-prints">Desk Prints</a></li>
                    <li><a href="{{env('BASEPATH')}}poster-printing">Premium Photo Posters</a></li>
                </ul>
            </div>
            <div class="col">
                <h5>STATIONERY</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}journals">Premium Journals</a></li>
                    <li><a href="{{env('BASEPATH')}}notebooks">Notebooks</a></li>
                    <li><a href="{{env('BASEPATH')}}guest-books">Guest Books</a></li>
                </ul>

                <h5>TRAVEL</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}journals">Travel Journals</a></li>
                    <li><a href="{{env('BASEPATH')}}notebooks">Travel Notebooks</a></li>
                </ul>

            </div>
            <div class="col">
                <h5>CALENDARS</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}wall-calendar">Wall Calendars</a></li>
                    <li><a href="{{env('BASEPATH')}}desk-calendar">Desk Calendars</a></li>
                    <li><a href="{{env('BASEPATH')}}hd-calendar">Hi Colour Calendars</a></li>
                </ul>

                <h5>PERSONALISED WALL ART</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}canvas-photo-prints">Canvas Prints</a></li>
                    <li><a href="{{env('BASEPATH')}}poster-printing">Premium Photo Posters</a></li>
                </ul>

                <h5>GIFT VOUCHERS</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}vouchers">Gift Vouchers</a></li>
                </ul>                
            </div>
            <div class="col">
                <h5>YOUR ORDER</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}faq">FAQs</a></li>
                    <li><a href="{{env('BASEPATH')}}track">Tracking</a></li>
                    <li><a href="{{env('BASEPATH')}}contact">Contact Us</a></li>
                    <li><a href="{{env('BASEPATH')}}shipping">Shipping</a></li>
                </ul>

                <h5>ABOUT ALBUMWORKS</h5>
                <ul>
                    <li><a href="{{env('BASEPATH')}}about">About Us</a></li>
                    <li><a href="{{env('BASEPATH')}}photo-books-howto">How to Make</a></li>
                    <li><a href="{{env('BASEPATH')}}blog">Blog</a></li>
                    <li><a href="{{env('BASEPATH')}}promotions">Promotions</a></li>
                    <li><a href="{{env('BASEPATH')}}privacy">Privacy</a></li>
                    <li><a href="{{env('BASEPATH')}}terms">Terms of Use</a></li>
                </ul>

                <h5>WEBINARS</h5>
                <ul>
                    <li><a href="https://www.youtube.com/user/albumworks/videos">View past webinars</a></li>
                </ul>
            </div>
            <div class="widecol col group">
                <div class="trustpilot-widget" data-locale="en-US" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="55f980f20000ff000583623f" data-style-height="150px" data-style-width="140" data-theme="light">
                    <a href="https://www.trustpilot.com/review/albumworks.com.au" target="_blank">Trustpilot</a>
                </div>
                <div id="lcqualitybadge"></div>
                <p><a href="http://www.freepik.com" rel="nofollow" target="_blank">Hi Colour Logo designed by macrovector / Freepik</a></p>
                <div id="social">
                    <h5>FOLLOW AND LIKE US</h5>
                    <a href="https://www.facebook.com/albumworks" target="_blank"><img src="{{asset('img/fb.png')}}" /></a>
                    &nbsp;
                    <a href="https://twitter.com/albumworks" target="_blank"><img src="{{asset('img/twit.png')}}" /></a>
                    &nbsp;
                    <a href="https://www.instagram.com/albumworks" target="_blank"><img src="{{asset('img/insta.png')}}" /></a>
                    &nbsp;
                    <a href="https://www.youtube.com/user/albumworks" target="_blank"><img src="{{asset('img/youtube.png')}}" /></a>
                </div>
            </div>
        </section>
        <p class="nomobile">&copy; <?=date('Y')?> Pictureworks Group Pty Ltd, trading as <em>albumworks</em>&#153; ABN 28 119 011 933.</p>
        <p class="mobileonlyblock">&copy; <?=date('Y')?> Pictureworks Group Pty Ltd<br>trading as <em>albumworks</em>&#153; ABN 28 119 011 933.</p>
    </section>
</footer>

<div id="theatre"> </div>
<div id="lightbox_download" class="lightbox">
    <form id="lightbox_dlform_form" class="webtolead" action="#" method="post" onsubmit="return checkdlform(this)">
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=FORTYOFF" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
        <input type="hidden" value="Download" name="00N3600000BOyGd">
        <input type="hidden" value="AP" name="00N3600000BOyAt">
        <input type="hidden" value="PG" name="00N3600000Loh5K">
        <input type="hidden" name="00N3600000Los6F" value="Windows">
        <!-- Referring Promotion --><input type="hidden" value="FORTYOFF" name="00N3600000LosAC">
        <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
        <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
        <!-- Adword fields -->
        <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
        <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
        <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
        <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
        <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
        <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
        <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
        <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
        <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
        <!-- Actual fields -->

        <h3>Download our free Editor</h3>
        <p class="dlbl">YOU'RE JUST SECONDS AWAY FROM CREATING A BEAUTIFUL KEEPSAKE USING YOUR PHOTOS.</p>
        <p class="dlbl">ENTER YOUR DETAILS BELOW TO DOWNLOAD OUR FREE EDITOR AND BEGIN WORKING ON YOUR PROJECT.</p>

        <div class="group">
            <img class="editorimg" src="{{asset('img/editor.jpg')}}" alt="computer with editor" />
            <div class="dlformbit">
                <label for="field_lightbox_download_first_name">Name:</label>
                <input type="text" id="field_lightbox_download_first_name" name="first_name" class="firstname smartedit inputbox">
                <label for="field_lightbox_download_email">Email:</label>
                <input type="text" name="email" id="field_lightbox_download_email" class="email smartedit inputbox">
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
                    <label for="field_lightbox_download_emailoptout">Keep me updated with special offers and software updates</label>
                </div>
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">DOWNLOAD OUR FREE EDITOR</a>
                <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and Conditions of this service</p>
            </div>
        </div>
    </form>
</div>
<div id="lightbox_mobile" class="lightbox">
    <form id="lightbox_mobileform_form" class="webtolead" action="#" method="post" onsubmit="return checkmobileform(this)">
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" class="mobreturl" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=MOBILEFORM" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
        <input type="hidden" value="Mobile Form" name="00N3600000BOyGd">
        <input type="hidden" value="AP" name="00N3600000BOyAt">
        <input type="hidden" value="PG" name="00N3600000Loh5K">
        <input type="hidden" name="00N3600000Los6F" value="Windows">
        <!-- Referring Promotion --><input type="hidden" value="MOBILEFORM" name="00N3600000LosAC">
        <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
        <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
        <!-- Adword fields -->
        <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
        <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
        <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
        <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
        <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
        <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
        <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
        <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
        <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
        <!-- Actual fields -->

        <h3>One More Step...</h3>
        <p class="dlbl">This product isn't quite ready for Mobile and Tablet users at the moment. The good news is, this product is ready to create on all Desktop and Laptop computers.</p>
        <p class="dlbl">Enter your email below and we'll send you a link to get started next time you're at a computer!</p>
        <!-- <p class="dlbl"><img class="editorimg" src="{{env('BASEPATH')}}img/mobiledesk.jpg" /></p> -->
        <p><img style="width:100%" src="https://www.albumworks.com.au/img/mobiledesk.jpg" /></p>

        <div class="group">
            <div class="dlformbit">
                <label for="field_lightbox_mobile_first_name">Name:</label>
                <input type="text" id="field_lightbox_mobile_first_name" name="first_name" class="firstname smartedit inputbox">
                <label for="field_lightbox_mobile_email">Email:</label>
                <input type="text" name="email" id="field_lightbox_mobile_email" class="email smartedit inputbox">
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" id="field_lightbox_mobile_emailoptout" name="emailOptOut" class="check">
                    <label for="field_lightbox_mobile_emailoptout">Keep me updated with special offers and software updates</label>
                </div>
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">SUBMIT</a>
            </div>
        </div>
    </form>
</div>
<div id="lightbox_gifts" class="lightbox">
    <div id="lightbox_gifts_form">
        <a class="close_handle" href="javascript:void(0)" onclick="hide_hoverform()"><img src="{{env('BASEPATH')}}img/close.png" /></a>
        <h3>Have you tried our other products?</h3>
        <p>OUR PRODUCT RANGE IS BIGGER AND BETTER THAN EVER</p>
        <div class="group">
            <img src="{{env('BASEPATH')}}img/giftspopup/1.jpg" />
            <img src="{{env('BASEPATH')}}img/giftspopup/2.jpg" />
            <img src="{{env('BASEPATH')}}img/giftspopup/3.jpg" />
            <img src="{{env('BASEPATH')}}img/giftspopup/4.jpg" />
        </div>
        <p class="caption">PHOTO MUGS  &bull;  TOTE BAGS  &bull;  JIGSAW PUZZLES  &bull;  FRAMED PRINTS  &bull;  PHOTO MAGNETS  T-SHIRTS  &bull;  CUSHIONS  &bull;  DESKTOP PLAQUES  &bull;  SERVING TRAYS  &bull;  PERSONALISED_TAGS  &bull;  PHONE CASES  &bull;  GREETING CARDS  &bull;  AND MORE...</p>
        <p class="tellme">ALBUMWORKS HAS A RANGE OF GIFT IDEAS, PERFECT FOR ALL MOMENTS IN LIFE - NO MATTER HOW BIG OR SMALL. EXPLORE THE NEW GIFT RANGE TODAY.</p>
        <p><a href="{{env('BASEPATH')}}gifts" class="cta">VIEW ALL GIFTS</a></p>
    </div>
</div>
<div id="lightbox_mailinglist" class="lightbox">
    <div id="lightbox_mailinglist_form">
        <a class="close_handle" href="javascript:void(0)" onclick="hide_hoverform()"><img src="{{env('BASEPATH')}}img/close.png" /></a>
        <h3>Have you joined our mailing list?</h3>
        <p>SPECIAL OFFERS, SOFTWARE UPDATES + MORE!</p>
        <div class="group">
            <img style="width:auto; float:none;" data-src="{{env('BASEPATH')}}img/bookpopup.jpg" class="lazyload" />
        </div>
        <p class="tellme">DON’T WANT TO MISS OUT? YOU DON’T HAVE TO! SIGN UP TO RECEIVE OUR NEWSLETTER,<br>EXCLUSIVE OFFERS AND PROMOTIONS. GUARANTEED GREAT DEALS ON PHOTO BOOKS!</p>
        <form class="mailinglist webtolead" action="#" method="post" onsubmit="return checkmlform(this)">
            <input type="hidden" value="00D36000000oZE6" name="sfga">
            <input type="hidden" value="00D36000000oZE6" name="oid">
            <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&ret=thankyou-newsletter" name="retURL">
            <input type="hidden" value="Website" name="lead_source">
            <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
            <input type="hidden" value="Newsletter Signup" name="00N3600000BOyGd">
            <input type="hidden" value="AP" name="00N3600000BOyAt">
            <input type="hidden" value="PG" name="00N3600000Loh5K">
            <input type="hidden" name="00N3600000Los6F" value="Windows">
            <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
            <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
            <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
            <input type="checkbox" checked="checked" value="1" name="emailOptOut" class="check" style="display:none">

            <p style="display:none">
                <label for="field1_first_name">Name:</label>
                <input type="text" class="inputbox" value="Name" id="field1_first_name" name="first_name" value="Name" />
            </p>

            <p><input type="text" class="inputbox" value="" id="field1_email" name="email" placeholder="EMAIL" /></p>

            <p><a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">SUBMIT</a></p>
        </form>
    </div>
</div>

<div id="splittheatre"> </div>
<div id="splitchoice" class="lightbox">
    <form id="splitchoice_form" class="webtolead" action="https://www.albumworks.com.au" method="post" onsubmit="return checkscform(this)">
        {!! csrf_field() !!}
        <input type="hidden" value="00D36000000oZE6" name="sfga">
        <input type="hidden" value="00D36000000oZE6" name="oid">
        <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=FORTYFIRST" name="retURL">
        <input type="hidden" value="Website" name="lead_source">
        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
        <input type="hidden" value="Download" name="00N3600000BOyGd">
        <input type="hidden" value="AP" name="00N3600000BOyAt">
        <input type="hidden" value="PG" name="00N3600000Loh5K">
        <input type="hidden" name="00N3600000Los6F" value="Windows">
        <!-- Referring Promotion --><input type="hidden" value="FORTYFIRST" name="00N3600000LosAC">
        <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
        <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
        <!-- Adword fields -->
        <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
        <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
        <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
        <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
        <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
        <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
        <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
        <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
        <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
        <!-- Actual fields -->

        <h3>How would you like to get started?</h3>
        <p class="dlbl">CHOOSE TO MAKE YOUR PRODUCT WITH OUR HIGH PERFORMANCE</p>
        <p class="dlbl">DOWNLOAD EDITOR OR ONLINE IN YOUR WEB BROWSER</p>

        <div class="group">
            <div class="dlopt">
                <p><img src="https://www.albumworks.com.au/img/splitchoice/download.jpg" /></p>
                <p class="dlbl">&bull; PERFECT FOR PROJECTS BIG AND SMALL</p>
                <p class="dlbl">&bull; WIDEST RANGE OF DESIGN TOOLS AND ABILITIES</p>
                <p class="dlbl">&bull; SUPER FAST PERFORMANCE</p>
                <div style="background: #1162bb; text-align:center; padding: 5px; border-radius:20px; margin: 2em 0;">
                    <p style="color: #ffffff; font-size: 16px;line-height: 1.6; margin: 0; font-weight: 800;">
                        GET NEW SPECIALS EVERY MONTH & AN EXCLUSIVE <br>
                        40% DISCOUNT OFF YOUR FIRST ORDER:
                    </p>
                </div>
                <p class="inputboxp group">
                    <input type="text" id="field_splitchoice_download_first_name" name="first_name" class="firstname inputbox" placeholder="Name(Optional)" style="float:left">
                    <input type="text" id="field_splitchoice_download_email" name="email" class="email inputbox" placeholder="Email(Optional)" style="float:right">
                </p>
                <div class="checkarea group">
                    <input type="checkbox" checked="checked" value="1" id="field_lightbox_download_emailoptout" name="emailOptOut" class="check">
                    <label for="field_lightbox_download_emailoptout">Keep me updated with special offers and software updates</label>
                </div>

                <p><div class="g-recaptcha" style="padding-left:calc(50% - 152px)" data-sitekey="6LcHNrsZAAAAAJF0hLoCNFils86kV9ik3TG5mXVr" data-callback="grcsetform"></div></p>
                <script type="text/javascript">
                    var __robot = true;
                    var grcsetform = function() {
                        __robot = false;
                        $('#splitchoice_form, #horidlform, #pbsplitchoice_form').attr('action', 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8');
                        $('#dlcta, #horidlcta, #pbsplitcta').click(function(){
                            $(this).closest('form').submit();
                        });
                    }
                </script>
                <a id="dlcta" style="margin-top:10px; padding:10px 30px; font-size: 17px;" class="cta" href="javascript:void(0)" onclick="if(__robot) alert('Please tick `I\'m not a robot`');">DOWNLOAD OUR FREE EDITOR</a>

                <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and Conditions of this service</p>
            </div>
            <div class="dlopt">
                <img src="https://www.albumworks.com.au/img/splitchoice/online.jpg" style="float:left; margin-top:20px;" />
                <div style="float:left;margin:20px 0 0 20px">
                    <p style="margin:0 0 0.5em" class="dlbl">&bull; GREAT FOR SMALL/MEDIUM PROJECTS</p>
                    <p style="margin:0 0 0.5em" class="dlbl">&bull; CREATE ON ALL TYPES OF DEVICES</p>
                    <p style="margin:0 0 0.5em" class="dlbl">&bull; USE PHOTOS FROM YOUR DEVICE</p>
                    <a style="margin-top:1em; font-size:14px" class="cta" id="splitaltcta" href="javascript:void(0)">CREATE PRODUCTS ONLINE</a>
                    <p class="smallprint">There is no charge until you are ready to order your product.<br>In downloading the software you agree to the Terms and<br>Conditions of this service</p>
                </div>
            </div>
        </div>
    </form>
</div>

<div style="position:fixed">
    @include('templates.script')
</div>

@yield('scripts')

</body>
</html>