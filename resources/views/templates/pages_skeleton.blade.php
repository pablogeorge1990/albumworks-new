<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">

    <!-- JS -->

    <script type="text/javascript" src="{!! asset('js/jquery/jquery-3.2.1.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/common/common.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('http://code.jquery.com/ui/1.10.1/jquery-ui.js') !!}"></script>

    <link href="{!! asset('http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css') !!}"type="text/css" media="all" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.10.0/themes/smoothness/jquery-ui.css" type="text/css" media="all" rel="stylesheet">



    <!--  CSS -->
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">


</head>
<body>
<div class="container nomenucontent">
    <div id="main">
        <div id="submain">
            <div id="right">
                @yield('body')
            </div>
        </div>
    </div>
</div>
</body>
</html>
