<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">

    <!-- JS -->

    <script type="text/javascript" src="{!! asset('js/jquery/jquery-3.2.1.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('http://code.jquery.com/ui/1.10.1/jquery-ui.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/tinymce/jquery.tinymce.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/tinymce/tinymce.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jquery/jquery-ui-timepicker-addon.js') !!}"></script>



    <script src="" type=""></script>

    <script type="text/javascript">
        tinymce.init({
            selector:'.wysiwyg',
            paste_as_text: true,
            plugins: "moxiemanager image code link",
            image_advtab: true,
            width : "100%",
            height : 430,
            extended_valid_elements : "style,span,a[class|name|href|target|title|onclick|rel],script[type|src],iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]"
        });
    </script>

    <link href="{!! asset('http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css') !!}"type="text/css" media="all" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.10.0/themes/smoothness/jquery-ui.css" type="text/css" media="all" rel="stylesheet">
    <link href="{!! asset('css/jquery-ui-timepicker-addon.css') !!}" type="text/css" media="all" rel="stylesheet">
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.datepicker').datetimepicker({
                dateFormat: "yy-mm-dd",
                timeFormat: "HH:mm:ss"
            });
        });
    </script>

    <!--  CSS -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">


</head>
<body>

    @yield('body')
</body>
</html>