@extends('admin.layouts.main')
@section('body')
    <div class="content">
        <div id="fullwidth">

        <form method="post" id="adminform" action="/admin/save/{{$blogpage}}">
            {{ csrf_field() }}
            <div class="container">
                <div id="content-head">
                    <div id="content-head-contents">
                        <h1> </h1>
                        <h2>{{$params['title']}}</h2>
                        <div id="content-head-modules">
                            <div class="module">
                                <a href="{{ url('/admin/'.$blogpage) }}" style="position:fixed; top:10px; left:10px; text-decoration:none"  class="redbutton">CANCEL CHANGES</a>
                                <button style="position:fixed; top:10px; right:10px;" type="submit" class="cupid-green">SAVE CHANGES</button>
                            </div>
                        </div>
                        <div class="clr"> </div>
                    </div>
                </div>

                <div class="fullwidth">
                    {!! $params['content'] !!}
                    @foreach($params as $key => $value)
                        @if(!is_array($value))
                            <textarea name="{{$key}}" style="display:none">{{ base64_encode($value)}}</textarea>
                            @if($key == 'type' && $value == 'sandra')

                                @if(isset($_POST['thumbnail']))
                                    <textarea name="thumbnail" style="display:none">{!! base64_encode($_POST['thumbnail']) !!}</textarea>
                                @endif
                                @if(isset($_POST['feature_text']))
                                    <textarea name="feature_text" style="display:none">{!! base64_encode($_POST['feature_text']) !!}</textarea>
                                @endif
                            @endif
                        @else
                            <textarea name="tags" style="display:none">{!! json_encode($value) !!}</textarea>
                        @endif
                    @endforeach
                </div>
            </div>
        </form>
        </div>
    </div>
@endsection