
@extends('admin.layouts.main')

@section('body')
    <div class="container">
        <div id="main">
            <div id="right">
                <div id="submain">
                    {{--link the content htmls to tinymce--}}
                    {{--$blogpage--}}
                    <form method="post" id="adminform" action="/admin/preview/{{$blogpage}}">
                        {{ csrf_field() }}
                        <div id="content-head-modules">
                            <h2><button onclick="if(confirm('Are you sure? You\'ll lose any changes you might have made since the last save.')) { window.location = '/admin'; } return false;" class="redbutton">CLOSE</button></h2>
                            <button type="submit" class="cupid-green">PREVIEW CHANGES</button>
                        </div>
                        <div class="clr"></div>
                        @foreach($pages as $key => $value)
                            @php
                            $explanation = null;
                            echo '<p> <p><label for="field_'.$key.'">'.$key.'</label></p>';
                                switch($key){
                                case 'content':
                                case 'introtext':
                                echo '<textarea name="'.$key.'" id="field_'.$key.'" class="inputbox wysiwyg">'.$value.'</textarea>';
                                break;
                                case 'slug':
                                case 'created':
                                case 'last_modified':
                                case 'hits':
                                echo '<span class="pretendhidden">'.$value.'</span>';
                                echo '<input type="hidden" name="'.$key.'" id="field_'.$key.'" value="'.$value.'" />';
                                break;
                                case 'type':
                                if(!$explanation) $explanation = 'The type of page you want to make';
                                echo '<select name="'.$key.'" id="field_'.$key.'" class="inputbox">
                                    <option value="sandra">Blog Post</option>
                                    <option value="normal">Normal</option>
                                    <option value="wrapper">iFrame</option>
                                    <option value="other">Other?</option>
                                </select>';
                                echo '<br/><br/><div id="blog_thumbnail">
                                    <p><label for="field_tags">blog_thumbnail (258*113 px)</label></p>
                                    <textarea name="thumbnail" id="field_thumbnail" class="inputbox wysiwyg">'.$blog_thumbnail.'</textarea>
                                </div><br/><br/>
                                <div id="blog_feature_text">
                                    <p><label for="field_feature_text">blog_feature_text</label></p>
                                    <input name="feature_text" id="field_feature_text" class="inputbox" value="'.$blog_feature_text.'">
                                </div>';
                                break;
                                case 'status':
                                if(!$explanation) $explanation = 'This is how you turn an article off';
                                echo '<select name="'.$key.'" id="field_'.$key.'" class="inputbox">
                                    <option value="1">Published</option>
                                    <option value="0">Offline</option>
                                </select>';

                                break;
                                case 'publish_up':
                                if(!$explanation) $explanation = 'If you want an article to only become available at a certain time, add that date and time here. For blog posts, this is the date we order posts by, so it\'s fairly important for posts but less important for other article types unless you\'re doing something tricky';
                                case 'publish_down':
                                if(!$explanation) $explanation = 'If you want an article to only be available until a certain time.';
                                echo '<input type="text" name="'.$key.'" id="field_'.$key.'" class="inputbox datepicker" value="'.$value.'" />';
                                break;
                                case 'meta_title':
                                if(!$explanation) $explanation = 'Used only in the &lt;title&gt; tag. If you don\'t set this, then the article title will be used.';
                                default:
                                echo '<input type="text" name="'.$key.'" id="field_'.$key.'" class="inputbox" value="'.$value.'" />';
                                break;
                                }
                                echo '<br><em>'.$explanation.'</em></p>';
                            @endphp
                        @endforeach
                        @php
                        echo '<div id="blog_tags">
                            <p><label for="field_tags">blog_tags</label></p>';
                            foreach($blogtags as $key => $value){
                                echo '<input type="checkbox" name="tags[]"'.$value['checked'].' id="tag'.$value["tag"].'" value="'.$value["tag"].'">';
                                echo '<label for="tag'.$value['tag'].'">&nbsp;'.$value['tag'].'&nbsp;</label>';
                            }
                            echo '</div><br/><br/><br/>';


                        @endphp
                    </form>
                </div>
            </div>
        </div>
@endsection
