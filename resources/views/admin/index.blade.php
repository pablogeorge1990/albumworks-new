@extends('admin.layouts.main')
@section('body')
    <div class="container">
        <div id="main">
            <div id="right">
                <div id="submain">
                    <button style="float:right" onclick="window.open('<?=env('BASEPATH').'admin/push'?>'); return false;" class="cta">PUSH TO STAGING</button>
                    <h1>ADMIN</h1>


                        <h4 class="lefty" style="clear:both">Add New Page</h4>
                        <form action="{{route('create_blog')}}" method="get">
                            <p>To create a new page or blog post, enter the url: <strong>http://www.albumworks.com.au/<input type="text" name="page" value="page" class="addpage" /></strong></p>
                        </form>
                        <script type="text/javascript">
                            jQuery(document).ready(function() {
                                jQuery(".addpage").keydown(function(event) {
                                    // Allow: backspace,    delete,                 tab,                escape,                 and enter
                                    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                                                // Allow: Ctrl+A
                                            (event.keyCode == 65 && event.ctrlKey === true) ||
                                                // Allow: home, end, left, right
                                            (event.keyCode >= 35 && event.keyCode <= 39)) {
                                        // let it happen, don't do anything
                                        return;
                                    }
                                    else {
                                        // Ensure that it is a number and stop the keypress
                                        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 65 || event.keyCode > 90) && (event.keyCode < 96 || event.keyCode > 122 ) && (event.keyCode != 173) && (event.keyCode != 109) && (event.keyCode != 189) && (event.keyCode != 190) && (event.keyCode != 191)) {
                                            event.preventDefault();
                                        }
                                    }
                                });
                            });
                        </script>


                        <h4 class="lefty">Edit Existing Page</h4>
                        <p>To edit an existing page, click it below:</p>
                        <ul>
                            @foreach ($pages as $page)
                                <li><a href="{{url('/admin/'.$page)}}">{{$page}}</a></li>
                            @endforeach
                        </ul>
                </div>
            </div>
        </div>

@endsection
