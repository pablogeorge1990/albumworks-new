<p>START GIT COMMIT FEEDBACK</p>
{{dump($commitChanges)}}
<p>END GIT COMMIT FEEDBACK</p>


<p>Please recommit the changes manually if any errors are thrown in GIT COMMIT FEEDBACK.</p>

<p>Please update the database on the live server by transferring /public/db/albumworks.db to /public/db/ of the live server. Make sure that the permission of albumworks.db is 777 ( to update the blog hits on the live server)</p>