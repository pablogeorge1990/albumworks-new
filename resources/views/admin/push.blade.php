<div id="content-head">
    <div id="content-head-contents">
        <h1> </h1>
        <h2>Welcome to Push to Staging!</h2>
        <div class="clr"> </div>
    </div>
</div>
<div class="fullwidth">

<form method="post" onsubmit="if((jQuery('#commit').val() == '') || (jQuery('#commit').val() == jQuery('#commit').attr('alt'))) { alert('You must enter comments to continue'); return false; } else { jQuery('#dothis').replaceWith('<em>Your patience is appreciated...</em>'); }">
    {{ csrf_field() }}
    <h3 style="margin:0;">Welcome!</h3>
    <p>This is the albumworks.com.au automated push-to-staging. To begin the process and set up the staging site, please enter any comments you have around what this revision of the site is for.</p>
    <p>e.g. <em>Contains updates to getstarted product wizard - mainly ordering decisions</em></p>
    <input type="text" name="commit" id="commit" class="inputbox smartedit" style="font-size:16px; width:100%; padding:2px; border:1px solid gray;" value="Commit Comments" alt="Commit Comments" />
    <p style="text-align:right; padding-top:5px;"><button class="cupid-green" type="submit" id="dothis">Let's do this!</button></p>
</form>