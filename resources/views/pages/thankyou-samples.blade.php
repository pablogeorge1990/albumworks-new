@extends('templates.header')

@section('body')

    <main class="normal textonly">
    <h1>Thank you!</h1>
    <p>We have received your request and will be in touch shortly!</p>
</main>

@endsection