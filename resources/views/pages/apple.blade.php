@extends('templates.header')

@section('body')
    <main class="normal static">
        <h1>Print your Apple Photobook</h1>
        <div class="bluebox">
            <h3>Apple may have announced that their Photo Book service has ended but you can still get your Photo Book printed with <em>albumworks</em>. And you get even more choice than what you are used to!</h3>
            <p>All product names, logos, and brands are property of their respective owners. All company, product and service names used in this website are for identification purposes only.</p>
            <p>Apple is a trademark of Apple Inc, registered in the U.S. and other countries. <em>albumworks</em> has no relationship with and is not a partner of Apple Inc.</p>
        </div>
        <div class="group leftleft">
            <div class="group" style="background-image:url({{asset('img/apple/02.jpg')}})">
                <img class="rightside" src="{{asset('img/apple/01.jpg')}}" />
                <h3>More Choice</h3>
                <br><br>
                <p>Produce your Photo Book in Standard Definition or our market leading full photographic High Definition quality with a choice of 250gsm Luster or 250gsm Glossy paper stocks. You can even add a stunning quality Slipcase or Presentation Box to add that special touch.</p>
            </div>
            <div class="group" style="background-image:url({{asset('img/apple/04.jpg')}})">
                <img class="rightside" src="{{asset('img/apple/03.jpg')}}" />
                <h3>Pricing</h3>
                <br><br>
                <p>Prices start from just $19.95, and keep an eye out for our regular special offers. You can price your book at the next page before you choose to proceed.</p>
            </div>
        </div>
        <div class="bluebox">
            <h2>Ready to make your Photo Book?</h2>
            <p>Get started now with our easy-to-use upload tool.</p>
            <p><a href="contact?apple" class="cta">GET STARTED</a></p>
        </div>
    </main>
@endsection