@extends('templates.header')
@section('body')

    <main id="blogpost" class="normal static">
        <h1>{!! $title !!}</h1>
        <p class="date">{!! strtoupper(date('F jS, Y', strtotime($publish_up))) !!}</p>
        <section class="blogcontent">{!! $content !!}</section>
    </main>
@endsection