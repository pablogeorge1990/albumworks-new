@extends('templates.header')

@section('body')

    <main class="normal textonly">
    <h1>Thanks so much for your purchase.</h1>
    <p>Your voucher and a separate tax invoice will be emailed to your nominated email address within the next five minutes.</p>
    <p>If you have any complications at all, please <a href="contact">contact</a> our customer service team on 1300 553 448.</p>
</main>

@endsection