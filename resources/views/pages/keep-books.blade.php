@extends('templates.header')

@section('body')
    <main class="normal static">
    <h1>Keep Books</h1>
    <h1 style="font-size:25px;">For the little moments in life <img src="{{asset('img/keep-books/00-icon.png')}}" /></h1>
    <div class="group leftright">
        <div class="group">
            <div class="content">
                <h3>Little books for little moments</h3>
                <p>We often overlook the little moments. An afternoon at the beach or a weekend away camping. Keep Books are the perfect solution for remembering these precious moments. Fill your Keep Book with your memories using our easy-to-use online Editor in a quick and simple fashion!</p>
                <p><a href="{{env('BASEPATH')}}keep-book-themes" class="cta">CREATE NOW</a></p>
            </div>
            <img src="{{asset('img/keep-books/01.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Create in an instant</h3>
                <p>Our online Editor makes it quick and easy to create your Keep Book. Simply “autofill” your photos, review your book and order - it’s that easy. And with the ability to create on any device, you can create your Keep Book online anywhere!</p>
                <p><a href="{{env('BASEPATH')}}keep-book-themes" class="cta">CREATE NOW</a></p>
            </div>
            <img src="{{asset('img/keep-books/02.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>It’s all in the details</h3>
                <p>Keep Books are made with a 300gsm flexible Softcover - making these books durable but also affordable. Add your own images and text to your book cover.</p>
                <p>Our Photo Books start at 40 pages, but our Keep Books start with just 20 pages - perfect for reliving the little moments in life. Select from our pre-designed themes and off you go! </p>
                <p><a href="{{env('BASEPATH')}}keep-book-themes" class="cta">CREATE NOW</a></p>
            </div>
            <img src="{{asset('img/keep-books/03.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Affordable quality printing</h3>
                <p>Keep Books are printed in Standard Definition to keep the cost low for you but without compromising on quality. Printed on the latest generation of digital printing presses on 150gsm silk paper.</p>
                <p><a href="{{env('BASEPATH')}}keep-book-themes" class="cta">CREATE NOW</a></p>
            </div>
            <img src="{{asset('img/keep-books/04.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>We’re here to help</h3>
                <p>Our amazing and friendly Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So if you have any questions or need assistance with your project, don’t hesitate to get in touch with us! You can start a live chat session at the bottom of the page or call us on 1300 553 448.</p>
            </div>
            <img src="{{asset('img/keep-books/05.jpg')}}" height="380" />
        </div>
    </div>    
</main>

@endsection