@extends('templates.header')

@section('body')

    <main class="normal static">
        <section class="faqpage">
            @php
                foreach($c['supportview'] as $line){
                    if((strpos($line, 'viewSolution.php') !== FALSE) and (strpos($line, 'solname=') !== FALSE)){
                        preg_match('/solname=([0-9a-zA-Z\-]+)"/', $line, $matches);
                        if(sizeof($matches) == 2){
                            $pagename = $matches[1];
                            $line = str_replace('&solname='.$pagename, '', $line);
                            $line = str_replace('viewSolution.php?', 'faq?', $line);
                        }
                    }

                    $line = str_replace('searchResults.php', 'faq?view=searchKeyWord', $line);

                    $line = str_replace('vendor_ID=1031&', '', $line);
                    echo $line;
                }
            @endphp
        </section>
        <section class="static group semimodules">
            <div class="semimodule" style="background-image:url(img/faq/shipping.jpg)">
                <h2>Shipping</h2>
                <p>Our priority is to make all your products as fast as we can - but never at the expense of quality. In fact 95% off all items are manufactured and dispatched within 2-4 business days. However we do recommend allowing 7 business days for dispatch for those rare occasions when your order doesn't pass internal quality control.</p>
            </div>
            <div class="semimodule" style="background-image:url(img/contact.jpg)">
                <h2>Contact</h2>
                <p>Still have questions? Ask us! Our amazing Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible.</p>
                <p><a href="contact" class="cta">CONTACT</a></p>
            </div>
        </section>
    </main>

@endsection
