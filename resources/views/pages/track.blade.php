@extends('templates.header')

@section('body')

    <main class="normal static">
        <h1>Tracking</h1>
        <!--
        <div class="emgbanner">
            <p><strong>COVID-19 UPDATE:</strong></p>
            <p>
                OUR SHIPPING PARTNERS ARE DOING THEIR VERY BEST AT THIS TOUGH TIME.<br>
                WE HAVE RECEIVED SOME REPORTS OF MULTIPLE DAY DELAYS IN DELIVERY.<br>
                PLEASE BE AWARE OF THIS WHEN ORDERING.
            </p>
        </div>
        -->
        @if(isset($trackdata))
            <? $something_has_dispatched = false; ?>
            @foreach($trackdata as $orderid => $order)
                <div class="group trackinfo">
                    @if(isset($order->asurl) and $order->asurl)
                        <div class="albumshareinfo nomobile">
                            <h3><?=$order->project?></h3>
                            <p><a href="<?=$order->asurl?>" target="_blank"><img src="<?=$order->asthumb?>" /></a></p>
                            <p><a target="_blank" href="<?=$order->asurl?>" class="cta">VIEW ONLINE PREVIEW</a></p>
                            <p><a target="_blank" href="<?=$order->asdel?>" class="dottedlink">DELETE ONLINE PREVIEW</a></p>
                        </div>
                    @endif
                    <h2 class="blue">Order Details:</h2>
                    <div class="clr"></div>
                    <dl class="maindetails">
                        <dt style="margin-bottom:0; margin-top:0;">Order ID:</dt>
                        <dd><?=$orderid?></dd>
                        <dt><a class="dottedlink" target="_blank" href="https://www.albumworks.com.au/aw-support-centre/portal/invoice.php?albumid=<?=$_GET['orderid']?>&email=<?=$_GET['email']?>">Print INVOICE</a></dt><dd></dd>

                        <dt>Product:</dt>
                        <dd><?=$order->description?></dd>

                        <dt>Stage:</dt>
                        <dd><?
                            switch($order->stage){
                                case 1: echo 'Arrived'; break;
                                case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: echo 'Uploaded'; break;
                                case 10: case 11: case 12: case 13: case 14: case 15: echo 'Printed'; break;
                                case 16: case 17: case 18: case 19: echo 'Assembled'; break;
                                case 20: echo 'Dispatched'; break;
                            }
                        ?></dd>
                    </dl>
                    <dl class="subdetails">
                        <dt>Delivery Address:</dt>
                        <dd><?
                            if(isset($order->company) and $order->company) echo $order->company.'<br>';
                            if(isset($order->name) and $order->name) echo $order->name.'<br>';
                            if(isset($order->address1) and $order->address1) echo $order->address1.'<br>';
                            if(isset($order->address2) and $order->address2) echo $order->address2.'<br>';
                            if(isset($order->address3) and $order->address3) echo $order->address3.'<br>';
                            if(isset($order->address4) and $order->address4) echo $order->address4.'<br>';
                            if(isset($order->city) and $order->city) echo $order->city.', ';
                            if(isset($order->state) and $order->state) echo $order->state.' ';
                            if(isset($order->postcode) and $order->postcode) echo $order->postcode.'<br>';
                            if(isset($order->country) and $order->country) echo $order->country.'<br>';
                            ?>
                        </dd>

                        <dt>Price:</dt>
                        @if(isset($order->total))
                            <dd>$<?=number_format($order->total, 2)?></dd>
                        @else
                            <dd>$0.00</dd>
                        @endif

                        <dt>Quantity:</dt>
                        <dd><?=$order->quantity?></dd>

                        <dt>Pay Date:</dt>
                        @if(isset($order->paydate))
                            <dd><?
                                $date = new DateTime(date('Y-m-d H:i:s', strtotime($order->paydate)));
                                echo date('jS F, Y', $date->getTimestamp());
                            ?></dd>
                        @else
                            <dd></dd>
                        @endif

                        <dt>Delivery Method:</dt>
                        @if(isset($order->logisticsproviders))
                            <dd><?=implode('<br>', $order->logisticsproviders)?></dd>
                        @else
                            <dd></dd>
                        @endif

                        <dt>Consignment Numbers:</dt>
                        @if(isset($order->connotes))
                            <dd><?
                                foreach($order->connotes as $connote){
                                    if(in_array('DHL', $order->logisticsproviders))
                                        echo '<a class="connote" target="_blank" href="http://www.dhl.com/content/g0/en/express/tracking.html?AWB='.$connote.'&brand=DHL">'.$connote.'</a><br>';
                                    else if(in_array('DHLEC', $order->logisticsproviders))
                                        echo '<a class="connote" target="_blank" href="https://dhlecommerce.asia/track/Track?ref='.$connote.'">'.$connote.'</a><br>';
                                    else if(in_array('ARAMEX', $order->logisticsproviders))
                                        echo '<a class="connote" target="_blank" href="https://www.aramex.com/track/results?mode=0&ShipmentNumber='.$connote.'">'.$connote.'</a><br>';
                                    else echo $connote.'<br>';
                                }
                                implode('<br>', $order->connotes)
                            ?></dd>
                        @else
                            <dd></dd>
                        @endif
                    </dl>
                    @if(isset($order->asurl) and $order->asurl)
                        <div class="albumshareinfo mobileonlyblock">
                            <h3><?=$order->project?></h3>
                            <p><a href="<?=$order->asurl?>" target="_blank"><img src="<?=$order->asthumb?>" /></a></p>
                            <p><a target="_blank" href="<?=$order->asurl?>" class="cta">VIEW ONLINE PREVIEW</a></p>
                            <p><a target="_blank" href="<?=$order->asdel?>" class="dottedlink">DELETE ONLINE PREVIEW</a></p>
                        </div>
                    @endif
                    <p class="tracknew nomobile"><button onclick="window.location = 'track';" class="cta">TRACK NEW ORDER</button></p>
                    <hr>
                    <h2 class="blue">Tracking Details:</h2>
                    <ul class="tracking">
                        <li class="<?=(isset($order->ordered) ? '' : 'disabled')?>">
                            <? if(isset($order->ordered)): ?>
                                <h3><?=(!isset($order->uploaded) ? '&rarr;' : '&bull;')?> Order Arrived <span><?=$order->ordered?></span><h3>
                                <? if(!isset($order->uploaded)): ?>
                                    <p>Please ensure you have returned to your editor and uploaded your project files.</p>
                                <? endif; ?>
                            <? else: ?>
                                <h3>Order Arrived</h3>
                            <? endif; ?>
                        </li>
                        <li class="<?=(isset($order->uploaded) ? '' : 'disabled')?>">
                            <? if(isset($order->uploaded)): ?>
                                <h3>&bull; Order Uploaded <span><?=$order->uploaded?></span><h3>
                            <? else: ?>
                                <h3>Order Uploaded</h3>
                            <? endif; ?>
                        </li>
                        <li class="<?=(isset($order->uploaded) ? '' : 'disabled')?>">
                            <? if(isset($order->uploaded)): ?>
                                <h3><?=(!isset($order->printed) ? '&rarr;' : '&bull;')?> At Pre-Print <span><?=$order->uploaded?></span><h3>
                                <? if(!isset($order->printed)): ?>
                                    <p>Your files have been received and are now being prepared for Printing.</p>
                                <? endif; ?>
                            <? else: ?>
                                <h3>At Pre-Print</h3>
                            <? endif; ?>
                        </li>
                        <li class="<?=(isset($order->printed) ? '' : 'disabled')?>">
                            <? if(isset($order->printed)): ?>
                                <h3>&bull; Printing <span><?=$order->printed?></span><h3>
                            <? else: ?>
                                <h3>Printing</h3>
                            <? endif; ?>
                        </li>
                        <li class="<?=(isset($order->printed) ? '' : 'disabled')?>">
                            <? if(isset($order->printed)): ?>
                                <h3><?=(!isset($order->dispatched) ? '&rarr;' : '&bull;')?> Assembly <span><?=$order->printed?></span><h3>
                                <? if(!isset($order->dispatched)): ?>
                                    <p>Your order has now been printed and transitioned to the assembly stage. We would expect it to be shipped within 0-2 working days barring any unforeseen errors requiring reprinting.</p>
                                <? endif; ?>
                            <? else: ?>
                                <h3>Assembly</h3>
                            <? endif; ?>
                        </li>
                        <li class="<?=(isset($order->dispatched) ? '' : 'disabled')?>">
                            <? if(isset($order->dispatched)): ?>
                                <? $something_has_dispatched = true; ?>
                                <h3>&rarr; Dispatched <span><?=$order->dispatched?></span><h3>
                                <p>Your Album has been dispatched via <?=(isset($order->logisticsproviders[0]) ? $order->logisticsproviders[0] : 'Express Courier')?> and should arrive within the next 1-5 working days (depending on the performance of the delivery network). We hope you enjoy your order!</p>
                            <? else: ?>
                                <h3>Dispatched</h3>
                            <? endif; ?>
                        </li>
                    </ul>
                </div>
            @endforeach
            <h2 class="blue">Common questions:</h2>
            <div class="cq">
                @if(!$something_has_dispatched)
                    <h3>How long will my order take?</h3>
                    <p>In over 90% of cases we dispatch our products within 4 working days (or less) from the time of payment and 99% within 5 days but please keep in mind that our service level goal is to dispatch orders within 7 working days and we highly recommend allowing this full 7 days plus shipping time for your order. Unfortunately, there are many factors that can affect how long long an order takes to make, so we can never “100% guarantee” when an order will be dispatched.</p>
                    <h3>My order has been printed. What happens next?</h3>
                    <p>Once a product has been printed it needs to cure for a minimum of 24 hours to ensure the inks have hardened and settled into the paper correctly. We then bind your album, using state of the art PUR glue binding methods. This requires a 24 hour curing time to ensure that the bind is as strong as possible. 90% of orders are dispatched in 4 business days and 99% within 5 business days of payment, however in rare situations it may take slightly longer. This may be caused by your order not passing our Quality Control checkpoint.</p>
                    <h3>Can I request quicker production of my order?</h3>
                    <p>We already produce our products as quickly as we can, so unfortunately there isn't anything extra we can do to produce your order any quicker. We do everything we can to ensure the swift production of all our products and in 90% of cases we dispatch within 4 working days or less and 99% in 5 working days.</p>
                @else
                    <h3>When will my order be delivered?</h3>
                    <p>Your order is being delivered via express courier and should arrive at your nominated address within the next 1-5 working days. You can find the consignment number at the top of this page under the “Order Details” section.</p>
                    <h3>How do I track the delivery of my order?</h3>
                    <p>You can track the delivery of your order by clicking on the Consignment Number at the top of this page. You should also have received an email with details about your delivery. If you have not received an email, feel free to contact our <a href="https://www.albumworks.com.au/contact">Customer Service</a> team.</p>
                    <h3>How do I order more copies of my order?</h3>
                    <p>When you order we will send you a personal link to view your order online - you can share that link with your friends (or delete it if you like) and of course order an extra copy of your book.<br><br>In some cases, you can duplicate your existing order from inside the editor and just order again.</p>
                @endif
            </div>
        @else
            <div class="group leftright">
                <div class="group">
                    <div class="content">
                        <h3>Where is my order?</h3>
                        <p>Track the progress of any current order</p>
                            <form method="get" class="bigform trackform">
                            <p>
                                <label for="portal_auth_form_albumid">Order ID:</label>
                                @if(isset($_GET['orderid']))
                                    <input type="text" name="orderid" id="portal_auth_form_albumid" class="inputbox" value="<?=$_GET['orderid']?>" placeholder="Order ID" size="19" maxlength="20" />
                                @else
                                    <input type="text" name="orderid" id="portal_auth_form_albumid" class="inputbox" placeholder="Order ID" size="19" maxlength="20" />
                                @endif
                                <span class="note"><br/>Your Order ID was emailed to you when you ordered.</span>
                            </p>
                            <p>
                                <label for="portal_auth_form_albumid">Email:</label>
                                @if(isset($_GET['email']))
                                    <input type="text" name="email" id="portal_auth_form_email" class="inputbox" placeholder="Email Address" value="<?=$_GET['email']?>" size="40" maxlength="250" />
                                @else
                                    <input type="text" name="email" id="portal_auth_form_email" class="inputbox" placeholder="Email Address"  size="40" maxlength="250" />
                                @endif
                            </p>
                            @if(isset($error))
                                <p style="color:red; font-style:italic; clear:both"><?=$error?></p>
                            @endif
                            <p class="loginbuttontd" style="clear:both">
                                <button type="submit" id="portal_auth_form_submit" class="cta">TRACK ORDER</button>
                            </p>
                        </form>
                    </div>
                    <img src="https://www.albumworks.com.au/aw-support-centre/portal/include/img/01.jpg" height="380" />
                </div>
                <div class="group">
                    <div class="content">
                        <h3>No need to wait</h3>
                        <p>All of our products are delivered with full tracking available. Once your order has been dispatched, you should receive it within 2-3 business days* in metropolitan areas. Some rural and remote areas may take longer.</p>
                        <p>We recommend giving yourself a full <span style="font-weight: bold;">7 business days</span> for production of your order to ensure delivery before any important event or occasion.. But we will work our hardest to give you a pleasant surprise - your product arriving sooner!</p>
                        <p><a href="{{env('BASEPATH')}}shipping">Click here</a> for more shipping information</p>
                        <p style="font-style:italic">*We have received reports of multiple day delays in delivery due to COVID-19.</p>
                    </div>
                    <img src="{{asset('img/tracking/02.jpg')}}" height="380" />
                </div>
            </div>
        @endif
</main>

@endsection

