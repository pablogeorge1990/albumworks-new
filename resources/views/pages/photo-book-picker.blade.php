@extends('templates.header')

@section('title', 'Photo Books: Beautiful and personalised | albumworks')
@section('meta_description', 'Create beautiful personalised Photo Books from your computer, table or mobile phone. Easy to use, free software. 100% design freedom, fast delivery and local support.')

@section('body')

    <?php
    $theme = ['contemporary', 'Contemporary'];
    switch(@$_GET['theme']){
        case 'blank': $theme = [$_GET['theme'], 'Build Your Own Theme']; break;
        case 'modern-white': $theme = [$_GET['theme'], 'Modern White']; break;
        case 'modern-black': $theme = [$_GET['theme'], 'Modern Black']; break;
        case 'pastel': $theme = [$_GET['theme'], 'Pastel']; break;
        case 'scrapbook': $theme = [$_GET['theme'], 'Scrapbook']; break;
        case 'classic': $theme = [$_GET['theme'], 'Classic']; break;
        case 'light-bright': $theme = [$_GET['theme'], 'Light and Bright']; break;
        case 'black-borders': $theme = [$_GET['theme'], 'Black Borders']; break;
        case 'white-borders': $theme = [$_GET['theme'], 'White Borders']; break;
        case 'contemporary': $theme = [$_GET['theme'], 'Contemporary']; break;
        case 'full-bleed': $theme = [$_GET['theme'], 'Full Bleed']; break;
        case 'plates': $theme = [$_GET['theme'], 'Plates']; break;
        case 'scatter': $theme = [$_GET['theme'], 'Scatter']; break;
        case 'clustered': $theme = [$_GET['theme'], 'Clustered']; break;
        case 'in-love': $theme = [$_GET['theme'], 'In Love']; break;
        case 'honeymoon': $theme = [$_GET['theme'], 'Honeymoon']; break;
        case 'memories': $theme = [$_GET['theme'], 'Memories']; break;
        case 'a-lifes-story': $theme = [$_GET['theme'], 'A Life\'s Story']; break;
        case 'elegant-wedding': $theme = [$_GET['theme'], 'Elegant Wedding']; break;
        case 'stylish-wedding': $theme = [$_GET['theme'], 'Stylish Wedding']; break;
        case 'baby-boy': $theme = [$_GET['theme'], 'Baby Boy']; break;
        case 'baby-girl': $theme = [$_GET['theme'], 'Baby Girl']; break;
        case 'cherish': $theme = [$_GET['theme'], 'Cherish']; break;
        case 'family-history': $theme = [$_GET['theme'], 'Family History']; break;
        case 'a-year-to': $theme = [$_GET['theme'], 'A Year to Remember']; break;
        case 'sweet-memories': $theme = [$_GET['theme'], 'Sweet Memories']; break;
        case 'travel-scrapbook': $theme = [$_GET['theme'], 'Travel Scrapbook']; break;
        case 'travel-pano': $theme = [$_GET['theme'], 'Travel Panorama']; break;
        case 'travel-snapshots': $theme = [$_GET['theme'], 'Travel Snapshots']; break;
        case 'travel-collage': $theme = [$_GET['theme'], 'Travel Collage']; break;
        case 'my-pet': $theme = [$_GET['theme'], 'My Pet']; break;
    }
    ?>

    <main class="normal static" id="style2018">
        <div class="phodwizselection group anipicker" style="display: block;">
            <div class="info">
                <h2 class="widgethead">Select your Photo Book</h2>
                <div class="formstyle">
                    <h4 class="group">
                        <span>Size:</span>
                        <select name="size" id="select_size" class="inputbox" onchange="change_options($(this).attr('name'), this.value, false)">
                            <option value="16x12" cm="(40.5 x 30.5cm)">16x12" Landscape</option>
                            <option value="14x10" cm="(35 x 25cm)">14x10" Landscape</option>
                            <option value="12x12" cm="(30.5 x 30.5cm)">12x12" Square</option>
                            <option value="11x85" cm="(27.5 x 22cm)" selected="selected">11x8.5" Landscape</option>
                            <option value="8x11" cm="(20.5 x 27.5cm)">8x11" Portrait</option>
                            <option value="8x8" cm="(20.5 x 20.5cm)">8x8" Square</option>
                            <option value="8x6" cm="(20.5 x 15cm)">8x6" Landscape</option>
                        </select>
                    </h4>
                    <div class="previewbit">
                        <p><strong id="preview_size">(27.5 x 22cm)</strong></p>
                    </div>
                    <h4 class="group">
                        <span>Binding:</span>
                        <select name="binding" id="select_binding" class="inputbox" onchange="change_options($(this).attr('name'), this.value, false)">
                            <option value="classic" minpages="40" maxpages="200" paper="150gsm Standard Colour Premium Silk" paper_upgrades="190gsm Standard Colour Photo Pearl or 260gsm Hi Colour Photo Luster">Classic Photo Book</option>
                            <option value="layflat" minpages="40" maxpages="140" paper="400gsm Standard Colour E-Photo Luster or Matte" paper_upgrades="400gsm Hi Colour E-Photo Luster or Matte">Layflat Photo Book</option>
                        </select>
                    </h4>
                    <div class="previewbit">
                        <p>
                            <strong>Pages:</strong> <span id="selection_minpages">40 included</span>
                            &nbsp;&nbsp;
                            <strong>Max Pages:</strong> <span id="selection_maxpages">200</span>
                        </p>
                        <p><strong>Paper:</strong> <span id="paper">150gsm Standard Colour Premium Silk</span></p>
                        <p id="paper_upgrades">
                            <strong>Paper upgrades:</strong> <span id="preview_paper_upgrades">190gsm Standard Colour Photo Pearl or 260gsm Hi Colour Photo Luster</span>
                        </p>
                    </div>
                    <h4 class="group">
                        <span>Cover:</span>
                        <select name="cover" id="select_cover" class="inputbox" onchange="change_options($(this).attr('name'), this.value, false)">
                            <option value="photocover" cover_details="<p><strong>Cover finish: </strong><span>Matte or Gloss</span></p><p>Print your image and text directly on the front and back cover.</p>" extras="pbox,scase">Photocover</option>
                            <option value="softcover" cover_details="<p><strong>Cover finish: </strong><span>190gsm flexible card cover</span></p><p>Print your image and text directly on the front and back cover.</p>" extras="">Softcover</option>
                            {{-- <option value="deboss" cover_details='<p>Smaller image debossed into the front cover</p><p><strong>Cover finish: </strong><span>Choose from Faux Leather or Buckram</span></p><p><img src="img/photobookwizard/materials.png" style="width:auto" alt="materials" /></p>' extras="pbox,scase">Debossed Material Cover</option> --}}
                            <option value="material" cover_label="Premium Material Cover" cover_details="<p><strong>Cover finish: </strong><span>Choose from Faux Leather or Buckram</span></p><p style='color:red;'>CHOOSE THE COLOUR OF YOUR COVER MATERIAL ONCE YOU HAVE FINISHED YOUR DESIGN AND ARE READY TO ORDER</p><p><img src='img/photobookwizard/material-swatches-3.png' style='width:auto' alt='materials' /></p>" extras="pbox,scase">Premium Material Cover</option>
                        </select>
                    </h4>
                    <div class="previewbit">
                        <div id="selection_coverdetails">
                            <p><strong>Cover finish:</strong> Matte or Gloss</p>
                            <p>Print your image and text directly on the front and back cover.</p>
                        </div>
                    </div>
                </div>

            </div>
            <a name="proceed"></a>
            <div class="preview">
                <h2 class="widgethead">Preview</h2>
                <input type="hidden" id="select_theme" value="<?=$theme[0]?>" />
                <h5><span><?=$theme[1]?></span> &nbsp;&nbsp; <a href="{{env('BASEPATH')}}themes">Choose different theme</a></h5>
                <div class="bookstage">
                    <img src="{{env('BASEPATH')}}img/aw-spin.gif" id="awspin" />
                    <iframe onload="window.parent.parent.scrollTo(0,0)" id="albumshareframe" frameborder="0" scrolling="no" align="center" allowtransparency="true" src=""> </iframe>
                </div>
                <div class="summary">
                    <p>From <strong id="preview_price">$74.95</strong> <span id="preview_shipping">+ $12.50 shipping</span></p>
                </div>
                <div id="preview_promo">
                    <div class="promobox">
                        <? if((time() > mktime(0,0,0,1,31,2019)) && (time() < mktime(0,0,0,2,22,2019))): ?>
                        <h4>25% OFF ORDERS OVER $50!</h4>
                        <div>SAVEFEB</div>
                        <? elseif((time() > mktime(0,0,0,2,22,2019)) && (time() < mktime(0,0,0,3,14,2019))): ?>
                        <h4>GET UP TO 50 EXTRA PAGES FREE IN YOUR BOOK</h4>
                        <div><span style="font-weight:400">Use Voucher Code: </span>XTRAPAGES</div>
                        <? elseif((time() > mktime(0,0,0,1,7,2019)) && (time() < mktime(0,0,0,2,1,2019))): ?>
                        <h4>40 EXTRA PAGES FREE IN ANY PHOTO BOOK</h4>
                        <div>SUMMER40</div>
                        <? endif; ?>
                    </div>
                </div>
                <p>
                    <a href="javascript:void(0)" onclick="if($(this).hasClass('active')) {  pb_start_editor(__url); }" class="cta active">CREATE NOW</a><br>
                    <!--Or <a href="#join">download our desktop Editor</a> to create this product -->
                </p>

                <?php
                    $bnmls = [];

                    if(time() < mktime(0,0,0,6,20,2019)){
                        $bnmls['11x85'] = [
                            'label' => '11x8.5" Landscape Photocover Photo Book',
                            'link' => 'https://www.albumworks.com.au/directsale-may19a',
                            'options' => [
                                [
                                    'pages' => 40,
                                    'wasprice' => 69.95,
                                    'isprice' => 49.95,
                                    'percoff' => 28.5
                                ],
                                [
                                    'pages' => 70,
                                    'wasprice' => 105.95,
                                    'isprice' => 69.95,
                                    'percoff' => 34
                                ],
                                [
                                    'pages' => 100,
                                    'wasprice' => 141.95,
                                    'isprice' => 85.20,
                                    'percoff' => 40
                                ]
                            ]
                        ];
                        $bnmls['12x12'] = [
                            'label' => '12x12" Square Photocover Photo Book',
                            'link' => 'https://www.albumworks.com.au/directsale-may19b',
                            'options' => [
                                [
                                    'pages' => 40,
                                    'wasprice' => 86.95,
                                    'isprice' => 64.95,
                                    'percoff' => 25
                                ],
                                [
                                    'pages' => 70,
                                    'wasprice' => 131.95,
                                    'isprice' => 86.95,
                                    'percoff' => 34
                                ],
                                [
                                    'pages' => 100,
                                    'wasprice' => 175.95,
                                    'isprice' => 99.20,
                                    'percoff' => 43.5
                                ]
                            ]
                        ];
                        $bnmls['16x12'] = [
                            'label' => '16x12" Landscape Photocover Photo Book',
                            'link' => 'https://www.albumworks.com.au/directsale-may19c',
                            'options' => [
                                [
                                    'pages' => 40,
                                    'wasprice' => 119.95,
                                    'isprice' => 77.95,
                                    'percoff' => 35
                                ],
                                [
                                    'pages' => 70,
                                    'wasprice' => 172.45,
                                    'isprice' => 99.95,
                                    'percoff' => 42
                                ],
                                [
                                    'pages' => 100,
                                    'wasprice' => 224.95,
                                    'isprice' => 112.45,
                                    'percoff' => 50
                                ]
                            ]
                        ];
                    }

                    if(sizeof($bnmls)){
                        ?> <div> <?
                    }

                    foreach($bnmls as $code => $bnml){
                        ?>
                            <div class="bnmlpromo" id="bnmlpromo_<?=$code?>">
                                <h5><span>BUY NOW, MAKE LATER</span></h5>
                                <h6>Purchase a pre-paid deal now and you have 6 months to create your product.</h6>
                                <? foreach($bnml['options'] as $option): ?>
                                    <p>
                                        <input type="radio" name="bnmlpromo_<?=$code?>" value="<?=$option['pages']?>" id="bnmlpromo_<?=$code?>_<?=$option['pages']?>" />
                                        <label for="bnmlpromo_<?=$code?>_<?=$option['pages']?>">
                                            <span><?=$bnml['label']?>: </span>
                                            <strong><?=$option['pages']?> pages</strong>
                                            <span>&nbsp;|&nbsp;</span>
                                            <s>$<?=number_format($option['wasprice'], 2)?></s>
                                            <strong>NOW $<?=number_format($option['isprice'], 2)?></strong>
                                            <span class="percoff"><?=$option['percoff']?>% OFF</span>
                                        </label>
                                    </p>
                                <? endforeach; ?>
                                <p class="ctabit">
                                    <a href="javascript:void(0)" onclick="window.location = '<?=$bnml['link']?>?'+$('input[name=bnmlpromo_<?=$code?>]:checked').val()" class="cta active">BUY NOW</a><br>
                                </p>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('#bnmlpromo_<?=$code?> input[type=radio]:first').click();
                                });
                            </script>
                        <?
                    }

                    if(sizeof($bnmls)){
                        ?> </div> <?
                    }
                ?>
            </div>
        </div>

        <a name="join"></a>
        <div class="nomobile">
            <h1 class="lowerh1">Making a big Photo Book with lots of Photos?</h1>
            <h2>Download our desktop Editor to get the full range of features and abilities!</h2>
            <form class="box group" id="horidlform" action="https://www.albumworks.com.au" method="post" onsubmit="return checkdlform(this)">
                {!! csrf_field() !!}
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=FORTYFIRST" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="FORTYFIRST" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>

                <!-- Actual fields -->
                <input type="text" name="first_name" class="firstname inputbox" placeholder="NAME">
                <input type="text" name="email" class="email inputbox" placeholder="EMAIL">
                <p><div class="g-recaptcha" style="padding-left:calc(50% - 152px); clear:both" data-sitekey="6LcHNrsZAAAAAJF0hLoCNFils86kV9ik3TG5mXVr" data-callback="grcsetform"></div></p>
                <a id="horidlcta" class="cta" href="javascript:void(0)" onclick="if(__robot) alert('Please tick `I\'m not a robot`');">GET OUR FREE EDITOR</a>
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
                    <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
                </div>
            </form>
        </div>

        <div class="threecols group">
            <div>
                <img src="img/photobookwizard/01.jpg" alt="man and woman looking at computer smiling" />
                <h3>How much does a Photo Book cost?</h3>
                <p>Our Photo Books start from just $19.95. There is a book for every budget. Size, cover type and print definition all play a part in the cost of your Photo Book. Use our handy calculator to find the right book for you.</p>
                <a href="https://www.albumworks.com.au/calculator" class="cta">LEARN MORE</a>
            </div>
            <div>
                <img src="img/photobookwizard/02.jpg" alt="handing a photobook with deboss cover from generation to generation" />
                <h3>Dispatched within just 7 days</h3>
                <p>Our priority is to make all your products as fast as we can - but never at the expense of quality. In fact 95% off all items are manufactured and dispatched within 2-4 business days.</p>
                <a href="https://www.albumworks.com.au/shipping" class="cta">LEARN MORE</a>
            </div>
            <div>
                <img src="img/photobookwizard/03.jpg" alt="an array of photo books, some open and some closed, showing their covers and contents" />
                <h3>Need help?</h3>
                <p>Help is never far away! Our Melbourne based support team is there to support you. Call us on 1300 553 448 or email service@albumworks.com.au - we would love to help.</p>
                <a href="https://www.albumworks.com.au/contact" class="cta">LEARN MORE</a>
            </div>
        </div>

        <link rel="stylesheet" type="text/css" media="screen" href="css/flippybook.css" />
        <style type="text/css">
            .phodwizselection .info .formstyle h4{
                margin-bottom:25px;
            }

            .previewbit{
                margin-bottom:50px;
            }

            .previewbit p{
                margin-bottom:0.25em;
            }

            .preview h5{
                font-weight:800;
                text-align:left;
                font-size:19px;
                margin-bottom:20px;
            }

            .preview h5 a{
                font-size:15px;
            }

            #style2018 h2.widgethead{
                font-size:22px;
                border-bottom:1px solid #1163b9;
                text-align: left;
                font-style:normal;
                padding-bottom:10px;
                margin:0 0 20px;
                font-family: 'Source Sans Pro', sans-serif;
                font-weight:600;
                max-width:100%;
            }

            .phodwizselection .info {
                width: calc(40% - 30px);
            }
            .phodwizselection .preview{
                width: calc(60% - 30px);
            }

            .info strong{
                font-weight:800;
            }

            .phodwizselection .info .formstyle h4 select{
                width:calc(100% - 160px);
            }

            .bookstage{
                border:none;
                border-radius:0px;
                background:url({{env('BASEPATH')}}img/pickerbg.png) transparent;
                background-repeat: no-repeat;
                background-size:cover;
                background-position:top center;
                position:relative;
            }

            #awspin{
                position:absolute;
                top:40%;
                left:45%;
                width:50px;
                z-index:0;
            }

            #albumshareframe{
                margin-top:-15px;
                height: 420px;
                width: 100%;
                z-index: 1;
                position: absolute;
                left: 0;
                top: 0;
            }
            #albumshareframe.landscape{
                margin-top:20px;
            }
            #albumshareframe.square{
                margin-top:20px;
            }

            .summary{
                margin-top:50px;
            }

            .phodwizselection #preview_promo .promobox{
                max-width:400px;
                margin-bottom:25px;
            }

        </style>
        <script type="text/javascript" src="{{env('BASEPATH')}}js/modernizr.js"></script>
        <script type="text/javascript" src="{{env('BASEPATH')}}js/picker.js?v5"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#select_size').change(function(){
                    if($('#bnmlpromo_'+$('#select_size').val()).length){
                        var $bnml = $('#bnmlpromo_'+$('#select_size').val());
                        $bnml.siblings().each(function(){
                            if($(this).is(':visible')) {
                                $(this).slideUp();
                            }
                        });
                        if(!$bnml.is(':visible')){
                            $bnml.slideDown();
                        }
                    }
                    else{
                        $('.bnmlpromo').each(function(){
                            if($(this).is(':visible')) {
                                $(this).slideUp();
                            }
                        });
                    }
                });
                
                <? if(isset($_GET['size']) and $_GET['size']): ?>
                    $('#select_size').val('<?=$_GET['size']?>').change();
                <? endif; ?>

                <? if(isset($_GET['theme']) and $_GET['theme']): ?>
                    $('#select_theme').val('<?=$_GET['theme']?>').change();
                <? endif; ?>

                <? if(isset($_GET['cover']) and $_GET['cover']): ?>
                    <? if($_GET['cover'] == 'material'): ?>
                        $('#select_size').val('12x12').change();
                    <? endif; ?>
                    $('#select_cover').val('<?=$_GET['cover']?>').change();
                <? endif; ?>

                set_orientation();
                show_share(__orientation, __shares[$('#select_cover').val().replace("softcover","photocover")+'|'+__orientation+'|'+$('#select_theme').val()]);
            });
        </script>
    </main>
    <div id="pbsplitchoice" class="lightbox">
        <form id="pbsplitchoice_form" class="webtolead" action="https://www.albumworks.com.au" method="post" onsubmit="return checkscform(this)">
            {!! csrf_field() !!}
            <input type="hidden" value="00D36000000oZE6" name="sfga">
            <input type="hidden" value="00D36000000oZE6" name="oid">
            <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=FORTYFIRST" name="retURL">
            <input type="hidden" value="Website" name="lead_source">
            <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
            <input type="hidden" value="Download" name="00N3600000BOyGd">
            <input type="hidden" value="AP" name="00N3600000BOyAt">
            <input type="hidden" value="PG" name="00N3600000Loh5K">
            <input type="hidden" name="00N3600000Los6F" value="Windows">
            <!-- Referring Promotion --><input type="hidden" value="FORTYFIRST" name="00N3600000LosAC">
            <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
            <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
            <!-- Adword fields -->
            <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
            <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
            <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
            <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
            <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
            <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
            <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
            <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
            <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
            <!-- Actual fields -->

            <h3> You’re about to create a Photo Book<br/>using <strong><em>albumworks Online</em></strong></h3>
            <p class="dlbl">Quick and easy! Make on mobile, tablet or your desktop browser.</p>
            <p><a class="cta" id="pbsplitaltcta" href="javascript:void(0)">CREATE ONLINE</a></p>

            <div class="group">
                <div class="dlopt group">
                    <div class="left">
                        <p style="text-align: left; font-weight: 600;">Planning a bigger book with more photos? Choose albumworks Desktop - faster with more design tools and more options!</p>
                        <p><img src="{{env('BASEPATH')}}img/aw-desktop.jpg" /></p>
                    </div>
                    <div class="right">
                        <p><input type="text" id="field_pbsplitchoice_download_first_name" name="first_name" class="firstname inputbox" placeholder="Name"></p>
                        <p><input type="text" id="field_pbsplitchoice_download_email" name="email" class="email inputbox" placeholder="Email"></p>
                        <div class="checkarea group">
                            <input type="checkbox" checked="checked" value="1" id="field_pblightbox_download_emailoptout" name="emailOptOut" class="check">
                            <label for="field_lightbox_download_emailoptout">Keep me updated with special offers and software updates</label>
                        </div>
                        <p><div class="g-recaptcha" style="padding-left:calc(50% - 152px); clear:both" data-sitekey="6LcHNrsZAAAAAJF0hLoCNFils86kV9ik3TG5mXVr" data-callback="grcsetform"></div></p>
                        <p style="margin-top:0;"><a style="margin-top:10px; padding:10px 30px; font-size: 17px;" class="cta" href="javascript:void(0)" id="pbsplitcta" onclick="if(__robot) alert('Please tick `I\'m not a robot`');">DOWNLOAD OUR FREE EDITOR</a></p>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection