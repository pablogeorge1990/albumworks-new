@extends('templates.header')

@section('body')

    <main class="normal static">
        <?
        $error = false;
        $user_email = isset($_GET['user']) ? $_GET['user'] : false;
        $email = str_replace(' ', '+', $user_email);
        $email = custom_escape_string($email);
        define('SFNAMESPACE_SOQL', '');
        $name = $balance = $level = $required = $nextlevel = $totalspend = $vendor = 0;
        if(!$error){
            $query = "SELECT ".SFNAMESPACE_SOQL."Value_to_100__c,  ".SFNAMESPACE_SOQL."Gift_Card_Balance__c, Name, ".SFNAMESPACE_SOQL."Rewards_Level__c, ".SFNAMESPACE_SOQL."Total_Order_Value__c, ".SFNAMESPACE_SOQL."Vendor_ID__c FROM Contact WHERE Email = '".$email."' AND ".SFNAMESPACE_SOQL."Vendor_ID__c = '1031' ";
            try{
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://pwg:MCgC_riGqERn2oERhEK@api.photo-products.com.au/zenpanel/api/sf/query');
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$query, true]));
                $response = curl_exec($ch);
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                if(!in_array($status, [0, 200]))
                    throw new Exception($response);

                $response = json_decode($response);
                if(sizeof($response->records) <= 0){
                    $query = "SELECT ".SFNAMESPACE_SOQL."Value_to_100__c, ".SFNAMESPACE_SOQL."Gift_Card_Balance__c, Name, ".SFNAMESPACE_SOQL."Rewards_Level__c, ".SFNAMESPACE_SOQL."Total_Order_Value__c, ".SFNAMESPACE_SOQL."Vendor_ID__c FROM Contact WHERE Email = '".$email."' AND ".SFNAMESPACE_SOQL."Vendor_ID__c = '3183' ";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://pwg:MCgC_riGqERn2oERhEK@api.photo-products.com.au/zenpanel/api/sf/query');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$query, true]));
                    $response = curl_exec($ch);
                    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);

                    if(!in_array($status, [0, 200]))
                        throw new Exception($response);

                    $response = json_decode($response);
                }

                $balance = '$'.number_format($response->records[0]->{SFNAMESPACE_SOQL."Gift_Card_Balance__c"}, 2);
                $to100 = '$'.number_format($response->records[0]->{SFNAMESPACE_SOQL."Value_to_100__c"}, 2);
                $name = explode(' ', $response->records[0]->Name);
                $name = $name[0];
                $tov = (isset($response->records[0]->{SFNAMESPACE_SOQL."Total_Order_Value__c"}) and is_numeric($response->records[0]->{SFNAMESPACE_SOQL."Total_Order_Value__c"}) and $response->records[0]->{SFNAMESPACE_SOQL."Total_Order_Value__c"}) ? $response->records[0]->{SFNAMESPACE_SOQL."Total_Order_Value__c"} : 0;
                $totalspend = '$'.number_format($tov, 2);
                $level = $response->records[0]->{SFNAMESPACE_SOQL."Rewards_Level__c"};
                $vendor = $response->records[0]->{SFNAMESPACE_SOQL."Vendor_ID__c"};

                switch($level){
                    case 'White':
                        $nextlevel = 'Blue';
                        $required = '$'.number_format(125 - $tov, 2);
                        break;

                    case 'Blue':
                        $nextlevel = 'Silver';
                        $required = '$'.number_format(1000 - $tov, 2);
                        break;

                    case 'Silver':
                        $nextlevel = 'Gold';
                        $required = '$'.number_format(2000 - $tov, 2);
                        break;

                    default:
                        $nextlevel = '';
                        $required = '$0.00';
                        break;
                }

            }
            catch(Exception $e){
                $error = 'Could not query: '.$e->getMessage().' '.json_encode(@$response);
            }
        }

        if($error){
        ?>
        <h2>Error</h2>
        <p>An unexpected error occured.</p>
        <p><a href="{{env('BASEPATH')}}mybalance">Click here to go back and try again.</a></p>
        <p>If this happens multiple times, please <a href="{{env('BASEPATH')}}contact">contact customer service</a> to find out your balance.</p>
        <!-- <p><code>ERROR: <?=$error?></code></p> -->
        <?
        }
        else if(!$name){
        ?>
        <h2>Email not found</h2>
        <p>The email address "<?=$email?>" could not be found in our system, sorry.</p>
        <p><a href="{{env('BASEPATH')}}mybalance">Click here to go back and try again.</a></p>
        <?
        }
        else{
        ?>
        <p style="text-align:left"><img src="{{env('BASEPATH')}}img/rewards/logo.png" alt="albumworks REWARDS" width="260" /></p>
        <h2 style="text-align:left; max-width:100%; margin: 30px auto; font-size:22px" class="group">
            <span style="float:left; padding-top:5px;">Welcome back, <?=$name?></span>
            <span style="float:right;"><strong>Your Level:</strong> <?=$level?> member <img src="{{env('BASEPATH')}}img/rewards/<?=strtolower($level)?>.png" /></span>
        </h2>

        <h4 class="sectionhead">Your Rewards Dollars:</h4>
        <div class="center">
            <p class="balance"><?=$balance?></p>
            <p>You currently have <strong><?=$balance?></strong> to spend on your next purchase.</p>
            <p>
                <? if($level == 'Gold'): ?>
                You are currently a <strong><?=$level?> Member</strong>.
                <? else: ?>
                You are currently a <strong><?=$level?> Member</strong>. Your total spend is <strong><?=$totalspend?></strong>.
                <? endif; ?>

                <? if($nextlevel): ?>
                Spend <strong><?=$required?></strong> to reach <?=$nextlevel?> level, and unlock more rewards.
                <? else: ?>
                Spend <?=$to100?> to earn $20 Rewards dollars to spend on your next order.
                <? endif; ?>
            </p>
        </div>

        <h4 class="sectionhead" style="margin-top:50px;">How does it work?</h4>
        <div class="center">
            <p>Once you are in the Rewards Club you earn Rewards dollars for every $100 you spend.</p>
            <p>The amount of dollars you earn depends on what level you are.</p>
        </div>
        <div style="margin:0 auto; width:100%; max-width:380px">
            <div style="background:url({{env('BASEPATH')}}img/rewards/white.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left;">
                <h3 style="margin-bottom:0; font-weight:400">White member</h3>
                <h4 style="margin:0; font-size:14px; font-weight:700">Spent less than $125</h4>
            </div>
            <div style="background:url({{env('BASEPATH')}}img/rewards/blue.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left;">
                <h3 style="margin-bottom:0; font-weight:400">Blue member</h3>
                <h4 style="margin:0; font-size:14px; font-weight:700">Spent at least $125 with us</h4>
                <h4 style="margin:0; font-size:14px; font-weight:300; font-style: italic;">Get $10 for every $100 spent as a Rewards Club member</h4>
            </div>
            <div style="background:url({{env('BASEPATH')}}img/rewards/silver.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left;">
                <h3 style="margin-bottom:0; font-weight:400">Silver member</h3>
                <h4 style="margin:0; font-size:14px; font-weight:700">Spent at least $1,000 with us</h4>
                <h4 style="margin:0; font-size:14px; font-weight:300; font-style: italic;">Get $15 for every $100 spent as a Rewards Club member</h4>
            </div>
            <div style="background:url({{env('BASEPATH')}}img/rewards/gold.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left;">
                <h3 style="margin-bottom:0; font-weight:400">Gold member</h3>
                <h4 style="margin:0; font-size:14px; font-weight:700">Spent at least $2,000 with us</h4>
                <h4 style="margin:0; font-size:14px; font-weight:300; font-style: italic;">Get $20 for every $100 spent as a Rewards Club member</h4>
            </div>
        </div>
        <p class="center">View full <a href="{{env('BASEPATH')}}rewards-terms">Terms &amp; Conditions here</a></p>
        <?
        }
        ?>
    </main>

@endsection