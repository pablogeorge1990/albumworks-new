@extends('templates.header')
@section('title', 'Photo Books: Beautiful and personalised | albumworks')
@section('meta_description', 'Create beautiful personalised Photo Books. Easy to use, free software. 100% design freedom, fast delivery and local support.')


@section('body')
    <main class="normal static">
        <h1 style="margin-bottom:5px">How To Make</h1>
        <h2 class="my-header" style="margin-top:0">using the Download Editor</h2>
        <p class="center" style="margin-bottom:35px"><a target="_blank" href="https://www.youtube.com/user/albumworks"><img src="{{asset('img/howtomake2020/cta.png')}}" alt="For more helpful tips, visit our video tutorials"/></a></p>
        <div class="group leftright">
            <div class="group">
                <div class="content">
                    <h3>1. Download our Editor</h3>
                    <p>If you haven’t already, click "CREATE NOW" at the top of the page. Enter your details then click ‘DOWNLOAD OUR FREE EDITOR’. Once downloaded,  open the setup file to install the program on your computer. Once installed, you can return to the Editor at any time. Just look for the <em>albumworks</em> logo on your desktop (PC) or in your Applications folder (Mac).</p>
                    <p>If you have problems installing the Editor, see this <a href="https://www.albumworks.com.au/faq?solution_ID=30010">step through process</a></p>
                    <p>For more details see our <a href="https://www.albumworks.com.au/faq?solution_ID=30010">FAQs</a> or watch <a href="https://www.youtube.com/watch?v=BZbp6U4YID8">our detailed tutorial here</a>.</p>
                </div>
                <img src="{{asset('img/howtomake2020/01.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>2. Select your Photo Book type</h3>
                    <p>Open the Editor and click ‘START NEW PROJECT’. Our Editor wizard will take you through choosing your Photo Book binding, theme, size, orientation and cover type  Our best selling size is the 11 x 8.5" Landscape - not too big and not too small!</p>
                </div>
                <img src="{{asset('img/howtomake2020/02.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>3. Upload your Photos</h3>
                    <p>Next you’ll be asked to upload your photos. Upload images directly from your computer or import them from Facebook, Instagram or Flickr. We recommend organising your photos into folders before starting, but you can upload more photos later. Click on the white "+" symbol for import options and follow the prompts.</p>
                </div>
                <img src="{{asset('img/howtomake2020/03.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>4. Drag and drop your photos</h3>
                    <p>Putting your photos onto the pages of your Photo Book is as simple as dragging and dropping them. You can make them bigger and smaller, enlarge and crop.</p>
                </div>
                <img src="{{asset('img/howtomake2020/04.gif')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>5. Apply design features</h3>
                    <p>Add backgrounds, masks, frames, scrapbooking and lots of other design features to your Photo Book, just by dragging them onto the pages. You’ll find these options in the tabs at the bottom of the page.</p>
                </div>
                <img src="{{asset('img/howtomake2020/05.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>6. Add text to your pages</h3>
                    <p>Adding text throughout your book is simple! Click on the "T" symbol in the tool bar and a text box will appear on your page. Customise your text by changing the font, colour and size in the far right hand panel. You can save your text style to use throughout the album.</p>
                </div>
                <img src="{{asset('img/howtomake2020/06.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>7. Add extra pages to your Book</h3>
                    <p>All Classic and Layflat Photo Books come with 40 pages as standard. You can add more pages by clicking on the "Page Options" button on the toolbar. You can have up to 200 pages in Classic Books and up to 140 pages in Layflat Books.</p>
                </div>
                <img src="{{asset('img/howtomake2020/07.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>8. Review your project</h3>
                    <p>Once you have finished designing your Photo Book you should carefully review it using the "Preview" feature - click on the white "eye" button on the toolbar. Be sure to double check your spelling, and ensure all important content is well within the red lines on the page.</p>
                </div>
                <img src="{{asset('img/howtomake2020/08.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>9. Proceed to the Shopping Cart</h3>
                    <p>Once you’re finished, go to checkout by clicking the green Shopping Cart in the top right corner. The Editor will check for any warnings or errors with your project before opening the Shopping Cart. Follow the Editor prompts and it will open a new web page for you to place your order. If this is your first order with <em>albumworks</em> you will need to create an account at this stage.</p>
                </div>
                <img src="{{asset('img/howtomake2020/09.jpg')}}" height="380" />
            </div>
            <div class="group">
                <div class="content">
                    <h3>10. Order your product</h3>
                    <p>After logging in, simply follow the prompts inside the Shopping Cart to order your product! Add finishing options that are available for your product or upgrade your printing to Hi Colour.</p>
                </div>
                <img src="{{asset('img/howtomake2020/10.jpg')}}" height="380" />
            </div>
        </div>
    </main>

@endsection