@extends('templates.header')

@section('body')
    <main class="normal static">
    <h1>Bulk Ordering</h1>
    <div class="group marquee">
        <div class="content">
            <h2>Take advantage of our Bulk Order Speciality Service!</h2>
            <p>We will personally manage your order from inception, creation and to delivery, ensuring you have the maximum discount and a product that has been manually design checked to surpass your final product expectations.</p>
            <p>Fill out the form below and our specialist will be in touch with you within 48 hours.</p>

            <p><em>Form currently offline. Please contact customer service<br/>via 1300-553-448.</em></p>

            <form action="endpoints/mailform.php" method="post" class="bigform noempties norobot">
                <input type="hidden" name="email__returl" value="thankyou-bulk" />
                <input type="hidden" name="email__body" value="dump" />
                <input type="hidden" name="email__subject" value="Bulk Order Quote Request" />
                <input type="hidden" name="email__to" value="awservice" />
                
                <p>
                    <label for="contact_first_name">First Name:</label>
                    <input id="field_contact_first_name" class="inputbox" name="firstname" type="text" />
                </p>

                <p>
                    <label for="contact_last_name">Last Name:</label>
                    <input id="field_contact_last_name" class="inputbox" name="lastname" type="text" />
                </p>

                <p>
                    <label for="contact_email">Email:</label>
                    <input id="field_contact_email" class="inputbox required validate-email" name="email" type="text" />
                </p>

                <p>
                    <label for="contact_number">Phone:</label>
                    <input id="field_contact_number" class="inputbox required" name="contact_number" type="text" />
                </p>

                <p>
                    <label for="postcode">Delivery Postcode:</label>
                    <input id="field_postcode" class="inputbox canempty" name="postcode" type="text" />
                </p>

                <p class="subtext"><em>The below details below are optional. Provide as much detail in order to get the best bulk order deal. If you're not 100% sure what you are after, just drop us a short message!</em></p>

                <p>
                    <select name="print_quality" id="field_print_quality" class="inputbox canempty">
                        <option value="">Print Quality</option>
                        <option value="High Definition">High Definition</option>
                        <option value="Standard Definition">Standard Definition</option>
                    </select>
                </p>

                <p>
                    <select name="book_type" id="field_book_type" class="inputbox canempty">
                        <option value="">Book Type</option>
                        <option value="Classic Photo Book">Classic Photo Book</option>
                        <option value="Layflat Photo Book">Layflat Photo Book</option>
                    </select>
                </p>

                <p>
                    <select name="book_size" id="field_book_size" class="inputbox canempty">
                        <option value="">Book Size</option>
                        <option value="6 x 6">6 x 6</option>
                        <option value="8 x 6">8 x 6</option>
                        <option value="8 x 8">8 x 8</option>
                        <option value="8 x 11">8 x 11</option>
                        <option value="11 x 8.5">11 x 8.5</option>
                        <option value="12 x 12">12 x 12</option>
                        <option value="16 x 12">16 x 12</option>
                    </select>
                </p>

                <p>
                    <select name="cover_type" id="field_cover_type" class="inputbox canempty">
                        <option value="">Cover Type</option>
                        <option value="Soft Photocover">Soft Photocover</option>
                        <option value="Hard Photocover">Hard Photocover</option>
                        <option value="Debossed Cover">Debossed Cover</option>
                        <option value="Premium Material Cover">Premium Material Cover</option>
                    </select>
                </p>

                <p>
                    <select name="pages" id="field_pages" class="inputbox canempty">
                        <option value="">Pages</option>
                        <option value="31-40">31-40</option>
                        <option value="41-50">41-50</option>
                        <option value="51-60">51-60</option>
                        <option value="61-70">61-70</option>
                        <option value="71-80">71-80</option>
                        <option value="81-90">81-90</option>
                        <option value="91-100">91-100</option>
                        <option value="101-110">101-110</option>
                        <option value="111-120">111-120</option>
                        <option value="121-130">121-130</option>
                        <option value="131-140">131-140</option>
                        <option value="141-150">141-150</option>
                        <option value="151-160">151-160</option>
                        <option value="161-170">161-170</option>
                        <option value="171-180">171-180</option>
                        <option value="181-190">181-190</option>
                        <option value="191-200">191-200</option>
                    </select>
                </p>

                <p>
                    <label for="quantity">Quantity:</label>
                    <input id="quantity" class="inputbox canempty" name="quantity" type="text" />
                </p>

                <p>
                    <label for="message">Message:</label>
                    <textarea id="message" class="inputbox canempty" cols="50" name="text" rows="10"></textarea>
                </p>

                <p>
                    <div class="g-recaptcha" data-sitekey="6LcHNrsZAAAAAJF0hLoCNFils86kV9ik3TG5mXVr"></div>
                </p>

                <p><a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="cta">SUBMIT</a></p>
            </form>

        </div>
        <img src="{{asset('img/bulk/01.jpg')}}" class="nomobile" />
    </div> 
</main>

@endsection