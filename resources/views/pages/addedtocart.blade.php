@extends('templates.header')

@section('body')
    <main class="normal static">
        <h2 class="preheading">Your project has been added to your Cart!</h2>
        <h1 class="haspre">What’s next?</h1>
        <div class="floatingblocks group">
            <div class="block">
                <p class="img"><img src="img/addedtocart/checkout.png" /></p>
                <h2>Check out items in your cart</h2>
                <p>View your items by clicking on your Cart at the top of the screen. Or just click below.</p>
                <p><a href="javascript:void(0)" onclick="tpxBasketOnClick()" class="cta check-btn">CHECK OUT</a></p>
            </div>
            <div class="block">
                <p class="img"><img src="img/addedtocart/make.png" /></p>
                <h2>Make another product</h2>
                <p>Choose another photo product to make and add to your cart. View our all new Gifts range!</p>
                <p><a href="gifts" class="cta">VIEW ALL GIFTS</a></p>
            </div>
            <div class="block">
                <p class="img"><img src="img/addedtocart/promo.png" /></p>
                <h2>View our promotions</h2>
                <p>Don’t miss out! Visit our Promotions page to see if you could potentially save on your order!</p>
                <p><a href="promotions" class="cta">PROMOTIONS</a></p>
            </div>
            <div class="block">
                <p class="img"><img src="img/addedtocart/contact.png" /></p>
                <h2>Contact us</h2>
                <p>Got a question? We can help. Visit our <a href="faq">FAQs page</a> here or click below to get in touch with us.</p>
                <p><a href="contact" class="cta">CONTACT US</a></p>
            </div>
        </div>
    </main>

@endsection