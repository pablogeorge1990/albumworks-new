@extends('templates.header')

@section('title', $__pagedata['document_title'])
@section('meta_description', $__pagedata['meta_description'])

@section('body')

    <nav class="persistent">
        @foreach($__pagedata['menu'] as $link => $label)
            <a href="#{{$link}}" class="<?=(($link == 'overview') ? 'active' : '')?>">{{$label}}</a>
        @endforeach
        <a class="cta" href="{{$__pagedata['createlink']}}">CREATE NOW</a>
    </nav>
    <div class="dummypers"></div>
    <main class="fullwidth bookmark landing" id="overview">
        @foreach($__pagedata['contents'] as $item)
            @if($item['type'] == 'parallax')
                <section class="parallax panel hastext leader" style="background-image:url({{asset(@$item['image'])}})" data-imgr="{{(660/1600)}}">
                    <div class="text {!! @$item['textclass'] !!}">
                        <h2>{!! $item['heading'] !!}</h2>
                        @if($item['subline'])
                            <p>{!!@$item['subline'] !!}</p>
                        @endif
                    </div>
                </section>
            @elseif($item['type'] == 'bigheader')
                <section class="bigheader bookmark" id="{{@$item['id']}}">
                    <h1>{!! $item['heading'] !!}</h1>
                    <p>{!! $item['subline'] !!}</p>
                </section>
            @elseif($item['type'] == 'prodpanel')
                <section class="panel hastext prodpanel group {{@$item['classes']}}" id="{{@$item['id']}}" style="background-image:url({{@asset($item['image'])}})" imagearea="800">
                    <div class="text {{@$item['textclasses']}}">
                        <h2>{!!$item['heading']!!}</h2>
                        {!!@$item['content'] !!}
                        <p><a href="{{$__pagedata['createlink']}}" class="cta">CREATE NOW</a></p>
                    </div>
                    <img src="{{asset(@$item['mobileimage'])}}" class="mobileonlyblock" />
                </section>
            @elseif($item['type'] == 'prodpres')
                <section class="prodpres narrow group {{@$item['classes']}}" id="{{@$item['id']}}">
                    <div class="wording vertmid">
                        <h2>{!! $item['heading'] !!}</h2>
                        {!! @$item['content'] !!}
                    </div>
                    <img src="{{asset(@$item['image'])}}" />
                </section>
            @endif
        @endforeach
        <div class="psc group bookmark" id="pricing">
            @if(sizeof($__pagedata['prices']) and isset($__pagedata['prices'][0]) and sizeof($__pagedata['prices'][0]))
                <div class="prices group">
                    <h2>Prices</h2>
                    @foreach($__pagedata['prices'] as $pricetable)
                        <table cellpadding="0" cellspacing="0" class="{{sizeof($__pagedata['prices']) > 1 ? 'multitable' : 'single'}}">
                            <thead>
                            @foreach($pricetable as $i => $row)
                                @if($i == 1)
                                    </thead>
                                    <tbody>
                                @endif
                                <tr>
                                    @foreach($row as $j => $cell)
                                        @if($i == 0 and $j == 0)
                                            <td style="width:20%">{{$cell}}</td>
                                        @elseif($i == 0 or $j == 0)
                                            <th style="width:{{(($i==0)?floor(80/sizeof($row)).'%':'auto')}}">{!! $cell !!}</th>
                                        @else
                                            <td>{!!$cell !!}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endforeach
                </div>
            @endif

            <div class="shipping">
                <h2>Shipping</h2>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    @foreach($__pagedata['shipping'] as $i => $row)
                        @if($i == 1)
                            </thead>
                            <tbody>
                        @endif
                        <tr>
                            @foreach($row as $j => $cell)
                                @if($i == 0 and $j == 0)
                                    <td style="width:20%">{!! $cell !!}</td>
                                @elseif($i == 0 or $j == 0)
                                    <th style="width:{{(($i==0)?floor(80/sizeof($row)).'%':'auto')}}">{!! $cell !!}</th>
                                @else
                                    <td>{!! $cell !!}</td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="notes">
                    {!! @$__pagedata['below_shipping'] !!}
                    {{--<p><strong>PO BOXES:</strong> <em>Delivery of all items to a PO Box address will incur an additional $5.00</em></p>--}}
                    {{--<p><strong>MULTIPLE ITEMS:</strong> <em>Where multiple items are purchased at the same time, "FIRST COPY" shipping price will be charged for the largest item and "EXTRA COPY" shipping price will be charged for all other items.</em></p>--}}
                </div>

                <div class="contact">
                    <h2>Contact</h2>
                    <img src="{{asset('img/contact.jpg')}}" />
                    <p>Still have questions? Ask us! Our amazing Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible.</p>
                    <p><a href="contact" class="cta">CONTACT</a></p>
                </div>
            </div>
        </div>
    </main>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#primary .cta').attr('href', '{{$__pagedata['createlink']}}');
        });
    </script>


@endsection


