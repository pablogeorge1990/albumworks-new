@extends('templates.header')

@section('body')
    <main class="normal static">

        <h1 style="margin-bottom:0.5em;">Create your product <select id="onlineoffline"><option>Online</option><option>Offline</option></select></h1>
        <div class="subheading group onlineonly">
            <img style="float:right; margin-left:10px;" src="{{env('BASEPATH')}}img/create-online.jpg" />
            <p>Create your photo product online with our intuitive Online Editor.  Great for smaller projects when you’re on the go, as you can create on your phone, tablet or computer.</p>
        </div>
        <div class="subheading group offlineonly" style="display:none">
            <img style="float:right; margin-left:10px;" src="{{env('BASEPATH')}}img/create-offline.jpg" />
            <p>Perfect for projects with lots of photos. Our Downloadable Editor can be used offline on your desktop computer at your convenience and comes with the widest range of design tools and abilities.</p>
        </div>
        <div class="group">
            <div class="productblock">
                <h3>Photo Books</h3>
                <a href="{{env('BASEPATH')}}themes"><img src="{{env('BASEPATH')}}img/popular-desktop/01-photobooks.jpg" /></a>
                <p>Create a beautiful Photo Book full of all your memories. A fantastic photo keepsake in a range of covers, sizes and paper types. <a href="{{env('BASEPATH')}}themes">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}themes" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Canvas Prints</h3>
                <a href="{{env('BASEPATH')}}canvas-photo-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-desktop/02-canvas.jpg" /></a>
                <p>Classic Canvas construction, printed on high quality poly-cotton canvas with a satin finish. Create your own gallery from your favourite photos. <a href="{{env('BASEPATH')}}canvas-photo-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}canvas-photo-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Calendars</h3>
                <a href="{{env('BASEPATH')}}wall-calendar-create-now"><img src="{{env('BASEPATH')}}img/popular-desktop/03-calendars.jpg" /></a>
                <p>Beautiful 12 month personalised Calendars filled with your precious photos and printed on premium photo papers. <a href="{{env('BASEPATH')}}wall-calendar">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}wall-calendar-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Framed Prints</h3>
                <a href="{{env('BASEPATH')}}framed-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-desktop/04-framed.jpg" /></a>
                <p>High resolution prints with exceptional colour reproduction. Choose from full frame prints or prints with a mat board.  <a href="{{env('BASEPATH')}}framed-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}framed-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Premium Prints</h3>
                <a href="{{env('BASEPATH')}}premium-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-desktop/05-premiumprints.jpg" /></a>
                <p>Quality photos prints are back. Printed on our state of the art main lab HD photo press. Bring your photos to life in a classic set of premium photo prints. <a href="{{env('BASEPATH')}}premium-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}premium-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Instagram Prints</h3>
                <a href="{{env('BASEPATH')}}instagram-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-desktop/06-instagram.jpg" /></a>
                <p>Turn any of your photos into stylish Photo Cards. Create from your phone, tablet or desktop using your Instagram, Facebook or local photos. <a href="{{env('BASEPATH')}}instagram-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}instagram-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
        </div>

        <div class="nomobile">
            <h2 class="downloadhead">Or download our Advanced Editor to get the full range of products</h2>
            <form class="box group" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
                <a name="join"></a>
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=MOBILEBOOK" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="MOBILEBOOK" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>

                <!-- Actual fields -->
                <input type="text" name="first_name" class="firstname inputbox" placeholder="NAME">
                <input type="text" name="email" class="email inputbox" placeholder="EMAIL">
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET OUR FREE EDITOR</a>
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
                    <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
                </div>
            </form>
        </div>
    </main>

@endsection