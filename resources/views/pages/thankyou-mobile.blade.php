@extends('templates.header')

@section('body')

    <main class="normal textonly">
        <h1>Thank you!</h1>
        <p>We have sent a link to your requested email address!</p>
    </main>

@endsection