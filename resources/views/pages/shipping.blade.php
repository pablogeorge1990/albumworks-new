@extends('templates.header')

@section('body')

<main class="normal static">
    <h1>Shipping</h1>
    <div class="group leftright">
        <div class="group">
            <div class="content">
                <h3>Make it snappy</h3>
                <p>Our priority is to make all your products as fast as we can - but never at the expense of quality. In fact 95% off items are manufactured and dispatched within 2-4 business days. However we do recommend allowing 7 business days* for dispatch for those rare occasions when your order doesn't pass internal quality control.</p>
            </div>
            <img src="img/shipping/01.jpg" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Receiving your order</h3>
                <p>Our Hardcover Photo Books are delivered via DHL Express which typically takes 2 business days to metropolitan areas. Some rural and remote areas may take longer and sometimes a 3rd party courier service will be utilised by DHL in these areas. If your order is greater than 3kg and/or is regional, it will be delivered by Australia Post EPARCEL instead of DHL Express.</p>
                <p>Softcover Books, Canvas Prints, and some photo gift products are delivered using a selection of logistics providers including Australia Post, Couriers Please and Aramex. Delivery of these items to metropolitan areas typically takes 2 - 5 business days.</p>
                <p><strong>PO BOXES:</strong> Unfortunately delivery to a PO Box or locked bag is not available for Canvas Prints and large Framed Prints.</p>
            </div>
            <img src="img/shipping/02.jpg" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Production and delivery times</h3>
                <p>The time it takes to print and produce your order is separate from delivery time. Photo Books need a little extra time for inks to set and binding to cure so that your Photo Book will be the best quality it can be. As a guide, 95% of orders are produced within 2-4 business days.
                    <br><br>
                    We print our albums and photo products both here in Australia and in Malaysia in state of the art production facilities with the most modern and up to date printing techniques and printing presses.
                    <br><br>
                    Our Hi Colour and Layflat printing presses are located in Malaysia enabling us to provide unparalleled print quality choice.
                    <br><br>
                    Our Standard Colour products are produced either in Australia or Malaysia with most photo gifting products produced in Australia.
                    <br><br>
                    Once produced, your order will be dispatched and on its way to you! You can track your orders progress using our tracking system below.
                </p>
            </div>
            <img src="img/shipping/03.jpg" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Track your order</h3>
                <p>Want to know where your order is?</p>
                <p>You can track it online.</p>
                <p><a href="{{env('BASEPATH')}}track" class="cta">TRACK NOW</a></p>
            </div>
            <img src="img/shipping/04.jpg" height="380" />
        </div>
    </div>    
</main>

@endsection