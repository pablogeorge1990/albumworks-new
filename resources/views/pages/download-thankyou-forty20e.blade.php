@extends('templates.header')

@section('css')
    <style>
        .h3-landing-forty20b{
            padding: 20px;
            width: 50%;
            background: #1063ba;
            color: #ffffff;
        }
    </style>
@endsection

@section('body')

    <main class="normal static">
    <h1>Thanks<span id="dl_thankyou_name"></span>!</h1>

    <div id="voucher-promo-code"><h3 class="title text-center h3-landing-forty20b"> YOUR 40% OFF VOUCHER CODE: <span class="font-weight-bold voucher-code-forty20b">FORTYE</span></h3></div>

    <div class="group leftright">
        <? if(time() > mktime(0,0,0,1,2,2018) and time() < mktime(0,0,0,1,31,2018)): ?>
            <div class="group">
                <div class="content">
                    <h3 class="reversed">Check out our latest promotion!</h3>
                    <p>Don't miss out on fantastic savings on <em>albumworks</em> products! From Photo Books to Calendars, there's always potential savings available. Why not check out what's on offer right now?!</p>
                    <p><a class="cta" href="promotions">PROMOTIONS</a></p>
                </div>
                <img src="{{asset('img/promo/jan18/promo.jpg')}}" height="380" />
            </div>
        <? endif; ?>
        <? if(time() > mktime(0,0,0,2,1,2018) and time() < mktime(0,0,0,3,1,2018)): ?>
            <div class="group">
                <div class="content">
                    <h3 class="reversed">Check out our latest promotion!</h3>
                    <p>Don't miss out on fantastic savings on <em>albumworks</em> products! From Photo Books to Calendars, there's always potential savings available. Why not check out what's on offer right now?!</p>
                    <p><a class="cta" href="promotions">PROMOTIONS</a></p>
                </div>
                <img src="{{asset('img/promo/feb18/promo.jpg')}}" height="380" />
            </div>
        <? endif; ?>
        <div class="group">
            <div class="content">
                <h3>Your Editor should now be downloading</h3>
                <p>Once your download is completed, you can get started on your project!</p>
                <p>We’ve put together a quick guide on making your very first Photo Book! Visit our “How To Make” page.</p>
                <p><a class="cta" href="photo-books-howto">HOW TO MAKE</a></p>
                <p>Need help now? We are available Monday to Friday from 9am to 6pm on <span style="font-weight:bold">1300 553 448</span>, via live chat or via email at service@albumworks.com.au. We are more than happy to assist with any inquiry, big or small!</p>
            </div>
            <img src="{{asset('img/dlty/01.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Not sure if the Editor has downloaded?</h3>
                <p>Your download should start automatically within a few seconds, but if not, please click <a href="https://s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/albumworksSetup.exe">here for Windows</a> or <a href="https://s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/albumworksSetup.dmg">here for Mac</a>.</p>
                <ul class="dlinstructions chrome">
                    <li>Once your download begins, it should appear in the bottom left hand corner of your page.</li>
                    <li>Once your download has completed, please click on the .exe at the bottom left of the screen.</li>
                    <li>The <em>albumworks</em> Setup should now pop up on your screen. From here you can simply follow the prompts to install the <em>albumworks</em> Editor. </li>
                </ul>
                <ul class="dlinstructions firefox">
                    <li>Once your download begins, it should give you the option to Save File or Cancel. Please select Save File.</li>
                    <li>Your download should appear in the top right hand corner of your web browser. </li>
                    <li>When your download has completed the Download Complete box should pop up in the bottom right hand corner for a few moments.</li>
                    <li>Please click on the down arrow in the top right hand corner of your screen and then click on the .exe file.  </li>
                    <li>The <em>albumworks</em> Setup should now pop up on your screen. From here you can simply follow the prompts to install the <em>albumworks</em> Editor. </li>
                </ul>
                <ul class="dlinstructions ie">
                    <li>Once your download begins, you should have the option to Run, Save or Cancel. Please select Save </li>
                    <li>It will then ask you to save this in a particular folder. Select the location you wish and click 'Save'. </li>
                    <li>Your editor should then start downloading. </li>
                    <li>Once your download is complete, please select 'Run' from the options available. </li>
                    <li>Your system will then ask if you want to "Run" or "Don't Run". Please select "Run".</li>
                    <li>The <em>albumworks</em> Setup should now pop up on your screen. From here you can simply follow the prompts to install the <em>albumworks</em> Editor. </li>
                </ul>                    
                <ul class="dlinstructions safari">
                    <li>Once you have selected to download the editor, your download should appear in the top right hand corner of your web browser. </li>
                    <li>Once your download has completed, please click on the down arrow in the top right hand corner of your screen and then click on the .exe file</li>
                    <li>The <em>albumworks</em> Setup should now pop up on your screen. From here you can simply follow the prompts to install the <em>albumworks</em> Editor.</li>
                </ul>                    
            </div>
            <img class="browserimg" src="img/grey.png" width="530" height="380" />
        </div>
    </div>   
    
    <h1>Book an introduction to <em>albumworks</em> phone call (15 mins)</h1> 
    <div style="margin:0 auto; width:780px">
        <script type="text/javascript" src="//pictureworks.simplybook.me/iframe/pm_loader_v2.php?width=780&url=//pictureworks.simplybook.me&theme=clean_slide_blue&layout=cleanside&timeline=flexible&mode=desktop&mobile_redirect=0&hidden_steps=event&event=2"></script>
    </div>
    
    <div style="display: none">
        <script type="text/javascript">
            $(document).ready(function(){
                (function(){var a={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(a){for(var b=0;b<a.length;b++){var c=a[b].string;var d=a[b].prop;this.versionSearchString=a[b].versionSearch||a[b].identity;if(c){if(c.indexOf(a[b].subString)!=-1)return a[b].identity}else if(d)return a[b].identity}},searchVersion:function(a){var b=a.indexOf(this.versionSearchString);if(b==-1)return;return parseFloat(a.substring(b+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};a.init();window.jQuery.client={os:a.OS,browser:a.browser}})();
                
                if($.client.browser == 'Chrome'){
                    $('.chrome').show();
                    $('.browserimg').attr('src', '{{asset('img/dlty/chrome.jpg')}}');
                }
                if($.client.browser == 'Firefox'){
                    $('.firefox').show();
                    $('.browserimg').attr('src', '{{asset('img/dlty/firefox.jpg')}}');
                }
                if($.client.browser == 'Explorer'){
                    $('.ie8').show();
                    $('.browserimg').attr('src', '{{asset('img/dlty/ie.jpg')}}');
                }
                if($.client.browser == 'Safari'){
                    $('.safari').show();
                    $('.browserimg').attr('src', '{{asset('img/dlty/safari.jpg')}}');
                }

                var download_link = '//s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/albumworksSetup.exe';
                if($.client.os == 'Mac')
                    download_link = '//s3-ap-southeast-2.amazonaws.com/albumprinter-editors-syd/albumworksSetup.dmg';
                $('body').append('<iframe id="downloadiframe" style="position:absolute; width:1px; height:1px; top:-999em; left:-999em;" src="'+download_link+'"></iframe>');
            });
        </script>    

        <!-- Facebook Conversion Code for AW Editor Software Downloads -->
        <script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
        }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6016085745726', {'value':'0.01','currency':'AUD'}]);
        fbq('track', 'Lead');
        </script>
        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6016085745726&amp;cd[value]=0.01&amp;cd[currency]=AUD&amp;noscript=1" /></noscript>            
        
        <!-- Google Code for Lead Conversion Page -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1060947230;
        var google_conversion_language = "en_AU";
        var google_conversion_format = "1";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "yiZdCLL8UhCeivP5Aw";
        var google_conversion_value = 1.00;
        var google_conversion_currency = "AUD";
        var google_remarketing_only = false;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1060947230/?value=1.00&amp;currency_code=AUD&amp;label=yiZdCLL8UhCeivP5Aw&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>        

                         
        <!-- BING CODE FOR CONVERSIONS -->
        <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5220373"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5220373&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
    </div>
</main>

@endsection

@section('scripts')
   
@endsection