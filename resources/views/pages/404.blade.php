@extends('templates.header')

@section('body')
    <main class="normal static">
        <h1>OH NO!</h1>
        <p>This page appears to be missing...</p>
        <p>What are you looking for?  </p>
        <p><a href="photo-books">Photo Books?</a> </p>
        <p><a href="photo-calendar">Calendars?</a> </p>
        <p>How about <a href="canvas-photo-prints">Canvas Prints?</a></p>
        <p>Still not sure? Just head back to our <a href="/">Home Page</a>.</p>
        <p>Thanks!</p>
    </main>

@endsection