@extends('templates.header')

@section('title', 'Photo Books: Beautiful and personalised | albumworks')
@section('meta_description', 'Create beautiful personalised Photo Books from your computer, table or mobile phone. Easy to use, free software. 100% design freedom, fast delivery and local support.')

@section('body')
    <main class="normal static" id="style2018">
        <h1><span>Photo Books</span></h1>
        <h2>Use our product selector to choose the right book for you</h2>

        <div class="phodwiz">
            <a href="javascript:void(0)" class="left arrow">Left</a>
            <a href="javascript:void(0)" class="right arrow">Right</a>
            <div class="phodwiztheatre">
                <div class="wizstage activestage" stage="1" style="opacity:1; display:block;">
                    <h3><span class="group"><span>STEP 1. BINDING</span></span></h3>
                    <div class="wizstagechoicescontainer">
                        <ul class="wizstagechoices">
                            <li class="visible" type="classic" binding_h4="Classic Photo Book" binding_min="40" binding_max="200" binding_paper="150gsm SD Premium Silk" binding_paper_upgrades="250gsm HD Lustre or Glossy">
                                <img src="img/photobookwizard/binding/classic-book.jpg" alt="classic" />
                                <h4>CLASSIC PHOTO BOOK</h4>
                                <h5>Traditional book making. 40 pages.</h5>
                                <p>from <strong>$19.95</strong></p>
                            </li>
                            <li class="visible" type="layflat" binding_h4="Layflat Photo Book" binding_min="40" binding_max="152" binding_paper="400gsm SD E-Photo Matte" binding_paper_upgrades="">
                                <img src="img/photobookwizard/binding/layflat-book.jpg" alt="layflat" />
                                <h4>LAYFLAT PHOTO BOOK</h4>
                                <h5>Lays completely flat when open. 40 pages.</h5>
                                <p>from <strong>$59.50</strong></p>
                            </li>
                            <li class="visible" type="board" binding_h4="Board Mounted Photo Book" binding_min="20" binding_max="50" binding_paper="SD E-Photo Matte paper. Fused onto heavyweight core board" binding_paper_upgrades="">
                                <img src="img/photobookwizard/binding/board-mounted.jpg" alt="board mounted" />
                                <h4>BOARD MOUNTED PHOTO BOOK</h4>
                                <h5>Thick, luscious pages. High quality. 40 pages.</h5>
                                <p>from <strong>$149.95</strong></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="wizstage" stage="2">
                    <h3><span class="group"><a href="javascript:void(0)" class="back inactive">Back</a><span>STEP 2. SIZE</span></span></h3>
                    <div class="wizstagechoicescontainer">
                        <ul class="wizstagechoices">
                            <li class="visible" type="11x85" orientation="landscape" size_label='11 x 8.5" Landscape' size_alt="(27.5 x 22cm)" shipping="+ $11.95 shipping">
                                <img src="img/photobookwizard/size/11x85.jpg" alt="11x85" />
                                <h4>11x8.5" LANDSCAPE</h4>
                                <h5>22 x 27.5cm</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="12x12" orientation="square" size_label='12 x 12" Square' size_alt="(30.5 x 30.5cm)" shipping="+ $14.95 shipping">
                                <img src="img/photobookwizard/size/12x12.jpg" alt="12x12" />
                                <h4>12x12" SQUARE</h4>
                                <h5>30.5 x 30.5cm</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="16x12" orientation="landscape" size_label='16 x 12" Landscape' size_alt="(40.5 x 30.5cm)" shipping="+ $16.95 shipping">
                                <img src="img/photobookwizard/size/16x12.jpg" alt="16x12" />
                                <h4>16x12" LANDSCAPE</h4>
                                <h5>40.5cm x 30.5cm</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="8x11" orientation="portrait" size_label='8 x 11" Portrait' size_alt="(20.5 x 27.5cm)" shipping="+ $11.95 shipping">
                                <img src="img/photobookwizard/size/8x11.jpg" alt="8x11" />
                                <h4>8x11" PORTRAIT</h4>
                                <h5>20.5 x 27.5cm</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="8x8" orientation="square" size_label='8 x 8" Square' size_alt="(20.5 x 20.5cm)" shipping="+ $11.95 shipping">
                                <img src="img/photobookwizard/size/8x8.jpg" alt="8x8" />
                                <h4>8x8" SQUARE</h4>
                                <h5>20.5 x 20.5cm</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="8x6" orientation="landscape" size_label='8 x 6" Landscape' size_alt="(20.5 x 15cm)" shipping="+ $11.95 shipping">
                                <img src="img/photobookwizard/size/8x6.jpg" alt="8x6" />
                                <h4>8x6" LANDSCAPE</h4>
                                <h5>20.5 x 15cm</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="6x6" orientation="square" size_label='6 x 6" Square' size_alt="(15 x 15cm)" shipping="+ $11.95 shipping">
                                <img src="img/photobookwizard/size/6x6.jpg" alt="6x6" />
                                <h4>6x6" SQUARE</h4>
                                <h5>15 x 15cm</h5>
                                <p>from <strong> </strong></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="wizstage" stage="3">
                    <h3><span class="group"><a href="javascript:void(0)" class="back inactive">Back</a><span>STEP 3. COVER</span></span></h3>
                    <div class="wizstagechoicescontainer">
                        <ul class="wizstagechoices">
                            <li class="visible" type="photocover" cover_label="Photocover" cover_details="<p><strong>Cover finish: <span>Matte or Gloss</span></strong></p><p>Print your image and text directly on the front and back cover.</p>" extras_strong="Presentation Box &nbsp;|&nbsp; Slip Case">
                                <img orig="img/photobookwizard/cover/ORIENTATION/hardcover.jpg" alt="photocover" />
                                <h4>PHOTOCOVER</h4>
                                <h5>Hard imagewrap cover</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="softcover"cover_label="Softcover" cover_details="<p><strong>Cover finish: <span>190gsm flexible card cover</span></strong></p><p>Print your image and text directly on the front and back cover.</p>" extras_strong="">
                                <img orig="img/photobookwizard/cover/ORIENTATION/softcover.jpg" alt="softcover"  />
                                <h4>SOFTCOVER</h4>
                                <h5>Soft, flexible cover</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="deboss" cover_label="Debossed Material Cover" cover_details='<p><strong>Smaller image debossed into the front cover</strong></p><p><strong>Cover finish: <span>Choose from Faux Leather or Buckram</span></strong></p><p><img src="img/photobookwizard/materials.png" alt="materials" /></p>' extras_strong="Presentation Box &nbsp;|&nbsp; Slip Case">
                                <img orig="img/photobookwizard/cover/ORIENTATION/debossed.jpg" alt="deboss" />
                                <h4>DEBOSSED COVER</h4>
                                <h5>Material cover, with small debossed image</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="material" cover_label="Premium Material Cover" cover_details="<p><strong>Cover finish: <span>Choose from Faux Leather or Buckram</span></strong></p><p><img src='img/photobookwizard/materials.png' alt='materials' /></p>" extras_strong="Presentation Box &nbsp;|&nbsp; Slip Case &nbsp;|&nbsp; Hot Text Stamping">
                                <img orig="img/photobookwizard/cover/ORIENTATION/material.jpg" alt="material" />
                                <h4>PREMIUM MATERIAL COVER</h4>
                                <h5>Faux leather or Buckram material </h5>
                                <p>from <strong> </strong></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="wizstage" stage="4">
                    <h3><span class="group"><a href="javascript:void(0)" class="back inactive">Back</a><span>STEP 4. THEME</span></span></h3>
                    <div class="wizstagechoicescontainer">
                        <ul class="wizstagechoices">
                            <li class="visible" type="blank">
                                <img src="img/photobookwizard/theme/blank.jpg" alt="blank" />
                                <h4>BLANK</h4>
                                <h5>A blank canvas on which to tell your story</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="contemporary">
                                <img src="img/photobookwizard/theme/contemporary.jpg" alt="contemporary" />
                                <h4>CONTEMPORARY</h4>
                                <h5>A great place to start for a clean, modern look</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="classic">
                                <img src="img/photobookwizard/theme/classic.jpg" alt="classic" />
                                <h4>CLASSIC</h4>
                                <h5>A collection of different layouts and designs</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="fullbleed">
                                <img src="img/photobookwizard/theme/full-bleed.jpg" alt="fullbleed" />
                                <h4>FULL BLEED</h4>
                                <h5>Single image to a page, full width and height</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="travel">
                                <img src="img/photobookwizard/theme/travel.jpg" alt="travel" />
                                <h4>TRAVEL</h4>
                                <h5>Travel inspired designs and backgrounds</h5>
                                <p>from <strong> </strong></p>
                            </li>
                            <li class="visible" type="wedding">
                                <img src="img/photobookwizard/theme/wedding.jpg" alt="wedding" />
                                <h4>WEDDING</h4>
                                <h5>Wedding inspired designs for your Special Day</h5>
                                <p>from <strong> </strong></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a name="selection"></a>
        </div>

        <div class="phodwizselection group">
            <div class="info">
                <h3>YOUR SELECTION</h3>
                <div class="infobits">
                    <div class="bit" id="selection_binding">
                        <h4>&#8226; <span>Layflat Photo Book</span> <a target="_blank" href="https://www.albumworks.com.au/photo-books#binding">Learn more</a></h4>
                        <p>
                            <strong>Min pages: <span id="selection_minpages">40 included</span></strong>
                            <strong>Max pages: <span id="selection_maxpages">152</span></strong>
                        </p>
                        <p>
                            <strong>Paper: <span id="paper">400gsm SD E-Photo Matte</span> <a target="_blank" href="https://www.albumworks.com.au/photo-books#paper">Learn more</a></strong>
                        </p>
                    </div>
                    <div class="bit" id="selection_size">
                        <h4>&#8226; <span id="selection_sizelabel">12 x 12" Square</span> <a target="_blank" href="https://www.albumworks.com.au/photo-books#sizing">Learn more</a></h4>
                        <p id="selection_sizealt">(30.5 x 30.5cm)</p>
                    </div>
                    <div class="bit" id="selection_cover">
                        <h4>&#8226; <span id="selection_coverlabel">Premium Material Cover</span> <a target="_blank" href="https://www.albumworks.com.au/photo-books#covers">Learn more</a></h4>
                        <div id="selection_coverdetails">
                            <p><strong>Cover finish: <span>Choose from Faux Leather or Buckram</span></strong></p>
                            <p><img src="img/photobookwizard/materials.png" alt="materials" /></p>
                        </div>
                    </div>
                    <div class="bit" id="selection_extras">
                        <h4>&#8226; <span>Optional Upgrades</span> <a target="_blank" href="https://www.albumworks.com.au/photo-books#accessories">Learn more</a></h4>
                        <p><strong>Presentation Box | Slip Case | Hot Text Stamping</strong></p>
                    </div>
                </div>
                <p class="l40"><a href="javascript:void(0)" onclick="window.location = 'photo-book-wizard';" class="bluecta">START AGAIN</a></p>
            </div>
            <a name="proceed"></a>
            <div class="preview">
                <p><img orig="img/photobookwizard/summary/blank.jpg" src="img/photobookwizard/summary/blank.jpg" /></p>
                <p><a href="javascript:void(0)" onclick="if($(this).hasClass('active')) { start_editor(__url); }" class="cta">CREATE NOW</a></p>
                <p><strong id="preview_type">Layflat Photo Book | 40 pages included</strong></p>
                <p><strong id="preview_size">12 x 12" Square</strong></p>
                <p><strong id="preview_price">$119.95</strong></p>
                <p><strong id="preview_shipping"> </strong></p>
                <? if(time() < mktime(0,0,0,7,12,2018)): ?>
                    <div id="preview_promo">
                        <div class="promobox">
                            <h4>SAVE 32% OFF YOUR ORDER</h4>
                            <div>AWEDITOR2018</div>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>

        <div class="nomobile">
            <h2>Or download our Advanced Editor to get the full range of products</h2>
            <form class="box group" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
                <a name="join"></a>
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=PHOTOBOOKWIZARD" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="PHOTOBOOKWIZARD" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>

                <!-- Actual fields -->
                <input type="text" name="first_name" class="firstname inputbox" placeholder="NAME">
                <input type="text" name="email" class="email inputbox" placeholder="EMAIL">
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET OUR FREE EDITOR</a>
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
                    <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
                </div>
            </form>
        </div>

        <div class="threecols group">
            <div>
                <img src="img/photobookwizard/01.jpg" alt="man and woman looking at computer smiling" />
                <h3>How much does a Photo Book cost?</h3>
                <p>Our Photo Books start from just $19.95. There is a book for every budget. Size, cover type and print definition all play a part in the cost of your Photo Book. Use our handy calculator to find the right book for you.</p>
                <a href="https://www.albumworks.com.au/calculator" class="cta">LEARN MORE</a>
            </div>
            <div>
                <img src="img/photobookwizard/02.jpg" alt="handing a photobook with deboss cover from generation to generation" />
                <h3>Dispatched within just 7 days</h3>
                <p>Our priority is to make all your products as fast as we can - but never at the expense of quality. In fact 95% off all items are manufactured and dispatched within 2-4 business days.</p>
                <a href="https://www.albumworks.com.au/shipping" class="cta">LEARN MORE</a>
            </div>
            <div>
                <img src="img/photobookwizard/03.jpg" alt="an array of photo books, some open and some closed, showing their covers and contents" />
                <h3>Need help?</h3>
                <p>Help is never far away! Our Melbourne based support team is there to support you. Call us on 1300 553 448 or email service@albumworks.com.au - we would love to help.</p>
                <a href="https://www.albumworks.com.au/contact" class="cta">LEARN MORE</a>
            </div>
        </div>

        <style type="text/css">
            html, body {
                overflow-x: hidden;
            }
        </style>
        <script src="js/hammer.min.js"></script>
        <script type="text/javascript">
            __binding = __size = __cover = __theme = __orientation = __url = 0;
            __can_click = true;

            // regen from spreadsheet
            __urls = []; __urls['classic|6x6|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NzMwMGY5YjA,.GbxrrEMGvOxo5ODeIr1gMEd4czvGtd6-LN7466Z9Z5D5DQPVotMeE26fZOjEXEIse-LmUAx5bEA,"; __urls['classic|8x6|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ZDcxYTIyYmI,.lVBi3eZhzWqPGHbZF1_mg0Wyc7FhUtV3cuc3ygW1FB--WV0ielWg1vm1J5RE0DfjWM6tKY-8QYI,"; __urls['classic|8x6|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.YjFmZTU1YmE,.yB9vJ_ZF9xgcq_dfvC7a5S3n8NjyV8hGMDvIuAbsHfGoDvVX5t-HynkhyKrBEY2CqFvYBuiGsnE,"; __urls['classic|8x8|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ZWNiZTEzMmQ,._IxsZhl3QEV4r2SFz48VZYKEwWH-8g1GX7kFkeKf5njen5_VJSRgiQ1zQsHUGhNKr_4IkMNPjTY,"; __urls['classic|8x8|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ZTc3NWZlZGQ,.mgJT5kC75HozRI02KJEkqSUDmY_t3AZVEG3EAFz8bsqc0EYQjLKCnvhXQ3YDd1lG8iwXjs9x614,"; __urls['classic|8x8|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.Yzc1Nzk4NDg,.ZuzLJaoH9a9NxHgr-bO88lcTsY8XGhppOhRi6db9xEpIelsDPU4qE8oQ8kqZcYjaXk1LM0cGYLE,"; __urls['classic|8x11|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.YjY5ZmYyMDY,.c_YowN4pqOJMoqSmyWPSoDH_OOR2AjrI3gnsD5QLWaZVhSb4cSg-RyO8LzbM0GClvNpgwMauiAE,"; __urls['classic|8x11|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.YTg2ZTUzMWE,.9Ypq1NnumtUfSyWlpCkHiTVaFDrRQMjbdEtUgijZQP7eBSIKQ3N8AkZIhbF5lkj3zm0I_jrdLBk,"; __urls['classic|8x11|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.MjU5ODJlOTQ,.3j2LVMyk923XJTMtsoX_ITyMcTnuDrJsLTD8wrQYoAbCk_ONlgvn4kAnUw2X_i8XWb3g0WrStb0,"; __urls['classic|11x85|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.NTE0N2JlYzY,.UJ_H8ZUwtytUXcLPLoKicC1oiZ5LjMfzBRE__0yvYmQyV0h-6MOy5Rs8RVbZJC9o9dl4ZpgouPOktQhJt1zbHw,,"; __urls['classic|11x85|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.ZWJiMzdhZjA,.EH42erXklzsmzpv80t50InXMNJL1BH8BjURosWLqVWYDKcqkbeIU0WpM990uk9v56Cm6Ii23bFLCUsrQhE1vbg,,"; __urls['classic|11x85|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.MjVjNGZiY2U,.Sr8o2fO3ZsaFLNrCQcrzOhSJ__s6yGmufZ7YUDl0bUb8-7H6NezuHLXbANQMWGHYqCWjbCfuHHfVSS2AtCooQg,,"; __urls['classic|12x12|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.NWU0MjcyYmE,.Go42Ci52os5Vy7SIks4egcyEiiCH9kHE5vJ-yvsZebTFysItrv1pNfcrAKi0Wfae5E4_CFv4AbNDJt5qfz6_qw,,"; __urls['classic|12x12|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.YmE5OTYyOGM,.vm9ylYDJVEhCve_oOtDbk7w9g9oxbvo0D5dPE4mCnf74irU6HSSKVH9j0jZxuzBZks__N6qYDbhPmS5XFTHwCg,,"; __urls['classic|12x12|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.ZjJlMDU2MTI,.IIEKObQUO4XUqWrNitCffHBrcU4WKNMIlsPPqhet-4_-1JRIAcly8_0-Q2qeGRlMHAjcvrXt1c4Sd8tP45yyqA,,"; __urls['classic|12x12|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.MzEwNjU0NzQ,.xOOUh6Kot9-fqPL-UHCY1ZFpjfmN0HE8eTNuBSR9Mg3tn-fiM3cFe6Ii5fKMob5RM3bVTHl7hgguxGtSfUkJvA,,"; __urls['classic|16x12|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.NjMxYjgxNDI,.dxVSv_G98XAbxQGwiyQwHSNe1zTrZloLdg65NOWvgTkZotZEu6RQ9MJGeHm8SLRENaRpMeh3KTO5ETaX7o3Mtg,,"; __urls['classic|16x12|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.YTMxMmNiNjM,.wEyJti_6ZudQODniE85c3Jno5NhVotSRmXdnmd75jSjG3Uxf2H1GKf4bNGMQgCZUL_wurZ4TE01VHXReRD4UCw,,"; __urls['layflat|8x6|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.YzJjNWUzOTg,.rUIK_Pg-KsZEXzXSva7iCzAkU3msUc8CpCyH5zy2LC9OSO3Y340nFH9KuoN78PCLNMMJWv5KA0k,"; __urls['layflat|8x8|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.N2MyODE0OTU,.KFQ0luQH_FLzowXfeC79_LQYwmXEMRK0QEK5mD88r4j6xDy8h3zhxWgIgieDG6B5p0Vv5mgwv-8,"; __urls['layflat|8x8|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ZDU2ODU4NGQ,.6joIfmsm2TNggLAAkaXw2666-S9ON1kKT4HKvyZp_1uVnd-InBG913r5Z_cjn-uppjRTysYTeyM,"; __urls['layflat|8x11|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.NTliMTgzMDQ,.L0azrlE1alUte2LH8ZaYELFNs0z7WwjdUIq3jZ4AzYrxxfkZMYUTK44cogBVNq_nX69JFpnGFcs,"; __urls['layflat|8x11|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.NmNhZDIyMDc,.ODhkWhY97cWHl8_1kEXMBWRS5qfwTcBIaW7IyLnzAGQqubjFYsJmdSa2ZL3irBj1I3-kGVuWUDQ,"; __urls['layflat|11x85|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ZGZhZmUzZTc,.-AaWy9_9IrO34WDS3XCt8G03hvvNYHDLIrbawqynTH4xRxs2xjNDEL1GPonAxgb6NxRdpJiBqHo,"; __urls['layflat|11x85|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ZmE1OWNmNjc,.u25DzmHwoPXBv7-cLtFFeZQ_F2Im9yi2WFb2uT_t5hIFURxBe0tPenZx512yT44KlZLwlp6YjvA,"; __urls['layflat|12x12|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.MzZjOWM3ODQ,.-Yiua3-0A9ipPd5zxxXJZ3QkcozySZ6xZW1yH4j7SNHFvNC5OwbR89gRK-_2don4pH7uZB-G4p0,"; __urls['layflat|12x12|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ZGEzOTJhNWI,.Ry8tCC_kZP-NxtyPAR06nA3C-fUol2ZvJwoLlrlfLjecr32h5HmzldZsgt8Jm7GcED4TLNfo9TM,"; __urls['layflat|12x12|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NzJhNDA2NmI,.kaUACGS4zCAkv2KuO7rk7ZX7F6SBzdOPoydhcY3oiFYnffsRAp_RdYF7RddNd8tGqQv4irzfs0k,"; __urls['layflat|16x12|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.MDJhNDE3NTc,.7kk3laKB4gsOzQ1TE1YnYxlhPRkJ4qdHxfzYy_F9jOaF11r_cUzTuLRf3rS5z0uLadwTzl4ry5hNSFhC2JCkpA,,"; __urls['layflat|16x12|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ZjNkYWIyMzc,.kn0boYHaQF8yX7iaVN0sHl_s5L3r_f6pwEsX2ahKlMH8gPUvukjejIoTGynfPEP09Igeh_bVEz8,"; __urls['board|6x6|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.MjYzOWZhYWM,.AHlzyKIVbGN-N8zI8TNJVrWdypwCpgwfzpSebnqDwiJu10ExbNPDPp3Vy68Tlv8nH2qyqSQ2fT4,"; __urls['board|8x8|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ZjE5YWYwMzU,.v0CMSUQDZPhQtmPPgCzAM3GhUniADR_yk0RC2dEtdq9XagwCBX6sMfUEx_6fs0MNkUL19tP88kM,"; __urls['board|8x8|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ODQxOWYxN2Y,.6VOfELSyj8EoY3HftKXP9glcurDcn0JLulTPCEASSvz7fAAjSFTh1tFcz_fxSeMn2V9chsAnrx0,"; __urls['board|8x8|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.NDg4MTc2NzA,.rkNPx6Tpy5ga49mMA7lML33hPi10QjKVaYkfSaBStdgcUtSn_9SvTnx4V4JOVujlClm6JIqgi9s,"; __urls['board|8x11|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.MGFmYWEzYTE,.HMLH07UfHuKRjVJO6eR1yHNyDaq8uBtqekjS_AaUH2Aaasx6Rph3ozrKlSUO_ERrJYt2z3p-sfA,"; __urls['board|8x11|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.MjRiMjc3MmQ,.PKBfU_nGOQRIB5JO8x_Q5fIA1hu8022yvV_qrFCoBi75GxkpWxyu91gKqGig9wE5PwQDPs3iUwY,"; __urls['board|8x11|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.NzQ3NDI5ZjM,.6pEqkfMXd_6fhW4eWFGUD9aB8EHKB3hZw4xxTHFsAAYvOK0JbLaHB_IRusX8HrQFRMlVvifa8Eo,"; __urls['board|11x85|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ODY1YTljZmE,.B7k1J3JX56oIC2uR5uHxWoW4dQxCUx1Way0NSceJMrbO5paGrM4FSXubD6GLnYaSsDUCMX3xQmE,"; __urls['board|11x85|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.NGZjODZlNTQ,.AYlGVQpTD88CD3o6YFmzt0ymZMSv0XiKSFv0wE2zXtZgg0zRivvk0_bjarMWZXKRIhavpZnVxek,"; __urls['board|11x85|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.OWU5MWM1ZWI,.FlIgKlh_R80b5wDe5YYx-v7XChGp93geq7HeiBb7JUj3fnhMHigJbLge6yF6_ajwuUoeLSf2KPQ,"; __urls['board|12x12|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ZTkxNzY4ZjU,.uLO_hpgIdChzKWgg2CnMR0511LzWpPGdU5lRtB-X5CXBaUin6nNFGyEpx4C12IMmt-_CZmBe8cc,"; __urls['board|12x12|deboss|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.NmU1NjA1MGY,.muj5aj1TTfYEqrLR3VxowJr7S908ys4EDSDoDE_obc9coBtEMNilKZehkhIH8ZPfmwiEDZscGJk,"; __urls['board|12x12|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.NDhhMDA1NTQ,.wH1exVoH3el4lCUvxYBFOjoNQlCmUye9exJzR-2ovb0d-yJ3oNzDZdG6tGULbF7sMWNVjyhyLh4,"; __urls['board|16x12|material|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.YjY1ZjExYTQ,.Cnwgg44baqd9bWpjllxntgMLzPH7HXpJrLmmgneIlwJir9QnUi1Wkij_tNwGUsqOWeVhnqikyow,"; __urls['classic|6x6|softcover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.YTA0MGQ2ZDY,.LUAGGKBq-5PxnuYEkff_7xf55v-oxWEzQc3tTU540GXiIzwXlORULHrzaaEK4IBEOVarcJDwZuw,"; __urls['classic|8x6|softcover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=48.YmNkODY2NjM,.jIovTPM54EDpdOS8Ls8A0Zq_2MJLfmBfA_3WWRroBWyaVpUrSHiD_oAgooJ9JOI5"; __urls['classic|8x6|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=48.ZjQ4MjUzZTg,.cB2PTJwDxuTbCj8LkAgMZFJrwCLIP_HSFIe2L9l2L5jUAXXOQAFBQD3L9ZVhwKaU"; __urls['classic|8x8|softcover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=48.MzQ4MGMxOTU,.XXtM5w5OvYjHtBw6eOC_e1XexC0zA2cFTiF4qokfDnWXhirAYc7SDCK_mOPP2AoK"; __urls['classic|8x8|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=48.NWIwMzcwZmQ,.Wague9-kS84HQ998TIyQu4ttLuaSgD65wt3y4dXf98EPlNyMaXxSDk-KICfwmsK-"; __urls['classic|8x8|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=48.NzFiZmExZmU,.Fetn5hF75X8IUxguv3mrR71v0xE_Yp-d79iKKbqiFxJefXA7jYOgSGVAZlRRxbQp"; __urls['classic|8x11|softcover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.NTJhZjg4ZDA,.vk9GXEQYTga7H0_exZAUtkiyOViuOrCnOL7Msa7gUdGGQv2q1Qw44I8MLDfuZuCvILO49Ow2Kbw,"; __urls['classic|8x11|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.OTU0MjVjMmU,.Z-aswwsei6INT_iqU1aDuZUICCknIC3KqzufbAeREpsg8GUmTRmqev0ZIf7tfkeTaDbAIJ6l8YI,"; __urls['classic|8x11|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.MjY5YTdmNTI,.bzpNF_VJ6T1c7kQ-zyDF4XTcnFGZAbuNxMEOF9AM0RDSmWMmSojaQyWWUpukkWiwYB6XFjopWII,"; __urls['classic|11x85|softcover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.ZDUzMmJjN2M,.BUra92oI8OKdZZ-7EbKYB3m1YMlIOXJLfrfpBI0iQJxFDG50irD4xOgely3c10oElpvnVlWuZSg,"; __urls['classic|11x85|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.NGE1NDA0ODg,.-7_BHxZb_SAt3oPBI3YEcrt8KqUbdiQ_H2mPaSReR5L4QpnWsp_JiFvXAMRYqiIEULYtQ1bTTbM,"; __urls['classic|11x85|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.MDUyMGU0OWE,.mE8PBw6vItXbRn5uheZlw4f757h7M26VcN3yR3EqqOusvIFxj-YnLkqq_Mr1N80HgahKo-zkZ-I,"; __urls['classic|12x12|softcover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.NGVmOTQ0MDQ,.NPc3m8Ck74-Dj8_QYO5vu1BwTOcyMhJbxur4F-FWMeWubXsQ5scld1paOdLU_1ln6qKV6ojxn3M,"; __urls['classic|12x12|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.M2Y0ZDIxODQ,.oB65EuZshH-pGH0IFWxdif8AaCjNlloEGB6bqbmms2euULgVPvnSKpucmz6UP8tYKRjs4qzG-ag,"; __urls['classic|12x12|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.M2Q0ODIyNGY,.bXlcwcp6MEryWK5XqSyuJ9NaeVajuo6VHncBDmXXsr9d6xGGopqtV0V_OiFe0R1gWEckWNAgBMY,"; __urls['classic|12x12|material|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.NGNhZDA4NGI,.u1mrVW6kKRMmHGe0Jz_CRll9EkIi0Vp-sN1WQfSjenxmECc90TvpAgg3xWHLnvrul0lZieGFe7M,"; __urls['classic|16x12|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.ZjdiNTgwYjE,.KxkYuZDz4hTew2046IBsxmlo70ZgVycLmBy1_1MBIBaqYhCiDyIVa2-XUHFoKq3mMVXT2exOwF8,"; __urls['classic|16x12|material|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.NDIzYTI5MzA,.L2o8cf0GHzlkoZcSvhHN1AxxJ5U9JcRxeIWNYz2VpyBY0MVxO6RUDVBxb6sixsOABVOfuWjh1bE,"; __urls['layflat|8x6|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.MjM2NGI2YWE,.sAyMTnK_lClahd3lGKfb8jF4UZsPgZNUEkNh6uD0xKZMxdE99HgDnurUaX25pFI7wUbFfdPXiCE,"; __urls['layflat|8x8|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.ZDgxYTU4MGI,.XN8rvUu8k1400mYu0kJVNkp4_ieGj5T1aVeXDay03G7xic7RSpHZ9cuLlcUkcPU2F_jIky3LZvM,"; __urls['layflat|8x8|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.YjkwZTRhMTY,.aRR7ONTkLiEepo_xCytuRfWQTTlePorJNQJMW0n7cVk5JYK_f0TUcZz3NDLNTPPEW-D2I2paZlA,"; __urls['layflat|8x11|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.ZTFlMWQ5MzU,.Cg65VB1MKZsFtwHqsrvhXdQYWdI29EVQsOqc_LveYn1NttgHcTi1nUf3nF02rDq2tDAO7QfiSj0,"; __urls['layflat|8x11|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.NWZlMjYzY2E,.RDlKMJmui5LrslSukOsXhfH8-j6SRVSDiuPGBlMVcILUZZzShuGBLA_k8vnfjgplGNcH2spaAvM,"; __urls['layflat|11x85|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ODUyMDNiYjg,.hxmBQi0p79aGeHBAamqOGX9Kx9eCi5QLj7DI-QAJD1ZbRhavsq7nu9laJQaNRe4DqvJjGiwLjZo,"; __urls['layflat|11x85|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.MTU4ZDQxY2U,.DjEgSjXsnENN7bxYWLxDesqWQb9bhQ6La27HMeZznt_EWUPsaZ2Qt2El8QNSYPLpRUMUtB-Q87A,"; __urls['layflat|12x12|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.YTY2ZGIxOTY,.JVs-Le2ecO6yRKz5v3dThMSBUd-5FzWaNxIFgh-i9hBFdD58uy-fZYmd448oj9Ouf_wvktJWvn6otWVGFbs-oA,,"; __urls['layflat|12x12|deboss|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.ZDk2Y2U1Mzc,.rhxgpIJMRS9HoZUoiWcVGjRtokgB_xDz2tPTmfiKY-BiPWYV7GMRS58I8VFBmKiBezQ-pfSju-VcTtlgX-dzdQ,,"; __urls['layflat|12x12|material|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.MzE4ZjA0NDM,.6NMPIlz3L0WqEaoNPKbmPnDztNszoSp4PPlROHw0pVMkm6-iZWPvUneAz3svU2RTNfc0yXxEOmNdZX-bW1SoLg,,"; __urls['layflat|16x12|photocover|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.YmIyNTQyYTM,.WBoI_rjZwCGsZrtYCOjavr93OlB2wW1yZ2YLwenqMr0rd_YHIz4gxliV6ZyeK0QShP7NiTz4WZfJhhnUqk3h5Q,,"; __urls['layflat|16x12|material|blank']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.NDY1NmNjMzY,.drXbbj6km0g9SIHUtt8xAmmMM5B6UtR_uDDBQcPAq4fsjETsDldnuk_RkxRzzXF1RBI2n9rwGhRqa6HnO22mOw,,"; __urls['classic|6x6|softcover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.YmU3Y2FmMmY,.pEdKSclJVXi5XrKOjkc-VMgGDn8id0Z1V-3ynddW9ESa09Ykhjyq0-4EFxRHU8URo-8mcQ-hVPw,"; __urls['classic|8x6|softcover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.ZWYyODU0ZDU,.a1mJCO2iKzF1W8BwSAyvXptoqQZo10IXllWV3vw2S5UOwa6aeQ3dgEB_sPRstdGkt4Xzj60CAiI,"; __urls['classic|8x6|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.OWVhZjZiNTk,.6rFAV3MSIb5HFtuy6Jdf9vK_SnN4pFXblAQWunbxP_hOHKbOvk2Xgk9OGYFrAof7l69E5jk75Eo,"; __urls['classic|8x8|softcover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.NTM4ZGI0OTU,.GJkxif7MwGvhmEz1JJCwrgSGLKgZQR_nGDCK0wfqy92NqSB0E0bMBIwRThKvTcLe-xq7BAKqMOE,"; __urls['classic|8x8|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.YmI2NWE1YmU,.QOFguCHXQQc6GjoWX3gW4sPhfhO-EL7u0FVuhWppHRnPQ4_Z2VqUIcKzOWYhLHvtsv2RAWSN40Y,"; __urls['classic|8x8|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.MGE2N2ZlODQ,.bNCw0rxrNfCpE_ktudsiplF20lvM5siIqEs_y1wHFTwN4DEa5jU5p-wH6oDv-8JEFutlmU54Fg0,"; __urls['classic|8x11|softcover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.MDJiMTQ3ZjM,.qLG9osSC5JvrOWC-8TmQvji12ckQF5OVQz6w6pc-NHXAFK3LS1XKhVPnwaJPvrI4bWOTlnBFJw4,"; __urls['classic|8x11|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.YzI4N2E4NDg,.1mlHPlBOAZmmhmp-HC9REDiuUFPH8YRDUeWp4c58uBQNGhbkTwkbiwoEZXFDAnYOyxeWJ-LGbwA,"; __urls['classic|8x11|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.NmY4ZWU0YmY,.Spywtbv9YT-bex0R0gjsvo7U5ysUDUoTJjOvGam7qftxM-k7Y_GQen6cRzKVgyQhy7z9537ssQ8,"; __urls['classic|11x85|softcover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.MjMzM2Y4NDM,.Rfqk2n9L-nZCndXriO3fL3z2rh90brApnewW1wNY0Nh4D8J_o45t7vRr7OtorT2EC2-sA4zkmtg,"; __urls['classic|11x85|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.NWNhMGQ5YTI,.1wWYMAOk_bgWjxrgdvZy_FCa5PBGxQZdxCJWVQq3VNiHw_Gw_CttprpEj2MQVShWhxNFxv9NLwc,"; __urls['classic|11x85|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.ZDVkYzQzMDU,.IWGHtxChUV23TZI4jDvDcC6Tj72kHzMXbr33OTiB1FPzJQxwCxCkFze3kE4ATYCBwpLZhQdvvsI,"; __urls['classic|12x12|softcover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.YWVmNTAxYjk,.NpfBffXscBgFXLLJmDJaaXDNW-NlggSE1ohAsF7uIDBMS-QMwyPkmZOxFzIZnhefwjVq5CNb5cI,"; __urls['classic|12x12|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.YzljN2M4M2U,.lcUUQSOe-dPM-fG5gLy-rGHJre3cQly8TYDeIn_RfYLwfhzkOv1qi_gSBdA9muc2wwr7nUrbMjA,"; __urls['classic|12x12|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.N2Y1YzI3ODc,.sRm_nVAyTFSkvsowATexAjQWLogmhgzKR9y_jcsPVjm7QUkS2N3lx6pVCvu8QR19U_cZH0sWMzc,"; __urls['classic|12x12|material|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ZThkNmJhM2M,.XJBn4nPeC_SQJVfNxoY6LYncj3f9co4HGcSGWi6My6Du5foqeDNGLixlAyFy_qErY-mdXEg7rCA,"; __urls['classic|16x12|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.YTNjYmI0YmY,.EPV8UqJAuFz3vKPD4YYgo3ShXLQdjPkanGklzxl9FYjJltZhWZi_aXow3unzx9Ch4kxxZzKOx6g,"; __urls['classic|16x12|material|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.YTk5ZDhiMGQ,.xkb5ecc5aRgBiV83bOSWgNbAWK607fkPV5jCFvt615MqFayPCxYThwHjg4kArIMYx-0tLVAalps,"; __urls['layflat|8x6|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ZDdlNDY5NjU,.4zGuLnQb-DxYm_6GHEFAKgsLvJdmjMRDRSovVVV59qUOKaC6GBD5t3_HMhyGKpgimZFBRYDZBdw,"; __urls['layflat|8x8|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NjQ3OTMxZWE,.0vDd-bqQTOf6ItdbgqxnGU5hAGeBdrwpaQWgvaa6v62i2m7y9V8dPtLM-WBayZpZiqKpOKxohWc,"; __urls['layflat|8x8|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ZDJhMDI0MjA,.3TLi6TGS4ccLqYcmYw9_Q81bbDCDnEc4HFiPP0hJzhXag0AjgGXWbC6a7Dyt8dlbxUT9VCATfsg,"; __urls['layflat|8x11|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.Y2NiNDJjYzA,.p1Z3rCfhoUaH3J1bs0eSOpCoDsbrFGU4tffo4RJeZKKdkipqgJnWHLadmipFW7uX06spgPLl2koE0G5Di-FgNA,,"; __urls['layflat|8x11|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.Y2Y2ZDUxYWI,.hNnjyJNMjZWE-EDq7IKia9rzUBacqA7zc7xTeztoYJY2bAsjHWyZJ5SNODS8CL3a1N0Ka51j_uYBv0k1_mzNqQ,,"; __urls['layflat|11x85|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.MTU0MGU4ZjY,.p1t9zPDSVnb7OkuhJH5QNh5p8cuLcoPhn0WtrW673zIS8w3JqgUe3iBKAiIJDYJ4cn7iAP2tvDPNNx9pHNkqtQ,,"; __urls['layflat|11x85|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.NjllNWIzMWM,.H1R9ciETnwz0sirW7F0vtOrhlycAj3C6266eb1L8P-R-qfV4k1nJrraE7YnBiZqA8psrbW1pSoxbd3TlXNr3ig,,"; __urls['layflat|12x12|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.NGVhZDEwNmQ,.bUGkzHSl-BnzegKwvPJdS-7amtvcL_Ark4D2JZ4AyTw6jmjTpWiJyCDYCb4UpOTPi75T-eaALCvh2DzYdtiKAg,,"; __urls['layflat|12x12|deboss|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.MGNiYzk0ZjM,.p9rIkVndZfseUewg_nsrtOCiu4z72_SGMGWzTjnEgDBQ34N5zTFAigMFBl-q21mG1fHCp5V_ADkdAirG92dTJQ,,"; __urls['layflat|12x12|material|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.ZTI3ZjIwZDU,.3hHiYnN3KdECBB9-L1IHMDRgkqVrc2BCtodQ7ijb4MxkNS5obN3GNU49UaJ5GOrK4o3WbMtdxE6OJTbkEFlZnA,,"; __urls['layflat|16x12|photocover|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=60.ZjMzMjFhMWY,.j-jAL8wfuftfptlXBvdXafari_U0XK2IYyutlXWvRGUyJOX_X94lweKOIn5CbyTLtZtayxKUbo9aIIF-gkicbQ,,"; __urls['layflat|16x12|material|classic']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.YzA4ZDRjZjA,.c6FNJd29j3YK-VtQVqgqGu02HP5c9-7D44OPFIAKMWkB_WXtyptMvM_lXOuuU7JSlV1o-s8u4uib_DKplDnwrw,,"; __urls['classic|6x6|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ZWM1ZjFmODY,.LyQwj88BxuHY6ABMm3CxNeAGrBTkkRbDmT5A3htDz7hpI8QtxEuACNlCU53Sj7tNEi6rpV9X5s4,"; __urls['classic|8x6|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.NGEzY2ExN2I,.8sMLR45ruwBQiSEuZQVDzs04YUQcpYjOMw62VjrRhz3R85nUe5XTOdbufUwNq8UMOXXeVj37W2o,"; __urls['classic|8x6|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.ZmRlYjhjNWI,.ihn7rqcodSYYUO-GvhHc521QKmvz6ZpZ-EVMxuAyjXPUDNqsorGkRzHx6raGZ4wKUToE0mpjD8s,"; __urls['classic|8x8|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.MzhjMTY5MmE,.fPkiKlmcJA0OoHbo18Iz5MegqNL1OEhtHQOQUlyc0g2AFaPXf6HdKSOsDLj7TnTs9zj-exVomU4,"; __urls['classic|8x8|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.ZjE3ZWJkM2Q,.Keq63h8mwTUvcUYfAsjiRUNuSIOpSWbpbdlx8VCC0RqCbqanZMJfUj6ARhFflHjnbbWmUxS7h48,"; __urls['classic|8x8|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.ZGUyM2Y4M2I,.imc9azjutZjkoRvFEDOcDmSqjAKKnQSAqCOhlhRLnJ-I25N-m-V6eS8MG9ruw7FU2IRkbdM-7Q4,"; __urls['classic|8x11|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.OWQwNmVjOTA,.s73XTa2c5XONISUbIFeL__X2RBjx9h_K62TYZdtJZ_zfaZFcnouGKDsvWWZE9N1BhnYs-4MaVyk,"; __urls['classic|8x11|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.NDM0NTNiZWU,.k6MavCyXC_zo_OJa69G7tfJZGHf2g2kQ_kfUQgtnBmFlPJ5Sxto6yIvs7T7Qo3-vxVi_73lG7DI,"; __urls['classic|8x11|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.M2NjYTQ1YzU,.gI6p-zzCX0q6biWZ8ogU6Efga6Sla7B4zjO8v_APGaztacjBEhpnAeOyy-0hcrRja8x9w2yAOIA,"; __urls['classic|11x85|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.ZDdiNzcxNmE,.GQ6PAgkltmrF9MrJydrVMsJRg5LC6JoDncUqL27wfo_JUeWVlPIgZKccFhDtakHO8OOf1YA0teA,"; __urls['classic|11x85|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.MTYzYzZhNjA,.frlbc7-uC_dmy-7aCJtCCeYOHWlYdSTfA9B0OJFw5nh7o35U5XJmeIzJnJF-xTqdOOi3StXdJXY,"; __urls['classic|11x85|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.Mzg5ZDBhMjM,.lbvzvaw11ooA67qJoDGDav8NQcpyL7LQsrV-iwosaABCTIPWHyz6Z3CsbglAnGH2j_yLskshyZA,"; __urls['classic|12x12|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.YmRlNGRhOWY,.kOnMomHaea-9NAFH4Rr4oBkN-LxLInvtc_-jTaTfMOef-KhFS3F1nbgGJHFJ0mUCMhDF912K6ow,"; __urls['classic|12x12|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.NDY4ZWU2NDE,.67nQmb4QtaVq4EPYhlpExxfmdEnkjTyR2U7IsuUWKHvHjzakU4V60beqykjlDMht-6GuFjQdrBM,"; __urls['classic|12x12|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.MmQ5YmIzMmU,.H9fcWjm39EIj1zqIoLT4Ch9TNEOmMLLiRLrCRynM-A160oKcJA-2uDiVIoUys8_puF-wy47kRpY,"; __urls['classic|12x12|material|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.NGYyYzhmMzA,.UJAh03qvF7e56pN41XKQT7NZNX-iJFcBsOrxPO1GB6gGpTWFePwd5X_htRDS8BIzbY8inm5q3MTGnrn1Vj4hgQ,,"; __urls['classic|16x12|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NTc3YjE3M2M,.gpo0gZfFLPEdDJx3IF62dWJw22jzl0A0rRSPiZKswoCiyJFWEakGnhhl3FxYyeQ5ebK_3-lO7Cs,"; __urls['classic|16x12|material|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.N2MzYWQ1MmU,.S7iqNAmSvz26AH6fCR4u8R1xsCVotH5QRwb7CnKmpQj0pTt5vpBT4GSrQWmuM8elHNUw4_ChUR_4lkyyH7QqMw,,"; __urls['layflat|8x6|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.YTY1NDZjOWY,.vi98Dhar5tJI1SpHgqA-MNRkWInc_fhSrvXyVtPb1k5Xh5pqsIsRViolLPXYZwrqoxSkHoohC7xNU6g2CjSbXQ,,"; __urls['layflat|8x8|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.YjA3ZWRjNDY,.WJj8v5BrBGh0O26MhLo9WS_b2MOF_gL6JWINQfhqgl5dI9LiyrYVar1nEycS7OqW0vnmc63RMJRw8HDpEzlh5g,,"; __urls['layflat|8x8|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.ODNiYTljZjE,.oDA9RgwBA0ZFhf4-xckTxN2cIZSnceIibwDGYhRhPEhkn71sryyZAuu3P7zp2Ziyr8OTFgqZH9HyXgOv7pRk9w,,"; __urls['layflat|8x11|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.M2Y5ZGJjOGM,.Uad_0XLZrvc4xsNYxH5hdN89tMWFGopuV8vbboSStTZCRVnY4RDR0HIufkasz7OVGpw4Xdsk4F6tUHJoTZWrKA,,"; __urls['layflat|8x11|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.NjgwZWJiN2M,.iHzgGaISe-sgYX-PzsUen7DTNWvTZvWGutmJbZYkDUrOzbWM-fEBofc443A3odizFtOXFZ7LFJpyyGkz03fJIA,,"; __urls['layflat|11x85|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=60.YTJiZjkyMzc,.eJhB_LQ9gQU4XzqCrM_3LHAvWNTbbt8Wc2vY1pCT_zPqlhAFZ19aoSiQaYKXZcedQm3Ta3T08sMynVKIPqt_pQ,,"; __urls['layflat|11x85|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=60.ZDM1ZGI4ZGM,.6w65DXzr1bI5jPgVKhGE2QaMdLglpblmNM0_kzbhK_KiVMKkxkLpUuS8v1tDStXcYxe8TIyT2FYxeuE5VbuAMg,,"; __urls['layflat|12x12|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.Zjk4OGEwOWU,.OXgwqDuQNi8yrTZrEk8aRXc_tjGmeeI-EDQ4h64qWR18082u5ME0teNtFxzdb4CBugyNPOYjlhs-7K77t8xUBw,,"; __urls['layflat|12x12|deboss|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.ODgzNmU2NDU,.yzfofAhGqiP94KQHZudhMfFOgc2Y_RiElvghtYp-1Rdlq40O1BDCrR3CVc8geoBbRY6GLDzYrYT3yEdYH1Xgvw,,"; __urls['layflat|12x12|material|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.ZjVjNjhmY2Y,.aCadtQxPnKqRVMYhwB2sbyq7dvSRwWB8o0lfbeXNbgv4GA_DpLpC7ag9zbtmqX2ZOC4j1ULfGyB6YLggtkMveA,,"; __urls['layflat|16x12|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=62.ZjY1NDU4NDU,.qtZqs9nuglCJ20tXWDtjBdGmVbMMRf7CMa1iZqy1TjtuSbqdrsMmlot8nKSfT6ptT4tEJUSI9fUu_it6_RMTqA,,"; __urls['layflat|16x12|material|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.OTk4YzgzMDU,.fc6CLSvG-wcA-v0M30JF4U2tE4ETyx7LqQ4J8g3GapUmgnaeJykDE_NABd8As2G8pLfh4c1DB0RNCD6CG4z1dw,,"; __urls['classic|6x6|softcover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.YmY3NDY0MWM,.lksMXKZAf6_G4ZT2iK9ozAPWax205b90oMfXmQEZgk6VsxgY2v8B2MeaKa9iwH0kYpIS6zWdlX0,"; __urls['classic|8x6|softcover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.ZThjODFhM2Y,.27m8iJQCAp28klZ8cWdYDAiHP5AatsEcTKi9iVVCcA-3VDJuoHNDIsYyeFfgzkKWBv-CYEHib68,"; __urls['classic|8x6|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.ZTE4Y2NlMWE,.1HtC6pGP71fctGRTc20GcS_967MTFd8R1utnshmMQaGo7LxfALJF7hXgRcD8ZtfbsHCHsPPpynY,"; __urls['classic|8x8|softcover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.ZWJjZWQzNDA,.PpWOfu8jQO1GsXsVu4llxrnYpzJu7H52j_98dgKln2eDSmw9dujUU2Q6AMm-nXMkqOey4He3ghI,"; __urls['classic|8x8|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.ZDM3MGQ3NjY,.XBSgmZSjDjyXpYcsHtt_jFjPNzqpPjKkJls5ILRMe-HLpMWD2L25kbkYYQcVIaV7Cvy5HqVkdgU,"; __urls['classic|8x8|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=49.ZGRhNjFhOTQ,.DnKN--Av_51kw7hbcPCPd6zItfiOX6_jB4qVkTOjjHeHn2BnZoWOybCaqV6Hxs4chjs9GlXFXso,"; __urls['classic|8x11|softcover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.MDk2YjhkODA,.RmZeUQUReMqHjRqtuHrnYWIeuOWQ6Ge5UgMnfOgeIR9pUnJGR6qi--RP1xDNL9S9lp19I0T_Pm4,"; __urls['classic|8x11|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.OWQ5OGNmMWY,.kA_vhnn87NjSqYR6UH-Lj4s944ylnXYcVVK_YCQD4l5TdV5iSyTaU8DMHGm07VkZr2sPbPZjt5E,"; __urls['classic|8x11|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.YWE4ZWVmZmI,.6AcAntv0otRhKlmkU-9Vk0qmuCLnbRd0ulZeLZ8ZV5oxT5DCuiqJobxe5GbnhJAllzmCIeC8Qgw,"; __urls['classic|11x85|softcover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.ZDM0NmExMzI,.g8PYwrunTGB4bdDrk7X2IPfqUff_KtipXqOb07jYsqtmNwIJdiHcWiJ_da2PdZLqfWx2gYp1YFY,"; __urls['classic|11x85|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.NGQyMjkzYzQ,.Fz_cqETEhO0Lc2gaFhTLF_iNauXesrUqeVgmhf0AQU4lB5J_o_afOTrgpoCDKiU4JNw-twQIrzQ,"; __urls['classic|11x85|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.ZmE2NmIzOTc,.kJbrwpcfXC998ET0Nga5aJagG8iKt8jGm0pvm1-av80V6TM-OJxWsuSori-BzRM5UG47q8NjEWo,"; __urls['classic|12x12|softcover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.MmU1YWY2MWI,.lopcNIJSPUdPSmYzt_hZVSXHDS7aUGnLnsDCplRWR7xVdRzKnT_R6QNra2Oh7fsdGqBOopMoNBg,"; __urls['classic|12x12|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.NTY0Y2E5YzQ,.F93jcvsRBq3OoGUOHfOoJP3ZczaZyKBD6kFGKge5H8m9JVUMXBiEc6fPVT8k7guuKzmwGY-CXR8,"; __urls['classic|12x12|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.YWM0NjU3NWY,.Hp3il8dfvSBzpWwcLtmr4M57r6JXN7QGHe-r_NX_MPZH9HkosojNLCZo5JKLJW2xmxma_a3tHls,"; __urls['classic|12x12|material|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.MzIwMzQwNDk,.1JoLCkrV01q2fm_5vvVHfOBcgrhZnmLHF3_11vzGx7UvrX5NkJjh5iROelX0wMbU5ULPu3UUHsQ,"; __urls['classic|16x12|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ZWJlYmZlZWU,.5LVmwjQZgNuT1lsxQ-DvOUCy0Afw5njUG8joUzL_c9xITcHO2AIbFuczbeY3HxH_wPc79ZljoMM,"; __urls['classic|16x12|material|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.MDU2NDcxYjg,.w4O2qlBovUlnR6HoAp39rJTTf-pCBc5_UgoQv5CYn5Lm7Ro1GqVfX13qb4M1PY6rO4WMoDcnsyo,"; __urls['layflat|8x6|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.MGNiOTdjYjM,.TZc-rf8J4RYTCH4jTBfCc1fnUcyxoNliRpS82iVFLE-YTefoeJbDOUeUNiNHlz5j8VQlr1m4LoU,"; __urls['layflat|8x8|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.MjcxNDY1NTM,.Nc5aK3_JfZ1_Lag9NzAhJp4GWGHpaKQ-10Tmu9xpldBe2zmtAToJyvKY95CSccP8DwwFgLPqrtc,"; __urls['layflat|8x8|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=55.M2UwZWIzNWQ,.6Hoz4e6l42CS5hhpHzh58EvjbHcgmVj-p7xFe5bOwpxlowFqv-Wr3y0qXPUz9PmOajPoznFAg5s,"; __urls['layflat|8x11|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.N2NlYmJkYTc,.rDNQ1Zm9HppoNxceD3WtvPfn80k-DyVzIJQE_LX_ohzDrzYAjQJ9sIryxu52j--fXWABTOh5msc,"; __urls['layflat|8x11|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ZjA3YmQwNDM,.y0SkUpwe7Wkv_MtKZPeXubBZPF3gqiMj1oYgUQ-f2FX80Vux2jCHrYZaVbgt0IdDINwZoDroJFg,"; __urls['layflat|11x85|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.ZTMyNjUwYWI,.13H1UvK8s5k-BVgJuxQzmkoa7VNRIIdYFsb5ytU87b2P_q3CqjHa7gpA-b8acX3TsLwjNRD6liHAkYOPy46vPQ,,"; __urls['layflat|11x85|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.YjhjM2MyM2U,.maDYsrXzVg1ByvymiH6RLtsZZMJmHCemFoCZNLnqlXzW1YDf1UANWqZpQFm1NyhKzw5vWeJY86rMoCMXI7ME_Q,,"; __urls['layflat|12x12|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.MDQ3NGExZmU,.yZiNLCas7L21wo3Do812rZYGP6uf9eMzgKLhKxwlHTi54bysAudjviBwzFmD6ZqQBw4UU6CU8lgCoewaj5Z8lA,,"; __urls['layflat|12x12|deboss|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.Mzk1YzMwNDY,.924nNhPWNbDuXsjYJBh45PiOoHfiAvWwfUxyhfS4khKEhaRD-dm3UoRGQUZeSMJr67nArvi-mP6UD2mEeGR-fQ,,"; __urls['layflat|12x12|material|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.YmQ5N2VkMGU,.SqrLWQ29GxXtSWxxp7EuFXCC2aFziXeM8AKsbkPVpsTnEXQcpGnkjkZWUc6HrByKba07JITp9IgUAh7-dt4BeA,,"; __urls['layflat|16x12|photocover|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.YjA4NTIwNmI,.3FLjTFecKL9TVnt3hxfb6GKFY7b9jr4Ipu6p6eRkgPvAoqY52qTXYuKILZlJVN4xaVBh8mP1861dKYvoUiHEAQ,,"; __urls['layflat|16x12|material|travel']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.N2JiOGEzYjQ,.WdSw3_FD6uXB0g8JZtMH9vq_nJcq83WSqHbWJWdF-aeXUBFLKsnvLdGuP2vgzTDQ1X3vofLi2UXARw54CVaROA,,"; __urls['classic|6x6|softcover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.NmFhMjA4MmE,.VXCoqqlBXxjVn6k3ADfUY038hnO_QhAJGoLTo9sYj-QVKGumUbLGw0V8mEUFA5b2u-NJlwUQWxQ,"; __urls['classic|8x6|softcover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.NDU5OWU4MDQ,.LheGzmN0rQDPphG-OxYtLmX3TcdsNRG93KoXuLanflm0Rc6kCaxmGavNb8dhozcIz2p0oosi3bo,"; __urls['classic|8x6|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.NzU3NGM4OTI,.JJUVy3rTYJFPVgxNXT01YWuHwqzNjBgmystL6oehCDs8JMEeWCOmqM9-IJO5srVoxeEfodkWgu8,"; __urls['classic|8x8|softcover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.ZTI5OTY5NTM,.Mkb9QaZZTidAJ7Aneuzz1Z_Mfwj-qPTigizoYGPeIKN7KmW2ESDw7UpObgXSjki1-kIWoyspH_k,"; __urls['classic|8x8|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.ZWE2ZTJmMTQ,.qx8D5qEfoyD4A1tZSaErhEvxbzHs2hSjG8v98UCeGtPEAWGpqF8WfTuoSgsx6DqKDd4jGNk5Z18,"; __urls['classic|8x8|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=50.ZGUxOGNjYmI,.YGv_eLMuVMN4YfJ_9Wp6tOwyKAe8D-b8vIeyBJv2-dJHWGEkRltpb-RFIXsOiT6XigCT5JGrJnk,"; __urls['classic|8x11|softcover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.YmY5Yzg0Y2M,.zATLAe70IHRPgCNZ09HLVG_YD1aSgqATu3StJWD5AuxpCg2yD0Tc78F2lgzWw0XsQbK9DStSdDw,"; __urls['classic|8x11|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.OGMxYzhlZDk,.6PcR0dFWJesOvrNmSI-QYmeZ933_XVhDRSeIxHhlsTTtDb2pYYIdpeT5QkAz8Nz67oiNQHVsRQ8,"; __urls['classic|8x11|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.NmJlMzI1YTU,.NY2he4Sa8eKeblOZNO1XJ9j2niDw9n9rv84D4IHfuEPKnl9KbtuXhh8oItoMGhzsGbDeAZWwnbQ,"; __urls['classic|11x85|softcover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.YmNmZTUwNzI,.KzWrr83pak5mYIcPaiE5IvAH30Og2UeQDuzpdsGqwhsMeHZlyOfR6_eJ1Ih9vBOlPzlSlrNwlbA,"; __urls['classic|11x85|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.YmJkNWQyYzY,.E12Yz6itf9SEbDCiTO3HqW-A2h7H7gqWCHipS1q0G16AZuQ9Hy82ZK_d_JOMr0L51o-MYQjmXCY,"; __urls['classic|11x85|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.NWFjNTY5NTU,.jg3ZhkeJnK-ihgIeGGnSrDvMW61Vv0rHN80UEZdG7ki2-LON9rdV2Zg3aoI4IVPL8NAhYQ8dTSc,"; __urls['classic|12x12|softcover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.Mjc0NzAwZjU,.1F56NtdmKMDZcdz5HI6_a1jGUmccyQjPiZJv5YhWef_CelPklKqkonTEvN2KkR6RWGU2MjAeTdY,"; __urls['classic|12x12|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.Y2VjZjYzMGU,.69i8LN4omOmdYClllZKftknDdJRgqn6mq84C418b0oUKiltOBE3cEOM9KOFkHQHSlm31aheRYVk,"; __urls['classic|12x12|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.MjM3MTIyYjA,.9G0ZHjpuk7k2mCNG2KJV_LznEs4iU-VpcY8nBPiXmIfJdLYqLCqWUxqs6fbk8lPJDw8YJaHs2CA,"; __urls['classic|12x12|material|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.MDJjMmZjMDc,.bBkiuBcBXxsjyIP7OYYKwaUlEfTf3aqu-_Bzciee34vnLL_4DNLzK1M8OQnZ6AbwN0LU-g2r-68,"; __urls['classic|16x12|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.MTcyNmYzNzA,.rrPWTCToJ9jS_vk5Cl43_Q38pqa3YPVC-aYBqMAPvtNAhvCnlzhO3Cin0TQmobltwGvaF0J44qY,"; __urls['classic|16x12|material|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NTFkNTI5N2Y,.V1Dtj43B4aUgKPRiXUM-q8l553Glv1x5iy81ECZhZnVRzaWVIWSYoBOolBBaTMAewgbN4t0dD44,"; __urls['layflat|8x6|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.OGFiOTRiYWM,.RRDgwaV_859Uv8AQ8qNZ2USgI_alQfaaM00DG8snGx_kfpQizwT3VuzStdmF6aKn-eZN0WTK_qg,"; __urls['layflat|8x8|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.NjU1ZTEyYzM,.hNBD7RIeYActE7yGebs-rH61gx0blis23n_6aHbt_CMO5-OVt39z8K0JTh7OL4llxlevBxg273o,"; __urls['layflat|8x8|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=56.ZWZmYmRhNDE,.gxMKqx4d-4aEr7bW4bPztXGMPD9Iq8TFpelY3YIX46HyhnhDBzmo-CIcVs60KoigK8Db8nZSBG4,"; __urls['layflat|8x11|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.MTUxMTg1ZjU,.5NUTbRMKxDT5ya6bMeI54t0i03cCTWY0g5zUBToUSfaUJtZ5ONjZ5Be9sGWvQEEd_HbVM-xgzut4p1Lgg1GZiw,,"; __urls['layflat|8x11|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=57.NzZhODY2ZmQ,.nHjv_SMWTdKSeuqC7LNMoRxqzHroIzW53vkoGQiu0XOEISf46pBJcztEJVymzGXJX7_4L30IcIrz9bXeJmXNoQ,,"; __urls['layflat|11x85|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.MjAyYmRmOGE,.k17klylyg3q-rk1_TjSV3Fi_F8FNSQAxkA3wTdG4Fs_75yoX-1j7EYj53uPviQpP6TpyuiHn51dXripMLLpJIw,,"; __urls['layflat|11x85|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=58.Y2M2MjhjYjU,.I7kk4RRo2c3PTQcyjCBNHaDW_HhvSMFsyqGtLQxZrdewp-6yHTdm45trwo2s6UZBZ_0ONFYeE-FVBFImvF62nw,,"; __urls['layflat|12x12|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.MGQ0ZmJiNTc,.hoA9XNT94rrtluuJEt03vxnUsDsQ17BMCE1TIkEpK5PKgG3iTQepyYhk_ny6PAfgq2Vi_if-84OLQQ2zNjY0Vw,,"; __urls['layflat|12x12|deboss|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.ZGVkNGJlNDI,.-dUu7qxLWAvE9y5_h4Ldy-7OVbapCrcerzWt7sdNl5JT0M8q47azHN9njxQF7Uplg8dzmKEtyYaIr8PoZAfIhw,,"; __urls['layflat|12x12|material|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.NGM3YTVlMzg,.Rxcrmer_-o1mKaL0Ng3UCmKmmlu8P5o_-mv-xSAf8iLiOGyDAE5uNigsbZ4ccKk0p9EyS78FByDztoAxucqH5Q,,"; __urls['layflat|16x12|photocover|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=60.ODY0MzIzMzM,.0v1ICtpbd0ncyvNb10e6e1EQIpmbtgChdeFHnrvxcYVyaYKm2wSkcM_oSY5xuIbrN5UZFtbdaeGQ3V8nI8K1tg,,"; __urls['layflat|16x12|material|wedding']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=59.YmY3M2FiYzk,.VUZFs0tfbwC-75GWRJlu6elU67PSj-riTRZlEiKqxoW9stvJe-xEoltfP6c-BrtXbn4nvqDlEBomB-KcB3O9iQ,,";
            __prices = []; __prices['classic|6x6|softcover']=19.95; __prices['classic|8x6|softcover']=29.95; __prices['classic|8x6|photocover']=49.95; __prices['classic|8x8|softcover']=39.95; __prices['classic|8x8|photocover']=67.5; __prices['classic|8x8|deboss']=77.5; __prices['classic|8x11|softcover']=44.95; __prices['classic|8x11|photocover']=69.95; __prices['classic|8x11|deboss']=79.95; __prices['classic|11x85|softcover']=49.95; __prices['classic|11x85|photocover']=69.95; __prices['classic|11x85|deboss']=79.95; __prices['classic|12x12|softcover']=66.95; __prices['classic|12x12|photocover']=86.95; __prices['classic|12x12|deboss']=96.95; __prices['classic|12x12|material']=96.95; __prices['classic|16x12|photocover']=119.95; __prices['classic|16x12|material']=129.95; __prices['layflat|8x6|photocover']=59.5; __prices['layflat|8x8|photocover']=77.5; __prices['layflat|8x8|deboss']=87.5; __prices['layflat|8x11|photocover']=79.95; __prices['layflat|8x11|deboss']=89.95; __prices['layflat|11x85|photocover']=79.95; __prices['layflat|11x85|deboss']=89.95; __prices['layflat|12x12|photocover']=96.95; __prices['layflat|12x12|deboss']=106.95; __prices['layflat|12x12|material']=106.95; __prices['layflat|16x12|photocover']=129.95; __prices['layflat|16x12|material']=139.95; __prices['board|6x6|material']=149.95; __prices['board|8x8|photocover']=199.95; __prices['board|8x8|deboss']=209.95; __prices['board|8x8|material']=209.95; __prices['board|8x11|photocover']=279.95; __prices['board|8x11|deboss']=289.95; __prices['board|8x11|material']=289.95; __prices['board|11x85|photocover']=279.95; __prices['board|11x85|deboss']=289.95; __prices['board|11x85|material']=289.95; __prices['board|12x12|photocover']=349.95; __prices['board|12x12|deboss']=359.95; __prices['board|12x12|material']=359.95; __prices['board|16x12|material']=449.95;

            function set_prices(){
                var minprice = 99999999;
                if(__binding && __size && __cover) {
                    $('.wizstage[stage=4] li').each(function() {
                        var $li = $(this);
                        var theme = $li.attr('type');
                        if ((typeof __prices[__binding + '|' + __size + '|' + __cover] === 'number') && (typeof __urls[__binding+'|'+__size+'|'+__cover+'|'+theme] === "string")) {
                            minprice = Math.min(minprice, __prices[__binding + '|' + __size + '|' + __cover]);
                            $li.addClass('visible').removeClass('invisible').find('strong').text(curr(__prices[__binding + '|' + __size + '|' + __cover], false));
                        }
                        else $li.removeClass('visible').addClass('invisible');
                    });
                }
                else if(__binding && __size) {
                    $('.wizstage[stage=3] li').each(function() {
                        var $li = $(this);
                        var cover = $li.attr('type');
                        if (typeof __prices[__binding + '|' + __size + '|' + cover] === 'number') {
                            minprice = Math.min(minprice, __prices[__binding + '|' + __size + '|' + cover]);
                            $li.addClass('visible').removeClass('invisible').find('strong').text(curr(__prices[__binding + '|' + __size + '|' + cover], false));
                        }
                        else $li.removeClass('visible').addClass('invisible');
                    });
                }
                else if(__binding) {
                    $('.phodwiz .wizstage[stage=2] li').each(function() {
                        var $li = $(this);

                        var size = $li.attr('type');
                        if (typeof __prices[__binding + '|' + size + '|softcover'] === 'number') {
                            minprice = Math.min(minprice, __prices[__binding + '|' + size + '|softcover']);
                            $li.addClass('visible').removeClass('invisible').find('strong').text(curr(__prices[__binding + '|' + size + '|softcover'], false));
                        }
                        else if (typeof __prices[__binding + '|' + size + '|photocover'] === 'number') {
                            minprice = Math.min(minprice, __prices[__binding + '|' + size + '|photocover']);
                            $li.addClass('visible').removeClass('invisible').find('strong').text(curr(__prices[__binding + '|' + size + '|photocover'], false));
                        }
                        else if (typeof __prices[__binding + '|' + size + '|deboss'] === 'number') {
                            minprice = Math.min(minprice, __prices[__binding + '|' + size + '|deboss']);
                            $li.addClass('visible').removeClass('invisible').find('strong').text(curr(__prices[__binding + '|' + size + '|deboss'], false));
                        }
                        else if (typeof __prices[__binding + '|' + size + '|material'] === 'number') {
                            minprice = Math.min(minprice, __prices[__binding + '|' + size + '|material']);
                            $li.addClass('visible').removeClass('invisible').find('strong').text(curr(__prices[__binding + '|' + size + '|material'], false));
                        }
                        else $li.removeClass('visible').addClass('invisible');
                    });
                }

                $('#preview_price').html(curr(minprice));
                wizard_resize(false);
            }

            $(document).ready(function(){
                gtag('config', 'UA-1338422-2', {'page_path': '/photo-book-wizard-stage1'});
                wizard_resize(true);

                var phodwiz = $('.phodwiz')[0];
                var gestures = new Hammer(phodwiz);
                gestures.on('swipeleft swiperight', function(e){
                    if(e.type == 'swipeleft')
                        $(this).find('.right.arrow').click();
                    else $(this).find('.left.arrow').click();
                });

                $('.phodwiz > .arrow').click(function(){
                    if(__can_click) {
                        __can_click = false;
                        var $arrowbutton = $(this);
                        var $phodwiz = $(this).closest('.phodwiz');
                        var $activestage = $phodwiz.find('.activestage');
                        var $activewizstagechoices = $activestage.find('.wizstagechoices');
                        var curleft = parseFloat($activewizstagechoices.css('left').replace(/\D/g, '')) * -1;
                        var maxleft = (0 - ($activewizstagechoices.width() - $activestage.width() - 40));
                        var itemwidth = $activewizstagechoices.find('li:first').width();
                        var setcount = Math.floor($activestage.width() / itemwidth);
                        var setwidth = (itemwidth + 17);

                        if (setcount < $activewizstagechoices.find('li.visible').length) {
                            if ($arrowbutton.hasClass('left')) {
                                if (curleft >= 0) {
                                    var items_to_prepend = $activewizstagechoices.find('li').slice(-1);
                                    if($activewizstagechoices.find('li').slice(-1).hasClass('invisible'))
                                        items_to_prepend = $activewizstagechoices.find('li').slice(-2);
                                    $activewizstagechoices.prepend(items_to_prepend).css('left', curleft - setwidth);
                                    curleft = parseFloat($activewizstagechoices.css('left').replace(/\D/g, '')) * -1;
                                }
                                $activewizstagechoices.animate({
                                    left: (curleft + setwidth) + 'px'
                                }, 300, function(){
                                    __can_click = true;
                                });
                            }
                            else if ($arrowbutton.hasClass('right')) {
                                if (curleft < maxleft) {
                                    var items_to_append = $activewizstagechoices.find('li').slice(0, 1);
                                    if($activewizstagechoices.find('li').slice(0, 1).hasClass('invisible'))
                                        items_to_append = $activewizstagechoices.find('li').slice(0, 2);
                                    $activewizstagechoices.append(items_to_append).css('left', curleft + setwidth);
                                    curleft = parseFloat($activewizstagechoices.css('left').replace(/\D/g, '')) * -1;
                                }
                                $activewizstagechoices.animate({
                                    left: (curleft - setwidth) + 'px'
                                }, 300, function(){
                                    __can_click = true;
                                });
                            }
                        }
                        else __can_click = true;
                    }
                });

                $('.wizstagechoices > li').click(function(){
                    var $li = $(this);
                    var $phodwiz = $li.closest('.phodwiz');
                    var $activestage = $phodwiz.find('.activestage');
                    var $activechoices = $activestage.find('.wizstagechoices');
                    var stagenum = parseInt($activestage.attr('stage'));
                    var $img = $li.find('img');

                    // blue box animation
                    $img.clone().addClass('ani').css({
                        'width':$img.width(),
                        'top':($img.offset().top - $(window).scrollTop())+'px',
                        'left':($img.offset().left)+'px',
                    }).appendTo('body');
                    $('img.ani').animate({
                        'margin':'-30px',
                        'width':($(this).width()+30)
                    }, 300, 'swing', function(){
                        $(this).animate({
                            'margin':'-10px',
                            'width':($(this).width()-30)
                        }, 300, 'swing', function(){
                            $(this).remove();
                        });
                    });

                    // move on
                    window.setTimeout(function(){
                        if(stagenum == 1){
                            __binding = $li.attr('type');
                            __size = __cover = __theme = 0;

                            $('#selection_binding h4 span').html($li.attr('binding_h4'));
                            $('#selection_minpages').html($li.attr('binding_min')+" included");
                            $('#selection_maxpages').html($li.attr('binding_max'));
                            $('#paper').html($li.attr('binding_paper'));
                            $('#paper_upgrades').remove();
                            if($li.attr('binding_paper_upgrades') !== '') {
                                $('#paper').closest('p').after('<p id="paper_upgrades"><strong>Paper upgrades:</strong> ' + $li.attr('binding_paper_upgrades') + '</p>');
                            }
                            $('#selection_binding').show();
                            $('#preview_type').html($li.attr('binding_h4')+" | "+$li.attr('binding_min')+" pages included");
                            $('.phodwizselection').slideDown(500);
                        }
                        else if(stagenum == 2){
                            __size = $li.attr('type');
                            __orientation = $li.attr('orientation');
                            __cover = __theme = 0;

                            $phodwiz.find('.wizstage[stage=3] .wizstagechoices img').each(function(){
                                var $img = $(this);
                                var src = $img.attr('orig').replace('ORIENTATION', __orientation);
                                $img.attr('src', src);
                            });

                            $('#selection_sizelabel').html($li.attr('size_label'));
                            $('#selection_sizealt').html($li.attr('size_alt'));
                            $('.preview img').attr('src', 'img/photobookwizard/summary/'+__cover+'/'+__size+'.jpg');
                            $('#preview_size').html($li.attr('size_label'));
                            $('#preview_shipping').html($li.attr('shipping'));
                            $('#selection_size').slideDown(500);
                        }
                        else if(stagenum == 3){
                            __cover = $li.attr('type');
                            __theme = 0;

                            $('.preview img').attr('src', 'img/photobookwizard/summary/'+__cover+'/'+__size+'.jpg');
                            $('#selection_coverlabel').html($li.attr('cover_label'));
                            $('#selection_coverdetails').html($li.attr('cover_details'));
                            if($li.attr('extras_strong') !== ""){
                                $('#selection_extras p strong').html($li.attr('extras_strong'));
                                $('#selection_extras').slideDown(500);
                            }
                            else $('#selection_extras').hide();
                            $('#selection_cover').slideDown(500);
                        }

                        gtag('config', 'UA-1338422-2', {'page_path': '/photo-book-wizard-stage'+(stagenum+1)});
                        '/photo-book-wizard-stage1'});
                        if(stagenum <= 3){
                            set_prices();

                            var newheight = ($('.phodwiz').height() + 45);
                            $('.phodwiz, .phodwiz .phodwiztheatre').animate({
                                'height': newheight
                            }, 900);

                            $activechoices.animate({
                                'opacity':0,
                                'margin-top':'45px'
                            }, 500);
                            $activestage.find('.back').fadeOut(400, function(){ $(this).addClass('inactive'); });
                            $activestage.removeClass('activestage');

                            var $newactivestage = $phodwiz.find('.wizstage[stage='+(stagenum+1)+']').addClass('activestage');
                            $newactivestage.find('.back').hide().removeClass('inactive').fadeIn(400);
                            $newactivestage.css('top',((stagenum-1)*45)+'px');
                            $newactivestage.find('.wizstagechoices').css('left', '0px');
                            window.setTimeout(function(){
                                $newactivestage.show().animate({
                                    'opacity':1,
                                    'top': (stagenum*45)+'px'
                                }, 500);
                            }, 400);
                        }
                        else{
                            __theme = $li.attr('type');

                            if(typeof __urls[__binding+'|'+__size+'|'+__cover+'|'+__theme] === "string") {
                                __url = __urls[__binding + '|' + __size + '|' + __cover + '|' + __theme];
                                $('.preview .cta').addClass('active');
                                if(mobile)
                                    $('html, body').animate({scrollTop: $("a[name=proceed]").offset().top - 20}, 400);
                                else $('html, body').animate({scrollTop: $("a[name=selection]").offset().top - 20}, 400);
                            }
                            else alert('ERROR');
                        }
                    }, 500);
                });

                $('.back').click(function(){
                    if(!$(this).hasClass('inactive')){
                        $(this).addClass('inactive');

                        var $phodwiz = $('.phodwiz');
                        var $activestage = $phodwiz.find('.activestage');
                        var $activechoices = $activestage.find('.wizstagechoices');
                        var stagenum = parseInt($activestage.attr('stage'));
                        var $img = $(this).find('img');

                        if(stagenum == 2){
                            __binding = __size = __cover = __theme = 0;
                            $('.phodwizselection').slideUp(500);
                        }
                        else if(stagenum == 3){
                            __size = __cover = __theme = 0;
                            $('.preview img').attr('src', $('.preview img').attr('orig'));
                            $('#preview_shipping').html('');
                            $('#preview_size').html('');
                            $('#selection_size').slideUp(500);
                        }
                        else if(stagenum == 4){
                            __cover = __theme = 0;
                            $('#selection_cover').slideUp(500);
                            $('#selection_extras').hide();
                            $('.preview img').attr('src', 'img/photobookwizard/summary/'+__cover+'/'+__size+'.jpg');
                            $('.preview .cta').removeClass('active');
                        }
                        set_prices();

                        var newheight = ($('.phodwiz').height() - 45);
                        $('.phodwiz, .phodwiz .phodwiztheatre').animate({
                            'height': newheight
                        }, 700);

                        $activestage.animate({
                            'opacity':0,
                            'top': ((stagenum-1)*45)+'px'
                        }, 400, function(){
                            $activestage.hide().removeClass('activestage');
                        });

                        window.setTimeout(function(){
                            var $newactivestage = $phodwiz.find('.wizstage[stage='+(stagenum-1)+']').addClass('activestage');
                            $newactivestage.find('.back').hide().removeClass('inactive').fadeIn(400);
                            var $newactivechoices = $newactivestage.find('.wizstagechoices');
                            $newactivechoices.animate({
                                'opacity':1,
                                'margin-top':(parseFloat($newactivechoices.css('margin-top').replace(/\D/g,''))-45)+'px'
                            }, 400);
                        }, 300);
                    }
                });
            });

            function wizard_resize(doreset){
                var ww = $(window).width();
                var is_normal = ww >= 1100;
                var is_narrow = ww < 1100;
                var is_mobile = ww < 800;

                $('.phodwiz').css('opacity', 1);

                if(is_normal){
                    $('.phodwiz').width('100%');
                    $('.phodwiz .phodwiztheatre, .phodwiz .phodwiztheatre .wizstage').width(960);
                }
                else if(is_mobile){
                    var workable_area = Math.min(ww - 120, 360);
                    var newheight = workable_area + 40;

                    $('.phodwiz').width(workable_area + 40);
                    $('.phodwiz .phodwiztheatre, .phodwiz .phodwiztheatre .wizstage, .phodwiz .wizstagechoicescontainer .wizstagechoices li.visible').width(workable_area).height(workable_area);
                    $('.wizstagechoices').each(function(){$(this).width($(this).children('.wizstagechoices > li.visible').length * (workable_area + 17));});
                    $('.phodwiz .wizstagechoicescontainer .wizstagechoices, .phodwiz .wizstagechoicescontainer .wizstagechoices li.visible').height(newheight);
                    $('.phodwiz .wizstagechoicescontainer').height(newheight + 95);

                    var stagenum = parseInt($('.phodwiz .activestage').attr('stage'));
                    $('.phodwiz').height(newheight + 125 + (stagenum - 1) * 45);
                    $('.phodwiz .phodwiztheatre').height(newheight + 95 + (stagenum - 1) * 45);
                }
                else if(is_narrow){
                    $('.phodwiz').width(690);
                    $('.phodwiz .phodwiztheatre, .phodwiz .phodwiztheatre .wizstage').width(637);
                }

                if(!is_mobile){
                    $('.phodwiz .wizstagechoicescontainer .wizstagechoices li.visible').width(310);
                    $('.wizstagechoices').each(function(){
                        $(this).width($(this).find('li.visible').length * 327);
                    });
                    $('.phodwiz .wizstagechoicescontainer .wizstagechoices, .phodwiz .wizstagechoicescontainer .wizstagechoices li.visible').height(390);

                    var stagenum = parseInt($('.phodwiz .activestage').attr('stage'));
                    $('.phodwiz, .phodwiz .phodwiztheatre').height(485 + (stagenum - 1) * 45);
                    $('.phodwiz .wizstagechoicescontainer').height(485);
                }

                if(doreset) {
                    reset_wizard();
                }
            }

            function reset_wizard(){
                $('.phodwiz').each(function() {
                    var $phodwiz = $(this);
                    $phodwiz.find('.wizstagechoices').css('left', 0);
                    $phodwiz.find('.activestage').removeClass('activestage');
                    $phodwiz.find('.wizstage:first').addClass('activestage');
                    $phodwiz.find('.back').addClass('inactive');
                });
            }
        </script>
    </main>
@endsection