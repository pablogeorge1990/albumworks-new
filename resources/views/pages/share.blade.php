
@extends('templates.header')

@section('body')


    @php

    $slug = $_GET['id'];
    $redirect = "https://shareframe.photo-products.com.au/p/".$slug;
                                        
    if(@$c['mobile_browser'] > 0)
        return header("Location: ".$redirect);
    else {
    @endphp
            <main class="normal static nopadding">
                <script type="text/javascript">
                    $(document).ready(function(){
                        if($(window).width() < 960){
                            $('main').append('<iframe id="albumshareframe" style="width:100%; height:600px; margin-bottom:50px" frameBorder="0" src="<?=$redirect?>" scrolling="no" onload="window.parent.parent.scrollTo(0,0)"></iframe> ');
                        }
                        else{
                            $('main').append('<iframe id="albumshareframe" style="width:100%; height:1348px" frameBorder="0" src="<?=$redirect?>" scrolling="no" onload="window.parent.parent.scrollTo(0,0)"></iframe> ');                            
                        }
                    })
                </script>
            </main>

    @php
    }
    @endphp


@endsection