@extends('templates.header')

@section('body')
    <main class="normal static static2">
        <h1>Upgrading to the new <em>albumworks</em> Editor</h1>
        <p class="larger">The time has come to move over to the new and improved <em>albumworks</em> editor! Remember, you must finish any existing projects in your current Editor - you will not be able to finish them in the new Editor. Existing projects must be ordered by <strong>31 October 2018</strong>.</p>
        <p class="nomobile">To install the latest <em>albumworks</em> Editor, simply <a href="#join">click here</a>, download to your computer and install.</p>
        <p>We hope you will be impressed by the new version. With that great news, here's what to expect</p>

        <ol>
            <li>Faster performance</li>
            <li>Support for the latest cameras with images over 20MP </li>
            <li>NEW backgrounds and themes </li>
            <li>NEW range of page layouts</li>
            <li>Up to date product range with all NEW products</li>
        </ol>

        <h2>Questions?</h2>
        <p>If you are not sure how to download and install the NEW <em>albumworks</em> Editor, please call us on 1300 553 448 or email at &#115;&#101;&#114;&#118;&#105;&#099;&#101;&#064;&#097;&#108;&#098;&#117;&#109;&#119;&#111;&#114;&#107;&#115;&#046;&#099;&#111;&#109;&#046;&#097;&#117;</p>

        <h2>Completing projects</h2>
        <p>Completing your existing project is really easy. Your current editor will be active from now until Oct 31 for you to finish and order any existing projects. Once you have completed your last project in your current editor you then get to move over to the brand new <em>albumworks</em> editor which is filled with so many additional features and speed we just know you will love it!</p>

        <h2>Re-ordering old albums</h2>
        <p>Re-ordering a project you have completed is as easy as contacting our customer service team here in Melbourne who will be delighted to assist you over the phone, email or live chat. Please note this is subject to availability - providing your previous order is still part of our product range, we can arrange a re-order for you.</p>

        <h2>How to upgrade</h2>
        <p class="nomobile">Use the download form below to download and install the NEW <em>albumworks</em> Editor.</p>

        <div class="nomobile">
            <a name="join"></a>
            <h2 class="downloadhead">Download the NEW <em>albumworks</em> Editor</h2>
            <form class="box group" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=NEWEDITOR" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="NEWEDITOR" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>

                <!-- Actual fields -->
                <input type="text" name="first_name" class="firstname inputbox" placeholder="NAME">
                <input type="text" name="email" class="email inputbox" placeholder="EMAIL">
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET OUR FREE EDITOR</a>
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
                    <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
                </div>
            </form>
        </div>

        <h2 class="downloadhead">Get 32% off your first order with the following Voucher Code:</h2>
        <p class="promocodeblock">2018EDITOR</p>
        <br><br><br>
    </main>

@endsection