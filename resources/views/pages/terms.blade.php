@extends('templates.header')

@section('body')

    <main class="normal textonly">

	<h2>TERMS OF USE AND CONDITIONS</h2>

		<p>Welcome to the terms of use and conditions for albumworks, a photo product service provided by Pictureworks Group ABN 28 119 011 933 trading as albumworks. By accessing the albumworks website, using the albumworks Editor software, and/or submitting an order, you agree to be bound by these terms and conditions. These terms and conditions govern your use of the albumworks service and the products and services offered by albumworks. Please read them carefully.</p>

	<p>1. ORDER ACCEPTANCE<br />
		All orders are accepted and handled in accordance with the conditions below. The customer accepts these conditions by accessing the albumworks website, using albumworks Editor software and/or submitting an order.
		<br />
		<br />
		When ordering you must observe the laws of Australia and the country to which the product is being distributed.

		<br /><br />

	2. RESPONSIBILITY FOR IMAGES AND COPYRIGHT <br />
		As the creator of your photo product, you are solely responsible for the contents and design of the product file you submit to albumworks. albumworks does not accept responsibility for the product file you submit to us. In printing your photo product on your behalf, albumworks does NOT claim any ownership rights in the images you submit. Further, we require that the copyright in the photographs and images is held by you or you have permission to use the material. You are solely responsible for any unauthorized use of copyright material and indemnify albumworks against any claims made in connection with your use of our service and your breach of these terms of use or your breach of any rights of third parties, as outlined in section 6.
		<br /><br />

	3. OFFENSIVE AND EXPLICIT IMAGES<br />
		albumworks reserves the right to NOT produce any item that it reasonably believes may contain explicit or sexual imagery; is invasive of privacy; is vulgar, obscene or profane; or which may harass or cause distress or inconvenience to, or incite hatred of, any person.<br /><br />

		4. LIABILITY<br />
		The usage of the albumworks service is at your own risk, and we warrant only that your order is provided in an acceptable condition. If the condition of your order is not acceptable albumworks' only obligation will be for albumworks to either refund your order or reprint your order at no additional cost to you.  albumworks does not accept any liability whatsoever for damages or loss whether direct, indirect or consequential.<br /><br />
		albumworks does not warrant that the albumworks service will be bug, virus or error free, uninterrupted or secure.  You are solely responsible for any damage to your computer system or loss of data or projects that results from your usage of the albumworks Editor software.<br /><br />


		5. QUALITY AND SUBMISSION<br /> Please take care in proofing and previewing your photo product and carefully check your spelling. Once an order is placed, no changes can be made to it.
		<br /><br />
		albumworks emphasises that the quality of the prints in the products ordered will never be better than is to be expected on the basis of what you see on your computer monitor when using the albumworks Editor software. You should carefully examine the resolution, clarity and brightness of your photos with a calibrated monitor if you have any doubts about your image quality. Be aware that as computer monitors have a backlight they often make your photos look brighter than is actually the case.
		<br /><br />
		albumworks liability is limited to errors which are related to production, shipping and delivery of the product.
		<br /><br />

	6. INDEMNITY<br /> You agree to indemnify albumworks against any claim made against or suffered by albumworks in connection with your use of albumworks and your breach of these Terms of Use or your breach of any rights of third parties.<br /><br />

	7. DISPUTE AND ARBITRATION PROCEDURE
	</p>
		<ol style="line-height: 1.5; margin-bottom: 1.3em;">
			<li style="list-style-position: inside; list-style-type: decimal; margin-left: 10px;">Notice of Dispute: In the event of a dispute arising out of or relating to these Terms of Use, the customer must give written notice to albumworks, setting out the nature of the dispute and the relief sought.</li>
			<li style="list-style-position: inside; list-style-type: decimal; margin-left: 10px;">Negotiation: Within 14 days of receipt of the notice of dispute, albumworks and the customer will negotiate in good faith to resolve the dispute.</li>
			<li style="list-style-position: inside; list-style-type: decimal; margin-left: 10px;">Mediation: If the dispute is not resolved through negotiation, the parties will participate in mediation in accordance with the rules of the Australian Centre for International Commercial Arbitration (ACICA).</li>
			<li style="list-style-position: inside; list-style-type: decimal; margin-left: 10px;">Arbitration: If the dispute is not resolved through mediation, the dispute will be referred to and finally resolved by arbitration in accordance with the rules of the ACICA. The seat of the arbitration shall be Melbourne. The arbitration proceedings will be conducted in the English language.</li>
			<li style="list-style-position: inside; list-style-type: decimal; margin-left: 10px;">Governing Law: These Terms of Use and any arbitration proceedings shall be governed by and construed in accordance with the laws of Victoria, Australia.</li>
		</ol>


<p>
	8. ONLINE PREVIEW and PRODUCTION<br /> On order we will by default create an online preview of your submitted project and send you by 	email an anonymous URL. A link in this email enables you to have this preview deleted.<br /><br />
		Our service level goal is to produce an order in seven working days or less, however this is not a guarantee. Delayed manufacture is not a basis for cancellation of your order or for a request for compensation. You can track at the “Track” page on the albumworks web site.<br /><br />
		Once produced your order is dispatched according to the options in section 9.
		<br /><br />

	9. DISPATCH<br />
		When production has been completed, delivery times are an additional estimated 2-4 working days to metropolitan areas of major cities. Other destinations may take longer. Remote deliveries may take considerably longer subject to destination, weather conditions, pandemics and other events.<br /><br />
		Delayed delivery is not a basis for cancellation of any order or for a request for compensation.<br /><br />

		AUSTRALIAN ORDERS: <br /> After production we dispatch by Courier or Australia Post depending on product and service level selected by you.
		<br /><br />
		If you are not available to accept delivery, the delivery driver has the 'Authority To Leave' an item if he/she deems it safe to do so. If it's not deemed safe to leave an item, a card will be left explaining how you can receive delivery or pick-up the product at your local post office or freight depot (make sure you take your ID with you).
		<br /><br />

	INTERNATIONAL ORDERS:<br />
		International delivery may be available for certain products. Due to the complexity of International shipping, please contact our Customer Service team to discuss your options.

	<br /><br />

	10. GUARANTEE<br /> If you are not completely satisfied with your product, please contact us via our contact form on the albumworks website within two weeks of delivery or call 1300 553 448 during business hours. If there has been a production issue with your product we will happily resupply your item or refund your order.<br /><br />

	Pictureworks Group Pty Ltd trading as <em>albumworks</em><br /> 
	Level 2, 696 Bourke St, Melbourne VIC 3000<br /> 
	Phone: 1300 553 448 (during business hours)<br />
	Email: <script type="text/javascript"><!--/* Generated by www.email-encoder.com */
			for(var zackyj=["Yg","Ig","ZQ","Yg","dg","cg","PA","cg","dQ","Lg","YQ","cw","aQ","dQ","cg","aQ","Yw","Yw","aw","YQ","Lw","Yw","Lg","bw","bA","YQ","YQ","PA","cg","Ig","dQ","bQ","Yw","bA","Zg","cw","YQ","dA","aA","dw","bw","bw","bw","dg","bw","IA","ZQ","PQ","Lg","cw","ZQ","dQ","QA","aQ","Pg","Pg","aw","bQ","ZQ","QA","dw","bA","bQ","YQ","ZQ","cw","bQ","Og","bQ","Lg","YQ","cg"],jnftqy=[26,8,44,53,19,58,0,45,54,34,51,60,11,27,18,20,48,35,32,1,69,21,65,57,12,39,70,68,4,41,40,28,62,25,6,43,24,13,3,56,14,63,36,46,30,2,17,7,38,33,22,67,50,47,71,42,59,37,49,23,29,52,55,10,5,16,64,15,9,61,66,31],zbxqlo=new Array,i=0;i<jnftqy.length;i++)zbxqlo[jnftqy[i]]=zackyj[i];for(var i=0;i<zbxqlo.length;i++)document.write(atob(zbxqlo[i]+"=="));
			// --></script><noscript>Please enable JavaScript to see the email address (<a href="https://www.email-encoder.com/enablejs/" target="_blank" rel="noopener noreferrer">How-to</a>).</noscript><br />
	ACN 119 011 933 ABN 28 119 011 933</p>
</main>

@endsection