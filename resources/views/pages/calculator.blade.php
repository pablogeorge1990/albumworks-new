
@extends('templates.header')

@section('title', 'Quality Photo Books at affordable prices | albumworks')
@section('meta_description', 'Our beautiful Photo Books come in a range of sizes, covers and binding options. See our Photo Book prices.')

@section('body')

    <main>
        <section id="intro">
            <h1>Photo Books Pricing</h1>
            <!-- <div class="emgbanner">
                <p><strong>COVID-19 UPDATE:</strong></p>
                <p>
                    A TEMPORARY SURCHARGE OF $2.00 WILL BE APPLIED TO THE SHIPPING PRICE OF EACH ORDER. THIS IS DUE TO AN INCREASE IN FREIGHT COST <br>
                    AS A RESULT OF THE COVID-19 PANDEMIC.
                </p>
            </div> -->
            <p>
                There’s a book for every budget - from Softcover books to Premium Material
                covers. Use our pricing calculator below to choose the Photo Book that is
                going to bring your memories to life or have a play with all the options to
                find the right product for you.
            </p>

            <? if(time() > mktime(0,0,0,6,15,2023) and time() < mktime(0,0,0,7,6,2023)): ?>
            <div class="promo-banner-calculator">
                <h2>Apply the latest promotion to your order with the Voucher Code below!</h2>
                <br>
                <p>
                    <span class="cal-code">
                        17BIRTHDAY
                    </span>
                </p>
                <p>
                    <a href="https://www.albumworks.com.au/promotions" class="cta">LEARN MORE</a>
                </p>
            </div>
            <? elseif(time() > mktime(0,0,0,5,25,2023) and time() < mktime(0,0,0,6,15,2023)): ?>
            <div class="promo-banner-calculator">
                <h2>Apply the latest promotion to your order with the Voucher Code below!</h2>
                <br>
                <p>
                    <span class="cal-code">
                        MYSTORY
                    </span>
                </p>
                <p>
                    <a href="https://www.albumworks.com.au/promotions" class="cta">LEARN MORE</a>
                </p>
            </div>
            <? elseif(time() > mktime(0,0,0,3,16,2023) and time() < mktime(0,0,0,4,6,2023)): ?>
            <div class="promo-banner-calculator">
                <h2>Apply the latest promotion to you order with the Voucher Codes below!</h2>
                <br>
                <table style="margin: 0 auto;" width="90%">
                    <tr>
                        <td style="padding: 0 25px;">
                            <p style="font-size: 14px; font-weight: bold;">For all Layflat Photo Books</p>
                            <span class="cal-code">
                                LAYFLATISBEST
                            </span>
                        </td>
                        <td style="padding: 0 25px;">
                            <p style="font-size: 14px; font-weight: bold;">For all Classic Photo Books</p>
                            <span class="cal-code">
                                CLASSICISBEST
                            </span>
                        </td>
                    </tr>
                </table>
                <p>
                    <a href="https://www.albumworks.com.au/promotions" class="cta">LEARN MORE</a>
                </p>
            </div>
            <? elseif(time() > mktime(0,0,0,2,23,2023) and time() < mktime(0,0,0,3,16,2023)): ?>
            <div class="promo-banner-calculator">
                <h2>Apply the latest promotion to you order with the Voucher Code below!</h2>
                <br>
                <table style="margin: 0 auto;" width="90%">
                    <tr>
                        <td style="padding: 0 25px;">
                            <p style="font-size: 14px; font-weight: bold;">For all Photo Books</p>
                            <span class="cal-code">
                                MYYEARBOOK
                            </span>
                        </td>
                        <td style="padding: 0 25px;">
                            <p style="font-size: 14px; font-weight: bold;">For all Calendars</p>
                            <span class="cal-code">
                                2023CALENDAR
                            </span>
                        </td>
                    </tr>
                </table>
                <p>
                    <a href="https://www.albumworks.com.au/promotions" class="cta">LEARN MORE</a>
                </p>
            </div>
            <? elseif(time() > mktime(0,0,0,10,6,2022) and time() < mktime(0,0,0,11,3,2022)): ?>
            <div class="promo-banner-calculator">
                <h2>Apply the latest promotion to you order with the Voucher Code below!</h2>
                <br>
                <table style="margin: 0 auto;" width="90%">
                    <tr>
                        <td style="padding: 0 25px;">
                            <p style="font-size: 14px; font-weight: bold;">40% OFF any Softcover</p>
                            <span class="cal-code">
                                SOFTCOVER
                            </span>
                        </td>
                        <td style="padding: 0 25px;">
                            <p style="font-size: 14px; font-weight: bold;">30% OFF any Hardcover</p>
                            <span class="cal-code">
                                HARDCOVER
                            </span>
                        </td>
                    </tr>
                </table>
                <p>
                    <a href="https://www.albumworks.com.au/promotions" class="cta">LEARN MORE</a>
                </p>
            </div>
            <? elseif(time() > mktime(0,0,0,7,23,2021) and time() < mktime(0,0,0,8,18,2021)): ?>
            <div class="promo-banner-calculator">
                <h2>Apply the latest promotion to you order with the Voucher Code below!</h2>
                <br>
                <p>
                    <span class="cal-code">
                        MYMEMORIES
                    </span>
                </p>
                <p>
                    <a href="https://www.albumworks.com.au/promotions" class="cta">LEARN MORE</a>
                </p>
            </div>
            <? endif; ?>

        </section>
        <section id="calculator" class="group">
            <section id="binding" class="calcbox">
                <h3>Select Binding</h3>
                <div class="radioselecter threeup group">
                    <div class="radiooption active">
                        <img src="{{asset('img/classic.jpg')}}" alt="perfect binding" width="284" />
                        <div>
                            <label for="field_bindingpur" class="tooltip nomobile" title="Traditional book making with a valley in the middle of the book that defines classic binding. 40 pages.">Classic Photo Book</label>
                            <label for="field_bindingpur" class="mobileonlyblock">Classic</label>
                            <input type="radio" name="binding" id="field_bindingpur" value="pur" papertype="" checked/>
                        </div>
                    </div>
                    <div class="radiooption">
                        <img src="{{asset('img/layflat.jpg')}}" alt="layflat binding" width="284" />
                        <div>
                            <label for="field_bindinglf" class="tooltip nomobile" title="Images flow seamlessly from one page to the next with no valley. One beautiful spread and your book lays completely flat when open.">Layflat Photo Book</label>
                            <label for="field_bindinglf" class="mobileonlyblock">Layflat</label>
                            <input type="radio" name="binding" id="field_bindinglf" value="lf" papertype="" />
                        </div>
                    </div>
                    {{-- <div class="radiooption">
                        <img src="{{asset('img/1-board.jpg')}}" alt="flushmount binding" width="212" />
                        <div>
                            <label for="field_bindingfm" class="tooltip nomobile" title="Pages are mounted on top of our thick core board, to create a layflat book with thick, rigid pages.">Board Mounted Photo Book</label>
                            <label for="field_bindingfm" class="mobileonlyblock">Mounted</label>
                            <input type="radio" name="binding" id="field_bindingfm" value="fm" papertype="SD 190gsm E-Photo Matte" />
                        </div>
                    </div> --}}
                    <div class="radiooption">
                        <img src="{{asset('img/keep-books.jpg')}}" alt="mobilebook binding" width="284" />
                        <div>
                            <label for="field_bindingmb" class="tooltip nomobile" title="Keep Books are the fast and easy solution for Photo Books. Make online in 3 steps. 20 pages included.">Keep Book</label>
                            <label for="field_bindingmb" class="mobileonlyblock">Mobile</label>
                            <input type="radio" name="binding" id="field_bindingmb" value="mb" papertype="Standard Colour 150gsm Premium Silk" />
                        </div>
                    </div>
                </div>
            </section>
            <section id="paper" class="calcbox group">
                <h3>Choose Paper / Quality</h3>
                <div class="radioselector twoup group">
                    <div class="radiooption active radiochange bottom-up">
                        <p class="heading-sd" style="visibility: hidden">SD</p>
                        <h4 class="qualityheading"><div class="tooltip" title="Standard Colour - Stunning 4 colour digital printing for vibrant photo reproduction">Standard Colour</div></h4>
                        <input type="radio" name="paperType" id="type-sd" value="sd" checked/>
                    </div>
                    <div class="radiooption radiochange bottom-up">
                        <img src="{{asset('img/hc.png')}}" alt="HD" class="hd-icon"/>
                        <h4 class="qualityheading"><div class="tooltip" title="Hi Colour: Photo quality 7 colour printing with exceptional vibrancy and beautiful depth of colour">Hi Colour</div></h4>
                        <input type="radio" name="paperType" id="type-hd" value="hd"/>
                    </div>
                </div>
                <section quality="SD" id="type_sd">
                    <div class="radioselecter twoup group">
                        <div class="radiooption active">
                            <img src="{{asset('img/silk.jpg')}}" alt="silk" />
                            <div>
                                <label for="field_papersilk" class="tooltip nomobile" title="Durable 150gsm paper with a soft and silk finish.">Standard Colour 150gsm Premium Silk</label>
                                <label for="field_papersilk" class="mobileonlyblock">Premium Silk</label>
                                <input type="radio" name="paper" id="field_papersilk" value="silk" checked/>
                            </div>
                        </div>
                        <div class="radiooption">
                            <img src="{{asset('img/pearl.jpg')}}" alt="lustre" />
                            <div>
                                <label for="field_papereggshell" class="tooltip nomobile" title="Just like classic photo paper, offering brilliant depth of colour. Satin finish with low reflection.">Standard Colour 190gsm Photo Pearl</label>
                                <label for="field_papereggshell" class="mobileonlyblock">Photo Pearl</label>
                                <input type="radio" name="paper" id="field_paperpearl" value="pearl" />
                            </div>
                        </div>
                    </div>
                </section>
                <section quality="HD" id="type_hd" class="hidden">
                    <div class="radioselecter twoup group">
                        <div class="radiooption2 active">
                            <img src="{{asset('img/paper/HD-luster.jpg')}}" alt="lustre" class="big-mobile"/>
                            <div>
                                <label for="field_paperlustre" class="tooltip nomobile" title="Premium, archival & glare free paper with a professional touch.">Hi Colour 260gsm Photo Luster</label>
                                <label for="field_paperlustre" class="mobileonlyblock">Photo Luster</label>
                                <input type="radio" name="paper" id="field_paperlustre" value="lustre" checked/>
                            </div>
                        </div>
                        <div class="radiooption2 nomobile">
                            <img src="https://www.albumworks.com.au/img/hdpap.jpg?v2" alt="lustre" class="big-mobile">
                        </div>
                    </div>
                </section>
                <section quality="SDLF" id="type_sdlf" class="hidden">
                    <div class="radioselecter twoup group">
                        <div class="radiooption3 active">
                            <img src="{{asset('img/paper/lf-sc-l.jpg')}}" alt="scl" />
                            <div>
                                <label for="field_paperscl" class="tooltip nomobile" title="Premium durable photo paper with a gentle textured sheen.">Standard Colour 400gsm E-Photo Lustre</label>
                                <label for="field_paperscl" class="mobileonlyblock">E-Photo Lustre</label>
                                <input type="radio" name="paper" id="field_paperscl" value="scl" checked/>
                            </div>
                        </div>
                        <div class="radiooption3">
                            <img src="{{asset('img/paper/lf-sc-m.jpg')}}" alt="scm" />
                            <div>
                                <label for="field_paperscm" class="tooltip nomobile" title="Premium durable photo paper with a flat finish.">Standard Colour 400gsm E-Photo Matte</label>
                                <label for="field_paperscm" class="mobileonlyblock">E-Photo Matte</label>
                                <input type="radio" name="paper" id="field_papermatte" value="scm" />
                            </div>
                        </div>
                    </div>
                </section>
                <section quality="HDLF" id="type_hdlf" class="hidden">
                    <div class="radioselecter twoup group">
                        <div class="radiooption4 active">
                            <img src="{{asset('img/paper/lf-hc-l.jpg')}}" alt="hcl" />
                            <div>
                                <label for="field_paperhcl" class="tooltip nomobile" title="Premium durable photo paper with a gentle textured sheen.">Hi Colour 400gsm E-Photo Lustre</label>
                                <label for="field_paperhcl" class="mobileonlyblock">E-Photo Lustre</label>
                                <input type="radio" name="paper" id="field_paperhcl" value="hcl" checked/>
                            </div>
                        </div>
                        <div class="radiooption4">
                            <img src="{{asset('img/paper/lf-hc-m.jpg')}}" alt="hcm" />
                            <div>
                                <label for="field_paperhcm" class="tooltip nomobile" title="Premium durable photo paper with a flat finish.">Hi Colour 400gsm E-Photo Matte</label>
                                <label for="field_paperhcm" class="mobileonlyblock">E-Photo Matte</label>
                                <input type="radio" name="paper" id="field_paperhcm" value="hcm" />
                            </div>
                        </div>
                    </div>
                </section>
            </section>
            <section id="products" class="calcbox">
                <h3>Select Product Size</h3>
                <table cellpadding="0" cellspacing="0" width="100%" id="prodheadings" class="nomobile">
                    <thead>
                        <tr>
                            <td class="dead"></td>
                            <th code="softcover">
                                <label class="tooltip" title="Design your own cover with images and text. 190gsm, flexible cover.">Softcover</label>
                            </th>
                            <th code="photocover">
                                <label class="tooltip" title="Design your own cover with text and image. Matte or Glossy finish.">Photocover</label>
                            </th>
                            {{-- <th code="deboss">
                                <label class="tooltip" title="Combination of material cover with a personalised image. Your image choice is debossed into the material cover.">Debossed Cover</label>
                            </th> --}}
                            <th code="material">
                                <label class="tooltip" title="Premium material in a wide range of colours. Choose Buckram or Faux Leather material.">Premium Material</label>
                            </th>
                        </tr>
                    </thead>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%" id="prodvals">
                    <thead class="mobileonlythead">
                        <tr>
                            <th>
                                <label>Size</label>
                            </th>
                            <th code="softcover">
                                <label>Soft cover</label>
                            </th>
                            <th code="photocover">
                                <label>Photo cover</label>
                            </th>
                            {{-- <th code="deboss">
                                <label>Deboss</label>
                            </th> --}}
                            <th code="material">
                                <label>Material</label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="p86" productname='8 x 6" Landscape'>
                            <th>8 x 6"</th>
                            <td>Calculating</td>
                            <td>Calculating</td>
                            {{-- <td>Calculating</td> --}}
                            <td>Calculating</td>
                        </tr>
                        <tr id="p88" productname='8 x 8" Square'>
                            <th>8 x 8"</th>
                            <td>Calculating</td>
                            <td>Calculating</td>
                            {{-- <td>Calculating</td> --}}
                            <td>Calculating</td>
                        </tr>
                        <tr id="p811" productname='8 x 11" Portrait'>
                            <th>8 x 11"</th>
                            <td>Calculating</td>
                            <td>Calculating</td>
                            {{-- <td>Calculating</td> --}}
                            <td>Calculating</td>
                        </tr>
                        <tr id="p1185" productname='11 x 8.5" Landscape'>
                            <th>11 x 8.5"</th>
                            <td>Calculating</td>
                            <td class="default">Calculating</td>
                            {{-- <td>Calculating</td> --}}
                            <td>Calculating</td>
                        </tr>
                        <tr id="p1212" productname='12 x 12" Square'>
                            <th>12 x 12"</th>
                            <td>Calculating</td>
                            <td>Calculating</td>
                            {{-- <td>Calculating</td> --}}
                            <td>Calculating</td>
                        </tr>
                        <tr id="p1410" productname='14 x 10" Landscape'>
                            <th>14 x 10"</th>
                            <td>Calculating</td>
                            <td>Calculating</td>
                            {{-- <td>Calculating</td> --}}
                            <td>Calculating</td>
                        </tr>
                        <tr id="p1612" productname='16 x 12" Landscape'>
                            <th>16 x 12 "</th>
                            <td>Calculating</td>
                            <td>Calculating</td>
                            {{-- <td>Calculating</td> --}}
                            <td>Calculating</td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <section id="pagesoptions" class="group">
                <section id="pages" class="calcbox">
                    <h3>Choose Your Page Count</h3>
                    <dl id="extrapages">
                        <dt>Page Count:</dt>
                            <dd><input type="text" id="field_pagecount" class="inputbox intonly" value="Calc" min="40" max="200" increments="2" /></dd>
                        <dt id="extrapagesind" class="tooltip" title="" template="Extra pages are charged at<br>{PRICE}<br>and must be ordered in pairs.">Extra pages:</dt>
                            <dd id="extrapageprice">Calculating</dd>
                    </dl>
                    <dl id="pagetiers" class="group">
                        <dt>
                            <span class="nomobile">Included Free:</span>
                            <span class="mobileonlyinline">Included:</span>
                        </dt>
                            <dd id="papinc">Calc</dd>
                        <dt>Minimum:</dt>
                            <dd id="papmin">Calc</dd>
                        <dt>Maximum:</dt>
                            <dd id="papmax">Calc</dd>
                    </dl>
                </section>
                <section id="options" class="calcbox">
                    <h3>Optional Extras</h3>
                    <p id="container_pages_optionsslipcase">
                        <label for="pages_optionsslipcase" class="mobileonlyinline">Slip Case</label>
                        <label for="pages_optionsslipcase" class="tooltip nomobile" title="Handmade Slip Case, great for gift giving or protecting your Photo Book">Photo Book Slip Case</label>
                        <input type="checkbox" id="pages_optionsslipcase" />
                        <span>Calculating</span>
                    </p>
                    <p id="container_pages_optionsslipcasehs">
                        <label for="pages_optionsslipcasehs" class="mobileonlyinline">Slip Case Hot Stamping</label>
                        <label for="pages_optionsslipcasehs" class="tooltip nomobile" title="Personalise your slip case cover with a custom message">Photo Book Slip Case Hot Stamping</label>
                        <input type="checkbox" id="pages_optionsslipcasehs" disabled/>
                        <span>Calculating</span>
                    </p>
                    <p id="container_pages_optionspresbox">
                        <label for="pages_optionspresbox" class="mobileonlyinline">Presentation Box</label>
                        <label for="pages_optionspresbox" class="tooltip nomobile" title="Tailored to fit your Photo Book snugly, complete with ribbon pull and box lid.">Photo Book Presentation Box</label>
                        <input type="checkbox" id="pages_optionspresbox" />
                        <span>Calculating</span>
                    </p>
                    <p id="container_pages_optionspresboxhs">
                        <label for="pages_optionspresboxhs" class="mobileonlyinline">Presentation Box Hot Stamping</label>
                        <label for="pages_optionspresboxhs" class="tooltip nomobile" title="Personalise your presentation box cover with a custom message">Photo Book Presentation Box Hot Stamping</label>
                        <input type="checkbox" id="pages_optionspresboxhs" disabled/>
                        <span>Calculating</span>
                    </p>
                    <p id="container_pages_optionshs">
                        <label for="pages_optionshs" class="mobileonlyinline">Book Cover Hot Stamping</label>
                        <label for="pages_optionshs" class="tooltip nomobile" title="Personalise your cover with a custom message">Text Hot Stamping (Book Cover)</label>
                        <input type="checkbox" id="pages_optionshs" />
                        <span>Calculating</span>
                    </p>
                    <p id="container_pages_optionsvip">
                        <label for="pages_optionsvip" class="mobileonlyinline">VIP Care</label>
                        <label for="pages_optionsvip" class="tooltip nomobile" title="VIP Care is for the customer who wants to make sure that their creation is 'just right', from layout on the page to image quality, our team of professionals will review your design and provide advice on things to look at before going ahead with final production.">VIP Care</label>
                        <input type="checkbox" id="pages_optionsvip" />
                        <span>Calculating</span>
                    </p>
                </section>
            </section>
            <section id="summary" class="calcbox group">
                <h3>Your Summary</h3>
                <div id="details">
                    <h4>Details:</h4>
                    <ul>
                        <li><span>Calculating</span> Calculating</li>
                    </ul>
                </div>
                <div id="totals">
                    <ul>
                        <li>
                            <label>Subtotal:</label>
                            <span id="subtotal">Calculating</span>
                        </li>
                        <li>
                            <label id="qlabel">Quantity:</label>
                            <span id="quantity"><input type="text" id="field_quantity" value="1" class="inputbox intonly" min="1" max="999" increments="1" /></span>
                        </li>
                        <li>
                            <label id="poboxlabel" for="poboxind" title="">PO BOX DELIVERY:</label>
                            <input type="checkbox" id="poboxind" />
                        </li>
                        <li>
                            <label id="shippingind" title="">DELIVERY:</label>
                            <span id="shipping">Calculating</span>
                        </li>
                        <li>
                            <div id="promocodefield">
                                <label class="deemphasised">Promotion Code?</label>
                                <span id="promocodespan">
                                    <input type="text" id="field_promocode" value="" class="inputbox" placeholder="Enter your promotion code" onkeypress="if(event.keyCode == 13) $(this).next().click();" />
                                    <button id="promocode_button" onclick="validate_promocode(true)">+</button>
                                </span>
                                <div id="promoerror">Error: Invalid Promotion Code. <a href="javascript:void(0)" onclick="try_promocode()">Try again</a></div>
                            </div>
                            <div id="promocoderesult">
                                <label>Discount:</label>
                                <span id="discount">$0.00</span>
                                <a href="javascript:void(0)" onclick="edit_promocode()">X</a>
                            </div>
                        </li>
                    </ul>
                    <h4>TOTAL: <span id="total">Calculating</span></h4>
                </div>
            </section>
        </section>
    </main>
    <script type="text/javascript">
        var __pricing = $.parseJSON('<?=json_encode($pricing)?>');
        var __currentprod = null;
        var __currentcover = null;
    </script>
    <script>
        $(document).ready(function(){
            $('#field_papersilk').prop("checked", true);
        });
    </script>

@endsection
