@extends('templates.header')

@section('body')
    <main class="normal static">
        <h1>Select your product</h1>
        <p class="subheading">Create a photo product online with our intuitive Online Editor. Our Photo Books allow you to choose from a Simple or Advanced editing experience, depending on your Photo Book project. See below for our most popular products.</p>
        <div class="group">
            <div class="productblock">
                <h3>Mobile Books</h3>
                <a href="{{env('BASEPATH')}}mobile-books"><img src="{{env('BASEPATH')}}img/popular-mobile/01-mobilebooks.jpg" /></a>
                <p>Fast, simple, easy. Mobile books are the quickest way to create a Photo Book full of memories. Available in a range of sizes and covers. <a href="{{env('BASEPATH')}}photo-books">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}mobile-books" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Canvas Prints</h3>
                <a href="{{env('BASEPATH')}}canvas-photo-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-mobile/02-canvas.jpg" /></a>
                <p>Classic Canvas construction, printed on high quality poly-cotton canvas with a satin finish. Create your own gallery from your favourite photos. <a href="{{env('BASEPATH')}}canvas-photo-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}canvas-photo-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Calendars</h3>
                <a href="{{env('BASEPATH')}}wall-calendar-create-now"><img src="{{env('BASEPATH')}}img/popular-mobile/03-calendars.jpg" /></a>
                <p>Beautiful 12 month personalised Calendars filled with your precious photos and printed on premium photo papers. <a href="{{env('BASEPATH')}}wall-calendar">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}wall-calendar-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Framed Prints</h3>
                <a href="{{env('BASEPATH')}}framed-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-mobile/04-framed.jpg" /></a>
                <p>High resolution prints with exceptional colour reproduction. Choose from full frame prints or prints with a mat board. <a href="{{env('BASEPATH')}}framed-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}framed-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Premium Prints</h3>
                <a href="{{env('BASEPATH')}}premium-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-mobile/05-premiumprints.jpg" /></a>
                <p>Quality photos prints are back. Printed on our state of the art main lab HD photo press. Bring your photos to life in a classic set of premium photo prints. <a href="{{env('BASEPATH')}}premium-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}premium-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
            <div class="productblock">
                <h3>Instagram Prints</h3>
                <a href="{{env('BASEPATH')}}instagram-prints-create-now"><img src="{{env('BASEPATH')}}img/popular-mobile/06-instagram.jpg" /></a>
                <p>Turn any of your photos into stylish Photo Cards. Create from your phone, tablet or desktop using your Instagram, Facebook or local photos. <a href="{{env('BASEPATH')}}instagram-prints">Learn more &gt;</a></p>
                <p class="ctaholder"><a href="{{env('BASEPATH')}}instagram-prints-create-now" class="cta">CREATE NOW</a></p>
            </div>
        </div>
    </main>

@endsection