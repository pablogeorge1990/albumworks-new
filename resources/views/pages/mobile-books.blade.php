@extends('templates.header')

@section('title', 'Mobile Books: Beautiful and personalised | albumworks')
@section('meta_description', 'Create beautiful personalised Photo Books from your mobile phone. Easy to use, free software. 100% design freedom, fast delivery and local support.')

@section('body')
    <main class="normal static" id="style2018">
        <h1><span>Mobile Books</span></h1>
        <h2>Use our product selector to choose the right book for you</h2>

        <div class="phodwizselection group anipicker" style="display: block;">
            <div class="info">
                <div class="formstyle">
                    <h4 class="group">
                        <span>Size:</span>
                        <select name="size" id="select_size" class="inputbox" onchange="change_options($(this).attr('name'), this.value, false)">
                            <option value="11x85" cm="(27.5 x 22cm)">11x8.5" Landscape</option>
                            <option value="12x12" cm="(30.5 x 30.5cm)">12x12" Square</option>
                            <option value="8x11" cm="(20.5 x 27.5cm)">8x11" Portrait</option>
                            <option value="8x8" cm="(20.5 x 20.5cm)">8x8" Square</option>
                            <option value="8x6" cm="(20.5 x 15cm)">8x6" Landscape</option>
                        </select>
                    </h4>
                    <h4 class="group">
                        <span>Cover:</span>
                        <select name="cover" id="select_cover" class="inputbox" onchange="change_options($(this).attr('name'), this.value, false)">
                            <option value="photocover" cover_details="<p><strong>Cover finish: </strong><span>Matte or Gloss</span></p><p>Print your image and text directly on the front and back cover.</p>" extras="pbox,scase">Photocover</option>
                            <option value="softcover" cover_details="<p><strong>Cover finish: </strong><span>190gsm flexible card cover</span></p><p>Print your image and text directly on the front and back cover.</p>" extras="">Softcover</option>
                        </select>
                    </h4>
                    <h4 class="group">
                        <span>Theme:</span>
                        <select name="theme" id="select_theme" class="inputbox" onchange="change_options($(this).attr('name'), this.value, true)">
                            <option value="fullbleed">Single Photo</option>
                            <option value="contemporary">Multiple Photos</option>
                        </select>
                    </h4>
                    <div class="summary">
                        <p>From <strong id="preview_price">$39.95</strong> <span id="preview_shipping">+ $11.95 shipping</span></p>
                        <p>
                            <a href="javascript:void(0)" onclick="if($(this).hasClass('active')) {  start_editor(__url); }" class="cta active">CREATE NOW</a><br>
                            Or <a href="#join">download our desktop Editor</a> to create this product
                        </p>
                    </div>
                    <div id="preview_promo">
                        <div class="promobox">
                            <? if((time() > mktime(0,0,0,7,12,2018)) && (time() < mktime(0,0,0,8,2,2018))): ?>
                            <h4>SAVE UP TO 46% OFF YOUR ORDER</h4>
                            <div>12BDAY</div>
                            <? endif; ?>
                            <? if((time() > mktime(0,0,0,8,2,2018)) && (time() < mktime(0,0,0,8,23,2018))): ?>
                            <h4>FATHER'S DAY: SAVE 34% OFF YOUR ORDER</h4>
                            <div>DAD34OFF</div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>

            </div>
            <a name="proceed"></a>
            <div class="preview">
                <div class="bookstage">
                    <? foreach(['landscape', 'portrait', 'square'] as $orientation): ?>
                    <figure class='<?=$orientation?> book'>
                        <ul class='hardcover_front'>
                            <li>
                                <div class="coverDesign">
                                    <img src="img/photobookpicker/<?=$orientation?>/covers/photocover.png" />
                                </div>
                            </li>
                            <li></li>
                        </ul>
                        <ul class='page'>
                            <li></li>
                            <li class="rightpage">
                                <img src="img/photobookpicker/<?=$orientation?>/themes/fullbleed-right.png" />
                            </li>
                            <li class="leftpage">
                                <img src="img/photobookpicker/<?=$orientation?>/themes/fullbleed-left.png" />
                            </li>
                            <li></li>
                        </ul>
                        <ul class='hardcover_back'><li></li><li></li></ul>
                        <ul class='book_spine'><li></li><li></li></ul>
                    </figure>
                    <? endforeach; ?>
                </div>
                <div class="previewbit">
                    <p><strong id="preview_size">&#8226; 11 x 8.5" Landscape</strong> <span>(27.5 x 22cm)</span></p>
                    <p>
                        <strong>Pages:</strong> <span id="selection_minpages">20 included</span>
                        &nbsp;&nbsp;
                        <strong>Max Pages:</strong> <span id="selection_maxpages">200</span>
                    </p>
                    <p><strong>Paper:</strong> <span id="paper">150gsm SD Premium Silk</span></p>
                </div>
                <div class="previewbit">
                    <p><strong id="selection_coverlabel">&#8226; Photocover</strong> <a target="_blank" href="https://www.albumworks.com.au/photo-books#binding">Learn more</a></p>
                    <div id="selection_coverdetails">
                        <p><strong>Cover finish:</strong> Matte or Gloss</p>
                        <p>Print your image and text directly on the front and back cover.</p>
                    </div>
                </div>
                <div class="previewbit" id="availableoptions">
                    <p><strong>&#8226; Available Options</strong> <a target="_blank" href="https://www.albumworks.com.au/photo-books#accessories">Learn more</a></p>
                    <p class="opt_pbox"><strong>Presentation Box:</strong> A stylish box, tailored to fit your Photo Book</p>
                    <p class="opt_scase"><strong>Slip Case:</strong> A handmade slip cover to protect your Photo Book</p>
                </div>
            </div>
        </div>

        <h2 class="byline">All Mobile Books are available online ONLY</h2>

        <div class="nomobile">
            <h2>Or download our Advanced Editor to get the full range of products</h2>
            <form class="box group" id="horidlform" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="post" onsubmit="return checkdlform(this)">
                <a name="join"></a>
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=MOBILEBOOK" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Download" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="MOBILEBOOK" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">

                <!-- Adword fields -->
                <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>

                <!-- Actual fields -->
                <input type="text" name="first_name" class="firstname inputbox" placeholder="NAME">
                <input type="text" name="email" class="email inputbox" placeholder="EMAIL">
                <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">GET OUR FREE EDITOR</a>
                <div class="checkarea">
                    <input type="checkbox" checked="checked" value="1" for="hori_emailOptOut" name="emailOptOut" class="check">
                    <label for="hori_emailOptOut">Keep me updated with special offers, promotions and software updates</label>
                </div>
            </form>
        </div>

        <div class="threecols group">
            <div>
                <img src="img/phonebooks/01.jpg" alt="man and woman looking at computer smiling" />
                <h3>Easy-to-use Editor</h3>
                <p>It's as easy as click and create. Select a product, choose your photos, drag and drop, add your own text and bring your photos to life in one of our wide, wide range of products.</p>
                <a href="https://www.albumworks.com.au/photo-books-howto" class="cta">LEARN MORE</a>
            </div>
            <div>
                <img src="img/phonebooks/02.jpg" alt="handing a photobook with deboss cover from generation to generation" />
                <h3>Dispatched within just 7 days</h3>
                <p>Our priority is to make all your products as fast as we can - but never at the expense of quality. In fact 95% off all items are manufactured and dispatched within 2-4 business days.</p>
                <a href="https://www.albumworks.com.au/shipping" class="cta">LEARN MORE</a>
            </div>
            <div>
                <img src="img/phonebooks/03.jpg" alt="an array of photo books, some open and some closed, showing their covers and contents" />
                <h3>More Photo Books means more choices</h3>
                <p>With 3 different book binding methods, 4 different cover types across 7 different sizes, there’s just so much to choose from! We have a book for every need and occassion.</p>
                <a href="https://www.albumworks.com.au/photo-books" class="cta">LEARN MORE</a>
            </div>
        </div>

        <link rel="stylesheet" type="text/css" media="screen" href="css/flippybook.css" />
        <script type="text/javascript">
            ;window.Modernizr=function(a,b,c){function w(a){j.cssText=a}function x(a,b){return w(m.join(a+";")+(b||""))}function y(a,b){return typeof a===b}function z(a,b){return!!~(""+a).indexOf(b)}function A(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:y(f,"function")?f.bind(d||b):f}return!1}var d="2.6.2",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l={}.toString,m=" -webkit- -moz- -o- -ms- ".split(" "),n={},o={},p={},q=[],r=q.slice,s,t=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},u={}.hasOwnProperty,v;!y(u,"undefined")&&!y(u.call,"undefined")?v=function(a,b){return u.call(a,b)}:v=function(a,b){return b in a&&y(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=r.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(r.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(r.call(arguments)))};return e}),n.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:t(["@media (",m.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c};for(var B in n)v(n,B)&&(s=B.toLowerCase(),e[s]=n[B](),q.push((e[s]?"":"no-")+s));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)v(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},w(""),i=k=null,function(a,b){function k(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function l(){var a=r.elements;return typeof a=="string"?a.split(" "):a}function m(a){var b=i[a[g]];return b||(b={},h++,a[g]=h,i[h]=b),b}function n(a,c,f){c||(c=b);if(j)return c.createElement(a);f||(f=m(c));var g;return f.cache[a]?g=f.cache[a].cloneNode():e.test(a)?g=(f.cache[a]=f.createElem(a)).cloneNode():g=f.createElem(a),g.canHaveChildren&&!d.test(a)?f.frag.appendChild(g):g}function o(a,c){a||(a=b);if(j)return a.createDocumentFragment();c=c||m(a);var d=c.frag.cloneNode(),e=0,f=l(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function p(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return r.shivMethods?n(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+l().join().replace(/\w+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(r,b.frag)}function q(a){a||(a=b);var c=m(a);return r.shivCSS&&!f&&!c.hasCSS&&(c.hasCSS=!!k(a,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),j||p(a,c),a}var c=a.html5||{},d=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,e=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,f,g="_html5shiv",h=0,i={},j;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",f="hidden"in a,j=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){f=!0,j=!0}})();var r={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,supportsUnknownElements:j,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:q,createElement:n,createDocumentFragment:o};a.html5=r,q(b)}(this,b),e._version=d,e._prefixes=m,e.testStyles=t,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+q.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};

            // regen from spreadsheet
            var __prices = [];
            __prices['8x6|softcover']=11.95;
            __prices['8x6|photocover']=21.95;
            __prices['8x8|softcover']=24.95;
            __prices['8x8|photocover']=34.95;
            __prices['8x11|softcover']=29.95;
            __prices['8x11|photocover']=39.95;
            __prices['11x85|softcover']=29.95;
            __prices['11x85|photocover']=39.95;
            __prices['12x12|softcover']=44.95;
            __prices['12x12|photocover']=54.95;

            var __urls = [];
            __urls['12x12|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=63.ZTQ0NzE2OTg,.Oj8Y8Qj6rqaDdKKzjDZ4nKMdOmldXDzbEQBLo9ix4nPjsNHimLCJEZ4ZvhPetPc4Pltggi56qZOISFNjIwJ54Q,,";
            __urls['11x85|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=63.MTRmYTgzM2I,.kVt0MGcZkMxBKjnWhkrcf2C7KZPMWYXikYPOcDU-NVd8Ks2msIWsKFgxViCN-ow-kLVkKQXxk_Qj3ddRS2O3_w,,";
            __urls['11x85|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=63.NmY4Nzk0OGI,.23gmUJQytd2o0f1mRjzWTk6xoSZYVTLHsbKJLN5yNXZldgiwCMnlp3iFsYywntQbgZsbQcgZvSB2OkbfwefJEg,,";
            __urls['8x11|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=62.ZjU5ODJhYWI,.BdKuRIoWoEwLAGRXGnr9JSwlcHUOfRL_RJq_HxIbnmcxO1jHCcfveTO5P7qMhvP06cMkwgxhkrG6csQeNZmzDA,,";
            __urls['8x11|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=62.MDBjMThhZjg,.wb-yMxaLPEhTMvqYG1xUJ75ew5p3VFcLlZOadVlcIEiTJpMCcA4ClkFkNXqXajx9j85z5I1k3BeFX9ggrk-97g,,";
            __urls['12x12|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=64.YmQxYjIyY2Q,.rlml_HopRDvqd1xMV66WKCY9XhKCkUl-cufQeSdwLPHX9wQ0q8qntuAqOwM9J48IHzpMakelHPnpzTiwraHmlg,,";
            __urls['8x6|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.MTMxNzBkNzE,.IsTxMp8_kGf_vv0q8GrWr0k3fmTNcZOr8ktdMTU8NWS2IcB86PsugHbRe57PgQBOAom6Ugi-QJPep0F1eEAVSQ,,";
            __urls['8x6|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.ZTAwZGY5ODU,.Sn41UXOsv8QVrxY6IfIoNSkNSUBYh8x5_CXsXTgnOfzjJWYbe7ykbAVHLs2K8IT20GPTq5AJ7iAXid_u7YlmQw,,";
            __urls['8x8|photocover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.ZjE3YjA3ZGI,.hu0FMvdUPF3wfy27tHP7AkP5jw4gHxdTYHNw5MhAwSXzTQaeIIU8KSXd1_eTVnvCTn1SFXQlaZ7STJFZi6nygg,,";
            __urls['8x8|softcover|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=61.Zjg4ZGIzMWE,.4Nm22XQZXWtSmb7w3gogL0CpffCuYh9tpVKqmNRzLkRLbQx6GcVfZM4BUX_xBJmso0CGQK5HJFliczLKiy1ImQ,,";
            //__urls['6x6|soft|fullbleed']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=62.OWRmN2JjZWI,.-2QT0whNXIt7GqKiaagsZDNq90agQ92NbCwoQAhwwBIy3jkhm2BNemOBjIeP4pRSAL4wxzVg-TVBSYA5xhA25Q,,";
            __urls['12x12|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.ODM1YjE4MWQ,.b8wuEV1oG3NjAtTXGlPwIjSG8JXZubu_jHY_AlVNIOKg23v9nlQnoRIXw-tWy1B4w4DY56swFDU,";
            __urls['11x85|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.MDZiZTZjODM,.1UzTtcDWEXpmz0HSsWzKdH14K5Qcb5DrQioYSxc-kVvM5pl4ZOIX1ub454BVnogYYNFbL76jzMY,";
            __urls['11x85|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=53.MTVlN2VmZTU,.2K2eP9GP6ZnePrqeha2QJvnjq9LQrHjpgrqgnxIJTWthitat_N8CSqdn2KE_hP4VJEjx0-_Q-Ww,";
            __urls['8x11|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.YjhiZGU1ZTY,.l-Hf2rNjw8hHfFf6fAovgyM9L01urniPsOoiZEXVS37axrTsj5Z93JCJf9VeRRmL9MNXioryi6U,";
            __urls['8x11|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.MDM2NmUwMDA,.7k-AD2KeeSljRThMCTR6BOqwPIUwpt6YtY_SUu80A0ThrMR69hKTKaSxtmHnWCocUVbjE2lLYss,";
            __urls['12x12|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=54.YTQ2MTVjNjc,.qxkJtRXGA1XyxfoOZW137pVnxpVeHVGMVQJfxYYcO6iJGKAzVgR-5iruLmiUMIYDGbae-3DOkTU,";
            __urls['8x6|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.YTQ0ZDQzYmI,.pLzdqzgWxPCaaoURUDxMyL2Uxlnqd_pnKSCZEMDVAqgWWKWVeeUBkJzIod4wDpaacYAx02fBjv4,";
            __urls['8x6|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.YmFhNzBmYTE,.z8_v_VQGjxobnXR-IeJznkfiM1Y7ge9EuSJORCoJxmnQ_eIDU8yZ5ZzwmWSUQXnB5IbaMDjcNL0,";
            __urls['8x8|photocover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.ZTExZjQwODc,.j3HZgJe5llX8rn1m0-extGLWbniKpqeVE62GQUH1Gmk6B4MpMUHxMwPexSjdrbocXXhxWtkKD9s,";
            __urls['8x8|softcover|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=51.MWQyNDNhODk,.x2CvcNPKeUc2trc4HAM3byNtQOF94bKIINh9HgzdX_29QqC27-0R5MABTw49AD2StQ53NiY6mO0,";
            //__urls['6x6|soft|contemporary']="https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=52.N2M4YzhlMWI,.RrRsdHAOPYj2c4jQopkYQzhb8bx4Sk1mqpq2jRV0P_R41wYQnPfnuM4O7wpKThGHZGjScZhu6k0,";

            __url = __urls['11x85|photocover|fullbleed'];
            __orientation = 'landscape';
            function change_options(key, val, openbook){
                var size = $('#select_size').val();
                var cover = $('#select_cover').val();
                var theme = $('#select_theme').val();
                var price_index = size+"|"+cover;
                var url_index = size+"|"+cover+"|"+theme;
                __url = __urls[url_index];

                // update size select options
                $('#select_size option').each(function(){
                    var option = $(this).attr('value');
                    var attempt = option+'|'+cover+'|'+theme;
                    if(typeof __urls[attempt] === 'undefined') {
                        $(this).prop('disabled', 'disabled');
                    }
                    else $(this).prop('disabled', '');
                });
                if($('#select_size option:selected').prop('disabled')){
                    $('#select_size').val('11x85');
                    return change_options(key, val, openbook);
                }

                // update cover select options
                $('#select_cover option').each(function(){
                    var option = $(this).attr('value');
                    var attempt = size+'|'+option+'|'+theme;
                    if(typeof __urls[attempt] === 'undefined') {
                        $(this).prop('disabled', 'disabled');
                    }
                    else $(this).prop('disabled', '');
                });
                if($('#select_cover option:selected').prop('disabled')){
                    if(typeof __urls[size+'|photocover|'+theme] !== 'undefined')
                        $('#select_cover').val('photocover');
                    else if(typeof __urls[size+'|softcover|'+theme] !== 'undefined')
                        $('#select_cover').val('softcover');
                    else if(typeof __urls[size+'|deboss|'+theme] !== 'undefined')
                        $('#select_cover').val('deboss');
                    else if(typeof __urls[size+'|material|'+theme] !== 'undefined')
                        $('#select_cover').val('material');
                    return change_options(key, val, openbook);
                }

                // update theme select options
                $('#select_theme option').each(function(){
                    var option = $(this).attr('value');
                    var attempt = size+'|'+cover+'|'+option;
                    if(typeof __urls[attempt] === 'undefined') {
                        $(this).prop('disabled', 'disabled');
                    }
                    else $(this).prop('disabled', '');
                });
                if($('#select_theme option:selected').prop('disabled')){
                    $('#select_theme').val('contemporary');
                    return change_options(key, val, openbook);
                }

                // update prices
                $('#preview_price').html(curr(__prices[price_index]));

                // update summary and set up vars
                if(key == 'size'){
                    var shipping_price = 11.95;
                    switch(val){
                        case "11x85": __orientation = 'landscape'; break;
                        case "12x12": __orientation = 'square'; shipping_price = 14.95; break;
                        case "16x12": __orientation = 'landscape'; shipping_price = 16.95; break;
                        case "8x11": __orientation = 'portrait'; break;
                        case "8x8": __orientation = 'square'; break;
                        case "8x6": __orientation = 'landscape'; break;
                        case "6x6": __orientation = 'square'; break;
                    }
                    $('#preview_shipping').html("+ "+curr(shipping_price)+" shipping");
                }
                $('#selection_minpages').html(20);
                $('#selection_maxpages').html(200);
                $('#paper').html('150gsm SD Premium Silk');
                $('#paper_upgrades').hide();

                $('#preview_size').html("&#8226; "+$('#select_size option:selected').html());
                $('#preview_size').next().html($('#select_size option:selected').attr('cm'));

                $('#selection_coverlabel').html("&#8226; "+$('#select_cover option:selected').html());
                $('#selection_coverdetails').html($('#select_cover option:selected').attr('cover_details'));

                $('#availableoptions').hide();
                if($('#select_cover option:selected').attr('extras') !== ''){
                    $('#availableoptions').show();
                }

                // book animation bits
                $('.'+__orientation+'.book .coverDesign img').attr('src', 'img/photobookpicker/'+__orientation+'/covers/'+$('#select_cover').val()+'.png');
                $('.'+__orientation+'.book .rightpage img').attr('src', 'img/photobookpicker/'+__orientation+'/themes/'+$('#select_theme').val()+'-right.png');
                $('.'+__orientation+'.book .leftpage img').attr('src', 'img/photobookpicker/'+__orientation+'/themes/'+$('#select_theme').val()+'-left.png');

                $('.book').removeClass('hovering').removeClass('active');
                $('.'+__orientation+'.book').addClass('active');

                if(openbook) {
                    window.setTimeout(function () {
                        $('.' + __orientation + '.book').addClass('hovering');
                    }, 100);
                    window.setTimeout(function () {
                        $('.' + __orientation + '.book').removeClass('hovering');
                    }, 1500);
                }
            }

            $(document).ready(function() {
                $('.landscape.book').addClass('active');
            });
        </script>
    </main>
@endsection