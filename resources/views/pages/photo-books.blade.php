@extends('templates.header')

@section('title', 'Photo Books: Beautiful and personalised | albumworks')
@section('meta_description', 'Create beautiful personalised Photo Books. Easy to use, free software. 100% design freedom, fast delivery and local support.')

@section('body')

    <nav class="persistent">
        <a href="#overview" class="active">OVERVIEW</a>
        <a href="#quality">QUALITY</a>
        <a href="#paper">PAPER</a>
        <a href="#covers">COVERS</a>
        <a href="#sizing">SIZING</a>
        <a href="#binding">BINDING</a>
        <a href="#accessories">ACCESSORIES</a>
        <a href="calculator">PRICING</a>
        <a class="cta" href="javascript:void(0)" onclick="splitchoice('photo-book-picker')">CREATE NOW</a>
    </nav>
    <div class="dummypers"></div>
    <main class="fullwidth">
        <? if(time() > mktime(0,0,0,1,2,2018) and time() < mktime(0,0,0,1,31,2018)): ?>
            <section onclick="window.location = 'promotions';" class="parallax panel hastext leader" style="cursor:pointer; background-image:url(img/promo/jan18/photobooks.jpg)" data-imgr="<?=(620/1600)?>" id="overview">
                <!--
                <div class="text">
                    <h2><em>"The best thing to hold onto in life is each other"</em></h2>
                    <p>- AUDREY HEPBURN</p>
                </div>
                -->
            </section>
        <? else: ?>
            <section class="parallax panel hastext leader" style="background-image:url(img/photobooks/header.jpg)" data-imgr="<?=(620/1600)?>" id="overview">
                <div class="text">
                    <h2><em>"The best thing to hold onto in life is each other"</em></h2>
                    <p>- AUDREY HEPBURN</p>
                </div>
            </section>
        <? endif; ?>
        <section class="bigheader bookmark">
            <h1>Photo Books</h1>
            <p><em>"We do not remember days, we remember moments"</em></p>
        </section>
        <section class="prodpres group">
            <img src="img/photobooks/book-intro.png" alt="open book" />
            <div class="wording">
                <h2>Wider choice with more included as standard</h2>
                <h3>Our books contain just that bit extra.</h3>
                <p>All Classic Photo Books contain 40 pages as standard (<em>not just 20</em>), with 150gsm quality Silk paper (<em>some go down as low as 100gsm</em>) and the books are bound beautifully and contain fly leaves (<em>which many have done away with</em>). It all adds up to more book for your dollar.  Which is important, because your memories deserve it.</p>
            </div>
        </section>
        <section class="slider fullwidth">
            <nav>
                <a href="javascript:void(0)"><span class="nomobile">100% DESIGN FREEDOM</span><span class="mobileonlyinline">DESIGN</span></a>
                <a href="javascript:void(0)"><span class="nomobile">CAREFUL CRAFTSMANSHIP</span><span class="mobileonlyinline">CRAFTSMANSHIP</span></a>
                <a href="javascript:void(0)"><span class="nomobile">QUALITY GUARANTEED</span><span class="mobileonlyinline">QUALITY</span></a>
                <a href="javascript:void(0)"><span class="nomobile">FRIENDLY SUPPORT</span><span class="mobileonlyinline">SUPPORT</span></a>
            </nav>
            <div class="slidewindow">
                <div class="slideholder">
                    <div class="slide" style="background-image:url(img/photobooks/BG-design.png)">
                        <div class="content">
                            <h3>Easy to use, free software</h3>
                            <p>Our Editor allows you to design your Photo Book exactly how you want it. Just choose a theme, apply layouts, backgrounds and embellishments, then drag and drop your photos wherever you want them. It's that easy.</p>
                            <p><strong>Note:</strong> <em>We recommend that you have your photos organised first to make the drag and drop bit just a little easier!</em></p>
                        </div>
                    </div>
                    <div class="slide" style="background-image:url(img/photobooks/BG-craftsmanship.png)">
                        <div class="content">
                            <h3>We don't cut corners.</h3>
                            <p>All books are beautifully case bound and are complete with end papers, something that many others cut out. We think they are important because they protect the front and last papers (and to be honest, it's how a real book has been made for centuries).</p>
                        </div>
                    </div>
                    <div class="slide" style="background-image:url(img/photobooks/BG-quality.png)">
                        <div class="content">
                            <h3>Quality you can count on.</h3>
                            <p>It doesn't happen often, but we can make mistakes. When it does, we stand behind everything we make for you. Every page, every cover and every delivery - if there is a reason you're not 100% happy with your product, just get in touch with our friendly customer service team. We promise to do everything we can to help.</p>
                        </div>
                    </div>
                    <div class="slide" style="background-image:url(img/photobooks/BG-support.png)">
                        <div class="content">
                            <h3>Support when you need it.</h3>
                            <p>Our Melbourne based Customer Service team are here to support you. Whether you want advice on choosing a font or how to align photos in the editor, just call us on <strong>1300 553 448</strong> , use the live chat feature at the bottom right of this page,  or email us at <a href="mailto:&#115;&#101;&#114;&#118;&#105;&#099;&#101;&#064;&#097;&#108;&#098;&#117;&#109;&#119;&#111;&#114;&#107;&#115;&#046;&#099;&#111;&#109;&#046;&#097;&#117;">&#115;&#101;&#114;&#118;&#105;&#099;&#101;&#064;&#097;&#108;&#098;&#117;&#109;&#119;&#111;&#114;&#107;&#115;&#046;&#099;&#111;&#109;&#046;&#097;&#117;</a> - we would love to help!</p>
                            <p><a href="contact">Contact &gt;</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="static bookmark" id="quality">
            <h2>Choose beautiful Standard Colour or stunning Hi Colour printing</h2>
            <div class="group leftright">
                <div class="group">
                    <div class="content">
                        <h3><strong>Standard Colour:</strong> raised to a higher standard</h3>
                        <p>Beautiful 4 colour digital printing comes as standard. We print all our Standard Colour Photo Books on the latest generation Digital Printing Presses and carefully colour manage all printing. This process results in your photos looking just as good as the day you took them.</p>
                        <p><img src="img/photobooks/SD-quality-icon.jpg" alt="4 colour 150gsm premium silk"/></p>
                    </div>
                    <img src="img/photobooks/SD.jpg" height="387" />
                </div>
                <div class="group">
                    <div class="content">
                        <h3><strong>Hi Colour:</strong> producing the colour that your camera saw</h3>
                        <p>If you want to take your Photo Book to the next level, print it on the world’s newest, very latest generation Hi Colour press. Your colours will be even more vibrant with smoother gradients and finer resolution than in Standard Colour. We achieve this with 7 colour specialty inks instead of 4 colour, use a finer screen mode for superior dot placement and achieve stronger colour control by using premium papers. The end result is more even colour, punch and vibrancy to your precious photos.</p>
                        <p><img src="img/photobooks/HD-icon.png?v260" alt="7 colour 260gsm lustre and glossy by canon"/></p>
                    </div>
                    <img src="img/photobooks/HD.jpg" height="387" />
                </div>
            </div>
        </section>
        <section class="static greyslider bookmark" id="paper">
            <h2>A wider choice of high quality papers</h2>
            <p>There's a paper type for every occasion. Sometimes it's softly textured luster, other times its earthy traditional paper. With a choice of 150gsm Silk or gorgeous 190gsm Photo Pearl paper in Standard Colour, or our brand new upgraded 260gsm Photo Luster in Hi Colour, we've got the paper choices to match your desire.</p>
            <section class="slider">
                <nav>
                    <a href="javascript:void(0)"><span class="nomobile">Standard Colour 150gsm PREMIUM SILK PAPER</span><span class="mobileonlyinline">SILK</span></a>
                    <a href="javascript:void(0)"><span class="nomobile">Standard Colour 190gsm PHOTO PEARL PAPER</span><span class="mobileonlyinline">SD-PAPER</span></a>
                    <a href="javascript:void(0)"><span class="nomobile">Hi Colour 260gsm PHOTO LUSTER PAPER</span><span class="mobileonlyinline">LUSTER</span></a>
                    <!--<a href="javascript:void(0)"><span class="nomobile">250gsm PHOTO GLOSSY</span><span class="mobileonlyinline">GLOSSY</span></a> -->
                </nav>
                <div class="slidewindow">
                    <div class="slideholder">
                        <div class="slide">
                            <div class="content">
                                <h3>Silk - setting the standard</h3>
                                <p>150gsm in weight with a slightly off-white colour tone and very smooth feel, Silk is an all-rounder that produces great colour balance with good tones. If nice all round colour reproduction in Standard Colour is your goal, then Silk is the right paper for you.</p>
                            </div>
                            <img src="img/photobooks/sd-silk.jpg" />                            
                        </div>
                        <div class="slide">
                            <div class="content">
                                <h3>Standard Colour Paper Upgrade Options</h3>
                                <p>Upgrade your Hardcover Photo Book from 150gsm Premium Silk to 190gsm Photo Pearl. With a heavier weight, Photo Pearl is thick and luxurious with a brilliant depth of colour. It has a stunning pearlescent finish, hence its name.</p>
                            </div>
                            <img src="img/photobooks/SD-papers-2.jpg" />
                        </div>
                        <div class="slide">
                            <div class="content">
                                <h3>Hi Colour Photo Luster</h3>
                                <p>A simply stunning Photo Paper and our latest sheet - now upgraded from 240gsm up to 260gsm with the release of Hi Colour printing. Luster has a very gentle eggshell finish, has exceptional vibrancy and at 260gsm has plenty of heft in the hand. This specialty photo paper provides remarkable dot placement stability to work in harmony with the Hi Colour Press. It reflects light evenly for beautiful visual clarity.</p>
                            </div>
                            <img src="img/photobooks/hd-lustre.jpg" />
                        </div>
                        <!--
                        <div class="slide">
                            <div class="content">
                                <h3>HD Photo Glossy</h3>
                                <p>The paper of choice when you want colour, colour and even more colour. It's the CEO's favourite sheet, but some may find it carries just too much vibrancy. We love it for bringing maximum punch to landscapes and vistas, or where depth of detail is everything. 250gsm, like Luster, it is 300 year archival and bulky in the hand.</p>
                            </div>
                            <img src="img/photobooks/hd-glossy.jpg" />
                        </div>
                        -->
                    </div>
                </div>
            </section>
        </section>
        <section id="covers" class="static bookmark">
            <h2>We've got covers covered</h2>
            <p class="leadin">With so many covers to choose from, you'll be sure to find the cover that's right for your photo keepsake. Our Photocovers are vibrant and sleek and our Premium Material covers offer a stylish, sophisticated touch. </p>
            <div class="picker group">
                <nav class="nomobile">
                    <a href="javascript:void(0)" onclick="picker_switch('coverhard', this)">Hard Photocover</a>
                    <a href="javascript:void(0)" onclick="picker_switch('coversoft', this)">Soft Photocover</a>
                    {{-- <a href="javascript:void(0)" onclick="picker_switch('coverdebossed', this)">Debossed Hardcover</a> --}}
                    <a href="javascript:void(0)" onclick="picker_switch('coverbuckram', this)">Buckram Material</a>
                    <a href="javascript:void(0)" onclick="picker_switch('coverfaux', this)">Faux Leather</a>
                </nav>
                <div class="pickeropts">
                    <div id="coverhard">
                        <h3 class="mobileonlyblock">Hard Photocover</h3>
                        <p class="img"><img src="img/photobooks/hard-photocover2.jpg?v2" style="width:350px" /></p>
                        <p>A hardcover book that allows you to design anything at all on the cover. We print the cover on a high quality printing press, laminate it, then bind your cover onto a high quality case. <br><br>As standard, all Photocover Photo Books are made with a beautiful Matte laminate finish. Or you can choose a Luster finish to give your Photo Book a gentle sheen - the choice is yours!</p>
                    </div>
                    <div id="coversoft">
                        <h3 class="mobileonlyblock">Soft Photocover</h3>
                        <p class="img"><img src="img/photobooks/soft-photocover.jpg" /></p>
                        <p>You can choose a Softcover Photo Book for a magazine-like finish. Printed on 300gsm card and laminated with a micron film - this means the cover stays nice and flexible. Design whatever your like on the cover - it’s your canvas to paint.</p>
                    </div>
                    <!-- <div id="coverdebossed">
                        <h3 class="mobileonlyblock">Debossed Hardcover</h3>
                        <p class="img"><img src="img/photobooks/debossed.jpg" /></p>
                        <p>Can't decide between a Photocover or Material cover? Our all NEW Debossed Photo Books allow you have an image and title on your Photo Book cover! Your image will be debossed into the front cover.</p>
                    </div> -->
                    <div id="coverbuckram">
                        <h3 class="mobileonlyblock">Buckram Material</h3>
                        <p class="img"><img src="img/photobooks/buckram.jpg" /></p>
                        <div class="swatchpicker">
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/EUROBUCKRAM404_500.jpg', 'European Buckram White', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_500.jpg" alt="European Buckram White 404/500" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/CHROMO369_300.jpg', 'European Buckram Metallic Pearl', 400, 400)"><img src="img/photobooks/buckram/s/2021/CHROMO369_300.jpg" alt="European Buckram Metallic Pearl 369/308" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/CHROMO369_302.jpg', 'European Buckram Metallic Steel', 400, 400)"><img src="img/photobooks/buckram/s/2021/CHROMO369_302.jpg" alt="European Buckram Metallic Steel 369/314" /></a>
                            <!--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/EUROBUCKRAM404_502.jpg', 'European Buckram Battleship Grey', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_502.jpg" alt="European Buckram Battleship Grey 404/502" /></a>-->
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/EUROBUCKRAM404_526.jpg', 'Black Linen<br><span>(Temporary replacing European Buckram Black)</span>', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_526.jpg" alt="European Buckram Black 404/526" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/EUROBUCKRAM404_505.jpg', 'European Buckram Blue', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_505.jpg" alt="European Buckram Blue 404/505" /></a>
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/EUROBUCKRAM404_516.jpg', 'European Buckram Canary', 400, 400)"><img src="img/photobooks/buckram/s/2021/EUROBUCKRAM404_516.jpg" alt="European Buckram Canary 404/516" /></a> -->
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/new/EUROBUCKRAM404_519.png', 'European Buckram Vivid Burgundy', 400, 400)"><img src="img/photobooks/buckram/s/new/EUROBUCKRAM404_519.png" alt="European Buckram Vivid Burgundy 404/519" /></a>
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_BRILLIANTA4319.jpg', 'Linen Baby Blue', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_BRILLIANTA4319.jpg" alt="Linen Baby Blue" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_BRILLIANTA4320.jpg', 'Linen Baby Pink', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_BRILLIANTA4320.jpg" alt="Linen Baby Pink" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_MAGIC42005.jpg', 'Linen Dark Red Glitter', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_MAGIC42005.jpg" alt="Linen Dark Red Glitter" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_MAGIC42001.jpg', 'Linen Black & Gold Glitter', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_MAGIC42001.jpg" alt="Linen Black & Gold Glitter" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_MAGIC42016.jpg', 'Linen Snow White Glitter', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_MAGIC42016.jpg" alt="Linen Snow White Glitter" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_NATUUR1085.jpg', 'Linen Cream', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_NATUUR1085.jpg" alt="Linen Cream" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_SENATOR74000.jpg', 'Linen Sepia', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_SENATOR74000.jpg" alt="Linen Sepia" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_SENATOR74007.jpg', 'Linen Aqua', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_SENATOR74007.jpg" alt="Linen Aqua" /></a> -->
                            <!-- <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/2021/LIN_SENATOR74012.jpg', 'Linen Vermilion', 400, 400)"><img src="img/photobooks/buckram/s/2021/LIN_SENATOR74012.jpg" alt="Linen Vermilion" /></a> -->


                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/CHROMO369_308.jpg', 'Metallic Buckram Gold', 400, 400)"><img src="img/photobooks/buckram/s/CHROMO369_308.jpg" alt="Metallic Buckram Gold 369/308" /></a>--}}
                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/CHROMO369_314.jpg', 'Metallic Buckram Aqua', 400, 400)"><img src="img/photobooks/buckram/s/CHROMO369_314.jpg" alt="Metallic Buckram Aqua 369/314" /></a>--}}
                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/EUROBUCKRAM404_509.jpg', 'European Buckram Catalina Blue', 400, 400)"><img src="img/photobooks/buckram/s/EUROBUCKRAM404_509.jpg" alt="European Buckram Catalina Blue 404/509" /></a>--}}
                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/EUROBUCKRAM404_510.jpg', 'European Buckram Blue Danube', 400, 400)"><img src="img/photobooks/buckram/s/EUROBUCKRAM404_510.jpg" alt="European Buckram Blue Danube 404/510" /></a>--}}
                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/EUROBUCKRAM404_517.jpg', 'European Buckram Flame', 400, 400)"><img src="img/photobooks/buckram/s/EUROBUCKRAM404_517.jpg" alt="European Buckram Flame 404/517" /></a>--}}
                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/EUROBUCKRAM404_522.jpg', 'European Buckram Espresso', 400, 400)"><img src="img/photobooks/buckram/s/EUROBUCKRAM404_522.jpg" alt="European Buckram Espresso 404/522" /></a>--}}
                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/Metallic-Gold-Buckram-CHROMO369_300.jpg', 'Metallic Buckram Pearl', 400, 400)"><img src="img/photobooks/buckram/s/Metallic-Gold-Buckram-CHROMO369_300.jpg" alt="Metallic Buckram Pearl 369/300" /></a>--}}
                            {{--<a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/buckram/l/Metallic-GreyCHROMO369_302.jpg', 'Metallic Buckram Steel', 400, 400)"><img src="img/photobooks/buckram/s/Metallic-GreyCHROMO369_302.jpg" alt="Metallic Buckram Steel 369/302" /></a>--}}
                        </div>
                        <p>Choose from our range of gorgeous material covers. We have 6 elegant Faux Leather colours to give your Photo Book that traditional luxurious feel.</p>
                        <p style="color:red">PLEASE NOTE: Some colours may be out of stock when ordering</p>
                    </div>
                    <div id="coverfaux">
                        <h3 class="mobileonlyblock">Faux Leather</h3>
                        <p class="img"><img src="img/photobooks/faux-leather.jpg?v2" /></p>
                        <div class="swatchpicker">
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/covers/black2.jpg', 'Black', 400, 400)"><img src="img/photobooks/covers/black.jpg" alt="Black" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/covers/darkblue3l.jpg', 'Dark Blue', 400, 400)"><img src="img/photobooks/covers/darkblue3l.jpg" alt="Dark Blue" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/covers/brown3l.jpg', 'Brown', 400, 400)"><img src="img/photobooks/covers/brown3l.jpg" alt="Brown" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/covers/darkgrey3l.jpg', 'Dark Grey', 400, 400)"><img src="img/photobooks/covers/darkgrey3l.jpg" alt="Dark Grey" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/covers/red3l.jpg', 'Red', 400, 400)"><img src="img/photobooks/covers/red3l.jpg" alt="Red" /></a>
                            <a class="dialog" href="javascript:void(0)" onclick="lightbox_image('img/photobooks/covers/ivory3l.jpg', 'Ivory', 400, 400)"><img src="img/photobooks/covers/ivory3l.jpg" alt="Ivory" /></a>
                        </div>
                        <p>Choose from our range of gorgeous material covers. We have 4 elegant Faux Leather colours to give your Photo Book that traditional luxurious feel.</p>
                        <p style="color:red">PLEASE NOTE: Some colours may be out of stock when ordering</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="static group bookmark" id="sizing">
            <h2>Sizing</h2>
            <div class="selector group">
                <img class="visual" src="img/photobooks/11x85.jpg?v2" alt="photo book range" />
                <div class="clickers">
                    <div class="group">
                        <div>
                            <h3>Small</h3>
                            <nav>
                                <a href="javascript:void(0)" onclick="selector_select(this, 'img/photobooks/8x6.jpg?2', '$29.95')">8 x 6"</a>
                                <a href="javascript:void(0)" onclick="selector_select(this, 'img/photobooks/8x8.jpg?2', '$39.95')">8 x 8"</a>
                            </nav>
                        </div>

                        <div>
                            <h3>Medium</h3>
                            <nav>
                                <a href="javascript:void(0)" onclick="selector_select(this, 'img/photobooks/8x11.jpg?2', '$49.95')">8 x 11"</a>
                                <a class="active" href="javascript:void(0)" onclick="selector_select(this, 'img/photobooks/11x85.jpg?2', '$49.95')">11 x 8.5"</a>
                            </nav>
                        </div>

                        <div>
                            <h3>Large</h3>
                            <nav>
                                <a href="javascript:void(0)" onclick="selector_select(this, 'img/photobooks/12x12.jpg?2', '$69.95')">12 x 12"</a>
                                <a href="javascript:void(0)" onclick="selector_select(this, 'img/photobooks/14x10.jpg?2', '$104.95')">14 x 10"</a>
                                <a href="javascript:void(0)" onclick="selector_select(this, 'img/photobooks/16x12.jpg?2', '$124.95')">16 x 12"</a>
                            </nav>
                        </div>
                    </div>

                    <p>from <strong class="price">$49.95</strong></p>

                    <p class="view"><a href="calculator">VIEW PRICING &gt;</a></p>

                    <p><a href="javascript:void(0)" onclick="splitchoice('popular')" class="cta">CREATE NOW</a></p>
                </div>
            </div>
        </section>
        <section class="static vidslider bookmark" id="binding">
            <h2>Bound your way</h2>
            <p class="leadin">We employ classic bookmaking techniques to ensure our Photo Books are of the same high quality as our digital printing. We use state-of-the-art PUR glue for strong perfect binding to ensure your albums will run the distance. Our covers are carefully trimmed and the corners tucked for an elegant finish.  Choose from traditional Classic binding or Layflat binding for seamless page spreads.</p>
            <div class="slider lightgreyslider fullwidth" style="margin-top:40px;">
                <nav>
                    <a href="javascript:void(0)"><span class="nomobile">CLASSIC PHOTO BOOK</span><span class="mobileonlyinline">CLASSIC</span></a>
                    <a href="javascript:void(0)"><span class="nomobile">LAYFLAT PHOTO BOOK</span><span class="mobileonlyinline">LAYFLAT</span></a>
                    {{-- <a href="javascript:void(0)"><span class="nomobile">BOARD MOUNTED PHOTO BOOK</span><span class="mobileonlyinline">BOARD MOUNTED</span></a> --}}
                </nav>
                <div class="slidewindow">
                    <div class="slideholder">
                        <div class="slide vidslide">
                            <div class="content">
                                <h3>Classic Photo Books</h3>
                                <p>Classic books have the valley in the middle of the book with that beautiful wave and fall in the sheet that defines a traditional book. You can have up to 200 pages, more pages than with Layflat binding, which means with the maximum pages your book will be a hefty 25mm thick. Pages are bound into the book using a really clever, special glue called PUR. It's clever because when we apply the glue, it cures but stays flexible for life and it's super strong. We've tested it on books up to 5cm thick and then hung the book for hours off a single page - the result? Nothing budges!</p>
                            </div>
                            <video width="100%" loop autoplay muted preload><source src="img/photobooks/book-vid.mp4" type="video/mp4"></video>
                        </div>
                        <div class="slide vidslide">
                            <div class="content">
                                <h3>Layflat Photo Books</h3>
                                <p>Layflat books do exactly that... lay flat. It's the polar opposite of the Classic Photo Book - the book is completely flat from left page all the way to right with no join - it's one big continuous sheet. This sheet is then glued to the one following it, and so on. The result is a super thick bonded page, making the binding unbelievably strong. Printed on either 400gsm E-Photo Lustre or Matte paper which is water resistant and dirt-repellent,   Choose from Standard Colour or Hi Colour printing.  Layflat Photo Books can hold up to 140 pages.</p>
                            </div>
                            <video width="100%" loop autoplay muted preload><source src="img/photobooks/layflat4.mp4" type="video/mp4"></video>
                        </div>
                        {{-- <div class="slide vidslide">
                            <div class="content">
                                <h3>Board Mounted Photo Books</h3>
                                <p>Board Mounted Layflat Photo Books are our best Photo Books yet. Superior in quality, these Photo Books are perfect for luscious wedding photos, professional portfolio photography or any of your most treasured photos. Board Mounted Photo Books may look similar in appearance to our Layflat Photo Books. However, it's the careful production of these books that makes them so special. Each individual page of our Board Mounted books are mounted specially on top of our thick core board. Creating luscious, board like pages.  Thicker pages creates a thicker, more luxurious book.</p>
                                <p>All images are printed on E-Photo Matte paper before being fused onto heavyweight board core, resulting in the recognisable thickness of our Board Mounted Layflat Photo Books.</p>
                            </div>
                            <video width="100%" loop autoplay muted preload><source src="img/photobooks/board-mount3.mp4" type="video/mp4"></video>
                        </div> --}}
                    </div>
                </div>
            </div>
        </section>
        <section class="static bookmark" id="accessories">
            <h2>Stunning Accessories</h2>
            <div class="group leftright">
                <div class="group">
                    <div class="content">
                        <h3>Leave your mark</h3>
                        <p>Our Silver and Blind Stamping will leave a lasting impression. Choose the perfect message to complete your perfect Photo Book.</p>
                        <p>Just $14.95 for all Premium Material covers in every Photo Book size. Also available for all Slip Cases and Presentation Boxes.</p>
                        <p>Choose from two typefaces and three stunning finishes - Gold, Silver or Blind.</p>
                        <ul class="hoverchanger group">
                            <li><img src="img/photobooks/accessories/gold-helvet.jpg" big="img/photobooks/accessories/gold-helvet-big.jpg" /></li>
                            <li><img src="img/photobooks/accessories/silver-helvet.jpg" big="img/photobooks/accessories/silver-helvet-big.jpg" /></li>
                            <li><img src="img/photobooks/accessories/gold-optima.jpg" big="img/photobooks/accessories/gold-optima-big.jpg" /></li>
                            <li><img src="img/photobooks/accessories/silver-optima.jpg" big="img/photobooks/accessories/silver-optima-big.jpg" /></li>
                            <li><img src="img/photobooks/accessories/blind-optima.jpg" big="img/photobooks/accessories/blind-optima-big.jpg" /></li>
                        </ul>
                    </div>
                    <img src="img/photobooks/accessories/gold-helvet-big.jpg" height="387" />
                </div>
                <div class="group">
                    <div class="content">
                        <h3>Slip Cases</h3>
                        <p>Slip Cases are the perfect home for your Photo Book. Every Slip Case is individually handmade and comes in a selection of 10 material options. You can match your album cover to your Slip Case or make it stand out by choosing a contrasting colour palette. Great for gift giving and ideal for protecting your precious memories.</p>
                    </div>
                    <img src="img/photobooks/slipcase.jpg" height="387"/>
                </div>
                <div class="group">
                    <div class="content">
                        <h3>Presentation Boxes</h3>
                        <p>Our Presentation Boxes are individually handmade and are the ideal home for your precious keepsake. Each Presentation Box is tailored to fit your Photo Book snugly while the thoughtful ribbon pull provides convenient, lift-out access to your album. Choose from 10 colour material options.</p>
                    </div>
                    <img src="img/photobooks/pres-box.jpg" height="387" />
                </div>
            </div>
        </section>
        <section class="static group semimodules">
            <div class="semimodule" style="background-image:url(img/calcicon.jpg)">
                <h2>Pricing</h2>
                <p>Take out the guess work with our nifty pricing calculator. Add optional extras or change your paper type - our calculator does all the work for you!</p>
                <p><a href="calculator" class="cta">CALCULATE</a></p>
            </div>
            <div class="semimodule" style="background-image:url(img/contact.jpg)">
                <h2>Contact</h2>
                <p>Still have questions? Ask us! Our amazing Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible.</p>
                <p><a href="contact" class="cta">CONTACT</a></p>
            </div>
        </section>
    </main>

    <script type="text/javascript">
        $(document).ready(function(){
            if(!sessionStorage.getItem('seen_photogifts_popup')){
                window.setTimeout(function(){
                    sessionStorage.setItem("seen_photogifts_popup","true");
                    launch_popup_lightbox();
                }, 14000);
            }            
        });
    </script>
    
@endsection
