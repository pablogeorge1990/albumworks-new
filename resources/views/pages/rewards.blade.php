@extends('templates.header')

@section('title', 'albumworks Rewards: Earn Rewards dollars to spend with albumworks')
@section('meta_description', 'Earn Rewards dollars when you purchase products with us. Find out how your can start earning with albumworks.')

@section('body')

    <main class="normal static">
    <h1 style="color:#f3ac1e"><em>albumworks</em> Rewards</h1>
    <div class="rewards">
        <form id="rewardForm" class="form-validate bigform" name="rewardform" onsubmit="return validaterewards()">
            <p style="width:100%; max-width:400px; margin:0 auto;">
                <label for="contact_email">Email:</label>
                <input id="contact_email" class="inputbox required validate-email" name="email" type="text" style="width:calc(100% - 20px)" /> 
            </p>
            <p style="text-align:center; margin-top:1em;">
                <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="cta">CHECK YOUR BALANCE &amp; LEVEL</a>
            </p>
        </form>                        
    </div>
    
    <div class="group leftright">
        <div class="group">
            <div class="content">
                <h3>We believe in giving back</h3>
                <p>Want to be spoilt with exclusive offers and Rewards dollars? Of course you do! Our <em>albumworks Rewards</em> program is our way of giving back to our wonderful customers. As a Rewards member, you will accumulate exclusive Rewards dollars that can be applied to any order on any product.</p>
            </div>
            <img src="img/rewards/01.jpg" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>More orders = more rewards</h3>
                <p>Get more bang for your buck! As an <em>albumworks Rewards</em> member, for every $100 you spend you will receive Rewards dollars (depending on your member level) to spend on your next order. The more you spend, the more Rewards dollars you earn!</p>
            </div>
            <img src="img/rewards/02.jpg" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Exclusive rewards for members</h3>
                <p>All you have to do is spend over $50 and you will automatically become an <em>albumworks Rewards</em> member. As a member, you'll receive exclusive offers not available to the public. Be first in line for the best of our offers!</p>
            </div>
            <img src="img/rewards/03.jpg" height="380" />
        </div>
    </div>    

    <div>
        <h2 style="font-size:22px;">Learn more about <em>albumworks Rewards</em>:</h2>
        <h4>How does it work?</h4>
        <p>Spend $50 or more and you are automatically in the Club. You start earning Rewards Dollars when you have spent more than a total of $125 with us.  It's that simple.</p>
        <p>Once you are in the Rewards Club you earn Rewards dollars for every $100 you spend. The amount of dollars you earn depends on what level you are.  The more you have spent, the bigger are the rewards that you earn.</p>
        
        <h4>The levels of Rewards are:</h4>
        <div style="background:url({{env('BASEPATH')}}img/rewards/white.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left; min-height:36px;">
            <h3 style="margin:0; font-weight:400; text-align:left">White member</h3>
            <h4 style="margin:0; font-size:14px; font-weight:700">Spent less than $125</h4>
        </div>
        <div style="background:url({{env('BASEPATH')}}img/rewards/blue.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left; min-height:36px;">
            <h3 style="margin:0; font-weight:400; text-align:left">Blue member</h3>
            <h4 style="margin:0; font-size:14px; font-weight:700">Spent at least $125</h4>
            <h4 style="margin:0; font-size:14px; font-weight:300; font-style: italic;">Get $10 for every $100 spent as a Rewards Club member</h4>
        </div>
        <div style="background:url({{env('BASEPATH')}}img/rewards/silver.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left; min-height:36px;">
            <h3 style="margin:0; font-weight:400; text-align:left">Silver member</h3>
            <h4 style="margin:0; font-size:14px; font-weight:700">Spent at least $1,000</h4>
            <h4 style="margin:0; font-size:14px; font-weight:300; font-style: italic;">Get $15 for every $100 spent as a Rewards Club member</h4>
        </div>
        <div style="background:url({{env('BASEPATH')}}img/rewards/gold.png) left 4px no-repeat; padding-left:120px; margin-bottom:20px; text-align: left; min-height:36px;">
            <h3 style="margin:0; font-weight:400; text-align:left">Gold member</h3>
            <h4 style="margin:0; font-size:14px; font-weight:700">Spent at least $2,000</h4>
            <h4 style="margin:0; font-size:14px; font-weight:300; font-style: italic;">Get $20 for every $100 spent as a Rewards Club member</h4>
        </div>         
        
        <h4>What if I have spent $99 on my order, do I still earn Rewards dollars?</h4>
        <p>Once you are in our Rewards Club, we start tallying your spend as a Rewards Club member. As a Rewards Club member, if you spend $99 you will not accrue any Rewards dollars. However you only need to spend $1 or more on your next order to pass over the $100 threshold and earn your Rewards dollars!</p>
        <p>The only time this is not the case is when you are at White Level, as you must spend $125 to reach Blue Level. So if your first order is $100, you must spend another $25 to reach the Blue Level, and become eligible to start earning Rewards dollars on your orders.</p>

        <h4>Can I use promotions and discounts with my Rewards dollars?</h4>
        <p>You sure can! You will be able to use your Rewards dollars for all our promotions, unless we explicitly state that Rewards dollars are exempted on that particular promotion. Exempted promotions will be rare, and you will be able to use your dollars on most campaigns.</p>

        <h4>Does the program count my discount or the actual Rewards dollars spent?</h4>
        <p>The program will only count the dollars you actually spend, not the discounted amount. Eg. If you order a $100 Photo Book, but you get 20% off, we would only tally the $80 you actually spent towards your Rewards Club account.</p>

        <h4>Do my Rewards dollars expire?</h4>
        <p>As long as you purchase once in a calendar year (from either the launch of this Rewards Club, or your last purchase) your dollars will stay active and you will be able to continue to earn and use them. If you don't use your Rewards dollars within a year of earning them, they will expire.</p>

        <h4>How do I check my Rewards dollars balance?</h4>
        <p>If you have a registered email account with <em>albumworks</em>, you can check your balance using the login form at the top of the page.</p>
        
        <h4>What can I use my Rewards dollars on?</h4>
        <p>You can use your Rewards dollars on any product in our range! You can also use it for upgrading to HD, adding finishing options, getting more copies - whatever you want.</p>

        <h4>How do I claim my Rewards dollars?</h4>
        <p>Your Rewards dollars will be automatically added to your account. So just create and order a product as you always do, and your dollars will be in the Shopping Cart when you check out. You can choose not to use them by removing them from the cart for a particular purchase. They will then remain in your account, and be accrued for your next purchase.</p>

        <h4>Can I transfer my Rewards dollars?</h4>
        <p>Only you can use your Rewards dollars. They cannot be transferred to other people, or between two of your own accounts.</p>

        <h4>I am part of a company, or organisation. Can we earn points?</h4>
        <p>No. Our Rewards Club is restricted to individuals.</p>
         
        <p>View full <a href="{{env('BASEPATH')}}rewards-terms">Terms &amp; Conditions here</a></p>
    </div>
</main>

@endsection