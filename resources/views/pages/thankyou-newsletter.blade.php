@extends('templates.header')

@section('body')

    <main class="normal textonly">
    <h1>Thank you!</h1>
    <p>We have added you to our mailing list!</p>
</main>

@endsection