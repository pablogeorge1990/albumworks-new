@extends('templates.header')

@section('body')
    <main class="normal static" id="contactpage">
    <h1>Contact Us</h1>
    <!-- <div class="emgbanner">
        <p><strong>COVID-19 UPDATE:</strong></p>
        <p>OUR SHIPPING PARTNERS ARE DOING THEIR VERY BEST AT THIS TOUGH TIME. <br>
            WE HAVE RECEIVED SOME REPORTS OF MULTIPLE DAY DELAYS IN DELIVERY. <br>
            PLEASE BE AWARE OF THIS WHEN ORDERING.</p>
    </div> -->
    <div class="group marquee">
        <div class="content">     
            <dl>
                <dt>Hours:</dt>
                <dd>
                    <p>9:00am - 5:00pm AEST | Monday to Friday</p>
                </dd>
                
                <dt>Call us:</dt>
                <dd>
                    <p>1300 553 448</p><br>
                    <p>We may use the following number to call customers:</p>
                    <p>03 9999 1800</p>
                </dd>
                
                <dt>Live Chat:</dt>
                <dd>
                    <p>Message us during our Service Hours and we'll instantly message you back.</p>
                </dd>

                <dt>Bulk Order:</dt>
                <dd>
                    <p>Looking to place a bulk order? <a href="bulk">Click here</a> to fill out our bulk order enquiry form and we'll get back to you as soon as possible</p>
                </dd>
                
                <dt>Join our mailing list:</dt>
                <dd>
                    <form class="mailinglist bigform webtolead" action="#" method="post" onsubmit="return checkmlform(this)">
                        <input type="hidden" value="00D36000000oZE6" name="sfga">
                        <input type="hidden" value="00D36000000oZE6" name="oid">
                        <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&ret=thankyou-newsletter" name="retURL">
                        <input type="hidden" value="Website" name="lead_source">
                        <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                        <input type="hidden" value="Newsletter Signup" name="00N3600000BOyGd">
                        <input type="hidden" value="AP" name="00N3600000BOyAt">
                        <input type="hidden" value="PG" name="00N3600000Loh5K">
                        <input type="hidden" name="00N3600000Los6F" value="Windows">
                        <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
                        <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                        <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                        <input type="checkbox" checked="checked" value="1" name="emailOptOut" class="check" style="display:none">

                        <p>
                            <input type="text" class="inputbox" value="" id="field_first_name" name="first_name" placeholder="Name" />
                        </p>
                        <p>
                            <input type="text" class="inputbox" value="" id="field_email" name="email" placeholder="Email address" />
                        </p>
                        <p>
                            <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">SUBMIT</a>
                        </p>
                    </form>                    
                </dd>

                <dt>Follow us on our social pages </dt>
                <dd>
                    <a href="https://www.facebook.com/albumworks" target="_blank"><img src="{{asset('img/fb.png')}}" /></a>
                    &nbsp;
                    <a href="https://twitter.com/albumworks" target="_blank"><img src="{{asset('img/twit.png')}}" /></a>
                    &nbsp;
                    <a href="https://www.instagram.com/albumworks" target="_blank"><img src="{{asset('img/insta.png')}}" /></a>
                    &nbsp;
                    <a href="https://www.youtube.com/user/albumworks" target="_blank"><img src="{{asset('img/youtube.png')}}" /></a>
                </dd>

                <dt>View our Webinars</dt>
                <dd>
                    <p>Handy tips on our editor and products.</p>
                    <p>    
                        <a href="https://www.youtube.com/user/albumworks/videos" class="cta">WEBINARS</a>             
                    </p>
                </dd>

                <dt>Book an Editor intro</dt>
                <dd>
                    <p>Book a 15 minute phone call with one of our Customer Service team. We’ll answer any key
                        questions you have about our Editor.
                    </p>
                    <p>
                        <a class="cta" href="http://pictureworks.simplybook.me/v2/">BOOK NOW</a>
                    </p>
                </dd>

                <dt>Send us a message:</dt>
                <dd>
                    <form action="endpoints/mailform.php" method="post" class="bigform noempties norobot">
                        <input type="hidden" name="email__returl" value="thankyou-contact" />
                        <input type="hidden" name="email__body" value="dump" />
                        <input type="hidden" name="email__subject" value="Web Query" />
                        <input type="hidden" name="email__to" value="awservice" />

                        <p>
                            <select name="reason" id="contact_reason" class="inputbox canempty" onchange="if($(this).val() == 'I would like a paper sampler') $('.papersampler').show(); else $('.papersampler').hide();">
                                <option value="">Please Select a Reason</option>
                                <option value="I would like a paper sampler">I would like a paper sampler</option>
                                <option value="I would like help using the Editor">I would like help using the Editor</option>
                                <option value="I have a question about your products">I have a question about your products</option>
                                <option value="I have a question about delivery">I have a question about delivery</option>
                                <option value="I have an Apple Photobook I want to print">I have an Apple Photobook I want to print</option>
                                <option value="I have a question about your prices ">I have a question about your prices </option>
                                <option value="I have a question about your current promotions">I have a question about your current promotions</option>
                                <option value="I need help with a voucher code">I need help with a voucher code</option>
                                <option value="I need help with my account/login details">I need help with my account/login details</option>
                                <option value="Other">Other</option>
                            </select>
                        </p>
                        <div class="papersampler">
                            <p>A sample of our paper types will be posted to you so you can experience the difference between our paper types and printing qualities - SD and HD. This will assist you in making the right choice for your finished product.</p>
                            <p>Should you wish to have our monitor calibration sheets, please specify this.</p>
                            <p>Please fill out the form below and we will post our samples to you:</p>
                        </div>
                        <p>
                            <label for="field_name">Name:</label>
                            <input type="text" name="name" id="field_name" class="inputbox" />
                        </p>
                        <p>
                            <label for="field_email">Email:</label>
                            <input type="text" name="email" id="field_email" class="inputbox" />
                        </p>
                        <p class="papersampler">
                            <label for="field_address">Postal Address:</label>
                            <input type="text" name="address" id="field_address" class="inputbox canempty" />
                        </p>
                        <p>
                            <label for="message">Message:</label>
                            <textarea id="message" class="inputbox canempty" cols="50" name="text" rows="10"></textarea>
                        </p>

                        <p>
                            <div class="g-recaptcha" data-sitekey="6LcHNrsZAAAAAJF0hLoCNFils86kV9ik3TG5mXVr"></div>
                        </p>

                        <p>
                            <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">SUBMIT</a>
                        </p>
                    </form>

                    <? if(isset($_GET['apple'])): ?>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('html, body').animate({scrollTop: $("#contact_reason").offset().top - 20}, 400);
                                window.setTimeout(function(){
                                    $('#contact_reason').val("I'm trying to make an Apple Photobook");
                                    $('#field_name').focus();
                                }, 300);
                            });
                        </script>
                    <? endif; ?>
                </dd>
            </dl>
        </div>
        <img src="img/contact/01.jpg" class="nomobile" />
    </div>
</main>


@endsection