@extends('templates.header')

@section('body')
    <main class="normal static">
        <div class="filtergallery group">
            <div class="nomobile filter">
                <h2>Select your Theme</h2>
                <fieldset class="group">
                    <label for="filter_everyday">Everyday</label>
                    <input type="checkbox" id="filter_everyday" value="everyday" />
                </fieldset>
                <fieldset class="group">
                    <label for="filter_travel">Travel</label>
                    <input type="checkbox" id="filter_travel" value="travel" />
                </fieldset>
                <fieldset class="group">
                    <label for="filter_family">Family</label>
                    <input type="checkbox" id="filter_family" value="family" />
                </fieldset>
                <fieldset class="group">
                    <label for="filter_children">Children</label>
                    <input type="checkbox" id="filter_children" value="children" />
                </fieldset>
                <fieldset class="group">
                    <label for="filter_wedding">Wedding</label>
                    <input type="checkbox" id="filter_wedding" value="wedding" />
                </fieldset>
                <fieldset class="group">
                    <label for="filter_love">Love</label>
                    <input type="checkbox" id="filter_love" value="love" />
                </fieldset>
                <fieldset class="group">
                    <label for="filter_memories">Memories</label>
                    <input type="checkbox" id="filter_memories" value="memories" />
                </fieldset>
            </div>
            <div class="gallery <?=@$_GET['cover']?>gallery">
                <h2>Photo Books</h2>
                <ul>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=contemporary'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/contemporary-COVER.jpg')}}" hover="{{asset('img/themes/contemporary-PAGES.jpg')}}" class="filter_everyday filter_family filter_memories"><p><strong>Contemporary</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=classic'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/classic-COVER.jpg')}}" hover="{{asset('img/themes/classic-PAGES.jpg')}}" class="filter_everyday filter_family filter_memories"><p><strong>Classic</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=blank'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/blank-COVER.jpg')}}" hover="{{asset('img/themes/blank-PAGES.jpg')}}" class="filter_everyday filter_travel filter_family filter_children filter_wedding filter_love filter_memories"><p><strong>Build Your Own Theme</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=modern-white'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/modern-white-COVER.jpg')}}" hover="{{asset('img/themes/modern-white-PAGES.jpg')}}" class="filter_everyday filter_travel filter_family filter_children filter_wedding filter_love filter_memories"><p><strong>Modern White</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=modern-black'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/modern-black-COVER.jpg')}}" hover="{{asset('img/themes/modern-black-PAGES.jpg')}}" class="filter_everyday filter_travel filter_family filter_children filter_wedding filter_love filter_memories"><p><strong>Modern Black</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=light-bright'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/light-bright-COVER.jpg')}}" hover="{{asset('img/themes/light-bright-PAGES.jpg')}}" class="filter_everyday filter_family filter_wedding filter_love filter_memories"><p><strong>Light and Bright</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=full-bleed'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/full-bleed-COVER.jpg')}}" hover="{{asset('img/themes/full-bleed-PAGES.jpg')}}" class="filter_everyday filter_travel filter_family filter_children filter_memories"><p><strong>Full Bleed</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=travel-scrapbook'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/travel-scrapbook-COVER.jpg')}}" hover="{{asset('img/themes/travel-scrapbook-PAGES.jpg')}}" class="filter_travel"><p><strong>Travel Scrapbook</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=travel-pano'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/travel-pano-COVER.jpg')}}" hover="{{asset('img/themes/travel-pano-PAGES.jpg')}}" class="filter_travel"><p><strong>Travel Panorama</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=travel-snapshots'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/travel-snapshots-COVER.jpg')}}" hover="{{asset('img/themes/travel-snapshots-PAGES.jpg')}}" class="filter_travel"><p><strong>Travel Snapshots</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=travel-collage'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/travel-collage-COVER.jpg')}}" hover="{{asset('img/themes/travel-collage-PAGES.jpg')}}" class="filter_travel"><p><strong>Travel Collage</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=black-borders'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/black-borders-COVER.jpg')}}" hover="{{asset('img/themes/black-borders-PAGES.jpg')}}" class="filter_everyday"><p><strong>Black Borders</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=white-borders'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/white-borders-COVER.jpg')}}" hover="{{asset('img/themes/white-borders-PAGES.jpg')}}" class="filter_everyday"><p><strong>White Borders</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=clustered'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/clustered-COVER.jpg')}}" hover="{{asset('img/themes/clustered-PAGES.jpg')}}" class="filter_everyday filter_travel filter_family"><p><strong>Clustered</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=memories'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/memories-COVER.jpg')}}" hover="{{asset('img/themes/memories-PAGES.jpg')}}" class="filter_everyday filter_travel filter_family filter_memories"><p><strong>Memories</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=a-lifes-story'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/a-lifes-story-COVER.jpg')}}" hover="{{asset('img/themes/a-lifes-story-PAGES.jpg')}}" class="filter_family filter_memories"><p><strong>A Life's Story</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=elegant-wedding'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/elegant-wedding-COVER.jpg')}}" hover="{{asset('img/themes/elegant-wedding-PAGES.jpg')}}" class="filter_wedding filter_love"><p><strong>Elegant Wedding</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=stylish-wedding'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/stylish-wedding-COVER.jpg')}}" hover="{{asset('img/themes/stylish-wedding-PAGES.jpg')}}" class="filter_wedding filter_love"><p><strong>Stylish Wedding</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=baby-boy'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/baby-boy-COVER.jpg')}}" hover="{{asset('img/themes/baby-boy-PAGES.jpg')}}" class="filter_family filter_children"><p><strong>Baby Boy</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=baby-girl'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/baby-girl-COVER.jpg')}}" hover="{{asset('img/themes/baby-girl-PAGES.jpg')}}" class="filter_family filter_children"><p><strong>Baby Girl</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=cherish'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/cherish-COVER.jpg')}}" hover="{{asset('img/themes/cherish-PAGES.jpg')}}" class="filter_children"><p><strong>Cherish</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=family-history'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/family-history-COVER.jpg')}}" hover="{{asset('img/themes/family-history-PAGES.jpg')}}" class="filter_family filter_memories"><p><strong>Family History</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=a-year-to'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/a-year-to-COVER.jpg')}}" hover="{{asset('img/themes/a-year-to-PAGES.jpg')}}" class="filter_everyday filter_family filter_memories"><p><strong>A Year to Remember</strong></p></li>
                    <li onclick="window.location = '{{env('BASEPATH')}}photo-book-picker?theme=my-pet'+'&size='+__size+'&cover='+__cover" default="{{asset('img/themes/my-pet-COVER.jpg')}}" hover="{{asset('img/themes/my-pet-PAGES.jpg')}}" class="filter_everyday filter_family filter_memories"><p><strong>My Pet</strong></p></li>
                </ul>
            </div>
        </div>
    </main>
    <script type="text/javascript" src="{{env('BASEPATH')}}js/picker.js?v5"></script>
    <script type="text/javascript">
        var __size = '11x85', __cover = 'photocover';
        $(document).ready(function(){
            window.setTimeout(function(){
                <? if(isset($_GET['theme'])): ?>
                    $('#filter_<?=$_GET['theme']?>').click().change();
                <? elseif(isset($_GET['size'])): ?>
                    __size = '<?=$_GET['size']?>';
                    if(__size === '16x12')
                        __cover = 'photocover';
                <? elseif(isset($_GET['cover'])): ?>
                    __cover = '<?=$_GET['cover']?>';
                    if(__cover === 'material')
                        __size = '12x12';
                    else if(__cover === 'softcover')
                        __size = '8x6';
                    else if(__cover === 'photocover')
                        __size = '8x8';
                    else if(__cover === 'deboss')
                        __size = '8x8';
                <? endif; ?>

                var price = typeof(__prices['classic|'+__size+'|'+__cover]) === 'undefined' ? '?' : __prices['classic|'+__size+'|'+__cover];
                if(price !== '?'){
                    $('span.price').text(price.toFixed(2));
                }
            }, 500);
        });
    </script>

@endsection