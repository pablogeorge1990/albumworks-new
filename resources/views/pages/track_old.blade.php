@extends('templates.header')

@section('body')

    <main class="normal static">
        <h1>Tracking</h1>
        <iframe id="portal_iframe" src="{{env('BASEPATH')}}aw-support-centre/portal/index.php?vendor=1031" width="100%" onload="$(this).height($(this.contentWindow.document.body).height());" frameborder="0"></iframe>
        <div class="group leftright">
            <div></div>
            <div class="group">
                <div class="content">
                    <h3>No need to wait</h3>
                    <p>All of our products are delivered via express courier so once your order has been dispatched, you should receive it within 2-3 business days in metropolitan areas.</p>
                    <p>We recommend giving yourself a full <span style="font-weight: bold;">7 business days</span> for production of your order to ensure delivery before any important event or occasion.. But we will work our hardest to give you a pleasant surprise - your product arriving sooner!</p>
                </div>
                <img src="{{asset('img/tracking/02.jpg')}}" height="380" />
            </div>
        </div>
    </main>

@endsection

