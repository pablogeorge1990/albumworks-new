@extends('templates.header')

@section('body')

<main class="blog group">
    <section id="blogmain">
        <h1><em>albumworks</em> Blog</h1>
        @foreach ($paginated_blogs as $blog)
            <div class="blogshort">
                <p class="date">{!! strtoupper(date('F jS, Y', strtotime($blog->publish_up))) !!}</p>
                <h2><a href="{{$blog->slug}}">{!!   $blog->title !!}</a></h2>
                <div class="intro">
                    {!!   $blog->introtext !!}
                </div>
                <p class="readmore"><a href="{{$blog->slug}}">READ MORE &gt;</a></p>
            </div>
        @endforeach
        {{ $paginated_blogs->links() }}
    </section>
    
    <div id="mostread" class="blogasset nomobile">
        <h4>RECENT</h4>
        <ul>
        @foreach ($newest_blogs as $popular)
            <li><a href="{{$popular['slug']}}">{!! $popular['title'] !!}</a></li>
        @endforeach
        </ul>
    </div>
</main>


@endsection