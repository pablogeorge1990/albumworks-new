@extends('templates.header')

@section('title', 'Promotions: Stay up to date with albumworks promotions')
@section('meta_description', 'Current promotions with albumworks. Great savings on Photo Books and photo products. 100% design freedom, fast delivery and local support.')

@section('body')

    <main class="normal static">
        <h1>Promotions</h1>

        <div class="stripform">
            <form class="mailinglist webtolead" action="#" method="post" onsubmit="return checkmlform(this)">
                <input type="hidden" value="00D36000000oZE6" name="sfga">
                <input type="hidden" value="00D36000000oZE6" name="oid">
                <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&ret=thankyou-newsletter" name="retURL">
                <input type="hidden" value="Website" name="lead_source">
                <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                <input type="hidden" value="Newsletter Signup" name="00N3600000BOyGd">
                <input type="hidden" value="AP" name="00N3600000BOyAt">
                <input type="hidden" value="PG" name="00N3600000Loh5K">
                <input type="hidden" name="00N3600000Los6F" value="Windows">
                <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
                <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                <input type="checkbox" checked="checked" value="1" name="emailOptOut" class="check" style="display:none">

                <p style="display:none">
                    <label for="field1_first_name">Name:</label>
                    <input type="text" class="inputbox" value="Name" id="field1_first_name" name="first_name" value="Name" />
                </p>

                <p>New to <em>albumworks</em>? Sign up today to receive your first order discount!</p>
                <p>
                    <label for="field1_email">Email:</label>
                    <input type="text" class="inputbox" value="" id="field1_email" name="email" />
                    <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">SUBMIT</a>
                </p>
            </form>
        </div>

        <div class="promotions group">
            <?php
            $promotions = [
                [
                    'heading' => 'Capture Your Story – 35% OFF all products',
                    'body' => 'Narrate your unique journey with our beautifully crafted photo books and take 35% off your product total including extra pages and accessories. Transform your favourite memories into a tangible keepsake, perfect for sharing with loved ones or simply for revisiting on a cosy evening. Our exceptional print quality will ensure that your story is brought to life in vivid detail, creating a priceless collection of moments that will stand the test of time.
<br><br>
*35% discount is valid for any albumworks product (excluding Gift Vouchers). Voucher Code MYSTORY must be entered at time of ordering and discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 14 JUNE 2023 (11:59PM AEST)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/mystory',
                    'img' => '20230519/promo.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,5,25,2023),
                    'off' => mktime(0,0,0,6,15,2023),
                    'voucher' => 'MYSTORY',
                ],

                [
                    'heading' => 'Spend More, Save More - up to 40% OFF',
                    'body' => 'It\'s our 17th birthday!  Celebrate with more memories and more savings.<br><br>

Make any albumworks product by 5th July and we’ll give you the following discount: <br><br>

Spend over $50, get 25% off*<br>
Spend over $100, get 33% off*<br>
Spend over $150, get 40% off*<br><br>

*Discount will be applied to the product total only - shipping will be charged at the full price.<br><br>

Valid for any albumworks product except Gift Vouchers.  Not valid with any other Voucher Code.<br><br>

Simply use the Voucher Code below when placing your order.<br><br>',
                    'deadline' => 'Wednesday 5th July, 2023 (11:59PM AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/17birthday',
                    'img' => '20230608/promo1.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,6,15,2023),
                    'off' => mktime(0,0,0,7,6,2023),
                    'voucher' => '17BIRTHDAY',
                ],
                [
                    'heading' => 'WIN 1 of 5 $250 Vouchers!',
                    'body' => 'We\'re giving away 5 albumworks Vouchers valued at $250 each! Help us celebrate our 17th birthday by sharing a special birthday moment, and you could WIN!<br><br>

Pick out your favourite birthday photo and enter now!<br><br>',
                    'deadline' => 'Wednesday 5th February, 2023 (11:59PM AEDT)',
                    'linklabel' => 'ENTER NOW',
                    'link' => 'https://woobox.com/rsywga',
                    'img' => '20230608/promo2.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,6,15,2023),
                    'off' => mktime(0,0,0,7,6,2023),
                    'voucher' => '',
                ],

                [
                    'heading' => 'Bigger Books = Bigger Savings – up to 40% Off*',
                    'body' => 'Are you tired of scrolling through endless digital photos on your phone or computer?   Then it’s time to create a beautiful photo book to showcase your favourite moments, whether it\'s a heartwarming family album, an enchanting travel journey, or a compilation of life\'s milestones.   Take 33% off or if your product total is more than $150 enjoy an incredible 40% discount.
<br><br>
*Discount is valid for any albumworks product (excluding Gift Vouchers). Voucher Code GOBIGGER must be entered at time of ordering and discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 24 MAY 2023 (11:59PM AEST)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/biggerbooks2023',
                    'img' => '20230427/promo.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,5,2,2023),
                    'off' => mktime(0,0,0,5,25,2023),
                    'voucher' => 'GOBIGGER',
                ],

                [
                    'heading' => 'Mother’s Day Special: 38% off* all products!',
                    'body' => 'Make any albumworks product by our Mother’s Day order deadline, and we’ll give you a 38% discount off the product total!  Design your product(s) using our download Editor or online Editor. Then simply use the Voucher Code below in the shopping cart when ordering to get your discount!<br><br>
38% off discount is valid for any albumworks product (excluding Gift Vouchers).  Voucher code LOVEMUM2023 must be entered at time of ordering.<br><br>
*Discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'Monday 1st May, 2023 (11:59PM AEST)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/lovemum2023',
                    'img' => '20230330/promo1.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,4,6,2023),
                    'off' => mktime(0,0,0,5,2,2023),
                    'voucher' => 'LOVEMUM2023',
                ],
                [
                    'heading' => 'WIN: 1 of 4 Weekly Prizes!',
                    'body' => 'We\'re giving away 4 x $150 albumworks vouchers this Mother\'s Day.  Share a photo of your Mum and you could win! Here’s how to enter:<br><br>
1. Follow <a href="https://www.instagram.com/albumworks/" target="_blank">our Instagram Page</a> or Like <a href="https://www.facebook.com/albumworks" target="_blank">our Facebook Page</a><br>
2. Post your “Mum Moment” photo<br>
3. Tag your photo with #albumworksmum<br><br>
That’s it! We’ll select 1 winner each week until the competition ends on Monday 1st May!<br><br>',
                    'deadline' => 'Monday 1st May, 2023 (11:59PM AEST)',
                    'linklabel' => 'ENTER VIA INSTAGRAM',
                    'link' => 'https://www.instagram.com/albumworks/',
                    'linklabel2' => 'ENTER VIA FACEBOOK',
                    'link2' => 'https://www.facebook.com/albumworks',
                    'img' => '20230330/promo3.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,4,6,2023),
                    'off' => mktime(0,0,0,5,2,2023),
                    'voucher' => '',
                ],

                [
                    'heading' => 'Our best books at the best prices – 40% off” Layflat Photo Books',
                    'body' => 'Layflat photo books are the perfect way to preserve and showcase your most cherished memories, whether it be commemorating a special occasion, creating a family album, or showcasing your photography.  With seamless, durable binding and thick photo quality paper, Layflat books allow you to display your favorite photos in stunning detail and full-spread layouts. <br><br>
Now available in beautiful 7 colour printing adding exceptional vibrancy to your images.
<br><br>
*40% off discount is valid for any Layflat Photo Book. Voucher Code LAYFLATISBEST must be entered at time of ordering and 40% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 5 APRIL 2023 (11:59PM AEST)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/layflat-is-best',
                    'img' => '20230309/promo1.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,3,16,2023),
                    'off' => mktime(0,0,0,4,6,2023),
                    'voucher' => 'LAYFLATISBEST',
                ],
                [
                    'heading' => 'Our best books at the best prices – 35% off* Classic Photo Books',
                    'body' => 'Create any Classic Photo Book and get 35% off your product total!
<br><br>
*Voucher Code CLASSICISBEST must be entered at time of ordering and 35% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 5 APRIL 2023 (11:59PM AEST)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/classic-is-best',
                    'img' => '20230309/promo2.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,3,16,2023),
                    'off' => mktime(0,0,0,4,6,2023),
                    'voucher' => 'CLASSICISBEST',
                ],
                [
                    'heading' => 'Yearbook Special - 35% OFF* all Photo Books',
                    'body' => 'Creating a yearbook is a wonderful way to tell your story and capture those special memories of the year, from birthday parties and family holidays to everyday moments and milestones. Not only is it a beautiful keepsake to treasure for years to come, but it\'s also the perfect way to share your family\'s stories with future generations.<br><br>
<br><br>
*35% off discount is valid for any Photo Book. Voucher Code MYYEARBOOK must be entered at time of ordering and 35% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 15 MARCH 2023 (11:59PM AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/year-book',
                    'img' => '20230216/promo1.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,2,23,2023),
                    'off' => mktime(0,0,0,3,16,2023),
                    'voucher' => 'MYYEARBOOK',
                ],
                [
                    'heading' => '40% off* all Calendars',
                    'body' => 'Stay organized and ahead of schedule in 2023 with our stylish wall and desk calendars - now 40% OFF! Order now and start planning a productive and successful year.<br><br>
<br><br>
*40% off discount is valid for any Calendar. Voucher Code 2023CALENDAR must be entered at time of ordering and 40% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 15 MARCH 2023 (11:59PM AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/2023calendar',
                    'img' => '20230216/promo2.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,2,23,2023),
                    'off' => mktime(0,0,0,3,16,2023),
                    'voucher' => '2023CALENDAR',
                ],
                [
                    'heading' => '35% off all products: EOY Sale!',
                    'body' => 'Celebrate the end of 2022 with 35% off all products! Make any albumworks product by 4th January and we’ll give you a 35% discount off your product total! Make your product(s) using either our download or online Editor. Then simply use the Voucher Code below in the shopping cart when ordering to get your discount! <br><br>*Discount will be applied to the product total only - shipping will be charged at the full price.<br><br>Valid for any albumworks product except Gift Vouchers.<br><br>',
                    'deadline' => 'Wednesday 4 January, 2023 (11:59PM AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/eoy2022',
                    'img' => '20221208/promo.jpg?v2',
                    'smallscript' => 'Not valid with other Voucher Codes',
                    'on' => mktime(0,0,0,12,13,2022),
                    'off' => mktime(0,0,0,1,5,2023),
                    'voucher' => 'EOY2022',
                ],
                [
                    'heading' => 'Christmas Special: 25% off all Gift Vouchers',
                    'body' => 'Our Christmas order deadline might have passed, but you can still get a Gift Voucher for your loved one delivered instantly to your inbox – right up until Christmas. Order now and you can get 25% OFF the price of your voucher! For example buy a $100 Gift Voucher and only pay $75.<br><br>',
                    'deadline' => 'Sunday 25 December, 2022 (11:59PM AEDT)',
                    'linklabel' => 'BUY NOW',
                    'link' => 'https://www.albumworks.com.au/vouchers',
                    'img' => '20221209/promo.jpg?v2',
                    'smallscript' => 'Gift vouchers are valid for 12 months from purchase date.',
                    'on' => mktime(0,0,0,12,15,2022),
                    'off' => mktime(0,0,0,12,26,2022),
                    'voucher' => '',
                ],
                [
                    'heading' => '40% off* SD Calendars - 7 Days Only',
                    'body' => 'Create any Standard Definition (SD) Wall or Desk Calendar and get 40% off your product total. Voucher Code SDCALENDARSALE must be entered at time of ordering. *40% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 9 NOVEMBER 2022, 11.59pm (AEST)',
                    'linklabel' => 'CREATE CALENDAR',
                    'link' => 'https://www.albumworks.com.au/wall-calendar',
                    'img' => '20221028/promo.jpg',
                    'smallscript' => '',
                    'on' => mktime(12,0,0,11,2,2022),
                    'off' => mktime(0,0,0,11,10,2022),
                    'voucher' => 'SDCALENDARSALE',
                ],
                [
                    'heading' => '40% OFF* any Softcover Photo Book',
                    'body' => 'Celebrate those smaller moments in a stunning Softcover Photo Book!<br><br>Sometimes life\'s important moments don\'t need one of our hardcover books.  For those occasions a Softcover book fits perfectly. It might be a week down the coast with your family, a football team\'s yearbook or a special anniversary party.  Don\'t let those photos disappear in the back of your hard drive.<br><br>*40% off discount is valid to any Softcover Photo Book.Voucher Code SOFTCOVER must be entered at time of ordering. Shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 2 NOVEMBER 2022, 11.59pm (AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/softcover',
                    'img' => '20221002/promo-sc.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,10,6,2022),
                    'off' => mktime(0,0,0,11,3,2022),
                    'voucher' => 'SOFTCOVER',
                ],
                [
                    'heading' => '38% off* any Canvas Print - 4 Days Only',
                    'body' => 'Create any Canvas Print and get 38% off your product total. Voucher Code CANVASSALE must be entered at time of ordering. *38% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'MON 24 OCTOBER 2022, 11.59pm (AEDT)',
                    'linklabel' => 'CREATE CANVAS',
                    'link' => 'https://www.albumworks.com.au/canvas-photo-prints-create-now',
                    'img' => '20221020/promo.jpg',
                    'smallscript' => 'Not valid with other Voucher Codes',
                    'on' => mktime(0,0,0,10,6,2022),
                    'off' => mktime(0,0,0,10,25,2022),
                    'voucher' => 'CANVASSALE',
                ],
                [
                    'heading' => '30% off* any Hardcover Photo Books',
                    'body' => 'Create any Hardcover Photo Book and get 30% off your product total.  Voucher Code HARDCOVER must be entered at time of ordering. *30% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 2 NOVEMBER 2022, 11.59pm (AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/softcover',
                    'img' => '20221002/promo-hc.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,10,6,2022),
                    'off' => mktime(0,0,0,11,3,2022),
                    'voucher' => 'HARDCOVER',
                ],

                [
                    'heading' => 'Treasure your BIG holiday adventures with 40% off* BIG books!',
                    'body' => 'Create a BIG Photo Book to preserve your BIG holiday memories and save 40%, including extra pages and accessories. Add pictures of all your favorite moments, whether it\'s lounging on the beach, exploring a new city, or enjoying time with loved ones… and add captions to rekindle the wonderful stories of your trip.    With extra large pages, you can bring your beautiful photos to life and relive your holiday experiences for years to come.<br><br>
                                *40% off discount is valid for any 16 x 12" Landscape, 14 x 10" Landscape or 12 x 12" Square sized Photo Book. Voucher Code BIGBOOKS2023 must be entered at time of ordering and 40% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 22 FEBRUARY 2023 (11:59PM AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/bigbooks2023',
                    'img' => '20230127/promo1.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,2,2,2023),
                    'off' => mktime(0,0,0,2,23,2023),
                    'voucher' => 'BIGBOOKS2023',
                ],
                [
                    'heading' => '33% off* all other Photo Book sizes',
                    'body' => 'Create any 11 x 8.5" Landscape, 8 x 11" Portrait, 8 x 8" Square or 8 x 6" Landscape sized Photo Book and get 33% off your product total!  <br><br>*Voucher Code OTHERBOOKS2023 must be entered at time of ordering and 33% discount will be applied to the product total only - shipping will be charged at the full price.<br><br>',
                    'deadline' => 'WED 22 FEBRUARY 2023 (11:59PM AEDT)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/bigbooks2023',
                    'img' => '20230127/promo2.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,2,2,2023),
                    'off' => mktime(0,0,0,2,23,2023),
                    'voucher' => 'OTHERBOOKS2023',
                ],

                [
                    'heading' => 'Celebrate Life’s Important Moments!',
                    'body' => 'Rekindle those so-very-important memories and celebrate those special occasions - overseas holidays, significant birthdays, family weddings...or a picnic with friends by creating a gorgeous Photo Book and live those moments again.<br><br>
                                Save 30% OFF your next albumworks order by using the Voucher Code below.<br><br>
                                * Valid for all albumworks products - not including Gift Vouchers.  Shipping is not included in the product total and will be charged at the full price.<br><br>',
                    'deadline' => 'WED 14 SEPTEMBER 2022. 11.59pm (AEST)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/memories2022',
                    'img' => '20220822/promo.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,8,24,2022),
                    'off' => mktime(0,0,0,9,15,2022),
                    'voucher' => 'MEMORIES2022',
                ],
                [
                    'heading' => '33% OFF any product!',
                    'body' => 'Make any albumworks product by Aug 23rd and we’ll give you a 33% discount off your product total! Make your product(s) using our download Editor or online Editor. Then simply use the Voucher Code below in the shopping cart when ordering to get your discount!<br><br>* Valid for all albumworks products - not including Gift Vouchers.  Shipping is not included in the product total and will be charged at the full price.<br><br>',
                    'deadline' => 'TUE 23 AUGUST 2022, 11.59pm (AEST)',
                    'linklabel' => 'HOW TO CLAIM',
                    'link' => 'https://www.albumworks.com.au/loveyoudad',
                    'img' => '20220802/promo.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,8,4,2022),
                    'off' => mktime(0,0,0,8,24,2022),
                    'voucher' => 'LOVEYOUDAD',
                ],
                [
                    'heading' => '11 x 8.5" SD Photocover - from $53.50',
                    'body' => 'Standard Definition Photo Book Vouchers<br>
Buy Now & Make Later – 6 months to make.<br>
<br>
- 11 x 8.5" Landscape<br>
- 40 pages, 70 pages or 100 pages<br>
- SD printing<br>
- Hard photocover<br><br>',
                    'deadline' => 'Wed 10 Aug, 2022 (11:59PM AEST)',
                    'linklabel' => 'BUY VOUCHER',
                    'link' => 'https://www.albumworks.com.au/directsale-jul22a',
                    'img' => '20220721/01.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,6,30,2022),
                    'off' => mktime(0,0,0,8,11,2022),
                    'voucher' => '',
                ],
                [
                    'heading' => '12 x 12" SD Photocover - from $66.95',
                    'body' => 'Standard Definition Photo Book Vouchers<br>
Buy Now & Make Later – 6 months to make.<br>
<br>
- 12 x 12" Square<br>
- 40 pages, 70 pages or 100 pages<br>
- SD printing<br>
- Hard photocover<br><br>',
                    'deadline' => 'Wed 10 Aug, 2022 (11:59PM AEST)',
                    'linklabel' => 'BUY VOUCHER',
                    'link' => 'https://www.albumworks.com.au/directsale-jul22b',
                    'img' => '20220721/02.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,6,30,2022),
                    'off' => mktime(0,0,0,8,11,2022),
                    'voucher' => '',
                ],
                [
                    'heading' => '16 x 12" SD Photocover - from $87.50',
                    'body' => 'Standard Definition Photo Book Vouchers<br>
Buy Now & Make Later – 6 months to make.<br>
<br>
- 16 x 12" Landscape<br>
- 40 pages, 70 pages or 100 pages<br>
- SD printing<br>
- Hard photocover<br><br>',
                    'deadline' => 'Wed 10 Aug, 2022 (11:59PM AEST)',
                    'linklabel' => 'BUY VOUCHER',
                    'link' => 'https://www.albumworks.com.au/directsale-jul22c',
                    'img' => '20220721/03.jpg',
                    'smallscript' => '',
                    'on' => mktime(0,0,0,6,30,2022),
                    'off' => mktime(0,0,0,8,11,2022),
                    'voucher' => '',
                ],
            ];

            foreach($promotions as $promo){
            if(isset($_GET['sjb']) or ($promo['on'] < time() and $promo['off'] > time())){
            ?>
            <div class="promotion">
                <img src="img/promo/<?=$promo['img']?>" />
                <div class="heading">
                    <h3><?=$promo['heading']?></h3>
                    <p style="display:none"><a href="javascript:void(0)" onclick="$(this).parent().next().next().slideToggle(200); $(this).parent().hide(); $(this).parent().next().show();">View less information &#x25B2;</a></p>
                    <p><a href="javascript:void(0)" onclick="$(this).parent().next().slideToggle(200); $(this).parent().hide(); $(this).parent().prev().show();">View more information &#x25BC;</a></p>
                    <p style="display:none"><?=$promo['body']?></p>
                    <p>Not valid with other Voucher Codes</p>
                </div>
                <div class="body">
                    <? if($promo['voucher']): ?>
                    <h3>Voucher Code:</h3>
                    <p>
                        <span class="code"><?=$promo['voucher']?></span>
                    </p>
                    <? endif; ?>

                    <h3>Offer deadline:</h3>
                    <p><?=$promo['deadline']?></p>

                    <p>
                        <a href="<?=$promo['link']?>" class="cta"><?=$promo['linklabel']?></a>
                        <? if(@$promo['link2']): ?>
                            <a href="<?=$promo['link2']?>" class="cta"><?=$promo['linklabel2']?></a>
                        <? endif; ?>
                    </p>

                    <? if($promo['smallscript']): ?>
                    <p class="smaller"><?=$promo['smallscript']?></p>
                    <? endif; ?>
                </div>
            </div>
            <?
            }
            }
            ?>
        </div>

        <section class="static group semimodules">
            <div class="semimodule-2">
                <h2 style="font-size:27px;">Webinars + tutorials</h2>
                <a href="https://www.gotostage.com/channel/a8113601621940fc82fed57d50d29db2"><img src="{{asset('img/home/01-webinar.jpg')}}" alt="webinar" class="image-adjust"/></a>
                <p>Want to become an expert at using our desktop Editor? Our webinar series aims to help you learn the ropes of our easy-to-use Editor. From photo editing, to ordering a Photo Book, we're covering all you need to know!</p>
                <p><a href="https://www.youtube.com/user/albumworks/videos" class="cta">VIEW WEBINARS</a></p>
            </div>
            <div class="semimodule-2">
                <h2 style="font-size:27px;">Stay connected</h2>
                <img src="{{asset('img/home/02-social.jpg')}}" alt="socialmedia" class="image-adjust"/>
                <p>Join our community of photo lovers. We‘ve been producing Photo Books for more than 16 years and love sharing every part of the journey. Follow our social media channels to stay up to date with the latest news, savings and offers!</p>
                <p style="margin-top:34px;"><a href="https://www.facebook.com/albumworks"><img src="{{asset('img/home/03-fb.png')}}" alt="facebook" style="width:140px;"/></a><a href="https://www.instagram.com/albumworks"><img src="{{asset('img/home/04-insta.png')}}" alt="instagram" style="width:140px; padding-left:20px;"/></a></p>
            </div>
        </section>

        <section class="static group semimodules" style="margin-top:0;">
            <div class="semimodule">
                <h2>Join the family</h2>
                <p>Don't want to miss out? You don't have to! Sign up to receive our newsletter, exclusive offers and all of our promotions. Great deals on Photo Books and more!</p>
                <form class="mailinglist webtolead" action="#" method="post" onsubmit="return checkmlform(this)">
                    <input type="hidden" value="00D36000000oZE6" name="sfga">
                    <input type="hidden" value="00D36000000oZE6" name="oid">
                    <input type="hidden" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&ret=thankyou-newsletter" name="retURL">
                    <input type="hidden" value="Website" name="lead_source">
                    <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                    <input type="hidden" value="Newsletter Signup" name="00N3600000BOyGd">
                    <input type="hidden" value="AP" name="00N3600000BOyAt">
                    <input type="hidden" value="PG" name="00N3600000Loh5K">
                    <input type="hidden" name="00N3600000Los6F" value="Windows">
                    <!-- Referring Promotion --><input type="hidden" value="" name="00N3600000LosAC">
                    <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                    <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                    <input type="checkbox" checked="checked" value="1" name="emailOptOut" class="check" style="display:none">

                    <p style="display:none">
                        <label for="field2_first_name">Name:</label>
                        <input type="text" class="inputbox" value="Name" id="field2_first_name" name="first_name" value="Name" />
                    </p>
                    <p>
                        <label for="field2_email">Email:</label>
                        <input type="text" class="inputbox" value="" id="field2_email" name="email" />
                    </p>
                    <p>
                        <a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">SUBMIT</a>
                    </p>
                </form>
            </div>
            <div class="semimodule" style="background-image:url(img/contact.jpg)">
                <h2>Contact</h2>
                <p>Still have questions? Ask us! Our amazing Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible.</p>
                <p><a href="contact" class="cta">CONTACT</a></p>
            </div>
        </section>
    </main>

@endsection
