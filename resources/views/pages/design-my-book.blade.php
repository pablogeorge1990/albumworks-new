@extends('templates.header')

@section('css')
<link rel="stylesheet" type="text/css" media="screen" href="{{env('BASEPATH')}}css/bootstrap-grid.min.css" />
@endsection

@section('body')
<main class="">
	<h1 class="my-header">Design My Book</h1>
	<div class="row">
		<div class="col-md-5">
			<h2 class="my-header">Let us create your Photo Book for you!</h2>
			<p>Selecting photos, ordering your images, making page layouts can take some time. Why not leave it with our Photo Book design experts to create and design your Photo Book for you!</p>
			<h2 class="my-header2">Design service fee:</h2>
			<p class="price-text"><b><s>$69.95</s></b> $35.00</p>
			<br>
			<!-- Need Links -->
			<p><a href="http://dmb.albumworks.com.au/" class="cta">DESIGN MY BOOK</a></p>
		</div>
		<div class="col-md-7">
			<br>
			<br>
			<img src="{{asset('img/design-my-book/01.jpg')}}" height="329" />
		</div>
	</div>
	<br>
	<br>
	<br>
	<div class="row">
		<div class="col-md-4 text-center">
			<img src="{{asset('img/design-my-book/02.jpg')}}" height="134" />
			<p class="small-header">CHOOSE YOUR PHOTOS & STYLE</p>
			<br>
			<p>Select a book size and style. Upload photos or entire albums, select favourite photos and add text your images.</p>
		</div>
		<div class="col-md-4 text-center">
			<img src="{{asset('img/design-my-book/03.jpg')}}" height="134" />
			<p class="small-header">WE’LL MAKE IT FOR YOU</p>
			<br>
			<p>Our design team will curate your photos and create your custom book in 3 business days. We'll keep you updated and email you when it's ready to review.</p>
		</div>
		<div class="col-md-4 text-center">
			<img src="{{asset('img/design-my-book/04.jpg')}}" height="134" />
			<p class="small-header">REVIEW & ORDER</p>
			<br>
			<p>You will receive one free round of amendments. When you’ve approved your final design, submit and order your book</p>
		</div>
	</div>
	<br>
	<br>
	<br>
	<div class="row">
		<div class="col-md-12 text-center">
			<!-- Need Links -->
			<p><a href="http://dmb.albumworks.com.au/" class="cta">DESIGN MY BOOK</a></p>
		</div>
	</div>
	<br>
	<hr>
	<br>
	<h1 class="my-header">About Design My Book</h1>
	<br>
	<div class="row">
		<div class="col-md-2 text-right">
			<img src="{{asset('img/design-my-book/icon.png')}}" height="30" />
		</div>
		<div class="col-md-10">
			<p class="about-header"><b>CHOOSE YOUR BOOK DETAILS</b></p>
			<p class="about-text">The <span class="text-italics">Design My Book</span> wizard will take you through all of our Photo Book options. You can pick your book size, book cover, paper type and optional accessories. </p>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-2 text-right">
			<img src="{{asset('img/design-my-book/icon.png')}}" height="30" />
		</div>
		<div class="col-md-10">
			<p class="about-header"><b>UPLOAD AS MANY PHOTOS AS YOU WANT</b></p>
			<p class="about-text">Got lots of photos? Upload your photos and leave the rest to us! Our designers will curate and perfectly design all the pages of your Photo Book using your uploaded photos. You can upload from any device. We recommend organising your photos into a single folder to make it easier for you - but you are able to upload from multiple folders if you want.</p>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-2 text-right">
			<img src="{{asset('img/design-my-book/icon.png')}}" height="30" />
		</div>
		<div class="col-md-10">
			<p class="about-header"><b>RECEIVE YOUR COMPLETED BOOK IN 3 DAYS</b></p>
			<p class="about-text">Our designers will design every new Photo Book from scratch - so we take up to 3 business days to get your finished Photo Book back to you. We take all of your photos, colour palette, cover type and more into consideration in order to create a personally unique Photo Book just for you.</p>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-2 text-right">
			<img src="{{asset('img/design-my-book/icon.png')}}" height="30" />
		</div>
		<div class="col-md-10">
			<p class="about-header"><b>REVIEW YOUR PHOTO BOOK BEFORE ORDERING</b></p>
			<p class="about-text">Once your Photo Book has been professionally designed, you will be sent a preview link to review. If at this time you are not satisfied with your first design, you have the chance to revise it for free. Your requested amendments will be forwarded to our designers to amend your album. Please note that this could take an additional 3 business days all depending on the amount of revisions you have provided. Your first revision is free of charge and any subsequent revisions are at a cost of $29.95 each.</p>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-2 text-right">
			<img src="{{asset('img/design-my-book/icon.png')}}" height="30" />
		</div>
		<div class="col-md-10">
			<p class="about-header"><b>MONEY BACK GUARANTEE</b></p>
			<p class="about-text">If you’re not happy with your finished design, we’ll credit your account with the cost of the design service fee. So you can use your credit amount on any product in the albumworks range!</p>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
</main>

@endsection

@section('scripts')
<script type="text/javascript" src="{{env('BASEPATH')}}js/bootstrap.min.js"></script>
@endsection