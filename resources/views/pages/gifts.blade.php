@extends('templates.header')

@section('body')
    <main class="normal static">
    <h1>Gifts</h1>
    <p class="subheading">Make a statement with a personalised photo gift. From homely cushion covers to stylish framed prints, add your own photos and text to make a one-of-a-kind gift that's sure to be treasured. It's so easy - make online, upload your photos and order!</p>
    <div class="group">
        <div class="productblock">
            <h3>Mugs</h3>
            <a href="{{env('BASEPATH')}}photo-mugs-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/mugs.jpg" /></a>
            <p>Create a Photo Mug with your own photos and text. Photo Mugs are a great gift idea or a great keepsake for yourself.</p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}photo-mugs-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Framed Prints</h3>
            <a href="{{env('BASEPATH')}}framed-prints-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/framed.jpg" /></a>
            <p>Top quality framed print with classic black wooden frame. All Framed Prints come with a convenient hook to hang on your wall and a handy desk stand to sit upright. <a href="{{env('BASEPATH')}}framed-prints">Learn more &gt;</a></p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}framed-prints-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Canvas Prints</h3>
            <a href="{{env('BASEPATH')}}canvas-photo-prints-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/canvas.jpg" /></a>
            <p>Experience your photos in a whole new way with our stunning Canvas prints. Traditional Canvas hand stretched over a timber frame. <a href="{{env('BASEPATH')}}canvas-photo-prints">Learn more &gt;</a></p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}canvas-photo-prints-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Poster Prints</h3>
            <a href="{{env('BASEPATH')}}poster-printing-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/poster-icon.jpg" /></a>
            <p>Poster prints taken to the next level with photographic quality re-production. Choose from 3 high quality paper types. <a href="{{env('BASEPATH')}}poster-printing">Learn more &gt;</a></p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}poster-printing-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Cushions</h3>
            <a href="{{env('BASEPATH')}}cushions-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/cushion.jpg" /></a>
            <p>Bring your living room to life with your own personalised Cushions. The perfect addition to your home decor.</p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}cushions-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Premium Prints</h3>
            <a href="{{env('BASEPATH')}}premium-prints-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/premium-prints.jpg" /></a>
            <p>Quality photo prints are back.  Bring your photos to life in a classic set of premium photo prints in beautiful quality with rich and deep colour.  <a href="{{env('BASEPATH')}}premium-prints">Learn more &gt;</a></p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}premium-prints-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Desk Prints</h3>
            <a href="{{env('BASEPATH')}}desk-prints-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/desk-prints.jpg" /></a>
            <p>Stand your beautiful photos proudly on your desk with a handy set of Desk Prints. Each Desk Flip contains 28 pages which you can design in any way you like. <a href="{{env('BASEPATH')}}desk-prints">Learn more &gt;</a></p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}desk-prints-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Instagram Prints</h3>
            <a href="{{env('BASEPATH')}}instagram-prints-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/instagram.jpg" /></a>
            <p>Turn any of your photos into stylish Photo Cards. Create from your phone, tablet or desktop using your Instagram, Facebook or local photos. <a href="{{env('BASEPATH')}}instagram-prints">Learn more &gt;</a></p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}instagram-prints-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Greeting Cards</h3>
            <a href="{{env('BASEPATH')}}greeting-cards-create-now"><img src="{{env('BASEPATH')}}img/gifts/new/greeting-card.jpg" /></a>
            <p>Send a truly personalised message with a Photo Greeting Card. Customise using your own photos and text.</p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}greeting-cards-create-now" class="cta">CREATE NOW</a></p>
        </div>
        <div class="productblock">
            <h3>Gift Vouchers</h3>
            <a href="{{env('BASEPATH')}}vouchers"><img src="{{env('BASEPATH')}}img/gifts/new/gift-voucher.jpg" /></a>
            <p>Let them decide. Gift Vouchers can be used on the entire albumworks product range. Gift Vouchers are delivered instantly to your inbox, ready to use and valid for three years.</p>
            <p class="ctaholder"><a href="{{env('BASEPATH')}}vouchers" class="cta">BUY NOW</a></p>
        </div>
    </div>    
</main>

@endsection