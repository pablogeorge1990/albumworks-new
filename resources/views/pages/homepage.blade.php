@extends('templates.header')

@section('title', 'Photo Books: Make your own personalised photo keepsake with albumworks')
@section('meta_description', '&#128216; Create beautiful personalised Photo Books, Calendars, Cards, Canvas & other photo keepsakes. 100% design freedom, fast delivery and local support.')

@section('body')

<main class="fullwidth">
    <? if(time() > mktime(0,0,0,6,15,2023) and time() < mktime(0,0,0,7,6,2023)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20230608/web.jpg); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20230608/mobile.jpg" /></a></p>
    <? elseif(time() > mktime(0,0,0,5,25,2023) and time() < mktime(0,0,0,6,15,2023)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20230519/web.jpg); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20230519/mobile.jpg" /></a></p>
    <? elseif(time() > mktime(0,0,0,4,6,2023) and time() < mktime(0,0,0,5,2,2023)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20230330/web.jpg); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20230330/mobile.jpg" /></a></p>
    <? elseif(time() > mktime(0,0,0,3,16,2023) and time() < mktime(0,0,0,4,6,2023)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20230309/web.jpg?v2); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20230309/mobile.jpg?v2" /></a></p>
    <? elseif(time() > mktime(0,0,0,11,30,2022) and time() < mktime(0,0,0,12,13,2022)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20221125/web.jpg?v4); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20221125/mobile.jpg?v4" /></a></p>
    <? elseif(time() > mktime(0,0,0,8,24,2022) and time() < mktime(0,0,0,10,25,2022)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20221020/web.jpg); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20221020/mobile.jpg" /></a></p>
    <? elseif(time() > mktime(0,0,0,9,15,2022) and time() < mktime(0,0,0,10,6,2022)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20220907/web.jpg); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20220907/mobile.jpg" /></a></p>
    <? elseif(time() > mktime(0,0,0,10,6,2022) and time() < mktime(0,0,0,11,3,2022)): ?>
        <section class="panel leader nomobile" style="background-image:url(img/promo/20221002/web.jpg); cursor:pointer" onclick="window.location = 'promotions';">
        </section>
        <p class="mobileonlyblock"><a href="promotions"><img style="width:100%" src="img/promo/20221002/mobile.jpg" /></a></p>
    <? else: ?>
        <section class="vidbg panel hastext leader" kbsw="2000" kbsh="625">
            <video autoplay muted>
                <source src="{{asset('img/homepage.mp4')}}" type="video/mp4">
                <img src="{{asset('img/00-header.jpg')}}" />
            </video>
            <div class="text">
                <h2>Remember this moment,<br>cherish this story,<br>celebrate this life.</h2>
                <p>MAKE SIMPLY BEAUTIFUL PHOTO BOOKS, STATIONERY, PREMIUM PRINTS, CALENDARS AND TRAVEL PRODUCTS</p>
            </div>
        </section>
    <? endif; ?>
    <section class="group introbox">
        <h2><span>Easy to make, fun to create, cherish for life</span></h2>
        <div class="content group">
            <p>It's as easy as click and create. Select a product, choose your photos, drag and drop, add your own text and bring your photos to life in one of our wide, wide range of products. And if you are not quite sure where to start, you can <a href="http://pictureworks.simplybook.me/v2/" target="_blank">book an appointment</a> for a walk through of how easy it is or just call us on <strong>1300 553 448</strong>.</p>
            <p class="imgblock">
                <img src="{{asset('img/support.jpg')}}" alt="local support" />
                <img src="{{asset('img/dispatch.jpg')}}" alt="rapid dispatch" />
                <img src="{{asset('img/quality.jpg')}}" alt="quality guarantee" />
                <img src="{{asset('img/choices.jpg')}}" alt="more choices" />
            </p>
        </div>
        <img src="{{asset('img/intro.jpg')}}" />
    </section>
    <section class="panel twoup">
        <div class="subpanel">
            <h3>Webinars + tutorials</h3>
            <a href="https://www.gotostage.com/channel/a8113601621940fc82fed57d50d29db2"><img src="{{asset('img/home/01-webinar.jpg')}}" alt="webinar" class="image-adjust"/></a>
            <p class="description" style="padding-bottom:7px;">
                Want to become an expert at using our desktop Editor? Our webinar series aims to help you learn the ropes of our easy-to-use Editor. From photo editing, to ordering a Photo Book, we’re covering all you need to know! To view past webinars click view webinar below
            </p>
            <p class="thecta">
                <a href="https://www.youtube.com/user/albumworks/videos" class="cta">VIEW WEBINARS</a>
            </p>
        </div>
        <div class="subpanel">
            <h3>Stay connected</h3>
            <img src="{{asset('img/home/02-social.jpg')}}" alt="socialmedia" class="image-adjust"/>
            <p class="description">Join a community of photo lovers. We‘ve been producing Photo Books for more than 10 years and love sharing every part of the journey. Follow our social media channels to stay up to date on the latest savings and offers!</p>
            <p class="thecta"><a href="https://www.facebook.com/albumworks"><img src="{{asset('img/home/03-fb.png')}}" alt="facebook" style="width:140px;"/></a><a href="https://www.instagram.com/albumworks"><img src="{{asset('img/home/04-insta.png')}}" alt="instagram" style="width:140px; padding-left:20px;"/></a></p>
        </div>
    </section>
    <section class="panel hastext" style="background-image:url({{asset('img/01-photobooks.jpg')}})">
        <div class="text leftside">
            <h2><strong>PHOTO BOOKS:</strong> when memories matter</h2>
            <p>Create a travel photo book, remember a birthday, celebrate a wedding, document your family history with <em>albumworks</em>’ easy-to-use design editor. Choose from Australia's widest range of print quality, paper choice, binding styles, covers and accessories to bring your treasured memories to life.</p>
            <a href="photo-books" class="cta">LEARN MORE</a>
        </div>
    </section>
    <section class="panel hastext" style="background-image:url({{asset('img/home/prints.jpg')}})">
        <div class="text">
            <h2><strong>Prestige Prints:</strong> because your memories deserve it</h2>
            <p>Photo print quality has fallen over the years, paper has got thinner and colours less vibrant, but not at <em>albumworks</em>. Your photos are brought to life on the highest quality photographic press and printed on archival quality photo papers. Your prints will be more vibrant and they are designed to last for the many decades to come.</p>
            <a href="premium-prints" class="cta">LEARN MORE</a>
        </div>
    </section>
    <section class="panel twoup">
        <div class="subpanel">
            <img src="{{asset('img/home/calendars.jpg')}}" alt="calendars" />
            <h3>Custom Calendars</h3>
            <p class="description">Beautiful desk and wall Calendars, customised for your perfect year. Choose from our pre-designed grid layouts or edit each date cell with your custom dates. Available in smooth and silky Standard Colour paper or upgrade to stunning colour with Hi Colour printing.</p>
            <p class="thecta"><a href="wall-calendar" class="cta">LEARN MORE</a></p>
        </div>
        <div class="subpanel">
            <img src="{{asset('img/home/decor.jpg')}}" alt="prints and decor" />
            <h3>Captivating decor</h3>
            <p class="description">Rescue your photos from the depth of your hard drive or smartphone and bring them to life on your walls with a stunning Canvas Print, trendy wooden print or framed print. Celebrate the important moments in life with out easy to make Home Decor range.</p>
            <p class="thecta"><a href="canvas-photo-prints" class="cta">LEARN MORE</a></p>
        </div>
    </section>
    <section class="panel hastext" style="background-image:url({{asset('img/05-delivery.jpg')}})">
        <div class="text leftside">
            <h2>Dispatched within 7 business days</h2>
            <p>We take service and quality seriously, and we know that you want your product ASAP. Typically items are dispatched within 4-5 business days, but sometimes we will re-make your order to ensure the quality is just right, so this can add a couple of days in production . Please allow 2-3 business days for delivery to major capital cities (and a little longer if you live in a regional area).</p>
            <a href="shipping" class="cta">LEARN MORE</a>
        </div>
    </section>
    <section class="panel hastext" style="background-image:url({{asset('img/06-range-new.jpg')}})">
        <div class="text">
            <h2>Price &amp; range</h2>
            <p>With Photo Books starting from just $29.95 and products in our gift range starting from only $3.95 there is a product for every budget. Have a play with the price calculator to find the right product for you.</p>
            <a href="calculator" class="cta">LEARN MORE</a>
        </div>
    </section>
    <section class="panel hastext" style="background-image:url({{asset('img/gifts.jpg')}})">
        <div class="text leftside">
            <h2>Gift solutions for every occasion</h2>
            <p>Make a statement with a personalised photo gift. From homely cushion covers to stylish framed prints, add your own photos and text to make a one-of-a-kind gift that's sure to be treasured.</p>
            <a href="gifts" class="cta">VIEW ALL GIFTS</a>
        </div>
    </section>
    <section class="introbox">
        <h2><span>What our customers are saying</span></h2>
        <div class="trustpilot-widget" data-locale="en-US" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="55f980f20000ff000583623f" data-style-height="130px" data-style-width="100%" data-theme="light" data-stars="5" data-schema-type="Organization">
            <a href="https://www.trustpilot.com/review/albumworks.com.au" target="_blank">Trustpilot</a>
        </div>
    </section>
</main>

@endsection

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 10035965;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<noscript>
    <a href="https://www.livechatinc.com/chat-with/10035965/">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
</noscript>
<!-- End of LiveChat code -->
