@extends('templates.header')

@section('title', $__pagedata['document_title'])

@if(isset($__pagedata['meta_description']))
    @section('meta_description', $__pagedata['meta_description'])
@endif

@section('body')

    <main class="retail {{@$__pagedata['mainclass']}}">
    <div class="ecommerce group">
        <h1 class="mobileonlyblock">{!! $__pagedata['title'] !!}</h1>
        <div class="images">
            <img src="img/grey.png" width="510" height="510" class="primary" />
            <nav class="others group">
                @foreach($__pagedata['images'] as $src => $data)
                    <img src="{{$src}}" alt="{{@$data['alt']}}" imgid="{{@$data['imgid']}}" />
                @endforeach
            </nav>
        </div>
        <div class="details">
            <h1 class="nomobile">{{$__pagedata['title']}}</h1>
            @if(isset($__pagedata['categories']))
                <p>
                    <label for="field_{{$__pagedata['categories']['code']}}">{!!$__pagedata['categories']['label']!!}:</label>
                    <select name="{{$__pagedata['categories']['code']}}" id="field_{{$__pagedata['categories']['code']}}" class="category inputbox">
                        @foreach($__pagedata['categories']['values'] as $category)
                            <option value="{{$category['code']}}" data-products='{{json_encode($category['products'])}}'>{!!$category['label']!!}</option>
                        @endforeach
                    </select>
                </p>
                <p>
                    <label for="field_size">Size:</label>
                    <select name="size" id="field_size" class="primaryfield inputbox">
                    </select>
                </p>
            @else
                <p>
                    @if(isset($__pagedata['primary']))
                        <label for="field_{{$__pagedata['primary'][0]}}">{{$__pagedata['primary'][1]}}:</label>
                        <select name="{{$__pagedata['primary'][0]}}" id="field_{{$__pagedata['primary'][0]}}" class="primaryfield inputbox">
                    @else
                        <label for="field_size">Size:</label>
                        <select name="size" id="field_size" class="primaryfield inputbox">
                    @endif
                        @foreach($__pagedata['products'] as $code => $product)
                            <option value="{{$code}}"
                                @foreach($product as $key => $val)
                                    @if($key != 'label')
                                        data-{{$key}}="{{$val}}"
                                    @endif
                                @endforeach
                                >{!!$product['label']!!}</option>
                        @endforeach
                    </select>
                </p>
            @endif
            @foreach($__pagedata['options'] as $option => $label)
                @if(isset($__pagedata['pre_messages'][$option]))
                    <p class="fieldmessage">{{$__pagedata['pre_messages'][$option]}}</p>
                @endif
                <p class="{{$option}}_container">
                    <label>{!!$label!!}:</label>
                    @if(isset($__pagedata['extras'][$option]))
                        @if(isset($__pagedata['extras'][$option]['type']) and $__pagedata['extras'][$option]['type'] == 'image')
                            <img src="{{$__pagedata['extras'][$option]['src']}}" />
                        @elseif(isset($__pagedata['extras'][$option]['type']) and $__pagedata['extras'][$option]['type'] == 'productspecific')
                            @foreach($__pagedata['extras'][$option]['items'] as $product => $values)
                                <select name="{{$option}}" id="field_{{$product}}_{{$option}}" class="inputbox extras productspecific">
                                    @foreach($values as $sublabel => $subprice)
                                        <option id="{{md5('field_'.$product.'_'.$option.'_'.$sublabel)}}" value="{{$sublabel}}" data-price="{{$subprice}}">{!!$sublabel!!}</option>
                                    @endforeach
                                </select>                            
                            @endforeach
                        @else
                            <select name="{{$option}}" id="field_{{$option}}" class="inputbox extras">
                                @foreach($__pagedata['extras'][$option] as $sublabel => $subprice)
                                    @if(is_array($subprice))
                                        <option id="{{md5($sublabel)}}" value="{{$sublabel}}"
                                            @foreach($subprice as $key => $price)
                                                data-{{$key}}="{{$price}}"
                                            @endforeach
                                            >{!!$sublabel!!}</option>
                                    @else
                                        <option value="{{$subprice}}">{!!$sublabel!!}</option>
                                    @endif
                                @endforeach
                            </select>
                        @endif
                    @else
                        <span class="likeabox"></span>
                    @endif
                </p>
                @if(isset($__pagedata['post_messages'][$option]))
                    <p class="fieldmessage">{{$__pagedata['post_messages'][$option]}}</p>
                @endif                
            @endforeach
            @if(isset($__pagedata['flashpromotion']) and is_array($__pagedata['flashpromotion']))
                <span class="promotion_image"><img src="img/christmas.jpg" /></span>
                <span class="promotion">{!!$__pagedata['flashpromotion']['message']!!}</span>
                <span class="promotion_code">{!!$__pagedata['flashpromotion']['promocode']!!}</span>
            @endif
            @if(isset($__pagedata['precreate']))
                <span class="precreate">{!!$__pagedata['precreate']!!}</span>
            @endif
            <p class="url_container">
                <a target="_blank" href="javascript:void(0)" onclick="gtag('config', 'UA-1338422-2', {'page_path': '/editor-start'});" class="cta">CREATE NOW</a>
            </p>
            <p class="nomobile dledlink">
                [ <a href="javascript:splitchoice('popular')">or install our Download Editor</a> ]
            </p>
            <div class="about">
                <p><strong>ABOUT:</strong></p>
                <p>{!! $__pagedata['about'] !!}</p>
            </div>
        </div>
    </div>
        <hr/>
        <br/><br/>
    <div class="psc group bookmark" id="pricing">
        @if(sizeof($__pagedata['prices']) and isset($__pagedata['prices'][0]) and sizeof($__pagedata['prices'][0]))
            <div class="prices group">
                <h2>Prices</h2>
                @foreach($__pagedata['prices'] as $pricetable)
                    <table cellpadding="0" cellspacing="0" class="{{sizeof($__pagedata['prices']) > 1 ? 'multitable' : 'single'}}">
                        <thead>
                        @foreach($pricetable as $i => $row)
                            @if($i == 1)
                                </thead>
                                <tbody>
                            @endif
                            <tr>
                                @foreach($row as $j => $cell)
                                    @if($i == 0 and $j == 0)
                                        <td style="width:20%">{!!$cell!!}</td>
                                    @elseif($i == 0 or $j == 0)
                                        <th style="width:{{(($i==0)?floor(80/sizeof($row)).'%':'auto')}}">{!! $cell !!}</th>
                                    @else
                                        <td>{!! $cell  !!}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endforeach
            </div>
        @endif

        <div class="shipping">
            <h2>Shipping</h2>
            <table cellpadding="0" cellspacing="0">
                <thead>
                @foreach($__pagedata['shipping'] as $i => $row)
                @if($i == 1)
                    </thead>
                    <tbody>
                @endif
                <tr>
                    @foreach($row as $j => $cell)
                        @if($i == 0 and $j == 0)
                            <td style="width:20%">{!! $cell !!}</td>
                        @elseif($i == 0 or $j == 0)
                            <th style="width:{{(($i==0)?floor(80/sizeof($row)).'%':'auto')}}">{!! $cell !!}</th>
                        @else
                            <td>{!! $cell !!}</td>
                        @endif
                    @endforeach
                </tr>
                @endforeach
                </tbody>
            </table>

            <div class="notes">
                {!! @$__pagedata['below_shipping'] !!}
                {{--<p><strong>PO BOXES:</strong> <em>Delivery of all items to a PO Box address will incur an additional $5.00</em></p>--}}
                {{--<p><strong>MULTIPLE ITEMS:</strong> <em>Where multiple items are purchased at the same time, "FIRST COPY" shipping price will be charged for the largest item and "EXTRA COPY" shipping price will be charged for all other items.</em></p>--}}
            </div>

            <div class="contact">
                <h2>Contact</h2>
                <img src="img/contact.jpg" />
                <p>Still have questions? Ask us! Our amazing Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible.</p>
                <p><a href="contact" class="cta">CONTACT</a></p>
            </div>
        </div>

        <script type="text/javascript">
            @if(isset($__pagedata['urls']))
                var complicated_urls = '{{$__pagedata['urls']['formula']}}';
                var urls = {!! json_encode($__pagedata['urls']['values']) !!};
            @else
                var complicated_urls = false;
            @endif
        </script>
    </div>
</main>

@endsection