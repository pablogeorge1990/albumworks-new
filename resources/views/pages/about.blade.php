@extends('templates.header')

@section('body')
    <main class="normal static">
    <h1>About Us</h1>
    <div class="group leftright">
        <div class="group">
            <div class="content">
                <h3>We know Photo Books</h3>
                <p>We began <?=(date('Y')-2006)?> years ago as a little company with a big passion for making beautiful Photo Books. Since then we have grown to be one of the biggest and most loved Photo Book providers to Australia. We constantly strive to improve our production, and editor software, and we have a highly trained local Customer Service team here to help you every step of the way. We believe that every page of your story should be treated with utmost care, and captured in the highest quality.</p>
            </div>
            <img src="{{asset('img/about/01.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Quality you can trust</h3>
                <p>That's a guarantee. Happy customers are our business. We want to do everything we can to ensure your albumworks experience is the best possible. If you receive your order and you're not 100% happy, please get in touch with our friendly team. We'll do everything we can to help. We're proud of our products, and we know you'll love them.  And if it's not right we will refund your order or produce a new copy.</p>
            </div>
            <img src="{{asset('img/about/02.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Aussie customer service</h3>
                <p>Our amazing and friendly Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible.</p>
            </div>
            <img src="{{asset('img/about/03.jpg')}}" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>We're here to help</h3>
                <p>With so many options, and an Editor to learn, creating the perfect photo keepsake is a fun and challenging project - but you might have questions along the way. So we're here to help! Read through our FAQs for some tips or make a booking with one of our friendly customer service representatives. In a courtesy phone call, we'll answer any key questions you have and pass on some useful tips.</p>
                <p>And if you still need help, don't hesitate to get in touch with our Customer Service team.</p>
                <p><a href="{{env('BASEPATH')}}contact" class="cta">CONTACT</a></p>
            </div>
            <img src="{{asset('img/about/04.jpg')}}" height="380" />
        </div>
    </div>    
</main>

@endsection