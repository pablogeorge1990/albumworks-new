@extends('templates.header')

@section('body')

    <?php
        $button_text = isset($_GET['btxt']) ? $_GET['btxt'] : 'PURCHASE NOW';
        $button_text = urldecode($button_text);
    ?>

    <main class="normal static">
    <h1>Gift Vouchers</h1>
    <div class="group leftright">
        <div class="group">
            <div class="content">
                <h3>Let them choose</h3>
                <p>Our Gift Vouchers can be used on our entire product range, including Photo Books, Canvas, Calendars and more! All Gift Vouchers are valid for three years from purchase date.</p>
                <p style="font-style:italic;">*Gift Vouchers cannot be used in conjunction with any other offer or promotional Voucher Code.</p>
            </div>
            <img src="img/vouchers/01.jpg" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Instant delivery</h3>
                <p>Gift Vouchers are emailed to you instantly as an Adobe PDF attachment. You can print them or simply forward the email on to the recipient. So they're perfect at the last minute!</p>
            </div>
            <img src="img/vouchers/02.jpg" height="380" />
        </div>
        <div class="group">
            <div class="content">
                <h3>Starting from just $25</h3>
                <p>Our Gift Vouchers range from as low as $25 all the way up to $250. They're an amazing gift idea on any budget! Order your Gift Voucher today using the order form below!</p>
            </div>
            <img src="img/vouchers/03.jpg" height="380" />
        </div>
    </div>  
    
    <hr/>
    
    <section class="static group semimodules vouchermodules">
        <div class="semimodule">
            <h2>How to order</h2>
            <p>1. Fill in your details in the form and click '<?=ucwords(strtolower($button_text))?>'.</p>
            <p>2. Pay via credit card on the next screen at the NAB Payment Gateway.</p>
            <p>3. You will be instantly emailed your Gift Voucher which you can either print or forward to the recipient.</p>
        </div>
        <div class="semimodule voucherform">
            <h2>Purchase your Gift Voucher</h2>
            <?
                $promo = $type = '';
                date_default_timezone_set('Australia/Melbourne');
                if(isset($_GET['sjbtest']) or (time() > mktime(0,0,0,5,9,2023) and time() < mktime(0,0,0,5,15,2023))){
                    $promo = 'MUM2023';
                    $type = '30OFF';
                }

                $errors = array();
                switch($type){
                    case '25OFF':
                        $gift_voucher_options = array(
                            '18.75' => array('printable' => '$25 Value, NOW $18.75'),
                            '30.00' => array('printable' => '$40 Value, NOW $30'),
                            '37.50' => array('printable' => '$50 Value, NOW $37.50'),
                            '45.00' => array('printable' => '$60 Value, NOW $45'),
                            '60.00' => array('printable' => '$80 Value, NOW $60'),
                            '75.00' => array('printable' => '$100 Value, NOW $75'),
                            '90.00' => array('printable' => '$120 Value, NOW $90'),
                            '105.00' => array('printable' => '$140 Value, NOW $105'),
                            '120.00' => array('printable' => '$160 Value, NOW $120'),
                            '135.00' => array('printable' => '$180 Value, NOW $135'),
                            '150.00' => array('printable' => '$200 Value, NOW $150'),
                            '168.75' => array('printable' => '$225 Value, NOW $168.75'),
                            '187.50' => array('printable' => '$250 Value, NOW $187.50'),
                            '225.00' => array('printable' => '$300 Value, NOW $225'),
                            '262.50' => array('printable' => '$350 Value, NOW $262.50'),
                            '300.00' => array('printable' => '$400 Value, NOW $300'),
                            '337.50' => array('printable' => '$450 Value, NOW $337.50'),
                            '375.00' => array('printable' => '$500 Value, NOW $375'),
                        );
                                    
                        ob_start();
                        ?>
                            <? if(sizeof($errors) == 1): ?>
                                <div class="gverror">
                                    <p><strong>An error has occured:</strong><br><?=array_pop($errors)?></p>
                                </div>
                            <? elseif(sizeof($errors) > 1): ?>
                                <div class="gverror">
                                    <p><strong>Multiple errors have occured:</strong></p>
                                    <ul>
                                        <? foreach($errors as $error): ?>
                                            <li><?=$error?></li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            <? endif; ?>                                    
                            <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" onsubmit="return validateGvForm()" class="bigform"> <!-- test to live -->
                                <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" /> 
                                <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
                                <input type="hidden" name="vendor_name" value="6010010" />
                                <input type="hidden" name="gst_rate" value="10">
                                <input type="hidden" name="gst_added" value="true">     
                                <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
                                <input type="hidden" name="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=<?=$promo?>&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
                                <input type="hidden" name="return_link_text" value="Return to albumworks" />
                                <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/gift-voucher-complete" />
                                
                                <p>
                                    <label for="name">Name:</label> 
                                    <input name="First Name" id="gv_first_name" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="First Name" />
                                    <input type="hidden" name="required_fields" value="First Name" />
                                </p>

                                <p>
                                    <label for="email">Email:</label>
                                    <input name="Email" id="gv_email" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="Email" />
                                    <input type="hidden" name="required_fields" value="Email" />
                                    <input type="hidden" name="receipt_address" value="<<Email>>" />
                                </p>

                                <p>
                                    <label for="gv_quantity">Quantity:</label>
                                    <select name="Gift Voucher" id="gv_quantity" class="inputbox">
                                        <? for($i = 1; $i <= 100; $i++): ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                        <? endfor; ?>
                                    </select>
                                </p>
                                <p>
                                    <label for="giftvoucher">Gift Voucher Type:</label> 
                                    <select name="Gift Voucher" id="gv_giftvoucher" class="inputbox"> 
                                        <option value="">Please Select</option> 
                                        <? foreach($gift_voucher_options as $id => $opt): ?>
                                            <option value="<?=$id?>"><?=$opt['printable']?></option>
                                        <? endforeach; ?>
                                    </select> 
                                </p>

                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style=""><?=$button_text?></a>
                                <img style="margin-left:30px" class="deets" src="img/vouchers/04.jpg" />
                                <div class="clr"></div>
                            </form>
                        <?
                        $html=ob_get_clean();                
                    break;
                                    
                    case '30OFF':
                        $gift_voucher_options = array(
                            '17.50' => array('printable' => '$25 Value, NOW $17.50'),
                            '28.00' => array('printable' => '$40 Value, NOW $28 (8x8 SD Photo Book)'),
                            '35.00' => array('printable' => '$50 Value, NOW $35'),
                            '42.00' => array('printable' => '$60 Value, NOW $42'),
                            '56.00' => array('printable' => '$80 Value, NOW $56 (11x8.5 SD Photo Book)'),
                            '70.00' => array('printable' => '$100 Value, NOW $70 (12x12 SD Photo Book)'),
                            '84.00' => array('printable' => '$120 Value, NOW $84 (16x12 SD Photo Book)'),
                            '98.00' => array('printable' => '$140 Value, NOW $98'),
                            '112.00' => array('printable' => '$160 Value, NOW $112'),
                            '126.00' => array('printable' => '$180 Value, NOW $126'),
                            '140.00' => array('printable' => '$200 Value, NOW $140'),
                            '157.50' => array('printable' => '$225 Value, NOW $157.50'),
                            '175.00' => array('printable' => '$250 Value, NOW $175'),
                            '210.00' => array('printable' => '$300 Value, NOW $210'),
                            '245.00' => array('printable' => '$350 Value, NOW $245'),
                            '280.00' => array('printable' => '$400 Value, NOW $280'),
                            '315.00' => array('printable' => '$450 Value, NOW $315'),
                            '350.00' => array('printable' => '$500 Value, NOW $350'),
                        );
                                    
                        ob_start();
                        ?>
                            <? if(sizeof($errors) == 1): ?>
                                <div class="gverror">
                                    <p><strong>An error has occured:</strong><br><?=array_pop($errors)?></p>
                                </div>
                            <? elseif(sizeof($errors) > 1): ?>
                                <div class="gverror">
                                    <p><strong>Multiple errors have occured:</strong></p>
                                    <ul>
                                        <? foreach($errors as $error): ?>
                                            <li><?=$error?></li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            <? endif; ?>                                    
                            <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" onsubmit="return validateGvForm()" class="bigform"> <!-- test to live -->
                                <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" /> 
                                <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
                                <input type="hidden" name="vendor_name" value="6010010" />
                                <input type="hidden" name="gst_rate" value="10">
                                <input type="hidden" name="gst_added" value="true">     
                                <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
                                <input type="hidden" name="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=<?=$promo?>&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
                                <input type="hidden" name="return_link_text" value="Return to albumworks" />
                                <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/gift-voucher-complete" />
                                
                                <p>
                                    <label for="name">Name:</label> 
                                    <input name="First Name" id="gv_first_name" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="First Name" />
                                    <input type="hidden" name="required_fields" value="First Name" />
                                </p>

                                <p>
                                    <label for="email">Email:</label> 
                                    <input name="Email" id="gv_email" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="Email" />
                                    <input type="hidden" name="required_fields" value="Email" />
                                    <input type="hidden" name="receipt_address" value="<<Email>>" />
                                </p>

                                <p>
                                    <label for="gv_quantity">Quantity:</label>
                                    <select name="Gift Voucher" id="gv_quantity" class="inputbox">
                                        <? for($i = 1; $i <= 100; $i++): ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                        <? endfor; ?>
                                    </select>
                                </p>
                                <p>
                                    <label for="giftvoucher">Gift Voucher Type:</label> 
                                    <select name="Gift Voucher" id="gv_giftvoucher" class="inputbox"> 
                                        <option value="">Please Select</option> 
                                        <? foreach($gift_voucher_options as $id => $opt): ?>
                                            <option value="<?=$id?>"><?=$opt['printable']?></option>
                                        <? endforeach; ?>
                                    </select> 
                                </p>
                                
                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style=""><?=$button_text?></a>
                                <img style="margin-left:30px" class="deets" src="img/vouchers/04.jpg" />
                                <div class="clr"></div>
                            </form>
                        <?
                        $html=ob_get_clean();                
                    break;

                    case '35OFF':
                        $gift_voucher_options = array(
                            '16.25' => array('printable' => '$25 Value, NOW $16.25'),
                            '19.50' => array('printable' => '$30 Value, NOW $19.50 (8x8 SD Photo Book)'),
                            '26.00' => array('printable' => '$40 Value, NOW $26'),
                            '32.50' => array('printable' => '$50 Value, NOW $32.50'),
                            '39.00' => array('printable' => '$60 Value, NOW $39'),
                            '52.00' => array('printable' => '$80 Value, NOW $52 (11x8.5 SD Photo Book)'),
                            '65.00' => array('printable' => '$100 Value, NOW $65 (12x12 SD Photo Book)'),
                            '78.00' => array('printable' => '$120 Value, NOW $78 (16x12 SD Photo Book)'),
                            '91.00' => array('printable' => '$140 Value, NOW $91'),
                            '110.50' => array('printable' => '$170 Value, NOW $110.50'),
                            '130.00' => array('printable' => '$200 Value, NOW $130'),
                            '162.50' => array('printable' => '$250 Value, NOW $162.50'),
                        );

                        ob_start();
                        ?>
                            <? if(sizeof($errors) == 1): ?>
                                <div class="gverror">
                                    <p><strong>An error has occured:</strong><br><?=array_pop($errors)?></p>
                                </div>
                            <? elseif(sizeof($errors) > 1): ?>
                                <div class="gverror">
                                    <p><strong>Multiple errors have occured:</strong></p>
                                    <ul>
                                        <? foreach($errors as $error): ?>
                                            <li><?=$error?></li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            <? endif; ?>
                            <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" onsubmit="return validateGvForm()" class="bigform"> <!-- test to live -->
                                <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" />
                                <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
                                <input type="hidden" name="vendor_name" value="6010010" />
                                <input type="hidden" name="gst_rate" value="10">
                                <input type="hidden" name="gst_added" value="true">
                                <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
                                <input type="hidden" name="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=<?=$promo?>&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
                                <input type="hidden" name="return_link_text" value="Return to albumworks" />
                                <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/gift-voucher-complete" />

                                <p>
                                    <label for="name">Name:</label>
                                    <input name="First Name" id="gv_first_name" type="text" class="inputbox" />
                                    <input type="hidden" name="information_fields" value="First Name" />
                                    <input type="hidden" name="required_fields" value="First Name" />
                                </p>

                                <p>
                                    <label for="email">Email:</label>
                                    <input name="Email" id="gv_email" type="text" class="inputbox" />
                                    <input type="hidden" name="information_fields" value="Email" />
                                    <input type="hidden" name="required_fields" value="Email" />
                                    <input type="hidden" name="receipt_address" value="<<Email>>" />
                                </p>

                                <p>
                                    <label for="gv_quantity">Quantity:</label>
                                    <select name="Gift Voucher" id="gv_quantity" class="inputbox">
                                        <? for($i = 1; $i <= 100; $i++): ?>
                                            <option value="<?=$i?>"><?=$i?></option>
                                        <? endfor; ?>
                                    </select>
                                </p>
                                <p>
                                    <label for="giftvoucher">Gift Voucher Type:</label>
                                    <select name="Gift Voucher" id="gv_giftvoucher" class="inputbox">
                                        <option value="">Please Select</option>
                                        <? foreach($gift_voucher_options as $id => $opt): ?>
                                            <option value="<?=$id?>"><?=$opt['printable']?></option>
                                        <? endforeach; ?>
                                    </select>
                                </p>

                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style=""><?=$button_text?></a>
                                <img style="margin-left:30px" class="deets" src="img/vouchers/04.jpg" />
                                <div class="clr"></div>
                            </form>
                        <?
                        $html=ob_get_clean();
                    break;

                    case '20OFF':
                        $gift_voucher_options = array(
                            '20.00' => array('printable' => '$25 Gift Voucher. You Pay $20'),
                            '32.00' => array('printable' => '$40 Gift Voucher. You Pay $32'),
                            '40.00' => array('printable' => '$50 Gift Voucher. You Pay $40'),
                            '48.00' => array('printable' => '$60 Gift Voucher. You Pay $48'),
                            '64.00' => array('printable' => '$80 Gift Voucher. You Pay $64'),
                            '80.00' => array('printable' => '$100 Gift Voucher. You Pay $80'),
                            '96.00' => array('printable' => '$120 Gift Voucher. You Pay $96'),
                            '112.00' => array('printable' => '$140 Gift Voucher. You Pay $112'),
                            '160.00' => array('printable' => '$200 Gift Voucher. You Pay $160'),
                            '200.00' => array('printable' => '$250 Gift Voucher. You Pay $200'),
                            '240.00' => array('printable' => '$300 Gift Voucher, You Pay $240'),
                            '280.00' => array('printable' => '$350 Gift Voucher, You Pay $280'),
                            '320.00' => array('printable' => '$400 Gift Voucher, You Pay $320'),
                            '360.00' => array('printable' => '$450 Gift Voucher, You Pay $360'),
                            '400.00' => array('printable' => '$500 Gift Voucher, You Pay $400'),                        
                        );
                                    
                        ob_start();
                        ?>
                            <? if(sizeof($errors) == 1): ?>
                                <div class="gverror">
                                    <p><strong>An error has occured:</strong><br><?=array_pop($errors)?></p>
                                </div>
                            <? elseif(sizeof($errors) > 1): ?>
                                <div class="gverror">
                                    <p><strong>Multiple errors have occured:</strong></p>
                                    <ul>
                                        <? foreach($errors as $error): ?>
                                            <li><?=$error?></li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            <? endif; ?>                                    
                            <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" onsubmit="return validateGvForm()" class="bigform"> <!-- test to live -->
                                <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" /> 
                                <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
                                <input type="hidden" name="vendor_name" value="6010010" />
                                <input type="hidden" name="gst_rate" value="10">
                                <input type="hidden" name="gst_added" value="true">     
                                <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
                                <input type="hidden" name="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?brand=AW&promo=<?=$promo?>&First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
                                <input type="hidden" name="return_link_text" value="Return to albumworks" />
                                <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/gift-voucher-complete" />
                                
                                <p>
                                    <label for="name">Name:</label> 
                                    <input name="First Name" id="gv_first_name" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="First Name" />
                                    <input type="hidden" name="required_fields" value="First Name" />
                                </p>

                                <p>
                                    <label for="email">Email:</label> 
                                    <input name="Email" id="gv_email" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="Email" />
                                    <input type="hidden" name="required_fields" value="Email" />
                                    <input type="hidden" name="receipt_address" value="<<Email>>" />
                                </p>

                                <p>
                                    <label for="gv_quantity">Quantity:</label>
                                    <select name="Gift Voucher" id="gv_quantity" class="inputbox">
                                        <? for($i = 1; $i <= 100; $i++): ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                        <? endfor; ?>
                                    </select>
                                </p>
                                <p>
                                    <label for="giftvoucher">Gift Voucher Type:</label> 
                                    <select name="Gift Voucher" id="gv_giftvoucher" class="inputbox"> 
                                        <option value="">Please Select</option> 
                                        <? foreach($gift_voucher_options as $id => $opt): ?>
                                            <option value="<?=$id?>"><?=$opt['printable']?></option>
                                        <? endforeach; ?>
                                    </select> 
                                </p>
                                
                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style=""><?=$button_text?></a>
                                <img style="margin-left:30px" class="deets" src="img/vouchers/04.jpg" />
                                <div class="clr"></div>
                            </form>
                        <?
                        $html=ob_get_clean();                
                    break;
                    
                    default:
                        $gift_voucher_options = array(
                            '25' => array('printable' => '$25 Value', 'price_cents' => '2500', 'price' => '25.00'),
                            '30' => array('printable' => '$30 Value', 'price_cents' => '3000', 'price' => '30.00'),
                            '40' => array('printable' => '$40 Value', 'price_cents' => '4000', 'price' => '40.00'),
                            '50' => array('printable' => '$50 Value', 'price_cents' => '5000', 'price' => '50.00'),
                            '60' => array('printable' => '$60 Value', 'price_cents' => '6000', 'price' => '60.00'),
                            '80' => array('printable' => '$80 Value', 'price_cents' => '8000', 'price' => '80.00'),
                            '100' => array('printable' => '$100 Value', 'price_cents' => '10000', 'price' => '100.00'),
                            '120' => array('printable' => '$120 Value', 'price_cents' => '12000', 'price' => '120.00'),
                            '140' => array('printable' => '$140 Value', 'price_cents' => '14000', 'price' => '140.00'),
                            '170' => array('printable' => '$170 Value', 'price_cents' => '17000', 'price' => '170.00'),
                            '200' => array('printable' => '$200 Value', 'price_cents' => '20000', 'price' => '200.00'),
                            '250' => array('printable' => '$250 Value', 'price_cents' => '25000', 'price' => '250.00'),
                        );
                    
                        ob_start();
                        ?>   
                            <? if(sizeof($errors) == 1): ?>
                                <div class="gverror">
                                    <p><strong>An error has occured:</strong><br><?=array_pop($errors)?></p>
                                </div>
                            <? elseif(sizeof($errors) > 1): ?>
                                <div class="gverror">
                                    <p><strong>Multiple errors have occured:</strong></p>
                                    <ul>
                                        <? foreach($errors as $error): ?>
                                            <li><?=$error?></li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            <? endif; ?>                                    
                            <form action="https://transact.nab.com.au/live/hpp/payment" method="post" name="dlform" id="gvformID" onsubmit="return validateGvForm()" class="bigform"> <!-- test to live -->
                                <input type="hidden" name="refund_policy" value="https://www.albumworks.com.au/terms-and-conditions.html" /> 
                                <input type="hidden" name="privacy_policy" value="https://www.albumworks.com.au/privacy.html" />
                                <input type="hidden" name="vendor_name" value="6010010" />
                                <input type="hidden" name="gst_rate" value="10">
                                <input type="hidden" name="gst_added" value="true">     
                                <input type="hidden" name="payment_alert" value="giftvouchers@pictureworks.com.au" />
                                <input type="hidden" name="reply_link_url" value="https://api.photo-products.com.au/giftvoucher/gvhandler.php?First Name=&Email=&Gift Voucher=&bank_reference=&payment_amount=&payment_number=" />
                                <input type="hidden" name="return_link_text" value="Return to albumworks" />
                                <input type="hidden" name="return_link_url" value="https://www.albumworks.com.au/gift-voucher-complete" />
                                
                                <p>
                                    <label for="name">Name:</label> 
                                    <input name="First Name" id="gv_first_name" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="First Name" />
                                    <input type="hidden" name="required_fields" value="First Name" />
                                </p>

                                <p>
                                    <label for="email">Email:</label> 
                                    <input name="Email" id="gv_email" type="text" class="inputbox" /> 
                                    <input type="hidden" name="information_fields" value="Email" />
                                    <input type="hidden" name="required_fields" value="Email" />
                                    <input type="hidden" name="receipt_address" value="<<Email>>" />
                                </p>

                                <p>
                                    <label for="gv_quantity">Quantity:</label>
                                    <select name="Gift Voucher" id="gv_quantity" class="inputbox">
                                        <? for($i = 1; $i <= 100; $i++): ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                        <? endfor; ?>
                                    </select>
                                </p>
                                <p>
                                    <label for="giftvoucher">Gift Voucher Type:</label> 
                                    <select name="Gift Voucher" id="gv_giftvoucher" class="inputbox"> 
                                        <option value="">Please Select</option> 
                                        <? foreach($gift_voucher_options as $id=>$opt): ?>
                                            <option value="<?=$opt['price']?>"><?=$opt['printable']?></option>
                                        <? endforeach; ?>
                                    </select> 
                                </p>

                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="$(this).closest('form').submit()" class="button cta inline" type="submit" style=""><?=$button_text?></a>
                                <img style="margin-left:30px" class="deets" src="img/vouchers/04.jpg" />
                                <div class="clr"></div>
                            </form>
                        <?
                        $html=ob_get_clean();                
                    break;
                }      
                echo $html;                      
            ?>            
        </div>
    </section>      
</main>

@endsection