@extends('templates.header')

@section('body')

    <main class="normal textonly">
    <h1>Privacy Policy</h1>
    <p>As one of Australia's largest providers of Photo Books, Posters and Calendars, albumworks takes the protection of your Privacy seriously. Your Privacy is very important to us and we are bound by and conform to the Australian National Privacy Guidelines (which are part of the Privacy Act) and also adhere to the Anti-Spam Regulations.</p>

    <h2>Collection of information</h2>
    <p>In providing you access to the albumworks service we collect your email address and when you order albumworks photo books, posters or calendars or other products, billing and delivery information. We will only use this for the purpose that you provided it to us and we will not disclose the information to third parties without your approval. Your information is required to ensure billing and delivery of your products, however our suppliers are not permitted to retain, store, share, or use the data for any other purpose. We do not store your credit card information. No access will be provided to your information except where required by Australian law and in compliance with Australian legislation.</p>

    <h2>Email</h2>
    <p>If you have given your approval, we will from time to time send you information about albumworks products and services. This information is dispatched direct from our mail servers or via our email gateway service. You can unsubscribe from this list at any time by contacting Customer Service (service@albumworks.com.au or 1300 553 448) or by clicking the unsubscribe link at the base of every email that we send.</p>
</main>

@endsection