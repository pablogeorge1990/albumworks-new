@extends('templates.header')

@section('title', 'Quality Photo Books at affordable prices | albumworks')
@section('meta_description', 'Our beautiful Photo Books come in a range of sizes, covers and binding options. See our Photo Book prices.')

@section('body')
    <main class="normal static"> 
        <h2 class="con-head">Your order is confirmed! <img src="{{ asset('img/order-confirm/tick.png') }}" class="con-img"></h2> <br>

        <h2 class="con-subhead">Thank you! Your order has been accepted.</h2><br>

        <h2 class="con-head">Your order number is: <span class="con-order-no">{{ $orderId }}</span></h2><br>

        <h2 class="con-subhead2">If you have used the download Editor to create your order, please select the "Upload Order" option to send your project files to us.</h2>
        
        <br><br><br>
        <div class="promotions group">
            An order confirmation email will be sent to you shortly. <br>
            You will also receive an email with an Online Preview of your order to share with family and friends.
        </div>
        <br><br>
        <hr>
        <section class="static group semimodules">
            <div class="semimodule semimodule-con">
                <div>
                    <img src="{{ asset('img/order-confirm/make.png') }}" alt="">
                </div>
                <h2 class="con-bottom">Make another product</h2>
                <p>Choose another photo product to make and add to your cart. View our all new Gifts range!</p>
                <p><a href="gifts" class="cta">GIFTS</a></p>
            </div>
            <div class="semimodule semimodule-con">
                <div>
                    <img src="{{ asset('img/order-confirm/contact.png') }}" alt="">
                </div>
                <h2 class="con-bottom">Contact</h2>
                <p>Got a question? We can help. Visit our FAQs page here or click below to get in touch with us.</p>
                <p><a href="contact" class="cta">CONTACT</a></p>
            </div>
        </section>
    </main>

@endsection
