@extends('templates.header')

@section('title', 'Photo Books: Beautiful and personalised | albumworks')
@section('meta_description', 'Create beautiful personalised Photo Books. Easy to use, free software. 100% design freedom, fast delivery and local support.')

@section('body')

<main class="retail">
    <div class="ecommerce group">
        <h1 class="mobileonlyblock">Photo Books</h1>
        <div class="images">
            <img src="img/photobooks/retail/photobook1.jpg" width="510" height="510" class="primary">
            <nav class="others group">
                <img src="img/photobooks/retail/photobook1.jpg" alt="" imgid="">
                <img src="img/photobooks/retail/photobook2.jpg" alt="" imgid="">
                <img src="img/photobooks/retail/photobook3.jpg" alt="" imgid="">
                <img src="img/photobooks/retail/photocover.jpg" alt="" imgid="photocover">
                <img src="img/photobooks/retail/debossed.jpg" alt="" imgid="debossed">
                <img src="img/photobooks/retail/softcover.jpg" alt="" imgid="softcover">
                <img src="img/photobooks/retail/prem-material.jpg" alt="" imgid="material">
            </nav>
        </div>
        <div class="details">
            <h1 class="nomobile">Photo Book</h1>
            <p>
                <label for="field_binding">Binding:</label>
                <select name="binding" id="field_binding" class="inputbox">
                    <option>Classic</option>
                    <option>Layflat</option>
                    <option>Board Mounted Layflat</option>
                </select>
            </p>
            <p>
                <label for="field_cover">Cover:</label>
                <select name="cover" id="field_cover" class="inputbox">
                </select>
            </p>
            <p>
                <label for="field_product">Size:</label>
                <select name="product" id="field_product" class="inputbox">
                </select>
            </p>
            <p class="pages_container">
                <label>Pages:</label>
                <span class="likeabox">40</span>
            </p>
            <p class="price_container">
                <label>Price:</label>
                <span class="likeabox"> </span>
            </p>

            <p class="url_container nomobile">
                <a target="_blank" href="" class="cta">CREATE NOW</a>
            </p>

            <div class="mobileonlyblock" id="pbmobileform">
                <h3 class="reversed">Ready to make your Photo Book?<br/>Choose from 2 options!</h3>

                <h4 class="rondelheading"><span class="reversed rondel">1</span> CREATE ON DESKTOP</h4>
                <p>Experience our full Editor experience with our Desktop Editor - only available on desktop computers. Enter your details below and we'll send you a link to get started next time you're at a computer!</p>
                <form id="insitumobileform" class="webtolead" action="#" method="post" onsubmit="return checkmobileform(this)">
                    <input type="hidden" value="00D36000000oZE6" name="sfga">
                    <input type="hidden" value="00D36000000oZE6" name="oid">
                    <input type="hidden" class="mobreturl" value="https://www.albumworks.com.au/download/dl_process.php?ltc&ap&referringPromotion=MOBILEFORM" name="retURL">
                    <input type="hidden" value="Website" name="lead_source">
                    <input type="hidden" value="Web-to-Lead" name="00N20000001STqA">
                    <input type="hidden" value="Mobile Form" name="00N3600000BOyGd">
                    <input type="hidden" value="AP" name="00N3600000BOyAt">
                    <input type="hidden" value="PG" name="00N3600000Loh5K">
                    <input type="hidden" name="00N3600000Los6F" value="Windows">
                    <!-- Referring Promotion --><input type="hidden" value="MOBILEFORM" name="00N3600000LosAC">
                    <!-- Web-to-Lead Bad JS/Cookie check --><input type="hidden" value="1" name="00N3600000LosKl">
                    <!-- Link To Contact --><input type="hidden" value="1" name="00N3600000RTz4Z">
                    <!-- Adword fields -->
                    <input type="hidden" class="dlform_group" maxlength="255" name="00N3600000BOyH7" size="255" type="text" />
                    <input type="hidden" class="dlform_campaign" maxlength="255" name="00N3600000BOyGi" size="255" type="text" />
                    <input type="hidden" class="dlform_term" maxlength="255" name="00N3600000LotLP" size="255" type="text" />
                    <input type="hidden" class="dlform_keyword" maxlength="255" name="00N3600000BOyHC" size="255" type="text"/>
                    <input type="hidden" class="dlform_network" maxlength="255" name="00N3600000BOyHM" size="255" type="text" />
                    <input type="hidden" class="dlform_referrer" name="00N3600000BOyHR" size="255" type="text"/>
                    <input type="hidden" class="dlform_match" name="00N3600000BOyHH" size="255" type="text"/>
                    <input type="hidden" class="dlform_position" name="00N3600000LotwB" size="255" type="text"/>
                    <input type="hidden" class="dlform_target" name="00N3600000MdGNq" size="255" type="text"/>
                    <!-- Actual fields -->

                    <div class="group">
                        <div class="dlformbit">
                            <input type="hidden" id="situfield_lightbox_mobile_first_name" name="first_name" value="Name" class="field_lightbox_mobile_first_name firstname smartedit inputbox">
                            <label for="situfield_lightbox_mobile_email">Email:</label>
                            <input type="text" id="situfield_lightbox_mobile_email" name="email" class="field_lightbox_mobile_email email smartedit inputbox">
                            <div class="checkarea">
                                <input type="checkbox" id="situfield_lightbox_mobile_emailoptout" checked="checked" value="1" name="emailOptOut" class="field_lightbox_mobile_emailoptout check">
                                <label for="situfield_lightbox_mobile_emailoptout">Keep me updated with special offers and software updates</label>
                            </div>
                            <p><a class="cta" href="javascript:void(0)" onclick="$(this).closest('form').submit();">SUBMIT</a></p>
                        </div>
                    </div>
                </form>

                <h4 class="rondelheading"><span class="reversed rondel">2</span> CREATE ONLINE</h4>
                <p>You can still make a Photo Book making our online Editor. You will have limited editing abilities and restricted to just 40 pages.</p>
                <p class="url_container">
                    <a target="_blank" href="" class="cta">CREATE NOW</a>
                </p>
            </div>

            <div class="about">
                <p><strong>ABOUT:</strong></p>
                <p>No one knows more about making stunning Photo Books than we do. With over 10 years of experience, and almost 1,000,000 Books made, we reckon our books are the best in the business.<br><br>All of our Classic Photo Books come in Standard Definition, or unsurpassed High Definition. Or try one of our Layflat Photo Books and allow your images to flow from one page to the next without losing valuable parts of your image.</p>
            </div>
        </div>
    </div>
    <section class="static group semimodules">
        <div class="semimodule" style="background-image:url(img/calcicon.jpg)">
            <h2>Pricing</h2>
            <p>Take out the guess work with our nifty pricing calculator. Add optional extras or change your paper type - our calculator does all the work for you!</p>
            <p><a href="calculator" class="cta">Calculate</a></p>
        </div>
        <div class="semimodule" style="background-image:url(img/contact.jpg)">
            <h2>Contact</h2>
            <p>Still have questions? Ask us! Our amazing Customer Service team are all experts in Photo Book creation. And they're right here in Australia with you. So on the rare occasion an issue arises, we can sort it out as fast as possible.</p>
            <p><a href="contact" class="cta">CONTACT</a></p>
        </div>
    </section>
    <script type="text/javascript">
        <?
            $data = [
                'Classic' => [
                    'Softcover' => [
                        '6x6' => [
                            'price' => 19.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ZWM1ZjFmODY,.LyQwj88BxuHY6ABMm3CxNeAGrBTkkRbDmT5A3htDz7g,',
                        ],
                        '8x6' => [
                            'price' => 19.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.NGEzY2ExN2I,.8sMLR45ruwBQiSEuZQVDzs04YUQcpYjOJw0z1EVM7pg,',
                        ],
                        '8x8' => [
                            'price' => 37.50,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.MzhjMTY5MmE,.fPkiKlmcJA0OoHbo18Iz5MegqNL1OEhtADvUcHXP3r4,',
                        ],
                        '8x11' => [
                            'price' => 44.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.OWQwNmVjOTA,.s73XTa2c5XONISUbIFeL__X2RBjx9h_K62TYZdtJZ_w,',
                        ],
                        '11x8.5' => [
                            'price' => 49.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.ZDdiNzcxNmE,.GQ6PAgkltmrF9MrJydrVMsJRg5LC6JoDncUqL27wfo_3LZgRhUnQOA,,',
                        ],
                        '12x12' => [
                            'price' => 66.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.YmRlNGRhOWY,.kOnMomHaea-9NAFH4Rr4oBkN-LxLInvtc_-jTaTfMOdGBVqM1BTluA,,',
                        ],
                    ],
                    'Photocover' => [
                        '8x6' => [
                            'price' => 37.50,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.ZmRlYjhjNWI,.ihn7rqcodSYYUO-GvhHc521QKmvz6ZpZBl2QFoblF-o,',
                        ],
                        '8x8' => [
                            'price' => 57.50,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.ZjE3ZWJkM2Q,.Keq63h8mwTUvcUYfAsjiRUNuSIOpSWbpyjdP4qEAfwk,',
                        ],
                        '8x11' => [
                            'price' => 69.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.NDM0NTNiZWU,.k6MavCyXC_zo_OJa69G7tfJZGHf2g2kQ_kfUQgtnBmE,',
                        ],
                        '11x8.5' => [
                            'price' => 69.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.MTYzYzZhNjA,.frlbc7-uC_dmy-7aCJtCCeYOHWlYdSTfA9B0OJFw5nhu83vptsdo5Q,,',
                        ],
                        '12x12' => [
                            'price' => 86.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.NDY4ZWU2NDE,.67nQmb4QtaVq4EPYhlpExxfmdEnkjTyR2U7IsuUWKHt-YoEhQUWDlQ,,',
                        ],
                        '16x12' => [
                            'price' => 119.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=35.NTc3YjE3M2M,.gpo0gZfFLPEdDJx3IF62dWJw22jzl0A0rRSPiZKswoAa2V8n4kAtKg,,',
                        ],
                    ],
                    'Debossed Cover' => [
                        '8x8' => [
                            'price' => 67.50,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=31.ZGUyM2Y4M2I,.imc9azjutZjkoRvFEDOcDmSqjAKKnQSAN7RWcw5F414,',
                        ],
                        '8x11' => [
                            'price' => 79.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.M2NjYTQ1YzU,.gI6p-zzCX0q6biWZ8ogU6Efga6Sla7B4zjO8v_APGaw,',
                        ],
                        '11x8.5' => [
                            'price' => 79.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.Mzg5ZDBhMjM,.lbvzvaw11ooA67qJoDGDav8NQcpyL7LQsrV-iwosaAAR4uEh2rgOrg,,',
                        ],
                        '12x12' => [
                            'price' => 96.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.MmQ5YmIzMmU,.H9fcWjm39EIj1zqIoLT4Ch9TNEOmMLLiRLrCRynM-A3Y2FmzDUSlcQ,,',
                        ],
                    ],
                    'Premium Material' => [
                        '12x12' => [
                            'price' => 96.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=37.NGYyYzhmMzA,.UJAh03qvF7e56pN41XKQT7NZNX-iJFcBsOrxPO1GB6jxY85mPqBUiA,,',
                        ],
                        '16x12' => [
                            'price' => 129.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=37.N2MzYWQ1MmU,.S7iqNAmSvz26AH6fCR4u8R1xsCVotH5QRwb7CnKmpQiAbguBwb8fng,,',
                        ],
                    ],
                ],
                'Layflat' => [
                    'Photocover' => [
                        '8x6' => [
                            'price' => 54.50,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=37.YTY1NDZjOWY,.vi98Dhar5tJI1SpHgqA-MNRkWInc_fhSrvXyVtPb1k64qkoRCcu2Cw,,',
                        ],
                        '8x8' => [
                            'price' => 74.50,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=37.YjA3ZWRjNDY,.WJj8v5BrBGh0O26MhLo9WS_b2MOF_gL6JWINQfhqgl4P3x9BwxA1Aw,,',
                        ],
                        '8x11' => [
                            'price' => 79.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=38.M2Y5ZGJjOGM,.Uad_0XLZrvc4xsNYxH5hdN89tMWFGopuV8vbboSStTaMU2FxMfuMTg,,',
                        ],
                        '11x8.5' => [
                            'price' => 79.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=39.YTJiZjkyMzc,.eJhB_LQ9gQU4XzqCrM_3LHAvWNTbbt8Wc2vY1pCT_zOIQNj2umRKgQ,,',
                        ],
                        '12x12' => [
                            'price' => 96.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=40.Zjk4OGEwOWU,.OXgwqDuQNi8yrTZrEk8aRXc_tjGmeeI-EDQ4h64qWR18082u5ME0tQ,,',
                        ],
                        '16x12' => [
                            'price' => 129.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=41.ZjY1NDU4NDU,.qtZqs9nuglCJ20tXWDtjBdGmVbMMRf7CMa1iZqy1TjtuSbqdrsMmlsNeaePBS8pD',
                        ],
                    ],
                    'Debossed Cover' => [
                        '8x8' => [
                            'price' => 82.50,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=37.ODNiYTljZjE,.oDA9RgwBA0ZFhf4-xckTxN2cIZSnceIibwDGYhRhPEgFKoaeiG15dg,,',
                        ],
                        '8x11' => [
                            'price' => 89.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=38.NjgwZWJiN2M,.iHzgGaISe-sgYX-PzsUen7DTNWvTZvWGutmJbZYkDUqgicRM2N_o5A,,',
                        ],
                        '11x8.5' => [
                            'price' => 89.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=39.ZDM1ZGI4ZGM,.6w65DXzr1bI5jPgVKhGE2QaMdLglpblmNM0_kzbhK_IG36VKqyBcbQ,,',
                        ],
                        '12x12' => [
                            'price' => 106.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=40.ODgzNmU2NDU,.yzfofAhGqiP94KQHZudhMfFOgc2Y_RiElvghtYp-1Rdlq40O1BDCrQ,,',
                        ],
                    ],
                    'Premium Material' => [
                        '12x12' => [
                            'price' => 106.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=40.ZjVjNjhmY2Y,.aCadtQxPnKqRVMYhwB2sbyq7dvSRwWB8o0lfbeXNbgv4GA_DpLpC7Q,,',
                        ],
                        '16x12' => [
                            'price' => 139.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=40.OTk4YzgzMDU,.fc6CLSvG-wcA-v0M30JF4U2tE4ETyx7LqQ4J8g3GapUmgnaeJykDEw,,',
                        ],
                    ],
                ],
                'Board Mounted Layflat' => [
                    'Photocover' => [
                        '8x8' => [
                            'price' => 199.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ZjE5YWYwMzU,.v0CMSUQDZPhQtmPPgCzAM3GhUniADR_yk0RC2dEtdq8,',
                        ],
                        '8x11' => [
                            'price' => 279.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.MGFmYWEzYTE,.HMLH07UfHuKRjVJO6eR1yHNyDaq8uBtqekjS_AaUH2BfPYpHXb8QnA,,',
                        ],
                        '11x8.5' => [
                            'price' => 279.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.ODY1YTljZmE,.B7k1J3JX56oIC2uR5uHxWoW4dQxCUx1Way0NSceJMratFLpX6KaKDA,,',
                        ],
                        '12x12' => [
                            'price' => 349.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.ZTkxNzY4ZjU,.uLO_hpgIdChzKWgg2CnMR0511LzWpPGdU5lRtB-X5CWSQRHTj7i_bw,,',
                        ],
                    ],
                    'Debossed Cover' => [
                        '8x8' => [
                            'price' => 209.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.ODQxOWYxN2Y,.6VOfELSyj8EoY3HftKXP9glcurDcn0JLulTPCEASSvw,',
                        ],
                        '8x11' => [
                            'price' => 289.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.MjRiMjc3MmQ,.PKBfU_nGOQRIB5JO8x_Q5fIA1hu8022yvV_qrFCoBi57knBR8pkkrg,,',
                        ],
                        '11x8.5' => [
                            'price' => 289.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.NGZjODZlNTQ,.AYlGVQpTD88CD3o6YFmzt0ymZMSv0XiKSFv0wE2zXtby_6c5kGlSDA,,',
                        ],
                        '12x12' => [
                            'price' => 359.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.NmU1NjA1MGY,.muj5aj1TTfYEqrLR3VxowJr7S908ys4EDSDoDE_obc-qTsIcd1PeDg,,',
                        ],
                    ],
                    'Premium Material' => [
                        '6x6' => [
                            'price' => 149.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.MjYzOWZhYWM,.AHlzyKIVbGN-N8zI8TNJVrWdypwCpgwfzpSebnqDwiI,',
                        ],
                        '8x8' => [
                            'price' => 209.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=32.NDg4MTc2NzA,.rkNPx6Tpy5ga49mMA7lML33hPi10QjKVaYkfSaBStdg,',
                        ],
                        '8x11' => [
                            'price' => 289.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=33.NzQ3NDI5ZjM,.6pEqkfMXd_6fhW4eWFGUD9aB8EHKB3hZw4xxTHFsAAZVOndp-OmAJQ,,',
                        ],
                        '11x8.5' => [
                            'price' => 289.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.OWU5MWM1ZWI,.FlIgKlh_R80b5wDe5YYx-v7XChGp93geq7HeiBb7JUhHMxarUHcAGA,,',
                        ],
                        '12x12' => [
                            'price' => 359.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.NDhhMDA1NTQ,.wH1exVoH3el4lCUvxYBFOjoNQlCmUye9exJzR-2ovb3pavYQsDD1QQ,,',
                        ],
                        '16x12' => [
                            'price' => 449.95,
                            'url' => 'https://orders.photo-products.com.au/?fsaction=OnlineAPI.create&id=34.YjY1ZjExYTQ,.Cnwgg44baqd9bWpjllxntgMLzPH7HXpJrLmmgneIlwK1IPJT3KTH2A,,',
                        ],
                    ],
                ],
            ];
        ?>
        var complicated_urls = 'ignore';
        var __data = <?=json_encode($data)?>;
        
        $(document).ready(function(){
            $('#field_binding').change(function(){
                var newval = $(this).val();
                
                $('#field_cover option').remove();
                $.each(__data[newval], function(cover, sizes){
                    $('#field_cover').append('<option>'+cover+'</option>');
                });
                $('#field_cover').change();
            });
            $('#field_binding').change();
            
            $('#field_cover').change(function(){
                var binding = $('#field_binding').val();
                var newval = $(this).val();
                
                switch(newval){
                    case 'Softcover': $('.images .others img[imgid=softcover]').click(); break;
                    case 'Photocover': $('.images .others img[imgid=photocover]').click(); break;
                    case 'Debossed Cover': $('.images .others img[imgid=debossed]').click(); break;
                    case 'Premium Material': $('.images .others img[imgid=material]').click(); break;
                }
                
                $('#field_product option').remove();
                $.each(__data[binding][newval], function(size, data){
                    size = size.replace('x', ' x ')+'"';
                    $('#field_product').append('<option>'+size+'</option>');
                });
                $('#field_product').change();
            });
            $('#field_cover').change();
            
            $('#field_product').change(function(){
                var binding = $('#field_binding').val();
                var cover = $('#field_cover').val();
                var newval = $(this).val().replace(' x ', 'x');
                newval = newval.substring(0, newval.length - 1);
                                              
                $('.url_container a').attr('href', __data[binding][cover][newval]['url']);
                $('.price_container span').text(curr(__data[binding][cover][newval]['price']));
            });

            window.setTimeout(function(){
                $('#field_product').change();
            }, 1000);
        });
    </script>
</main>

@endsection