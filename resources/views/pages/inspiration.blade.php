@extends('templates.header')
@section('title', 'Photo Books: Beautiful and personalised | albumworks')
@section('meta_description', 'Create beautiful personalised Photo Books. Easy to use, free software. 100% design freedom, fast delivery and local support.')

@section('body')

    <main class="normal static">
        <h1>Inspiration</h1>
        <iframe width="100%" height="500" src="#" frameborder="0" scrolling="no"></iframe>
        <div class="">

            <div class="">
                <div class="inspiration_desc">
                    <p>Coming soon.</p>
                </div>
            </div>
            <div class="inspiration_btn"><a href="javascript:splitchoice('popular')" class="cta">GET STARTED</a></div>
        </div>
        <p class="exploremore">Explore more Photo Books</p>
        <table class="inspiration_explore_more">
            <tr>
                <td>
                    <a href=" https://www.albumworks.com.au/inspiration-travel">
                        <img src="img/inspiration/travel_thumb.png"/>
                    </a>
                    <p>Travel Photo Book</p>
                </td>
                <td>
                    <a href="https://www.albumworks.com.au/inspiration-weddings">
                        <img src="img/inspiration/wedding_thumb.png"/>
                    </a>
                    <p>Wedding Photo Book</p>
                </td>
                <td>
                    <a href="https://www.albumworks.com.au/inspiration-family">
                        <img src="img/inspiration/family_thumb.png"/>
                    </a>
                    <p>Family Photo Book</p>
                </td>
                <td>
                    <a href="https://www.albumworks.com.au/inspiration-baby">
                        <img src="img/inspiration/baby_thumb.png"/>
                    </a>
                    <p>Baby Photo Book</p>
                </td>
            </tr>
        </table>
    </main>@endsection
