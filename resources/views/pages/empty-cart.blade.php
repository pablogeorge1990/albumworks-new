
@extends('templates.header')

@section('title', 'Quality Photo Books at affordable prices | albumworks')
@section('meta_description', 'Our beautiful Photo Books come in a range of sizes, covers and binding options. See our Photo Book prices.')

@section('body')
    <main>
        <section id="intro">
            <h1>Empty Cart</h1>
            <p>Sorry!! Your Shopping Cart is Empty...</p>
        </section>
    </main>

@endsection