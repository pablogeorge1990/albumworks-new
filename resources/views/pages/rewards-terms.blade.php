@extends('templates.header')

@section('body')

    <main class="normal textonly">
    <h1>Terms and conditions of <em>albumworks</em> rewards</h1>
    <ol class="terms">
        <li>These Terms and Conditions are the basis of <em>albumworks</em> Rewards. It is the member's responsibility to read and understand them.</li>
        <li>These Terms and Conditions should be read in conjunction with the Terms and Conditions of using the <em>albumworks</em> website or placing an order via the <em>albumworks</em> Editor.</li>
        <li>These Terms and Conditions became effective 1  August 2015 and are amended by <em>albumworks</em> from time to time. The current Terms and Conditions are those available at this website.</li>
        <li>Every Member is bound by these Terms and Conditions.</li>
        <li>You automatically become a Member of <em>albumworks</em> Rewards on placing an order with <em>albumworks</em> to the value of over $50. You may elect to opt out of <em>albumworks</em> Rewards by emailing <script type="text/javascript">
            //<![CDATA[
            <!--
            var x="function f(x){var i,o=\"\",l=x.length;for(i=l-1;i>=0;i--) {try{o+=x.c" +
                "harAt(i);}catch(e){}}return o;}f(\")\\\"function f(x,y){var i,o=\\\"\\\\\\\""+
                "\\\\,l=x.length;for(i=0;i<l;i++){y%=127;o+=String.fromCharCode(x.charCodeAt" +
                "(i)^(y++));}return o;}f(\\\"\\\\\\\\\\\\\\\\\\\\VYNQXPKn60*0 *)`kv*l%<*6l\\" +
                "\\\\\\016q94?;,6`(9/(6\\\\\\\\003\\\\\\\\004\\\\\\\\\\\"\\\\\\\\\\\\002\\\\" +
                "\\\\010\\\\\\\\007\\\\\\\\023\\\\\\\\n\\\\\\\\037\\\\\\\\006\\\\\\\\030\\\\" +
                "\\\\000\\\\\\\\037C\\\\\\\\r\\\\\\\\000\\\\\\\\035_\\\\\\\\023\\\\\\\\006(W" +
                "V\\\\\\\\003\\\\\\\\021\\\\\\\\r\\\\\\\\026\\\\\\\\036A!\\\\\\\\\\\\\\\\\\\\"+
                "\\\\\\\\\\\\#<pawpnklJj`o{bg~`xg;uxu7{n 2\\\\\\\\177!\\\\\\\\002\\\\\\\\010" +
                "\\\\\\\\031\\\\\\\\023\\\\\\\\037\\\"\\\\,56)\\\"(f};)lo,0(rtsbus.o nruter}" +
                ";)i(tArahc.x=+o{)--i;0=>i;1-l=i(rof}}{)e(hctac};l=+l;x=+x{yrt{)29=!)31/l(tA" +
                "edoCrahc.x(elihw;lo=l,htgnel.x=lo,\\\"\\\"=o,i rav{)x(f noitcnuf\")"         ;
            while(x=eval(x));
            //-->
            //]]>
            </script> asking to be excluded from the Rewards program.</li>
        <li>Membership of <em>albumworks</em> Rewards is open only to individuals. Membership is NOT open to families, groups, companies, trusts, partnerships, other entities, government departments, agencies, animals or inanimate objects.</li>
        <li>There are four Membership Levels to <em>albumworks</em> rewards - White, Blue, Silver and Gold.</li>
        <li>Conditions of <em>albumworks</em> Rewards
        <ol>
            <li>Rewards dollars are applied as per the reward level defined at <a href="http://www.albumworks.com.au/rewards">http://www.albumworks.com.au/rewards</a> against the account used to place the order.</li>
            <li>Rewards dollars are earned only on Australian dollars spent per order, after any discount, existing Rewards dollars or gift voucher usage.</li>
            <li>Rewards dollars are not earned on gift voucher purchases.</li>
            <li>Members progress to the higher levels based on the aggregate value of orders placed.</li>
            <li>Rewards dollars cannot be transferred or traded and may only be used against the account they were earned in.</li>
            <li>Rewards dollars can only be applied against <em>albumworks</em> products and not Gift Vouchers, they cannot be converted to cash.</li>
            <li>Rewards dollars can only be applied to new project orders and cannot be redeemed in <em>albumworks</em> Albumshare</li>
            <li>Rewards dollars will not expire provided a member purchases at least one product in each 12 month period of greater than $50 in value. All Rewards dollars <span style="text-decoration: underline">will expire at the end of the 12 month period in which a member has not placed an order of greater than $50 in value.</span></li>
            <li>Once expired, Rewards dollars cannot be re-credited.</li>
            <li>Bulk orders and orders in commercial quantities are excluded from the rewards program. This condition applies to any order which has received a discount which is not available to the general <em>albumworks</em> customer base.</li>
        </ol>
        </li>
        <li>The Rewards dollars account balance will be displayed in the shopping cart when an order is placed.</li>
        <li>We reserve the right to make any changes to the Terms and Conditions or Rewards or Reward levels at any time.</li>
        <li>We give no warranty as to the continuing availability of <em>albumworks</em> Rewards and <em>albumworks</em> Rewards can be terminated at any time at <em>albumworks</em> discretion.</li>
        <li>We reserve the right to suspend the Rewards dollars of any member for any reason.</li>
        <li>Subject to non-excludable warranties under consumer protection legislation, <em>albumworks</em> and any of its staff or officers are not liable for any loss or claim of any kind that arises out of or is in any way connected with these Terms and Conditions including any changes to these Terms and Conditions.</li>
    </ol>
</main>

@endsection